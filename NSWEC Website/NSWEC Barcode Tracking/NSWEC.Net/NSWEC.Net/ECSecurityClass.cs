﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Drawing;
using System.Net;
using System.Globalization;
using System.Text.RegularExpressions;
using SMCryptography;
using DevExpress.Web;
using DevExpress.Web.ASPxHtmlEditor;

namespace NSWEC.Net
{
    public class ECSecurityClass
    {
        private static string connStr = ConfigurationManager.ConnectionStrings["DefaultDBConString"].ToString();
        private static int _maxSupportLoginCount = 10;
        private static string _passPhrase = "5upplyM@5t3r"; //"3cB@rc0d3Tr@k1ng";
        private static string _initVector = "h8G7f6E5d4C3b2@1";
        private static string _urlPassPhrase = "5upplyM@5t3r0nl1n3";//"3cB@rc0d3Tr@k1ng0nl1n3";
        public const string REDIRECT_BLOCKED_HTML = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html lang=\"en\" xml:lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"><head><title>Request Denied</title><style type=\"text/css\">body {font-family: Arial, Helvetica, Verdana, Sans-Serif;font-size: small;font-weight: normal;color: #000000;}div {margin-left: auto;margin-right: auto;text-align: center;}.box {width: 600px;background-color: #F2F2F2;border-left: solid 1px #C2C2C2;border-right: solid 1px #C2C2C2;vertical-align: middle;padding: 20px 10px 20px 10px;}p {text-align: left;}.red {font-weight: bold;color: Red;text-align: center;}.band {height: 20px;color: White;background: rgb(30, 181, 146);width: 600px;border-left: solid 1px #333333;border-right: solid 1px #333333;padding: 3px 10px 0px 10px;}div#wrap {margin-top: 50px;}</style></head><body><div id=\"wrap\"><div class=\"band\"></div><div class=\"box\"><p class=\"red\">Request denied by SmartFreight.</p><p><b> Reason: </b> -9/ Too many invalid login attempts (Temporarily locked)</p><p>Please try again later.</p></div><div class=\"band\"></div></div></body></html>";
        public const string REDIRECT_BLOCKED_HTML1 = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html lang=\"en\" xml:lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"><head><title>Request Denied</title><style type=\"text/css\">body {font-family: Arial, Helvetica, Verdana, Sans-Serif;font-size: small;font-weight: normal;color: #000000;}div {margin-left: auto;margin-right: auto;text-align: center;}.box {width: 600px;background-color: #F2F2F2;border-left: solid 1px #C2C2C2;border-right: solid 1px #C2C2C2;vertical-align: middle;padding: 20px 10px 20px 10px;}p {text-align: left;}.red {font-weight: bold;color: Red;text-align: center;}.band {height: 20px;color: White;background: rgb(30, 181, 146);width: 600px;border-left: solid 1px #333333;border-right: solid 1px #333333;padding: 3px 10px 0px 10px;}div#wrap {margin-top: 50px;}</style></head><body><div id=\"wrap\"><div class=\"band\"></div><div class=\"box\"><p class=\"red\">Request denied by SmartFreight.</p><p><b> Reason: </b>You are not allowed to visit this web site.</p></div><div class=\"band\"></div></div></body></html>";
        static readonly char[] padding = { '=' };

        public ECSecurityClass() { }

        public static string SiteKeyV3()
        {
            string result = string.Empty;
            object objSiteKeyV3 = ConfigurationManager.AppSettings["SiteKeyV3"];
            if (objSiteKeyV3 != null)
                result = objSiteKeyV3.ToString();
            return result;
        }
        public static double ScoreV3()
        {
            double result = 0;
            object objScoreV3 = ConfigurationManager.AppSettings["ScoreV3"];
            if (objScoreV3 != null)
                result = Convert.ToDouble(objScoreV3);
            return result;
        }
        public static string SecretKeyV3()
        {
            string result = string.Empty;
            object objSecretKeyV3 = ConfigurationManager.AppSettings["SecretKeyV3"];
            if (objSecretKeyV3 != null)
                result = objSecretKeyV3.ToString();
            return result;
        }

        public static int MaxLoginAttempts()
        {
            int result = 0;
            object objMaxLoginAttempts = ConfigurationManager.AppSettings["MaxLoginAttempts"];
            if (objMaxLoginAttempts != null)
                result = ECWebClass.StrToIntDef(objMaxLoginAttempts.ToString(), result);
            return result;
        }
        public static int ShowCaptchaAttempts()
        {
            int result = 0;
            object objShowCaptchaAttempts = ConfigurationManager.AppSettings["ShowCaptchaAttempts"];
            if (objShowCaptchaAttempts != null)
                result = ECWebClass.StrToIntDef(objShowCaptchaAttempts.ToString(), result);
            return result;
        }
        public static int LockDuration()
        {
            int result = 0;
            object objLockDuration = ConfigurationManager.AppSettings["LockDuration"];
            if (objLockDuration != null)
                result = ECWebClass.StrToIntDef(objLockDuration.ToString(), result);
            return result;
        }

        public static LoginUser GetLoginUser(string userName, string password, ref string processMsg)
        {
            LoginUser loginUser = null;
            bool result = userName.ToLower() == "sm4web" && password == "5m4w3b@dm1n";
            if (result)
                loginUser = new LoginUser(userName, password, true);
            else
            {
                result = userName.ToLower() == "sfaccount" && password == "v6tDK%vs";
                if (result)
                    loginUser = new LoginUser(userName, password, false);
                else
                {
                    object maxSupLoginCountObj = ConfigurationManager.AppSettings["MaxSupportLoginCount"];
                    if (maxSupLoginCountObj != null)
                    {
                        int maxSupLoginCount = ECWebClass.StrToIntDef(maxSupLoginCountObj.ToString(), 0);
                        if (maxSupLoginCount > 0)
                            _maxSupportLoginCount = maxSupLoginCount;
                    }

                    DataTable dtLogin;
                    using (SqlConnection sqlConnection = new SqlConnection(connStr))
                    {
                        // check support login
                        string suppLogin = string.Empty;
                        bool useSuppSeq = false;
                        DataTable dtApp = GetApplicationDataTable();
                        if (dtApp != null && dtApp.Rows.Count > 0)
                        {
                            DataRow drApp = dtApp.Rows[0];
                            if (!(drApp["SupportLogin"] is DBNull) && drApp["SupportLogin"].ToString() != string.Empty)
                                suppLogin = drApp["SupportLogin"].ToString().ToLower();
                        }
                        if (!string.IsNullOrEmpty(suppLogin) && userName.ToLower().StartsWith(suppLogin))
                        {
                            string suppSeq = userName.Substring(suppLogin.Length);
                            if (!string.IsNullOrEmpty(suppSeq))
                            {
                                int seq = ECWebClass.StrToIntDef(suppSeq, 0);
                                useSuppSeq = seq > 0 && seq <= _maxSupportLoginCount;
                            }
                        }
                        dtLogin = new DataTable();
                        string sql = "select * from NSW_Application where SupportLogin=@SupportLogin ";
                        using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                        {
                            if (useSuppSeq && !string.IsNullOrEmpty(suppLogin))
                                cmd.Parameters.AddWithValue("@SupportLogin", suppLogin);
                            else
                                cmd.Parameters.AddWithValue("@SupportLogin", userName);
                            using (SqlDataAdapter adapter = new SqlDataAdapter())
                            {
                                adapter.SelectCommand = cmd;
                                adapter.Fill(dtLogin);
                                result = dtLogin != null && dtLogin.Rows.Count == 1;
                                if (result)
                                {
                                    CryptographyRijndael _key = new CryptographyRijndael(_passPhrase, _initVector);
                                    result = password == _key.Decrypt(dtLogin.Rows[0]["SupportPassword"].ToString());
                                    if (result)
                                        loginUser = new LoginUser(userName, password, true);
                                }
                                if (!result)
                                {
                                    // normal login
                                    dtLogin = new DataTable();
                                    cmd.CommandText = @"select * from SecurityLogin where LoginId=@LoginId ";
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@LoginId", userName);
                                    adapter.SelectCommand = cmd;
                                    adapter.Fill(dtLogin);
                                    result = dtLogin != null && dtLogin.Rows.Count == 1;
                                    if (result)
                                    {
                                        DataRow drLogin = dtLogin.Rows[0];
                                        result = !(drLogin["Active"] is DBNull) && drLogin["Active"].ToString() != string.Empty &&
                                            Convert.ToChar(drLogin["Active"]) == 'Y';
                                        if (!result)
                                        {
                                            processMsg = "Your login account has been disabled, please contact administrator for support";
                                            return null;
                                        }

                                        CryptographyRijndael _key = new CryptographyRijndael(_passPhrase, _initVector);
/*
                                        string aa = _key.Decrypt(dtLogin.Rows[0]["Password"].ToString());
                                        
                                        aa = _key.Decrypt("4Z+DylOdolIkhX/3c2T8lA==");
                                        aa = _key.Decrypt("A2ZOGjhvATObv5nmiAQVyw==");
                                        aa = _key.Decrypt("1jer1YW1ILHvPNgAVDNgtg==");
                                         * */
                                        result = password == _key.Decrypt(dtLogin.Rows[0]["Password"].ToString());
                                        if (result)
                                        {
                                            if (!(dtLogin.Rows[0]["PasswordExpires"] is DBNull))
                                            {
                                                DateTime dtLoginExpires = Convert.ToDateTime(dtLogin.Rows[0]["PasswordExpires"]).AddDays(1);
                                                TimeSpan ts = dtLoginExpires - DateTime.Now;
                                                result = ts.Days > 0;
                                                if (!result)
                                                {
                                                    processMsg = "Your login password has expired and must be changed";
                                                    return null;
                                                }
                                                if (ts.Days < 4)
                                                    processMsg = string.Format("Your login password will expire in {1} day(s).{0}Please go to 'User Setup' page to change your password.",
                                                        Environment.NewLine, ts.Days);
                                            }
                                            if (result)
                                                loginUser = new LoginUser(userName, password, false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return loginUser;
        }

        public static bool CanAccessPage(LoginUser loginUser, string pageId)
        {
            bool result = loginUser != null && loginUser.SpecialUser;
            if (!result)
            {
                DataTable dtComponentAccess = GetLoginUserComponentAccess(null);
                result = dtComponentAccess == null || dtComponentAccess.Rows.Count == 0;
                if (!result)
                {
                    DataRow[] drs = dtComponentAccess.Select(string.Format("[Url]='~/{0}'", pageId));
                    result = drs == null || drs.Length == 0;
                    if (!result)
                    {
                        dtComponentAccess = GetLoginUserComponentAccess(loginUser.UserName);
                        result = dtComponentAccess != null && dtComponentAccess.Rows.Count > 0;
                        if (result)
                        {
                            drs = dtComponentAccess.Select(string.Format("[Url]='~/{0}'", pageId));
                            result = drs != null && drs.Length > 0;
                        }
                    }
                }
            }

            return result;
        }
        public static void ConfigureMenuForLogin(LoginUser loginUser, ASPxMenu mainMenu)
        {
            mainMenu.ClientSideEvents.ItemClick = @"function(s, e) {  
                    e.processOnServer = false; 
                    if (e.item.parent.name == 'itmTheme') { 
                        ASPxClientUtils.SetCookie('EC_Theme', e.item.GetText()); 
                        e.processOnServer = true; 
                    } 
                } ";

            DataTable dtUserComponentAccess = GetLoginUserComponentAccess(null);
            DataRow[] drsMenuAccess, drsComponentFrame;

            DevExpress.Web.MenuItem mItemSetup = mainMenu.Items.FindByName("mItemSetup") as DevExpress.Web.MenuItem;
            DevExpress.Web.MenuItem mItemReport = mainMenu.Items.FindByName("mItemReport") as DevExpress.Web.MenuItem;

            if (loginUser.SpecialUser)
            {
                DevExpress.Web.MenuItem itmSystemSetup = new DevExpress.Web.MenuItem("System Setup", "itmSystemSetup", string.Empty,
                    "~/SystemSetup.aspx?qs=" + GetQSEncripted(String.Format("loginId={0}", loginUser.UserName)));
                itmSystemSetup.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                mItemSetup.Items.Add(itmSystemSetup);

                DevExpress.Web.MenuItem mItemUtilities = mainMenu.Items.FindByName("mItemUtilities") as DevExpress.Web.MenuItem;

                DevExpress.Web.MenuItem itmImportContainer = new DevExpress.Web.MenuItem("Import PPDM", "itmImportContainer", string.Empty, "~/ImportPPDM.aspx?qs=" +
                    GetQSEncripted(String.Format("loginId={0}", loginUser.UserName)));
                itmImportContainer.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                mItemUtilities.Items.Add(itmImportContainer);

                DevExpress.Web.MenuItem itmImportSecDevice = new DevExpress.Web.MenuItem("Import Security Device", "itmImportSecDevice", string.Empty, "~/ImportDevice.aspx?qs=" +
                    GetQSEncripted(String.Format("loginId={0}", loginUser.UserName)));
                itmImportSecDevice.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                mItemUtilities.Items.Add(itmImportSecDevice);
                /*
                DevExpress.Web.MenuItem itmLabelDesigner = new DevExpress.Web.MenuItem("Label Designer", "itmLabelDesigner", string.Empty, loginUser.LabelDesignerLink);
                itmLabelDesigner.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                mItemUtilities.Items.Add(itmLabelDesigner);
                 */

                DevExpress.Web.MenuItem mItemScanReport = new DevExpress.Web.MenuItem("Barcode Scan Summary Report", "mItemScanReport", string.Empty, "~/BarcodeScanSummaryRptParam.aspx?qs=" +
                    GetQSEncripted(String.Format("loginId={0}", loginUser.UserName)));
                mItemScanReport.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                mItemReport.Items.Add(mItemScanReport);
            }
            else
            {
                if (loginUser.IsAdmin)
                {
                    DevExpress.Web.MenuItem mItemUtilities = mainMenu.Items.FindByName("mItemUtilities") as DevExpress.Web.MenuItem;

                    DevExpress.Web.MenuItem itmImportContainer = new DevExpress.Web.MenuItem("Import Consignment Container", "itmImportContainer", string.Empty, "~/ImportPPDM.aspx?qs=" +
                        GetQSEncripted(String.Format("loginId={0}", loginUser.UserName)));
                    itmImportContainer.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                    mItemUtilities.Items.Add(itmImportContainer);

                    DevExpress.Web.MenuItem itmImportSecDevice = new DevExpress.Web.MenuItem("Import Security Device", "itmImportSecDevice", string.Empty, "~/ImportDevice.aspx?qs=" +
                        GetQSEncripted(String.Format("loginId={0}", loginUser.UserName)));
                    itmImportSecDevice.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                    mItemUtilities.Items.Add(itmImportSecDevice);
                }
                drsMenuAccess = dtUserComponentAccess.Select("FormName = 'MasterPage'");
                if (drsMenuAccess != null && drsMenuAccess.Length > 0)
                {
                    DevExpress.Web.MenuItem menItem, subItem;
                    for (int i = 0; i < drsMenuAccess.Length; i++)
                    {
                        DataRow drMenuAccess = drsMenuAccess[i];
                        menItem = mainMenu.Items.FindByName(drMenuAccess["FrameName"].ToString()) as DevExpress.Web.MenuItem;
                        if (menItem != null)
                        {
                            subItem = menItem.Items.FindByName(drMenuAccess["ComponentName"].ToString()) as DevExpress.Web.MenuItem;
                            if (subItem != null)
                            {
                                subItem.ClientEnabled = false;
                                subItem.ClientVisible = false;
                            }
                        }
                    }
                }
                dtUserComponentAccess = GetLoginUserComponentAccess(loginUser.UserName);
            }

            if (dtUserComponentAccess != null && dtUserComponentAccess.Rows.Count > 0)
            {
                drsMenuAccess = dtUserComponentAccess.Select("FormName = 'MasterPage'");
                if (drsMenuAccess != null && drsMenuAccess.Length > 0)
                {
                    DevExpress.Web.MenuItem menItem, subItem;

                    foreach (DataRow dr in drsMenuAccess)
                    {
                        menItem = mainMenu.Items.FindByName(dr["ComponentName"].ToString()) as DevExpress.Web.MenuItem;
                        if (menItem != null && menItem.Items != null && menItem.Items.Count > 0)
                        {
                            drsComponentFrame = dtUserComponentAccess.Select(string.Format("FormName = 'MasterPage' AND FrameName = '{0}'", dr["ComponentName"]));
                            if (drsComponentFrame != null && drsComponentFrame.Length > 0)
                            {
                                foreach (DevExpress.Web.MenuItem subMItem in menItem.Items)
                                {
                                    subMItem.ClientVisible = false;
                                    subMItem.ClientEnabled = false;
                                }
                            }
                            else
                            {
                                foreach (DevExpress.Web.MenuItem subMItem in menItem.Items)
                                {
                                    subMItem.ClientVisible = true;
                                    subMItem.ClientEnabled = true;
                                }
                            }
                        }
                    }

                    for (int i = 0; i < drsMenuAccess.Length; i++)
                    {
                        DataRow drMenuAccess = drsMenuAccess[i];
                        menItem = mainMenu.Items.FindByName(drMenuAccess["FrameName"].ToString()) as DevExpress.Web.MenuItem;
                        if (menItem != null)
                        {
                            subItem = menItem.Items.FindByName(drMenuAccess["ComponentName"].ToString()) as DevExpress.Web.MenuItem;
                            if (subItem == null)
                            {
                                string url = string.Empty;
                                if (!(drMenuAccess["Url"] is DBNull) && drMenuAccess["Url"].ToString() != string.Empty)
                                {
                                    url += drMenuAccess["Url"];
                                    if (!(drMenuAccess["UrlParam"] is DBNull) && drMenuAccess["UrlParam"].ToString() != string.Empty)
                                    {
                                        if (drMenuAccess["UrlParam"].ToString() == "loginId")
                                            url += "?qs=" + GetQSEncripted(String.Format("{0}={1}", drMenuAccess["UrlParam"], loginUser.UserName));
                                    }
                                }
                                else
                                    if (drMenuAccess["ComponentName"].ToString() == "itmLabelDesigner")
                                        url += loginUser.LabelDesignerLink;

                                subItem = new DevExpress.Web.MenuItem(drMenuAccess["Caption"].ToString(),
                                    drMenuAccess["ComponentName"].ToString(), string.Empty, url);
                                subItem.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                                menItem.Items.Add(subItem);
                            }
                            subItem.ClientEnabled = Convert.ToChar(drMenuAccess["Enabled"]) == 'Y';
                            subItem.ClientVisible = Convert.ToChar(drMenuAccess["Visible"]) == 'Y';
                        }
                    }
                }
            }
            if (mItemSetup.Items.Count == 0)
            {
                if (loginUser.UserName.ToLower() == "sfaccount")
                {
                    DevExpress.Web.MenuItem mItemScanReport = new DevExpress.Web.MenuItem("Barcode Scan Summary Report", "mItemScanReport", string.Empty, "~/BarcodeScanSummaryRptParam.aspx?qs=" +
                    GetQSEncripted(String.Format("loginId={0}", loginUser.UserName)));
                    mItemScanReport.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                    mItemReport.Items.Add(mItemScanReport);
                }
                else
                {
                    DevExpress.Web.MenuItem itmUser = new DevExpress.Web.MenuItem("User Setup", "itmUser", string.Empty, "~/UserSetup.aspx?qs=" +
                        GetQSEncripted(String.Format("loginId={0}", loginUser.UserName)));
                    itmUser.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                    mItemSetup.Items.Add(itmUser);
                }
            }

            ECWebClass.SetMenuStyle(mainMenu);

            foreach (DevExpress.Web.MenuItem mItem in mainMenu.Items)
            {
                if (mItem.Name != "mItemHome" && mItem.Name != "mItemHelp" && mItem.Name != "mItemLogout")
                {
                    mItem.ClientVisible = GetMenuItemVisibility(mItem);
                    // check sub menu item
                    if (mItem.ClientVisible && mItem.Items != null && mItem.Items.Count > 0)
                    {
                        foreach (DevExpress.Web.MenuItem subMItem in mItem.Items)
                        {
                            if (subMItem.Items != null && subMItem.Items.Count > 0)
                            {
                                bool subItemVisible = false;
                                foreach (DevExpress.Web.MenuItem subItem in subMItem.Items)
                                {
                                    subItemVisible = subItem.ClientVisible;
                                    if (subItemVisible)
                                        break;
                                }
                                if (subItemVisible)
                                {
                                    if (!subMItem.ClientVisible)
                                    {
                                        subMItem.ClientVisible = true;
                                        subMItem.ClientEnabled = true;
                                    }
                                }
                                else
                                    if (subMItem.ClientVisible)
                                    {
                                        foreach (DevExpress.Web.MenuItem subItem in subMItem.Items)
                                        {
                                            subItem.ClientVisible = true;
                                            subItem.ClientEnabled = true;
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }

        private static bool GetMenuItemVisibility(DevExpress.Web.MenuItem mItem)
        {
            bool result = mItem.Items.Count > 0;
            if (result)
            {
                foreach (DevExpress.Web.MenuItem m_Item in mItem.Items)
                {
                    result = m_Item.ClientVisible;
                    if (result)
                        break;
                }
            }
            return result;
        }

        private static DataTable GetLoginUserComponentAccess(object loginId)
        {
            const string sqlstr =
                @"SELECT distinct FormName,FrameName,ComponentName,Url,UrlParam,Caption,[Enabled],Visible FROM  SecurityLogin SL 
                    INNER JOIN SecurityLoginRole SLR ON SLR.SecurityLogin=SL.ID 
                    INNER JOIN SecurityRoleFunction SRF ON SRF.SecurityRole=SLR.SecurityRole 
                    INNER JOIN SecurityFunction SF ON SF.ID=SRF.SecurityFunction and SF.Active='Y'
                    INNER JOIN SecurityFunctionCategory SFC ON SFC.ID=SF.Category and SFC.Active='Y'
                    INNER JOIN SecurityComponent SCOMP ON SCOMP.SecurityFunction=SF.ID 
                WHERE SL.LoginId=@login or (@login is null) ORDER BY FormName, FrameName, Caption ";
            DataTable dt = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (loginId != null && loginId.ToString() != string.Empty)
                        sqlCommand.Parameters.AddWithValue("@login", loginId);
                    else
                        sqlCommand.Parameters.AddWithValue("@login", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dt);
                    }
                }
            }
            return dt;
        }
        public static DataRow GetLoginUserComponentAccess(object loginId, object pageId, object componentName)
        {
            DataRow drResult = null;
            DataTable dt = GetLoginUserComponentAccess(loginId);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow[] drs = dt.Select(string.Format("FormName = '{0}' and ComponentName = '{1}'", pageId, componentName));
                if (drs != null && drs.Length > 0)
                    drResult = drs[0];
            }
            return drResult;
        }

        public static DataTable GetLoginCatalogueDataTableByRO(object cdId)
        {
            string sqlstr = @"select SL.CDID, RO.Name [ROName],ISNULL(RO.Name + ' (' + RO.Code + ')', 'N/A') [ReturningOffice],  
                SL.FirstName + ' ' + SL.FamilyName [Contact], SL.* 
                from SecurityLogin SL left join NSW_ReturningOffice RO ON RO.Code=SL.CDID where SL.CDID=@CdId or @CdId is null";

            DataTable dtLogin = new DataTable();
            dtLogin.TableName = "ROSecurityLogin";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (cdId != null && cdId.ToString() != string.Empty)
                        sqlCommand.Parameters.AddWithValue("@CdId", cdId);
                    else
                        sqlCommand.Parameters.AddWithValue("@CdId", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLogin);
                    }
                }
            }
            return dtLogin;
        }
        public static DataTable GetLoginCatalogueDataTable(object loginId)
        {
            string sqlstr = @"select SL.CDID, RO.Name [ROName],ISNULL(RO.Name + ' (' + RO.Code + ')', 'N/A') [ReturningOffice], SL.FirstName + ' ' + SL.FamilyName [Contact],  SL.* 
                from SecurityLogin SL left join NSW_ReturningOffice RO ON RO.Code=SL.CDID where SL.LoginId=@LoginId or @LoginId is null";

            DataTable dtLogin = new DataTable();
            dtLogin.TableName = "ROSecurityLogin";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (loginId != null && loginId.ToString() != string.Empty)
                        sqlCommand.Parameters.AddWithValue("@LoginId", loginId);
                    else
                        sqlCommand.Parameters.AddWithValue("@LoginId", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLogin);
                    }
                }
            }
            return dtLogin;
        }
        public static DataTable GetSecurityFunctionCategoryDataTable()
        {
            string sqlstr = @"select * from SecurityFunctionCategory";

            DataTable dtFuncCat = new DataTable();
            dtFuncCat.TableName = "SecurityFunctionCategory";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtFuncCat);
                    }
                }
            }
            return dtFuncCat;
        }
        public static DataTable GetSecurityCategoryFunctionDataTable()
        {
            string sqlstr = @"select SF.*, SFC.Name [CategoryName] from SecurityFunction SF 
                inner join SecurityFunctionCategory SFC ON SFC.ID=SF.Category where SF.Active='Y' ";

            DataTable dtFunc = new DataTable();
            dtFunc.TableName = "SecurityFunction";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtFunc);
                    }
                }
            }
            return dtFunc;
        }
        public static DataTable GetSecurityFunctionDataTable(object funcId)
        {
            string sqlstr = "select * from SecurityFunction where ID=@ID or @ID is null  ";
            DataTable dtFunc = new DataTable();
            dtFunc.TableName = "SecurityFunction";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (funcId != null)
                        sqlCommand.Parameters.AddWithValue("@ID", funcId);
                    else
                        sqlCommand.Parameters.AddWithValue("@ID", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtFunc);
                    }
                }
            }
            return dtFunc;
        }

        public static DataTable GetLoginDataTableById(object loginId)
        {
            string sqlstr = "select * from SecurityLogin where LoginId=@LoginId or @LoginId is null ";
            DataTable dtLogin = new DataTable();
            dtLogin.TableName = "SecurityLogin";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (loginId != null)
                        sqlCommand.Parameters.AddWithValue("@LoginId", loginId);
                    else
                        sqlCommand.Parameters.AddWithValue("@LoginId", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLogin);
                    }
                }
            }
            return dtLogin;
        }
        /* */
        public static DataTable GetLoginDataTableById(object id, object loginId)
        {
            string sqlstr = "select * from SecurityLogin where LoginId=@LoginId and ID<>@ID";
            DataTable dtLogin = new DataTable();
            dtLogin.TableName = "SecurityLogin";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@LoginId", loginId);
                    sqlCommand.Parameters.AddWithValue("@ID", id);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLogin);
                    }
                }
            }
            return dtLogin;
        }
       
        public static bool UpdateFunctionCategory(ref string message, DataTable dtFuncCat, DataRow drFuncCat, bool isDelete)
        {
            bool result = false;
            string sqlStr = string.Empty;
            SqlCommand cmd = null;
            using (SqlConnection sqlConn = new SqlConnection(connStr))
            {
                try
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        int funcCatId = Convert.ToInt32(drFuncCat["ID"]);
                        if (funcCatId == -1)// new
                        {
                            sqlStr = ECWebClass.GetInsertDataSetSQLString(dtFuncCat.TableName, dtFuncCat);
                            sqlStr += "; SELECT CAST(scope_identity() AS int)";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                foreach (DataColumn col in dtFuncCat.Columns)
                                {
                                    if (col.ColumnName != "ID")
                                        cmd.Parameters.AddWithValue("@" + col.ColumnName, drFuncCat[col.ColumnName]);
                                }
                                // insert new record and get the new identity
                                result = (Int32)cmd.ExecuteScalar() > -1;// new id returned
                            }
                        }
                        else// edit/delete
                        {
                            sqlStr = "SELECT * FROM SecurityFunctionCategory WHERE ID=@ID";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ID", funcCatId);

                                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                {
                                    sqlDataAdapter.SelectCommand = cmd;
                                    using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                    {
                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                        DataTable dtFuncCatData = new DataTable();
                                        sqlDataAdapter.Fill(dtFuncCatData);
                                        if (dtFuncCatData.Rows.Count > 0)
                                        {
                                            DataRow drFuncCatData = dtFuncCatData.Rows[0];
                                            if (!isDelete)
                                            {
                                                foreach (DataColumn col in dtFuncCat.Columns)
                                                {
                                                    if (col.ColumnName != "ID")
                                                        drFuncCatData[col.ColumnName] = drFuncCat[col.ColumnName];
                                                }
                                            }
                                            else
                                                drFuncCatData.Delete();
                                            result = sqlDataAdapter.Update(dtFuncCatData) > 0;
                                        }
                                    }
                                }
                            }
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        message = ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            } // end of using

            return result;
        }

        public static bool UpdateSecurityFunction(ref string message, DataTable dtFunc, DataRow drFunc, DataTable dtSecComp, bool isDelete)
        {
            bool result = false;
            string sqlStr = string.Empty;
            SqlCommand cmd = null;
            using (SqlConnection sqlConn = new SqlConnection(connStr))
            {
                try
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        int funcId = Convert.ToInt32(drFunc["ID"]);
                        if (funcId == -1)// new
                        {
                            sqlStr = ECWebClass.GetInsertDataSetSQLString(dtFunc.TableName, dtFunc);
                            sqlStr += "; SELECT CAST(scope_identity() AS int)";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                foreach (DataColumn col in dtFunc.Columns)
                                {
                                    if (col.ColumnName != "ID")
                                        cmd.Parameters.AddWithValue("@" + col.ColumnName, drFunc[col.ColumnName]);
                                }
                                // insert new record and get the new identity
                                funcId = (Int32)cmd.ExecuteScalar();
                                result = funcId > 0;// new id returned
                            }
                        }
                        else// edit/delete
                        {
                            sqlStr = "SELECT * FROM SecurityFunction WHERE ID=@ID";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ID", funcId);

                                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                {
                                    sqlDataAdapter.SelectCommand = cmd;
                                    using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                    {
                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                        DataTable dtFuncData = new DataTable();
                                        sqlDataAdapter.Fill(dtFuncData);
                                        if (dtFuncData.Rows.Count > 0)
                                        {
                                            DataRow drFuncData = dtFuncData.Rows[0];
                                            if (!isDelete)
                                            {
                                                foreach (DataColumn col in dtFunc.Columns)
                                                {
                                                    if (col.ColumnName != "ID")
                                                        drFuncData[col.ColumnName] = drFunc[col.ColumnName];
                                                }
                                            }
                                            else
                                                drFuncData.Delete();
                                            result = sqlDataAdapter.Update(dtFuncData) > 0;
                                        }
                                    }
                                }
                            }
                        }
                        if (result && !isDelete && dtSecComp != null && dtSecComp.Rows.Count > 0)
                        {
                            foreach (DataRow drSecComp in dtSecComp.Rows)
                            {
                                result = UpdateSecurityComponent(sqlTrans, dtSecComp, drSecComp, funcId, ref message);
                                if (!result)
                                    break;
                            }
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        message = ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            } // end of using

            return result;
        }

        private static bool UpdateSecurityComponent(SqlTransaction sqlTrans, DataTable dtSecComp, DataRow drSecComp, int funcId, ref string message)
        {
            // FromDB: 0/1  Flag: -1/0/1/2
            bool fromDB = Convert.ToInt16(drSecComp["FromDB"]) == 1;
            int flag = Convert.ToInt16(drSecComp["Flag"]);

            bool result = (fromDB && flag == 0) || (!fromDB && flag < 0);

            if (!result)
            {
                string sqlStr = string.Empty;
                SqlCommand cmd = null;

                if (fromDB)
                {
                    switch (flag)
                    {
                        case -1: // delete from database
                        case 1: // modified
                            try
                            {
                                sqlStr = "SELECT * FROM SecurityComponent WHERE ID=@ID";
                                using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                {
                                    cmd.Parameters.AddWithValue("@ID", drSecComp["ID"]);

                                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                    {
                                        sqlDataAdapter.SelectCommand = cmd;
                                        using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                        {
                                            sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                            sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                            sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                            sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                            DataTable dtSecCompData = new DataTable();
                                            sqlDataAdapter.Fill(dtSecCompData);
                                            if (dtSecCompData.Rows.Count > 0)
                                            {
                                                DataRow drSecCompData = dtSecCompData.Rows[0];
                                                if (flag == -1)
                                                    drSecCompData.Delete();
                                                else
                                                {
                                                    foreach (DataColumn col in dtSecComp.Columns)
                                                    {
                                                        if (col.ColumnName != "ID" && col.ColumnName != "Flag" && col.ColumnName != "FromDB")
                                                            drSecCompData[col.ColumnName] = drSecComp[col.ColumnName];
                                                    }
                                                }
                                                result = sqlDataAdapter.Update(dtSecCompData) > 0;
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                message = ex.ToString();
                            }
                            break;
                        default:
                            result = true;
                            break;
                    }
                }
                else
                    if (flag > 0) // new 
                    {
                        try
                        {
                            int compId = -1;
                            sqlStr = ECWebClass.GetInsertDataSetSQLString(dtSecComp.TableName, dtSecComp);
                            sqlStr += "; SELECT CAST(scope_identity() AS int)";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                foreach (DataColumn col in dtSecComp.Columns)
                                {
                                    if (col.ColumnName != "ID" && col.ColumnName != "Flag" && col.ColumnName != "FromDB")
                                    {
                                        if (col.ColumnName != "SecurityFunction")
                                            cmd.Parameters.AddWithValue("@" + col.ColumnName, drSecComp[col.ColumnName]);
                                        else
                                            cmd.Parameters.AddWithValue("@" + col.ColumnName, funcId);
                                    }
                                }
                                compId = (Int32)cmd.ExecuteScalar();
                                result = compId > 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            message = ex.ToString();
                        }
                    }
            }

            return result;
        }

        public static DataTable GetSecurityComponentDataTable(object funcId)
        {
            string sqlstr = "select D.*, 0 [Flag], 1 [FromDB] from SecurityComponent D where D.SecurityFunction=@SecurityFunction order by D.FormName,D.FrameName,D.ComponentName ";
            DataTable dt = new DataTable();
            dt.TableName = "SecurityComponent";
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@SecurityFunction", funcId);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dt);
                    }
                }
            }
            return dt;
        }

        public static DataTable GetSecurityRoleDataTable(object roleId)
        {
            string sqlstr = "select * from SecurityRole where ID=@ID or @ID is null";
            DataTable dtRole = new DataTable();
            dtRole.TableName = "SecurityRole";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (roleId != null)
                        sqlCommand.Parameters.AddWithValue("@ID", roleId);
                    else
                        sqlCommand.Parameters.AddWithValue("@ID", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtRole);
                    }
                }
            }
            return dtRole;
        }

        public static DataTable GetSecurityRoleFunctionDataTable(object roleId)
        {
            string sqlstr = "select * from SecurityRoleFunction where SecurityRole=@SecurityRole or @SecurityRole is null";

            DataTable dtRoleFunc = new DataTable();
            dtRoleFunc.TableName = "SecurityRoleFunction";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (roleId != null)
                        sqlCommand.Parameters.AddWithValue("@SecurityRole", roleId);
                    else
                        sqlCommand.Parameters.AddWithValue("@SecurityRole", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtRoleFunc);
                    }
                }
            }
            return dtRoleFunc;
        }

        public static DataTable GetSecurityLoginRoleDataTable(object idLogin)
        {
            string sqlstr = "select * from SecurityLoginRole where SecurityLogin=@SecurityLogin or @SecurityLogin is null";
            DataTable dtLoginRole = new DataTable();
            dtLoginRole.TableName = "SecurityLoginRole";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (idLogin != null)
                        sqlCommand.Parameters.AddWithValue("@SecurityLogin", idLogin);
                    else
                        sqlCommand.Parameters.AddWithValue("@SecurityLogin", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLoginRole);
                    }
                }
            }
            return dtLoginRole;
        }

        public static DataTable GetSecurityDeviceData(object devId)
        {
            DataTable dt = new DataTable();
            string sqlstr = "select * from SecurityDevice where DeviceId=@DeviceId or @DeviceId is null";
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(sqlstr, sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    if (devId != null)
                        sqlCommand.Parameters.AddWithValue("@DeviceId", devId);
                    else
                        sqlCommand.Parameters.AddWithValue("@DeviceId", DBNull.Value);
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static bool UpdateSecurityRole(System.Web.UI.Page page, ref string message, ref int roleId, DataTable dtRole, string funcList, bool isDelete)
        {
            bool result = false;
            string sqlStr = string.Empty;
            SqlCommand cmd = null;
            using (SqlConnection sqlConn = new SqlConnection(connStr))
            {
                try
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        DataRow drRole = dtRole.Rows[0];
                        roleId = Convert.ToInt32(drRole["ID"]);

                        if (roleId == -1)// new
                        {
                            sqlStr = ECWebClass.GetInsertDataSetSQLString(dtRole.TableName, dtRole);
                            sqlStr += "; SELECT CAST(scope_identity() AS int)";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                foreach (DataColumn col in dtRole.Columns)
                                {
                                    if (col.ColumnName != "ID")
                                        cmd.Parameters.AddWithValue("@" + col.ColumnName, drRole[col.ColumnName]);
                                }
                                // insert new record and get the new identity
                                roleId = (Int32)cmd.ExecuteScalar();
                                result = roleId > 0;// new id returned
                            }
                        }
                        else// edit/delete
                        {
                            sqlStr = String.Format("SELECT * FROM {0} WHERE ID=@ID", dtRole.TableName);
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ID", roleId);

                                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                                sqlDataAdapter.SelectCommand = cmd;
                                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                DataTable dtRoleData = new DataTable();
                                sqlDataAdapter.Fill(dtRoleData);
                                if (dtRoleData.Rows.Count > 0)
                                {
                                    DataRow drRoleData = dtRoleData.Rows[0];
                                    if (!isDelete)
                                    {
                                        foreach (DataColumn col in dtRole.Columns)
                                        {
                                            if (col.ColumnName != "ID")
                                                drRoleData[col.ColumnName] = drRole[col.ColumnName];
                                        }
                                    }
                                    else
                                        drRoleData.Delete();
                                    result = sqlDataAdapter.Update(dtRoleData) > 0;
                                }
                            }
                        }
                        if (result && !isDelete)
                        {
                            sqlStr = "delete from SecurityRoleFunction where SecurityRole=@SecurityRole";
                            using (SqlCommand cmdDelete = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmdDelete.Parameters.AddWithValue("@SecurityRole", roleId);
                                result = cmdDelete.ExecuteNonQuery() >= 0;
                            }
                            if (result && funcList != null && funcList != string.Empty)
                            {
                                sqlStr = @"insert into SecurityRoleFunction (SecurityRole,SecurityFunction,Created,CreatedBy) 
                                values (@SecurityRole,@SecurityFunction,getdate(),@CreatedBy) ";
                                string[] funcArray = funcList.Split(',');
                                for (int i = 0; i < funcArray.Length; i++)
                                {
                                    using (SqlCommand cmdInsert = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                    {
                                        cmdInsert.Parameters.AddWithValue("@SecurityRole", roleId);
                                        cmdInsert.Parameters.AddWithValue("@SecurityFunction", funcArray[i]);
                                        cmdInsert.Parameters.AddWithValue("@CreatedBy", (page.Session["EC_User"] as LoginUser).UserName);
                                        result = cmdInsert.ExecuteNonQuery() > 0;
                                        if (!result)
                                            break;
                                    }
                                }
                            }
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        message = ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            } // end of using

            return result;
        }

        public static bool UpdateLoginUser(System.Web.UI.Page page, ref string message, DataTable dtLogin, string roleList, bool isDelete)
        {
            bool result = false;
            string sqlStr = string.Empty;
            SqlCommand cmd = null;
            using (SqlConnection sqlConn = new SqlConnection(connStr))
            {
                try
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        DataRow drLogin = dtLogin.Rows[0];
                        int id = Convert.ToInt32(drLogin["ID"]);
                        bool newLogin = id == -1;

                        if (newLogin)// new
                        {
                            sqlStr = ECWebClass.GetInsertDataSetSQLString(dtLogin.TableName, dtLogin);
                            sqlStr += "; SELECT CAST(scope_identity() AS int)";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                foreach (DataColumn col in dtLogin.Columns)
                                {
                                    if (col.ColumnName != "ID")
                                        cmd.Parameters.AddWithValue("@" + col.ColumnName, drLogin[col.ColumnName]);
                                }
                                // insert new record and get the new identity
                                id = (Int32)cmd.ExecuteScalar();
                                result = id > -1;// new id returned
                            }
                        }
                        else// edit/delete
                        {
                            sqlStr = String.Format("SELECT * FROM {0} WHERE ID=@ID", dtLogin.TableName);
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ID", id);

                                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                {
                                    sqlDataAdapter.SelectCommand = cmd;
                                    using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                    {
                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                        DataTable dtLoginData = new DataTable();
                                        sqlDataAdapter.Fill(dtLoginData);
                                        if (dtLoginData.Rows.Count > 0)
                                        {
                                            DataRow drLoginData = dtLoginData.Rows[0];
                                            if (!isDelete)
                                            {
                                                foreach (DataColumn col in dtLogin.Columns)
                                                {
                                                    if (col.ColumnName != "ID")
                                                        drLoginData[col.ColumnName] = drLogin[col.ColumnName];
                                                }
                                            }
                                            else
                                                drLoginData.Delete();
                                            result = sqlDataAdapter.Update(dtLoginData) > 0;
                                        }
                                    }
                                }
                            }
                        }
                        if (result && roleList != null && roleList != string.Empty)
                        {
                            sqlStr = "delete from SecurityLoginRole where SecurityLogin=@SecurityLogin";
                            using (SqlCommand cmdDelete = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmdDelete.Parameters.AddWithValue("@SecurityLogin", id);
                                result = cmdDelete.ExecuteNonQuery() >= 0;
                            }
                            if (result)
                            {
                                sqlStr = @"insert into SecurityLoginRole (SecurityLogin,SecurityRole,Created,CreatedBy) 
                                values (@SecurityLogin,@SecurityRole,getdate(),@CreatedBy) ";
                                string[] roleArray = roleList.Split(',');
                                for (int i = 0; i < roleArray.Length; i++)
                                {
                                    using (SqlCommand cmdInsert = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                    {
                                        cmdInsert.Parameters.AddWithValue("@SecurityLogin", id);
                                        cmdInsert.Parameters.AddWithValue("@SecurityRole", roleArray[i]);
                                        cmdInsert.Parameters.AddWithValue("@CreatedBy", (page.Session["EC_User"] as LoginUser).UserName);
                                        result = cmdInsert.ExecuteNonQuery() > 0;
                                        if (!result)
                                            break;
                                    }
                                }
                            }
                        }
                        if (result)
                        {
                            sqlTrans.Commit();
                        }
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        message = ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            } // end of using

            return result;
        }

        public static bool loginIsAdmin(string loginName)
        {
            bool result = false;
            string sqlStr = @"select SR.Name from SecurityLogin SL
                inner join SecurityLoginRole SLR on SLR.SecurityLogin=sl.ID
                inner join SecurityRole SR ON SR.ID =SLR.SecurityRole 
                where LoginId=@LoginId";
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(sqlStr, sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    sqlCommand.Parameters.AddWithValue("@LoginId", loginName);
                    adapter.SelectCommand = sqlCommand;
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    result = dt.Rows.Count > 0 && !(dt.Rows[0][0] is DBNull) && dt.Rows[0][0].ToString().StartsWith("Admin");
                }
            }
            return result;
        }
        public static bool RetrieveLoginPassword(System.Web.UI.Page page, object loginUserName, ref string message)
        {
            return false;
        }
        public static string GetQSEncripted(string qryStr)
        {
            // Base64 encoded strings may contain the characters a-z A-Z 0-9 + / =.
            string result = string.Empty;

            CryptographyRijndael _key = new CryptographyRijndael(_urlPassPhrase, _initVector);
            result = _key.Encrypt(qryStr);

            result = result.TrimEnd(padding).Replace('+', '-').Replace('/', '_');
            /*
            while (result.Contains("+"))
                result = _key.Encrypt(qryStr);
             */
            return result;
        }
        public static string GetQSDecripted(string encriptedQryStr)
        {
            string result = encriptedQryStr;
            result = result.Replace('_', '/').Replace('-', '+');

            switch (result.Length % 4)
            {
                case 2: result += "==";
                    break;
                case 3: result += "=";
                    break;
                default:
                    break;
            }

            CryptographyRijndael _key = new CryptographyRijndael(_urlPassPhrase, _initVector);
            result = _key.Decrypt(result);
            return result;
        }
        public static string GetPValueDecripted(string pValue)
        {
            CryptographyRijndael _key = new CryptographyRijndael(_passPhrase, _initVector);
            return _key.Decrypt(pValue);
        }
        public static string GetPValueEncripted(string pValue)
        {
            CryptographyRijndael _key = new CryptographyRijndael(_passPhrase, _initVector);
            return _key.Encrypt(pValue);
        }
        public static bool CheckQueryString(System.Web.UI.Page page, string paramName)
        {
            bool result = page.Request.QueryString.Count > 0;
            if (result)
            {
                try
                {
                    string[] qs = GetQSDecripted(page.Request.QueryString["qs"]).Split('=');

                    result = qs.Length > 0;
                    if (result)
                    {
                        foreach (string param in qs)
                        {
                            result = param == paramName;
                            if (result)
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    string err = ex.Message;
                }
            }
            return result;
        }

        public static bool CheckQueryString(System.Web.UI.Page page, string paramName, ref string paramValue)
        {
            bool result = page.Request.QueryString.Count > 0;
            if (result)
            {
                try
                {
                    string[] qs = GetQSDecripted(page.Request.QueryString["qs"]).Split('=');

                    result = qs.Length == 2;
                    if (result)
                    {
                        result = qs[0] == paramName;
                        if (result)
                        {
                            paramValue = qs[1];
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    string err = ex.Message;
                }
            }
            return result;
        }

        public static bool CheckShowCaptchaQueryString(string sq, ref string loginUser, ref string attCount)
        {
            bool result = !string.IsNullOrEmpty(sq);
            if (result)
            {
                try
                {
                    string[] qs = GetQSDecripted(sq).Split('|');

                    result = qs.Length == 2;
                    if (result)
                    {
                        loginUser = qs[0];
                        attCount = qs[1];
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    string err = ex.Message;
                }
            }
            return result;
        }

        public static bool LoginUserLocked(string userName)
        {
            return false;
            bool result = string.IsNullOrEmpty(userName);
            if (!result)
            {
                try
                {
                    DataTable dt = new DataTable();

                    using (SqlConnection sqlConnection = new SqlConnection(connStr))
                    {
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM LoginAttempt WHERE [LoginId]=@LoginId", sqlConnection))
                        {
                            cmd.Parameters.AddWithValue("@LoginId", userName);
                            using (SqlDataAdapter adapter = new SqlDataAdapter())
                            {
                                adapter.SelectCommand = cmd;
                                adapter.Fill(dt);
                            }
                        }
                    }
                    result = dt.Rows.Count > 0 && !(dt.Rows[0]["Expires"] is DBNull) && dt.Rows[0]["Expires"].ToString() != string.Empty;
                    if (result)
                        result = Convert.ToDateTime(dt.Rows[0]["Expires"]) > DateTime.Now;
                }
                catch (Exception ex)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool IsIPAllowed(string usrIpAddress)
        {
            var whiteListedIPs = ConfigurationManager.AppSettings["WhiteListedIPAddresses"];
            bool result = string.IsNullOrEmpty(whiteListedIPs);
            if (!result)
            {
                IPAddress ipAddress;
                result = IPAddress.TryParse(usrIpAddress, out ipAddress);
                if (result)
                {
                    var whiteListIPList = whiteListedIPs.Split(',').ToList();
                    result = whiteListIPList.Where(a => a.Trim().Equals(usrIpAddress, StringComparison.InvariantCultureIgnoreCase)).Any();
                    /*
                    if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        string[] ipParts = usrIpAddress.Split('.');

                        foreach (string banned in _fileContents.Ipv4Masks)
                        {
                            string[] blockedParts = banned.Split('.');
                            if (blockedParts.Length > 4) continue; // Not valid IP mask.

                            if (IsIpBlocked(ipParts, blockedParts))
                            {
                                return true;
                            }
                        }
                    }
                     * */
                }
            }
            return result;
        }

        public static DataTable GetApplicationDataTable()
        {
            string sqlstr = "select top 1 * from NSW_Application ";
            DataTable dtApp = new DataTable();
            dtApp.TableName = "NSW_Application";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtApp);
                    }
                }
            }
            return dtApp;
        }
        public static bool UpdateApplication(ref string message, DataTable dtApp)
        {
            bool result = false;
            string sqlStr = string.Empty;
            DataRow drApp = dtApp.Rows[0];
            SqlCommand cmd = null;
            using (SqlConnection sqlConn = new SqlConnection(connStr))
            {
                try
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        int appId = Convert.ToInt32(drApp["ID"]);
                        if (appId == -1)// new
                        {
                            sqlStr = ECWebClass.GetInsertDataSetSQLString(dtApp.TableName, dtApp);
                            sqlStr += "; SELECT CAST(scope_identity() AS int)";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                foreach (DataColumn col in dtApp.Columns)
                                {
                                    if (col.ColumnName != "ID")
                                        cmd.Parameters.AddWithValue("@" + col.ColumnName, drApp[col.ColumnName]);
                                }
                                // insert new record and get the new identity
                                result = (Int32)cmd.ExecuteScalar() > -1;// new id returned
                            }
                        }
                        else// edit/delete
                        {
                            sqlStr = String.Format("SELECT * FROM {0} WHERE ID=@ID", dtApp.TableName);
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ID", appId);

                                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                {
                                    sqlDataAdapter.SelectCommand = cmd;
                                    using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                    {
                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                        DataTable dtAppData = new DataTable();
                                        sqlDataAdapter.Fill(dtAppData);
                                        if (dtAppData.Rows.Count > 0)
                                        {
                                            DataRow drAppData = dtAppData.Rows[0];
                                            foreach (DataColumn col in dtApp.Columns)
                                            {
                                                if (col.ColumnName != "ID")
                                                    drAppData[col.ColumnName] = drApp[col.ColumnName];
                                            }
                                            result = sqlDataAdapter.Update(dtAppData) > 0;
                                        }
                                    }
                                }
                            }
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        message = ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            } // end of using

            return result;
        }

        public static void setReadOnlyFor(Control ctr, bool readOnly)
        {
            string jsStopBackspaceKey = @"function (s, e) {
                 var key = ASPxClientUtils.GetKeyCode(e.htmlEvent);
                  if (key == ASPxKey.Backspace) 
                      ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
            }";
            Color readOnlyBackColor = System.Drawing.Color.Gainsboro;
            if (ctr is ASPxPageControl)
            {
                ASPxPageControl pControl = ctr as ASPxPageControl;
                foreach (TabPage tabPage in pControl.TabPages)
                {
                    foreach (Control ctrl in tabPage.Controls)
                        setReadOnlyFor(ctrl, readOnly);
                }
            }
            else
                if (ctr is ASPxComboBox)
                {
                    (ctr as ASPxComboBox).ReadOnly = readOnly;
                    (ctr as ASPxComboBox).DropDownButton.Visible = !readOnly;
                    (ctr as ASPxComboBox).ClientSideEvents.DropDown = string.Empty;
                    if ((ctr as ASPxComboBox).ReadOnly)
                    {
                        (ctr as ASPxComboBox).ReadOnlyStyle.BackColor = readOnlyBackColor;
                        (ctr as ASPxComboBox).ClientSideEvents.DropDown = "function(s, e) {s.HideDropDown();}";
                        (ctr as ASPxComboBox).ClientSideEvents.KeyDown = jsStopBackspaceKey;
                    }
                }
                else
                    if (ctr is ASPxTextBox)
                    {
                        (ctr as ASPxTextBox).ReadOnly = readOnly;
                        if ((ctr as ASPxTextBox).ReadOnly)
                        {
                            (ctr as ASPxTextBox).ReadOnlyStyle.BackColor = readOnlyBackColor;
                            (ctr as ASPxTextBox).ClientSideEvents.KeyDown = jsStopBackspaceKey;
                        }
                    }
                    else
                        if (ctr is ASPxMemo)
                        {
                            (ctr as ASPxMemo).ReadOnly = readOnly;
                            if ((ctr as ASPxMemo).ReadOnly)
                            {
                                (ctr as ASPxMemo).ReadOnlyStyle.BackColor = readOnlyBackColor;
                                (ctr as ASPxMemo).ClientSideEvents.KeyDown = jsStopBackspaceKey;
                            }
                        }
                        else
                            if (ctr is ASPxDateEdit)
                            {
                                (ctr as ASPxDateEdit).ReadOnly = readOnly;
                                (ctr as ASPxDateEdit).DropDownButton.Visible = !readOnly;
                                if ((ctr as ASPxDateEdit).ReadOnly)
                                {
                                    (ctr as ASPxDateEdit).ReadOnlyStyle.BackColor = readOnlyBackColor;
                                    (ctr as ASPxDateEdit).ClientSideEvents.KeyDown = jsStopBackspaceKey;
                                }
                            }
                            else
                                if (ctr is ASPxCheckBox)
                                    (ctr as ASPxCheckBox).ReadOnly = readOnly;
                                else
                                    if (ctr is ASPxSpinEdit)
                                    {
                                        (ctr as ASPxSpinEdit).ReadOnly = readOnly;
                                        (ctr as ASPxSpinEdit).SpinButtons.ShowIncrementButtons = !readOnly;
                                        if ((ctr as ASPxSpinEdit).SpinButtons.ShowLargeIncrementButtons)
                                            (ctr as ASPxSpinEdit).SpinButtons.ShowLargeIncrementButtons = !readOnly;
                                        if ((ctr as ASPxSpinEdit).ReadOnly)
                                        {
                                            (ctr as ASPxSpinEdit).ReadOnlyStyle.BackColor = readOnlyBackColor;
                                            (ctr as ASPxSpinEdit).ClientSideEvents.KeyDown = jsStopBackspaceKey;
                                        }
                                    }
                                    else
                                        if (ctr is ASPxDropDownEdit)
                                        {
                                            (ctr as ASPxDropDownEdit).ReadOnly = readOnly;
                                            (ctr as ASPxDropDownEdit).DropDownButton.Visible = !readOnly;
                                            if ((ctr as ASPxDropDownEdit).ReadOnly)
                                            {
                                                (ctr as ASPxDropDownEdit).ReadOnlyStyle.BackColor = readOnlyBackColor;
                                                (ctr as ASPxDropDownEdit).ClientSideEvents.KeyDown = jsStopBackspaceKey;
                                            }
                                        }
                                        else
                                            if (ctr is ASPxHtmlEditor)
                                            {
                                                ASPxHtmlEditor htmlEditor = (ctr as ASPxHtmlEditor);

                                                htmlEditor.Settings.AllowDesignView = !readOnly;
                                                htmlEditor.Settings.AllowHtmlView = !readOnly;
                                                htmlEditor.Settings.AllowContextMenu = DevExpress.Utils.DefaultBoolean.True;// = DefaultBoolean.True;
                                                if (htmlEditor.Settings.AllowDesignView)
                                                    htmlEditor.ActiveView = HtmlEditorView.Design;

                                                htmlEditor.Settings.AllowPreview = true;
                                                htmlEditor.Settings.AllowInsertDirectImageUrls = false;

                                                if (readOnly)
                                                {
                                                    htmlEditor.Settings.AllowContextMenu = DevExpress.Utils.DefaultBoolean.False; //DefaultBoolean.False;
                                                    htmlEditor.ActiveView = HtmlEditorView.Preview;
                                                    htmlEditor.Styles.ViewArea.BackColor = readOnlyBackColor;
                                                    htmlEditor.ClientSideEvents.Init = "";
                                                }
                                            }
                                            else
                                                if (ctr is ASPxRadioButtonList)
                                                {
                                                    (ctr as ASPxRadioButtonList).ReadOnly = readOnly;
                                                    /*
                                                    (ctr as ASPxRadioButtonList).Enabled = !readOnly;
                                                    if (readOnly)
                                                        (ctr as ASPxRadioButtonList).ReadOnlyStyle.BackColor = readOnlyBackColor; 
                                                     */
                                                }
                                                else
                                                    if (ctr is ASPxButtonEdit)
                                                    {
                                                        (ctr as ASPxButtonEdit).ReadOnly = true;
                                                        foreach (EditButton btnEdit in (ctr as ASPxButtonEdit).Buttons)
                                                            btnEdit.Visible = !readOnly;
                                                        if (readOnly)
                                                        {
                                                            (ctr as ASPxButtonEdit).ReadOnlyStyle.BackColor = readOnlyBackColor;
                                                            (ctr as ASPxButtonEdit).ClientSideEvents.KeyDown = jsStopBackspaceKey;
                                                        }
                                                    }
                                                    else
                                                        if (ctr.HasControls())
                                                            setReadOnlyForChildControls(ctr, readOnly);
        }
        private static void setReadOnlyForChildControls(Control parentControl, bool readOnly)
        {
            foreach (Control childControl in parentControl.Controls)
                setReadOnlyFor(childControl, readOnly);
        }

        public static int SecurityLoginAttempt(string attemptUser, string attemptIP, bool loginSuccess, ref string message)
        {
            int attCount = 0;
            try
            {
                int maxLoginAttempts = MaxLoginAttempts();
                if (maxLoginAttempts == 0)
                    return attCount;
                int lockDuration = LockDuration();
                string sqlStr = string.Empty;
                SqlCommand cmd = null;
                bool success = false;
                DateTime dtNow = DateTime.Now;

                if (loginSuccess)
                {
                    using (SqlConnection sqlConn = new SqlConnection(connStr))
                    {
                        sqlConn.Open();
                        SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                        try
                        {
                            sqlStr = "DELETE FROM LoginAttempt WHERE ([Expires] IS NOT NULL AND [Expires]<@Now) or ([LastAttempt] IS NOT NULL AND [LastAttempt]<@Now1)";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@Now", dtNow);
                                cmd.Parameters.AddWithValue("@Now1", dtNow.AddMinutes(-lockDuration));
                                success = cmd.ExecuteNonQuery() > -1;
                            }
                            if (success)
                            {
                                sqlStr = "DELETE FROM LoginAttempt WHERE LoginId=@LoginId";
                                using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                {
                                    cmd.Parameters.AddWithValue("@LoginId", attemptUser);
                                    success = cmd.ExecuteNonQuery() > -1;
                                }
                            }
                            if (success)
                            {
                                attCount = 0;
                                sqlTrans.Commit();
                            }
                            else
                                sqlTrans.Rollback();
                        }
                        catch (SqlException ex)
                        {
                            sqlTrans.Rollback();
                            message = ex.ToString();
                        }
                    }
                }
                else
                {
                    using (SqlConnection sqlConn = new SqlConnection(connStr))
                    {
                        sqlConn.Open();
                        SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                        try
                        {
                            sqlStr = "DELETE FROM LoginAttempt WHERE ([Expires] IS NOT NULL AND [Expires]<@Now) or ([LastAttempt] IS NOT NULL AND [LastAttempt]<@Now1)";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@Now", dtNow);
                                cmd.Parameters.AddWithValue("@Now1", dtNow.AddMinutes(-lockDuration));
                                success = cmd.ExecuteNonQuery() > -1;
                            }
                            if (success)
                            {
                                sqlStr = "SELECT * FROM LoginAttempt WHERE LoginId=@LoginId";
                                using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                {
                                    cmd.Parameters.AddWithValue("@LoginId", attemptUser);

                                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                    {
                                        sqlDataAdapter.SelectCommand = cmd;
                                        using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                        {
                                            sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                            sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                            sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                            sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                            DataTable dtData = new DataTable();
                                            sqlDataAdapter.Fill(dtData);

                                            DataRow drData;
                                            if (dtData.Rows.Count > 0)
                                            {
                                                drData = dtData.Rows[0];

                                                drData["LastAttempt"] = dtNow;

                                                if (!(drData["Expires"] is DBNull) && drData["Expires"].ToString() != string.Empty && dtNow > Convert.ToDateTime(drData["Expires"]))
                                                {
                                                    attCount++;
                                                    drData["Attempt"] = attCount;
                                                    drData["Expires"] = DBNull.Value; // unlock
                                                }
                                                else
                                                {
                                                    attCount = ECWebClass.StrToIntDef(string.Format("{0}", drData["Attempt"]), 0);
                                                    if (attCount < maxLoginAttempts)
                                                        attCount++;
                                                    drData["Attempt"] = attCount;
                                                    if (attCount == maxLoginAttempts && drData["Expires"] is DBNull)
                                                        drData["Expires"] = DateTime.Now.AddMinutes(lockDuration); // lock
                                                }
                                            }
                                            else
                                            {
                                                // new failed attempt
                                                drData = dtData.NewRow();
                                                attCount++;
                                                drData["Attempt"] = attCount;
                                                drData["LoginId"] = attemptUser;
                                                drData["IP"] = attemptIP;
                                                drData["LastAttempt"] = dtNow;
                                                dtData.Rows.Add(drData);
                                            }
                                            success = sqlDataAdapter.Update(dtData) > 0;
                                        }
                                    }
                                }
                            }
                            if (success)
                                sqlTrans.Commit();
                            else
                            {
                                sqlTrans.Rollback();
                                attCount = 0;
                            }
                        }
                        catch (SqlException ex)
                        {
                            sqlTrans.Rollback();
                            message = ex.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }
            return attCount;
        }

        public static bool InsertSessionToken(LoginUser loginUser)
        {
            bool result = false;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        /*
                         Keep track of the forms authentication issued for each user so that only a single forms authentication cookie 
                         (the most recently issued) is valid for the same user.
                         */
                        string sqlStr = @"UPDATE SessionToken SET Valid='N', Updated=@Updated WHERE LoginId=@LoginId AND Valid='Y'";
                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            cmd.Parameters.AddWithValue("@LoginId", loginUser.UserName);
                            cmd.Parameters.AddWithValue("@Updated", DateTime.Now);
                            result = cmd.ExecuteNonQuery() >= 0;
                        }

                        sqlStr = @"INSERT INTO SessionToken (SessionTokenId,FormAuthTokenId,LoginId,Valid,Updated) VALUES(@SessionTokenId,@FormAuthTokenId,@LoginId,@Valid,@Updated)";
                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            cmd.Parameters.AddWithValue("@LoginId", loginUser.UserName);
                            cmd.Parameters.AddWithValue("@FormAuthTokenId", loginUser.FrmAuthCookie);
                            cmd.Parameters.AddWithValue("@SessionTokenId", loginUser.SessionId);
                            cmd.Parameters.AddWithValue("@Valid", 'Y');
                            cmd.Parameters.AddWithValue("@Updated", DateTime.Now);
                            result = cmd.ExecuteNonQuery() > 0;
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch(Exception ex)
                    {
                        sqlTrans.Rollback();
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public static bool UpdateSessionTokenInvalid(LoginUser loginUser)
        {
            bool result = false;
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();
                    string sqlStr = @"UPDATE SessionToken SET Valid='N', Updated=@Updated WHERE (SessionTokenId=@SessionTokenId or LoginId=@LoginId) AND Valid='Y'";
                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConnection))
                    {
                        cmd.Parameters.AddWithValue("@LoginId", loginUser.UserName);
                        cmd.Parameters.AddWithValue("@SessionTokenId", loginUser.SessionId);
                        cmd.Parameters.AddWithValue("@Updated", DateTime.Now);
                        result = cmd.ExecuteNonQuery() >= 0;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public static bool CheckSessionValid(LoginUser loginUser)
        {
            bool result = false;
            try
            {
                SqlCommand cmd = null;
                string sqlStr = string.Empty;
                using (SqlConnection sqlConn = new SqlConnection(connStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        sqlStr = "DELETE FROM SessionToken WHERE [Updated] IS NOT NULL AND [Updated]<@Updated";
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            cmd.Parameters.AddWithValue("@Updated", DateTime.Today.AddDays(-1));
                            result = cmd.ExecuteNonQuery() > -1;
                        }
                        if (result)
                        {
                            DataTable dt = new DataTable();
                            sqlStr = "select * from SessionToken where SessionTokenId=@SessionTokenId and FormAuthTokenId=@FormAuthTokenId and LoginId=@LoginId";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            using (SqlDataAdapter adapter = new SqlDataAdapter())
                            {
                                cmd.Parameters.AddWithValue("@SessionTokenId", loginUser.SessionId);
                                cmd.Parameters.AddWithValue("@FormAuthTokenId", loginUser.FrmAuthCookie);
                                cmd.Parameters.AddWithValue("@LoginId", loginUser.UserName);                                

                                adapter.SelectCommand = cmd;
                                adapter.Fill(dt);
                            }
                            result = dt != null && dt.Rows.Count == 1 && !(dt.Rows[0]["Valid"] is DBNull) && Convert.ToChar(dt.Rows[0]["Valid"]) == 'Y';
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        result = false;
                    }
                }
            }
            catch(Exception ex)
            {
                result = false;
            }
            return result;
        }

        public static bool IsValidEmailAddress(string email)
        {
            return Regex.IsMatch(email, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        }
    }

 
}
