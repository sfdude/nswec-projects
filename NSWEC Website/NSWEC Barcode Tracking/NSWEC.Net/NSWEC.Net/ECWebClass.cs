﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Text;
using DevExpress.Web;
using DevExpress.Web.ASPxHtmlEditor;
using GenericParsing;
using Newtonsoft.Json;


namespace NSWEC.Net
{
    public class ECWebClass
    {
        private readonly static string connStr = ConfigurationManager.ConnectionStrings["DefaultDBConString"].ToString();
        //private readonly static string slaveConnStr = ConfigurationManager.ConnectionStrings["SlaveDBConString"].ToString();
        private readonly static string dateTimeFormat = "dd/MM/yyyy HH:mm:ss:fff";

        private readonly static string[] _themeList = 
        { 
            "themeAqua", "themeBlackGlass", "themeGlass", "themeiOS", "themeOffice2003Blue", 
            "themeOffice2003Olive", "themeOffice2003Silver", "themeOffice2010Black", "themeOffice2010Blue", "themeOffice2010Silver", 
            "themePlasticBlue", "themeRedWine", "themeSoftOrange", "themeYouthFul" 
        };

        public static string[] stateElectionROSourceFieldList = 
        { 
            "CDID", "District", "RO Surname", 
            "RO Office Contact Details / _RO Mobile # (Divert)", 
            "RO Office Contact Details / _RO Office Primary # ", 
            "RO Email address", "RO Office Delivery Address"
        };
        public static string[] stateElectionRODBFieldList = 
        { 
            "Code", "Name", "Contact", 
            "OfficePhone",
            "DirectPhone", 
            "Email", "SpecialInstructions"
        };

        public static string[] lgaElectionConnoteSourceFieldList = 
        { 
            "SequentialID", "EventID", "DeliveryLocation", "WardID", "WardName", 
            "CouncilID", "LGAreaCode", "ElectoralAreaName", "LongVenueName",
            "LocationTypeID", "LocationTypeCode", "VenueID", "VenueName", 
            "ContestID", "ContestTypeCode", "CDID", "ElectoralAreaName", "ContainerID", "ContainerTypeCode", "Total_cartons"           
        };

        public static string[] lgaElectionConnoteDBFieldList = 
        { 
            "SequentialId", "EventId", "ReturningOfficeName", "WardCode", "WardName", 
            "LGAId", "LGACode", "LGAName", "VenueLongName",
            "VenueTypeCode", "VenueTypeName", "VenueCode", "VenueName",
            "ContestCode", "ContestName", "CDID", "AreaAuthorityName", "ContainerCode", "ContainerName", "ContainerQuantity"            
        };

        public static string[] stateElectionConnoteSourceFieldList = 
        { 
            "SequentialID", "EventID", "DeliveryLocation", "LocationTypeID", "LocationTypeCode", "VenueID", "VenueName", 
            "ContestID", "ContestTypeCode", "CDID", "AreaCode", "ContainerID", "ContainerTypeCode", "CartonQuantity"
        };
        public static string[] stateElectionConnoteDBFieldList = 
        { 
            "SequentialId", "EventId", "ReturningOfficeName", "VenueTypeCode", "VenueTypeName", "VenueCode", "VenueName",
            "ContestCode", "ContestName", "CDID", "AreaAuthorityName", "ContainerCode", "ContainerName", "ContainerQuantity"
        };
        public static string[] venueTypeList = 
        {
            "Pre-Poll", "Polling Place", "Returning Office", "Declared Institution", 
            "NSWEC Head Office", "NSWEC Warehouse", "STH", "CPVC", "Postal", "Declaration Votes", "N/A"
        };
        public static string[] contestList = 
        {
            "Councillor", "Mayor", "Referendum", "Poll", "Mixed",
            "Councillor Reserve", "Mayor Reserve", "Referendum Reserve", "Poll Reserve"
        };
        public static string[] ContainerTypeList = 
        {
            "Carton", "Bag", "Ballot Box"
        };

        public static string[] SGESourceFileList = 
        {
            "Source", "Product", "ContainerType", "LocationType", "Location", "ManagementLocations", "Area", "Consignment", "Container"
        };

        public static string[] ConnoteFieldList = 
        {
            "ConsKey","ConsNo","SourceId","Source","CDID","ReturningOfficeName","LGAId","LGACode","LGAName", "ContestAreaId", "ContestAreaCode",
	        "WardCode", "WardName","VenueCode","VenueName","VenueLongName","VenueTypeCode","VenueTypeName","ContestCode","ContestName",
	        "ContainerCode","ContainerName","ContainerQuantity","DeliveryNumber","CountCentreCode","CountCentreName","Created","CreatedBy"
        };

        public static string[] SGETableList =
        {
            "NSW_BarcodeSource", "NSW_CountCentre", "NSW_ReturningOffice", "NSW_LGA", "NSW_Ward", "NSW_Contest", 
            "NSW_VenueType", "NSW_ContainerType", "NSW_StatusStage", "NSW_Location", "NSW_Venue",
            /*"NSW_Consignment",*/ "NSW_Connote", "NSW_Container", "NSW_ContainerStatus"
        };
        public static string[] SGETableList1 =
        {
            "NSW_BarcodeSource", "NSW_CountCentre", "NSW_ReturningOffice", "NSW_LGA", "NSW_Ward", "NSW_Contest", 
            "NSW_VenueType", "NSW_ContainerType", "NSW_StatusStage", "NSW_Location", "NSW_Venue"
            //"NSW_Consignment", "NSW_Connote", "NSW_Container", "NSW_ContainerStatus"
        };

        private static Color sfColor = Color.FromArgb(0, 125, 134); // "#007D86"
        private static Color sfLightColor = Color.FromArgb(0, 206, 166); // "#00CEA6"
        private static Color sfLightColor1 = Color.FromArgb(137, 234, 243); // "#89EAF3" - Pantone: 630U

        public static string[] GetThemeList()
        {
            return _themeList;
        }
        public static void LoadThemeList(ASPxComboBox cmbThemes)
        {
            ListEditItem themeItem;
            cmbThemes.Items.Clear();
            foreach (string theme in _themeList)
            {
                themeItem = new ListEditItem(theme.Replace("theme", string.Empty), theme.Replace("theme", string.Empty));
                cmbThemes.Items.Add(themeItem);
            }
        }
        public static void setPageTheme(Page page)
        {
            return;
            HttpCookie c = page.Request.Cookies["EC_Theme"];
            if (c != null)
                page.Theme = c.Value;
            else
                if (page.Session["EC_User"] != null && (page.Session["EC_User"] as LoginUser).PageTheme != null &&
                    (page.Session["EC_User"] as LoginUser).PageTheme != string.Empty)
                    page.Theme = (page.Session["EC_User"] as LoginUser).PageTheme;
                else
                    page.Theme = ConfigurationManager.AppSettings["UITheme"];
        }

        public static void SetMainMenuFor(Page page)
        {
            ASPxPanel plnMasterPage = page.Master.FindControl("plnMasterPage") as ASPxPanel;
            if (plnMasterPage != null)
            {
                ASPxMenu mMain = plnMasterPage.FindControl("mMain") as ASPxMenu;
                if (mMain != null)
                {
                    DevExpress.Web.MenuItem mItem = mMain.Items.FindByName("theme" + page.Theme);
                    if (mItem != null)
                    {
                        mItem.Checked = true;
                        mItem.Selected = true;
                    }
                }
            }
        }

        public static DataTable GetConnoteCatalogueDataTable(LoginUser loginUser, ref string procMsg)
        {
            DataTable dtConnote = new DataTable();
            try
            {
                const string cmdText = "exec NSW_GetConnoteCatalogueGeneric @CDID";
                using (SqlConnection sqlConnection = new SqlConnection(connStr))
                using (SqlCommand cmd = new SqlCommand(cmdText, sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    if (loginUser.CDID != null)
                        cmd.Parameters.AddWithValue("@CDID", loginUser.CDID);
                    else
                        cmd.Parameters.AddWithValue("@CDID", DBNull.Value);
                    adapter.Fill(dtConnote);
                }
            }
            catch(Exception ex)
            {
                dtConnote = null;
                procMsg = string.Format("Error in GetConnoteCatalogueDataTable: {0}", ex.Message);
            }
            return dtConnote;
        }

        public static string GetInsertDataSetSQLString(string tableName, DataTable dt)
        {
            string sqlStr = string.Empty;
            string fieldList = string.Empty;
            string valueList = string.Empty;
            if (dt.Columns.Count > 0)
            {
                sqlStr = String.Format("INSERT INTO {0} (", tableName);
                foreach (DataColumn col in dt.Columns)
                {
                    if (((tableName == "CONNOTE" || tableName == "Proforma_CONNOTE") && col.ColumnName == "CONS_KEY") ||
                        col.ColumnName == "PreQty" || col.ColumnName == "PreWgt" || col.ColumnName == "PreCubic" ||
                        col.ColumnName == "Flag" || col.ColumnName == "FromDB" || col.ColumnName == "ID")
                        continue;
                    fieldList += col.ColumnName + ",";
                    valueList += String.Format("@{0},", col.ColumnName);
                }
                if (fieldList.EndsWith(","))
                    fieldList = fieldList.Remove(fieldList.Length - 1);
                if (valueList.EndsWith(","))
                    valueList = valueList.Remove(valueList.Length - 1);
                sqlStr += String.Format("{0}) VALUES ({1}) ", fieldList, valueList);
                if (tableName == "CONNOTE" || tableName == "Proforma_CONNOTE")
                    sqlStr += "; SELECT CAST(scope_identity() AS int)";
            }
            return sqlStr;
        }
        public static string GetInsertDataSetSQLString(string tableName, DataTable dt, bool returnId)
        {
            string sqlStr = string.Empty;
            string fieldList = string.Empty;
            string valueList = string.Empty;
            if (dt.Columns.Count > 0)
            {
                sqlStr = String.Format("INSERT INTO {0} (", tableName);
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName == "ID" || col.ColumnName == "Flag" || col.ColumnName == "FromDB")
                        continue;
                    fieldList += col.ColumnName + ",";
                    valueList += String.Format("@{0},", col.ColumnName);
                }
                if (fieldList.EndsWith(","))
                    fieldList = fieldList.Remove(fieldList.Length - 1);
                if (valueList.EndsWith(","))
                    valueList = valueList.Remove(valueList.Length - 1);
                sqlStr += String.Format("{0}) VALUES ({1}) ", fieldList, valueList);
                if (returnId)
                    sqlStr += "; SELECT CAST(scope_identity() AS int)";
            }
            return sqlStr;
        }

        public static bool ImportConsignmentData(LoginUser loginUser, DataTable dtSrc, ref string procMsg)
        {
            bool result = dtSrc.Rows.Count == 0;
            if (!result)
            {
                using (SqlConnection sqlConn = new SqlConnection(connStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        result = DeleteTableData(sqlTrans, "NSW_Container", ref procMsg) &&
                            DeleteTableData(sqlTrans, "NSW_ContainerStatus", ref procMsg) &&
                            DeleteTableData(sqlTrans, "NSW_Connote", ref procMsg) &&
                            DeleteTableData(sqlTrans, "NSW_Consignment", ref procMsg) &&
                            DeleteTableData(sqlTrans, "NSW_ReturningOffice", ref procMsg) &&
                            DeleteTableData(sqlTrans, "NSW_LGA", ref procMsg) &&
                            DeleteTableData(sqlTrans, "NSW_Ward", ref procMsg) &&
                            DeleteTableData(sqlTrans, "NSW_Venue", ref procMsg);// &&
                            //DeleteTableData(sqlTrans, "NSW_VenueType", ref procMsg) &&
                            //DeleteTableData(sqlTrans, "NSW_Contest", ref procMsg);
                        string eventId = string.Empty;

                        if (result)
                        {
                            string sqlStr = string.Format("SELECT * FROM {0} WHERE ID=-1", "NSW_Consignment");
                            foreach (DataRow drSource in dtSrc.Rows)
                            {
                                //if ((drSource["SequenceID"] is DBNull) || (!(drSource["SequenceID"] is DBNull) && drSource[0].ToString().Trim().Length == 0))
                                //    continue;
                                if ((drSource["SequentialID"] is DBNull) || (!(drSource["SequentialID"] is DBNull) && drSource[0].ToString().Trim().Length == 0))
                                    continue;
                                if (dtSrc.Columns.IndexOf("CartonQuantity") > -1 && 
                                    ((drSource["CartonQuantity"] is DBNull) || 
                                        (!(drSource["CartonQuantity"] is DBNull) && drSource["CartonQuantity"].ToString().Trim().Length == 0)))
                                    continue;
                                if (dtSrc.Columns.IndexOf("Total_cartons") > -1 && 
                                    ((drSource["Total_cartons"] is DBNull) || (!(drSource["Total_cartons"] is DBNull) && drSource["Total_cartons"].ToString().Trim().Length == 0)))
                                    continue;

                                if (string.IsNullOrEmpty(eventId) && dtSrc.Columns.IndexOf("EventID") > -1 &&
                                    !(drSource["EventID"] is DBNull) && drSource["EventID"].ToString() != string.Empty)
                                    eventId = drSource["EventID"].ToString().Trim().Remove(drSource["EventID"].ToString().Trim().IndexOf('-'));

                                using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                {
                                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                    {
                                        sqlDataAdapter.SelectCommand = cmd;
                                        using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                        {
                                            sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                            sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                            sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                            sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;
                                            /*
                                            SqlParameter insertParam = sqlDataAdapter.InsertCommand.Parameters.Add(
                                                "@ConsKey", SqlDbType.Int);
                                            insertParam.Direction = ParameterDirection.Output;
                                            sqlDataAdapter.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
                                            */
                                            DataTable dtData = new DataTable();
                                            sqlDataAdapter.Fill(dtData);

                                            int colIdx = -1;
                                            string colSrcName = string.Empty;
                                            DataRow drData = dtData.NewRow();
                                            foreach (DataColumn col in dtData.Columns)
                                            {
                                                if (col.ColumnName != "ID")
                                                {
                                                    if (col.ColumnName == "Created")
                                                        drData[col.ColumnName] = DateTime.Now;
                                                    else
                                                        if (col.ColumnName == "CreatedBy")
                                                            drData[col.ColumnName] = loginUser.UserName;
                                                        else
                                                        {
                                                            switch (loginUser.EventType)
                                                            {
                                                                case EventType.LOCAL_GOVERNMENT:
                                                                    colIdx = Array.IndexOf(lgaElectionConnoteDBFieldList, col.ColumnName);
                                                                    if (colIdx > -1)
                                                                    {
                                                                        colSrcName = lgaElectionConnoteSourceFieldList[colIdx];
                                                                        if (dtSrc.Columns.IndexOf(colSrcName) > -1) 
                                                                        {                                                                            
                                                                            if (col.ColumnName == "CDID")
                                                                            {
                                                                                if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                    drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(3, '0');
                                                                            }
                                                                            else
                                                                                if (col.ColumnName == "WardCode")
                                                                                {
                                                                                    if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                        drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(2, '0');
                                                                                    else
                                                                                        drData[col.ColumnName] = "00";
                                                                                }
                                                                                else
                                                                                if (col.ColumnName == "WardName")
                                                                                {
                                                                                    if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                        drData[col.ColumnName] = drSource[colSrcName];
                                                                                    else
                                                                                        drData[col.ColumnName] = "Undivided";
                                                                                }
                                                                                else
                                                                                if (col.ColumnName == "VenueTypeCode")
                                                                                {
                                                                                    if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                        drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(2, '0');
                                                                                }
                                                                                else
                                                                                    if (col.ColumnName == "VenueCode")
                                                                                    {
                                                                                        if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                            drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(5, '0');
                                                                                    }
                                                                                    else
                                                                                        if (col.ColumnName == "ContestCode")
                                                                                        {
                                                                                            if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                                drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(2, '0');
                                                                                        }
                                                                                        else
                                                                                            if (col.ColumnName == "ContainerCode")
                                                                                            {
                                                                                                ContainerType containerType = ContainerType.Carton;
                                                                                                if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                                {
                                                                                                    int containerTypeIdx = -1;
                                                                                                    if (int.TryParse(drSource[colSrcName].ToString(), out containerTypeIdx))
                                                                                                    {
                                                                                                        containerType = (ContainerType)containerTypeIdx;
                                                                                                        drData[col.ColumnName] = ((int)containerType).ToString().PadLeft(2, '0');
                                                                                                        drData["ContainerName"] = containerType.ToString();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        drData[col.ColumnName] = ((int)containerType).ToString().PadLeft(2, '0');
                                                                                                        drData["ContainerName"] = containerType.ToString();
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    drData[col.ColumnName] = ((int)containerType).ToString().PadLeft(2, '0');
                                                                                                    drData["ContainerName"] = containerType.ToString();
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                                if (col.ColumnName == "ContainerQuantity")
                                                                                                {
                                                                                                    if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                                        drData[col.ColumnName] = drSource[colSrcName];
                                                                                                    else
                                                                                                        drData[col.ColumnName] = 1;
                                                                                                }
                                                                                                else
                                                                                                    if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                                        drData[col.ColumnName] = drSource[colSrcName];
                                                                        }
                                                                    }
                                                                    break;
                                                                case EventType.STATE_BY_ELECTION:
                                                                case EventType.STATE_ELECTION:
                                                                    colIdx = Array.IndexOf(stateElectionConnoteDBFieldList, col.ColumnName);
                                                                    if (colIdx > -1)
                                                                    {
                                                                        colSrcName = stateElectionConnoteSourceFieldList[colIdx];
                                                                        if (dtSrc.Columns.IndexOf(colSrcName) > -1) //  && !(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty
                                                                        {
                                                                            if (col.ColumnName == "CDID")
                                                                            {
                                                                                if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                    drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(3, '0');
                                                                            }
                                                                            else
                                                                                if (col.ColumnName == "VenueTypeCode")
                                                                                {
                                                                                    if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                        drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(2, '0');
                                                                                }
                                                                                else
                                                                                    if (col.ColumnName == "VenueCode")
                                                                                    {
                                                                                        if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                            drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(5, '0');
                                                                                    }
                                                                                    else
                                                                                        if (col.ColumnName == "ContestCode")
                                                                                        {
                                                                                            if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                                drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(2, '0');
                                                                                        }
                                                                                        else
                                                                                            if (col.ColumnName == "ContainerCode")
                                                                                            {
                                                                                                ContainerType containerType = ContainerType.Carton;
                                                                                                if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                                {
                                                                                                    int containerTypeIdx = -1;
                                                                                                    if (int.TryParse(drSource[colSrcName].ToString(), out containerTypeIdx))
                                                                                                    {
                                                                                                        containerType = (ContainerType)containerTypeIdx;
                                                                                                        drData[col.ColumnName] = ((int)containerType).ToString().PadLeft(2, '0');
                                                                                                        drData["ContainerName"] = containerType.ToString();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        drData[col.ColumnName] = ((int)containerType).ToString().PadLeft(2, '0');
                                                                                                        drData["ContainerName"] = containerType.ToString();
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    drData[col.ColumnName] = ((int)containerType).ToString().PadLeft(2, '0');
                                                                                                    drData["ContainerName"] = containerType.ToString();
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                                if (col.ColumnName == "ContainerQuantity")
                                                                                                {
                                                                                                    if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                                        drData[col.ColumnName] = drSource[colSrcName];
                                                                                                    else
                                                                                                        drData[col.ColumnName] = 1;
                                                                                                }
                                                                                                else
                                                                                                    if (!(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                                                        drData[col.ColumnName] = drSource[colSrcName];
                                                                        }
                                                                    }
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                        }
                                                }
                                            }
                                            if (drData["ContainerCode"] is DBNull)
                                            {
                                                ContainerType containerType = ContainerType.Carton;
                                                drData["ContainerCode"] = ((int)containerType).ToString().PadLeft(2, '0');
                                                drData["ContainerName"] = containerType.ToString();
                                            }
                                            
                                            dtData.Rows.Add(drData);
                                            result = sqlDataAdapter.Update(dtData) > 0;
                                            if (!result)
                                                break;
                                        }
                                    }
                                }
                            }
                            /*
                            if (result)
                            {
                                if (result)
                                    sqlTrans.Commit();
                                else
                                    sqlTrans.Rollback();
                                return result;
                            }
                             */
                            if (result)
                            {
                                // connote
                                DataTable dtConsignment = new DataTable();
                                using (SqlCommand sqlCommand = new SqlCommand(
                                    @"SELECT DISTINCT left(EventId, charindex('-',EventId)-1) as EventId, CDID, ReturningOfficeName, LGAId, LGACode, LGAName, WardCode, WardName,
	                                    VenueCode, VenueName, VenueTypeCode, VenueTypeName,
	                                    ContestCode, ContestName, ContainerCode, ContainerName,
	                                    SUM(ContainerQuantity) AS [ContainerQuantity] FROM NSW_Consignment  
                                    WHERE CDID is not null and VENUECODE IS NOT NULL
                                    GROUP BY left(EventId, charindex('-',EventId)-1), CDID, ReturningOfficeName, LGAId, LGACode, LGAName, WardCode, WardName,
	                                    VenueCode, VenueName, VenueTypeCode, VenueTypeName,
	                                    ContestCode, ContestName, ContainerCode, ContainerName
                                    ORDER BY CDID, VenueCode", sqlTrans.Connection, sqlTrans))
                                {
                                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                                    {
                                        adapter.SelectCommand = sqlCommand;
                                        adapter.Fill(dtConsignment);
                                    }
                                }
                                if (dtConsignment.Rows.Count > 0)
                                {
                                    sqlStr = string.Format("SELECT * FROM {0} WHERE ConsKey=-1", "NSW_Connote");
                                    foreach (DataRow drConnote in dtConsignment.Rows)
                                    {
                                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                        {
                                            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                            {
                                                sqlDataAdapter.SelectCommand = cmd;
                                                using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                                {
                                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;
                                                   
                                                    DataTable dtData = new DataTable();
                                                    sqlDataAdapter.Fill(dtData);

                                                    int colIdx = -1;
                                                    string colSrcName = string.Empty;
                                                    DataRow drData = dtData.NewRow();
                                                    foreach (DataColumn col in dtData.Columns)
                                                    {
                                                        if (col.ColumnName != "ConsKey")
                                                        {
                                                            if (col.ColumnName == "Created")
                                                                drData[col.ColumnName] = DateTime.Now;
                                                            else
                                                                if (col.ColumnName == "CreatedBy")
                                                                    drData[col.ColumnName] = loginUser.UserName;
                                                                else
                                                                {
                                                                    colIdx = dtConsignment.Columns.IndexOf(col.ColumnName);
                                                                    if (colIdx > -1)
                                                                        drData[col.ColumnName] = drConnote[col.ColumnName];
                                                                }
                                                        }
                                                    }
                                                    switch (loginUser.EventType)
                                                    {
                                                        case EventType.LOCAL_GOVERNMENT:
                                                            if (!(drData["CDID"] is DBNull) && drData["CDID"].ToString().Trim() != string.Empty &&
                                                                !(drData["LGAId"] is DBNull) && drData["LGAId"].ToString().Trim() != string.Empty &&
                                                                !(drData["WardCode"] is DBNull) && drData["WardCode"].ToString().Trim() != string.Empty &&
                                                                !(drData["VenueCode"] is DBNull) && drData["VenueCode"].ToString().Trim() != string.Empty &&
                                                                !(drData["VenueTypeCode"] is DBNull) && drData["VenueTypeCode"].ToString().Trim() != string.Empty &&
                                                                !(drData["ContestCode"] is DBNull) && drData["ContestCode"].ToString().Trim() != string.Empty &&
                                                                !(drData["ContainerCode"] is DBNull) && drData["ContainerCode"].ToString().Trim() != string.Empty)
                                                                drData["ConsNo"] = string.Format("{0}{1}{2}{3}{4}{5}{6}",
                                                                    drData["CDID"].ToString().Trim().Substring(drData["CDID"].ToString().Trim().Length - 2),
                                                                    drData["LGAId"].ToString().Trim(),
                                                                    drData["WardCode"].ToString().Trim(),
                                                                    // Note: the venue location code may need to remain at 4 digits – however if 5 digits are required Compdata to advise of cost to implement this requirement.
                                                                    drData["VenueCode"].ToString().Trim().Substring(drData["VenueCode"].ToString().Trim().Length - 4),
                                                                    drData["VenueTypeCode"].ToString().Trim().Substring(drData["VenueTypeCode"].ToString().Trim().Length - 2),
                                                                    drData["ContestCode"].ToString().Trim().Substring(drData["ContestCode"].ToString().Trim().Length - 2),
                                                                    drData["ContainerCode"].ToString().Trim().Substring(drData["ContainerCode"].ToString().Trim().Length - 2));
                                                            break;
                                                        case EventType.STATE_BY_ELECTION:
                                                        case EventType.STATE_ELECTION:
                                                            if (!(drData["CDID"] is DBNull) && drData["CDID"].ToString().Trim() != string.Empty &&
                                                                !(drData["VenueCode"] is DBNull) && drData["VenueCode"].ToString().Trim() != string.Empty &&
                                                                !(drData["VenueTypeCode"] is DBNull) && drData["VenueTypeCode"].ToString().Trim() != string.Empty &&
                                                                !(drData["ContestCode"] is DBNull) && drData["ContestCode"].ToString().Trim() != string.Empty &&
                                                                !(drData["ContainerCode"] is DBNull) && drData["ContainerCode"].ToString().Trim() != string.Empty)
                                                                drData["ConsNo"] = string.Format("{0}{1}{2}{3}{4}",
                                                                    drData["CDID"].ToString().Trim().Substring(drData["CDID"].ToString().Trim().Length - 2),
                                                                    drData["VenueCode"].ToString().Trim().Substring(drData["VenueCode"].ToString().Trim().Length - 4),
                                                                    drData["VenueTypeCode"].ToString().Trim().Substring(drData["VenueTypeCode"].ToString().Trim().Length - 2),
                                                                    drData["ContestCode"].ToString().Trim().Substring(drData["ContestCode"].ToString().Trim().Length - 2),
                                                                    drData["ContainerCode"].ToString().Trim().Substring(drData["ContainerCode"].ToString().Trim().Length - 2));
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                    dtData.Rows.Add(drData);
                                                    result = sqlDataAdapter.Update(dtData) > 0;
                                                    if (!result)
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (result)
                                {
                                    // container
                                    DataTable dtConnote = new DataTable();
                                    using (SqlCommand sqlCommand = new SqlCommand("select * from NSW_Connote where ConsNo is not null", sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                                        {
                                            adapter.SelectCommand = sqlCommand;
                                            adapter.Fill(dtConnote);
                                        }
                                    }
                                    if (dtConnote.Rows.Count > 0)
                                    {
                                        int containerQty = 0;
                                        int lineNo = 0;
                                        string containerNo = string.Empty;
                                        string barcode = string.Empty;
                                        string sqlInsertContainer = @"INSERT INTO NSW_Container([ConsKey],[LineNo],[ContainerNo],[Barcode],[Created],[CreatedBy]) 
                                        VALUES (@ConsKey,@LineNo,@ContainerNo,@Barcode,@Created,@CreatedBy)";
                                        foreach (DataRow drConnote in dtConnote.Rows)
                                        {
                                            if (!(drConnote["ContainerQuantity"] is DBNull) && drConnote["ContainerQuantity"].ToString() != null)
                                            {
                                                containerQty = Convert.ToInt32(drConnote["ContainerQuantity"]);

                                                for (int i = 0; i < containerQty; i++)
                                                {
                                                    lineNo = i + 1;
                                                    containerNo = string.Format("{0}{1}", lineNo.ToString().PadLeft(3, '0'), containerQty.ToString().PadLeft(3, '0'));
                                                    barcode = string.Format("{0}{1}{2}", drConnote["ConsNo"], containerNo, drConnote["PrinterJobId"]).Trim();
                                                    using (SqlCommand cmd = new SqlCommand(sqlInsertContainer, sqlTrans.Connection, sqlTrans))
                                                    {
                                                        cmd.Parameters.AddWithValue("@ConsKey", drConnote["ConsKey"]);
                                                        cmd.Parameters.AddWithValue("@LineNo", lineNo);
                                                        cmd.Parameters.AddWithValue("@ContainerNo", containerNo);
                                                        cmd.Parameters.AddWithValue("@Barcode", barcode);
                                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                        cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                        result = cmd.ExecuteNonQuery() > 0;
                                                        if (!result)
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (result)
                                {
                                    // ro
                                    DataTable dtRO = new DataTable();
                                    using (SqlCommand sqlCommand = new SqlCommand(@"select * from NSW_ReturningOffice where EventId=@EventId", sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                                        {
                                            adapter.SelectCommand = sqlCommand;
                                            sqlCommand.Parameters.AddWithValue("@EventId", eventId);
                                            adapter.Fill(dtRO);
                                        }
                                    }
                                    if (dtRO.Rows.Count == 0)
                                    {
                                        dtRO = new DataTable();

                                        using (SqlCommand sqlCommand = new SqlCommand(@"select distinct CDID, ReturningOfficeName from NSW_Consignment
                                            where CHARINDEX(@EventId,EventId)=1 and CDID is not null order by CDID", sqlTrans.Connection, sqlTrans))
                                        {
                                            using (SqlDataAdapter adapter = new SqlDataAdapter())
                                            {
                                                adapter.SelectCommand = sqlCommand;
                                                sqlCommand.Parameters.AddWithValue("@EventId", eventId);
                                                adapter.Fill(dtRO);
                                            }
                                            if (dtRO.Rows.Count > 0)
                                            {
                                                string sqlInsertRO = @"INSERT INTO NSW_ReturningOffice ([EventId],[Code],[Name],[Active],[Created],[CreatedBy]) 
                                                    VALUES (@EventId,@Code,@Name,@Active,@Created,@CreatedBy)";
                                                /*
                                                string sqlInsertVenue = @"INSERT INTO NSW_Venue ([EventId],[Code],[Name],[LongName],[ROCode],[ROName],[LGACode],[WardCode],[VenueTypeCode],[VenueTypeName],[Active],[Created],[CreatedBy]) 
                                                    VALUES (@EventId,@Code,@Name,@LongName,@ROCode,@ROName,@LGACode,@WardCode,@VenueTypeCode,@VenueTypeName,@Active,@Created,@CreatedBy)";
                                                 */
                                                foreach (DataRow drRO in dtRO.Rows)
                                                {
                                                    if (!(drRO["CDID"] is DBNull) && drRO["CDID"].ToString() != string.Empty)
                                                    {
                                                        using (SqlCommand cmd = new SqlCommand(sqlInsertRO, sqlTrans.Connection, sqlTrans))
                                                        {
                                                            cmd.Parameters.AddWithValue("@EventId", eventId);
                                                            cmd.Parameters.AddWithValue("@Code", drRO["CDID"]);
                                                            /*
                                                            if (!(drRO["AreaAuthorityName"] is DBNull) && drRO["AreaAuthorityName"].ToString() != string.Empty)
                                                                cmd.Parameters.AddWithValue("@Name", drRO["AreaAuthorityName"]);
                                                            else
                                                                cmd.Parameters.AddWithValue("@Name", DBNull.Value); 
                                                             */
                                                            if (!(drRO["ReturningOfficeName"] is DBNull) && drRO["ReturningOfficeName"].ToString() != string.Empty)
                                                                cmd.Parameters.AddWithValue("@Name", drRO["ReturningOfficeName"]);
                                                            else
                                                                cmd.Parameters.AddWithValue("@Name", DBNull.Value); 
                                                            cmd.Parameters.AddWithValue("@Active", 'Y');
                                                            cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                            cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                            result = cmd.ExecuteNonQuery() > 0;
                                                            if (!result)
                                                                break;
                                                        }
                                                        /*
                                                        int maxTeam = 10; int startPoint = 9989;
                                                        for (int i = 0; i < maxTeam; i++)                                                        
                                                        {
                                                            using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                                                            {
                                                                cmd.Parameters.AddWithValue("@EventId", eventId);
                                                                cmd.Parameters.AddWithValue("@Code", string.Format("{0}", startPoint - i).PadLeft(5, '0'));
                                                                cmd.Parameters.AddWithValue("@Name", string.Format("DI TEAM {0}", (maxTeam - i).ToString().PadLeft(2, '0')));
                                                                cmd.Parameters.AddWithValue("@LongName", string.Format("DI TEAM {0}", (maxTeam - i).ToString().PadLeft(2, '0')));
                                                                cmd.Parameters.AddWithValue("@ROCode", drRO["CDID"]);
                                                                if (!(drRO["ReturningOfficeName"] is DBNull) && drRO["ReturningOfficeName"].ToString() != string.Empty)
                                                                    cmd.Parameters.AddWithValue("@ROName", drRO["ReturningOfficeName"]);
                                                                else
                                                                    cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                                                cmd.Parameters.AddWithValue("@LGACode", "000");
                                                                cmd.Parameters.AddWithValue("@WardCode", "00");

                                                                cmd.Parameters.AddWithValue("@VenueTypeCode", "04");
                                                                cmd.Parameters.AddWithValue("@VenueTypeName", "Declared Institution");                                                               
                                                                cmd.Parameters.AddWithValue("@Active", 'Y');
                                                                cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                                cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                                result = cmd.ExecuteNonQuery() > 0;
                                                                if (!result)
                                                                    break;
                                                            }
                                                        }

                                                        using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                                                        {
                                                            cmd.Parameters.AddWithValue("@EventId", eventId);
                                                            cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 2967).PadLeft(5, '0'));
                                                            //cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 9998).PadLeft(5, '0'));
                                                            cmd.Parameters.AddWithValue("@Name", "Sydney Town Hall");
                                                            cmd.Parameters.AddWithValue("@LongName", "Sydney Town Hall");
                                                            cmd.Parameters.AddWithValue("@ROCode", drRO["CDID"]);
                                                            if (!(drRO["ReturningOfficeName"] is DBNull) && drRO["ReturningOfficeName"].ToString() != string.Empty)
                                                                cmd.Parameters.AddWithValue("@ROName", drRO["ReturningOfficeName"]);
                                                            else
                                                                cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                                            cmd.Parameters.AddWithValue("@LGACode", "000");
                                                            cmd.Parameters.AddWithValue("@WardCode", "00");

                                                            cmd.Parameters.AddWithValue("@VenueTypeCode", "07");
                                                            cmd.Parameters.AddWithValue("@VenueTypeName", "Pre-Poll and Polling Place");
                                                            cmd.Parameters.AddWithValue("@Active", 'Y');
                                                            cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                            cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                            result = cmd.ExecuteNonQuery() > 0;
                                                            if (!result)
                                                                break;
                                                        }
                                                        
                                                        using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                                                        {
                                                            cmd.Parameters.AddWithValue("@EventId", eventId);
                                                            //cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 9999).PadLeft(5, '0'));
                                                            cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 9998).PadLeft(5, '0'));
                                                            cmd.Parameters.AddWithValue("@Name", "Postal Voting");
                                                            cmd.Parameters.AddWithValue("@LongName", "Postal Voting");
                                                            cmd.Parameters.AddWithValue("@ROCode", drRO["CDID"]);
                                                            if (!(drRO["ReturningOfficeName"] is DBNull) && drRO["ReturningOfficeName"].ToString() != string.Empty)
                                                                cmd.Parameters.AddWithValue("@ROName", drRO["ReturningOfficeName"]);
                                                            else
                                                                cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                                            cmd.Parameters.AddWithValue("@LGACode", "000");
                                                            cmd.Parameters.AddWithValue("@WardCode", "00");

                                                            cmd.Parameters.AddWithValue("@VenueTypeCode", "09");
                                                            cmd.Parameters.AddWithValue("@VenueTypeName", "Postal Voting");
                                                            cmd.Parameters.AddWithValue("@Active", 'Y');
                                                            cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                            cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                            result = cmd.ExecuteNonQuery() > 0;
                                                            if (!result)
                                                                break;
                                                        }
                                                         */
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (result)
                                {
                                    // LGA
                                    DataTable dtLGA = new DataTable();
                                    using (SqlCommand sqlCommand = new SqlCommand(@"select distinct CDID, ReturningOfficeName, LGAId, LGACode, LGAName from NSW_Consignment
                                        where CHARINDEX(@EventId,EventId)=1 and LGAId is not null and LGACode is not null order by LGAId", sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                                        {
                                            adapter.SelectCommand = sqlCommand;
                                            sqlCommand.Parameters.AddWithValue("@EventId", eventId);
                                            adapter.Fill(dtLGA);
                                        }
                                        if (dtLGA.Rows.Count > 0)
                                        {
                                            string sqlInsertLGA = @"INSERT INTO NSW_LGA ([EventId],[ROCode],[ROName],[Code],[AreaCode],[AreaAuthorityName],[Active],[Created],[CreatedBy]) 
                                                VALUES (@EventId,@ROCode,@ROName,@Code,@AreaCode,@AreaAuthorityName,@Active,@Created,@CreatedBy)";

                                            string sqlInsertVenue = @"INSERT INTO NSW_Venue ([EventId],[Code],[Name],[LongName],[ROCode],[ROName],[LGACode],[LGAName],[WardCode],[WardName],[VenueTypeCode],[VenueTypeName],[Active],[Created],[CreatedBy]) 
                                                    VALUES (@EventId,@Code,@Name,@LongName,@ROCode,@ROName,@LGACode,@LGAName,@WardCode,@WardName,@VenueTypeCode,@VenueTypeName,@Active,@Created,@CreatedBy)";

                                            foreach (DataRow drLGA in dtLGA.Rows)
                                            {
                                                if (!(drLGA["LGAId"] is DBNull) && drLGA["LGAId"].ToString() != string.Empty)
                                                {
                                                    using (SqlCommand cmd = new SqlCommand(sqlInsertLGA, sqlTrans.Connection, sqlTrans))
                                                    {
                                                        cmd.Parameters.AddWithValue("@EventId", eventId);
                                                        cmd.Parameters.AddWithValue("@ROCode", drLGA["CDID"]);
                                                        cmd.Parameters.AddWithValue("@ROName", drLGA["ReturningOfficeName"]);
                                                        cmd.Parameters.AddWithValue("@Code", drLGA["LGAId"]);
                                                        if (!(drLGA["LGACode"] is DBNull) && drLGA["LGACode"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@AreaCode", drLGA["LGACode"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@AreaCode", DBNull.Value);
                                                        if (!(drLGA["LGAName"] is DBNull) && drLGA["LGAName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@AreaAuthorityName", drLGA["LGAName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@AreaAuthorityName", DBNull.Value);
                                                        cmd.Parameters.AddWithValue("@Active", 'Y');
                                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                        cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                        result = cmd.ExecuteNonQuery() > 0;
                                                        if (!result)
                                                            break;
                                                    }
                                                }

                                                int maxTeam = 10; int startPoint = 9989;
                                                for (int i = 0; i < maxTeam; i++)
                                                {
                                                    using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                                                    {
                                                        cmd.Parameters.AddWithValue("@EventId", eventId);
                                                        cmd.Parameters.AddWithValue("@Code", string.Format("{0}", startPoint - i).PadLeft(5, '0'));
                                                        cmd.Parameters.AddWithValue("@Name", string.Format("DI TEAM {0}", (maxTeam - i).ToString().PadLeft(2, '0')));
                                                        cmd.Parameters.AddWithValue("@LongName", string.Format("DI TEAM {0}", (maxTeam - i).ToString().PadLeft(2, '0')));
                                                        cmd.Parameters.AddWithValue("@ROCode", drLGA["CDID"]);
                                                        if (!(drLGA["ReturningOfficeName"] is DBNull) && drLGA["ReturningOfficeName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@ROName", drLGA["ReturningOfficeName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                                        cmd.Parameters.AddWithValue("@LGACode", drLGA["LGAId"]);
                                                        if (!(drLGA["LGAName"] is DBNull) && drLGA["LGAName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@LGAName", drLGA["LGAName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@LGAName", DBNull.Value);
                                                        cmd.Parameters.AddWithValue("@WardCode", "00");
                                                        cmd.Parameters.AddWithValue("@WardName", "Undivided");

                                                        cmd.Parameters.AddWithValue("@VenueTypeCode", "04");
                                                        cmd.Parameters.AddWithValue("@VenueTypeName", "Declared Institution");
                                                        cmd.Parameters.AddWithValue("@Active", 'Y');
                                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                        cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                        result = cmd.ExecuteNonQuery() > 0;
                                                        if (!result)
                                                            break;
                                                    }
                                                }

                                                using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                                                {
                                                    cmd.Parameters.AddWithValue("@EventId", eventId);
                                                    cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 2967).PadLeft(5, '0'));
                                                    //cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 9998).PadLeft(5, '0'));
                                                    cmd.Parameters.AddWithValue("@Name", "Sydney Town Hall");
                                                    cmd.Parameters.AddWithValue("@LongName", "Sydney Town Hall");
                                                    cmd.Parameters.AddWithValue("@ROCode", drLGA["CDID"]);
                                                    if (!(drLGA["ReturningOfficeName"] is DBNull) && drLGA["ReturningOfficeName"].ToString() != string.Empty)
                                                        cmd.Parameters.AddWithValue("@ROName", drLGA["ReturningOfficeName"]);
                                                    else
                                                        cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                                    cmd.Parameters.AddWithValue("@LGACode", drLGA["LGAId"]);
                                                    if (!(drLGA["LGAName"] is DBNull) && drLGA["LGAName"].ToString() != string.Empty)
                                                        cmd.Parameters.AddWithValue("@LGAName", drLGA["LGAName"]);
                                                    else
                                                        cmd.Parameters.AddWithValue("@LGAName", DBNull.Value);
                                                    cmd.Parameters.AddWithValue("@WardCode", "00");
                                                    cmd.Parameters.AddWithValue("@WardName", "Undivided");
                                                    /*
                                                    cmd.Parameters.AddWithValue("@VenueTypeCode", "07");
                                                    cmd.Parameters.AddWithValue("@VenueTypeName", "Pre-Poll and Polling Place");
                                                     */
                                                    cmd.Parameters.AddWithValue("@VenueTypeCode", "02");
                                                    cmd.Parameters.AddWithValue("@VenueTypeName", "Polling Place");
                                                    cmd.Parameters.AddWithValue("@Active", 'Y');
                                                    cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                    cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                    result = cmd.ExecuteNonQuery() > 0;
                                                    if (!result)
                                                        break;
                                                }

                                                using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                                                {
                                                    cmd.Parameters.AddWithValue("@EventId", eventId);
                                                    cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 9990).PadLeft(5, '0'));
                                                    cmd.Parameters.AddWithValue("@Name", "Sydney Town Hall Pre-Poll");
                                                    cmd.Parameters.AddWithValue("@LongName", "Sydney Town Hall Pre-Poll");
                                                    cmd.Parameters.AddWithValue("@ROCode", drLGA["CDID"]);
                                                    if (!(drLGA["ReturningOfficeName"] is DBNull) && drLGA["ReturningOfficeName"].ToString() != string.Empty)
                                                        cmd.Parameters.AddWithValue("@ROName", drLGA["ReturningOfficeName"]);
                                                    else
                                                        cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                                    cmd.Parameters.AddWithValue("@LGACode", drLGA["LGAId"]);
                                                    if (!(drLGA["LGAName"] is DBNull) && drLGA["LGAName"].ToString() != string.Empty)
                                                        cmd.Parameters.AddWithValue("@LGAName", drLGA["LGAName"]);
                                                    else
                                                        cmd.Parameters.AddWithValue("@LGAName", DBNull.Value);
                                                    cmd.Parameters.AddWithValue("@WardCode", "00");
                                                    cmd.Parameters.AddWithValue("@WardName", "Undivided");

                                                    cmd.Parameters.AddWithValue("@VenueTypeCode", "01");
                                                    cmd.Parameters.AddWithValue("@VenueTypeName", "Pre-polling Place");
                                                    cmd.Parameters.AddWithValue("@Active", 'Y');
                                                    cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                    cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                    result = cmd.ExecuteNonQuery() > 0;
                                                    if (!result)
                                                        break;
                                                }

                                                using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                                                {
                                                    cmd.Parameters.AddWithValue("@EventId", eventId);
                                                    cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 9998).PadLeft(5, '0'));
                                                    cmd.Parameters.AddWithValue("@Name", "Postal Voting");
                                                    cmd.Parameters.AddWithValue("@LongName", "Postal Voting");
                                                    cmd.Parameters.AddWithValue("@ROCode", drLGA["CDID"]);
                                                    if (!(drLGA["ReturningOfficeName"] is DBNull) && drLGA["ReturningOfficeName"].ToString() != string.Empty)
                                                        cmd.Parameters.AddWithValue("@ROName", drLGA["ReturningOfficeName"]);
                                                    else
                                                        cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                                    cmd.Parameters.AddWithValue("@LGACode", drLGA["LGAId"]);
                                                    if (!(drLGA["LGAName"] is DBNull) && drLGA["LGAName"].ToString() != string.Empty)
                                                        cmd.Parameters.AddWithValue("@LGAName", drLGA["LGAName"]);
                                                    else
                                                        cmd.Parameters.AddWithValue("@LGAName", DBNull.Value);
                                                    cmd.Parameters.AddWithValue("@WardCode", "00");
                                                    cmd.Parameters.AddWithValue("@WardName", "Undivided");

                                                    cmd.Parameters.AddWithValue("@VenueTypeCode", "09");
                                                    cmd.Parameters.AddWithValue("@VenueTypeName", "Postal Voting");
                                                    cmd.Parameters.AddWithValue("@Active", 'Y');
                                                    cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                    cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                    result = cmd.ExecuteNonQuery() > 0;
                                                    if (!result)
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    // ward
                                    DataTable dtWard = new DataTable();
                                    using (SqlCommand sqlCommand = new SqlCommand(@"select distinct CDID, ReturningOfficeName, LGAId,LGAName, WardCode, WardName from NSW_Consignment
                                        where CHARINDEX(@EventId,EventId)=1 and WardCode is not null and WardName is not null order by WardCode", sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                                        {
                                            adapter.SelectCommand = sqlCommand;
                                            sqlCommand.Parameters.AddWithValue("@EventId", eventId);
                                            adapter.Fill(dtWard);
                                        }
                                        if (dtWard.Rows.Count > 0)
                                        {
                                            string sqlInsertWard = @"INSERT INTO NSW_Ward ([EventId],[ROCode],[ROName],[LGAId],[LGACode],[Code],[Name],[Active],[Created],[CreatedBy]) 
                                                VALUES (@EventId,@ROCode,@ROName,@LGAId,@LGACode,@Code,@Name,@Active,@Created,@CreatedBy)";
                                            foreach (DataRow drWard in dtWard.Rows)
                                            {
                                                if (!(drWard["WardCode"] is DBNull) && drWard["WardCode"].ToString() != string.Empty)
                                                {
                                                    using (SqlCommand cmd = new SqlCommand(sqlInsertWard, sqlTrans.Connection, sqlTrans))
                                                    {
                                                        cmd.Parameters.AddWithValue("@EventId", eventId);
                                                        cmd.Parameters.AddWithValue("@ROCode", drWard["CDID"]);
                                                        cmd.Parameters.AddWithValue("@ROName", drWard["ReturningOfficeName"]);
                                                        cmd.Parameters.AddWithValue("@LGAId", drWard["LGAId"]);
                                                        if (!(drWard["LGAName"] is DBNull) && drWard["LGAName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@LGACode", drWard["LGAName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@LGACode", DBNull.Value);
                                                        cmd.Parameters.AddWithValue("@Code", drWard["WardCode"]);

                                                        if (!(drWard["WardName"] is DBNull) && drWard["WardName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@Name", drWard["WardName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@Name", DBNull.Value);
                                                        cmd.Parameters.AddWithValue("@Active", 'Y');
                                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                        cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                        result = cmd.ExecuteNonQuery() > 0;
                                                        if (!result)
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // venue
                                    DataTable dtVenue = new DataTable();

                                    using (SqlCommand sqlCommand = new SqlCommand(@"select distinct CDID, ReturningOfficeName,LGAId,LGAName,WardCode,WardName,  
                                        VenueCode, VenueName, VenueLongName, VenueAddress, VenueTypeCode,VenueTypeName from NSW_Consignment
                                        where CHARINDEX(@EventId,EventId)=1 and VenueCode is not null order by VenueCode", sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                                        {
                                            adapter.SelectCommand = sqlCommand;
                                            sqlCommand.Parameters.AddWithValue("@EventId", eventId);
                                            adapter.Fill(dtVenue);
                                        }
                                        if (dtVenue.Rows.Count > 0)
                                        {
                                            string sqlInsertVenue = @"INSERT INTO NSW_Venue ([EventId],[Code],[Name],[LongName],[ROCode],[ROName],[LGACode],[LGAName],[WardCode],[WardName],[VenueTypeCode],[VenueTypeName],[Active],[Created],[CreatedBy]) 
                                                VALUES (@EventId,@Code,@Name,@LongName,@ROCode,@ROName,@LGACode,@LGAName,@WardCode,@WardName,@VenueTypeCode,@VenueTypeName,@Active,@Created,@CreatedBy)";
                                            foreach (DataRow drVenue in dtVenue.Rows)
                                            {
                                                if (!(drVenue["VenueCode"] is DBNull) && drVenue["VenueCode"].ToString() != string.Empty)
                                                {
                                                    using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                                                    {
                                                        cmd.Parameters.AddWithValue("@EventId", eventId);
                                                        cmd.Parameters.AddWithValue("@Code", drVenue["VenueCode"]);
                                                        if (!(drVenue["VenueName"] is DBNull) && drVenue["VenueName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@Name", drVenue["VenueName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@Name", DBNull.Value);
                                                        cmd.Parameters.AddWithValue("@LongName", DBNull.Value);
                                                        if (!(drVenue["CDID"] is DBNull) && drVenue["CDID"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@ROCode", drVenue["CDID"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@ROCode", DBNull.Value);                                                        
                                                        if (!(drVenue["ReturningOfficeName"] is DBNull) && drVenue["ReturningOfficeName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@ROName", drVenue["ReturningOfficeName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                                        if (!(drVenue["LGAId"] is DBNull) && drVenue["LGAId"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@LGACode", drVenue["LGAId"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@LGACode", DBNull.Value);
                                                        if (!(drVenue["LGAName"] is DBNull) && drVenue["LGAName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@LGAName", drVenue["LGAName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@LGAName", DBNull.Value);

                                                        if (!(drVenue["WardCode"] is DBNull) && drVenue["WardCode"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@WardCode", drVenue["WardCode"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@WardCode", DBNull.Value);
                                                        if (!(drVenue["WardName"] is DBNull) && drVenue["WardName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@WardName", drVenue["WardName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@WardName", DBNull.Value);

                                                        if (!(drVenue["VenueTypeCode"] is DBNull) && drVenue["VenueTypeCode"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@VenueTypeCode", drVenue["VenueTypeCode"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@VenueTypeCode", DBNull.Value);
                                                        if (!(drVenue["VenueTypeName"] is DBNull) && drVenue["VenueTypeName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@VenueTypeName", drVenue["VenueTypeName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@VenueTypeName", DBNull.Value);

                                                        cmd.Parameters.AddWithValue("@Active", 'Y');
                                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                        cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                        result = cmd.ExecuteNonQuery() > 0;
                                                        if (!result)
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                               /*
                                if (result)
                                {
                                    // venue type
                                    DataTable dtVenueType = new DataTable();
                                    using (SqlCommand sqlCommand = new SqlCommand(@"select distinct VenueTypeCode, VenueTypeName from NSW_Consignment order by VenueTypeCode",
                                        sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                                        {
                                            adapter.SelectCommand = sqlCommand;
                                            adapter.Fill(dtVenueType);
                                        }
                                        string sqlInsertVenueType = @"INSERT INTO NSW_VenueType ([Code],[Name],[Active],[Created],[CreatedBy]) 
                                                VALUES (@Code,@Name,@Active,@Created,@CreatedBy)";
                                        if (dtVenueType.Rows.Count > 0)
                                        {   
                                            foreach (DataRow drVenueType in dtVenueType.Rows)
                                            {
                                                if (!(drVenueType["VenueTypeCode"] is DBNull) && drVenueType["VenueTypeCode"].ToString() != string.Empty)
                                                {
                                                    using (SqlCommand cmd = new SqlCommand(sqlInsertVenueType, sqlTrans.Connection, sqlTrans))
                                                    {
                                                        cmd.Parameters.AddWithValue("@Code", drVenueType["VenueTypeCode"]);
                                                        if (!(drVenueType["VenueTypeName"] is DBNull) && drVenueType["VenueTypeName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@Name", drVenueType["VenueTypeName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@Name", DBNull.Value);
                                                        cmd.Parameters.AddWithValue("@Active", 'Y');
                                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                        cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                        result = cmd.ExecuteNonQuery() > 0;
                                                        if (!result)
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (int i = 0; i < venueTypeList.Length; i++)
                                            {
                                                using (SqlCommand cmd = new SqlCommand(sqlInsertVenueType, sqlTrans.Connection, sqlTrans))
                                                {
                                                    cmd.Parameters.AddWithValue("@Code", (i+1).ToString().PadLeft(2, '0'));
                                                    cmd.Parameters.AddWithValue("@Name", venueTypeList[i]);
                                                    cmd.Parameters.AddWithValue("@Active", 'Y');
                                                    cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                    cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                    result = cmd.ExecuteNonQuery() > 0;
                                                    if (!result)
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                if (result)
                                {
                                    // contest
                                    DataTable dtContest = new DataTable();
                                    using (SqlCommand sqlCommand = new SqlCommand(@"select distinct ContestCode, ContestName from NSW_Consignment order by ContestCode",
                                        sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                                        {
                                            adapter.SelectCommand = sqlCommand;
                                            adapter.Fill(dtContest);
                                        }
                                        string sqlInsertContest = @"INSERT INTO NSW_Contest ([Code],[Name],[Active],[Created],[CreatedBy]) 
                                            VALUES (@Code,@Name,@Active,@Created,@CreatedBy)";
                                        if (dtContest.Rows.Count > 0)
                                        {                                                            
                                            foreach (DataRow drContest in dtContest.Rows)
                                            {
                                                if (!(drContest["ContestCode"] is DBNull) && drContest["ContestCode"].ToString() != string.Empty)
                                                {
                                                    using (SqlCommand cmd = new SqlCommand(sqlInsertContest, sqlTrans.Connection, sqlTrans))
                                                    {
                                                        cmd.Parameters.AddWithValue("@Code", drContest["ContestCode"]);
                                                        if (!(drContest["ContestName"] is DBNull) && drContest["ContestName"].ToString() != string.Empty)
                                                            cmd.Parameters.AddWithValue("@Name", drContest["ContestName"]);
                                                        else
                                                            cmd.Parameters.AddWithValue("@Name", DBNull.Value);
                                                        cmd.Parameters.AddWithValue("@Active", 'Y');
                                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                        cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                        result = cmd.ExecuteNonQuery() > 0;
                                                        if (!result)
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (int i = 0; i < contestList.Length; i++)
                                            {
                                                using (SqlCommand cmd = new SqlCommand(sqlInsertContest, sqlTrans.Connection, sqlTrans))
                                                {
                                                    cmd.Parameters.AddWithValue("@Code", (i+1).ToString().PadLeft(2, '0'));
                                                    cmd.Parameters.AddWithValue("@Name", contestList[i]);
                                                    cmd.Parameters.AddWithValue("@Active", 'Y');
                                                    cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                    cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                    result = cmd.ExecuteNonQuery() > 0;
                                                    if (!result)
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                if (result)
                                {
                                    // container type
                                    DataTable dtContainerType = new DataTable();
                                    using (SqlCommand sqlCommand = new SqlCommand(@"select * from NSW_ContainerType", sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                                        {
                                            adapter.SelectCommand = sqlCommand;
                                            adapter.Fill(dtContainerType);
                                        }
                                        if (dtContainerType.Rows.Count == 0)
                                        {
                                            string sqlInsertContest = @"INSERT INTO NSW_ContainerType ([Code],[Name],[Active],[Created],[CreatedBy]) 
                                                VALUES (@Code,@Name,@Active,@Created,@CreatedBy)";
                                            for (int i = 0; i < ContainerTypeList.Length; i++)
                                            {
                                                using (SqlCommand cmd = new SqlCommand(sqlInsertContest, sqlTrans.Connection, sqlTrans))
                                                {
                                                    cmd.Parameters.AddWithValue("@Code", (i + 1).ToString().PadLeft(2, '0'));
                                                    cmd.Parameters.AddWithValue("@Name", ContainerTypeList[i]);
                                                    cmd.Parameters.AddWithValue("@Active", 'Y');
                                                    cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                    cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                                    result = cmd.ExecuteNonQuery() > 0;
                                                    if (!result)
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                                 */
                            }
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in ImportConsignmentData: {0}", ex.Message);
                    }
                }
            }
            return result;
        }

        public static bool ImportCountCentreData(LoginUser loginUser, DataTable dtSrc, ref string procMsg)
        {
            bool result = false;
            using (SqlConnection sqlConn = new SqlConnection(connStr))
            {
                sqlConn.Open();
                SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                try
                {
                    string tableName = "NSW_CountCentre";
                    result =DeleteTableData(sqlTrans, tableName, ref procMsg);
                    if (result)
                    {
                        string[] tobeDistinct = { "EventID", "ReturningOffice", "CountLocation" };
                        DataTable dtUniqSrc = new DataTable();
                        dtUniqSrc = dtSrc.DefaultView.ToTable(true, tobeDistinct);
                        DataView dvUniqSrc = dtUniqSrc.DefaultView;
                        dvUniqSrc.Sort = "CountLocation";
                        dvUniqSrc.RowFilter = "ReturningOffice<>CountLocation";
                        DataTable dtUniqCC = new DataTable();
                        dtUniqCC = dvUniqSrc.ToTable(true, "CountLocation");

                        result = dtUniqCC.Rows.Count == 0;
                        if (!result)
                        {
                            string sqlStr = string.Format("SELECT * FROM {0} WHERE ID=-1", tableName);
                            int cCount = 0;
                            foreach (DataRow dr in dtUniqCC.Rows)
                            {
                                if (!(dr[0] is DBNull) && dr[0].ToString() != string.Empty)
                                {
                                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                    {
                                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                        {
                                            sqlDataAdapter.SelectCommand = cmd;
                                            using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                            {
                                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                                DataTable dtData = new DataTable();
                                                sqlDataAdapter.Fill(dtData);

                                                cCount++;
                                                DataRow drData = dtData.NewRow();
                                                drData["EventId"] = DBNull.Value;
                                                drData["Code"] = string.Format("C{0}", cCount);
                                                drData["Name"] = dr[0]; 
                                                drData["Active"] = 'Y';
                                                drData["Created"] = DateTime.Now;
                                                drData["CreatedBy"] = loginUser.UserName;

                                                dtData.Rows.Add(drData);
                                                result = sqlDataAdapter.Update(dtData) > 0;
                                                if (!result)
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (result)
                        sqlTrans.Commit();
                    else
                        sqlTrans.Rollback();
                }
                catch (Exception ex)
                {
                    result = false;
                    sqlTrans.Rollback();
                    procMsg = string.Format("Error in ImportCountCentreData: {0}", ex.Message);
                }
            }
            return result;
        }
        public static bool ImportReturningOfficeData(LoginUser loginUser, DataTable dtSrc, ref string procMsg)
        {
            bool result = false;// DeleteTableData(sqlTrans, "NSW_Contest", ref procMsg); dtSrc.Rows.Count == 0;
            
            using (SqlConnection sqlConn = new SqlConnection(connStr))
            {
                sqlConn.Open();
                SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                try
                {                    
                    string tableName = "NSW_ReturningOffice";

                    result = DeleteTableData(sqlTrans, tableName, ref procMsg);
                    if (result)
                    {
                        result = dtSrc.Rows.Count == 0;
                        if (!result)
                        {
                            string sqlStr = string.Format("SELECT * FROM {0} WHERE ID=-1", tableName);
                            foreach (DataRow drSource in dtSrc.Rows)
                            {
                                using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                {
                                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                    {
                                        sqlDataAdapter.SelectCommand = cmd;
                                        using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                        {
                                            sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                            sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                            sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                            sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                            DataTable dtData = new DataTable();
                                            sqlDataAdapter.Fill(dtData);

                                            int colIdx = -1;
                                            string colSrcName = string.Empty;
                                            DataRow drData = dtData.NewRow();
                                            foreach (DataColumn col in dtData.Columns)
                                            {
                                                if (col.ColumnName != "ID")
                                                {
                                                    if (col.ColumnName == "Created")
                                                        drData[col.ColumnName] = DateTime.Now;
                                                    else
                                                        if (col.ColumnName == "CreatedBy")
                                                            drData[col.ColumnName] = loginUser.UserName;
                                                        else
                                                        {
                                                            switch (loginUser.EventType)
                                                            {
                                                                case EventType.LOCAL_GOVERNMENT:
                                                                    break;
                                                                case EventType.STATE_BY_ELECTION:
                                                                case EventType.STATE_ELECTION:
                                                                    colIdx = Array.IndexOf(stateElectionRODBFieldList, col.ColumnName);
                                                                    if (colIdx > -1)
                                                                    {
                                                                        colSrcName = stateElectionROSourceFieldList[colIdx];
                                                                        if (dtSrc.Columns.IndexOf(colSrcName) > -1 && !(drSource[colSrcName] is DBNull) && drSource[colSrcName].ToString() != string.Empty)
                                                                        {
                                                                            if (col.ColumnName == "Code")
                                                                                drData[col.ColumnName] = drSource[colSrcName].ToString().PadLeft(3, '0');
                                                                            else
                                                                                drData[col.ColumnName] = drSource[colSrcName];
                                                                        }
                                                                    }
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                        }
                                                }
                                            }
                                            dtData.Rows.Add(drData);
                                            result = sqlDataAdapter.Update(dtData) > 0;
                                            if (!result)
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (result)
                        sqlTrans.Commit();
                    else
                        sqlTrans.Rollback();
                }
                catch (Exception ex)
                {
                    result = false;
                    sqlTrans.Rollback();
                    procMsg = string.Format("Error in ImportReturningOfficeData: {0}", ex.Message);
                }
            }
            
            return result;
        }

        public static bool DeleteTableData(string tableName, ref string procMsg)
        {
            bool result = false;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        string sql = string.Format("delete from {0}", tableName);
                        using (SqlCommand cmd = new SqlCommand(sql, sqlTrans.Connection, sqlTrans))
                        {
                            result = cmd.ExecuteNonQuery() >= 0;
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in DeleteTableData for table '{0}': {1}", tableName, ex.Message);
                        result = false;
                    }
                }
            }
            catch(Exception ex)
            {
                procMsg = string.Format("Error in DeleteTableData for table '{0}': {1}", tableName, ex.Message);
            }
            return result;
        }
        public static bool DeleteTableData(SqlTransaction sqlTrans, string tableName, ref string procMsg)
        {
            bool result = false;
            try
            {
                try
                {
                    string sql = string.Format("delete from {0}", tableName);
                    using (SqlCommand cmd = new SqlCommand(sql, sqlTrans.Connection, sqlTrans))
                    {
                        result = cmd.ExecuteNonQuery() >= 0;
                    }
                }
                catch (SqlException ex)
                {
                    sqlTrans.Rollback();
                    procMsg = string.Format("Error in DeleteTableData for table '{0}': {1}", tableName, ex.Message);
                    result = false;
                }
            }
            catch (Exception ex)
            {
                procMsg = string.Format("Error in DeleteTableData for table '{0}': {1}", tableName, ex.Message);
            }
            return result;
        }

        public static DataTable GetUserGridLayoutTable(ASPxGridView gridView)
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = "SELECT * FROM NSW_GridLayout " +
                    "WHERE CreatedBy=@CreatedBy and PageName=@PageName and GridName=@GridName " +
                    "order by GridName ";
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@CreatedBy", (gridView.Page.Session["EC_User"] as LoginUser).UserName);
                    sqlCommand.Parameters.AddWithValue("@PageName", gridView.Page.ID);
                    sqlCommand.Parameters.AddWithValue("@GridName", gridView.ID);

                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dt);
                    }
                }
            }
            return dt;
        }

        public static DataTable GetTableData(string tableName)
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    string.Format("SELECT * FROM {0} ORDER BY Code", tableName), sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetSGETableData(string tableName)
        {
            DataTable dt = new DataTable();
            dt.TableName = tableName;

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    string.Format("SELECT * FROM {0}", tableName), sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetTableData(SqlTransaction sqlTrans, string tableName)
        {
            DataTable dt = new DataTable();

            using (SqlCommand sqlCommand = new SqlCommand(
                    string.Format("SELECT * FROM {0} ORDER BY Code", tableName),sqlTrans.Connection, sqlTrans))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = sqlCommand;
                adapter.Fill(dt);
            }
            return dt;
        }
        public static DataTable GetSQLData(SqlTransaction sqlTrans, string sql)
        {
            DataTable dt = new DataTable();

            using (SqlCommand sqlCommand = new SqlCommand(sql, sqlTrans.Connection, sqlTrans))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = sqlCommand;
                adapter.Fill(dt);
            }
            return dt;
        }

        public static DataTable GetSQLData(string sql)
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetTableData(SqlTransaction sqlTrans, string tableName, string orderName)
        {
            DataTable dt = new DataTable();

            string sqlStr;
            if (!string.IsNullOrEmpty(orderName))
                sqlStr = string.Format("SELECT * FROM {0} ORDER BY {1}", tableName, orderName);
            else
                sqlStr = string.Format("SELECT * FROM {0}", tableName);

            using (SqlCommand sqlCommand = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = sqlCommand;
                adapter.Fill(dt);
            }
            return dt;
        }

        public static DataTable GetProcessingLocationData()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    "SELECT * FROM NSW_ReturningOffice WHERE LocationTypeCode=4 ORDER BY Code", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }
        public static DataTable GetProcessingLocationData(SqlTransaction sqlTrans)
        {
            DataTable dt = new DataTable();

            using (SqlCommand sqlCommand = new SqlCommand(
                    "SELECT * FROM NSW_ReturningOffice WHERE LocationTypeCode=4 ORDER BY Code", sqlTrans.Connection, sqlTrans))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = sqlCommand;
                adapter.Fill(dt);
            }
            return dt;
        }

        public static DataTable GetStatusStageData()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    "SELECT * FROM NSW_StatusStage ORDER BY StageId", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetVenueData()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    @"select ID, ROCode [EMO Code], ROName [EMO Name], LGACode [District Code], LGAName [District Name],
                        Code, Name, LongName, VenueTypeCode, VenueTypeName, Active from NSW_Venue", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetVenueDataExt()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    /*
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], '-1' [ROCode], '-1' [LGACode] UNION
                    SELECT distinct Code, Name + ' (' + Code + ')' Name, ISNULL(ROCode,'-1') ROCode, ISNULL(LGACode, '-1') LGACode FROM NSW_Venue ORDER BY Name",
                     
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], null [ShortName], null [LongName], null [VenueTypeCode], null [VenueTypeName], null [ROCode], null [LGACode] UNION
                    SELECT distinct Code, Name + ' (' + Code + ')' [Name], Name [ShortName], LongName, VenueTypeCode, VenueTypeName, ROCode, LGACode 
                    FROM NSW_Venue ORDER BY Name",
                     */
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], null [ShortName], null [LongName], null [VenueTypeCode], null [VenueTypeName], null [ROCode], null [LGACode], null [WardCode] 
                    UNION
                    SELECT distinct Code, Name + ' (' + Code + ')' [Name], Name [ShortName], LongName, VenueTypeCode, VenueTypeName, ROCode, LGACode, WardCode 
                    FROM NSW_Venue ORDER BY Name",
                    sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetVenueTypeDataExt()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name] UNION
                    SELECT distinct Code, Name + ' (' + Code + ')' Name FROM NSW_VenueType ORDER BY Name", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetBarcodeSourceDataExt()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name] UNION
                    SELECT distinct Code, Name + ' (' + Code + ')' Name FROM NSW_BarcodeSource ORDER BY Name", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetProductTypeDataExt()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], '[NOT DEFINED]' [ShortName] UNION
                    SELECT distinct Code, Name + ' (' + Code + ')' [Name], Name [ShortName] FROM NSW_Contest ORDER BY Name", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetContainerTypeDataExt()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], null [ShortName] UNION
                    SELECT distinct Code, Name + ' (' + Code + ')' [Name], Name [ShortName] FROM NSW_ContainerType ORDER BY Name", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetVenueData(SqlTransaction sqlTrans)
        {
            DataTable dt = new DataTable();

            using (SqlCommand sqlCommand = new SqlCommand(
                    @"select ROCode [EMO Code], ROName [EMO Name], LGACode [District Code], LGAName [District name],
                        Code, Name, VenueTypeCode, VenueTypeName, Active from NSW_Venue", sqlTrans.Connection, sqlTrans))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = sqlCommand;
                adapter.Fill(dt);
            }
            return dt;
        }

        public static DataTable GetEMOData()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    "SELECT * FROM NSW_ReturningOffice WHERE LocationTypeCode<>'4' ORDER BY Code", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }
        public static DataTable GetEMODataExt(bool all=false)
        {
            DataTable dt = new DataTable();
            string sql = @"SELECT '-1' [Code], '[NOT DEFINED]' [Name] UNION
                    SELECT Code, Name + ' (' + Code + ')' Name FROM NSW_ReturningOffice WHERE LocationTypeCode<>'4' ORDER BY Name";
            if (all)
                sql = @"SELECT '-1' [Code], '[NOT DEFINED]' [Name] UNION
                    SELECT Code, Name + ' (' + Code + ')' Name FROM NSW_ReturningOffice ORDER BY Name";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }
        public static DataTable GetEMOData(SqlTransaction sqlTrans)
        {
            DataTable dt = new DataTable();

            using (SqlCommand sqlCommand = new SqlCommand(
                    "SELECT * FROM NSW_ReturningOffice WHERE LocationTypeCode<>'4' ORDER BY Code", sqlTrans.Connection, sqlTrans))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = sqlCommand;
                adapter.Fill(dt);
            }
            return dt;
        }

        public static DataTable GetStatusStageDataExt()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    @"SELECT '-1' [StageId], '[NOT DEFINED]' [Stage] UNION
                    select CAST(StageId as varchar(6)), CAST(StageId as varchar(6)) + ' - ' + [Description] Stage 
                    from NSW_StatusStage where StageId>1", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetScannedLocationDataExt(object cdid=null)
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    /*
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], '[NOT DEFINED]' [Name1] UNION
                      SELECT R.Code, R.Name + '(' + R.Code + ')' [Name], R.Name [Name1] from NSW_ReturningOffice R
                      INNER JOIN NSW_VenueType T ON T.Code=R.LocationTypeCode
                      where (T.Name='EM Office' or T.Name='Returning Office') and (R.Code=@Code or @Code is null)
                      UNION
                      SELECT V.Code, V.Name + '(' + V.Code + ')' [Name], V.Name [Name1] FROM NSW_Venue V
                      INNER JOIN NSW_VenueType T ON T.Code=V.VenueTypeCode
                      WHERE T.Name='Processing Location'
                      UNION 
                      SELECT 'WH' [Code], 'Warehouse' [Name], 'Warehouse' [Name1]", 
                     */
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], '[NOT DEFINED]' [Name1] UNION
                    SELECT R.Code, R.Name + '(' + R.Code + ')' [Name], R.Name [Name1] from NSW_ReturningOffice R
                    INNER JOIN NSW_VenueType T ON T.Code=R.LocationTypeCode
                    where (T.Name='EM Office' or T.Name='Returning Office') and (R.Code=@Code or @Code is null)
                    UNION
                    SELECT R.Code, R.Name + '(' + R.Code + ')' [Name], R.Name [Name1] from NSW_ReturningOffice R
                    where R.LocationTypeCode<>'3'
                    UNION 
                    SELECT 'WH' [Code], 'Warehouse' [Name], 'Warehouse' [Name1]",
                    sqlConnection))
                {
                    if (cdid != null && cdid.ToString() != string.Empty)
                        sqlCommand.Parameters.AddWithValue("@Code", cdid);
                    else
                        sqlCommand.Parameters.AddWithValue("@Code", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dt);
                    }
                }
            }
            return dt;
        }

        public static DataTable GetContestAreaDataExt()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], '-1' [ROCode] UNION
                      SELECT Code, AreaCode + ' (' + Code + ')' Name, ROCode FROM NSW_LGA ORDER BY Name", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetWardDataExt()
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    @"SELECT '-1' [Code], '[NOT DEFINED]' [Name], '-1' [ROCode], '-1' [LGAId] UNION
                        SELECT Code, Name + ' (' + Code + ')' Name, ROCode, LGAId FROM NSW_Ward ORDER BY Name", sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }
        
        public static DataTable GetROTableData()
        {
            DataTable dt = new DataTable();
            string sql = "select Code, Name from NSW_ReturningOffice where Active='Y' union select '[None]','Not Defined'";
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }
        public static bool UpdateTableRecordById(string tableName, DataRow dr, bool isSlave, ref string message, bool isDelete, string idNames=null)
        {
            bool result = false;

            string dbConnStr = connStr;
            if (isSlave)
            {
                object slaveConnStrObj = ConfigurationManager.ConnectionStrings["SlaveDBConString"];
                result = slaveConnStrObj == null || string.IsNullOrEmpty(slaveConnStrObj.ToString());
                if (!result)
                    dbConnStr = slaveConnStrObj.ToString();
            }
            if (!result)
            {
                using (SqlConnection sqlConn = new SqlConnection(dbConnStr))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                        try
                        {
                            string sqlStr = string.Empty; string[] keys = null;
                            if (!string.IsNullOrEmpty(idNames))
                            {
                                keys = idNames.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                                if (keys.Length > 0)
                                {
                                    int idx = 0;
                                    sqlStr = string.Format("SELECT * FROM {0} WHERE [{1}]=@{1} ", tableName, keys[idx]);
                                    idx++;
                                    while (idx < keys.Length)
                                    {
                                        sqlStr += string.Format("AND [{0}]=@{0}", keys[idx]);
                                        idx++;
                                    }
                                }
                            }
                            else
                                sqlStr = string.Format("SELECT * FROM {0} WHERE ID=@ID", tableName);
                            using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                if (keys != null && keys.Length > 0)
                                {
                                    int idx = 0;
                                    cmd.Parameters.AddWithValue(string.Format("@{0}", keys[idx]), dr[keys[idx]]);
                                    idx++;
                                    while (idx < keys.Length)
                                    {
                                        cmd.Parameters.AddWithValue(string.Format("@{0}", keys[idx]), dr[keys[idx]]);
                                        idx++;
                                    }
                                }
                                else
                                    cmd.Parameters.AddWithValue("@ID", dr["ID"]);
                                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                {
                                    sqlDataAdapter.SelectCommand = cmd;
                                    using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                    {
                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                        DataTable dtData = new DataTable();
                                        sqlDataAdapter.Fill(dtData);

                                        DataRow drData = null;
                                        bool isNew = dtData.Rows.Count == 0;
                                        if (!isNew)
                                            drData = dtData.Rows[0];
                                        else
                                            drData = dtData.NewRow();
                                        if (isDelete)
                                            drData.Delete();
                                        else
                                        {
                                            foreach (DataColumn col in dtData.Columns)
                                            {                                                
                                                if (col.ColumnName != "ID" || (col.ColumnName == "ID" && col.DataType != typeof(int)))
                                                    drData[col.ColumnName] = dr[col.ColumnName];
                                            }

                                            if (isNew)
                                                dtData.Rows.Add(drData);
                                        }
                                        result = sqlDataAdapter.Update(dtData) > 0;
                                    }
                                }
                            }
                            if (result)
                            {
                                sqlTrans.Commit();
                                if (!isSlave)
                                    //UpdateTableRecordById(tableName, dr, !isSlave, ref message, isDelete);
                                    UpdateSlaveTableRecordById(tableName, dr, isDelete, idNames);
                            }
                            else
                                sqlTrans.Rollback();
                        }
                        catch (SqlException ex)
                        {
                            sqlTrans.Rollback();
                            message = string.Format("Error in UpdateTableRecordById for table '{0}': {1}", tableName, ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("Error in UpdateTableRecordById for table '{0}': {1}", tableName, ex);
                    }
                } // end of using
            }
            return result;
        }

        private static DBTransaction GetDBTransaction(SqlCommand cmd, ref int seq)
        {
            seq++;
            DBTransaction dbTrans = new DBTransaction()
            {
                SQLSeq = seq,
                SQLStatement = cmd.CommandText.Replace(Environment.NewLine, " ")
            };
            if (cmd.Parameters != null && cmd.Parameters.Count > 0)
            {
                dbTrans.SQLParams = new DBTransactionParam[cmd.Parameters.Count];
                for (int i = 0; i < cmd.Parameters.Count; i++)
                {
                    dbTrans.SQLParams[i] = new DBTransactionParam()
                    {
                        ParamName = cmd.Parameters[i].ParameterName,
                        ParamType = cmd.Parameters[i].DbType
                    };

                    if (cmd.Parameters[i].DbType == DbType.DateTime || cmd.Parameters[i].DbType == DbType.DateTime2)
                    {
                        if (cmd.Parameters[i].Value != null && !string.IsNullOrEmpty(cmd.Parameters[i].Value.ToString()))
                            dbTrans.SQLParams[i].ParamValue = Convert.ToDateTime(cmd.Parameters[i].Value).ToString(dateTimeFormat);
                        else
                            dbTrans.SQLParams[i].ParamValue = null;
                    }
                    else
                        dbTrans.SQLParams[i].ParamValue = cmd.Parameters[i].Value;
                }
            }
            return dbTrans;
        }
        public static bool UpdateSlaveTableRecordById(string tableName, DataRow dr, bool isDelete, string idNames = null)
        {
            object slaveConnStrObj = ConfigurationManager.ConnectionStrings["SlaveDBConString"];
            bool result = slaveConnStrObj == null || string.IsNullOrEmpty(slaveConnStrObj.ToString());
            if (!result)
            {
                string sConnStr = slaveConnStrObj.ToString();
                string sqlStr = string.Empty; string[] keys = null;
                if (!string.IsNullOrEmpty(idNames))
                {
                    keys = idNames.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (keys.Length > 0)
                    {
                        int idx = 0;
                        sqlStr = string.Format("SELECT * FROM {0} WHERE [{1}]=@{1} ", tableName, keys[idx]);
                        idx++;
                        while (idx < keys.Length)
                        {
                            sqlStr += string.Format("AND [{0}]=@{0}", keys[idx]);
                            idx++;
                        }
                    }
                }
                else
                    sqlStr = string.Format("SELECT * FROM {0} WHERE ID=@ID", tableName);
                DataTable dt = new DataTable();
                using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConn))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    if (keys != null && keys.Length > 0)
                    {
                        int idx = 0;
                        cmd.Parameters.AddWithValue(string.Format("@{0}", keys[idx]), dr[keys[idx]]);
                        idx++;
                        while (idx < keys.Length)
                        {
                            cmd.Parameters.AddWithValue(string.Format("@{0}", keys[idx]), dr[keys[idx]]);
                            idx++;
                        }
                    }
                    else
                        cmd.Parameters.AddWithValue("@ID", dr["ID"]);
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dt);
                }
                //string sqlStr = string.Empty;
                int transSeq = 0;
                List<DBTransaction> dbTransList = new List<DBTransaction>();
                if (dt.Rows.Count > 0)
                {
                    if (isDelete)
                    {
                        if (keys != null && keys.Length > 0)
                        {
                            int idx = 0;
                            sqlStr = string.Format("DELETE FROM {0} WHERE [{1}]=@{1} ", tableName, keys[idx]);
                            idx++;
                            while (idx < keys.Length)
                            {
                                sqlStr += string.Format("AND [{0}]=@{0}", keys[idx]);
                                idx++;
                            }
                        }
                        else
                            sqlStr = string.Format("DELETE FROM {0} WHERE ID=@ID", tableName);
                        using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConn))
                        {
                            if (keys != null && keys.Length > 0)
                            {
                                int idx = 0;
                                cmd.Parameters.AddWithValue(string.Format("@{0}", keys[idx]), dr[keys[idx]]);
                                idx++;
                                while (idx < keys.Length)
                                {
                                    cmd.Parameters.AddWithValue(string.Format("@{0}", keys[idx]), dr[keys[idx]]);
                                    idx++;
                                }
                            }
                            else
                                cmd.Parameters.AddWithValue("@ID", dr["ID"]);
                            dbTransList.Add(GetDBTransaction(cmd, ref transSeq));
                        }
                    }
                    else
                    {
                        sqlStr = string.Format("UPDATE {0} SET ", tableName);
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            if (dt.Columns[i].ColumnName == "ID")
                                continue;
                            if (i == dt.Columns.Count -1)
                                sqlStr += string.Format("[{0}]=@{0}", dt.Columns[i].ColumnName);
                            else
                                sqlStr += string.Format("[{0}]=@{0},", dt.Columns[i].ColumnName);
                        }
                        if (keys != null && keys.Length > 0)
                        {
                            int idx = 0;
                            sqlStr += string.Format(" WHERE [{0}]=@{0} ", keys[idx]);
                            idx++;
                            while (idx < keys.Length)
                            {
                                sqlStr += string.Format("AND [{0}]=@{0}", keys[idx]);
                                idx++;
                            }
                        }
                        else
                            sqlStr += " WHERE ID=@ID";
                        using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConn))
                        {
                            for (int i = 0; i < dt.Columns.Count; i++)
                                cmd.Parameters.AddWithValue(string.Format("@{0}", dt.Columns[i].ColumnName), dr[dt.Columns[i].ColumnName]);
                            dbTransList.Add(GetDBTransaction(cmd, ref transSeq));
                        }
                    }
                }
                else
                {
                    string fStr = string.Empty;
                    string pStr = string.Empty;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (i == dt.Columns.Count - 1)
                        {
                            fStr += string.Format("[{0}]", dt.Columns[i].ColumnName);
                            pStr += string.Format("@{0}", dt.Columns[i].ColumnName);
                        }
                        else
                        {
                            fStr += string.Format("[{0}],", dt.Columns[i].ColumnName);
                            pStr += string.Format("@{0},", dt.Columns[i].ColumnName);
                        }
                    }
                    sqlStr = string.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableName, fStr, pStr);
                    using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConn))
                    {
                        for (int i = 0; i < dt.Columns.Count; i++)
                            cmd.Parameters.AddWithValue(string.Format("@{0}", dt.Columns[i].ColumnName), dr[dt.Columns[i].ColumnName]);
                        dbTransList.Add(GetDBTransaction(cmd, ref transSeq));
                    }
                }
                result = dbTransList.Count > 0;
                if (result)
                {
                    var writeTransFile = ConfigurationManager.AppSettings["WebServSourceFolder"];
                    result = writeTransFile != null && !string.IsNullOrEmpty(writeTransFile) && Directory.Exists(writeTransFile);
                    if (result)
                    {
                        DBUpdate dbUpdate = new DBUpdate()
                        {
                            ID = Guid.NewGuid().ToString().Replace("-", string.Empty).ToUpper(),
                            DBTransactions = dbTransList.ToArray()
                        };
                        var json = JsonConvert.SerializeObject(dbUpdate);
                        File.WriteAllText(string.Format(@"{0}\DBUpdate_{1}_{2}.json", writeTransFile, DateTime.Now.ToString("yyyyMMddHHmmssfff"), dbUpdate.ID), json);
                    }
                }
                
            }
            return result;
        }

        public static bool UpdateSlaveContainerConnote(DataRow drContainer)
        {
            object slaveConnStrObj = ConfigurationManager.ConnectionStrings["SlaveDBConString"];
            bool result = slaveConnStrObj == null || string.IsNullOrEmpty(slaveConnStrObj.ToString());
            if (!result)
            {
                string sConnStr = slaveConnStrObj.ToString();
                DataTable dt = new DataTable();
                using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM NSW_Container WHERE [ConsKey]=@ConsKey AND [LineNo]=@LineNo", sqlConn))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    cmd.Parameters.AddWithValue("@ConsKey", drContainer["ConsKey"]);
                    cmd.Parameters.AddWithValue("@LineNo", drContainer["LineNo"]);
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dt);
                }
                result = dt.Rows.Count > 0;
                if (result)
                {
                    int transSeq = 0;
                    List<DBTransaction> dbTransList = new List<DBTransaction>();
                    string sqlStr = "UPDATE NSW_Container SET ";
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dt.Columns[i].ColumnName == "ConsKey" || dt.Columns[i].ColumnName == "LineNo")
                            continue;
                        if (i == dt.Columns.Count - 1)
                            sqlStr += string.Format("{0}=@{0}", dt.Columns[i].ColumnName);
                        else
                            sqlStr += string.Format("{0}=@{0},", dt.Columns[i].ColumnName);
                    }
                    sqlStr += " WHERE [ConsKey]=@ConsKey AND [LineNo]=@LineNo";
                    using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConn))
                    {
                        for (int i = 0; i < dt.Columns.Count; i++)
                            cmd.Parameters.AddWithValue(string.Format("@{0}", dt.Columns[i].ColumnName), drContainer[dt.Columns[i].ColumnName]);
                        dbTransList.Add(GetDBTransaction(cmd, ref transSeq));
                    }
                    result = dbTransList.Count > 0;
                    if (result)
                    {
                        var writeTransFile = ConfigurationManager.AppSettings["WebServSourceFolder"];
                        result = writeTransFile != null && !string.IsNullOrEmpty(writeTransFile) && Directory.Exists(writeTransFile);
                        if (result)
                        {
                            DBUpdate dbUpdate = new DBUpdate()
                            {
                                ID = Guid.NewGuid().ToString().Replace("-", string.Empty).ToUpper(),
                                DBTransactions = dbTransList.ToArray()
                            };
                            var json = JsonConvert.SerializeObject(dbUpdate);
                            File.WriteAllText(string.Format(@"{0}\DBUpdate_{1}_{2}.json", writeTransFile, DateTime.Now.ToString("yyyyMMddHHmmssfff"), dbUpdate.ID), json);
                        }
                    }
                }
            }
            return result;
        }
        public static bool UpdateContainerConnote(DataRow drContainer, ref string message)
        {
            bool result = false;
            bool isNew = false;
            string dbConnStr = connStr;
                        
            using (SqlConnection sqlConn = new SqlConnection(dbConnStr))
            {
                try
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        string sqlStr = "SELECT * FROM NSW_Container WHERE [ConsKey]=@ConsKey AND [LineNo]=@LineNo";
                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            cmd.Parameters.AddWithValue("@ConsKey", drContainer["ConsKey"]);
                            cmd.Parameters.AddWithValue("@LineNo", drContainer["LineNo"]);
                            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                {
                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                    DataTable dtData = new DataTable();
                                    sqlDataAdapter.Fill(dtData);

                                    DataRow drData = null;
                                    isNew = dtData.Rows.Count == 0;
                                    if (!isNew)
                                        drData = dtData.Rows[0];
                                    else
                                        drData = dtData.NewRow();

                                    foreach (DataColumn col in dtData.Columns)
                                    {
                                        drData[col.ColumnName] = drContainer[col.ColumnName];
                                    }

                                    if (isNew)
                                        dtData.Rows.Add(drData);

                                    result = sqlDataAdapter.Update(dtData) > 0;
                                }
                            }
                        }
                        if (result && isNew)
                        {
                            sqlStr = @"update NSW_Connote set AdditionalContainerQuantity=isnull(AdditionalContainerQuantity,0)+1,
                                Modified=@Modified, ModifiedBy=@ModifiedBy WHERE ConsKey=@ConsKey";
                            using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ConsKey", drContainer["ConsKey"]);
                                cmd.Parameters.AddWithValue("@Modified", drContainer["Created"]);
                                cmd.Parameters.AddWithValue("@ModifiedBy", drContainer["CreatedBy"]);
                                result = cmd.ExecuteNonQuery() > 0;
                            }
                        }
                        if (result)
                        {
                            sqlTrans.Commit();
                            UpdateSlaveContainerConnote(drContainer);
                        }
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        message = ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            } // end of using
            
            return result;
        }

        public static bool UpdateSlaveContainerStatus(DataRow drStatus)
        {
            object slaveConnStrObj = ConfigurationManager.ConnectionStrings["SlaveDBConString"];
            bool result = slaveConnStrObj == null || string.IsNullOrEmpty(slaveConnStrObj.ToString());
            if (!result)
            {
                string sConnStr = slaveConnStrObj.ToString();
                DataTable dt = new DataTable();
                using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                //using (SqlCommand cmd = new SqlCommand("SELECT * FROM NSW_ContainerStatus WHERE [ConsKey]=@ConsKey AND [LineNo]=@LineNo AND [StatusDate]=@StatusDate AND [StatusTime]=@StatusTime", sqlConn))
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM NSW_ContainerStatus WHERE [ConsKey]=@ConsKey AND [LineNo]=@LineNo AND [StatusId]=@StatusId", sqlConn))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    cmd.Parameters.AddWithValue("@ConsKey", drStatus["ConsKey"]);
                    cmd.Parameters.AddWithValue("@LineNo", drStatus["LineNo"]);
                    cmd.Parameters.AddWithValue("@StatusId", drStatus["StatusId"]);
                    //cmd.Parameters.AddWithValue("@StatusDate", drStatus["StatusDate"]);
                    //cmd.Parameters.AddWithValue("@StatusTime", drStatus["StatusTime"]);
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dt);
                }
                string sqlStr = string.Empty;
                int transSeq = 0;
                List<DBTransaction> dbTransList = new List<DBTransaction>();
                if (dt.Rows.Count > 0)
                {
                    sqlStr = "UPDATE NSW_ContainerStatus SET ";
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dt.Columns[i].ColumnName == "ConsKey" || dt.Columns[i].ColumnName == "LineNo" || dt.Columns[i].ColumnName == "StatusId")// ||
                            //dt.Columns[i].ColumnName == "StatusDate" || dt.Columns[i].ColumnName == "StatusTime")
                            continue;
                        if (i == dt.Columns.Count - 1)
                            sqlStr += string.Format("{0}=@{0}", dt.Columns[i].ColumnName);
                        else
                            sqlStr += string.Format("{0}=@{0},", dt.Columns[i].ColumnName);
                    }
                    sqlStr += " WHERE [ConsKey]=@ConsKey AND [LineNo]=@LineNo AND [StatusId]=@StatusId";
                    using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConn))
                    {
                        for (int i = 0; i < dt.Columns.Count; i++)
                            cmd.Parameters.AddWithValue(string.Format("@{0}", dt.Columns[i].ColumnName), drStatus[dt.Columns[i].ColumnName]);
                        dbTransList.Add(GetDBTransaction(cmd, ref transSeq));
                    }                    
                }
                else
                {
                    string fStr = string.Empty;
                    string pStr = string.Empty;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (i == dt.Columns.Count - 1)
                        {
                            fStr += string.Format("[{0}]", dt.Columns[i].ColumnName);
                            pStr += string.Format("@{0}", dt.Columns[i].ColumnName);
                        }
                        else
                        {
                            fStr += string.Format("[{0}],", dt.Columns[i].ColumnName);
                            pStr += string.Format("@{0},", dt.Columns[i].ColumnName);
                        }
                    }
                    sqlStr = string.Format("INSERT INTO NSW_ContainerStatus ({0}) VALUES ({1})", fStr, pStr);
                    using (SqlConnection sqlConn = new SqlConnection(sConnStr))
                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConn))
                    {
                        for (int i = 0; i < dt.Columns.Count; i++)
                            cmd.Parameters.AddWithValue(string.Format("@{0}", dt.Columns[i].ColumnName), drStatus[dt.Columns[i].ColumnName]);
                        dbTransList.Add(GetDBTransaction(cmd, ref transSeq));
                    }
                }
                result = dbTransList.Count > 0;
                if (result)
                {
                    var writeTransFile = ConfigurationManager.AppSettings["WebServSourceFolder"];
                    result = writeTransFile != null && !string.IsNullOrEmpty(writeTransFile) && Directory.Exists(writeTransFile);
                    if (result)
                    {
                        DBUpdate dbUpdate = new DBUpdate()
                        {
                            ID = Guid.NewGuid().ToString().Replace("-", string.Empty).ToUpper(),
                            DBTransactions = dbTransList.ToArray()
                        };
                        var json = JsonConvert.SerializeObject(dbUpdate);
                        File.WriteAllText(string.Format(@"{0}\DBUpdate_{1}_{2}.json", writeTransFile, DateTime.Now.ToString("yyyyMMddHHmmssfff"), dbUpdate.ID), json);
                    }
                }
            }
            return result;
        }
        public static bool UpdateContainerStatus(DataRow drStatus, ref string message)
        {
            bool result = false;
            bool isNew = false;
            string dbConnStr = connStr;
                        
            using (SqlConnection sqlConn = new SqlConnection(dbConnStr))
            {
                try
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        //string sqlStr = "SELECT * FROM NSW_ContainerStatus WHERE [ConsKey]=@ConsKey AND [LineNo]=@LineNo AND [StatusDate]=@StatusDate AND [StatusTime]=@StatusTime";
                        string sqlStr = "SELECT * FROM NSW_ContainerStatus WHERE [ConsKey]=@ConsKey AND [LineNo]=@LineNo AND [StatusId]=@StatusId";
                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            cmd.Parameters.AddWithValue("@ConsKey", drStatus["ConsKey"]);
                            cmd.Parameters.AddWithValue("@LineNo", drStatus["LineNo"]);
                            cmd.Parameters.AddWithValue("@StatusId", drStatus["StatusId"]);
                            //cmd.Parameters.AddWithValue("@StatusDate", drStatus["StatusDate"]);
                            //cmd.Parameters.AddWithValue("@StatusTime", drStatus["StatusTime"]);
                            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                {
                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                    DataTable dtData = new DataTable();
                                    sqlDataAdapter.Fill(dtData);

                                    DataRow drData = null;
                                    isNew = dtData.Rows.Count == 0;
                                    if (isNew)
                                        drData = dtData.NewRow();
                                    else
                                        drData = dtData.Rows[0];
                                    foreach (DataColumn col in dtData.Columns)
                                    {
                                        drData[col.ColumnName] = drStatus[col.ColumnName];
                                    }
                                    if (isNew)
                                        dtData.Rows.Add(drData);

                                    result = sqlDataAdapter.Update(dtData) > 0;
                                }
                            }
                        }

                        if (result)
                        {
                            sqlTrans.Commit();
                            UpdateSlaveContainerStatus(drStatus);
                        }
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        message = ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            } // end of using
            
            return result;
        }

        public static DataTable GetConnoteDataTable(object consKey)
        {
            DataTable dtConnote = new DataTable();
            string sqlStr = "SELECT * FROM NSW_Connote WHERE ConsKey=@ConsKey";
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConnection))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                cmd.Parameters.AddWithValue("@ConsKey", consKey);
                adapter.SelectCommand = cmd;
                adapter.Fill(dtConnote);
            }
            return dtConnote;
        }
        public static DataTable GetConnoteContainerDataTable(object consKey)
        {
            DataTable dtConnote = new DataTable();
            string sqlStr = "SELECT * FROM NSW_Container WHERE ConsKey=@ConsKey";
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConnection))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                cmd.Parameters.AddWithValue("@ConsKey", consKey);
                adapter.SelectCommand = cmd;
                adapter.Fill(dtConnote);
            }
            return dtConnote;
        }
        public static DataTable GetConnoteContainerStatusDataTable(object consKey)
        {
            DataTable dtConnote = new DataTable();
            string sqlStr = "SELECT * FROM NSW_ContainerStatus WHERE ConsKey=@ConsKey";
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            using (SqlCommand cmd = new SqlCommand(sqlStr, sqlConnection))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                cmd.Parameters.AddWithValue("@ConsKey", consKey);
                adapter.SelectCommand = cmd;
                adapter.Fill(dtConnote);
            }
            return dtConnote;
        }
        public static DataTable GetLabelLayoutDef(object labelId)
        {
            DataTable dtLayout = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = "SELECT * FROM SM_ONLINE_LABEL WHERE ID=@ID OR @ID IS NULL ORDER BY LABEL_NAME ";

                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (labelId != null)
                        sqlCommand.Parameters.AddWithValue("@ID", labelId);
                    else
                        sqlCommand.Parameters.AddWithValue("@ID", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLayout);
                    }
                }
            }
            return dtLayout;
        }
        public static DataTable GetConnoteLabelData(string spName, object barcode)
        {
            DataTable dtLabel = new DataTable();
            dtLabel.TableName = "ConnoteLabelTable";

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String SelectCommand = String.Format("EXEC {0} '{1}'", spName, barcode);
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLabel);
                    }
                }
            }
            return dtLabel;
        }
        public static bool UpdateGridViewLayout(ASPxGridView gridView, ref int layoutId, object layoutName, object defaultChar, bool isDelete)
        {
            bool result = false;
            LoginUser loginUser = gridView.Page.Session["EC_User"] as LoginUser;
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                String sql;
                if (layoutId == -1) // new
                {   // the table primary key field is an auto-increment Identity 
                    // need to get the newly generated id returned
                    sql = "INSERT INTO NSW_GridLayout (PageName,GridName,LayoutName,Layout,RowsPerPage,DefaultLayout,Created,CreatedBy) " +
                        "VALUES (@PageName,@GridName,@LayoutName,@Layout,@RowsPerPage,@DefaultLayout,@Created,@CreatedBy); " +
                        "SELECT CAST(scope_identity() AS int)";
                }
                else //edit or delete
                    sql = "SELECT * FROM NSW_GridLayout WHERE ID = @ID";

                try
                {
                    sqlConnection.Open();
                    SqlTransaction sqlTransaction = sqlConnection.BeginTransaction();
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand(sql, sqlTransaction.Connection, sqlTransaction))
                        {

                            if (layoutId == -1)// new
                            {
                                cmd.Parameters.AddWithValue("@PageName", gridView.Page.ID);
                                cmd.Parameters.AddWithValue("@GridName", gridView.ID);
                                cmd.Parameters.AddWithValue("@LayoutName", layoutName);
                                cmd.Parameters.AddWithValue("@Layout", gridView.SaveClientLayout());
                                cmd.Parameters.AddWithValue("@RowsPerPage", gridView.SettingsPager.PageSize);
                                cmd.Parameters.AddWithValue("@DefaultLayout", Convert.ToChar(defaultChar));
                                DateTime created = DateTime.Now;
                                cmd.Parameters.AddWithValue("@Created", created.Subtract(new TimeSpan(0, 0, 0, 0, created.Millisecond)));
                                cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                // insert new record and get the new identity
                                layoutId = (Int32)cmd.ExecuteScalar();
                                result = layoutId > -1; // new id returned
                            }
                            else// edit
                            {
                                cmd.Parameters.AddWithValue("@ID", layoutId);

                                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                {
                                    sqlDataAdapter.SelectCommand = cmd;
                                    using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                    {
                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTransaction;
                                        sqlDataAdapter.InsertCommand.Transaction = sqlTransaction;
                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTransaction;

                                        DataTable dataTable = new DataTable();
                                        sqlDataAdapter.Fill(dataTable);
                                        if (dataTable.Rows.Count > 0)
                                        {
                                            DataRow dr = dataTable.Rows[0];
                                            if (isDelete)
                                                dr.Delete();
                                            else
                                            {
                                                dr["PageName"] = gridView.Page.ID;
                                                dr["GridName"] = gridView.ID;
                                                dr["LayoutName"] = layoutName;
                                                dr["Layout"] = gridView.SaveClientLayout();
                                                dr["RowsPerPage"] = gridView.SettingsPager.PageSize;
                                                dr["DefaultLayout"] = defaultChar;
                                                DateTime modified = DateTime.Now;
                                                dr["Modified"] = modified.Subtract(new TimeSpan(0, 0, 0, 0, modified.Millisecond));
                                                dr["ModifiedBy"] = loginUser.UserName;
                                            }
                                            result = sqlDataAdapter.Update(dataTable) > 0;
                                        }
                                    }
                                }
                            }
                        }
                        if (result)
                            sqlTransaction.Commit();
                        else
                            sqlTransaction.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTransaction.Rollback();
                        String eMessage = ex.ToString();
                        result = false;
                    }
                }
                catch (Exception ex)
                {
                    String eMessage = ex.ToString();
                }
            } // end of using
            return result;
        }

        public static void ClearValueFor(Control ctr)
        {
            if (ctr is ASPxPageControl)
            {
                ASPxPageControl pControl = ctr as ASPxPageControl;
                foreach (TabPage tabPage in pControl.TabPages)
                {
                    foreach (Control ctrl in tabPage.Controls)
                        ClearValueFor(ctrl);
                }
            }
            else
                if (ctr is ASPxComboBox)
                {
                    (ctr as ASPxComboBox).Value = null;
                    (ctr as ASPxComboBox).Text = string.Empty;
                }
                else
                    if (ctr is ASPxTextBox)
                    {
                        (ctr as ASPxTextBox).Value = null;
                        (ctr as ASPxTextBox).Text = string.Empty;
                    }
                    else
                        if (ctr is ASPxMemo)
                        {
                            (ctr as ASPxMemo).Value = null;
                            (ctr as ASPxMemo).Text = string.Empty;
                        }
                        else
                            if (ctr is ASPxDateEdit)
                            {
                                (ctr as ASPxDateEdit).Value = DateTime.Now;
                            }
                            else
                                if (ctr is ASPxCheckBox)
                                {
                                    (ctr as ASPxCheckBox).Checked = false;
                                }
                                else
                                    if (ctr is ASPxSpinEdit)
                                    {
                                        (ctr as ASPxSpinEdit).Value = 0;
                                    }
                                    else
                                        if (ctr is ASPxDropDownEdit)
                                        {
                                            (ctr as ASPxDropDownEdit).Text = string.Empty;
                                        }
                                        else
                                            if (ctr is ASPxHtmlEditor)
                                            {
                                                ASPxHtmlEditor htmlEditor = (ctr as ASPxHtmlEditor);
                                                htmlEditor.Html = string.Empty;
                                            }
                                            else
                                                if (ctr is ASPxRadioButtonList)
                                                    (ctr as ASPxRadioButtonList).SelectedIndex = 0;
                                                else
                                                    if (ctr is ASPxButtonEdit)
                                                    {
                                                        (ctr as ASPxButtonEdit).Value = string.Empty;
                                                    }
                                                    else
                                                        if (ctr.HasControls())
                                                            ClearValueForChildControls(ctr);
        }
        private static void ClearValueForChildControls(Control parentControl)
        {
            foreach (Control childControl in parentControl.Controls)
                ClearValueFor(childControl);
        }

        public static decimal StrToDecimalDef(string s, decimal @default)
        {
            decimal number;
            if (decimal.TryParse(s, out number))
                return number;
            return @default;
        }
        public static double StrToDoubleDef(string s, double @default)
        {
            double number;
            if (double.TryParse(s, out number))
                return number;
            return @default;
        }
        public static int StrToIntDef(string s, int @default)
        {
            int number;
            if (int.TryParse(s, out number))
                return number;
            return @default;
        }
        public static Int64 StrToInt64Def(string s, Int64 @default)
        {
            Int64 number;
            if (Int64.TryParse(s, out number))
                return number;
            return @default;
        }

        public static void GridViewExport(ASPxGridView gridView, string exportFileType)
        {
            ASPxGridViewExporter gvExporter = gridView.Page.Form.FindControl("gvExporter") as ASPxGridViewExporter;
            if (gvExporter == null)
            {
                gvExporter = new ASPxGridViewExporter();
                gridView.Page.Form.Controls.Add(gvExporter);
            }
            gvExporter.GridViewID = gridView.ID;
            gvExporter.Landscape = true;

            switch (exportFileType)
            {
                case "CSV":
                    gvExporter.WriteCsvToResponse();
                    break;
                case "PDF":
                    gvExporter.WritePdfToResponse();
                    break;
                case "RTF":
                    gvExporter.WriteRtfToResponse();
                    break;
                case "XLS":
                    gvExporter.WriteXlsToResponse();
                    break;
                case "XLSX":
                    gvExporter.WriteXlsxToResponse();
                    break;
                default:
                    gvExporter.WritePdfToResponse();
                    break;
            }

        }

        public static void SetGridViewStyle(ASPxGridView gridView)
        {
            gridView.Styles.Header.ForeColor = Color.White;
            gridView.Styles.Header.BackColor = sfColor;
            gridView.Styles.AlternatingRow.BackColor = sfLightColor1;

            gridView.Styles.RowHotTrack.BackColor = sfLightColor;
            gridView.Styles.RowHotTrack.ForeColor = Color.White;

            gridView.Styles.FocusedGroupRow.BackColor = sfColor;
            gridView.Styles.FocusedGroupRow.ForeColor = Color.White;

            gridView.Styles.FocusedRow.BackColor = sfColor;
            gridView.Styles.FocusedRow.ForeColor = Color.White;

            gridView.Styles.HeaderFilterItem.SelectedStyle.BackColor = sfColor;
            gridView.Styles.HeaderFilterItem.SelectedStyle.ForeColor = Color.White;

        }

        public static void SetMenuStyle(ASPxMenu menu)
        {
            menu.BackColor = sfColor;
            menu.ForeColor = Color.White;
            menu.SubMenuStyle.GutterWidth = 0;
            menu.SubMenuStyle.BackColor = sfColor;
            menu.SubMenuItemStyle.HoverStyle.BackColor = sfLightColor;
            menu.ItemStyle.HoverStyle.BackColor = sfLightColor;
        }

        public static void SetPopupStyle(ASPxPopupControl popup)
        {
            popup.HeaderStyle.BackColor = sfColor;
            popup.HeaderStyle.ForeColor = Color.White;
        }
        public static void SetButtonStyle(ASPxButton btn)
        {
            btn.BackColor = sfColor;
            btn.ForeColor = Color.White;
            btn.HoverStyle.BackColor = sfLightColor;
        }

        public static void SetRoundPanelStyle(ASPxRoundPanel rPanel)
        {
            rPanel.HeaderStyle.BackColor = sfColor;
            rPanel.HeaderStyle.ForeColor = Color.White;
            rPanel.BackColor = sfLightColor;
        }

        public static void SetPageControlHeaderStyle(ASPxPageControl pgControl)
        {
            pgControl.TabStyle.ForeColor = Color.White;
            pgControl.TabStyle.BackColor = sfColor;
            pgControl.ActiveTabStyle.BackColor = sfLightColor;
        }

        public static void SetPageControlHeaderStyle(ASPxPageControl pgControl, bool activeTabStyle)
        {
            pgControl.TabStyle.ForeColor = Color.White;
            pgControl.TabStyle.BackColor = sfColor;
            if (activeTabStyle)
                pgControl.ActiveTabStyle.BackColor = sfLightColor;
            else
                pgControl.ContentStyle.BackColor = sfLightColor;
        }
        
        public static bool ImportDeviceData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, ref string procMsg)
        {
            string tableName = "SecurityDevice";
            bool result = DeleteTableData(sqlTrans, tableName, ref procMsg);
            if (result)
            {
                string sqlStr = string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                SqlCommand cmd = null;
                SqlDataAdapter sqlDataAdapter = null;
                SqlCommandBuilder sqlCommandBuilder = null;
                foreach (DataRow dr in dtSrc.Rows)
                {
                    try
                    {
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            using (sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                {
                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                    DataTable dtData = new DataTable();
                                    sqlDataAdapter.Fill(dtData);

                                    DataRow drData = dtData.NewRow();
                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    string devId = string.Format("{0}", dr["DeviceId"]);
                                    if (!devId.Contains(":") && devId.Length == 12)
                                    {
                                        devId = string.Format("{0}{1}:{2}{3}:{4}{5}:{6}{7}:{8}{9}:{10}{11}",
                                            devId[0], devId[1], devId[2], devId[3], devId[4], devId[5], devId[6], devId[7], devId[8], devId[9], devId[10], devId[11]).ToUpper();
                                    }
                                    drData["DeviceId"] = devId;
                                    drData["Name"] = dr["Name"];
                                    drData["Active"] = 'Y';
                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                    result = sqlDataAdapter.Update(dtData) > 0;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in ImportDeviceData: {0}", ex.Message);
                    }
                    if (!result)
                        break;
                }
            }
            return result;
        }

        public static bool ImportBarcodeSourceData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, bool appendMode, ref string procMsg)
        {
            const string tableName = "NSW_BarcodeSource";
            bool result = false;
                            
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommandBuilder sqlCommandBuilder = null;
            DataRow drData = null;
            bool newRec = false;
            foreach (DataRow dr in dtSrc.Rows)
            {
                try
                {
                    string sqlStr = appendMode ? string.Format("SELECT * FROM {0} WHERE Code='{1}'", tableName, dr["SourceID"]) : string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                    {
                        using (sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = cmd;
                            using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                            {
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                DataTable dtData = new DataTable();
                                sqlDataAdapter.Fill(dtData);

                                newRec = dtData.Rows.Count == 0;
                                if (newRec)
                                {                                        
                                    drData = dtData.NewRow();
                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    drData["Code"] = dr["SourceID"];
                                }
                                else
                                    drData = dtData.Rows[0];
                                    
                                drData["Name"] = dr["Source"];
                                drData["BarcodeStart"] = dr["BarcodeStart"];
                                drData["BarcodeStop"] = dr["BarcodeStop"];
                                if (!(dr["BarcodeLength"] is DBNull) && ECWebClass.StrToIntDef(dr["BarcodeLength"].ToString(), 0) > 0)
                                    drData["BarcodeLength"] = dr["BarcodeLength"];
                                if (newRec)
                                {
                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                }
                                else
                                {
                                    drData["Modified"] = DateTime.Now;
                                    drData["ModifiedBy"] = loginUser.UserName;
                                }
                                result = sqlDataAdapter.Update(dtData) > 0;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in ImportBarcodeSourceData: {0}", ex.Message);
                }
                if (!result)
                    break;
            }
            
            return result;
        }
        public static bool ImportProductData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, bool appendMode, ref string procMsg)
        {
            const string tableName = "NSW_Contest";
            bool result = false;
            
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommandBuilder sqlCommandBuilder = null;
            DataRow drData = null;
            bool newRec = false;
            foreach (DataRow dr in dtSrc.Rows)
            {
                try
                {
                    string sqlStr = appendMode ? string.Format("SELECT * FROM {0} WHERE Code='{1}'", tableName, dr["ProductID"]) : string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                    {
                        using (sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = cmd;
                            using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                            {
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                DataTable dtData = new DataTable();
                                sqlDataAdapter.Fill(dtData);
                                
                                newRec = dtData.Rows.Count == 0;
                                if (newRec)
                                {
                                    drData = dtData.NewRow();
                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    drData["Code"] = dr["ProductID"];
                                }
                                else
                                    drData = dtData.Rows[0];

                                drData["Name"] = dr["Product"];
                                drData["Description"] = dr["QtyDescription"];
                                if (newRec)
                                {
                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                }
                                else
                                {
                                    drData["Modified"] = DateTime.Now;
                                    drData["ModifiedBy"] = loginUser.UserName;
                                }

                                result = sqlDataAdapter.Update(dtData) > 0;                                    
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in ImportProductData: {0}", ex.Message);
                }
                if (!result)
                    break;
            }
            return result;
        }
        public static bool ImportContainerTypeData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, bool appendMode, ref string procMsg)
        {
            const string tableName = "NSW_ContainerType";
            bool result = false;
            
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommandBuilder sqlCommandBuilder = null;
            DataRow drData = null;
            bool newRec = false;
            foreach (DataRow dr in dtSrc.Rows)
            {
                try
                {
                    string sqlStr = appendMode ? string.Format("SELECT * FROM {0} WHERE Code='{1}'", tableName, dr["ContainerTypeID"]) : string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                    {
                        using (sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = cmd;
                            using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                            {
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                DataTable dtData = new DataTable();
                                sqlDataAdapter.Fill(dtData);
                                
                                newRec = dtData.Rows.Count == 0;
                                if (newRec)
                                {
                                    drData = dtData.NewRow();
                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    drData["Code"] = dr["ContainerTypeID"];
                                }
                                else
                                    drData = dtData.Rows[0];

                                drData["Name"] = dr["ContainerType"];
                                if (newRec)
                                {
                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                }
                                else
                                {
                                    drData["Modified"] = DateTime.Now;
                                    drData["ModifiedBy"] = loginUser.UserName;
                                }

                                result = sqlDataAdapter.Update(dtData) > 0;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in ImportContainerTypeData: {0}", ex.Message);
                }
                if (!result)
                    break;
            }
            return result;
        }
        public static bool ImportLocationTypeData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, bool appendMode, ref string procMsg)
        {
            const string tableName = "NSW_VenueType";
            bool result = false;
            
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommandBuilder sqlCommandBuilder = null;
            DataRow drData = null;
            bool newRec = false;
            foreach (DataRow dr in dtSrc.Rows)
            {
                try
                {
                    string sqlStr = appendMode ? string.Format("SELECT * FROM {0} WHERE Code='{1}'", tableName, dr["LocationTypeID"]) : string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                    {
                        using (sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = cmd;
                            using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                            {
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                DataTable dtData = new DataTable();
                                sqlDataAdapter.Fill(dtData);
                                
                                newRec = dtData.Rows.Count == 0;
                                if (newRec)
                                {
                                    drData = dtData.NewRow();
                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    drData["Code"] = dr["LocationTypeID"];
                                }
                                else
                                    drData = dtData.Rows[0];

                                drData["Name"] = dr["LocationType"];
                                if (newRec)
                                {
                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                }
                                else
                                {
                                    drData["Modified"] = DateTime.Now;
                                    drData["ModifiedBy"] = loginUser.UserName;
                                }

                                result = sqlDataAdapter.Update(dtData) > 0;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in ImportLocationTypeData: {0}", ex.Message);
                }
                if (!result)
                    break;
            }
            return result;
        }
        public static bool ImportManagementLocationData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, bool appendMode, ref string procMsg)
        {
            const string tableName = "NSW_ReturningOffice";
            //const string tableNameCC = "NSW_CountCentre";
            bool result = false;
            
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommandBuilder sqlCommandBuilder = null;
            DataRow drData = null;
            bool newRec = false;

            DataTable dtVenue = GetTableData(sqlTrans, "NSW_Location");
            DataTable dtVenueType = GetTableData(sqlTrans, "NSW_VenueType");
            dtVenueType.PrimaryKey = new DataColumn[] { dtVenueType.Columns["Code"] };

            List<string> roIdList = new List<string>();

            foreach (DataRow dr in dtSrc.Rows)
            {
                if (roIdList.Contains(string.Format("{0}", dr["ManagementLocationID"])))
                    continue;
                try
                {
                    string sqlStr = appendMode ? string.Format("SELECT * FROM {0} WHERE Code='{1}'", tableName, dr["ManagementLocationID"]) : string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                    {
                        using (sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = cmd;
                            using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                            {
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                DataTable dtData = new DataTable();
                                sqlDataAdapter.Fill(dtData);
                                    
                                newRec = dtData.Rows.Count == 0;
                                if (newRec)
                                {
                                    drData = dtData.NewRow();
                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    drData["Code"] = dr["ManagementLocationID"];
                                }
                                else
                                    drData = dtData.Rows[0];

                                drData["Name"] = dr["ManagementLocation"];
                                DataRow[] drsCheck = dtVenue.Select(string.Format("Name='{0}'", string.Format("{0}", dr["ManagementLocation"]).Replace("'", "''")));
                                if (drsCheck != null && drsCheck.Length > 0 && !(drsCheck[0]["VenueTypeCode"] is DBNull))
                                {
                                    int typeId = StrToIntDef(drsCheck[0]["VenueTypeCode"].ToString(), 0);
                                    if (typeId != 4 && typeId != 6 && typeId != 7)
                                        typeId = 3;
                                    DataRow drType = dtVenueType.Rows.Find(typeId);
                                    if (drType != null)
                                    {
                                        drData["LocationTypeCode"] = drType["Code"];
                                        drData["LocationTypeName"] = drType["Name"];
                                    }
                                }
                                else
                                {
                                    drsCheck = dtVenue.Select(string.Format("Name like '{0}%'", string.Format("{0}", dr["ManagementLocation"]).Replace("'", "''")));
                                    if (drsCheck != null && drsCheck.Length > 0 && !(drsCheck[0]["VenueTypeCode"] is DBNull))
                                    {
                                        int typeId = StrToIntDef(drsCheck[0]["VenueTypeCode"].ToString(), 0);
                                        if (typeId != 4 && typeId != 6 && typeId != 7)
                                            typeId = 3;
                                        DataRow drType = dtVenueType.Rows.Find(typeId);
                                        if (drType != null)
                                        {
                                            drData["LocationTypeCode"] = drType["Code"];
                                            drData["LocationTypeName"] = drType["Name"];
                                        }
                                    }
                                    else
                                    {
                                        // default                                        
                                        drData["LocationTypeCode"] = 3;
                                        if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                                            drData["LocationTypeName"] = "Returning Office";
                                        else
                                            drData["LocationTypeName"] = "EM Office";
                                    }
                                }
                                if (newRec)
                                {
                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                }
                                else
                                {
                                    drData["Modified"] = DateTime.Now;
                                    drData["ModifiedBy"] = loginUser.UserName;
                                }
                                result = sqlDataAdapter.Update(dtData) > 0;
                                if (result)
                                    roIdList.Add(string.Format("{0}", dr["ManagementLocationID"]));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in ImportManagementLocationData: {0}", ex.Message);
                }
                if (!result)
                    break;
            }
            /*
            if (result)
            {
                DataRow[] drs = dtSrc.Select("(ManagementLocation not like '%EM Office%') and (ManagementLocation <> 'Sydney Town Hall')");
                if (drs != null && drs.Length > 0)
                {
                    tableName = "NSW_CountCentre";
                    result = DeleteTableData(sqlTrans, tableName, ref procMsg);
                    if (result)
                    {
                        sqlStr = string.Format("SELECT * FROM {0} WHERE ID=-1", tableName);

                        foreach (DataRow dr in drs)
                        {
                            try
                            {
                                using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                {
                                    using (sqlDataAdapter = new SqlDataAdapter())
                                    {
                                        sqlDataAdapter.SelectCommand = cmd;
                                        using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                        {
                                            sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                            sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                            sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                            sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                            DataTable dtData = new DataTable();
                                            sqlDataAdapter.Fill(dtData);

                                            DataRow drData = dtData.NewRow();
                                            drData["Code"] = dr["ManagementLocationID"];
                                            drData["Name"] = dr["ManagementLocation"];
                                            drData["Created"] = DateTime.Now;
                                            drData["CreatedBy"] = loginUser.UserName;
                                            dtData.Rows.Add(drData);
                                            result = sqlDataAdapter.Update(dtData) > 0;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                result = false;
                                procMsg = string.Format("Error in ImportProcessingLocationData: {0}", ex.Message);
                            }
                            if (!result)
                                break;
                        }
                    }
                }
            }
                */
            return result;
        }
        public static bool ImportAreaData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, bool appendMode, ref string procMsg)
        {
            string tableName = "NSW_LGA";
            bool result = false;
            
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommandBuilder sqlCommandBuilder = null;
            DataRow drData = null;
            bool newRec = false;
            foreach (DataRow dr in dtSrc.Rows)
            {
                if (!(dr["AreaID"] is DBNull) && !(dr["AreaID"].ToString().EndsWith("0") || dr["AreaID"].ToString() == "99999"))
                    continue;
                try
                {
                    string sqlStr = appendMode ? string.Format("SELECT * FROM {0} WHERE Code='{1}'", tableName, dr["AreaID"]) : string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                    {
                        using (sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = cmd;
                            using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                            {
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                DataTable dtData = new DataTable();
                                sqlDataAdapter.Fill(dtData);
                                    
                                newRec = dtData.Rows.Count == 0;
                                if (newRec)
                                {
                                    drData = dtData.NewRow();
                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    drData["Code"] = dr["AreaID"];
                                }
                                else
                                    drData = dtData.Rows[0];

                                drData["AreaCode"] = dr["Area"];
                                if (newRec)
                                {
                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                }
                                else
                                {
                                    drData["Modified"] = DateTime.Now;
                                    drData["ModifiedBy"] = loginUser.UserName;
                                }
                                result = sqlDataAdapter.Update(dtData) > 0;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in ImportAreaData: {0}", ex.Message);
                }
                if (!result)
                    break;
            }
            if (result && loginUser.EventType == EventType.LOCAL_GOVERNMENT)
            {
                tableName = "NSW_Ward";
                newRec = false;
                foreach (DataRow dr in dtSrc.Rows)
                {
                    if (!(dr["AreaID"] is DBNull) && (dr["AreaID"].ToString().EndsWith("0") || dr["AreaID"].ToString() == "99999"))
                        continue;
                    try
                    {
                        string sqlStr = appendMode ? string.Format("SELECT * FROM {0} WHERE Code='{1}'", tableName, dr["AreaID"]) : string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            using (sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                {
                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                    DataTable dtData = new DataTable();
                                    sqlDataAdapter.Fill(dtData);

                                    newRec = dtData.Rows.Count == 0;
                                    if (newRec)
                                    {
                                        drData = dtData.NewRow();
                                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                        drData["Code"] = dr["AreaID"];
                                    }
                                    else
                                        drData = dtData.Rows[0];

                                    drData["LGAId"] = string.Empty;
                                    drData["LGACode"] = string.Empty;

                                    drData["Name"] = dr["Area"];
                                    if (newRec)
                                    {
                                        drData["Created"] = DateTime.Now;
                                        drData["CreatedBy"] = loginUser.UserName;
                                        dtData.Rows.Add(drData);
                                    }
                                    else
                                    {
                                        drData["Modified"] = DateTime.Now;
                                        drData["ModifiedBy"] = loginUser.UserName;
                                    }
                                    result = sqlDataAdapter.Update(dtData) > 0;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in ImportAreaData: {0}", ex.Message);
                    }
                    if (!result)
                        break;
                }
            }
            
            return result;
        }
        public static bool ImportLocationData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, bool appendMode, ref string procMsg)
        {
            const string tableName = "NSW_Location";
            bool result = false;
            
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommandBuilder sqlCommandBuilder = null;
            DataRow drData = null;
            bool newRec = false;

            DataTable dtVenueType = GetTableData(sqlTrans, "NSW_VenueType");
            dtVenueType.PrimaryKey = new DataColumn[] { dtVenueType.Columns["Code"] };
            DataRow drVenueType = null;

            foreach (DataRow dr in dtSrc.Rows)
            {
                try
                {
                    string sqlStr = appendMode ? string.Format("SELECT * FROM {0} WHERE Code='{1}'", tableName, dr["VenueVoteTypeID"]) : string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                    {
                        using (sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = cmd;
                            using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                            {
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                DataTable dtData = new DataTable();
                                sqlDataAdapter.Fill(dtData);                                

                                newRec = dtData.Rows.Count == 0;
                                if (newRec)
                                {
                                    drData = dtData.NewRow();
                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    drData["Code"] = dr["VenueVoteTypeID"];
                                }
                                else
                                    drData = dtData.Rows[0];

                                drData["Name"] = dr["VenueVoteType"];
                                drData["LongName"] = dr["AddressID"];
                                drData["VenueTypeCode"] = dr["LocationTypeID"];
                                drVenueType = dtVenueType.Rows.Find(drData["VenueTypeCode"]);
                                if (drVenueType != null && !(drVenueType["Name"] is DBNull) && drVenueType["Name"].ToString() != string.Empty)
                                    drData["VenueTypeName"] = drVenueType["Name"];
                                if (newRec)
                                {
                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                }
                                else
                                {
                                    drData["Modified"] = DateTime.Now;
                                    drData["ModifiedBy"] = loginUser.UserName;
                                }

                                result = sqlDataAdapter.Update(dtData) > 0;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in ImportLocationData: {0}", ex.Message);
                }
                if (!result)
                    break;
            }
            return result;
        }

        public static DataTable GetEMODistrictData(SqlTransaction sqlTrans, ref string procMsg)
        {
            DataTable dtEMODistrict = new DataTable();
            try
            {
                const string cmdText = @"select distinct CDID,ReturningOfficeName,LGAId,c.LGACode from NSW_Consignment c
                      inner join NSW_Venue v on v.Name=c.ReturningOfficeName
                      where ReturningOfficeName<>'Sydney Town Hall'";
                
                using (SqlCommand cmd = new SqlCommand(cmdText, sqlTrans.Connection, sqlTrans))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dtEMODistrict);
                }
            }
            catch (Exception ex)
            {
                dtEMODistrict = null;
                procMsg = string.Format("Error in GetEMODistrictData: {0}", ex.Message);
            }
            return dtEMODistrict;
        }
        public static DataTable GetEMODistrictWardData(SqlTransaction sqlTrans, ref string procMsg)
        {
            DataTable dt = new DataTable();
            try
            {
                const string cmdText = @"select distinct CDID, ReturningOfficeName, LGAId,LGACode,WardCode,WardName
                    from NSW_Connote C where LGAId is not null and len(LGAId)>0 and WardCode is not null and len(WardCode)>0 order by CDID,LGAId,WardCode";

                using (SqlCommand cmd = new SqlCommand(cmdText, sqlTrans.Connection, sqlTrans))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                dt = null;
                procMsg = string.Format("Error in GetEMODistrictWardData: {0}", ex.Message);
            }
            return dt;
        }
        public static DataTable GetEMODistrictVenueData(SqlTransaction sqlTrans, ref string procMsg)
        {
            DataTable dt = new DataTable();
            try
            {
                const string cmdText = @"select distinct CDID, ReturningOfficeName, LGAId,LGACode,WardCode,WardName,VenueCode,VenueName,VenueLongName,VenueTypeCode,VenueTypeName 
                    from NSW_Connote C where VenueCode is not null and len(VenueCode)>0 order by CDID,LGAId,VenueCode,VenueTypeCode";

                using (SqlCommand cmd = new SqlCommand(cmdText, sqlTrans.Connection, sqlTrans))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                dt = null;
                procMsg = string.Format("Error in GetEMODistrictVenueData: {0}", ex.Message);
            }
            return dt;
        }
        public static DataTable GetEMODistrictVenueExtraData(SqlTransaction sqlTrans, ref string procMsg)
        {
            DataTable dt = new DataTable();
            try
            {
                const string cmdText = @"select L.Code, L.Name,L.LongName,L.VenueTypeCode,L.VenueTypeName 
                    from nsw_location L left Join NSW_Venue V on v.Code=L.code
                    where v.Code is null";

                using (SqlCommand cmd = new SqlCommand(cmdText, sqlTrans.Connection, sqlTrans))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                dt = null;
                procMsg = string.Format("Error in GetEMODistrictVenueExtraData: {0}", ex.Message);
            }
            return dt;
        }

        public static DataTable GetEMOCountCentreData(SqlTransaction sqlTrans, bool emoIsCC, ref string procMsg)
        {
            DataTable dt = new DataTable();
            try
            {
                string cmdText = @"select distinct CDID, ReturningOfficeName, CountCentreCode, CountCentreName from NSW_Connote
                    where ContainerCode='1' and countcentrecode is not null and LEN(countcentrecode)>0 and cdid<>countcentrecode
                    and ReturningOfficeName not in ('CPVC','Postal fulfilment centre','Sydney Town Hall')";
                if (emoIsCC)
                    cmdText = @"select distinct CDID, ReturningOfficeName, CountCentreCode, CountCentreName from NSW_Consignment
                        where ContainerCode='1'
                        and countcentrecode is not null and LEN(countcentrecode)>0 and cdid=countcentrecode
                        and ReturningOfficeName not in ('CPVC','Postal fulfilment centre','Sydney Town Hall')
                        union
                        select distinct CDID, ReturningOfficeName, isnull(CDID, CountCentreCode) CountCentreCode, isnull(ReturningOfficeName, CountCentreName) CountCentreName from NSW_Consignment
                        where ContainerCode='1'
                        and (countcentrecode is null or LEN(countcentrecode)=0)
                        and ReturningOfficeName not in ('CPVC','Postal fulfilment centre','Sydney Town Hall')";
                using (SqlCommand cmd = new SqlCommand(cmdText, sqlTrans.Connection, sqlTrans))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                dt = null;
                procMsg = string.Format("Error in GetEMODistrictVenueExtraData: {0}", ex.Message);
            }
            return dt;
        }
        public static bool UpdateEMOCountCentreData(LoginUser loginUser, SqlTransaction sqlTrans, ref string procMsg)
        {
            string sqlStr = @"update NSW_ReturningOffice set CountCentreCode=@CountCentreCode, CountCentreName=@CountCentreName,
                Modified=@Modified, ModifiedBy=@ModifiedBy where Code=@Code";

            DataTable dtEMOCountCentreData = GetEMOCountCentreData(sqlTrans, true, ref procMsg);
            bool result = dtEMOCountCentreData != null;
            if (result)
            {
                result = dtEMOCountCentreData.Rows.Count == 0;
                if (!result)
                {
                    try
                    {
                        foreach (DataRow dr in dtEMOCountCentreData.Rows)
                        {
                            using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@CountCentreCode", dr["CountCentreCode"]);
                                cmd.Parameters.AddWithValue("@CountCentreName", dr["CountCentreName"]);
                                cmd.Parameters.AddWithValue("@Modified", DateTime.Now);
                                cmd.Parameters.AddWithValue("@ModifiedBy", loginUser.UserName);
                                cmd.Parameters.AddWithValue("@Code", dr["CDID"]);
                                result = cmd.ExecuteNonQuery() > 0;
                                if (!result)
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in UpdateEMOCountCentreData: {0}", ex.Message);
                    }
                }
                if (result)
                {
                    dtEMOCountCentreData = GetEMOCountCentreData(sqlTrans, false, ref procMsg);
                    result = dtEMOCountCentreData.Rows.Count == 0;
                    if (!result)
                    {
                        try
                        {
                            foreach (DataRow dr in dtEMOCountCentreData.Rows)
                            {
                                using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                {
                                    cmd.Parameters.AddWithValue("@CountCentreCode", dr["CountCentreCode"]);
                                    cmd.Parameters.AddWithValue("@CountCentreName", dr["CountCentreName"]);
                                    cmd.Parameters.AddWithValue("@Modified", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@ModifiedBy", loginUser.UserName);
                                    cmd.Parameters.AddWithValue("@Code", dr["CDID"]);
                                    result = cmd.ExecuteNonQuery() > 0;
                                    if (!result)
                                        break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            result = false;
                            procMsg = string.Format("Error in UpdateEMOCountCentreData: {0}", ex.Message);
                        }
                    }
                }
            }
            return result;
        }


        public static bool UpdateEMODFTeamData(LoginUser loginUser, SqlTransaction sqlTrans, ref string procMsg)
        {
            DataTable dtDistrict = GetTableData(sqlTrans, "NSW_LGA");
            bool result = dtDistrict != null && dtDistrict.Rows.Count > 0;
            if (result)
            {
                try
                {
                    string sqlInsertVenue = @"INSERT INTO NSW_Venue ([ID],[Code],[Name],[LongName],[ROCode],[ROName],[LGACode],[LGAName],[WardCode],[WardName],[VenueTypeCode],[VenueTypeName],[Active],[Created],[CreatedBy])
                        VALUES (@ID,@Code,@Name,@LongName,@ROCode,@ROName,@LGACode,@LGAName,@WardCode,@WardName,@VenueTypeCode,@VenueTypeName,@Active,@Created,@CreatedBy)";

                    int maxTeam = 10; int startPoint = 2548;

                    DataRow[] drsDistrict = dtDistrict.Select("ROCode IS NOT NULL");
                    foreach (DataRow drDistrict in drsDistrict)
                    {
                        for (int i = 0; i < maxTeam; i++)
                        {
                            using (SqlCommand cmd = new SqlCommand(sqlInsertVenue, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ID", Guid.NewGuid().ToString().ToUpper());
                                if (i == maxTeam - 1)
                                    cmd.Parameters.AddWithValue("@Code", string.Format("{0}", 2533));
                                else
                                    cmd.Parameters.AddWithValue("@Code", string.Format("{0}", startPoint - i));

                                if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                                {
                                    cmd.Parameters.AddWithValue("@Name", string.Format("DI TEAM {0}", (maxTeam - i).ToString().PadLeft(2, '0')));
                                    cmd.Parameters.AddWithValue("@LongName", string.Format("DI TEAM {0}", (maxTeam - i).ToString().PadLeft(2, '0')));
                                    cmd.Parameters.AddWithValue("@VenueTypeName", "Declared Institution");
                                    cmd.Parameters.AddWithValue("@WardCode", "99999");
                                    cmd.Parameters.AddWithValue("@WardName", "Mixed");
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@Name", string.Format("DF TEAM {0}", (maxTeam - i).ToString().PadLeft(2, '0')));
                                    cmd.Parameters.AddWithValue("@LongName", string.Format("DF TEAM {0}", (maxTeam - i).ToString().PadLeft(2, '0')));
                                    cmd.Parameters.AddWithValue("@VenueTypeName", "Declared Facility");
                                    cmd.Parameters.AddWithValue("@WardCode", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@WardName", DBNull.Value);
                                }
                                cmd.Parameters.AddWithValue("@ROCode", drDistrict["ROCode"]);
                                if (!(drDistrict["ROName"] is DBNull) && drDistrict["ROName"].ToString() != string.Empty)
                                    cmd.Parameters.AddWithValue("@ROName", drDistrict["ROName"]);
                                else
                                    cmd.Parameters.AddWithValue("@ROName", DBNull.Value);

                                cmd.Parameters.AddWithValue("@LGACode", drDistrict["Code"]);
                                if (!(drDistrict["AreaCode"] is DBNull) && drDistrict["AreaCode"].ToString() != string.Empty)
                                    cmd.Parameters.AddWithValue("@LGAName", drDistrict["AreaCode"]);
                                else
                                    cmd.Parameters.AddWithValue("@LGAName", DBNull.Value);
                                cmd.Parameters.AddWithValue("@VenueTypeCode", "1");// ???
                                
                                cmd.Parameters.AddWithValue("@Active", 'Y');
                                cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                result = cmd.ExecuteNonQuery() > 0;
                                if (!result)
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in UpdateEMODFTeamData: {0}", ex.Message);
                }
            }
            return result;
        }
        
        public static bool UpdateEMODistrictWardData(LoginUser loginUser, SqlTransaction sqlTrans, ref string procMsg)
        {            
            const string tableName = "NSW_Ward";
            DataTable dtWardData = GetEMODistrictWardData(sqlTrans, ref procMsg);
            bool result = dtWardData != null;
            if (result)
            {
                result = dtWardData.Rows.Count == 0;
                if (!result)
                {
                    result = DeleteTableData(sqlTrans, tableName, ref procMsg);
                    //return true;
                    if (result)
                    {
                        try
                        {
                            string sqlStr = string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                            SqlCommand cmd = null;
                            SqlDataAdapter sqlDataAdapter = null;
                            SqlCommandBuilder sqlCommandBuilder = null;
                            foreach (DataRow dr in dtWardData.Rows)
                            {
                                try
                                {
                                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                    {
                                        using (sqlDataAdapter = new SqlDataAdapter())
                                        {
                                            sqlDataAdapter.SelectCommand = cmd;
                                            using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                            {
                                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                                DataTable dtData = new DataTable();
                                                sqlDataAdapter.Fill(dtData);

                                                DataRow drData = dtData.NewRow();

                                                drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                                drData["ROCode"] = dr["CDID"];
                                                drData["ROName"] = dr["ReturningOfficeName"];
                                                drData["LGAId"] = dr["LGAId"];
                                                drData["LGACode"] = dr["LGACode"];
                                                drData["Code"] = dr["WardCode"];
                                                drData["Name"] = dr["WardName"];                                                
                                                drData["Active"] = 'Y';
                                                drData["Created"] = DateTime.Now;
                                                drData["CreatedBy"] = loginUser.UserName;
                                                dtData.Rows.Add(drData);
                                                result = sqlDataAdapter.Update(dtData) > 0;
                                                if (!result)
                                                    break;
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    result = false;
                                    procMsg = string.Format("Error in UpdateEMODistrictWardData: {0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            result = false;
                            procMsg = string.Format("Error in UpdateEMODistrictWardData: {0}", ex.Message);
                        }
                    }
                }
            }
            return result;
        }
        public static bool UpdateEMODistrictVenueData(LoginUser loginUser, SqlTransaction sqlTrans, ref string procMsg)
        {
            const string tableName = "NSW_Venue";
            DataTable dtVenueData = GetEMODistrictVenueData(sqlTrans, ref procMsg);
            bool result = dtVenueData != null;
            if (result)
            {
                result = dtVenueData.Rows.Count == 0;
                if (!result)
                {
                    result = DeleteTableData(sqlTrans, tableName, ref procMsg);
                    if (result)
                    try
                    {
                        string sqlStr = string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                        SqlCommand cmd = null;
                        SqlDataAdapter sqlDataAdapter = null;
                        SqlCommandBuilder sqlCommandBuilder = null;

                        foreach (DataRow dr in dtVenueData.Rows)
                        {
                            try
                            {
                                using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                {
                                    using (sqlDataAdapter = new SqlDataAdapter())
                                    {
                                        sqlDataAdapter.SelectCommand = cmd;
                                        using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                        {
                                            sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                            sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                            sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                            sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                            sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                            DataTable dtData = new DataTable();
                                            sqlDataAdapter.Fill(dtData);

                                            DataRow drData = dtData.NewRow();

                                            drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                            drData["ROCode"] = dr["CDID"];
                                            drData["ROName"] = dr["ReturningOfficeName"];
                                            drData["LGACode"] = dr["LGAId"];
                                            drData["LGAName"] = dr["LGACode"];
                                            drData["WardCode"] = dr["WardCode"];
                                            drData["WardName"] = dr["WardName"];
                                            drData["Code"] = dr["VenueCode"];
                                            drData["Name"] = dr["VenueName"];
                                            drData["LongName"] = dr["VenueLongName"];
                                            drData["VenueTypeCode"] = dr["VenueTypeCode"];
                                            drData["VenueTypeName"] = dr["VenueTypeName"];
                                            drData["Active"] = 'Y';
                                            drData["Created"] = DateTime.Now;
                                            drData["CreatedBy"] = loginUser.UserName;
                                            dtData.Rows.Add(drData);
                                            result = sqlDataAdapter.Update(dtData) > 0;
                                            if (!result)
                                                break;
                                        }
                                    }
                                }
                            }
                            catch(Exception ex)
                            {
                                result = false;
                                procMsg = string.Format("Error in UpdateEMODistrictVenueData: {0}", ex.Message);
                            }
                        }
                        if (result)
                        {
                            dtVenueData = GetEMODistrictVenueExtraData(sqlTrans, ref procMsg);
                            result = dtVenueData != null;
                            if (result)
                            {
                                result = dtVenueData.Rows.Count == 0;
                                if (!result)
                                {
                                    foreach (DataRow dr in dtVenueData.Rows)
                                    {
                                        try
                                        {
                                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                            {
                                                using (sqlDataAdapter = new SqlDataAdapter())
                                                {
                                                    sqlDataAdapter.SelectCommand = cmd;
                                                    using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                                    {
                                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                                        sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                                        DataTable dtData = new DataTable();
                                                        sqlDataAdapter.Fill(dtData);

                                                        DataRow drData = dtData.NewRow();
                                                        /*
                                                        drData["ROCode"] = DBNull.Value;
                                                        drData["ROName"] = DBNull.Value;
                                                        drData["LGACode"] = DBNull.Value;
                                                        drData["LGAName"] = DBNull.Value;
                                                         */
                                                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                                        drData["Code"] = dr["Code"];
                                                        drData["Name"] = dr["Name"];
                                                        drData["LongName"] = dr["LongName"];
                                                        drData["VenueTypeCode"] = dr["VenueTypeCode"];
                                                        drData["VenueTypeName"] = dr["VenueTypeName"];
                                                        drData["Active"] = 'Y';
                                                        drData["Created"] = DateTime.Now;
                                                        drData["CreatedBy"] = loginUser.UserName;
                                                        dtData.Rows.Add(drData);

                                                        result = sqlDataAdapter.Update(dtData) > 0;
                                                        if (!result)
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            result = false;
                                            procMsg = string.Format("Error in UpdateEMODistrictVenueData: {0}", ex.Message);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in UpdateEMODistrictVenueData: {0}", ex.Message);
                    }
                }
            }
            return result;
        }

        private static bool UpdateSlavePPDMVersion(SqlTransaction sqlTrans, DataRow drSrcApp, ref string procMsg)
        {
            bool result = false;
            try
            {
                string sqlStr = @"update NSW_Application set PPDMVersion=@PPDMVersion, PPDMLoaded=@PPDMLoaded";
                using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                {
                    cmd.Parameters.AddWithValue("@PPDMVersion", drSrcApp["PPDMVersion"]);
                    cmd.Parameters.AddWithValue("@PPDMLoaded", drSrcApp["PPDMLoaded"]);
                    result = cmd.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                procMsg = string.Format("Error in UpdatePPDMVersion: {0}", ex.Message);
            }
            return result;
        }
        public static bool UpdatePPDMVersion(SqlTransaction sqlTrans, bool versionUpdate, ref string procMsg)
        {
            bool result = false;
            try
            {
                string sqlStr;
                if (!versionUpdate)
                    sqlStr = @"update NSW_Application set PPDMLoaded=@PPDMLoaded";
                else
                    sqlStr = @"update NSW_Application set PPDMVersion=ISNULL(PPDMVersion,0)+1, PPDMLoaded=@PPDMLoaded";
                using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                {
                    cmd.Parameters.AddWithValue("@PPDMLoaded", DateTime.Now);
                    result = cmd.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                procMsg = string.Format("Error in UpdatePPDMVersion: {0}", ex.Message);
            }
            return result;
        }
        public static bool UpdateEMODistrictData(SqlTransaction sqlTrans, ref string procMsg)
        {
            bool result = false;
            try
            {
                string sqlStr =
                @"update NSW_LGA
                  set ROCode=LINK.CDID,ROName=LINK.ReturningOfficeName
                  from NSW_LGA AREA
                  inner join (select distinct CDID,ReturningOfficeName,LGAId,c.LGACode from NSW_Consignment c
                  inner join NSW_Location v on v.Name=c.ReturningOfficeName
                  ) LINK
                  ON LINK.LGAId=AREA.Code AND LINK.LGACode=AREA.AreaCode";
                /*
                @"update NSW_LGA
                  set ROCode=LINK.CDID,ROName=LINK.ReturningOfficeName
                  from NSW_LGA AREA
                  inner join (select distinct CDID,ReturningOfficeName,LGAId,c.LGACode from NSW_Consignment c
                  inner join NSW_Location v on v.Name=c.ReturningOfficeName
                  where ReturningOfficeName<>'Sydney Town Hall') LINK
                  ON LINK.LGAId=AREA.Code AND LINK.LGACode=AREA.AreaCode";
                 * */
                using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                {
                    result = cmd.ExecuteNonQuery() >= 0;
                }
            }
            catch(Exception ex)
            {
                procMsg = string.Format("Error in UpdateEMODistrictData: {0}", ex.Message);
            }
            return result;
        }

        public static bool ImportConnoteData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, bool appendMode, ref string procMsg)
        {
            const string tableName = "NSW_Consignment";
            bool result = DeleteTableData(sqlTrans, tableName, ref procMsg);
            /*
            bool result = DeleteTableData(sqlTrans, tableName, ref procMsg) &&
                DeleteTableData(sqlTrans, "NSW_Connote", ref procMsg) &&
                DeleteTableData(sqlTrans, "NSW_Container", ref procMsg);
             */
            if (result)
            {
                string sqlStr = string.Format("SELECT * FROM {0} WHERE ID IS NULL", tableName);
                SqlCommand cmd = null;
                SqlDataAdapter sqlDataAdapter = null;
                SqlCommandBuilder sqlCommandBuilder = null;
                
                DataTable dtSource = GetTableData(sqlTrans, "NSW_BarcodeSource");
                dtSource.PrimaryKey = new DataColumn[] { dtSource.Columns["Code"] };

                DataTable dtCC = GetTableData(sqlTrans, "NSW_CountCentre");
                dtCC.PrimaryKey = new DataColumn[] { dtCC.Columns["Code"] };

                DataTable dtEMO = GetTableData(sqlTrans, "NSW_ReturningOffice");
                dtEMO.PrimaryKey = new DataColumn[] { dtEMO.Columns["Code"] };
                DataTable dtArea = GetTableData(sqlTrans, "NSW_LGA");
                dtArea.PrimaryKey = new DataColumn[] { dtArea.Columns["Code"] };

                DataTable dtWard = GetTableData(sqlTrans, "NSW_Ward");
                //dtWard.PrimaryKey = new DataColumn[] { dtWard.Columns["ROCode"], dtWard.Columns["LGAId"], dtWard.Columns["Code"] };

                DataTable dtVenueType = GetTableData(sqlTrans, "NSW_VenueType");
                dtVenueType.PrimaryKey = new DataColumn[] { dtVenueType.Columns["Code"] };
                DataTable dtProduct = GetTableData(sqlTrans, "NSW_Contest");
                dtProduct.PrimaryKey = new DataColumn[] { dtProduct.Columns["Code"] };
                /*
                DataTable dtVenue = GetTableData(sqlTrans, "NSW_Venue");
                dtVenue.PrimaryKey = new DataColumn[] { dtVenue.Columns["Code"] };
                */
                DataTable dtLocation = GetTableData(sqlTrans, "NSW_Location");
                dtLocation.PrimaryKey = new DataColumn[] { dtLocation.Columns["Code"] };

                DataTable dtContainerType = GetTableData(sqlTrans, "NSW_ContainerType");
                dtContainerType.PrimaryKey = new DataColumn[] { dtContainerType.Columns["Code"] };

                DataTable dtConnote = null;
                if (appendMode)
                {
                    dtConnote = GetTableData(sqlTrans, "NSW_Connote", "ConsNo");
                    dtConnote.PrimaryKey = new DataColumn[] { dtConnote.Columns["ConsNo"] };
                }
                
                DataRow drTarget = null;

                List<string> consIdList = new List<string>();

                int lineCount = 0;

                foreach (DataRow dr in dtSrc.Rows)
                {
                    lineCount++;
                    if (consIdList.Contains(string.Format("{0}", dr["ConsignmentID"])))
                        continue;
                    if (StrToIntDef(string.Format("{0}", dr["TotalContainers"]), 0) <= 0)
                        continue;
                    if (appendMode && dtConnote.Rows.Find(dr["ConsignmentID"]) != null)
                        continue;
                    try
                    {
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            using (sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                {
                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                    DataTable dtData = new DataTable();
                                    sqlDataAdapter.Fill(dtData);

                                    DataRow drData = dtData.NewRow();

                                    drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                                    drData["SequentialId"] = dr["ConsignmentID"];
                                    /*
                                    if (drData["SequentialId"].ToString() == "113")
                                    {
                                        string aa = dr["ManagementLocationID"].ToString();
                                    }
                                     * */
                                    drData["SourceId"] = dr["SourceID"];
                                    drTarget = dtSource.Rows.Find(drData["SourceId"]);
                                    if (drTarget != null && !(drTarget["Name"] is DBNull) && drTarget["Name"].ToString() != string.Empty)
                                        drData["Source"] = drTarget["Name"];

                                    drData["CDID"] = dr["ManagementLocationID"];
                                    drTarget = dtEMO.Rows.Find(drData["CDID"]);
                                    if (drTarget != null && !(drTarget["Name"] is DBNull) && drTarget["Name"].ToString() != string.Empty)
                                        drData["ReturningOfficeName"] = drTarget["Name"];
                                    else
                                    {
                                        drTarget = dtCC.Rows.Find(drData["CDID"]);
                                        if (drTarget != null && !(drTarget["Name"] is DBNull) && drTarget["Name"].ToString() != string.Empty)
                                            drData["ReturningOfficeName"] = drTarget["Name"];
                                    }

                                    drData["LGAId"] = dr["IssuingAreaID"];
                                    drTarget = dtArea.Rows.Find(drData["LGAId"]);
                                    if (drTarget != null && !(drTarget["AreaCode"] is DBNull) && drTarget["AreaCode"].ToString() != string.Empty)
                                    {
                                        drData["LGACode"] = drTarget["AreaCode"];
                                        drData["LGAName"] = drTarget["AreaCode"];
                                    }

                                    drData["ContestAreaId"] = dr["ContestAreaID"];
                                    if (!(drData["ContestAreaId"] is DBNull) && drData["ContestAreaId"].ToString() != string.Empty)
                                    {
                                        drTarget = dtArea.Rows.Find(drData["ContestAreaId"]);
                                        if (drTarget != null && !(drTarget["AreaCode"] is DBNull) && drTarget["AreaCode"].ToString() != string.Empty)
                                            drData["ContestAreaCode"] = drTarget["AreaCode"];
                                    }
                                    else
                                    {
                                        if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                                        {
                                            drData["ContestAreaId"] = "99999";
                                            drData["ContestAreaCode"] = "Mixed";
                                        }
                                        else
                                        {
                                            drData["ContestAreaId"] = 94;
                                            drData["ContestAreaCode"] = "Mixed";
                                        }
                                    }
                                    if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                                    {
                                        drData["WardCode"] = drData["ContestAreaId"];
                                        drData["WardName"] = drData["ContestAreaCode"];
                                        if (drData["WardCode"].ToString() == "94")
                                        {
                                            drData["WardCode"] = "00";
                                            drData["WardName"] = "Undivided";
                                        }
                                        else
                                        {
                                            DataRow[] drs = dtWard.Select(string.Format("Code='{0}'", drData["WardCode"]));
                                            if (drs != null && drs.Length > 0)
                                                drData["WardName"] = drs[0]["Name"];
                                        }
                                        /*
                                        if (!(drData["CDID"] is DBNull) && !(drData["LGAId"] is DBNull))
                                        {
                                            DataRow[] drs = dtWard.Select(string.Format("ROCode='{0}' AND LGAId='{1}' AND Code='{2}'", drData["CDID"], drData["LGAId"], drData["WardCode"]));                                            
                                            if (drs != null && drs.Length > 0)
                                                drData["WardName"] = drs[0]["Name"];
                                        }
                                        */
                                    }

                                    drData["ContestCode"] = dr["ProductID"];
                                    drTarget = dtProduct.Rows.Find(drData["ContestCode"]);
                                    if (drTarget != null && !(drTarget["Name"] is DBNull) && drTarget["Name"].ToString() != string.Empty)
                                        drData["ContestName"] = drTarget["Name"];

                                    drData["ContainerCode"] = dr["ContainerTypeID"];
                                    drTarget = dtContainerType.Rows.Find(drData["ContainerCode"]);
                                    if (drTarget != null && !(drTarget["Name"] is DBNull) && drTarget["Name"].ToString() != string.Empty)
                                        drData["ContainerName"] = drTarget["Name"];

                                    drData["VenueCode"] = dr["VenueVoteTypeID"];
                                    drTarget = dtLocation.Rows.Find(drData["VenueCode"]);
                                    if (drTarget != null)
                                    {                                        
                                        if (!(drTarget["Name"] is DBNull) && drTarget["Name"].ToString() != string.Empty)
                                            drData["VenueName"] = drTarget["Name"];
                                        if (!(drTarget["LongName"] is DBNull) && drTarget["LongName"].ToString() != string.Empty)
                                            drData["VenueLongName"] = drTarget["LongName"];
                                        if (!(drTarget["VenueTypeCode"] is DBNull) && drTarget["VenueTypeCode"].ToString() != string.Empty)
                                            drData["VenueTypeCode"] = drTarget["VenueTypeCode"];
                                        if (!(drTarget["VenueTypeName"] is DBNull) && drTarget["VenueTypeName"].ToString() != string.Empty)
                                            drData["VenueTypeName"] = drTarget["VenueTypeName"];                                        
                                    }

                                    //drData["WardCode"] = "00";
                                    //drData["WardName"] = "Undivided";

                                    drData["ContainerQuantity"] = dr["TotalContainers"];

                                    if (dtSrc.Columns.IndexOf("DeliveryNumber") >= 0)
                                    {
                                        if (dr["DeliveryNumber"] != DBNull.Value && dr["DeliveryNumber"].ToString() != string.Empty)
                                            drData["DeliveryNumber"] = dr["DeliveryNumber"];
                                    }

                                    if (dtSrc.Columns.IndexOf("CountLocationID") >= 0)
                                    {
                                        drData["CountCentreCode"] = dr["CountLocationID"];
                                        drTarget = dtEMO.Rows.Find(drData["CountCentreCode"]);
                                        if (drTarget != null && !(drTarget["Name"] is DBNull) && drTarget["Name"].ToString() != string.Empty)
                                            drData["CountCentreName"] = drTarget["Name"];
                                        else
                                        {
                                            drTarget = dtCC.Rows.Find(drData["CountCentreCode"]);
                                            if (drTarget != null && !(drTarget["Name"] is DBNull) && drTarget["Name"].ToString() != string.Empty)
                                                drData["CountCentreName"] = drTarget["Name"];
                                        }
                                    }

                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                    result = sqlDataAdapter.Update(dtData) > 0;
                                    if (result)
                                        consIdList.Add(string.Format("{0}", dr["ConsignmentID"]));

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in ImportConnoteData at line {1}: {0}", ex.Message, lineCount);
                    }
                    if (!result)
                        break;
                }
                if (result && !appendMode)
                    result = UpdateEMODistrictData(sqlTrans, ref procMsg);
            }
            return result;
        }

        public static bool ImportContainerData(LoginUser loginUser, SqlTransaction sqlTrans, DataTable dtSrc, ref string procMsg)
        {
            const string tableName = "NSW_Container";
            const string tableNameCon = "NSW_Connote";

            DataTable dtConsignment = GetTableData(sqlTrans, "NSW_Consignment", "SequentialId");

            bool result = dtConsignment != null && dtConsignment.Rows.Count > 0;
            if (result)
            {
                string sqlStr = string.Empty;
                string sqlStrParam = string.Empty;
                for (int i = 0; i < ConnoteFieldList.Length; i++)
                {
                    if (string.IsNullOrEmpty(sqlStr))
                    {
                        sqlStr = ConnoteFieldList[i];
                        sqlStrParam = string.Format("@{0}", ConnoteFieldList[i]);
                    }
                    else
                    {
                        sqlStr += string.Format(",{0}", ConnoteFieldList[i]);
                        sqlStrParam += string.Format(",@{0}", ConnoteFieldList[i]);
                    }
                }
                //sqlStr = string.Format("INSERT INTO {0} ({1}) VALUES ({2}); SELECT CAST(scope_identity() AS int)", tableNameCon, sqlStr, sqlStrParam);
                sqlStr = string.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableNameCon, sqlStr, sqlStrParam);
                
                /*
                string sqlStr = string.Format(@"INSERT INTO {0} (ConsNo,SourceId,[Source],CDID,ReturningOfficeName,LGAId,LGACode,LGAName,
	                VenueCode,VenueName,VenueLongName,VenueTypeCode,VenueTypeName,ContestCode,ContestName,
	                ContainerCode,ContainerName,ContainerQuantity,Created,CreatedBy)
                  VALUES (@ConsNo,@SourceId,@Source,@CDID,@ReturningOfficeName,@LGAId,@LGACode,@LGAName,
	                @VenueCode,@VenueName,@VenueLongName,@VenueTypeCode,@VenueTypeName,@ContestCode,@ContestName,
	                @ContainerCode,@ContainerName,@ContainerQuantity,@Created,@CreatedBy); SELECT CAST(scope_identity() AS int)", tableName);
                 */
                SqlCommand cmd = null;

                //int consKey = -1;
                object consKey;
                string consId = string.Empty;
                bool prePrinted = false;
                foreach (DataRow drConnote in dtConsignment.Rows)
                {
                    consKey = drConnote["ID"];
                    consId = string.Format("{0}", drConnote["SequentialId"]);
                    prePrinted = !(drConnote["SourceId"] is DBNull) && StrToIntDef(drConnote["SourceId"].ToString(), -1) == 2;
                    using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                    {
                        for (int i = 0; i < ConnoteFieldList.Length; i++)
                        {
                            if (ConnoteFieldList[i] == "ConsKey")
                                cmd.Parameters.AddWithValue(string.Format("@{0}", ConnoteFieldList[i]), consKey);
                            else
                            if (ConnoteFieldList[i] == "ConsNo")
                            {
                                if (!string.IsNullOrEmpty(consId))
                                    cmd.Parameters.AddWithValue(string.Format("@{0}", ConnoteFieldList[i]), consId);
                                else
                                    cmd.Parameters.AddWithValue(string.Format("@{0}", ConnoteFieldList[i]), DBNull.Value);
                            }
                            else
                            if (!(drConnote[ConnoteFieldList[i]] is DBNull))
                                cmd.Parameters.AddWithValue(string.Format("@{0}", ConnoteFieldList[i]), drConnote[ConnoteFieldList[i]]);
                            else
                                cmd.Parameters.AddWithValue(string.Format("@{0}", ConnoteFieldList[i]), DBNull.Value);
                        }
                        result = cmd.ExecuteNonQuery() > 0;
                        /*
                        // insert new record and get the new identity
                        consKey = (Int32)cmd.ExecuteScalar();
                        result = consKey > -1;// new id returned
                         */
                    }
                    if (result)
                    {
                        DataRow[] drsCarton = dtSrc.Select(string.Format("ConsignmentID='{0}'", consId));
                        if (drsCarton != null && drsCarton.Length > 0)
                        {
                            string sqlStrCtn = string.Format(@"INSERT INTO {0} (ConsKey,[LineNo],ContainerNo,Barcode,Created,CreatedBy,Activated) 
                                VALUES(@ConsKey,@LineNo,@ContainerNo,@Barcode,@Created,@CreatedBy,@Activated)", tableName);
                            foreach (DataRow dr in drsCarton)
                            {
                                using (cmd = new SqlCommand(sqlStrCtn, sqlTrans.Connection, sqlTrans))
                                {
                                    cmd.Parameters.AddWithValue("@ConsKey", consKey);
                                    cmd.Parameters.AddWithValue("@LineNo", dr["Line"]);
                                    cmd.Parameters.AddWithValue("@ContainerNo", consId);
                                    cmd.Parameters.AddWithValue("@Barcode", dr["Barcode"]);
                                    cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@CreatedBy", loginUser.UserName);
                                    /*
                                    if (prePrinted)
                                        cmd.Parameters.AddWithValue("@Activated", 'N');
                                    else
                                     */
                                        cmd.Parameters.AddWithValue("@Activated", 'Y');
                                    result = cmd.ExecuteNonQuery() > 0;
                                }
                                if (!result)
                                    return result;
                            }
                        }
                    }
                    else
                        return result;
                }
                /*
                foreach (DataRow dr in dtSrc.Rows)
                {
                    try
                    {
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            using (sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                {
                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                    DataTable dtData = new DataTable();
                                    sqlDataAdapter.Fill(dtData);

                                    DataRow drData = dtData.NewRow();
                                    drData["ConsKey"] = dr["ConsignmentID"];
                                    drData["ContainerNo"] = dr["ConsignmentID"];
                                    drData["LineNo"] = dr["Line"];

                                    drData["Barcode"] = dr["Barcode"];

                                    drData["Created"] = DateTime.Now;
                                    drData["CreatedBy"] = loginUser.UserName;
                                    dtData.Rows.Add(drData);
                                    result = sqlDataAdapter.Update(dtData) > 0;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in ImportContainerData: {0}", ex.Message);
                    }
                    if (!result)
                        break;
                }
                 */
            }
            return result;
        }

        public static bool ImportDeviceTableData(ref string procMsg)
        {
            object slaveConnStrObj = ConfigurationManager.ConnectionStrings["SlaveDBConString"];

            bool result = slaveConnStrObj == null || string.IsNullOrEmpty(slaveConnStrObj.ToString());
            if (!result)
            {
                try
                {
                    string tblDevice = "SecurityDevice";
                    using (SqlConnection sqlConn = new SqlConnection(slaveConnStrObj.ToString()))
                    {
                        sqlConn.Open();
                        SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                        try
                        {
                            result = DeleteTableData(sqlTrans, tblDevice, ref procMsg);
                            if (result)
                            {
                                DataTable tgtSGETable = GetSGETableData(tblDevice);
                                if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                    result = ImportSGETableData(sqlTrans, tgtSGETable, ref procMsg);
                                if (result)
                                    sqlTrans.Commit();
                                else
                                    sqlTrans.Rollback();
                            }
                        }
                        catch (Exception ex)
                        {
                            result = false;
                            sqlTrans.Rollback();
                            procMsg = string.Format("Error in ImportDeviceTableData: {0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in ImportDeviceTableData: {0}", ex.Message);
                }
            }
            return result;
        }

        public static bool ImportDeviceFile(LoginUser loginUser, string fileName, bool slaveDBUpdate, ref string procMsg)
        {
            bool result = false;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        result = File.Exists(fileName);
                        if (result)
                        {
                            using (GenericParserAdapter parser = new GenericParserAdapter(fileName))
                            {
                                parser.FirstRowHasHeader = true;
                                DataSet dsResult = parser.GetDataSet();
                                result = dsResult.Tables.Count > 0;
                                if (result)
                                {
                                    DataTable dtSource = dsResult.Tables[0];
                                    result = dtSource.Rows.Count > 0;
                                    if (result)
                                        result = ImportDeviceData(loginUser, sqlTrans, dtSource, ref procMsg);
                                }
                            }
                        }
                        if (result)
                        {
                            sqlTrans.Commit();
                            procMsg = "Device file has been imported successfully";
                            if (slaveDBUpdate)
                            {
                                string procMsgEx = string.Empty;
                                if (!ImportDeviceTableData(ref procMsgEx))
                                    procMsg += procMsgEx;
                            }
                        }
                        else
                        {
                            sqlTrans.Rollback();
                            if (!string.IsNullOrEmpty(procMsg))
                                procMsg = string.Format("No file has been imported - {0}", procMsg);
                            else
                                procMsg = "No file has been imported";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in ImportDeviceFile: {0}", ex.Message);
                    }
                }
            }
            catch(Exception ex)
            {
                result = false;
                procMsg = string.Format("Error in ImportDeviceFile: {0}", ex.Message);
            }
            return result;
        }

        public static bool ImportSGETableData(SqlTransaction sqlTrans, DataTable dtSrc, ref string procMsg)
        {
            bool result = false;
            string sqlStr = null;
            if (dtSrc.Columns.IndexOf("ID") > -1)
                sqlStr = string.Format("SELECT * FROM {0} WHERE ID IS NULL", dtSrc.TableName);
            else
                if (dtSrc.Columns.IndexOf("ConsKey") > -1)
                    sqlStr = string.Format("SELECT * FROM {0} WHERE ConsKey IS NULL", dtSrc.TableName);
            result = !string.IsNullOrEmpty(sqlStr);
            if (!result)
            {
                procMsg = string.Format("Primary key is not found in table '{0}'", dtSrc.TableName);
            }
            else
            {
                SqlCommand cmd = null;
                SqlDataAdapter sqlDataAdapter = null;
                SqlCommandBuilder sqlCommandBuilder = null;
                foreach (DataRow drSrc in dtSrc.Rows)
                {
                    try
                    {
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            using (sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                {
                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                    DataTable dtData = new DataTable();
                                    sqlDataAdapter.Fill(dtData);

                                    DataRow drData = dtData.NewRow();
                                    foreach (DataColumn col in dtSrc.Columns)
                                    {
                                        drData[col.ColumnName] = drSrc[col.ColumnName];
                                    }
                                    dtData.Rows.Add(drData);
                                    result = sqlDataAdapter.Update(dtData) > 0;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in ImportSGETableData for table '{0}': {1}", dtSrc.TableName, ex.Message);
                    }
                    if (!result)
                        break;
                }
            }
            return result;
        }

        private static bool BulkCopySGETableData(SqlTransaction sqlTrans, string tableName, ref string procMsg)
        {
            bool result = false;
            using (SqlConnection sqlSrcConn = new SqlConnection(connStr))
            {
                sqlSrcConn.Open();
                using (SqlCommand cmd = new SqlCommand(string.Format("SELECT * FROM {0}", tableName), sqlSrcConn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlTrans.Connection, SqlBulkCopyOptions.Default, sqlTrans))
                        {
                            bulkCopy.DestinationTableName = tableName;
                            bulkCopy.WriteToServer(reader);
                        }
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in BulkCopySGETableData for table '{0}': {1}", tableName, ex.Message);
                    }
                    finally
                    {
                        reader.Close();
                    }
                }

            }
            return result;
        }

        public static bool SGEImportTableData(bool appendMode, ref string procMsg)
        {
            bool result = false;

            var slaveUpdateViaWinServ = ConfigurationManager.AppSettings["SlaveUpdateViaWinServ"];

            if (slaveUpdateViaWinServ != null && Convert.ToBoolean(slaveUpdateViaWinServ))
            {
                var writeTransFile = ConfigurationManager.AppSettings["WebServSourceFolder"];
                result = writeTransFile != null && !string.IsNullOrEmpty(writeTransFile) && Directory.Exists(writeTransFile);
                if (result)
                {
                    File.WriteAllText(string.Format(@"{0}\SlaveDBUpdate_{1}.txt", writeTransFile, DateTime.Now.ToString("yyyyMMddHHmmssfff")), string.Format("{0}|{1}", procMsg, appendMode));
                    // todo: winserv change for append mode
                }
                return result;
            }            

            object slaveConnStrObj = ConfigurationManager.ConnectionStrings["SlaveDBConString"];

            result = slaveConnStrObj == null || string.IsNullOrEmpty(slaveConnStrObj.ToString());
            if (!result)
            {
                try
                {
                    using (SqlConnection sqlConn = new SqlConnection(slaveConnStrObj.ToString()))
                    {
                        sqlConn.Open();
                        SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                        try
                        {
                            DataTable tgtSGETable;
                            if (appendMode)
                            {
                                for (int i = SGETableList1.Length - 1; i >= 0; i--)
                                {
                                    result = DeleteTableData(sqlTrans, SGETableList1[i], ref procMsg);
                                    if (!result)
                                        break;
                                }
                                if (result)
                                {
                                    for (int i = 0; i < SGETableList1.Length; i++)
                                    {
                                        result = BulkCopySGETableData(sqlTrans, SGETableList1[i], ref procMsg);
                                        if (!result)
                                            break;
                                    }
                                    if (result)
                                    {
                                        tgtSGETable = GetSQLData("select T.* from NSW_Connote T inner join NSW_Consignment C on C.ID=T.ConsKey");
                                        if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                        {
                                            tgtSGETable.TableName = "NSW_Connote";
                                            result = ImportSGETableData(sqlTrans, tgtSGETable, ref procMsg);
                                            if (result)
                                            {
                                                tgtSGETable = GetSQLData("select T.* from NSW_Container T inner join dbo.NSW_Consignment C on C.ID=T.ConsKey");
                                                if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                                {
                                                    tgtSGETable.TableName = "NSW_Container";
                                                    result = ImportSGETableData(sqlTrans, tgtSGETable, ref procMsg);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = SGETableList.Length - 1; i >= 0; i--)
                                {
                                    result = DeleteTableData(sqlTrans, SGETableList[i], ref procMsg);
                                    if (!result)
                                        break;
                                }
                                if (result)
                                {                                    
                                    for (int i = 0; i < SGETableList.Length; i++)
                                    {
                                        result = BulkCopySGETableData(sqlTrans, SGETableList[i], ref procMsg);
                                        if (!result)
                                            break;
                                    }                                    
                                }
                            }
                            if (result)
                            {
                                tgtSGETable = GetSGETableData("NSW_Application");
                                if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                    result = UpdateSlavePPDMVersion(sqlTrans, tgtSGETable.Rows[0], ref procMsg);
                                if (result)
                                    result = DeleteTableData("NSW_Consignment", ref procMsg);
                            }
                                
                            if (result)
                                sqlTrans.Commit();
                            else
                                sqlTrans.Rollback();
                        }
                        catch (Exception ex)
                        {
                            result = false;
                            sqlTrans.Rollback();
                            procMsg = string.Format("Error in SGEImportTableData: {0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    procMsg = string.Format("Error in SGEImportTableData: {0}", ex.Message);
                }
            }
            return result;
        }
        public static bool SGEImportSourceFiles(LoginUser loginUser, string[] fileLists, bool appendMode, bool versionUpdate, bool slaveDBUpdate, ref string procMsg)
        {
            bool result = appendMode;
            try
            {
                int fileImported = 0;
                using (SqlConnection sqlConn = new SqlConnection(connStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        if (!result)
                            result =
                                DeleteTableData(sqlTrans, "NSW_ContainerStatus", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_Container", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_Connote", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_Consignment", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_ReturningOffice", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_LGA", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_Ward", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_Location", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_Venue", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_VenueType", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_Contest", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_ContainerType", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_CountCentre", ref procMsg) &&
                                DeleteTableData(sqlTrans, "NSW_BarcodeSource", ref procMsg);
                        if (result)
                        {
                            bool importError = false;
                            for (int i = 0; i < SGESourceFileList.Length; i++)
                            {
                                if (importError)
                                    break;
                                string importFile = SGESourceFileList[i];
                                for (int m = 0; m < fileLists.Length; m++)
                                {
                                    string fileFullName = fileLists[m];
                                    if (File.Exists(fileFullName))
                                    {
                                        if (string.Compare(importFile, Path.GetFileNameWithoutExtension(fileFullName), true) == 0)
                                        {
                                            using (GenericParserAdapter parser = new GenericParserAdapter(fileFullName))
                                            {
                                                parser.FirstRowHasHeader = true;
                                                DataSet dsResult = parser.GetDataSet();
                                                if (dsResult.Tables.Count > 0)
                                                {
                                                    DataTable dtSource = dsResult.Tables[0];
                                                    result = dtSource.Rows.Count > 0;
                                                    if (result)
                                                    {
                                                        switch (importFile)
                                                        {
                                                            case "Source":
                                                                result = ImportBarcodeSourceData(loginUser, sqlTrans, dtSource, appendMode, ref procMsg);
                                                                break;
                                                            case "Product":
                                                                result = ImportProductData(loginUser, sqlTrans, dtSource, appendMode, ref procMsg);
                                                                break;
                                                            case "ContainerType":
                                                                result = ImportContainerTypeData(loginUser, sqlTrans, dtSource, appendMode, ref procMsg);
                                                                break;
                                                            case "LocationType":
                                                                result = ImportLocationTypeData(loginUser, sqlTrans, dtSource, appendMode, ref procMsg);
                                                                break;
                                                            case "Area":
                                                                result = ImportAreaData(loginUser, sqlTrans, dtSource, appendMode, ref procMsg);
                                                                break;
                                                            case "Location":
                                                                result = ImportLocationData(loginUser, sqlTrans, dtSource, appendMode, ref procMsg);
                                                                break;
                                                            case "ManagementLocations":
                                                                result = ImportManagementLocationData(loginUser, sqlTrans, dtSource, appendMode, ref procMsg);
                                                                break;
                                                            case "Consignment":
                                                                result = ImportConnoteData(loginUser, sqlTrans, dtSource, appendMode, ref procMsg);
                                                                break;
                                                            case "Container":
                                                                result = ImportContainerData(loginUser, sqlTrans, dtSource, ref procMsg);
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                        importError = !result;
                                                        if (importError)
                                                        {
                                                            fileImported = 0;
                                                            break;
                                                        }
                                                        fileImported++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            result = fileImported > 0;
                            if (result && !appendMode)
                            {
                                // extra work on venue
                                result = UpdateEMODistrictVenueData(loginUser, sqlTrans, ref procMsg);
                                // extra work on df
                                if (result)
                                    result = UpdateEMODFTeamData(loginUser, sqlTrans, ref procMsg);
                                // extra work on RO
                                if (result)
                                    result = UpdateEMOCountCentreData(loginUser, sqlTrans, ref procMsg);
                                // extra work on Ward
                                if (result)
                                    result = UpdateEMODistrictWardData(loginUser, sqlTrans, ref procMsg);
                                if (result)
                                    result = UpdatePPDMVersion(sqlTrans, versionUpdate, ref procMsg);
                                
                            }
                        }
                        if (result)
                        {
                            sqlTrans.Commit();
                            procMsg = string.Format("{0} files have been imported successfully. ", fileImported);
                            if (slaveDBUpdate)
                            {
                                string procMsgEx = string.Format("{0}|{1}", loginUser.UserName, versionUpdate);
                                if (!SGEImportTableData(appendMode, ref procMsgEx))
                                    procMsg += procMsgEx;
                            }
                        }
                        else
                        {
                            sqlTrans.Rollback();
                            if (!string.IsNullOrEmpty(procMsg))
                                procMsg = string.Format("No file has been imported - {0}", procMsg);
                            else
                                procMsg = "No file has been imported";
                        }                            
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in SGEImportSourceFiles: {0}", ex.Message);
                    }
                }
                
            }
            catch(Exception ex)
            {
                result = false;
                procMsg = string.Format("Error in SGEImportSourceFiles: {0}", ex.Message);
            }
            return result;
        }

        public static DataTable GetBarcodeScanSummaryRptData(BarcodeLabelReportParam rptParam)
        {
            DataTable dtEnquiry = new DataTable();
            dtEnquiry.TableName = "Barcode_ScanSummary_Report";

            int timeOut = 60; // seconds
            object timeoutConf = ConfigurationManager.AppSettings["MaxSqlCommandTimeout"];
            if (timeoutConf != null && !string.IsNullOrEmpty(timeoutConf.ToString()))
                timeOut = Convert.ToInt32(timeoutConf);

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                string sql = "exec NSW_BarcodeScanSummaryReport @DateFrom,@DateTo";
                try
                {
                    using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.CommandTimeout = timeOut;
                        cmd.Parameters.AddWithValue("@DateFrom", rptParam.DateFrom);
                        cmd.Parameters.AddWithValue("@DateTo", rptParam.DateTo);
                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                        {
                            adapter.SelectCommand = cmd;
                            adapter.Fill(dtEnquiry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    dtEnquiry = null;
                    //enquiryParam.ProcessMessage = string.Format("Error in GetSGEBarcodeLabelRptData: {0}", ex.Message);
                }
            }

            return dtEnquiry;
        }

        public static DataTable GetSGEBarcodeLabelRptData(BarcodeLabelReportParam enquiryParam)
        {
            DataTable dtEnquiry = new DataTable();
            dtEnquiry.TableName = "Barcode_Label_Report";

            int timeOut = 60; // seconds
            object timeoutConf = ConfigurationManager.AppSettings["MaxSqlCommandTimeout"];
            if (timeoutConf != null && !string.IsNullOrEmpty(timeoutConf.ToString()))
                timeOut = Convert.ToInt32(timeoutConf);

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                string sql = "exec NSW_GetSGEContainerBarcodeReportData @SourceId,@EMOCode,@DistrictCode,@ContestAreaId,@WardCode,@VenueCode,@VenueTypeCode,@ProductCode,@ContainerCode,@ScanLocCode,@StageStatusId ";
                try
                {
                    using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.CommandTimeout = timeOut;
                        if (enquiryParam.SourceId != null && !string.IsNullOrEmpty(enquiryParam.SourceId))
                            cmd.Parameters.AddWithValue("@SourceId", enquiryParam.SourceId);
                        else
                            cmd.Parameters.AddWithValue("@SourceId", DBNull.Value);
                        if (enquiryParam.EMOCode != null && !string.IsNullOrEmpty(enquiryParam.EMOCode))
                            cmd.Parameters.AddWithValue("@EMOCode", enquiryParam.EMOCode);
                        else
                            cmd.Parameters.AddWithValue("@EMOCode", DBNull.Value);
                        if (enquiryParam.DistrictCode != null && !string.IsNullOrEmpty(enquiryParam.DistrictCode))
                            cmd.Parameters.AddWithValue("@DistrictCode", enquiryParam.DistrictCode);
                        else
                            cmd.Parameters.AddWithValue("@DistrictCode", DBNull.Value);
                        if (enquiryParam.ContestAreaCode != null && !string.IsNullOrEmpty(enquiryParam.ContestAreaCode))
                            cmd.Parameters.AddWithValue("@ContestAreaId", enquiryParam.ContestAreaCode);
                        else
                            cmd.Parameters.AddWithValue("@ContestAreaId", DBNull.Value);
                        if (enquiryParam.WardCode != null && !string.IsNullOrEmpty(enquiryParam.WardCode))
                            cmd.Parameters.AddWithValue("@WardCode", enquiryParam.WardCode);
                        else
                            cmd.Parameters.AddWithValue("@WardCode", DBNull.Value);
                        if (enquiryParam.VenueCode != null && !string.IsNullOrEmpty(enquiryParam.VenueCode))
                            cmd.Parameters.AddWithValue("@VenueCode", enquiryParam.VenueCode);
                        else
                            cmd.Parameters.AddWithValue("@VenueCode", DBNull.Value);
                        if (enquiryParam.VenueTypeCode != null && !string.IsNullOrEmpty(enquiryParam.VenueTypeCode))
                            cmd.Parameters.AddWithValue("@VenueTypeCode", enquiryParam.VenueTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@VenueTypeCode", DBNull.Value);
                        if (enquiryParam.ProductTypeCode != null && !string.IsNullOrEmpty(enquiryParam.ProductTypeCode))
                            cmd.Parameters.AddWithValue("@ProductCode", enquiryParam.ProductTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@ProductCode", DBNull.Value);
                        if (enquiryParam.ContainerTypeCode != null && !string.IsNullOrEmpty(enquiryParam.ContainerTypeCode))
                            cmd.Parameters.AddWithValue("@ContainerCode", enquiryParam.ContainerTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@ContainerCode", DBNull.Value);
                        if (enquiryParam.LastScannedLocationCode != null && !string.IsNullOrEmpty(enquiryParam.LastScannedLocationCode))
                            cmd.Parameters.AddWithValue("@ScanLocCode", enquiryParam.LastScannedLocationCode);
                        else
                            cmd.Parameters.AddWithValue("@ScanLocCode", DBNull.Value);                        
                        if (enquiryParam.LastStatusStageCode != null && !string.IsNullOrEmpty(enquiryParam.LastStatusStageCode))
                            cmd.Parameters.AddWithValue("@StageStatusId", enquiryParam.LastStatusStageCode);
                        else
                            cmd.Parameters.AddWithValue("@StageStatusId", DBNull.Value);

                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                        {
                            adapter.SelectCommand = cmd;
                            adapter.Fill(dtEnquiry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    dtEnquiry = null;
                    enquiryParam.ProcessMessage = string.Format("Error in GetSGEBarcodeLabelRptData: {0}", ex.Message);
                }
            }
            return dtEnquiry;
        }

        public static DataTable GetSGEBarcodeLabelRptDetailData(BarcodeLabelReportParam enquiryParam)
        {
            DataTable dtEnquiry = new DataTable();
            dtEnquiry.TableName = "Barcode_Label_Detail_Report";

            int timeOut = 60; // seconds
            object timeoutConf = ConfigurationManager.AppSettings["MaxSqlCommandTimeout"];
            if (timeoutConf != null && !string.IsNullOrEmpty(timeoutConf.ToString()))
                timeOut = Convert.ToInt32(timeoutConf);

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                string sql = "exec NSW_GetSGEContainerBarcodeReportDetailData @SourceId,@EMOCode,@DistrictCode,@ContestAreaId,@VenueCode,@VenueTypeCode,@ProductCode,@ContainerCode,@ScanLocCode,@StageStatusId ";
                try
                {
                    using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.CommandTimeout = timeOut;
                        if (enquiryParam.SourceId != null && !string.IsNullOrEmpty(enquiryParam.SourceId))
                            cmd.Parameters.AddWithValue("@SourceId", enquiryParam.SourceId);
                        else
                            cmd.Parameters.AddWithValue("@SourceId", DBNull.Value);
                        if (enquiryParam.EMOCode != null && !string.IsNullOrEmpty(enquiryParam.EMOCode))
                            cmd.Parameters.AddWithValue("@EMOCode", enquiryParam.EMOCode);
                        else
                            cmd.Parameters.AddWithValue("@EMOCode", DBNull.Value);
                        if (enquiryParam.DistrictCode != null && !string.IsNullOrEmpty(enquiryParam.DistrictCode))
                            cmd.Parameters.AddWithValue("@DistrictCode", enquiryParam.DistrictCode);
                        else
                            cmd.Parameters.AddWithValue("@DistrictCode", DBNull.Value);
                        if (enquiryParam.ContestAreaCode != null && !string.IsNullOrEmpty(enquiryParam.ContestAreaCode))
                            cmd.Parameters.AddWithValue("@ContestAreaId", enquiryParam.DistrictCode);
                        else
                            cmd.Parameters.AddWithValue("@ContestAreaId", DBNull.Value);
                        if (enquiryParam.VenueCode != null && !string.IsNullOrEmpty(enquiryParam.VenueCode))
                            cmd.Parameters.AddWithValue("@VenueCode", enquiryParam.VenueCode);
                        else
                            cmd.Parameters.AddWithValue("@VenueCode", DBNull.Value);
                        if (enquiryParam.VenueTypeCode != null && !string.IsNullOrEmpty(enquiryParam.VenueTypeCode))
                            cmd.Parameters.AddWithValue("@VenueTypeCode", enquiryParam.VenueTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@VenueTypeCode", DBNull.Value);
                        if (enquiryParam.ProductTypeCode != null && !string.IsNullOrEmpty(enquiryParam.ProductTypeCode))
                            cmd.Parameters.AddWithValue("@ProductCode", enquiryParam.ProductTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@ProductCode", DBNull.Value);
                        if (enquiryParam.ContainerTypeCode != null && !string.IsNullOrEmpty(enquiryParam.ContainerTypeCode))
                            cmd.Parameters.AddWithValue("@ContainerCode", enquiryParam.ContainerTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@ContainerCode", DBNull.Value);
                        if (enquiryParam.LastScannedLocationCode != null && !string.IsNullOrEmpty(enquiryParam.LastScannedLocationCode))
                            cmd.Parameters.AddWithValue("@ScanLocCode", enquiryParam.LastScannedLocationCode);
                        else
                            cmd.Parameters.AddWithValue("@ScanLocCode", DBNull.Value);
                        if (enquiryParam.LastStatusStageCode != null && !string.IsNullOrEmpty(enquiryParam.LastStatusStageCode))
                            cmd.Parameters.AddWithValue("@StageStatusId", enquiryParam.LastStatusStageCode);
                        else
                            cmd.Parameters.AddWithValue("@StageStatusId", DBNull.Value);

                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                        {
                            adapter.SelectCommand = cmd;
                            adapter.Fill(dtEnquiry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    dtEnquiry = null;
                    enquiryParam.ProcessMessage = string.Format("Error in GetSGEBarcodeLabelRptDetailData: {0}", ex.Message);
                }
            }
            return dtEnquiry;
        }

        public static DataTable GetSGEBarcodeLabelRptULDDetailData(BarcodeLabelReportParam enquiryParam)
        {
            DataTable dtEnquiry = new DataTable();
            dtEnquiry.TableName = "Barcode_Label_ULD_Detail_Report";

            int timeOut = 60; // seconds
            object timeoutConf = ConfigurationManager.AppSettings["MaxSqlCommandTimeout"];
            if (timeoutConf != null && !string.IsNullOrEmpty(timeoutConf.ToString()))
                timeOut = Convert.ToInt32(timeoutConf);

            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                string sql = "exec NSW_GetSGEContainerBarcodeReportULDDetailData @SourceId,@EMOCode,@DistrictCode,@ContestAreaId,@VenueCode,@VenueTypeCode,@ProductCode,@ContainerCode,@ScanLocCode,@StageStatusId ";
                try
                {
                    using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.CommandTimeout = timeOut;
                        if (enquiryParam.SourceId != null && !string.IsNullOrEmpty(enquiryParam.SourceId))
                            cmd.Parameters.AddWithValue("@SourceId", enquiryParam.SourceId);
                        else
                            cmd.Parameters.AddWithValue("@SourceId", DBNull.Value);
                        if (enquiryParam.EMOCode != null && !string.IsNullOrEmpty(enquiryParam.EMOCode))
                            cmd.Parameters.AddWithValue("@EMOCode", enquiryParam.EMOCode);
                        else
                            cmd.Parameters.AddWithValue("@EMOCode", DBNull.Value);
                        if (enquiryParam.DistrictCode != null && !string.IsNullOrEmpty(enquiryParam.DistrictCode))
                            cmd.Parameters.AddWithValue("@DistrictCode", enquiryParam.DistrictCode);
                        else
                            cmd.Parameters.AddWithValue("@DistrictCode", DBNull.Value);
                        if (enquiryParam.ContestAreaCode != null && !string.IsNullOrEmpty(enquiryParam.ContestAreaCode))
                            cmd.Parameters.AddWithValue("@ContestAreaId", enquiryParam.DistrictCode);
                        else
                            cmd.Parameters.AddWithValue("@ContestAreaId", DBNull.Value);
                        if (enquiryParam.VenueCode != null && !string.IsNullOrEmpty(enquiryParam.VenueCode))
                            cmd.Parameters.AddWithValue("@VenueCode", enquiryParam.VenueCode);
                        else
                            cmd.Parameters.AddWithValue("@VenueCode", DBNull.Value);
                        if (enquiryParam.VenueTypeCode != null && !string.IsNullOrEmpty(enquiryParam.VenueTypeCode))
                            cmd.Parameters.AddWithValue("@VenueTypeCode", enquiryParam.VenueTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@VenueTypeCode", DBNull.Value);
                        if (enquiryParam.ProductTypeCode != null && !string.IsNullOrEmpty(enquiryParam.ProductTypeCode))
                            cmd.Parameters.AddWithValue("@ProductCode", enquiryParam.ProductTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@ProductCode", DBNull.Value);
                        if (enquiryParam.ContainerTypeCode != null && !string.IsNullOrEmpty(enquiryParam.ContainerTypeCode))
                            cmd.Parameters.AddWithValue("@ContainerCode", enquiryParam.ContainerTypeCode);
                        else
                            cmd.Parameters.AddWithValue("@ContainerCode", DBNull.Value);
                        if (enquiryParam.LastScannedLocationCode != null && !string.IsNullOrEmpty(enquiryParam.LastScannedLocationCode))
                            cmd.Parameters.AddWithValue("@ScanLocCode", enquiryParam.LastScannedLocationCode);
                        else
                            cmd.Parameters.AddWithValue("@ScanLocCode", DBNull.Value);
                        if (enquiryParam.LastStatusStageCode != null && !string.IsNullOrEmpty(enquiryParam.LastStatusStageCode))
                            cmd.Parameters.AddWithValue("@StageStatusId", enquiryParam.LastStatusStageCode);
                        else
                            cmd.Parameters.AddWithValue("@StageStatusId", DBNull.Value);

                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                        {
                            adapter.SelectCommand = cmd;
                            adapter.Fill(dtEnquiry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    dtEnquiry = null;
                    enquiryParam.ProcessMessage = string.Format("Error in GetSGEBarcodeLabelRptULDDetailData: {0}", ex.Message);
                }
            }
            return dtEnquiry;
        }

        public static DataTable GetSGEULDChildContainerBarcodeDataTable(object uldBarcode, object uldConsKey)
        {
            string sqlStr = "exec NSW_GetSGEULDChildContainerBarcodeData";
            if (uldBarcode != null && !string.IsNullOrEmpty(uldBarcode.ToString()))
                sqlStr = "exec NSW_GetSGEULDChildContainerBarcodeData @ULDBarcode";
            else
                if (uldConsKey != null && !string.IsNullOrEmpty(uldConsKey.ToString()))
                    sqlStr = "exec NSW_GetSGEULDChildContainerBarcodeData null, @ULDConsKey";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(connStr))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (uldBarcode != null && !string.IsNullOrEmpty(uldBarcode.ToString()))
                    sqlCmd.Parameters.AddWithValue("@ULDBarcode", uldBarcode);
                else
                    if (uldConsKey != null && !string.IsNullOrEmpty(uldConsKey.ToString()))
                        sqlCmd.Parameters.AddWithValue("@ULDConsKey", uldConsKey);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        public static string DataTableToCSV(DataTable table, string delimiter, bool includeHeader)
        {
            StringBuilder result = new StringBuilder();

            if (includeHeader)
            {
                foreach (DataColumn column in table.Columns)
                {
                    result.Append(column.ColumnName);
                    result.Append(delimiter);
                }

                result.Remove(--result.Length, 0);
                result.Append(Environment.NewLine);
            }

            foreach (DataRow row in table.Rows)
            {
                foreach (object item in row.ItemArray)
                {
                    if (item is DBNull)
                        result.Append(delimiter);
                    else
                    {
                        string itemAsString = item.ToString();
                        // Double up all embedded double quotes
                        itemAsString = itemAsString.Replace("\"", "\"\"");

                        // To keep things simple, always delimit with double-quotes
                        // so we don't have to determine in which cases they're necessary
                        // and which cases they're not.
                        itemAsString = String.Format("\"{0}\"", itemAsString);

                        result.Append(itemAsString + delimiter);
                    }
                }
                result.Remove(--result.Length, 0);
                result.Append(Environment.NewLine);
            }
            return result.ToString();
        }

        public static byte[] DataSetToCSVZip(DataSet dsSource)
        {
            byte[] compressedBytes;
            byte[] fileBytes;
            using (var outStream = new MemoryStream())
            {
                using (var archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    foreach(DataTable dt in dsSource.Tables)
                    {
                        fileBytes = Encoding.ASCII.GetBytes(DataTableToCSV(dt, ",", true));
                        string fileName = String.Format("{0}_{1}.csv", dt.TableName, DateTime.Now.ToString("yyyyMMddHHmmss"));
                        var fileInArchive = archive.CreateEntry(fileName, CompressionLevel.Optimal);
                        using (var entryStream = fileInArchive.Open())
                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                        {
                            fileToCompressStream.CopyTo(entryStream);
                        }
                    }                    
                }
                compressedBytes = outStream.ToArray();
            }
            return compressedBytes;
        }
    }

    public class BarcodeLabelReportParam
    {
        public string SourceId { get; set; }
        public string EMOCode { get; set; }
        public string DistrictCode { get; set; }
        public string ContestAreaCode { get; set; }
        public string WardCode { get; set; }
        public string VenueCode { get; set; }
        public string VenueTypeCode { get; set; }
        public string ProductTypeCode { get; set; }
        public string ContainerTypeCode { get; set; }
        public string LastScannedLocationCode { get; set; }
        public string LastStatusStageCode { get; set; }
        public bool IncludeStatusHistory { get; set; }
        public bool IncludeULDChild { get; set; }
        public bool ExportToCSV { get; set; }
        public string ProcessMessage { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }

    public enum EventType
    {
        NOT_DEFINED, LOCAL_GOVERNMENT, STATE_BY_ELECTION, STATE_ELECTION
    }

    public enum ContainerType
    {
        Carton = 1, 
        Bag = 2, 
        Ballot_Box = 3
    }
}