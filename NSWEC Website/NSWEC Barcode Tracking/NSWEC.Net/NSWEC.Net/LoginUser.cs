﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using SMCryptography;

namespace NSWEC.Net
{
    public class LoginUser
    {
        private string _userName = string.Empty;
        private string _passWord = string.Empty;
        private string _pageTheme = string.Empty;
        private bool _specialUser = false;
        private int _sessionTimeOut = 0;
        private EventType _eventType = EventType.NOT_DEFINED;
        private string _labelDesignerLink = string.Empty;
        private Queue _labelViewerIdQueue = null;
        private string cdid = null;
        private string returningOffice = null;
        //private bool _isAdmin = false;
        private string _sessionId = string.Empty;
        private string _frmAuthCookie = string.Empty;

        private static string _url = "~/Label Designer/SupplyMaster Label Designer.application?qs=";
        private static string _passPhrase = "5upplyM@5t3r";//"3cB@rc0d3Tr@k1ng";
        private static string _initVector = "h8G7f6E5d4C3b2@1";
        private static string _urlPassPhrase = "5upplyM@5t3r0nl1n3";// "3cB@rc0d3Tr@k1ng0nl1n3";

        public LoginUser() { }
        public LoginUser(string userName, string passWord, bool specialUser)
        {
            CryptographyRijndael _key = new CryptographyRijndael(_passPhrase, _initVector);
            _userName = userName;
            _passWord = _key.Encrypt(passWord);
            while (_passWord.Contains("+"))
                _passWord = _key.Encrypt(passWord);

            CryptographyRijndael _keyLink = new CryptographyRijndael(_urlPassPhrase, _initVector);
            _labelDesignerLink = _keyLink.Encrypt(string.Format("username={0}&password={1}", _userName, _passWord));
            while (_labelDesignerLink.Contains("+"))
                _labelDesignerLink = _keyLink.Encrypt(string.Format("username={0}&password={1}", _userName, _passWord));
            _labelDesignerLink = string.Format("{0}{1}", _url, _labelDesignerLink);

            _specialUser = specialUser;
            //_isAdmin = _specialUser;
            if (!_specialUser)
            {
                DataTable dtLogin = ECSecurityClass.GetLoginCatalogueDataTable(_userName);
                if (dtLogin != null && dtLogin.Rows.Count > 0)
                {
                    DataRow drLogin = dtLogin.Rows[0];
                    if (!(drLogin["CDID"] is DBNull) && drLogin["CDID"].ToString() != string.Empty)
                        cdid = drLogin["CDID"].ToString();
                    if (!(drLogin["ROName"] is DBNull) && drLogin["ROName"].ToString() != string.Empty)
                        returningOffice = drLogin["ROName"].ToString();
                    if (!(drLogin["WebPageTheme"] is DBNull) && drLogin["WebPageTheme"].ToString() != string.Empty)
                        _pageTheme = drLogin["WebPageTheme"].ToString();
                    //_isAdmin = ECSecurityClass.loginIsAdmin(_userName);
                }
            }
            DataTable dtApp = ECSecurityClass.GetApplicationDataTable();
            if (dtApp != null && dtApp.Rows.Count > 0)
            {
                if (!(dtApp.Rows[0]["PreferredEvent"] is DBNull) && dtApp.Rows[0]["PreferredEvent"].ToString() != string.Empty)
                    _eventType = (EventType)dtApp.Rows[0]["PreferredEvent"];
                if (!(dtApp.Rows[0]["SessionTimeOut"] is DBNull) && dtApp.Rows[0]["SessionTimeOut"].ToString() != string.Empty)
                    _sessionTimeOut = Convert.ToInt32(dtApp.Rows[0]["SessionTimeOut"]);
            }
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
        }
        public string Password
        {
            get
            {
                return _passWord;
            }
        }
        public string PageTheme
        {
            get
            {
                return _pageTheme;
            }
            set
            {
                if (_pageTheme == value)
                    return;
                _pageTheme = value;
            }
        }
        public bool SpecialUser
        {
            get
            {
                return _specialUser;
            }
        }
        public int SessionTimeOut
        {
            get
            {
                return _sessionTimeOut;
            }
        }
        
        public EventType EventType
        {
            get
            {
                return _eventType;
            }
            set
            {
                if (_eventType == value)
                    return;
                _eventType = value;
            }
        }
        public string LabelDesignerLink
        {
            get { return _labelDesignerLink; }
        }

        public Queue LabelViewerIdQueue
        {
            get
            {
                if (_labelViewerIdQueue == null)
                    _labelViewerIdQueue = new Queue();
                return _labelViewerIdQueue;
            }
        }

        public string CDID
        {
            get
            {
                return cdid;
            }
            set
            {
                if (cdid == value)
                    return;
                cdid = value;
            }
        }
        public string ReturningOffice
        {
            get
            {
                return returningOffice;
            }
            set
            {
                if (returningOffice == value)
                    return;
                returningOffice = value;
            }
        }
        public bool IsAdmin
        {
            get
            {
                return _specialUser || ECSecurityClass.loginIsAdmin(_userName);// _isAdmin;
            }
        }
        public string SessionId
        {
            get
            {
                return _sessionId;
            }
            set
            {
                if (_sessionId == value)
                    return;
                _sessionId = value;
            }
        }
        public string FrmAuthCookie
        {
            get
            {
                return _frmAuthCookie;
            }
            set
            {
                if (_frmAuthCookie == value)
                    return;
                _frmAuthCookie = value;
            }
        }
        public void AddLabelPageId(System.Web.UI.Page page, string labelPageId)
        {
            int maxPageIdCount;
            object maxPageIdCountConf = ConfigurationManager.AppSettings["MaxLabelPageIdCount"];
            if (maxPageIdCountConf != null && !string.IsNullOrEmpty(maxPageIdCountConf.ToString()))
                maxPageIdCount = Convert.ToInt32(maxPageIdCountConf);
            else
                maxPageIdCount = 10;

            if (LabelViewerIdQueue.Count == maxPageIdCount)
            {
                object oldestId = LabelViewerIdQueue.Dequeue();
                if (oldestId != null && !string.IsNullOrEmpty(oldestId.ToString()))
                {
                    if (page.Session[string.Format("{0}.LabelLayoutDataTable", oldestId)] != null)
                        page.Session.Remove(string.Format("{0}.LabelLayoutDataTable", oldestId));
                    if (page.Session[string.Format("{0}.ConnoteLabelDataSet", oldestId)] != null)
                        page.Session.Remove(string.Format("{0}.ConnoteLabelDataSet", oldestId));
                }
            }
            LabelViewerIdQueue.Enqueue(labelPageId);
        }
    }
}