/*
USE [NSWEC]
GO
	drop table NSW_ProgressiveCount
	drop table NSW_Event
	drop table NSW_ReturningOffice
	drop table NSW_LGA
	drop table NSW_Ward
	drop table NSW_Location
	drop table NSW_Venue
	drop table NSW_Consignment
	drop table NSW_Connote
	drop table NSW_Container
	drop table NSW_ContainerStatus
	drop table NSW_Contest
	drop table NSW_ContainerType
	drop table NSW_VenueType
	drop table NSW_CountCentre
	drop table NSW_BarcodeSource
	drop table NSW_StatusStage
	
	select * from NSW_Consignment
	select * from NSW_Connote
	select * from NSW_Container
	order by ContainerNo
	
	exec NSW_GetConnoteCatalogueGeneric
	
	select distinct ContainerNo from NSW_Container
	
	update SecurityLogin set cdid=null
	
*/
-- drop table NSW_Application

if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Application') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_Application(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[WebApplicationTitle] [varchar](100) NULL,
		[CopyRightMessage] [varchar](100) NULL,
		[WebInfo] [varchar](max) NULL,
		[SessionTimeOut] [smallint] NULL,
		[MinPasswordChars] [int] NULL,
		[MinPasswordUppercaseChars] [int] NULL,
		[MinPasswordSpecialChars] [int] NULL,
		[PasswordSpecialCharList] [varchar](100) NULL,
		[DaysBeforePasswordExpires] [int] NULL,
		[SupportLogin] [varchar](100) NULL,
		[SupportPassword] [varchar](100) NULL,
		[IncomingVia] [varchar](5) NULL,
		[IncomingMailServer] [varchar](50) NULL,
		[IncomingMailPort] [int] NULL,
		[IncomingMailUserName] [varchar](50) NULL,
		[IncomingMailPassword] [varchar](50) NULL,
		[IncomingMailbox] [varchar](50) NULL,
		[OutgoingMailHost] [varchar](50) NULL,
		[OutgoingMailPort] [int] NULL,
		[OutgoingMailUserName] [varchar](50) NULL,
		[OutgoingMailPassword] [varchar](50) NULL,
		[OutgoingMailFromAddress] [varchar](50) NULL,
		[OutgoingMailFromName] [varchar](50) NULL,
		[MinPasswordNumericChars] [int] NULL,
		[MinPasswordLength] [int] NULL,
		[MaxPasswordLength] [int] NULL,
		[Notice] [varchar](max) NULL,
		[PreferredEvent] [int] NULL,
		[PreferredLabel] [int] NULL,
		[PPDMVersion] [int] NULL,
		[PPDMLoaded] [datetime] NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_NSW_Application] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_SessionTimeOut]  DEFAULT ((20)) FOR [SessionTimeOut]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordChars]  DEFAULT (8) FOR [MinPasswordChars]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordUppercaseChars]  DEFAULT (1) FOR [MinPasswordUppercaseChars]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordSpecialChars]  DEFAULT (0) FOR [MinPasswordSpecialChars]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_DaysBeforePasswordExpires]  DEFAULT (0) FOR [DaysBeforePasswordExpires]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordNumericChars]  DEFAULT (1) FOR [MinPasswordNumericChars]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordLength]  DEFAULT (6) FOR [MinPasswordLength]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MaxPasswordLength]  DEFAULT (20) FOR [MaxPasswordLength]
end
GO

if not exists(select * from syscolumns where name = 'PPDMVersion'
	and id = (select id from sysobjects where id = object_id(N'NSW_Application') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Application Add [PPDMVersion] [int] NULL
	Alter Table NSW_Application Add [PPDMLoaded] [datetime] NULL
end
go

-- drop table NSW_CountCentre
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_CountCentre') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_CountCentre(
		[ID] [varchar](40) NOT NULL,
		[EventId] [varchar](20) NULL,
		[Code] [varchar](5) NULL,
		[Name] [varchar](100) NULL,		
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_CountCentre_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_CountCentre ADD CONSTRAINT [DF_NSW_CountCentre_Active] DEFAULT ('Y') FOR [Active]
end
GO
/*
if not exists(select 1 from NSW_CountCentre)
begin

	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select '09992','Queanbeyan','Y',GETDATE(),'smsupport'
	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select '09993','Newcastle','Y',GETDATE(),'smsupport'
	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select '09994','Orange','Y',GETDATE(),'smsupport'
	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select '09995','Riverwood','Y',GETDATE(),'smsupport'

	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select 'CDVC','Centralised Declareation Vote Centre','Y',GETDATE(),'smsupport'
	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select 'LCCC','Legislative Council Count Centre','Y',GETDATE(),'smsupport'
	
end
GO
*/
-- drop table NSW_ReturningOffice
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_ReturningOffice') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_ReturningOffice(
		[ID] [varchar](40) NOT NULL,
		[EventId] [varchar](20) NULL,
		[Code] [varchar](3) NOT NULL,
		[Name] [varchar](100) NULL,
		[AddressLine1] [varchar](100) NULL,
		[AddressLine2] [varchar](100) NULL,
		[Suburb] [varchar](40) NULL,
		[State] [varchar](10) NULL,
		[PostCode] [varchar](10) NULL,
		[Country] [varchar](40) NULL,
		[Contact] [varchar](40) NULL,
		[DirectPhone] [varchar](20) NULL,
		[OfficePhone] [varchar](20) NULL,
		[Email] [varchar](300) NULL,
		[SpecialInstructions] [varchar](1000) NULL,
		[LocationTypeCode] [varchar](2) NULL,
		[LocationTypeName] [varchar](100) NULL,
		[CountCentreCode] [varchar](5) NULL, 
		[CountCentreName] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_ReturningOffice_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_ReturningOffice ADD CONSTRAINT [DF_NSW_ReturningOffice_Active] DEFAULT ('Y') FOR [Active]
end
GO

-- drop table NSW_LGA
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_LGA') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_LGA(
		[ID] [varchar](40) NOT NULL,
		[EventId] [varchar](20) NULL,
		[ROCode] [varchar](3) NULL,
		[ROName] [varchar](100) NULL,
		[Code] [varchar](5) NOT NULL,
		[AreaCode] [varchar](100) NOT NULL,
		[AreaAuthorityName] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_LGA_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_LGA ADD CONSTRAINT [DF_NSW_LGA_Active] DEFAULT ('Y') FOR [Active]
end
GO

if exists(select * from syscolumns where name = 'Code' and length < 5 
	and id = (select id from sysobjects where id = object_id(N'NSW_LGA') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_LGA Alter Column [Code] [varchar](5) NOT NULL
end
go

if exists(select * from syscolumns where name = 'AreaCode' and length = 40 
	and id = (select id from sysobjects where id = object_id(N'NSW_LGA') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_LGA Alter Column [AreaCode] [varchar](100) NOT NULL
end
go

-- drop table NSW_Ward
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Ward') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Ward(
		[ID] [varchar](40) NOT NULL,
		[EventId] [varchar](20) NULL,
		[ROCode] [varchar](3) NULL,
		[ROName] [varchar](100) NULL,
		[LGAId] [varchar](5) NOT NULL,
		[LGACode] [varchar](100) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Ward_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Ward ADD CONSTRAINT [DF_NSW_Ward_Active] DEFAULT ('Y') FOR [Active]
end
GO

if exists(select * from syscolumns where name = 'LGAId' and length < 5
	and id = (select id from sysobjects where id = object_id(N'NSW_Ward') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Ward Alter Column [LGAId] [varchar](5) NOT NULL
end
go

if exists(select * from syscolumns where name = 'LGACode' and length = 40 
	and id = (select id from sysobjects where id = object_id(N'NSW_Ward') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Ward Alter Column [LGACode] [varchar](100) NOT NULL
end
go

-- drop table NSW_Location
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Location') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Location(
		[ID] [varchar](40) NOT NULL,
		[Code] [varchar](5) NULL,
		[Name] [varchar](100) NULL,	
		[LongName] [varchar](200) NULL,	
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Location_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if not exists(select * from syscolumns where name = 'Modified'
	and id = (select id from sysobjects where id = object_id(N'NSW_Location') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Location Add [Modified] [datetime] NULL
	Alter Table NSW_Location Add [ModifiedBy] [varchar](100) NULL
end
go

-- drop table NSW_Venue
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Venue') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Venue(
		[ID] [varchar](40) NOT NULL,
		[EventId] [varchar](20) NULL,
		[ROCode] [varchar](3) NULL,
		[ROName] [varchar](100) NULL,
		[LGACode] [varchar](5) NULL,
		[LGAName] [varchar](100) NULL,
		[WardCode] [varchar](5) NULL,
		[WardName] [varchar](100) NULL,
		[Code] [varchar](5) NOT NULL,
		[Name] [varchar](100) NULL,
		[LongName] [varchar](200) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,
 		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Venue_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Venue ADD CONSTRAINT [DF_NSW_Venue_Active] DEFAULT ('Y') FOR [Active]
end
GO

if exists(select * from syscolumns where name = 'LGACode' and length < 5
	and id = (select id from sysobjects where id = object_id(N'NSW_Venue') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Venue Alter Column [LGACode] [varchar](5) NULL
end
go

if exists(select * from syscolumns where name = 'WardCode' and length < 5 
	and id = (select id from sysobjects where id = object_id(N'NSW_Venue') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Venue Alter Column [WardCode] [varchar](5) NULL
end
go

-- drop table NSW_VenueType
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_VenueType') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_VenueType(
		[ID] [varchar](40) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_VenueType_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_VenueType ADD CONSTRAINT [DF_NSW_VenueType_Active] DEFAULT ('Y') FOR [Active]
end
GO
-- drop table NSW_Contest
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Contest') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Contest(
		[ID] [varchar](40) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Description] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Contest_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Contest ADD CONSTRAINT [DF_NSW_Contest_Active] DEFAULT ('Y') FOR [Active]
end
GO
-- drop table NSW_ProgressiveCount
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_ProgressiveCount') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_ProgressiveCount(
		[ID] [varchar](40) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Description] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_ProgressiveCount_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_ProgressiveCount ADD CONSTRAINT [DF_NSW_ProgressiveCount_Active] DEFAULT ('Y') FOR [Active]
end
GO
-- drop table NSW_ContainerType
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_ContainerType') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_ContainerType(
		[ID] [varchar](40) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_ContainerType_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_ContainerType ADD CONSTRAINT [DF_NSW_ContainerType_Active] DEFAULT ('Y') FOR [Active]
end
GO

-- drop table NSW_Event
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Event') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Event(
		[ID] [varchar](40) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Event_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Event ADD CONSTRAINT [DF_NSW_Event_Active] DEFAULT ('Y') FOR [Active]
end
GO

-- drop table NSW_Consignment
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Consignment') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_Consignment(
		[ID] [varchar](40) NOT NULL,
		[SequentialId] [int] NULL,
		[EventId] [varchar](20) NULL,
		[SourceId] [int] NULL,
		[Source] [varchar](100) NULL,
		[CDID] [varchar](3) NULL,
		[ReturningOfficeName] [varchar](100) NULL,
		[AreaAuthorityName] [varchar](100) NULL,
		[DeliveryAddress] [varchar](200) NULL,
		[LGAId] [varchar](5) NULL,
		[LGACode] [varchar](100) NULL,			
		[LGAName] [varchar](100) NULL,
		[ContestAreaId] [varchar](5) NULL,
		[ContestAreaCode] [varchar](100) NULL,
		[WardCode] [varchar](5) NULL,
		[WardName] [varchar](100) NULL,
		[VenueCode] [varchar](5) NULL,
		[VenueName] [varchar](100) NULL,
		[VenueLongName] [varchar](200) NULL,
		[VenueAddress] [varchar](200) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,		
		[ContestCode] [varchar](2) NULL,
		[ContestName] [varchar](100) NULL,
		[ContainerCode] [varchar](2) NULL,
		[ContainerName] [varchar](100) NULL,
		[ContainerQuantity] [int] NULL,
		[DeliveryNumber] [int] NULL,
		[CountCentreCode] [varchar](5) NULL, 
		[CountCentreName] [varchar](100) NULL,
		[ProductCode] [varchar](40) NULL,
		[ProductDescription] [varchar](100) NULL,
		[PrinterJobId] [varchar](4) NULL,
		[PrinterQuantity] [int] NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL
	 CONSTRAINT [NSW_Consignment_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Consignment ADD CONSTRAINT [DF_NSW_Consignment_ContainerQuantity]  DEFAULT ((0)) FOR [ContainerQuantity]
end
GO

if exists(select * from syscolumns where name = 'LGAId' and length < 5 
	and id = (select id from sysobjects where id = object_id(N'NSW_Consignment') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Consignment Alter Column [LGAId] [varchar](5) NULL
end
go

if exists(select * from syscolumns where name = 'LGACode' and length = 40 
	and id = (select id from sysobjects where id = object_id(N'NSW_Consignment') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Consignment Alter Column [LGACode] [varchar](100) NULL
end
go

if exists(select * from syscolumns where name = 'ContestAreaId' and length < 5 
	and id = (select id from sysobjects where id = object_id(N'NSW_Consignment') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Consignment Alter Column [ContestAreaId] [varchar](5) NULL
end
go

if exists(select * from syscolumns where name = 'ContestAreaCode' and length = 40 
	and id = (select id from sysobjects where id = object_id(N'NSW_Consignment') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Consignment Alter Column [ContestAreaCode] [varchar](100) NULL
end
go

if exists(select * from syscolumns where name = 'WardCode' and length < 5 
	and id = (select id from sysobjects where id = object_id(N'NSW_Consignment') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Consignment Alter Column [WardCode] [varchar](5) NULL
end
go

-- drop table NSW_Connote
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Connote') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_Connote(
		[ConsKey] [varchar](40) NOT NULL,
		[ConsNo] [varchar](40) NULL,
		[EventId] [varchar](20) NULL,
		[SourceId] [varchar](2) NULL,
		[Source] [varchar](100) NULL,
		[CDID] [varchar](3) NULL,
		[ReturningOfficeName] [varchar](100) NULL,
		[LGAId] [varchar](5) NULL,
		[LGACode] [varchar](100) NULL,		
		[LGAName] [varchar](100) NULL,		
		[ContestAreaId] [varchar](5) NULL,
		[ContestAreaCode] [varchar](100) NULL,		
		[WardCode] [varchar](5) NULL,
		[WardName] [varchar](100) NULL,
		[VenueCode] [varchar](5) NULL,
		[VenueName] [varchar](100) NULL,
		[VenueLongName] [varchar](200) NULL,
		[VenueAddress] [varchar](200) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,		
		[ContestCode] [varchar](2) NULL,
		[ContestName] [varchar](100) NULL,
		[ContainerCode] [varchar](2) NULL,
		[ContainerName] [varchar](100) NULL,
		[ContainerQuantity] [int] NULL,
		[AdditionalContainerQuantity] [int] NULL,
		[DeliveryNumber] [int] NULL,
		[CountCentreCode] [varchar](5) NULL, 
		[CountCentreName] [varchar](100) NULL,
		[PrinterJobId] [varchar](4) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
		[Deleted] [datetime] NULL,
		[DeletedBy] [varchar](100) NULL,
		[DeletedReason] [varchar](1000) NULL,
	 CONSTRAINT [NSW_Connote_PK] PRIMARY KEY CLUSTERED 
	(
		[ConsKey] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Connote ADD CONSTRAINT [DF_NSW_Connote_ContainerQuantity]  DEFAULT ((0)) FOR [ContainerQuantity]
	ALTER TABLE NSW_Connote ADD CONSTRAINT [DF_NSW_Connote_AdditionalContainerQuantity]  DEFAULT ((0)) FOR [AdditionalContainerQuantity]
end
GO

if exists(select * from syscolumns where name = 'LGAId' and length < 5 
	and id = (select id from sysobjects where id = object_id(N'NSW_Connote') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Connote Alter Column [LGAId] [varchar](5) NULL
end
go

if exists(select * from syscolumns where name = 'LGACode' and length = 40 
	and id = (select id from sysobjects where id = object_id(N'NSW_Connote') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Connote Alter Column [LGACode] [varchar](100) NULL
end
go

if exists(select * from syscolumns where name = 'ContestAreaId' and length < 5 
	and id = (select id from sysobjects where id = object_id(N'NSW_Connote') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Connote Alter Column [ContestAreaId] [varchar](5) NULL
end
go

if exists(select * from syscolumns where name = 'ContestAreaCode' and length = 40 
	and id = (select id from sysobjects where id = object_id(N'NSW_Connote') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Connote Alter Column [ContestAreaCode] [varchar](100) NULL
end
go

if exists(select * from syscolumns where name = 'WardCode' and length < 5 
	and id = (select id from sysobjects where id = object_id(N'NSW_Connote') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Connote Alter Column [WardCode] [varchar](5) NULL
end
go

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'NSW_Connote_DeleteTrigger'))
	DROP TRIGGER NSW_Connote_DeleteTrigger
GO

CREATE TRIGGER NSW_Connote_DeleteTrigger ON NSW_Connote
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ConsKey [varchar](40)
	SELECT @ConsKey=ConsKey FROM deleted
	
	if exists(select 1 from NSW_ContainerStatus where ConsKey=@ConsKey)
		delete from NSW_ContainerStatus where ConsKey=@ConsKey
	
	if exists(select 1 from NSW_Container where ConsKey=@ConsKey)
		delete from NSW_Container where ConsKey=@ConsKey
			
	SET NOCOUNT OFF
END
Go

-- drop table NSW_Container
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Container') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_Container(
		[ConsKey] [varchar](40) NOT NULL,
		[LineNo] [int] NOT NULL,
		[ContainerNo] [varchar](40) NULL,
		[Barcode] [varchar](40) NULL,
		[VenueName] [varchar](100) NULL,
		[ParentBarcode] [varchar](40) NULL,
		[ReplaceBarcode] [varchar](40) NULL,
		[ULDProduct] [varchar](2) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
		[Deleted] [datetime] NULL,
		[DeletedBy] [varchar](100) NULL,
		[DeletedReason] [varchar](1000) NULL,
		[Added] [datetime] NULL,
		[AddedBy] [varchar](100) NULL,
		[Activated] [char](1) NULL,
	 CONSTRAINT [NSW_Container_PK] PRIMARY KEY CLUSTERED 
	(
		[ConsKey] ASC,
		[LineNo] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Container ADD CONSTRAINT [DF_NSW_Container_Activated] DEFAULT ('Y') FOR [Activated]
end
GO

if not exists(select * from syscolumns where name = 'ULDProduct'
	and id = (select id from sysobjects where id = object_id(N'NSW_Container') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Container Add [ULDProduct] [varchar](2) NULL
end
go

-- drop table NSW_ContainerStatus
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_ContainerStatus') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_ContainerStatus(
		[ConsKey] [varchar](40) NOT NULL,
		[LineNo] [int] NOT NULL,
		[StatusId] [varchar](40) NOT NULL,
		[StatusDate] [datetime] NOT NULL,
		[StatusTime] [varchar](10) NOT NULL,
		[StatusNotes] [varchar](500) NULL,
		[MovementDirection] [varchar](500) NULL,
		[CountCentreCode] [varchar](5) NULL, 
		[CountCentreName] [varchar](100) NULL, 
		[ConsNo] [varchar](40) NULL,
		[ContainerNo] [varchar](40) NULL,
		[PrinterJobId] [varchar](4) NULL,
		[Barcode] [varchar](40) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
		[Duplicated] [char](1) NULL,
		[Deleted] [datetime] NULL,
		[DeletedBy] [varchar](100) NULL,
		[DeletedReason] [varchar](1000) NULL,		
	 CONSTRAINT [NSW_ContainerStatus_PK] PRIMARY KEY CLUSTERED 
	(
		[ConsKey] ASC,
		[LineNo] ASC,
		--[StatusDate] ASC,
		--[StatusTime] ASC
		[StatusId] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if not exists(select * from syscolumns where name = 'Duplicated' 
	and id = (select id from sysobjects where id = object_id(N'NSW_ContainerStatus') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_ContainerStatus Add [Duplicated] [char](1) NULL
end
go

if not exists(select * from syscolumns where name = 'Deleted' 
	and id = (select id from sysobjects where id = object_id(N'NSW_ContainerStatus') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_ContainerStatus Add [Deleted] [datetime] NULL
	Alter Table NSW_ContainerStatus Add [DeletedBy] [varchar](100) NULL
	Alter Table NSW_ContainerStatus Add [DeletedReason] [varchar](1000) NULL
end
go

-- drop table NSW_Container
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_BarcodeSource') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_BarcodeSource(
		[ID] [varchar](40) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[BarcodeStart] [varchar](10) NULL,
		[BarcodeStop] [varchar](10) NULL,		
		[BarcodeLength] [int] NULL,		
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [NSW_BarcodeSource_PK] PRIMARY KEY CLUSTERED 	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_BarcodeSource ADD CONSTRAINT [DF_NSW_BarcodeSource_BarcodeLength] DEFAULT (0) FOR [BarcodeLength]
	ALTER TABLE NSW_BarcodeSource ADD CONSTRAINT [DF_NSW_BarcodeSource_Active] DEFAULT ('Y') FOR [Active]
end
GO


-- drop table NSW_LabelFormat
if not exists (select * from dbo.sysobjects where id = object_id(N'SM_ONLINE_LABEL') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE SM_ONLINE_LABEL(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LABEL_NAME] [varchar](50) NULL,
		[DESCRIPTION] [varchar](100) NULL,
		[LAYOUT] [varbinary](max) NULL,
		[STORED_PROCEDURE] [varchar](30) NULL,
		[CREATED_DATE] [datetime] NULL,
		[CREATED_BY] [varchar](100) NULL,
		[MODIFIED_DATE] [datetime] NULL,
		[MODIFIED_BY] [varchar](100) NULL,
		[ACTIVE] [char](1) NOT NULL,
	 CONSTRAINT [PK_SCAN_PRINT_LABEL] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	ALTER TABLE SM_ONLINE_LABEL ADD  CONSTRAINT [DF_SMOnlineLabel_Active]  DEFAULT ('Y') FOR [ACTIVE]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityLogin') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE SecurityLogin(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LoginId] [varchar](100) NOT NULL,
		[Password] [varchar](200) NOT NULL,
		[PasswordExpires] [datetime] NULL,
		[WebPageTheme] [varchar](20) NULL,
		
		[Salutation] [varchar](30) NULL,
		[FirstName] [varchar](30) NULL,
		[FamilyName] [varchar](30) NULL,
		[Phone] [varchar](20) NULL,
		[Mobile] [varchar](20) NULL,
		[Fax] [varchar](20) NULL,
		[Email] [varchar](255) NULL,
		[Comments] [varchar](1000) NULL,
		[CDID] [varchar](3) NULL,
		
		[Active] [char](1) NOT NULL,
		[Verified] [char](1) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityLogin] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityLogin ADD  CONSTRAINT [DF_SecurityLogin_Active]  DEFAULT ('N') FOR [Active]
	ALTER TABLE SecurityLogin ADD  CONSTRAINT [DF_SecurityLogin_Verified]  DEFAULT ('N') FOR [Verified]
end
GO

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'SecurityLogin_DeleteTrigger'))
	DROP TRIGGER SecurityLogin_DeleteTrigger
GO

CREATE TRIGGER SecurityLogin_DeleteTrigger ON SecurityLogin
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ID INT, @ContactId int, @LoginId varchar(100)
	SELECT @ID=ID, @LoginId=LoginId FROM deleted
		
	DELETE FROM SecurityLoginRole
	WHERE SecurityLogin=@ID
	
	delete from NSW_GridLayout
	where CreatedBy=@LoginId
				
	SET NOCOUNT OFF
END
Go

if not exists(select * from syscolumns where name = 'CDID'
	and id = (select id from sysobjects where id = object_id(N'SecurityLogin') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table SecurityLogin Add [CDID] [varchar](3) NULL
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityRole') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE SecurityRole(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityRole] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityRole ADD  CONSTRAINT [DF_SecurityRole_Active]  DEFAULT ('Y') FOR [Active]
end
GO

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'SecurityRole_DeleteTrigger'))
	DROP TRIGGER SecurityRole_DeleteTrigger
GO

CREATE TRIGGER SecurityRole_DeleteTrigger ON SecurityRole
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ID INT
	SELECT @ID=ID FROM deleted
	
	DELETE FROM SecurityRoleFunction
	WHERE SecurityRole=@ID
	
	DELETE FROM SecurityLoginRole
	WHERE SecurityRole=@ID
		
	SET NOCOUNT OFF
END
Go

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityLoginRole') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityLoginRole(
		[SecurityLogin] [int] NOT NULL,
		[SecurityRole] [int] NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityLoginRole] PRIMARY KEY CLUSTERED 
	(
		[SecurityLogin] ASC,
		[SecurityRole] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityFunctionCategory') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityFunctionCategory(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityFunctionCategory] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityFunctionCategory ADD  CONSTRAINT [DF_SecurityFunctionCategory_Active]  DEFAULT ('Y') FOR [Active]
end
GO

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'SecurityFunctionCategory_DeleteTrigger'))
	DROP TRIGGER SecurityFunctionCategory_DeleteTrigger
GO

CREATE TRIGGER SecurityFunctionCategory_DeleteTrigger ON SecurityFunctionCategory
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ID INT
	SELECT @ID=ID FROM deleted
	
	DELETE FROM SecurityFunction
	WHERE Category=@ID
		
	SET NOCOUNT OFF
END
Go

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityFunction') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityFunction(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[Category] [int] NOT NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityFunction] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityFunction ADD  CONSTRAINT [DF_SecurityFunction_Active]  DEFAULT ('Y') FOR [Active]
end
go

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'SecurityFunction_DeleteTrigger'))
	DROP TRIGGER SecurityFunction_DeleteTrigger
GO

CREATE TRIGGER SecurityFunction_DeleteTrigger ON SecurityFunction
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ID INT
	SELECT @ID=ID FROM deleted
	
	DELETE FROM SecurityComponent
	WHERE SecurityFunction=@ID
	
	DELETE FROM SecurityRoleFunction
	WHERE SecurityFunction=@ID
	
	SET NOCOUNT OFF
END
Go

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityRoleFunction') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityRoleFunction(
		[SecurityRole] [int] NOT NULL,
		[SecurityFunction] [int] NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityRoleFunction] PRIMARY KEY CLUSTERED 
	(
		[SecurityRole] ASC,
		[SecurityFunction] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityComponent') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityComponent(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[SecurityFunction] [int] NOT NULL,
		[FormName] [varchar](100) NOT NULL,
		[FrameName] [varchar](100) NULL,
		[ComponentName] [varchar](100) NULL,
		[Enabled] [char](1) NOT NULL,
		[Visible] [char](1) NOT NULL,
		[Caption] [varchar](255) NULL,
		[Url] [varchar](200) NULL,
		[UrlParam] [varchar](100) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityComponent] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityComponent ADD  CONSTRAINT [DF_SecurityComponent_Enabled]  DEFAULT ('Y') FOR [Enabled]
	ALTER TABLE SecurityComponent ADD  CONSTRAINT [DF_SecurityComponent_Visible]  DEFAULT ('Y') FOR [Visible]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'LoginAttempt') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE LoginAttempt(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LoginId] [varchar](100) NULL,
		[IP] [varchar](100) NULL,
		[Attempt] [int] NULL,
		[LastAttempt] [datetime] NULL,
		[Expires] [datetime] NULL,
	 CONSTRAINT [LoginAttempt_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end
GO
-- drop table SessionToken
if not exists (select * from dbo.sysobjects where id = object_id(N'SessionToken') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SessionToken(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[SessionTokenId] [varchar](1000) NULL,	
		[FormAuthTokenId] [varchar](1000) NULL,	
		[LoginId] [varchar](100) NULL,	
		[Valid] [char](1) NULL,
		[Updated] [datetime] NULL,
	 CONSTRAINT [SessionToken_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	ALTER TABLE SessionToken ADD  CONSTRAINT [DF_SessionToken_Valid]  DEFAULT ('N') FOR [Valid]
end
GO

if not exists(select * from syscolumns where name = 'FormAuthTokenId' 
	and id = (select id from sysobjects where id = object_id(N'SessionToken') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table SessionToken Add [FormAuthTokenId] [varchar](1000) NULL
end
go


if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetConnoteCatalogue') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetConnoteCatalogue
GO
-- exec NSW_GetConnoteCatalogue 
-- exec NSW_GetConnoteCatalogue '015'
CREATE PROCEDURE NSW_GetConnoteCatalogue
	@CDID varchar(3)=null 
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[CountCentreCode] [varchar](5) NULL
	)
	INSERT INTO @ContainerStatusLatestTable
	SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
	(     
	SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
		NSW_ContainerStatus.CountCentreCode,
		ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
		ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
	FROM NSW_Container
		LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
			AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
	) AS Q
	WHERE rn=1
		
	SELECT T.ConsKey,T.[LineNo], C.EventId, RIGHT(C.CDID, 2) [CDID], C.ReturningOfficeName, C.ReturningOfficeName+' ('+RIGHT(C.CDID, 2)+')' [ReturningOffice],
		C.LGAId, C.LGAName+' ('+LGAId+')' [LGA], C.WardCode, C.WardName+' ('+WardCode+')' [Ward],
		RIGHT(C.VenueCode,4) [VenueCode], C.VenueName, C.VenueName+' ('+RIGHT(C.VenueCode,4)+')' [Venue],
		C.VenueTypeCode, C.VenueTypeName, C.VenueTypeName+' ('+VenueTypeCode+')' [VenueType],
		C.ContestCode, C.ContestName, C.ContestName+' ('+ContestCode+')' [Contest],
		C.ContainerCode, C.ContainerName, C.ContainerName+' ('+ContainerCode+')' [ContainerType],
		C.ContainerQuantity, T.Barcode, T.Deleted, S.StatusNotes, S.StatusDate,S.StatusTime, S.StatusStage, 
		case 
			when CC.Code is not null then CC.Name+' ('+CC.Code+')'
			else null
		end [CountCentre] 
	from NSW_Container T
		INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
		INNER JOIN @ContainerStatusLatestTable S ON S.ConsKey=T.ConsKey AND S.[LineNo]=T.[LineNo] AND S.Barcode=T.Barcode
		left join NSW_CountCentre CC ON CC.Code=S.CountCentreCode
	WHERE C.CDID=@CDID or @CDID is null
	ORDER BY EventId, CDID, VenueName, ContestName, ContainerCode,Barcode
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetConnoteContainer') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetConnoteContainer
GO
-- exec NSW_GetConnoteContainer 36670,'02023002728020101002003'
-- exec NSW_GetConnoteContainer 1
-- exec NSW_GetConnoteContainer 7,'010554020301001002'
-- exec NSW_GetConnoteContainer 7,'010554020301002002'
-- exec NSW_GetConnoteContainer null,'110000001221'
CREATE PROCEDURE NSW_GetConnoteContainer 
	@Conskey [varchar](40)=null, @Barcode varchar(40)=null
AS 
BEGIN
	declare 
		@Conskey1 [varchar](40)=@Conskey,
		@Barcode1 varchar(40)=@Barcode
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[MovementDirection] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL,
		[CountCentreCode] [varchar](5) NULL,
		[CountCentreName] [varchar](100) NULL
	)
	INSERT INTO @ContainerStatusLatestTable
	SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes],MovementDirection,StatusStage,LocationCode,LocationName,CountCentreCode,CountCentreName
	FROM
	(     
	SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
		ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.MovementDirection, '') [MovementDirection], NSW_ContainerStatus.StatusStage,
		NSW_ContainerStatus.LocationCode, NSW_ContainerStatus.LocationName,NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.CountCentreName,
		ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
	FROM NSW_Container WITH (NOLOCK)
		LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
			AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] 
			and (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			--where NSW_ContainerStatus.StatusStage is not null
			where (NSW_Container.ConsKey=@Conskey1 or @Conskey1 is null) and (NSW_Container.Barcode=@Barcode1 or @Barcode1 is null)
	) AS Q
	WHERE rn=1
	
	--select * from @ContainerStatusLatestTable where StatusStage is not null
		
	SELECT T.ConsKey,T.[LineNo], C.ConsNo, C.SourceId, C.Source, C.CDID, C.ReturningOfficeName, C.ReturningOfficeName+' ('+C.CDID+')' [ReturningOffice],
		ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
		C.ContestAreaId, C.ContestAreaCode, C.ContestAreaCode+' ('+C.ContestAreaId+')' [ContestArea],
		ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
		C.VenueCode, C.VenueName, C.VenueName+' ('+C.VenueCode+')' [Venue],
		C.VenueTypeCode, C.VenueTypeName, C.VenueTypeName+' ('+VenueTypeCode+')' [VenueType],
		C.ContestCode, C.ContestName, C.ContestName+' ('+ContestCode+')' [Contest],
		C.ContainerCode, C.ContainerName, C.ContainerName+' ('+ContainerCode+')' [ContainerType],
		C.ContainerQuantity, T.ContainerNo, T.Barcode, T.VenueName, T.ParentBarcode, T.ReplaceBarcode, T.ULDProduct, T.Deleted, T.Added, T.Activated, 
		S.StatusNotes, S.MovementDirection, S.StatusDate,S.StatusTime, s.StatusStage, S.LocationCode, S.LocationName,
		case 
			when S.CountCentreCode is not null then S.CountCentreCode
			when RO.CountCentreCode IS not null then RO.CountCentreCode
			when RO1.CountCentreCode IS not null then RO1.CountCentreCode
			else C.CountCentreCode
		end [CountCentreCode],
		case 
			when S.CountCentreCode is not null then S.CountCentreName
			when RO.CountCentreCode IS not null then RO.CountCentreName
			when RO1.CountCentreCode IS not null then RO1.CountCentreName
			else C.CountCentreName
		end [CountCentreName],
		case 
			when S.CountCentreCode is not null then RO2.Code
			when RO.CountCentreCode IS not null then RO.Code
			when RO1.CountCentreCode IS not null then RO1.Code
			else C.CDID
		end [ROCode],
		case 
			when S.CountCentreCode is not null then RO2.Name
			when RO.CountCentreCode IS not null then RO.Name
			when RO1.CountCentreCode IS not null then RO1.Name
			else C.ReturningOfficeName
		end [ROName]
	from NSW_Container T WITH (NOLOCK)
		INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=T.ConsKey
		INNER JOIN @ContainerStatusLatestTable S ON S.ConsKey=T.ConsKey AND S.[LineNo]=T.[LineNo] AND S.Barcode=T.Barcode
		left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
		left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
		left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
		left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
	where (T.ConsKey=@Conskey1 or @Conskey1 is null) and (T.Barcode=@Barcode1 or @Barcode1 is null)
		--where StatusStage is not null
	ORDER BY CDID, C.VenueName, ContestName, ContainerCode,Barcode
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_GridLayout') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_GridLayout(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[PageName] [varchar](200) NULL,
		[GridName] [varchar](200) NULL,
		[LayoutName] [varchar](200) NULL,
		[Layout] [varchar](max) NULL,
		[RowsPerPage] [int] NULL,
		[DefaultLayout] [char](1) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_NSW_GridLayout] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	ALTER TABLE NSW_GridLayout ADD CONSTRAINT [DF_NSW_GridLayout_DefaultLayout]  DEFAULT ('N') FOR [DefaultLayout]	
end
GO

IF EXISTS (select * from dbo.sysobjects
		where id = object_id(N'ufn_DelimiterStringToTable')
		and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION ufn_DelimiterStringToTable
GO

CREATE FUNCTION ufn_DelimiterStringToTable ( 
	@StringInput VARCHAR(8000), @Delimiter char(1) )
	RETURNS @OutputTable TABLE ( [String] VARCHAR(100))
AS
BEGIN
    DECLARE @String VARCHAR(100)

    WHILE @StringInput is not null and LEN(@StringInput) > 0
    BEGIN
        SET @String = LEFT(@StringInput, ISNULL(NULLIF(CHARINDEX(@Delimiter, @StringInput) - 1, -1), LEN(@StringInput)))
        SET @StringInput = SUBSTRING(@StringInput, ISNULL(NULLIF(CHARINDEX(@Delimiter, @StringInput), 0), LEN(@StringInput)) + 1, LEN(@StringInput))

        INSERT INTO @OutputTable ( [String] )
        VALUES ( @String )
    END
    RETURN
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'GetDefaultConnoteLabels') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE GetDefaultConnoteLabels 
GO
-- exec GetDefaultConnoteLabels '110000001221|110000003821|110000012521'
-- exec GetDefaultConnoteLabels '10086009756030301003003|10086009756030301002003'
-- exec GetDefaultConnoteLabels '010554020201001001|010554020301002002'
-- EXEC GetDefaultConnoteLabels null, 7
-- EXEC GetDefaultConnoteLabels 
CREATE PROCEDURE GetDefaultConnoteLabels 
	@BarcodeList varchar(max)=null, @Conskey [varchar](40)=null
as 
begin
	--declare @BarcodeList varchar(max)=null, @Conskey int=null
	
	--set @BarcodeList='010554020201001001|010554020301002002'
	--set @BarcodeList='010554020201002002|010554020301002002'
	--set @BarcodeList='010554020201002002'
	--set @Conskey=7
	declare @ConnoteLabelTable table
	(
		-- CONNOTE
		[NSW_Connote.ConsKey] [varchar](40) NULL,
		[NSW_Connote.ConsNo] [varchar](40) NULL,
		[NSW_Connote.CDID] [varchar](3) NULL,
		[NSW_Connote.ReturningOfficeName] [varchar](100) NULL,
		[NSW_Connote.LGAId] [varchar](5) NULL,
		[NSW_Connote.LGACode] [varchar](100) NULL,
		[NSW_Connote.LGAName] [varchar](100) NULL,
		[NSW_Connote.WardCode] [varchar](5) NULL,
		[NSW_Connote.WardName] [varchar](100) NULL,
		[NSW_Connote.VenueCode] [varchar](5) NULL,
		[NSW_Connote.VenueName] [varchar](100) NULL,
		[NSW_Connote.VenueLongName] [varchar](200) NULL,
		[NSW_Connote.VenueAddress] [varchar](200) NULL,
		[NSW_Connote.VenueTypeCode] [varchar](2) NULL,
		[NSW_Connote.VenueTypeName] [varchar](100) NULL,
		[NSW_Connote.ContestCode] [varchar](2) NULL,
		[NSW_Connote.ContestName] [varchar](100) NULL,
		[NSW_Connote.ContainerCode] [varchar](2) NULL,
		[NSW_Connote.ContainerName] [varchar](100) NULL,
		[NSW_Connote.ContainerQuantity] [int] NULL,
		[NSW_Connote.AdditionalContainerQuantity] [int] NULL,
		[NSW_Connote.PrinterJobId] [varchar](4) NULL,
		[NSW_Connote.Created] [datetime] NULL,
		[NSW_Connote.CreatedBy] [varchar](100) NULL,
		[NSW_Connote.Modified] [datetime] NULL,
		[NSW_Connote.ModifiedBy] [varchar](100) NULL,
		[NSW_Connote.Deleted] [datetime] NULL,
		[NSW_Connote.DeletedBy] [varchar](100) NULL,
		[NSW_Connote.DeletedReason] [varchar](1000) NULL,
		-- CONNOTE_CARTONS
		[NSW_Container.ConsKey] [varchar](40) NULL,
		[NSW_Container.LineNo] [int] NULL,
		[NSW_Container.ContainerNo] [varchar](40) NULL,
		[NSW_Container.Barcode] [varchar](100) NULL,
		[NSW_Container.VenueName] [varchar](40) NULL,
		[NSW_Container.Created] [datetime] NULL,
		[NSW_Container.CreatedBy] [varchar](100) NULL,
		[NSW_Container.Modified] [datetime] NULL,
		[NSW_Container.ModifiedBy] [varchar](100) NULL,
		[NSW_Container.Deleted] [datetime] NULL,
		[NSW_Container.DeletedBy] [varchar](100) NULL,
		[NSW_Container.DeletedReason] [varchar](1000) NULL,
		[NSW_Container.Added] [datetime] NULL,
		[NSW_Container.AddedBy] [varchar](100) NULL,
		[NSW_Container.PackageRank] [varchar](50) NULL
	)
	declare @ConnoteTable table
	(
		[NSW_Connote.ConsKey] [varchar](40) NULL,
		[NSW_Connote.ConsNo] [varchar](40) NULL,
		[NSW_Connote.CDID] [varchar](3) NULL,
		[NSW_Connote.ReturningOfficeName] [varchar](100) NULL,
		[NSW_Connote.LGAId] [varchar](5) NULL,
		[NSW_Connote.LGACode] [varchar](100) NULL,
		[NSW_Connote.LGAName] [varchar](100) NULL,
		[NSW_Connote.WardCode] [varchar](5) NULL,
		[NSW_Connote.WardName] [varchar](100) NULL,
		[NSW_Connote.VenueCode] [varchar](5) NULL,
		[NSW_Connote.VenueName] [varchar](100) NULL,
		[NSW_Connote.VenueLongName] [varchar](200) NULL,
		[NSW_Connote.VenueAddress] [varchar](200) NULL,
		[NSW_Connote.VenueTypeCode] [varchar](2) NULL,
		[NSW_Connote.VenueTypeName] [varchar](100) NULL,
		[NSW_Connote.ContestCode] [varchar](2) NULL,
		[NSW_Connote.ContestName] [varchar](100) NULL,
		[NSW_Connote.ContainerCode] [varchar](2) NULL,
		[NSW_Connote.ContainerName] [varchar](100) NULL,
		[NSW_Connote.ContainerQuantity] [int] NULL,
		[NSW_Connote.AdditionalContainerQuantity] [int] NULL,
		[NSW_Connote.PrinterJobId] [varchar](4) NULL,
		[NSW_Connote.Created] [datetime] NULL,
		[NSW_Connote.CreatedBy] [varchar](100) NULL,
		[NSW_Connote.Modified] [datetime] NULL,
		[NSW_Connote.ModifiedBy] [varchar](100) NULL,
		[NSW_Connote.Deleted] [datetime] NULL,
		[NSW_Connote.DeletedBy] [varchar](100) NULL,
		[NSW_Connote.DeletedReason] [varchar](1000) NULL
	)
	
	declare @ConnoteCartonTable table
	(
		[NSW_Container.ConsKey] [varchar](40) NULL,
		[NSW_Container.LineNo] [int] NULL,
		[NSW_Container.ContainerNo] [varchar](40) NULL,
		[NSW_Container.Barcode] [varchar](40) NULL,
		[NSW_Container.VenueName] [varchar](40) NULL,
		[NSW_Container.Created] [datetime] NULL,
		[NSW_Container.CreatedBy] [varchar](100) NULL,
		[NSW_Container.Modified] [datetime] NULL,
		[NSW_Container.ModifiedBy] [varchar](100) NULL,
		[NSW_Container.Deleted] [datetime] NULL,
		[NSW_Container.DeletedBy] [varchar](100) NULL,
		[NSW_Container.DeletedReason] [varchar](1000) NULL,
		[NSW_Container.Added] [datetime] NULL,
		[NSW_Container.AddedBy] [varchar](100) NULL,
		[NSW_Container.PackageRank] [varchar](50) NULL
	)
	
	declare @ConsKeyTable table (ConsKey [varchar](40))
	
	declare @BarcodeTable table (Barcode varchar(40))
	
	if (@BarcodeList is null or LEN(@BarcodeList)=0) and (@Conskey is null or (@Conskey is not null and len(@Conskey)=0)) 
	begin
		declare @XSDSchema xml
		set @XSDSchema = (select top 0 * from @ConnoteLabelTable as ConnoteLabelTable
		for XML auto, BINARY BASE64, elements, xmlschema('ConnoteLabelTable'))
		select @XSDSchema
		return
	end
	
	if @BarcodeList is not null and LEN(@BarcodeList)>0
	begin
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList,'|')
	end
	else
	if @Conskey is not null and len(@Conskey)>0
	begin
		insert into @BarcodeTable
		select distinct Barcode from NSW_Container
		where ConsKey=@Conskey and Deleted is null
	end
	--select * from @BarcodeTable
	insert into @ConsKeyTable
	select distinct C.ConsKey from NSW_Container C
		inner join @BarcodeTable B ON B.Barcode=C.Barcode
	--select * from @ConsKeyTable	
	if exists(select 1 from @ConsKeyTable)
	begin		
		insert into @ConnoteCartonTable
		select C.ConsKey,C.[LineNo],C.ContainerNo,C.Barcode,C.VenueName,
			C.Created,C.CreatedBy,C.Modified,C.ModifiedBy,
			C.Deleted,C.DeletedBy,C.DeletedReason,C.Added,C.AddedBy,NULL 
		from NSW_Container C
			inner join @ConsKeyTable CK ON CK.ConsKey=C.ConsKey
			
		--select * from @ConnoteCartonTable	
		--select * from @ConsKeyTable
		
		insert into @ConnoteTable
		select C.ConsKey,C.ConsNo,C.CDID,C.ReturningOfficeName,C.LGAId,C.LGACode,C.LGAName,C.WardCode,C.WardName,
			C.VenueCode,C.VenueName,C.VenueLongName,C.VenueAddress,C.VenueTypeCode,C.VenueTypeName,
			C.ContestCode,C.ContestName,C.ContainerCode,C.ContainerName,C.ContainerQuantity,ISNULL(C.AdditionalContainerQuantity,0),C.PrinterJobId,
			C.Created,C.CreatedBy,C.Modified,C.ModifiedBy,C.Deleted,C.DeletedBy,C.DeletedReason 
		from NSW_Connote C 
			INNER JOIN @ConsKeyTable CK ON CK.ConsKey=C.ConsKey		
	end
	else
	begin
		select * from @ConnoteLabelTable
		return
	end
	
	--select * from @ConnoteCartonTable	
	--select * from @ConnoteTable
		
	DECLARE @Rank int=0, @Cons_Key [varchar](40), @LineNo int, @CartonNo varchar(7),
		@ItemQty int, @ItemQtyAdded int, @ItemQtyRemoved int, @RankTotal varchar(50)
	DECLARE KQ CURSOR FOR
	SELECT DISTINCT [NSW_Connote.ConsKey], [NSW_Connote.ContainerQuantity], [NSW_Connote.AdditionalContainerQuantity]
	FROM @ConnoteTable
	OPEN KQ
	FETCH KQ INTO @Cons_Key, @ItemQty, @ItemQtyAdded	
	WHILE @@FETCH_STATUS = 0                                
	BEGIN
		SELECT @ItemQtyRemoved=COUNT([LineNo]) FROM NSW_Container
		WHERE ConsKey=@Cons_Key AND Deleted IS NOT NULL
		
		SET @RankTotal=' of '+CAST(@ItemQty AS VARCHAR)
		IF @ItemQtyAdded>0 OR @ItemQtyRemoved>0
		BEGIN
			IF @ItemQtyAdded>@ItemQtyRemoved
				SET @RankTotal=' of '+CAST(@ItemQty AS VARCHAR)+'+'+CAST((@ItemQtyAdded-@ItemQtyRemoved) AS VARCHAR)
			ELSE
				SET @RankTotal=' of '+CAST(@ItemQty AS VARCHAR)+'-'+CAST((@ItemQtyRemoved-@ItemQtyAdded) AS VARCHAR)
		END
		
		SET @Rank=0  
		DECLARE PQ CURSOR FOR
		SELECT [NSW_Container.LineNo],[NSW_Container.ContainerNo] FROM @ConnoteCartonTable
		WHERE [NSW_Container.ConsKey]=@Cons_Key 
		--ORDER BY [NSW_Container.LineNo],[NSW_Container.ContainerNo]
		ORDER BY [NSW_Container.ContainerNo]
		OPEN PQ
		FETCH PQ INTO @LineNo, @CartonNo	
		WHILE @@FETCH_STATUS = 0                                
		BEGIN
			SET @Rank=@Rank+1
			UPDATE @ConnoteCartonTable
			SET [NSW_Container.PackageRank]=CAST(@Rank AS VARCHAR)+@RankTotal
			WHERE [NSW_Container.ConsKey]=@Cons_Key 
				AND [NSW_Container.LineNo]=@LineNo
				AND [NSW_Container.ContainerNo]=@CartonNo
			
			FETCH PQ INTO @LineNo, @CartonNo       
		END                                                       
		CLOSE PQ                                                   
		DEALLOCATE PQ
		
		FETCH KQ INTO @Cons_Key, @ItemQty, @ItemQtyAdded	      
	END                                                       
	CLOSE KQ                                                   
	DEALLOCATE KQ
	
	--select * from @ConnoteCartonTable	
	
	insert into @ConnoteLabelTable
	select Connote.*, Carton.* 
	from @ConnoteCartonTable Carton
		INNER JOIN @BarcodeTable B ON B.Barcode=Carton.[NSW_Container.Barcode]	
		inner join @ConnoteTable Connote ON Connote.[NSW_Connote.ConsKey] = Carton.[NSW_Container.ConsKey]
	
	update @ConnoteLabelTable
		set [NSW_Connote.VenueName]=[NSW_Container.VenueName]
	where [NSW_Container.VenueName] is not null
		
	select * from @ConnoteLabelTable
end
go

-- drop table SecurityDevice
if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityDevice') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE SecurityDevice(
		[ID] [varchar](40) NOT NULL,
		[DeviceId] [varchar](50) NOT NULL,
		[Password] [varchar](200) NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [SecurityDevice_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityDevice ADD CONSTRAINT [DF_SecurityDevice_Active] DEFAULT ('Y') FOR [Active]
end
GO


if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetLGAContainerBarcodeData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetLGAContainerBarcodeData
GO
-- exec NSW_GetLGAContainerBarcodeData '002',null,null,null,null,null,null,null,5.20
-- exec NSW_GetLGAContainerBarcodeData '003','018',null,'00709',null,'02',null,null,1.00
CREATE PROCEDURE NSW_GetLGAContainerBarcodeData 
	@CDID varchar(3)=null, @LGACode varchar(3)=null, @WardCode varchar(2)=null,
	@VenueCode varchar(5)=null, @VenueTypeCode varchar(2)=null,
	@ContestCode varchar(2)=null, @CCCode varchar(5)=null, @BarcodeList varchar(max)=null, 
	@StageStatusId varchar(4)=null
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[CountCentreCode] [varchar](5) NULL
	)
	/*
	StageId		Description
	1.00        Printer Scan-out to Returning Office
	1.10        Printer Scan-out to Sydney Town Hall
	1.11        Sydney Town Hall Scan-in from Printer
	1.12        Sydney Town Hall Scan-out to Returning Office
	2.00        Returning Office Scan-in from Printer
	2.10        Returning Office Scan-in from Sydney Town Hall
	3.00        Returning Office Scan-out to Venue
	4.00        Returning Office Scan-in from Venue
	5.00        Returning Office Scan-out to Warehouse
	5.10        Returning Office Scan-out to Sydney Town Hall
	5.11        Sydney Town Hall Scan-in from Returning Office
	5.12        Sydney Town Hall Scan-out to Warehouse
	5.20        Returning Office Scan-out to Count Centre
	5.21        Count Centre Scan-in from Returning Office
	5.22        Count Centre Scan-out to Warehouse
	6.00        Warehouse Scan-in from Returning Office
	6.10        Warehouse Scan-in from Sydney Town Hall
	6.20        Warehouse Scan-in from Count Centre

	*/
			
	if @StageStatusId is not null
	begin
		--print '@StageStatusId defined'
		
		if @StageStatusId<=1.1 -- (1.00 or 1.10)
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes],[StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and [StatusStage]=0
		end
		else
		if @StageStatusId=1.11 -- Sydney Town Hall Scan-in from Printer
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=1.10 or [StatusStage]=1.11)
		end
		else
		if @StageStatusId=1.12 -- Sydney Town Hall Scan-out to Returning Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=1.11 or [StatusStage]=1.12)
		end
		else
		if @StageStatusId=2.00 -- Returning Office Scan-in from Printer
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=0 or [StatusStage]=1.00 or [StatusStage]=2.00)
		end
		else
		if @StageStatusId=2.10 -- Returning Office Scan-in from Sydney Town Hall
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=1.12 or [StatusStage]=2.10)
		end
		else
		if @StageStatusId=3.00 -- Returning Office Scan-out to Venue
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=2.00 or [StatusStage]=2.10 or [StatusStage]=3.00)
		end
		else
		if @StageStatusId=4.00 -- Returning Office Scan-in from Venue
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=3.00 or [StatusStage]=4.00)
		end
		else
		if @StageStatusId=5.00 -- Returning Office Scan-out to Warehouse
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage], NSW_ContainerStatus.CountCentreCode,
				NSW_Connote.VenueTypeCode,NSW_Connote.VenueCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=4.00 or [StatusStage]=5.00 or 
				([StatusStage]=2.00 and [VenueTypeCode] in ('03','04') and not([VenueCode] between '09980' and '09989')))
		end
		else
		if @StageStatusId=5.10 -- Returning Office Scan-out to Sydney Town Hall
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				NSW_Connote.VenueTypeCode,NSW_Connote.VenueCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=4.00 or [StatusStage]=5.10 or 
				([StatusStage]=2.00 and [VenueTypeCode] in ('03','04') and not([VenueCode] between '09980' and '09989')))
		end
		else
		if @StageStatusId=5.11 -- Sydney Town Hall Scan-in from Returning Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.10 or [StatusStage]=5.11)
		end
		else
		if @StageStatusId=5.12 -- Sydney Town Hall Scan-out to Warehouse
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.11 or [StatusStage]=5.12)
		end
		else
		if @StageStatusId=5.20 -- Returning Office Scan-out to Count Centre
		begin
			-- ready to scan out
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				NSW_Connote.VenueTypeCode,NSW_Connote.VenueCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and [CountCentreCode] is null and ([StatusStage]=4.00 or 
				([StatusStage]=2.00 and [VenueTypeCode] in ('03','04') and not([VenueCode] between '09980' and '09989')))
			-- already scanned out to cc
			if @CCCode is not null and LEN(@CCCode)>0
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					NSW_Connote.VenueTypeCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.20 or [StatusStage]=5.21 or [StatusStage]=5.22 or [StatusStage]=6.20)
					and [CountCentreCode]=@CCCode
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					NSW_Connote.VenueTypeCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.20 or [StatusStage]=5.21 or [StatusStage]=5.22 or [StatusStage]=6.20)
					and [CountCentreCode] is not null
		end
		else
		if @StageStatusId=5.21 -- Count Centre Scan-in from Returning Office
		begin
			if @CCCode is not null and LEN(@CCCode)>0
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.20 or [StatusStage]=5.21) and [CountCentreCode]=@CCCode
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.20 or [StatusStage]=5.21) and [CountCentreCode] is not null
		end
		else
		if @StageStatusId=5.22 -- Count Centre Scan-out to Warehouse
		begin
			if @CCCode is not null and LEN(@CCCode)>0
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.21 or [StatusStage]=5.22) and [CountCentreCode]=@CCCode
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.21 or [StatusStage]=5.22) and [CountCentreCode] is not null
		end
		else
		if @StageStatusId=6.00 -- Warehouse Scan-in from Returning Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.00 or [StatusStage]=6.00)
		end
		else
		if @StageStatusId=6.10 -- Warehouse Scan-in from Sydney Town Hall
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.12 or [StatusStage]=6.10)
		end
		else
		if @StageStatusId=6.20 -- Warehouse Scan-in from Sydney Town Hall
		begin
			if @CCCode is not null and LEN(@CCCode)>0
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.22 or [StatusStage]=6.20) and [CountCentreCode]=@CCCode
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.22 or [StatusStage]=6.20) and [CountCentreCode] is not null
		end
	end
	else
	begin
		--print '@StageStatusId undefined'
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
		) AS Q
		WHERE rn=1
	end
	
	declare @BarcodeTable table (Barcode varchar(40))
	if @BarcodeList is not null and LEN(@BarcodeList)>0
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList,'|')
		
	if exists(select 1 from @BarcodeTable)
	begin
		SELECT T.Barcode, RIGHT(CDID,2) [CDID], ReturningOfficeName, 
			ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
			RIGHT(VenueCode,4) [VenueCode], 
			case
				when T.VenueName is not null then T.VenueName
				else C.VenueName
			end [VenueName], 
			VenueTypeCode, VenueTypeName, ContestCode, ContestName, ContainerCode, ContainerName, StatusStage, StatusDate,CountCentreCode
		FROM NSW_Container T
			INNER JOIN @BarcodeTable B ON B.Barcode=T.Barcode
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
		WHERE T.Deleted IS NULL AND (CDID=@CDID OR (@CDID IS NULL)) 
			AND (LGAId=@LGACode OR (@LGACode IS NULL)) 
			AND (WardCode=@WardCode OR (@WardCode IS NULL))
			AND (VenueCode=@VenueCode OR (@VenueCode IS NULL))
			AND (VenueTypeCode=@VenueTypeCode OR (@VenueTypeCode IS NULL))
			AND (ContestCode=@ContestCode OR (@ContestCode IS NULL))
			--AND (CountCentreCode=@CCCode OR (@CCCode IS NULL))
		order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
	end
	ELSE
		SELECT T.Barcode, RIGHT(CDID,2) [CDID], ReturningOfficeName, 
			ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
			RIGHT(VenueCode,4) [VenueCode], 
			case
				when T.VenueName is not null then T.VenueName
				else C.VenueName
			end [VenueName], 
			VenueTypeCode, VenueTypeName, ContestCode, ContestName, ContainerCode, ContainerName, StatusStage, StatusDate,CountCentreCode 
		FROM NSW_Container T
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
		WHERE T.Deleted IS NULL AND (CDID=@CDID OR (@CDID IS NULL)) 
			AND (LGAId=@LGACode OR (@LGACode IS NULL))
			AND (WardCode=@WardCode OR (@WardCode IS NULL))
			AND (VenueCode=@VenueCode OR (@VenueCode IS NULL)) 
			AND (VenueTypeCode=@VenueTypeCode OR (@VenueTypeCode IS NULL))
			AND (ContestCode=@ContestCode OR (@ContestCode IS NULL))
			--AND (CountCentreCode=@CCCode OR (@CCCode IS NULL))
		order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
END
GO


if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetROLGAForCountCentre') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetROLGAForCountCentre
GO
-- exec NSW_GetROLGAForCountCentre '09993'
CREATE PROCEDURE NSW_GetROLGAForCountCentre 
	@CCCode varchar(5)=null
AS 
BEGIN
	declare @BarcodeTable table 
	(
		Barcode varchar(40),
		[CDID] [varchar](3) NULL,
		[ReturningOfficeName] [varchar](100) NULL,
		[LGACode] [varchar](3) NULL,
		[LGAName] [varchar](100) NULL,
		[WardCode] [varchar](2) NULL,
		[WardName] [varchar](100) NULL,
		[VenueCode] [varchar](5) NULL,
		[VenueName] [varchar](100) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,
		[ContestCode] [varchar](2) NULL,
		[ContestName] [varchar](100) NULL,
		[ContainerCode] [varchar](2) NULL,
		[ContainerName] [varchar](100) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[StatusDate] [datetime] NULL,
		[CountCentreCode] [varchar](5) NULL
	)

	insert into @BarcodeTable
	exec NSW_GetLGAContainerBarcodeData null,null,null,null,null,null,@CCCode 
	
	select distinct CDID [ROCode], ReturningOfficeName [ROName],LGACode,LGAName from @BarcodeTable B
		left join NSW_CountCentre c on c.Code=B.CountCentreCode
	where c.Code=@CCCode or @CCCode is null
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetLGAContainerBarcodePropertyData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetLGAContainerBarcodePropertyData
GO
-- exec NSW_GetLGAContainerBarcodePropertyData '110000034921|110000035021|110000034821'
-- exec NSW_GetLGAContainerBarcodePropertyData '120000412322|120000021722|120000318522|120000314522'
CREATE PROCEDURE NSW_GetLGAContainerBarcodePropertyData 
	@BarcodeList varchar(max)
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,		
		[CountCentreCode] [varchar](5) NULL,
		[CountCentreName] [varchar](100) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL,
		[ContestName] [varchar](100) NULL,
		[SourceId] [int] NULL
	)
	
	declare @BarcodeTable table (Barcode varchar(40))
	if @BarcodeList is not null and LEN(@BarcodeList)>0
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList,'|')
	
	INSERT INTO @ContainerStatusLatestTable
	SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,CountCentreName,LocationCode,LocationName,ContestName,SourceId
	FROM
	(     
	SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
		ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
		NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.CountCentreName,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
		ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
	FROM NSW_Container WITH (NOLOCK)
		INNER JOIN @BarcodeTable B ON B.Barcode=NSW_Container.Barcode
		INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
		LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
			AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
	WHERE NSW_Container.Deleted IS NULL
	) AS Q
	WHERE rn=1
		
	SELECT T.Barcode, CDID [ROCode], ReturningOfficeName [ROName], 
			LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ContestAreaId, ISNULL(ContestAreaCode,'Undefined') [ContestAreaCode],
			WardCode, WardName,
			VenueCode, c.VenueName,
			VenueTypeCode, VenueTypeName, 
			ContestCode, C.ContestName , 
			ContainerCode,ContainerName,StatusStage,StatusNotes,StatusDate,			
			C.CountCentreName+' ('+C.CountCentreCode+')' [CountCentre], -- from PPDM, not trusted
			case 
				when S.CountCentreCode is not null then S.CountCentreCode
				when RO.CountCentreCode IS not null then RO.CountCentreCode
				when RO1.CountCentreCode IS not null then RO1.CountCentreCode
				else C.CountCentreCode
			end [CountCentreCode],
			case 
				when S.CountCentreCode is not null then S.CountCentreName
				when RO.CountCentreCode IS not null then RO.CountCentreName
				when RO1.CountCentreCode IS not null then RO1.CountCentreName
				else C.CountCentreName
			end [CountCentreName],
			case 
				when S.CountCentreCode is not null then S.CountCentreName+' ('+S.CountCentreCode+')'
				when RO.CountCentreCode IS not null then RO.CountCentreName+' ('+RO.CountCentreCode+')'
				when RO1.CountCentreCode IS not null then RO1.CountCentreName+' ('+RO1.CountCentreCode+')'
				else C.CountCentreName+' ('+C.CountCentreCode+')'
			end [FinalCountCentre],		
			LocationCode,LocationName,DeliveryNumber
		FROM NSW_Container T WITH (NOLOCK)
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=T.ConsKey
			left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
			left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
			left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
		WHERE T.Barcode IS NOT NULL AND LEN(T.Barcode)>0
		order by ReturningOfficeName, C.VenueName, VenueTypeName, C.ContestName, Barcode
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetLGA2020ContainerBarcodeData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetLGA2020ContainerBarcodeData
GO

/* 
exec NSW_GetLGA2020ContainerBarcodeData '1.00'
exec NSW_GetLGA2020ContainerBarcodeDataAdv '1.00'

exec NSW_GetLGA2020ContainerBarcodeData '5.14'
exec NSW_GetLGA2020ContainerBarcodeDataAdv '5.14'

exec NSW_GetLGA2020ContainerBarcodeData null,'130000001323|140000001324'
exec NSW_GetLGA2020ContainerBarcodeData null,null,1

exec NSW_GetLGA2020ContainerBarcodeData '5.00',null,null,'102'
exec NSW_GetLGA2020ContainerBarcodeData '5.11',null,null,'114'
exec NSW_GetLGA2020ContainerBarcodeData '5.11',null,null,'106',null,null,null,null,null,null,null,'174'
exec NSW_GetLGA2020ContainerBarcodeData '5.11',null,null,'114',null,null,null,null,null,null,null,'175'

exec NSW_GetLGA2020ContainerBarcodeData null,null,null,null,null,null,null,null,null,'6',null,'175'

exec NSW_GetLGA2020ContainerBarcodeData null,null,null,null,null,null,null,null,null,'6'
exec NSW_GetLGA2020ContainerBarcodeDataAdv null,null,null,null,null,null,null,null,null,'6'

*/
CREATE PROCEDURE NSW_GetLGA2020ContainerBarcodeData 
	@StageStatusId varchar(4)=null,@BarcodeList varchar(max)=null,@DeliveryNum int=null, 
	@CDID varchar(3)=null, @LGACode varchar(5)=null, @WardCode varchar(5)=null,
	@VenueCode varchar(5)=null, @VenueTypeCode varchar(2)=null,
	@ContestCode varchar(2)=null, @ContainerCode varchar(2)=null,
	@ScanLocCode varchar(5)=null, @CountCentreCode varchar(3)=null	 
AS 
BEGIN	
	declare 
		@CDID1 varchar(3)=@CDID, @LGACode1 varchar(5)=@LGACode, @WardCode1 varchar(5)=@WardCode,
		@VenueCode1 varchar(5)=@VenueCode, @VenueTypeCode1 varchar(2)=@VenueTypeCode,
		@ContestCode1 varchar(2)=@ContestCode, @ContainerCode1 varchar(2)=@ContainerCode,
		@DeliveryNum1 int=@DeliveryNum, @ScanLocCode1 varchar(5)=@ScanLocCode,		 
		@BarcodeList1 varchar(max)=@BarcodeList, @StageStatusId1 varchar(4)=@StageStatusId,
		@CountCentreCode1 varchar(3)=@CountCentreCode
	
	declare @BarcodeTable table (Barcode varchar(40))
	if @BarcodeList1 is not null and LEN(@BarcodeList1)>0
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList1,'|')
/*
StageId	Description
1.00	Printer Scan-out to Returning Office/STH/CPVC
2.00	Returning Office/STH/CPVC Scan-in from Printer
2.10	Returning Office Allocate Reserve Stock
3.00	Returning Office Scan-out to Venue
3.10	STH Scan-out to STH Count Centre
3.20	STH Count Centre Scan-in from STH
3.30	STH Count Centre Scan-out to Returning Office
3.40	CPVC Scan-out to Returning Office
3.50	CPVC Scan-out to Count Centre
4.00	Returning Office Scan-in from Venue
4.10	Returning Office Scan-in from STH Count Centre
4.11	Returning Office Scan-in from CPVC
4.12	Returning Office Scan-in from Processing Location
4.13	Returning Office Scan Carton into ULD
5.00	Returning Office Scan-out to Warehouse
5.05	STH Count Centre Scan-out to Count Centre
5.10	Returning Office Scan-out to Count Centre 
5.11	Count Centre Scan-in from Returning Office
5.12	Count Centre Scan-in from STH Count Centre
5.13	Count Centre Scan-in from Processing Location
5.14	Count Centre Scan Carton into ULD
5.15	Count Centre Scan-out to Warehouse
5.16	Count Centre Scan-in from CPVC
6.00	Warehouse Scan-in from Returning Office
6.10	Warehouse Scan-in from Count Centre
*/
	
	declare @ContainerStatusResultTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL,
		[SourceId] [int] NULL,
		[CDID] [varchar](3) NULL,
		[ReturningOfficeName] [varchar](100) NULL,
		[LGACode] [varchar](100) NULL,
		[LGAName] [varchar](100) NULL,
		[WardCode] [varchar](5) NULL,
		[WardName] [varchar](100) NULL,
		[VenueCode] [varchar](5) NULL,
		[VenueName] [varchar](100) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,
		[ContestCode] [varchar](2) NULL,
		[ContestName] [varchar](100) NULL,
		[ContainerCode] [varchar](2) NULL,
		[ContainerName] [varchar](100) NULL,
		[CountCentreCode] [varchar](5) NULL,
		[CountCentreName] [varchar](100) NULL,
		[ROCode] [varchar](3) NULL,
		[ROName] [varchar](100) NULL,
		[rn] [int] NULL
	)
	
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[ParentBarcode] [varchar](40) NULL,
		[ReplaceBarcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[MovementDirection] [varchar](500) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL,
		[CountCentreCode] [varchar](5) NULL,
		[CountCentreName] [varchar](100) NULL,
		[Deleted] [datetime] NULL
	)
	
	if exists(select 1 from @BarcodeTable)
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,ParentBarcode,ReplaceBarcode,StatusDate,StatusTime,[StatusNotes],[StatusStage],MovementDirection,
			LocationCode,LocationName,CountCentreCode,CountCentreName,Deleted 
		FROM
		(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_Container.ParentBarcode,NSW_Container.ReplaceBarcode,NSW_Container.Deleted, 
				NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.CountCentreName, NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],MovementDirection,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN @BarcodeTable B ON B.Barcode=NSW_Container.Barcode
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] 
			WHERE (NSW_Container.Deleted IS NULL or NSW_Container.DeletedReason='End of Life') AND NSW_Container.Barcode IS NOT NULL AND LEN(NSW_Container.Barcode)>0
				and (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
		) AS Q
		WHERE rn=1
	else
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,ParentBarcode,ReplaceBarcode,StatusDate,StatusTime,[StatusNotes],[StatusStage],MovementDirection,
			LocationCode,LocationName,CountCentreCode,CountCentreName,Deleted
		FROM
		(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_Container.ParentBarcode,NSW_Container.ReplaceBarcode,NSW_Container.Deleted, 
				NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.CountCentreName, NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],MovementDirection,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] 
			WHERE (NSW_Container.Deleted IS NULL or NSW_Container.DeletedReason='End of Life') AND NSW_Container.Barcode IS NOT NULL AND LEN(NSW_Container.Barcode)>0
				and (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
		) AS Q
		WHERE rn=1

	if @StageStatusId1 is not null
	begin
		if @StageStatusId1=1.00 -- Printer Scan-out to Returning Office/STH/CPVC
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL))					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))
			) AS Q
			WHERE rn=1 and [SourceId]=1 and [StatusStage]=0
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
		end
		else
		if @StageStatusId1=2.00 -- Returning Office, STH, CPVC Scan-in from Printer
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes, 
					CASE
						WHEN S.StatusStage='2.00' AND C.CDID<>S.LocationCode THEN 0
						WHEN S.StatusStage IS NULL THEN 0
						ELSE S.StatusStage
					END [StatusStage],
					LocationCode, LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL))					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))
			) AS Q
			WHERE [SourceId]=1 and ([StatusStage]=0 or [StatusStage]=1.00 or [StatusStage]=2.00)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
		end
		else
		if @StageStatusId1=3.00 -- Returning Office Scan-out to Venue
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes, 
					CASE
						WHEN S.StatusStage='2.00' AND C.CDID<>S.LocationCode THEN 0
						WHEN S.StatusStage IS NULL THEN 0
						ELSE S.StatusStage
					END [StatusStage],
					LocationCode, LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
					left join NSW_ReturningOffice RO3 WITH (NOLOCK) ON RO3.Code=C.CDID
				WHERE (C.ContestName not like '%RESERVE STOCK%')
					AND (C.ReturningOfficeName NOT LIKE '%Sydney Town Hall%') 
					AND (RO3.LocationTypeCode='3') -- RO only, exclude CC, PL such as CPVC
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL))					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))
			) AS Q
			WHERE [SourceId] in (1,2) and ([StatusStage]=2.00 OR [StatusStage]=2.10 OR [StatusStage]=3.00)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=3.10 -- STH Scan-out to STH Count Centre
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,
					CASE
						WHEN S.StatusStage='2.00' AND C.CDID<>S.LocationCode THEN 0
						WHEN S.StatusStage IS NULL THEN 0
						ELSE S.StatusStage
					END [StatusStage],
					LocationCode, LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
					left join NSW_ReturningOffice RO3 WITH (NOLOCK) ON RO3.Code=C.CDID
				WHERE (C.ReturningOfficeName LIKE '%Sydney Town Hall%')
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL))					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE (([SourceId]=1 AND ([StatusStage]=2.00 OR [StatusStage]=3.10)) 
					OR ([SourceId]=2 AND ContainerCode<>'1' AND ([StatusStage]=0 OR [StatusStage]=3.10)))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
		end
		else
		if @StageStatusId1=3.20 -- STH Count Centre Scan-in from STH
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (C.ReturningOfficeName LIKE '%Sydney Town Hall%')
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL))					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE [SourceId] in (1,2) and ([StatusStage]=3.10 OR [StatusStage]=3.20)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
		end
		else
		if @StageStatusId1=3.30 -- STH Count Centre Scan-out to Returning Office
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (C.ReturningOfficeName LIKE '%Sydney Town Hall%')
					AND (S.Deleted IS NULL)
					and (ContestCode<>'2')
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (RO1.Code=@CDID1 OR (@CDID1 IS NULL)) -- the RO for the LGA							
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE [SourceId] in (1,2) and ([StatusStage]=3.20 OR [StatusStage]=3.30)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
		end
		else
		if @StageStatusId1=3.40 -- CPVC Scan-out to Returning Office
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes, 
					CASE
						WHEN S.StatusStage='2.00' AND C.CDID<>S.LocationCode THEN 0
						WHEN S.StatusStage IS NULL THEN 0
						ELSE S.StatusStage
					END [StatusStage],
					LocationCode, LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
					left join NSW_ReturningOffice RO3 WITH (NOLOCK) ON RO3.Code=C.CDID
				WHERE (charindex('CPVC',RO3.Name)>0)
					and (ContestCode<>'2')
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (RO1.Code=@CDID1 OR (@CDID1 IS NULL)) -- the matched RO linked the LGA					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE (([SourceId]=1 and ([StatusStage]=2.00 OR [StatusStage]=3.40))
					OR ([SourceId]=2 AND ([StatusStage]=0 OR [StatusStage]=3.40)))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		-- 3.50	CPVC Scan-out to Count Centre
		if @StageStatusId1=3.50 -- CPVC Scan-out to Count Centre
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes, 
					CASE
						WHEN S.StatusStage='2.00' AND C.CDID<>S.LocationCode THEN 0
						WHEN S.StatusStage IS NULL THEN 0
						ELSE S.StatusStage
					END [StatusStage],
					LocationCode, LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
					left join NSW_ReturningOffice RO3 WITH (NOLOCK) ON RO3.Code=C.CDID
				WHERE (charindex('CPVC',RO3.Name)>0)
					AND (ContestCode='2') -- Councillor only
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (RO1.Code=@CDID1 OR (@CDID1 IS NULL)) -- the matched RO linked the LGA					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE (([SourceId]=1 and ([StatusStage]=2.00 OR [StatusStage]=3.50))
					or ([SourceId]=2 and ([StatusStage]=0 OR [StatusStage]=3.50)))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=4.00 -- Returning Office Scan-in from Venue
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,
					CASE
						WHEN S.StatusStage='4.00' AND C.CDID<>S.LocationCode THEN 3.00
						WHEN S.StatusStage IS NULL THEN 0
						ELSE S.StatusStage
					END [StatusStage],
					LocationCode, LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
					left join NSW_ReturningOffice RO3 WITH (NOLOCK) ON RO3.Code=C.CDID
				WHERE --(T.Deleted IS NULL or T.DeletedReason='End of Life') 
					(DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL))					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE rn=1 and [SourceId] in (1,2) and ([StatusStage]=3.00 OR [StatusStage]=4.00)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=4.10 -- Returning Office Scan-in from STH Count Centre
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (C.ReturningOfficeName LIKE '%Sydney Town Hall%')
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (RO1.Code=@CDID1 OR (@CDID1 IS NULL)) -- the RO for the LGA							
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE [SourceId] in (1,2) and --([StatusStage]=3.30 OR [StatusStage]=4.10)
				([StatusStage]=3.30 OR [StatusStage]=4.10 or ([StatusStage]=5.05 and ROCode=CountCentreCode)) 
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=4.11 -- Returning Office Scan-in from CPVC
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
					left join NSW_ReturningOffice RO3 WITH (NOLOCK) ON RO3.Code=C.CDID
				WHERE (charindex('CPVC',RO3.Name)>0)
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (RO1.Code=@CDID1 OR (@CDID1 IS NULL)) -- the matched RO linked the LGA					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE [SourceId] in (1,2) and 
				([StatusStage]=3.40 OR [StatusStage]=4.11 or ([StatusStage]=3.50 and ROCode=CountCentreCode))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=4.12 -- Returning Office Scan-in from Processing Location
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL)) 				
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))		
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))			
			) AS Q
			WHERE [StatusStage]=4.12
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=4.13 -- Returning Office Scan Carton into ULD
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR RO1.Code=@CDID1 OR (@CDID1 IS NULL))
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q -- all container scanned in RO
			WHERE ([StatusStage]=2.00 or [StatusStage]=2.10 or [StatusStage]=4.00 or [StatusStage]=4.10 or [StatusStage]=4.11 or [StatusStage]=4.12 or [StatusStage]=4.13 
				or ([StatusStage]=5.13 and ROCode=CountCentreCode))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=5.00 -- Returning Office Scan-out to Warehouse
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE S.ParentBarcode is null -- hide child carton of uld
					AND (S.Deleted IS NULL)
					and (((RO1.Code<>RO1.CountCentreCode or (C.LGACode is null and (C.CDID=@CDID1 or @CDID1 is null))) and ContestCode<>'2') 
						or (RO1.Code=RO1.CountCentreCode or (C.LGACode is null and (C.CDID=@CDID1 or @CDID1 is null))))
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR RO1.Code=@CDID1 OR (@CDID1 IS NULL))
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE (([StatusStage]=2.00 and (CDID=@CDID1 or @CDID1 is null)) -- the barcode scanned in from printer only for this RO of the connote
					or [StatusStage]=4.00 or [StatusStage]=4.10 or [StatusStage]=4.11 or [StatusStage]=4.12 or [StatusStage]=4.13 or [StatusStage]=5.00 or [StatusStage]=5.13)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=5.05 -- STH Count Centre Scan-out to Count Centre
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (C.ReturningOfficeName LIKE '%Sydney Town Hall%')
					AND (S.Deleted IS NULL)
					AND (ContestCode='2') -- Councillor only
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (RO1.Code=@CDID1 OR (@CDID1 IS NULL)) 				
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE [SourceId] in (1,2) and ([StatusStage]=3.20 OR [StatusStage]=5.05)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
		end
		else
		if @StageStatusId1=5.10 -- Returning Office Scan-out to Count Centre
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE S.ParentBarcode is null -- hide child carton of uld
					AND (S.Deleted IS NULL)
					AND (ContestCode='2') -- Councillor only
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 
					AND (RO1.Code=@CDID OR (C.LGACode is null and C.CDID=@CDID) OR @CDID is null) -- the RO mapped to the CC
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))		
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))	-- the final CC mapped to the RO for the LGA		
			) AS Q
			WHERE ([StatusStage]=4.00 or [StatusStage]=4.10 or [StatusStage]=4.11 or [StatusStage]=4.12 or [StatusStage]=4.13 or [StatusStage]=5.10)
				and ROCode<>CountCentreCode
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=5.11 -- Count Centre Scan-in from Returning Office
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (RO1.Code<>RO1.CountCentreCode and ContestCode='2')
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (RO1.Code=@CDID OR @CDID is null) -- the RO mapped to the CC
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))-- the final CC mapped to the RO for the LGA
									
			) AS Q
			WHERE ([StatusStage]=5.10 or [StatusStage]=5.11)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=5.12 -- Count Centre Scan-in from STH Count Centre
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (C.ReturningOfficeName LIKE '%Sydney Town Hall%')
					AND (ContestCode='2' or RO1.Code=RO1.CountCentreCode) -- Councillor only
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 
					AND (RO1.Code=@CDID OR @CDID is null) -- the RO mapped to the CC					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE ([StatusStage]=5.05 OR [StatusStage]=5.12 OR ([StatusStage]=3.30 and ROCode=CountCentreCode))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
		end
		else
		if @StageStatusId1=5.13 -- Count Centre Scan-in from Processing Location
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID OR @CDID is null) 					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))		
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))			
			) AS Q
			WHERE [StatusStage]=5.13
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=5.14 -- Count Centre Scan Carton into ULD
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM (
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode, LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],1 [rn]
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey					
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR RO1.Code=@CDID1 OR (@CDID1 IS NULL))
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE ([StatusStage]=5.11 or [StatusStage]=5.12 or [StatusStage]=5.13 or [StatusStage]=5.14 or [StatusStage]=5.16 
				or ([StatusStage]=4.00 and ROCode=CountCentreCode))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode				
		end
		else
		if @StageStatusId1=5.15 -- Count Centre Scan-out to Warehouse
		begin	
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE S.ParentBarcode is null -- hide child carton of uld
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR RO1.Code=@CDID1 OR (@CDID1 IS NULL))
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE ([StatusStage]=5.11 or [StatusStage]=5.12 or [StatusStage]=5.13 or [StatusStage]=5.14 or [StatusStage]=5.16 or [StatusStage]=5.15
				or (([StatusStage]=4.00 or [StatusStage]=4.10 or [StatusStage]=4.11 or [StatusStage]=4.12 or [StatusStage]=4.13) and ROCode=CountCentreCode))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end		
		else
		if @StageStatusId1=5.16 -- Count Centre Scan-in from CPVC
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
					left join NSW_ReturningOffice RO3 WITH (NOLOCK) ON RO3.Code=C.CDID
				WHERE (ContestCode='2') -- Councillor only
					AND (charindex('CPVC',RO3.Name)>0)
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (RO1.Code=@CDID OR @CDID is null) -- the RO mapped to the CC
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))-- the final CC mapped to the RO for the LGA
									
			) AS Q
			WHERE ([StatusStage]=3.50 or [StatusStage]=5.16)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=6.00 -- Warehouse Scan-in from Returning Office
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (ContestCode<>'2' or (RO1.Code=RO1.CountCentreCode and ContestCode='2'))
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR RO1.Code=@CDID1 OR (@CDID1 IS NULL))
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE ([StatusStage]=5.00 or [StatusStage]=6.00)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
		else
		if @StageStatusId1=6.10 -- Warehouse Scan-in from Count Centre
		begin
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE S.ParentBarcode is null -- hide child carton of uld
					AND (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR RO1.Code=@CDID1 OR (@CDID1 IS NULL))
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE ([StatusStage]=5.15 or [StatusStage]=6.10)
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode			
		end
	end
	else
	begin
		if @ContainerCode1='6'
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL))					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))				
			) AS Q
			WHERE (CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))
				order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
		else
			insert into @ContainerStatusResultTable
			SELECT * FROM
			(     
				SELECT S.ConsKey,S.[LineNo],Barcode,StatusDate,StatusTime,StatusNotes,StatusStage,LocationCode,LocationName,
					SourceId, CDID, ReturningOfficeName,
					LGAId [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
					WardCode, ISNULL(WardName,'Undefined') [WardName],
					VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
					ContestCode, ContestName, ContainerCode, ContainerName,
					case 
						when S.CountCentreCode is not null then S.CountCentreCode
						when RO.CountCentreCode IS not null then RO.CountCentreCode
						when RO1.CountCentreCode IS not null then RO1.CountCentreCode
						else C.CountCentreCode
					end [CountCentreCode],
					case 
						when S.CountCentreCode is not null then S.CountCentreName
						when RO.CountCentreCode IS not null then RO.CountCentreName
						when RO1.CountCentreCode IS not null then RO1.CountCentreName
						else C.CountCentreName
					end [CountCentreName],
					case 
						when S.CountCentreCode is not null then RO2.Code
						when RO.CountCentreCode IS not null then RO.Code
						when RO1.CountCentreCode IS not null then RO1.Code
						else C.CDID
					end [ROCode],
					case 
						when S.CountCentreCode is not null then RO2.Name
						when RO.CountCentreCode IS not null then RO.Name
						when RO1.CountCentreCode IS not null then RO1.Name
						else C.ReturningOfficeName
					end [ROName],
					1 AS rn
				FROM @ContainerStatusLatestTable S 
					INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=S.ConsKey
					left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
					left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
					left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
					left join NSW_ReturningOffice RO2 WITH (NOLOCK) ON RO2.Code=S.CountCentreCode
				WHERE (DeliveryNumber=@DeliveryNum1 OR (@DeliveryNum1 IS NULL)) 		
					AND (CDID=@CDID1 OR (@CDID1 IS NULL))					
					AND (LGAId=@LGACode1 OR (@LGACode1 IS NULL))
					AND (WardCode=@WardCode1 OR (@WardCode1 IS NULL))
					AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
					AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
					AND (ContestCode=@ContestCode1 OR (@ContestCode1 IS NULL))
					AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
					AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))	
					AND (RO1.CountCentreCode=@CountCentreCode1 OR (@CountCentreCode1 IS NULL))				
			) AS Q
			WHERE rn=1 order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode		
	end	
	/*
	if exists(select 1 from @BarcodeTable)
		SELECT R.* FROM @ContainerStatusResultTable R
		INNER JOIN @BarcodeTable B ON B.Barcode=R.Barcode 
	else
	*/
		SELECT * FROM @ContainerStatusResultTable	
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetContainerBarcodeData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetContainerBarcodeData
GO
-- exec NSW_GetContainerBarcodeData '015',null,null,'E',4
-- exec NSW_GetContainerBarcodeData '015',null,null,'L'
-- exec NSW_GetContainerBarcodeData '015','00719','150719020101002002'
-- exec NSW_GetContainerBarcodeData '015','00719','151841020101002002'
-- exec NSW_GetContainerBarcodeData '015','01841','151841020101002002'
-- exec NSW_GetContainerBarcodeData '015','00895','010895020301002002' 
-- exec NSW_GetContainerBarcodeData NULL,NULL,'010895020201001001|010895020501001001'
-- exec NSW_GetContainerBarcodeData NULL,NULL,'010895020301002002'
CREATE PROCEDURE NSW_GetContainerBarcodeData 
	@CDID varchar(3)=null, @VenueCode varchar(5)=null, @BarcodeList varchar(max)=null, 
	@StageStatus char(1)=null, @StageStatusId int=null
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL
	)
	if @StageStatus is not null
	begin	
		if @StageStatus='E'
		begin	
			if @StageStatusId is not null
			begin
				if @StageStatusId>0 and @StageStatusId<4
					INSERT INTO @ContainerStatusLatestTable
					SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
					(     
					SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo], NSW_Container.Barcode,NSW_Container.Added, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
						ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
						ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
					FROM NSW_Container
						LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
							AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					) AS Q
					WHERE rn=1 and ([StatusStage]=@StageStatusId-1 or ([Added] is not null and [StatusStage]=0))
			end
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_Container.Added, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and [StatusStage]<4
		end
		else
		if @StageStatus='L'
		begin
			if @StageStatusId is not null
			begin
				if @StageStatusId>4 and @StageStatusId<7
					INSERT INTO @ContainerStatusLatestTable
					SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
					(     
					SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
						ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
						ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
					FROM NSW_Container
						LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
							AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					) AS Q
					WHERE rn=1 and [StatusStage]=@StageStatusId-1
			end
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and [StatusStage]>3
		end
		else
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1
	end
	else
	begin
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
		) AS Q
		WHERE rn=1
	end
	
	declare @BarcodeTable table (Barcode varchar(40))
	if @BarcodeList is not null and LEN(@BarcodeList)>0
	begin
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList,'|')
		
		SELECT T.Barcode, RIGHT(CDID,2) [CDID], ReturningOfficeName, 
			ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
			RIGHT(VenueCode,4) [VenueCode], 
			case
				when T.VenueName is not null then T.VenueName
				else C.VenueName
			end [VenueName], 
            VenueTypeCode, VenueTypeName, ContestCode, ContestName, ContainerCode, ContainerName, StatusStage 
        FROM NSW_Container T
			INNER JOIN @BarcodeTable B ON B.Barcode=T.Barcode
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
        WHERE T.Deleted IS NULL AND (CDID=@CDID OR (@CDID IS NULL)) 
            AND (VenueCode=@VenueCode OR (@VenueCode IS NULL))
            order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
	end
	ELSE
		SELECT T.Barcode, RIGHT(CDID,2) [CDID], ReturningOfficeName, 
			ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
			RIGHT(VenueCode,4) [VenueCode], 
			case
				when T.VenueName is not null then T.VenueName
				else C.VenueName
			end [VenueName], 
            VenueTypeCode, VenueTypeName, ContestCode, ContestName, ContainerCode, ContainerName, StatusStage 
        FROM NSW_Container T
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
        WHERE T.Deleted IS NULL AND (CDID=@CDID OR (@CDID IS NULL)) 
            AND (VenueCode=@VenueCode OR (@VenueCode IS NULL)) 
            order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
END
GO

-- drop table NSW_StatusStage
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_StatusStage') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_StatusStage(
		[ID] [varchar](40) NOT NULL,
		[StageId] decimal(4,2) NOT NULL,
		[Description] [varchar](200) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_StatusStage_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end
GO
-- delete from NSW_StatusStage
-- select StageId,Description from NSW_StatusStage order by StageId
if not exists(select 1 from NSW_StatusStage)
begin
	-- LGA 2020
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 1.00, 'Printer Scan-out to Returning Office/STH/CPVC', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 2.00, 'Returning Office/STH/CPVC Scan-in from Printer', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 2.10, 'Returning Office Allocate Reserve Stock', GETDATE(), 'smsupport'	
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 3.00, 'Returning Office Scan-out to Venue', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 3.10, 'STH Scan-out to STH Count Centre', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 3.20, 'STH Count Centre Scan-in from STH', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 3.30, 'STH Count Centre Scan-out to Returning Office', GETDATE(), 'smsupport'
		
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 3.40, 'CPVC Scan-out to Returning Office', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 3.50, 'CPVC Scan-out to Count Centre', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 4.00, 'Returning Office Scan-in from Venue', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 4.10, 'Returning Office Scan-in from STH Count Centre', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 4.11, 'Returning Office Scan-in from CPVC', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 4.12, 'Returning Office Scan-in from Processing Location', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 4.13, 'Returning Office Scan Carton into ULD', GETDATE(), 'smsupport'
		
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.00, 'Returning Office Scan-out to Warehouse', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.05, 'STH Count Centre Scan-out to Count Centre', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.10, 'Returning Office Scan-out to Count Centre ', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.11, 'Count Centre Scan-in from Returning Office', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.12, 'Count Centre Scan-in from STH Count Centre', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.13, 'Count Centre Scan-in from Processing Location', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.14, 'Count Centre Scan Carton into ULD', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.15, 'Count Centre Scan-out to Warehouse', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.16, 'Count Centre Scan-in from CPVC', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 6.00, 'Warehouse Scan-in from Returning Office', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 6.10, 'Warehouse Scan-in from Count Centre', GETDATE(), 'smsupport'	
	
	/* SGA 2019
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 1.00, 'Printer Scan-out to Election Manager Office', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 2.00, 'Election Manager Office Scan-in from Printer', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 2.10, 'Election Manager Office Allocate Reserve Stock', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 3.00, 'Election Manager Office Scan-out to Venue', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 4.00, 'Election Manager Office Scan-in from Venue', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.00, 'Election Manager Office Scan-out to Warehouse', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.08, 'Election Manager Office Scan-out to CDVC', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.09, 'Election Manager Office Scan-out to LCCC', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.10, 'CDVC Scan-in from Election Manager Office', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.11, 'LCCC Scan-in from Election Manager Office', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.12, 'CDVC Scan-in from Processing Location', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.13, 'LCCC Scan-in from Processing Location', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.14, 'CDVC Scan-out to LCCC', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.15, 'LCCC Scan-in from CDVC', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.16, 'Processing Location Scan Carton into ULD', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.17, 'CDVC Scan-out to Warehouse', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 5.18, 'LCCC Scan-out to Warehouse', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 6.00, 'Warehouse Scan-in from Election Manager Office', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 6.10, 'Warehouse Scan-in from CDVC', GETDATE(), 'smsupport'
	
	insert into NSW_StatusStage (ID, StageId, [Description], Created, CreatedBy)
	select NEWID(), 6.11, 'Warehouse Scan-in from LCCC', GETDATE(), 'smsupport'
	*/
	/* LGA 2016
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 1.00, 'Printer Scan-out to Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 1.10, 'Printer Scan-out to Sydney Town Hall', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 1.11, 'Sydney Town Hall Scan-in from Printer', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 1.12, 'Sydney Town Hall Scan-out to Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 2.00, 'Returning Office Scan-in from Printer', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 2.10, 'Returning Office Scan-in from Sydney Town Hall', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 3.00, 'Returning Office Scan-out to Venue', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 4.00, 'Returning Office Scan-in from Venue', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.00, 'Returning Office Scan-out to Warehouse', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.10, 'Returning Office Scan-out to Sydney Town Hall', GETDATE(), 'sm'
		
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.11, 'Sydney Town Hall Scan-in from Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.12, 'Sydney Town Hall Scan-out to Warehouse', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.20, 'Returning Office Scan-out to Count Centre', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.21, 'Count Centre Scan-in from Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.22, 'Count Centre Scan-out to Warehouse', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 6.00, 'Warehouse Scan-in from Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 6.10, 'Warehouse Scan-in from Sydney Town Hall', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 6.20, 'Warehouse Scan-in from Count Centre', GETDATE(), 'sm'
	*/
end
go

if not exists(select 1 from SecurityLogin)
begin
	declare @CreatedBy varchar(100)='cts',
		@LoginKey int, @RoleId int, @CategoryId int, @FuncId int
		
-- create admin login			
	insert into SecurityLogin 
		(LoginId,[Password],WebPageTheme,Active,Verified,Created,CreatedBy)
	select 
		'cts@compdata.com.au','4Z+DylOdolIkhX/3c2T8lA==','Glass','Y','Y',GETDATE(),@CreatedBy 
	set @LoginKey=CAST(scope_identity() AS int)
		
-- Security Role
	insert into SecurityRole
		(Name,Active,Created,CreatedBy)
	select 'Administrator','Y',GETDATE(),@CreatedBy
	set @RoleId=CAST(scope_identity() AS int) 
	
-- Security Login Role
	insert into SecurityLoginRole
		(SecurityLogin,SecurityRole,Created,CreatedBy)
	select @LoginKey,@RoleId,GETDATE(),@CreatedBy
		
-- security function and system setup
	insert into SecurityFunctionCategory
		(Name,Active,Created,CreatedBy)
	select 'Security Management and System Setup','Y',GETDATE(),@CreatedBy
	set @CategoryId=CAST(scope_identity() AS int)
	-- Access to Security Role Management
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Security Role Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmRoleCatalogue','Y','Y','Security Role Management','~/SecurityRoleCatalogue.aspx','loginId',GETDATE(),@CreatedBy			
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	-- Access to Security Function Management
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Security Function Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmFunctionCatalogue','Y','Y','Security Function Management','~/SecurityFunctionCategory.aspx','loginId',GETDATE(),@CreatedBy						
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	
	-- Access to security device Management
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Security Device Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
		
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmSecurityDevice','Y','Y','Security Device Catalogue','~/SecurityDeviceCatalogue.aspx','loginId',GETDATE(),@CreatedBy
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	
	-- Access to User Management
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to User Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
		
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmUserManagement','Y','Y','User Management','~/UserCatalogue.aspx','loginId',GETDATE(),@CreatedBy
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'rpAdditional','Y','Y',null,null,null,GETDATE(),@CreatedBy
	/*								
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'edtPasswordExpiry','Y','Y',null,null,null,GETDATE(),@CreatedBy
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'lblPasswordExpiry','Y','Y',null,null,null,GETDATE(),@CreatedBy	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'grdUserRole','Y','Y',null,null,null,GETDATE(),@CreatedBy
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'cbActive','Y','Y',null,null,null,GETDATE(),@CreatedBy	
	*/							
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	
	-- system maintenance
	insert into SecurityFunctionCategory
		(Name,Active,Created,CreatedBy)
	select 'System Maintenance','Y',GETDATE(),@CreatedBy
	set @CategoryId=CAST(scope_identity() AS int)
	
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Maintenance (Returning Office, Venue, Venue Type, Contest, Container Type etc)',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemMaintenance','itmMaint','Y','Y','Maintenance','~/Maintenance.aspx','loginId',GETDATE(),@CreatedBy	
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	
	-- system report
	/*
	insert into SecurityFunctionCategory
		(Name,Active,Created,CreatedBy)
	select 'System Report','Y',GETDATE(),@CreatedBy
	set @CategoryId=CAST(scope_identity() AS int)
	
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Report (Label Details and Status)',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemReport','itmReport','Y','Y','Report','~/Report.aspx','loginId',GETDATE(),@CreatedBy	
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	*/
	-- system utilities
	/*
	insert into SecurityFunctionCategory
		(Name,Active,Created,CreatedBy)
	select 'System Utilities','Y',GETDATE(),@CreatedBy
	set @CategoryId=CAST(scope_identity() AS int)		
	
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Import Consignment Container',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)	
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemUtilities','itmImportContainer','Y','Y','Import Consignment Container','~/ImportContainer.aspx','loginId',GETDATE(),@CreatedBy	
			
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy	
	*/
end
go

if not exists(select 1 from SecurityFunction where Name='Access to Security Device Management')
begin
	declare @CreatedBy varchar(100), 
			@RoleId int, @CategoryId int, @FuncId int
	set @CreatedBy='cts'
	                                                         
	select @CategoryId=ID from SecurityFunctionCategory 
	where Name='Security Management and System Setup'
	
	select @RoleId=ID from SecurityRole
	where Name='Administrator'
	
	insert into SecurityFunction
	(Name,Category,Active,Created,CreatedBy)
	select 'Access to Security Device Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
		
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmSecurityDevice','Y','Y','Security Device Catalogue','~/SecurityDeviceCatalogue.aspx','loginId',GETDATE(),@CreatedBy
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
end
go

-- Generic as SGE implementation


if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetConnoteCatalogueGeneric') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetConnoteCatalogueGeneric
GO
-- exec NSW_GetConnoteCatalogueGeneric 
-- exec NSW_GetConnoteCatalogueGeneric '125'
CREATE PROCEDURE NSW_GetConnoteCatalogueGeneric
	@CDID varchar(3)=null 
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[ParentBarcode] [varchar](40) NULL,
		[ReplaceBarcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL,
		[CountCentreCode] [varchar](5) NULL,
		[CountCentreName] [varchar](100) NULL
	)
	INSERT INTO @ContainerStatusLatestTable
	SELECT ConsKey,[LineNo],Barcode,ParentBarcode,ReplaceBarcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],
		LocationCode,LocationName,CountCentreCode,CountCentreName 
	FROM
	(     
	SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_Container.ParentBarcode,NSW_Container.ReplaceBarcode, 
		NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
		NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.CountCentreName, NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,
		ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
		ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
	FROM NSW_Container WITH (NOLOCK)
		LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
			AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] 
			AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
	) AS Q
	WHERE rn=1
		
	SELECT T.ConsKey,T.[LineNo], C.EventId, C.Source+'(' + CAST(C.SourceId AS VARCHAR) + ')' [Source], 
		C.CDID, C.ReturningOfficeName, 
		C.ReturningOfficeName+' ('+C.CDID+')' [ElectionManagerOffice],
		C.LGAId, C.LGAName+' ('+LGAId+')' [District], 
		C.ContestAreaId, C.ContestAreaCode+' ('+ContestAreaId+')' [ContestArea],
		C.WardCode, C.WardName+' ('+WardCode+')' [Ward],
		C.VenueCode, C.VenueName, C.VenueName+' ('+C.VenueCode+')' [Venue],
		C.VenueTypeCode, C.VenueTypeName, C.VenueTypeName+' ('+VenueTypeCode+')' [VenueType],
		C.ContestCode, C.ContestName, C.ContestName+' ('+ContestCode+')' [ProductType],
		C.ContainerCode, C.ContainerName, C.ContainerName+' ('+ContainerCode+')' [ContainerType],
		C.ContainerQuantity, T.Barcode, T.ParentBarcode, T.ReplaceBarcode, T.Deleted, T.Activated, 
		S.StatusNotes, S.StatusDate,S.StatusTime, S.StatusStage, 
		S.LocationCode,S.LocationName, 
		case
			when S.LocationCode is not null and LEN(S.LocationCode)>0 then 
				S.LocationName+' ('+S.LocationCode+')'
			else
				S.LocationName
		end [LocationScanned],		
		C.CountCentreName+' ('+C.CountCentreCode+')' [CountCentre], -- from PPDM, not trusted
		case 
			when S.CountCentreCode is not null then S.CountCentreCode
			when RO.CountCentreCode IS not null then RO.CountCentreCode
			when RO1.CountCentreCode IS not null then RO1.CountCentreCode
			else C.CountCentreCode
		end [CountCentreCode],
		case 
			when S.CountCentreCode is not null then S.CountCentreName
			when RO.CountCentreCode IS not null then RO.CountCentreName
			when RO1.CountCentreCode IS not null then RO1.CountCentreName
			else C.CountCentreName
		end [CountCentreName],
		case 
			when S.CountCentreCode is not null then S.CountCentreName+' ('+S.CountCentreCode+')'
			when RO.CountCentreCode IS not null then RO.CountCentreName+' ('+RO.CountCentreCode+')'
			when RO1.CountCentreCode IS not null then RO1.CountCentreName+' ('+RO1.CountCentreCode+')'
			else C.CountCentreName+' ('+C.CountCentreCode+')'
		end [FinalCountCentre]
	from NSW_Container T WITH (NOLOCK)
		INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=T.ConsKey
		INNER JOIN @ContainerStatusLatestTable S ON S.ConsKey=T.ConsKey AND S.[LineNo]=T.[LineNo] AND S.Barcode=T.Barcode
		--left join NSW_CountCentre CC WITH (NOLOCK) ON CC.Code=S.CountCentreCode
		left join NSW_ReturningOffice RO WITH (NOLOCK) ON RO.Code=C.CountCentreCode
		left join NSW_LGA LGA WITH (NOLOCK) ON LGA.Code=C.LGAId
		left join NSW_ReturningOffice RO1 WITH (NOLOCK) ON RO1.Code=LGA.ROCode
	WHERE C.CDID=@CDID or @CDID is null
	ORDER BY EventId,SourceId, CDID, VenueName, ContestName, ContainerCode,Barcode
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetSGEContainerBarcodeData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetSGEContainerBarcodeData
GO

-- exec NSW_GetSGEContainerBarcodeData '110',null,null,null,null,null,null,null,null,'6.00'
-- exec NSW_GetSGEContainerBarcodeData '110',null,null,null,null,null,null,null,null,'5.11'

-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,'2493',null,null,'5.17'
-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.17'
-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'6.10'
-- exec NSW_GetSGEContainerBarcodeData '110','2','2','654',null,null,null,null,null,'3'
-- exec NSW_GetSGEContainerBarcodeData '102','2','2','654',null,null,null,null,null,'3'
-- exec NSW_GetSGEContainerBarcodeData '101','1','1',null,null,null,null,null,null,'2.00'
-- exec NSW_GetSGEContainerBarcodeData '101','1','1','919',null,null,null,null,null,5.20
-- exec NSW_GetSGEContainerBarcodeData '003','018',null,'00709',null,'02',null,null,1.00
CREATE PROCEDURE NSW_GetSGEContainerBarcodeData 
	@EMOCode varchar(3)=null, @DistrictCode varchar(3)=null, @ContestAreaId varchar(3)=null,
	@VenueCode varchar(5)=null, @VenueTypeCode varchar(2)=null,
	@ProductCode varchar(2)=null, @ScanLocCode varchar(5)=null, @ContainerCode varchar(2)=null,
	@BarcodeList varchar(max)=null, 
	@StageStatusId varchar(4)=null
AS 
BEGIN
	declare 
		@EMOCode1 varchar(3)=@EMOCode, @DistrictCode1 varchar(3)=@DistrictCode, @ContestAreaId1 varchar(3)=@ContestAreaId,
		@VenueCode1 varchar(5)=@VenueCode, @VenueTypeCode1 varchar(2)=@VenueTypeCode,
		@ProductCode1 varchar(2)=@ProductCode, @ScanLocCode1 varchar(5)=@ScanLocCode, @ContainerCode1 varchar(2)=@ContainerCode,
		@BarcodeList1 varchar(max)=@BarcodeList, @StageStatusId1 varchar(4)=@StageStatusId
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,		
		[CountCentreCode] [varchar](5) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL,
		[ContestName] [varchar](100) NULL,
		[SourceId] [int] NULL
	)
	
	/*
	StageId	Description
	1.00	Printer Scan-out to Election Manager Office
	2.00	Election Manager Office Scan-in from Printer
	2.10	Election Manager Office Allocate Reserve Stock
	3.00	Election Manager Office Scan-out to Venue
	4.00	Election Manager Office Scan-in from Venue
	5.00	Election Manager Office Scan-out to Warehouse
	5.08	Election Manager Office Scan-out to CDVC
	5.09	Election Manager Office Scan-out to LCCC
	5.10	CDVC Scan-in from Election Manager Office
	5.11	LCCC Scan-in from Election Manager Office
	5.12	CDVC Scan-in from Processing Location
	5.13	LCCC Scan-in from Processing Location
	5.14	CDVC Scan-out to LCCC
	5.15	LCCC Scan-in from CDVC
	5.16	Processing Location Scan Carton into ULD
	5.17	CDVC Scan-out to Warehouse
	5.18	LCCC Scan-out to Warehouse
	6.00	Warehouse Scan-in from Election Manager Office
	6.10	Warehouse Scan-in from CDVC
	6.11	Warehouse Scan-in from LCCC
	*/
	DECLARE @EMOName VARCHAR(100)
	IF @EMOCode1 IS NOT NULL
		SELECT @EMOName=Name FROM NSW_ReturningOffice WHERE Code=@EMOCode1
		
	
	if @StageStatusId1 is not null
	BEGIN
		-- print '@StageStatusId defined'
		if @StageStatusId1=1.00
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes],[StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo], NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Connote.SourceId=1 AND NSW_Container.Deleted IS NULL 
			) AS Q
			WHERE rn=1 and [StatusStage]=0
		end
		else
		-- exec NSW_GetSGEContainerBarcodeData '101',null,null,null,null,null,null,null,null,'2.00'		
		if @StageStatusId1=2.00 -- EM Office Scan-in from Printer
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId 
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], 
				CASE
					WHEN NSW_ContainerStatus.StatusStage='2.00' AND NSW_Connote.CDID<>NSW_ContainerStatus.LocationCode THEN 0
					WHEN NSW_ContainerStatus.StatusStage IS NULL THEN 0
					ELSE NSW_ContainerStatus.StatusStage
				END [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Connote.SourceId=1 AND NSW_Container.Deleted IS NULL
			) AS Q
			WHERE rn=1 and ([StatusStage]=0 or [StatusStage]=1.00 or [StatusStage]=2.00)
		end	
		else
		-- exec NSW_GetSGEContainerBarcodeData '101',null,null,null,null,null,null,null,null,'3.00'
		if @StageStatusId1=3.00 -- EM Office Scan-out to Venue
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], 
				CASE
					WHEN NSW_ContainerStatus.StatusStage='2.00' AND NSW_Connote.CDID<>NSW_ContainerStatus.LocationCode THEN 0
					WHEN NSW_ContainerStatus.StatusStage IS NULL THEN 0
					ELSE NSW_ContainerStatus.StatusStage
				END [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Connote.SourceId IN (1,2) AND NSW_Container.Deleted IS NULL
				and (NSW_Connote.ContestName not like '%RESERVE STOCK%')
				AND (NSW_Connote.ReturningOfficeName NOT LIKE '%Sydney Town Hall%')
			) AS Q
			WHERE rn=1 and ([StatusStage]=2.00 OR [StatusStage]=2.10 OR [StatusStage]=3.00)
		end
		else
		-- exec NSW_GetSGEContainerBarcodeData '101',null,null,null,null,null,null,null,null,'4.00'
		if @StageStatusId1=4.00 -- EM Office Scan-in from Venue
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId 
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], 
				CASE
					WHEN NSW_ContainerStatus.StatusStage='4.00' AND NSW_Connote.CDID<>NSW_ContainerStatus.LocationCode THEN 3.00
					WHEN NSW_ContainerStatus.StatusStage IS NULL THEN 0
					ELSE NSW_ContainerStatus.StatusStage
				END [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Connote.SourceId IN (1,2) 
				AND (NSW_Container.Deleted IS NULL or NSW_Container.DeletedReason='End of Life') 
			) AS Q
			WHERE rn=1 and ([StatusStage]=3.00 or [StatusStage]=4.00)
		end
		else
		-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.00'
		if @StageStatusId1=5.00 -- EM Office Scan-out to Warehouse
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] 
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Connote.SourceId IN (1,2) AND (NSW_Container.Deleted IS NULL) 
				AND (NSW_Connote.ContestCode not in ('6','9','14')) --Excluding: 6-LC; 9-LC SYDNEY TOWN HALL; 14-DECLARATION VOTES
				AND (NSW_Connote.ReturningOfficeName NOT LIKE '%Sydney Town Hall%')
			) AS Q
			WHERE rn=1 and ([StatusStage]=2.00 or [StatusStage]=4.00 or [StatusStage]=5.00)
		end
		else
		-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.08'
		-- exec NSW_GetSGEContainerBarcodeData '110',null,null,null,null,null,null,null,null,'5.08'
		if @StageStatusId1=5.08 -- Election Manager Office Scan-out to CDVC
		begin
			IF @EMOName IS NOT NULL AND LEN(@EMOName)>0 AND CHARINDEX('Sydney Town Hall', @EMOName)>0
			BEGIN
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
				FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.ContainerCode,NSW_Connote.SourceId,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container WITH (NOLOCK)
					INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
						AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
				WHERE NSW_Connote.SourceId IN (1,2) AND NSW_Container.Deleted IS NULL 
					AND NSW_Connote.ContestCode IN ('5','9','14') -- 5-LA SYDNEY TOWN HALL; 9-LC SYDNEY TOWN HALL; 14-Declaration votes 
				) AS Q
				WHERE rn=1 and (([StatusStage]=0.00 and [SourceId]=2) or [StatusStage]=2.00 or [StatusStage]=5.08) 
			END
			ELSE
			BEGIN 
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
				FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.ContainerCode,NSW_Connote.SourceId,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container WITH (NOLOCK)
					INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
						AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
				WHERE NSW_Connote.SourceId IN (1,2) AND NSW_Container.Deleted IS NULL
					AND (NSW_Connote.ContestCode='14' -- "DECLARATION VOTES" only
							OR [StatusStage]=5.08) 
				) AS Q
				WHERE rn=1 and ([StatusStage]=4.00 or [StatusStage]=5.08) 
			END
		end
		else
		-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.09'
		-- exec NSW_GetSGEContainerBarcodeData '110',null,null,null,null,null,null,null,null,'5.09'
		if @StageStatusId1=5.09 -- Election Manager Office Scan-out to LCCC
		begin
			IF @EMOName IS NOT NULL AND LEN(@EMOName)>0 AND CHARINDEX('Sydney Town Hall', @EMOName)>0
			begin
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
				FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.ContainerCode,NSW_Connote.SourceId,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container WITH (NOLOCK)
					INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
						AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
				WHERE NSW_Connote.SourceId IN (1,2) AND NSW_Container.Deleted IS NULL AND NSW_Connote.ContestCode='9' -- "LC SYDNEY TOWN HALL" only					
				) AS Q
				WHERE rn=1 and (([StatusStage]=0.00 and [SourceId]=2) or [StatusStage]=2.00 or [StatusStage]=5.09)
			end
			else
			begin
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
				FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.ContainerCode,NSW_Connote.SourceId,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container WITH (NOLOCK)
					INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
						AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
				WHERE NSW_Connote.SourceId IN (1,2) AND NSW_Container.Deleted IS NULL 
					AND (NSW_Connote.ContestCode='6' -- "LC" only
							OR [StatusStage]=5.09)  					
				) AS Q
				WHERE rn=1 and ([StatusStage]=4.00 or [StatusStage]=5.09)
			end
		end		
		else
		-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.10'
		if @StageStatusId1=5.10 -- CDVC Scan-in from EM Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Connote.SourceId IN (1,2)
				AND (NSW_Container.Deleted IS NULL or NSW_Container.DeletedReason='End of Life') 
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.08 or [StatusStage]=5.10)
		end
		else
		-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.11'
		if @StageStatusId1=5.11 -- LCCC Scan-in from EM Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Connote.SourceId IN (1,2) 
				AND (NSW_Container.Deleted IS NULL or NSW_Container.DeletedReason='End of Life') 
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.09 or [StatusStage]=5.11)
		end
		else
		-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.12'
		if @StageStatusId1=5.12 -- CDVC Scan-in from Processing Location, brand new nswec label
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL
			) AS Q
			WHERE rn=1 and [StatusStage]=5.12
		end
		else
		-- exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.13'
		if @StageStatusId1=5.13 -- LCCC Scan-in from Processing Location, brand new nswec label
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL
			) AS Q
			WHERE rn=1 and [StatusStage]=5.13
		end
		else
		/*
		exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.14'
		exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.15'
		*/		
		if @StageStatusId1=5.14 -- CDVC Scan-out to LCCC
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL
				and NSW_Container.ParentBarcode is null --and CHARINDEX('CDVC', LocationName)>0 
				AND (NSW_Connote.ContestCode='9' -- 9-LC SYDNEY TOWN HALL; deepac stated - Only LC products should be available in �Scan out to LCCC�
					OR NSW_Container.ULDProduct='LC' -- all child container are the same product type 9-LC SYDNEY TOWN HALL) 
					OR NSW_Connote.[Source]='CDVC') -- ALL NSWEC INTERNAL LABEL WITH CDVC AS THE SOURCE)
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.10 or [StatusStage]=5.12 or [StatusStage]=5.14 or ([StatusStage]=5.16 and CHARINDEX('CDVC', LocationName)>0))
		end
		else
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.15'	
		if @StageStatusId1=5.15 -- LCCC Scan-in from CDVC
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL --AND NSW_Connote.ContainerCode<>'6' 
				AND NSW_Container.ParentBarcode IS NULL 
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.14 or [StatusStage]=5.15)
			
			declare @tblULD table (	Barcode varchar(40))
			insert into @tblULD
			SELECT distinct Barcode
			FROM
			(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container WITH (NOLOCK)
					INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
						AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
				WHERE NSW_Container.Deleted IS NULL AND NSW_Connote.ContainerCode='6' 
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.14 or [StatusStage]=5.15)

			if exists(select 1 from @tblULD)
			begin
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
				FROM
				(     
					SELECT DISTINCT NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
						ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
						NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
						ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
					FROM NSW_Container WITH (NOLOCK)
						INNER JOIN @tblULD ULD ON ULD.Barcode=NSW_Container.ParentBarcode
						INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
						LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
							AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
							AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
					WHERE NSW_Container.Deleted IS NULL  
				) AS Q
				WHERE rn=1
			end
		end
		else	
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,'46',null,null,'5.16'		
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,'2564',null,null,'5.16'	
		if @StageStatusId1=5.16 -- Processing Location Scan Carton into ULD
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL 
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.10 or [StatusStage]=5.11 or [StatusStage]=5.12 or [StatusStage]=5.13 or [StatusStage]=5.15 or [StatusStage]=5.16)
		end
		else			
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.17'		
		if @StageStatusId1=5.17 -- CDVC Scan-out to Warehouse
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL
				and NSW_Container.ParentBarcode is null -- AND CHARINDEX('CDVC', LocationName)>0 -- hide child carton of uld
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.10 or [StatusStage]=5.12 or ([StatusStage]=5.16 AND CHARINDEX('CDVC', LocationName)>0) or [StatusStage]=5.17)
		end		
		else	
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'5.18'		
		if @StageStatusId1=5.18 -- LCCC Scan-out to Warehouse
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL
				and NSW_Container.ParentBarcode is null --AND CHARINDEX('LCCC', LocationName)>0 -- hide child carton of uld
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.11 or [StatusStage]=5.13 or [StatusStage]=5.15 or ([StatusStage]=5.16 AND CHARINDEX('LCCC', LocationName)>0) or [StatusStage]=5.18)
		end		
		else
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'6.00'	
		if @StageStatusId1=6.00 -- Warehouse Scan-in from EM Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Connote.SourceId IN (1,2) AND NSW_Container.Deleted IS NULL
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.00 or [StatusStage]=6.00)
		end
		else
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'6.10'
		if @StageStatusId1=6.10 -- Warehouse Scan-in from CDVC
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.17 or [StatusStage]=6.10)
		end
		else
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,'6.11'
		if @StageStatusId1=6.11 -- Warehouse Scan-in from LCCC
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
			FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container WITH (NOLOCK)
				INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
			WHERE NSW_Container.Deleted IS NULL
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.18 or [StatusStage]=6.11)
		end
	END
	else
	begin
		--exec NSW_GetSGEContainerBarcodeData null,null,null,null,null,null,null,null,null,null
		--print '@StageStatusId undefined'
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName,ContestName,SourceId
		FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContestName,NSW_Connote.SourceId,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container WITH (NOLOCK)
			INNER JOIN NSW_Connote WITH (NOLOCK) ON NSW_Connote.ConsKey=NSW_Container.ConsKey
			LEFT JOIN NSW_ContainerStatus WITH (NOLOCK) ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				AND (NSW_ContainerStatus.Duplicated is null or NSW_ContainerStatus.Duplicated='N') AND NSW_ContainerStatus.Deleted is null
		WHERE NSW_Container.Deleted IS NULL
		) AS Q
		WHERE rn=1
	end
	
	declare @BarcodeTable table (Barcode varchar(40))
	if @BarcodeList1 is not null and LEN(@BarcodeList1)>0
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList1,'|')
		
	if exists(select 1 from @BarcodeTable)
	begin
		SELECT T.Barcode, CDID [EMOCode], ReturningOfficeName [EMOName], 
			LGAId [DistrictCode], ISNULL(LGAName,'Undefined') [DistrictName],
			ContestAreaId, ISNULL(ContestAreaCode,'Undefined') [ContestAreaCode],
			VenueCode, C.VenueName,
			VenueTypeCode, VenueTypeName, 
			ContestCode [ProductCode], C.ContestName [ProductName], 
			ContainerCode,ContainerName,StatusStage,StatusDate,S.CountCentreCode,
			LocationCode,LocationName
		FROM NSW_Container T WITH (NOLOCK)
			INNER JOIN @BarcodeTable B ON B.Barcode=T.Barcode
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=T.ConsKey
		WHERE T.Barcode IS NOT NULL AND LEN(T.Barcode)>0
			AND (CDID=@EMOCode1 OR (@EMOCode1 IS NULL)) 
			AND (LGAId=@DistrictCode1 OR (@DistrictCode1 IS NULL)) 
			AND (ContestAreaId=@ContestAreaId1 OR (@ContestAreaId1 IS NULL))
			AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL))
			AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
			AND (ContestCode=@ProductCode1 OR (@ProductCode1 IS NULL))
			AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
			AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))
		order by ReturningOfficeName, C.VenueName, VenueTypeName, C.ContestName, Barcode
	end
	ELSE
	begin
		SELECT T.Barcode, CDID [EMOCode], ReturningOfficeName [EMOName],
			LGAId [DistrictCode], ISNULL(LGAName,'Undefined') [DistrictName],
			ContestAreaId, ISNULL(ContestAreaCode,'Undefined') [ContestAreaCode],
			VenueCode, C.VenueName,
			VenueTypeCode, VenueTypeName, 
			ContestCode [ProductCode], C.ContestName [ProductName], 
			ContainerCode, ContainerName, StatusStage, StatusDate,S.CountCentreCode,
			LocationCode,LocationName 
		FROM NSW_Container T WITH (NOLOCK)
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C WITH (NOLOCK) ON C.ConsKey=T.ConsKey
		WHERE T.Barcode IS NOT NULL AND LEN(T.Barcode)>0
			AND (CDID=@EMOCode1 OR (@EMOCode1 IS NULL)) 
			AND (LGAId=@DistrictCode1 OR (@DistrictCode1 IS NULL))
			AND (ContestAreaId=@ContestAreaId1 OR (@ContestAreaId1 IS NULL))
			AND (VenueCode=@VenueCode1 OR (@VenueCode1 IS NULL)) 
			AND (VenueTypeCode=@VenueTypeCode1 OR (@VenueTypeCode1 IS NULL))
			AND (ContestCode=@ProductCode1 OR (@ProductCode1 IS NULL))
			AND (ContainerCode=@ContainerCode1 OR (@ContainerCode1 IS NULL))
			AND (LocationCode=@ScanLocCode1 OR (@ScanLocCode1 IS NULL))
		order by ReturningOfficeName, C.VenueName, VenueTypeName, C.ContestName, Barcode
	end
END
GO


if exists (select * from dbo.sysobjects where id = object_id(N'NSW_CheckContainerStatusDuplicates') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_CheckContainerStatusDuplicates
GO
--exec NSW_CheckContainerStatusDuplicates
CREATE PROCEDURE NSW_CheckContainerStatusDuplicates
AS 
BEGIN
	declare @tlbDuplicates table
	(
		[LGAName] [varchar](100) NULL,
		[ContestName] [varchar](100) NULL,
		[VenueName] [varchar](100) NULL,
		[Barcode] [varchar](40) NULL,
		[StatusNotes] [varchar](500) NULL,
		[scan_count] [int] NULL,
		[extra_scan] [int] NULL
	)
	insert into @tlbDuplicates
	select cn.LGAName [District], cn.ContestName [Product], cn.VenueName, b.*,  scan_count-1 [extra_scan]  
	from (
			select distinct barcode, StatusNotes, count(barcode) as [scan_count] 
			from (
					select m.barcode, s.StatusDate,s.StatusTime, s.StatusNotes from NSW_ContainerStatus s inner join (
						select distinct t.Barcode, t.ConsKey, t.[LineNo] from NSW_Container t inner join
						(
							select * from (
								select ConsKey,[LineNo], StatusNotes, count(*) cou from NSW_ContainerStatus
								WHERE Duplicated IS NULL
								group by  ConsKey,[LineNo], StatusNotes
							) g
							where cou>1
						) k on k.ConsKey=t.ConsKey and k.[LineNo]=t.[LineNo]) m
							on m.ConsKey=s.ConsKey and m.[LineNo]=s.[LineNo]
					) a
			group by barcode, StatusNotes
		) b
	inner join NSW_Container t on t.Barcode=b.Barcode
	left join NSW_Connote cn on cn.ConsKey= t.ConsKey
	where scan_count>1
	order by b.Barcode, StatusNotes

	if exists(select 1 from @tlbDuplicates)
	begin
		declare @Err int
		
		BEGIN TRAN
		
		declare @Barcode varchar(40),@StatusNotes [varchar](500),@extraSscan int 
		DECLARE dupCursor CURSOR FOR 
		SELECT distinct Barcode, StatusNotes, extra_scan FROM @tlbDuplicates              
		OPEN dupCursor                                                       
		FETCH dupCursor INTO @Barcode,@StatusNotes,@extraSscan                     
		WHILE @@FETCH_STATUS = 0                                     
		BEGIN 
			with cts as (
			select top (@extraSscan) 
			s.* from NSW_ContainerStatus s
			inner join NSW_Container t on t.ConsKey=s.ConsKey and t.[LineNo]=s.[LineNo]
			where t.Barcode=@Barcode and s.StatusNotes=@StatusNotes
			order by s.StatusDate
			)
			update cts set Duplicated='Y' 
			
			SET @Err = @@Error
			IF @Err <> 0 
			begin
				CLOSE dupCursor                                                   
				DEALLOCATE dupCursor 
				GOTO EXCEPTION
			end
			
			FETCH dupCursor INTO @Barcode,@StatusNotes,@extraSscan                  
		END                                                          
		CLOSE dupCursor                                                      
		DEALLOCATE dupCursor
		
		COMMIT TRAN
		
		RETURN
		
		EXCEPTION:
		ROLLBACK TRAN
	end
end
go

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetSGEContainerBarcodeReportData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetSGEContainerBarcodeReportData
GO
--select COUNT(*) from NSW_Container
-- exec NSW_GetSGEContainerBarcodeReportData null,null,null,null,null,null,null,null,null,null,'2.00'
-- exec NSW_GetSGEContainerBarcodeReportData 1,125,200,null,200,null,null,null,null,null,'2.00'
CREATE PROCEDURE NSW_GetSGEContainerBarcodeReportData 
	@SourceId varchar(2)=null, @EMOCode varchar(3)=null, @DistrictCode varchar(5)=null, @ContestAreaId varchar(5)=null,
	@WardCode varchar(5)=null, @VenueCode varchar(5)=null, @VenueTypeCode varchar(2)=null,
	@ProductCode varchar(2)=null, @ContainerCode varchar(2)=null, @ScanLocCode varchar(5)=null, 
	@StageStatusId varchar(4)=null
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,		
		[CountCentreCode] [varchar](5) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL
	)
	
	if @StageStatusId is not null
	BEGIN
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime,[StatusNotes],[StatusStage],CountCentreCode,LocationCode,LocationName
		FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			INNER JOIN NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
		) AS Q
		WHERE rn=1 and [StatusStage]=@StageStatusId
	END
	else
	begin
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName
		FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			INNER JOIN NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] 
		) AS Q
		WHERE rn=1
	end
	
	SELECT T.ConsKey,T.[LineNo], T.Barcode, CDID [EMOCode], ReturningOfficeName [EMOName],
		LGAId [DistrictCode], ISNULL(LGAName,'') [DistrictName],
		ContestAreaId, ISNULL(ContestAreaCode,'') [ContestAreaCode],
		WardCode, ISNULL(WardName,'') [WardName],
		VenueCode, C.VenueName,VenueTypeCode, VenueTypeName, 
		ContestCode [ProductCode], ContestName [ProductName], 
		ContainerCode, ContainerName,StatusStage,StatusNotes,StatusDate,LocationCode,LocationName,
		ParentBarcode,
		case
			when T.Deleted is not null then 'Y' 
			else 'N'
		end [Deactivated]
	FROM NSW_Container T
		inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
		INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
	WHERE (SourceId=@SourceId OR (@SourceId IS NULL))
		AND (CDID=@EMOCode OR (@EMOCode IS NULL)) 
		AND (LGAId=@DistrictCode OR (@DistrictCode IS NULL))
		AND (ContestAreaId=@ContestAreaId OR (@ContestAreaId IS NULL))
		AND (WardCode=@WardCode OR (@WardCode IS NULL))
		AND (VenueCode=@VenueCode OR (@VenueCode IS NULL)) 
		AND (VenueTypeCode=@VenueTypeCode OR (@VenueTypeCode IS NULL))
		AND (ContestCode=@ProductCode OR (@ProductCode IS NULL))
		AND (ContainerCode=@ContainerCode OR (@ContainerCode IS NULL))
		AND (LocationCode=@ScanLocCode OR (@ScanLocCode IS NULL))
	order by ReturningOfficeName, C.VenueName, VenueTypeName, ContestName, Barcode
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetSGEContainerBarcodeReportDetailData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetSGEContainerBarcodeReportDetailData
GO
-- exec NSW_GetSGEContainerBarcodeReportData '2', null,null,null,null,null,null,null,null,6.00
-- exec NSW_GetSGEContainerBarcodeReportDetailData null,null,null,null,null,null,null,null,6.00
-- exec NSW_GetSGEContainerBarcodeReportData '101','1','1',null,null,null,null,null,'2.00'
-- exec NSW_GetSGEContainerBarcodeReportDetailData '101','1','1',null,null,null,null,null,'2.00'
CREATE PROCEDURE NSW_GetSGEContainerBarcodeReportDetailData 
	@SourceId varchar(2)=null, @EMOCode varchar(3)=null, @DistrictCode varchar(5)=null, @ContestAreaId varchar(5)=null,
	@VenueCode varchar(5)=null, @VenueTypeCode varchar(2)=null,
	@ProductCode varchar(2)=null, @ContainerCode varchar(2)=null, @ScanLocCode varchar(5)=null, 
	@StageStatusId varchar(4)=null
AS 
BEGIN
	exec NSW_CheckContainerStatusDuplicates
	
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,		
		[CountCentreCode] [varchar](5) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL
	)
	
	if @StageStatusId is not null
	BEGIN
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime,[StatusNotes],[StatusStage],CountCentreCode,LocationCode,LocationName
		FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			INNER JOIN NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
		) AS Q
		WHERE rn=1 and [StatusStage]=@StageStatusId
	END
	else
	begin
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName
		FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			INNER JOIN NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] 
		) AS Q
		WHERE rn=1
	end
	
	SELECT CS.* FROM NSW_ContainerStatus CS
		INNER JOIN @ContainerStatusLatestTable S on S.ConsKey=CS.ConsKey and S.[LineNo]=CS.[LineNo]
		INNER JOIN NSW_Connote C ON C.ConsKey=S.ConsKey
	WHERE (SourceId=@SourceId OR (@SourceId IS NULL))
		AND(CDID=@EMOCode OR (@EMOCode IS NULL)) 
		AND (LGAId=@DistrictCode OR (@DistrictCode IS NULL))
		AND (ContestAreaId=@ContestAreaId OR WardCode=@ContestAreaId OR (@ContestAreaId IS NULL))
		AND (VenueCode=@VenueCode OR (@VenueCode IS NULL)) 
		AND (VenueTypeCode=@VenueTypeCode OR (@VenueTypeCode IS NULL))
		AND (ContestCode=@ProductCode OR (@ProductCode IS NULL))
		AND (ContainerCode=@ContainerCode OR (@ContainerCode IS NULL))
		AND (S.LocationCode=@ScanLocCode OR (@ScanLocCode IS NULL))
		and CS.Duplicated IS NULL
	ORDER BY CS.ConsKey,CS.[LineNo],CS.StatusDate desc	
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetSGEContainerBarcodeReportULDDetailData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetSGEContainerBarcodeReportULDDetailData
GO
/*
 exec NSW_GetSGEContainerBarcodeReportData '2', null,null,null,null,null,null,null,null,6.10
 exec NSW_GetSGEContainerBarcodeReportDetailData null,null,null,null,null,null,null,null,6.10
 exec NSW_GetSGEContainerBarcodeReportULDDetailData null,null,null,null,null,null,null,null,6.10
*/
CREATE PROCEDURE NSW_GetSGEContainerBarcodeReportULDDetailData 
	@SourceId varchar(2)=null, @EMOCode varchar(3)=null, @DistrictCode varchar(5)=null, @ContestAreaId varchar(5)=null,
	@VenueCode varchar(5)=null, @VenueTypeCode varchar(2)=null,
	@ProductCode varchar(2)=null, @ContainerCode varchar(2)=null, @ScanLocCode varchar(5)=null, 
	@StageStatusId varchar(4)=null
AS 
BEGIN
	/*
	declare @EMOCode varchar(3)=null, @DistrictCode varchar(3)=null, @ContestAreaId varchar(3)=null,
	@VenueCode varchar(5)=null, @VenueTypeCode varchar(2)=null,
	@ProductCode varchar(2)=null, @ContainerCode varchar(2)=null, @ScanLocCode varchar(5)=null, 
	@StageStatusId varchar(4)=null
	set @StageStatusId ='6.10'
	*/
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,		
		[CountCentreCode] [varchar](5) NULL,
		[LocationCode] [varchar](5) NULL,
		[LocationName] [varchar](100) NULL
	)
	
	declare @TmpTable table
	(
		[ConsKey] [varchar](40) NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL
	)
	
	if @StageStatusId is not null
	BEGIN
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime,[StatusNotes],[StatusStage],CountCentreCode,LocationCode,LocationName
		FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContainerName,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			INNER JOIN NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] 
		) AS Q
		WHERE rn=1 and [StatusStage]=@StageStatusId and ContainerName='ULD'
	END
	else
	begin
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode,LocationCode,LocationName
		FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			NSW_ContainerStatus.CountCentreCode,NSW_ContainerStatus.LocationCode,NSW_ContainerStatus.LocationName,NSW_Connote.ContainerName,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			INNER JOIN NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
		) AS Q
		WHERE rn=1 and ContainerName='ULD'
	end
	
	insert into @TmpTable
	SELECT distinct C.ConsKey, S.[LineNo], S.Barcode FROM @ContainerStatusLatestTable S 
		INNER JOIN NSW_Connote C ON C.ConsKey=S.ConsKey
	WHERE (SourceId=@SourceId OR (@SourceId IS NULL))
		AND(CDID=@EMOCode OR (@EMOCode IS NULL)) 
		AND (LGAId=@DistrictCode OR (@DistrictCode IS NULL))
		AND (ContestAreaId=@ContestAreaId OR WardCode=@ContestAreaId OR (@ContestAreaId IS NULL))
		AND (VenueCode=@VenueCode OR (@VenueCode IS NULL)) 
		AND (VenueTypeCode=@VenueTypeCode OR (@VenueTypeCode IS NULL))
		AND (ContestCode=@ProductCode OR (@ProductCode IS NULL))
		AND (ContainerCode=@ContainerCode OR (@ContainerCode IS NULL))
		AND (S.LocationCode=@ScanLocCode OR (@ScanLocCode IS NULL))
			
	select S.ConsKey, S.[LineNo], T.Barcode,
		CDID [EMOCode], ReturningOfficeName [EMOName],
		LGAId [DistrictCode], ISNULL(LGAName,'') [DistrictName],
		ContestAreaId, ISNULL(ContestAreaCode,'') [ContestAreaCode],
		WardCode, ISNULL(WardName,'') [WardName],
		VenueCode, C.VenueName,VenueTypeCode, VenueTypeName, 
		ContestCode [ProductCode], ContestName [ProductName], 
		ContainerCode, ContainerName
	 from NSW_Container T
		inner join NSW_Connote C on C.ConsKey=T.ConsKey
		INNER JOIN @TmpTable S on S.Barcode=T.ParentBarcode
	order by s.ConsKey, S.[LineNo], T.Barcode
	
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetSGEULDChildContainerBarcodeData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetSGEULDChildContainerBarcodeData
GO
-- EXEC NSW_GetSGEULDChildContainerBarcodeData '120000069922'
-- EXEC NSW_GetSGEULDChildContainerBarcodeData '120000070022'
-- EXEC NSW_GetSGEULDChildContainerBarcodeData null,646
CREATE PROCEDURE NSW_GetSGEULDChildContainerBarcodeData 
	@ULDBarcode varchar(40)=null,@ULDConsKey [varchar](40)=null
AS 
BEGIN
	if @ULDConsKey is not null
		SELECT distinct T.ConsKey, T.[LineNo], UDL.[LineNo] [ULDLineNo], T.Barcode, CDID [EMOCode], ReturningOfficeName [EMOName],
			LGAId [DistrictCode], ISNULL(LGAName,'Undefined') [DistrictName],
			ContestAreaId, ISNULL(ContestAreaCode,'Undefined') [ContestAreaCode],
			VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
			ContestCode [ProductCode], C.ContestName [ProductName], 
			ContainerCode, ContainerName
		FROM NSW_Container T
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
			INNER JOIN NSW_Container UDL on UDL.Barcode=T.ParentBarcode
		WHERE UDL.ConsKey=@ULDConsKey
		order by ReturningOfficeName, C.VenueName, VenueTypeName, C.ContestName, Barcode
	else
	SELECT distinct T.ConsKey, T.[LineNo], T.Barcode, CDID [EMOCode], ReturningOfficeName [EMOName],
		LGAId [DistrictCode], ISNULL(LGAName,'Undefined') [DistrictName],
		ContestAreaId, ISNULL(ContestAreaCode,'Undefined') [ContestAreaCode],
		VenueCode, C.VenueName,	VenueTypeCode, VenueTypeName, 
		ContestCode [ProductCode], C.ContestName [ProductName], 
		ContainerCode, ContainerName
	FROM NSW_Container T
		INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
	WHERE ParentBarcode=@ULDBarcode
	order by ReturningOfficeName, C.VenueName, VenueTypeName, C.ContestName, Barcode
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_BarcodeScanSummaryReport') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_BarcodeScanSummaryReport 
GO
-- exec NSW_BarcodeScanSummaryReport '20190308','20190322'
create procedure NSW_BarcodeScanSummaryReport
	@DateFrom datetime,	@DateTo datetime
As
begin
	/*
select * from NSW_ContainerStatus
order by StatusDate desc
declare @DateFrom datetime,	@DateTo datetime
	set @DateFrom='20190308'
	set @DateTo='20190322'
*/
	exec NSW_CheckContainerStatusDuplicates
	
	declare
		@DateFrom1 datetime=@DateFrom,
		@DateTo1 datetime=@DateTo
	
	declare @tblWeeklyDate table
	(
		DateFrom datetime,
		DateTo datetime
	)
	declare @firstWeekDay int
	declare @bofWeekDate datetime, @eofWeekDate datetime 
	SET DATEFIRST 6
		
	set @firstWeekDay=DATEPART(DW,@DateFrom1)
	set @eofWeekDate= 
	case @firstWeekDay
		when 1 then DATEADD(day,7,@DateFrom1)
		when 2 then DATEADD(day,6,@DateFrom1)
		when 3 then DATEADD(day,5,@DateFrom1)
		when 4 then DATEADD(day,4,@DateFrom1)
		when 5 then DATEADD(day,3,@DateFrom1)
		when 6 then DATEADD(day,2,@DateFrom1)
		when 7 then DATEADD(day,1,@DateFrom1)
	end
	/*
	print @DateTo1 
	print @eofWeekDate
	return
	*/
	if @DateTo1<@eofWeekDate
	begin
		set @DateTo1=DATEADD(day,1,@DateTo1)
		insert into @tblWeeklyDate
		select @DateFrom1, @DateTo1
	end
	else
	begin
		set @bofWeekDate=@DateFrom1		
		insert into @tblWeeklyDate
		select @bofWeekDate, @eofWeekDate
		
		set @bofWeekDate=@eofWeekDate
		set @eofWeekDate=DATEADD(day,7,@eofWeekDate)
		while @eofWeekDate<=@DateTo1
		begin
			insert into @tblWeeklyDate
			select @bofWeekDate,@eofWeekDate 
			
			set @bofWeekDate=@eofWeekDate
			set @eofWeekDate=DATEADD(day,7,@eofWeekDate)
		end
		if @eofWeekDate>@DateTo1
		begin
			set @DateTo1=DATEADD(day,1,@DateTo1)
			insert into @tblWeeklyDate
			select @bofWeekDate,@DateTo1
		end
	end
	--select * from @tblWeeklyDate	
	
	declare @EventId int	
	select top 1 @EventId=PreferredEvent from NSW_Application 	
	
	declare @tblResult table
	(
		WeekDateFrom datetime,
		WeekDateTo datetime,
		NO_SCAN int,
		SF_Charge float,
		SF_GST float,
		SF_ChargeInclGst float		
	)
	
	DECLARE @tblScan table
	(
		ConsKey varchar(40),
		[LineNo] int,
		[StatusDate] datetime
	)	
	
	if @EventId=1 -- Local Government
		insert into @tblScan
		select S.ConsKey,[LineNo], StatusDate from NSW_ContainerStatus S
			inner join NSW_Connote C on C.ConsKey=S.ConsKey
		where Duplicated is null 
			and StatusDate between @DateFrom and @DateTo1+1
			and not ((S.StatusStage=4.13 or S.StatusStage=5.14) and (C.ContainerCode='6' or CHARINDEX('Scanned out of ULD',s.StatusNotes)>0))
	else
		insert into @tblScan
		select S.ConsKey,[LineNo], StatusDate from NSW_ContainerStatus S
			inner join NSW_Connote C on C.ConsKey=S.ConsKey
		where Duplicated is null 
			and StatusDate between @DateFrom and @DateTo1+1
			and not (S.StatusStage=5.16 and (C.ContainerCode='6' or CHARINDEX('Scanned out of ULD',s.StatusNotes)>0))
	--select * from @tblScan

	DECLARE WD CURSOR FOR 
	select WD.* from @tblWeeklyDate WD
	OPEN WD                                                  
	FETCH WD INTO @bofWeekDate, @eofWeekDate       
	WHILE @@FETCH_STATUS = 0                                
	BEGIN 
		if exists(select 1 from @tblScan where 
			[StatusDate]>=@bofWeekDate and [StatusDate]<@eofWeekDate)
		begin
			insert into @tblResult
			select distinct 
				@bofWeekDate, @eofWeekDate-1,				
				COUNT(ConsKey),
				0,0,0
			from @tblScan
			where [StatusDate]>=@bofWeekDate and [StatusDate]<@eofWeekDate
		end
		else
		begin
			insert into @tblResult
			select @bofWeekDate,@eofWeekDate-1,0,0,0,0
		end
				
		IF (@@ERROR <> '')                                        
			BREAK                                      
		
		FETCH WD INTO @bofWeekDate, @eofWeekDate     
	END                                                       
	CLOSE WD                                                   
	DEALLOCATE WD
		
	select 
		R.*, CONVERT(VARCHAR(10), R.WeekDateFrom, 103) + ' - ' + CONVERT(VARCHAR(10), R.WeekDateTo, 103) [DateRange], 
		@DateFrom1 [DateFrom], @DateTo1-1 [DateTo]
	from @tblResult R
	order by R.WeekDateFrom
end
go

