/*
USE [NSWEC]
GO

drop table NSW_ReturningOffice
	drop table NSW_LGA
	drop table NSW_Ward
	drop table NSW_Venue
	drop table NSW_Consignment
	drop table NSW_Connote
	drop table NSW_Container
	drop table NSW_ContainerStatus
	
*/
-- drop table NSW_Application

if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Application') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_Application(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[WebApplicationTitle] [varchar](100) NULL,
		[CopyRightMessage] [varchar](100) NULL,
		[WebInfo] [varchar](max) NULL,
		[SessionTimeOut] [smallint] NULL,
		[MinPasswordChars] [int] NULL,
		[MinPasswordUppercaseChars] [int] NULL,
		[MinPasswordSpecialChars] [int] NULL,
		[PasswordSpecialCharList] [varchar](100) NULL,
		[DaysBeforePasswordExpires] [int] NULL,
		[SupportLogin] [varchar](100) NULL,
		[SupportPassword] [varchar](100) NULL,
		[IncomingVia] [varchar](5) NULL,
		[IncomingMailServer] [varchar](50) NULL,
		[IncomingMailPort] [int] NULL,
		[IncomingMailUserName] [varchar](50) NULL,
		[IncomingMailPassword] [varchar](50) NULL,
		[IncomingMailbox] [varchar](50) NULL,
		[OutgoingMailHost] [varchar](50) NULL,
		[OutgoingMailPort] [int] NULL,
		[OutgoingMailUserName] [varchar](50) NULL,
		[OutgoingMailPassword] [varchar](50) NULL,
		[OutgoingMailFromAddress] [varchar](50) NULL,
		[OutgoingMailFromName] [varchar](50) NULL,
		[MinPasswordNumericChars] [int] NULL,
		[MinPasswordLength] [int] NULL,
		[MaxPasswordLength] [int] NULL,
		[Notice] [varchar](max) NULL,
		[PreferredEvent] [int] NULL,
		[PreferredLabel] [int] NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_NSW_Application] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_SessionTimeOut]  DEFAULT ((20)) FOR [SessionTimeOut]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordChars]  DEFAULT (8) FOR [MinPasswordChars]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordUppercaseChars]  DEFAULT (1) FOR [MinPasswordUppercaseChars]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordSpecialChars]  DEFAULT (0) FOR [MinPasswordSpecialChars]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_DaysBeforePasswordExpires]  DEFAULT (0) FOR [DaysBeforePasswordExpires]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordNumericChars]  DEFAULT (1) FOR [MinPasswordNumericChars]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MinPasswordLength]  DEFAULT (6) FOR [MinPasswordLength]
	ALTER TABLE NSW_Application ADD  CONSTRAINT [DF_NSW_Application_MaxPasswordLength]  DEFAULT (20) FOR [MaxPasswordLength]
end
GO

-- drop table NSW_CountCentre
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_CountCentre') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_CountCentre(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[EventId] [varchar](20) NULL,
		[Code] [varchar](5) NULL,
		[Name] [varchar](100) NULL,		
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_CountCentre_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_CountCentre ADD CONSTRAINT [DF_NSW_CountCentre_Active] DEFAULT ('Y') FOR [Active]
end
GO

if not exists(select 1 from NSW_CountCentre)
begin
	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select '09992','Queanbeyan','Y',GETDATE(),'smsupport'
	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select '09993','Newcastle','Y',GETDATE(),'smsupport'
	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select '09994','Orange','Y',GETDATE(),'smsupport'
	Insert into NSW_CountCentre (Code,Name,Active,Created,CreatedBy)
	select '09995','Riverwood','Y',GETDATE(),'smsupport'
end
GO

-- drop table NSW_ReturningOffice
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_ReturningOffice') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_ReturningOffice(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[EventId] [varchar](20) NULL,
		[Code] [varchar](3) NOT NULL,
		[Name] [varchar](100) NULL,
		[AddressLine1] [varchar](100) NULL,
		[AddressLine2] [varchar](100) NULL,
		[Suburb] [varchar](40) NULL,
		[State] [varchar](10) NULL,
		[PostCode] [varchar](10) NULL,
		[Country] [varchar](40) NULL,
		[Contact] [varchar](40) NULL,
		[DirectPhone] [varchar](20) NULL,
		[OfficePhone] [varchar](20) NULL,
		[Email] [varchar](300) NULL,
		[SpecialInstructions] [varchar](1000) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_ReturningOffice_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_ReturningOffice ADD CONSTRAINT [DF_NSW_ReturningOffice_Active] DEFAULT ('Y') FOR [Active]
end
GO

-- drop table NSW_LGA
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_LGA') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_LGA(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[EventId] [varchar](20) NULL,
		[ROCode] [varchar](3) NULL,
		[ROName] [varchar](100) NULL,
		[Code] [varchar](3) NOT NULL,
		[AreaCode] [varchar](40) NOT NULL,
		[AreaAuthorityName] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_LGA_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_LGA ADD CONSTRAINT [DF_NSW_LGA_Active] DEFAULT ('Y') FOR [Active]
end
GO

-- drop table NSW_Ward
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Ward') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Ward(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[EventId] [varchar](20) NULL,
		[ROCode] [varchar](3) NULL,
		[ROName] [varchar](100) NULL,
		[LGAId] [varchar](3) NOT NULL,
		[LGACode] [varchar](40) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Ward_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Ward ADD CONSTRAINT [DF_NSW_Ward_Active] DEFAULT ('Y') FOR [Active]
end
GO
-- drop table NSW_Venue
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Venue') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Venue(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[EventId] [varchar](20) NULL,
		[ROCode] [varchar](3) NULL,
		[ROName] [varchar](100) NULL,
		[LGACode] [varchar](3) NULL,
		[LGAName] [varchar](100) NULL,
		[WardCode] [varchar](3) NULL,
		[WardName] [varchar](100) NULL,
		[Code] [varchar](5) NOT NULL,
		[Name] [varchar](100) NULL,
		[LongName] [varchar](200) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,
 		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Venue_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Venue ADD CONSTRAINT [DF_NSW_Venue_Active] DEFAULT ('Y') FOR [Active]
end
GO
-- drop table NSW_VenueType
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_VenueType') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_VenueType(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_VenueType_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_VenueType ADD CONSTRAINT [DF_NSW_VenueType_Active] DEFAULT ('Y') FOR [Active]
end
GO
-- drop table NSW_Contest
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Contest') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Contest(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Contest_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Contest ADD CONSTRAINT [DF_NSW_Contest_Active] DEFAULT ('Y') FOR [Active]
end
GO
-- drop table NSW_ProgressiveCount
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_ProgressiveCount') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_ProgressiveCount(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Description] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_ProgressiveCount_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_ProgressiveCount ADD CONSTRAINT [DF_NSW_ProgressiveCount_Active] DEFAULT ('Y') FOR [Active]
end
GO
-- drop table NSW_ContainerType
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_ContainerType') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_ContainerType(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_ContainerType_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_ContainerType ADD CONSTRAINT [DF_NSW_ContainerType_Active] DEFAULT ('Y') FOR [Active]
end
GO

-- drop table NSW_Event
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Event') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_Event(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](2) NOT NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_Event_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Event ADD CONSTRAINT [DF_NSW_Event_Active] DEFAULT ('Y') FOR [Active]
end
GO

-- drop table NSW_Consignment
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Consignment') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_Consignment(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[SequentialId] [int] NULL,
		[EventId] [varchar](20) NULL,
		[CDID] [varchar](3) NULL,
		[ReturningOfficeName] [varchar](100) NULL,
		[AreaAuthorityName] [varchar](100) NULL,
		[DeliveryAddress] [varchar](200) NULL,
		[LGAId] [varchar](3) NULL,
		[LGACode] [varchar](40) NULL,		
		[LGAName] [varchar](100) NULL,
		[WardCode] [varchar](2) NULL,
		[WardName] [varchar](100) NULL,
		[VenueCode] [varchar](5) NULL,
		[VenueName] [varchar](100) NULL,
		[VenueLongName] [varchar](200) NULL,
		[VenueAddress] [varchar](200) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,		
		[ContestCode] [varchar](2) NULL,
		[ContestName] [varchar](100) NULL,
		[ContainerCode] [varchar](2) NULL,
		[ContainerName] [varchar](100) NULL,
		[ContainerQuantity] [int] NULL,
		[ProductCode] [varchar](40) NULL,
		[ProductDescription] [varchar](100) NULL,
		[PrinterJobId] [varchar](4) NULL,
		[PrinterQuantity] [int] NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL
	 CONSTRAINT [NSW_Consignment_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Consignment ADD CONSTRAINT [DF_NSW_Consignment_ContainerQuantity]  DEFAULT ((0)) FOR [ContainerQuantity]
end
GO

-- drop table NSW_Connote
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Connote') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_Connote(
		[ConsKey] [int] IDENTITY(1,1) NOT NULL,
		[ConsNo] [varchar](30) NULL,
		[EventId] [varchar](20) NULL,
		[CDID] [varchar](3) NULL,
		[ReturningOfficeName] [varchar](100) NULL,
		[LGAId] [varchar](3) NULL,
		[LGACode] [varchar](40) NULL,		
		[LGAName] [varchar](100) NULL,
		[WardCode] [varchar](2) NULL,
		[WardName] [varchar](100) NULL,
		[VenueCode] [varchar](5) NULL,
		[VenueName] [varchar](100) NULL,
		[VenueLongName] [varchar](200) NULL,
		[VenueAddress] [varchar](200) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,		
		[ContestCode] [varchar](2) NULL,
		[ContestName] [varchar](100) NULL,
		[ContainerCode] [varchar](2) NULL,
		[ContainerName] [varchar](100) NULL,
		[ContainerQuantity] [int] NULL,
		[AdditionalContainerQuantity] [int] NULL,
		[PrinterJobId] [varchar](4) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
		[Deleted] [datetime] NULL,
		[DeletedBy] [varchar](100) NULL,
		[DeletedReason] [varchar](1000) NULL,
	 CONSTRAINT [NSW_Connote_PK] PRIMARY KEY CLUSTERED 
	(
		[ConsKey] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE NSW_Connote ADD CONSTRAINT [DF_NSW_Connote_ContainerQuantity]  DEFAULT ((0)) FOR [ContainerQuantity]
	ALTER TABLE NSW_Connote ADD CONSTRAINT [DF_NSW_Connote_AdditionalContainerQuantity]  DEFAULT ((0)) FOR [AdditionalContainerQuantity]
end
GO

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'NSW_Connote_DeleteTrigger'))
	DROP TRIGGER NSW_Connote_DeleteTrigger
GO

CREATE TRIGGER NSW_Connote_DeleteTrigger ON NSW_Connote
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ConsKey INT
	SELECT @ConsKey=ConsKey FROM deleted
	
	if exists(select 1 from NSW_ContainerStatus where ConsKey=@ConsKey)
		delete from NSW_ContainerStatus where ConsKey=@ConsKey
	
	if exists(select 1 from NSW_Container where ConsKey=@ConsKey)
		delete from NSW_Container where ConsKey=@ConsKey
			
	SET NOCOUNT OFF
END
Go

if not exists(select * from syscolumns where name = 'AdditionalContainerQuantity'
	and id = (select id from sysobjects where id = object_id(N'NSW_Connote') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Connote Add [AdditionalContainerQuantity] [int] NULL
	ALTER TABLE NSW_Connote ADD CONSTRAINT [DF_NSW_Connote_AdditionalContainerQuantity]  DEFAULT ((0)) FOR [AdditionalContainerQuantity]
end
go

-- drop table NSW_Container
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_Container') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_Container(
		[ConsKey] [int] NOT NULL,
		[LineNo] [int] NOT NULL,
		[ContainerNo] [varchar](7) NULL,
		[Barcode] [varchar](40) NULL,
		[VenueName] [varchar](100) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
		[Deleted] [datetime] NULL,
		[DeletedBy] [varchar](100) NULL,
		[DeletedReason] [varchar](1000) NULL,
		[Added] [datetime] NULL,
		[AddedBy] [varchar](100) NULL,
	 CONSTRAINT [NSW_Container_PK] PRIMARY KEY CLUSTERED 
	(
		[ConsKey] ASC,
		[LineNo] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if not exists(select * from syscolumns where name = 'Added'
	and id = (select id from sysobjects where id = object_id(N'NSW_Container') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Container Add [Added] [datetime] NULL
end
go

if not exists(select * from syscolumns where name = 'AddedBy'
	and id = (select id from sysobjects where id = object_id(N'NSW_Container') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_Container Add [AddedBy] [varchar](100) NULL
end
go

-- drop table NSW_ContainerStatus
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_ContainerStatus') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE NSW_ContainerStatus(
		[ConsKey] [int] NOT NULL,
		[LineNo] [int] NOT NULL,
		[StatusDate] [datetime] NOT NULL,
		[StatusTime] [varchar](10) NOT NULL,
		[StatusNotes] [varchar](500) NULL,
		[MovementDirection] [varchar](500) NULL,
		[CountCentreCode] [varchar](5) NULL, 
		[ConsNo] [varchar](30) NULL,
		[ContainerNo] [varchar](7) NULL,
		[PrinterJobId] [varchar](4) NULL,
		[Barcode] [varchar](40) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [NSW_ContainerStatus_PK] PRIMARY KEY CLUSTERED 
	(
		[ConsKey] ASC,
		[LineNo] ASC,
		[StatusDate] ASC,
		[StatusTime] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if exists(select * from syscolumns where name = 'MovementDirection' and length = 20 
	and id = (select id from sysobjects where id = object_id(N'NSW_ContainerStatus') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_ContainerStatus Alter Column MovementDirection [varchar](500) NULL
end
go

if exists(select * from syscolumns where name = 'CountCentreCode' and length = 20 
	and id = (select id from sysobjects where id = object_id(N'NSW_ContainerStatus') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_ContainerStatus Alter Column [CountCentreCode] [varchar](5) NULL
end
go

if not exists(select * from syscolumns where name = 'StatusStage'
	and id = (select id from sysobjects where id = object_id(N'NSW_ContainerStatus') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table NSW_ContainerStatus Add [StatusStage] [decimal](4, 2) NULL
end
go

-- drop table NSW_LabelFormat
if not exists (select * from dbo.sysobjects where id = object_id(N'SM_ONLINE_LABEL') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE SM_ONLINE_LABEL(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LABEL_NAME] [varchar](50) NULL,
		[DESCRIPTION] [varchar](100) NULL,
		[LAYOUT] [varbinary](max) NULL,
		[STORED_PROCEDURE] [varchar](30) NULL,
		[CREATED_DATE] [datetime] NULL,
		[CREATED_BY] [varchar](100) NULL,
		[MODIFIED_DATE] [datetime] NULL,
		[MODIFIED_BY] [varchar](100) NULL,
		[ACTIVE] [char](1) NOT NULL,
	 CONSTRAINT [PK_SCAN_PRINT_LABEL] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	ALTER TABLE SM_ONLINE_LABEL ADD  CONSTRAINT [DF_SMOnlineLabel_Active]  DEFAULT ('Y') FOR [ACTIVE]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityLogin') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE SecurityLogin(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LoginId] [varchar](100) NOT NULL,
		[Password] [varchar](200) NOT NULL,
		[PasswordExpires] [datetime] NULL,
		[WebPageTheme] [varchar](20) NULL,
		
		[Salutation] [varchar](30) NULL,
		[FirstName] [varchar](30) NULL,
		[FamilyName] [varchar](30) NULL,
		[Phone] [varchar](20) NULL,
		[Mobile] [varchar](20) NULL,
		[Fax] [varchar](20) NULL,
		[Email] [varchar](255) NULL,
		[Comments] [varchar](1000) NULL,
		[CDID] [varchar](3) NULL,
		
		[Active] [char](1) NOT NULL,
		[Verified] [char](1) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityLogin] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityLogin ADD  CONSTRAINT [DF_SecurityLogin_Active]  DEFAULT ('N') FOR [Active]
	ALTER TABLE SecurityLogin ADD  CONSTRAINT [DF_SecurityLogin_Verified]  DEFAULT ('N') FOR [Verified]
end
GO

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'SecurityLogin_DeleteTrigger'))
	DROP TRIGGER SecurityLogin_DeleteTrigger
GO

CREATE TRIGGER SecurityLogin_DeleteTrigger ON SecurityLogin
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ID INT, @ContactId int, @LoginId varchar(100)
	SELECT @ID=ID, @LoginId=LoginId FROM deleted
		
	DELETE FROM SecurityLoginRole
	WHERE SecurityLogin=@ID
	
	delete from NSW_GridLayout
	where CreatedBy=@LoginId
				
	SET NOCOUNT OFF
END
Go

if not exists(select * from syscolumns where name = 'CDID'
	and id = (select id from sysobjects where id = object_id(N'SecurityLogin') 
		and OBJECTPROPERTY(id, N'IsUserTable')= 1))  
begin
	Alter Table SecurityLogin Add [CDID] [varchar](3) NULL
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityRole') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE SecurityRole(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityRole] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityRole ADD  CONSTRAINT [DF_SecurityRole_Active]  DEFAULT ('Y') FOR [Active]
end
GO

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'SecurityRole_DeleteTrigger'))
	DROP TRIGGER SecurityRole_DeleteTrigger
GO

CREATE TRIGGER SecurityRole_DeleteTrigger ON SecurityRole
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ID INT
	SELECT @ID=ID FROM deleted
	
	DELETE FROM SecurityRoleFunction
	WHERE SecurityRole=@ID
	
	DELETE FROM SecurityLoginRole
	WHERE SecurityRole=@ID
		
	SET NOCOUNT OFF
END
Go

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityLoginRole') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityLoginRole(
		[SecurityLogin] [int] NOT NULL,
		[SecurityRole] [int] NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityLoginRole] PRIMARY KEY CLUSTERED 
	(
		[SecurityLogin] ASC,
		[SecurityRole] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityFunctionCategory') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityFunctionCategory(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityFunctionCategory] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityFunctionCategory ADD  CONSTRAINT [DF_SecurityFunctionCategory_Active]  DEFAULT ('Y') FOR [Active]
end
GO

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'SecurityFunctionCategory_DeleteTrigger'))
	DROP TRIGGER SecurityFunctionCategory_DeleteTrigger
GO

CREATE TRIGGER SecurityFunctionCategory_DeleteTrigger ON SecurityFunctionCategory
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ID INT
	SELECT @ID=ID FROM deleted
	
	DELETE FROM SecurityFunction
	WHERE Category=@ID
		
	SET NOCOUNT OFF
END
Go

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityFunction') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityFunction(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[Category] [int] NOT NULL,
		[Active] [char](1) NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityFunction] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityFunction ADD  CONSTRAINT [DF_SecurityFunction_Active]  DEFAULT ('Y') FOR [Active]
end
go

IF  EXISTS (SELECT * FROM sys.triggers 
	WHERE object_id = OBJECT_ID(N'SecurityFunction_DeleteTrigger'))
	DROP TRIGGER SecurityFunction_DeleteTrigger
GO

CREATE TRIGGER SecurityFunction_DeleteTrigger ON SecurityFunction
FOR DELETE 
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ID INT
	SELECT @ID=ID FROM deleted
	
	DELETE FROM SecurityComponent
	WHERE SecurityFunction=@ID
	
	DELETE FROM SecurityRoleFunction
	WHERE SecurityFunction=@ID
	
	SET NOCOUNT OFF
END
Go

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityRoleFunction') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityRoleFunction(
		[SecurityRole] [int] NOT NULL,
		[SecurityFunction] [int] NOT NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityRoleFunction] PRIMARY KEY CLUSTERED 
	(
		[SecurityRole] ASC,
		[SecurityFunction] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityComponent') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
	CREATE TABLE SecurityComponent(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[SecurityFunction] [int] NOT NULL,
		[FormName] [varchar](100) NOT NULL,
		[FrameName] [varchar](100) NULL,
		[ComponentName] [varchar](100) NULL,
		[Enabled] [char](1) NOT NULL,
		[Visible] [char](1) NOT NULL,
		[Caption] [varchar](255) NULL,
		[Url] [varchar](200) NULL,
		[UrlParam] [varchar](100) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_SecurityComponent] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityComponent ADD  CONSTRAINT [DF_SecurityComponent_Enabled]  DEFAULT ('Y') FOR [Enabled]
	ALTER TABLE SecurityComponent ADD  CONSTRAINT [DF_SecurityComponent_Visible]  DEFAULT ('Y') FOR [Visible]
end
GO


if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetConnoteCatalogue') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetConnoteCatalogue
GO
-- exec NSW_GetConnoteCatalogue 
-- exec NSW_GetConnoteCatalogue '015'
CREATE PROCEDURE NSW_GetConnoteCatalogue
	@CDID varchar(3)=null 
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [int] NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[CountCentreCode] [varchar](5) NULL
	)
	INSERT INTO @ContainerStatusLatestTable
	SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
	(     
	SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
		NSW_ContainerStatus.CountCentreCode,
		ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
		ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
	FROM NSW_Container
		LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
			AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
	) AS Q
	WHERE rn=1
		
	SELECT T.ConsKey,T.[LineNo], C.EventId, RIGHT(C.CDID, 2) [CDID], C.ReturningOfficeName, C.ReturningOfficeName+' ('+RIGHT(C.CDID, 2)+')' [ReturningOffice],
		C.LGAId, C.LGAName+' ('+LGAId+')' [LGA], C.WardCode, C.WardName+' ('+WardCode+')' [Ward],
		RIGHT(C.VenueCode,4) [VenueCode], C.VenueName, C.VenueName+' ('+RIGHT(C.VenueCode,4)+')' [Venue],
		C.VenueTypeCode, C.VenueTypeName, C.VenueTypeName+' ('+VenueTypeCode+')' [VenueType],
		C.ContestCode, C.ContestName, C.ContestName+' ('+ContestCode+')' [Contest],
		C.ContainerCode, C.ContainerName, C.ContainerName+' ('+ContainerCode+')' [ContainerType],
		C.ContainerQuantity, T.Barcode, T.Deleted, S.StatusNotes, S.StatusDate,S.StatusTime, S.StatusStage, 
		case 
			when CC.Code is not null then CC.Name+' ('+CC.Code+')'
			else null
		end [CountCentre] 
	from NSW_Container T
		INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
		INNER JOIN @ContainerStatusLatestTable S ON S.ConsKey=T.ConsKey AND S.[LineNo]=T.[LineNo] AND S.Barcode=T.Barcode
		left join NSW_CountCentre CC ON CC.Code=S.CountCentreCode
	WHERE C.CDID=@CDID or @CDID is null
	ORDER BY EventId, CDID, VenueName, ContestName, ContainerCode,Barcode
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetConnoteContainer') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetConnoteContainer
GO
-- exec NSW_GetConnoteContainer 36670,'02023002728020101002003'
-- exec NSW_GetConnoteContainer 7
-- exec NSW_GetConnoteContainer 7,'010554020301001002'
-- exec NSW_GetConnoteContainer 7,'010554020301002002'
CREATE PROCEDURE NSW_GetConnoteContainer 
	@Conskey int=null, @Barcode varchar(40)=null
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [int] NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL
	)
	INSERT INTO @ContainerStatusLatestTable
	SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes],StatusStage FROM
	(     
	SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
		ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], NSW_ContainerStatus.StatusStage,
		ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
	FROM NSW_Container
		LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
			AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo] --where NSW_ContainerStatus.StatusStage is not null
	) AS Q
	WHERE rn=1
	
	--select * from @ContainerStatusLatestTable where StatusStage is not null
		
	SELECT T.ConsKey,T.[LineNo], RIGHT(C.CDID, 2) [CDID], C.ReturningOfficeName, C.ReturningOfficeName+' ('+RIGHT(C.CDID, 2)+')' [ReturningOffice],
		ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
		ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
		RIGHT(C.VenueCode,4) [VenueCode], C.VenueName, C.VenueName+' ('+RIGHT(C.VenueCode,4)+')' [Venue],
		C.VenueTypeCode, C.VenueTypeName, C.VenueTypeName+' ('+VenueTypeCode+')' [VenueType],
		C.ContestCode, C.ContestName, C.ContestName+' ('+ContestCode+')' [Contest],
		C.ContainerCode, C.ContainerName, C.ContainerName+' ('+ContainerCode+')' [ContainerType],
		C.ContainerQuantity, T.Barcode, T.Deleted, T.Added, S.StatusNotes, S.StatusDate,S.StatusTime, s.StatusStage 
	from NSW_Container T
		INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
		INNER JOIN @ContainerStatusLatestTable S ON S.ConsKey=T.ConsKey AND S.[LineNo]=T.[LineNo] AND S.Barcode=T.Barcode
	where (T.ConsKey=@Conskey or @Conskey is null) and (T.Barcode=@Barcode or @Barcode is null)
		--where StatusStage is not null
		--WHERE T.ConsKey=7
	ORDER BY CDID, VenueName, ContestName, ContainerCode,Barcode
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_GridLayout') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_GridLayout(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[PageName] [varchar](200) NULL,
		[GridName] [varchar](200) NULL,
		[LayoutName] [varchar](200) NULL,
		[Layout] [varchar](max) NULL,
		[RowsPerPage] [int] NULL,
		[DefaultLayout] [char](1) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	 CONSTRAINT [PK_NSW_GridLayout] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	ALTER TABLE NSW_GridLayout ADD CONSTRAINT [DF_NSW_GridLayout_DefaultLayout]  DEFAULT ('N') FOR [DefaultLayout]	
end
GO

IF EXISTS (select * from dbo.sysobjects
		where id = object_id(N'ufn_DelimiterStringToTable')
		and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION ufn_DelimiterStringToTable
GO

CREATE FUNCTION ufn_DelimiterStringToTable ( 
	@StringInput VARCHAR(8000), @Delimiter char(1) )
	RETURNS @OutputTable TABLE ( [String] VARCHAR(100))
AS
BEGIN
    DECLARE @String VARCHAR(100)

    WHILE @StringInput is not null and LEN(@StringInput) > 0
    BEGIN
        SET @String = LEFT(@StringInput, ISNULL(NULLIF(CHARINDEX(@Delimiter, @StringInput) - 1, -1), LEN(@StringInput)))
        SET @StringInput = SUBSTRING(@StringInput, ISNULL(NULLIF(CHARINDEX(@Delimiter, @StringInput), 0), LEN(@StringInput)) + 1, LEN(@StringInput))

        INSERT INTO @OutputTable ( [String] )
        VALUES ( @String )
    END
    RETURN
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'GetDefaultConnoteLabels') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE GetDefaultConnoteLabels 
GO
-- exec GetDefaultConnoteLabels '10086009756030301003003|10086009756030301002003'
-- exec GetDefaultConnoteLabels '010554020201001001|010554020301002002'
-- EXEC GetDefaultConnoteLabels null, 7
-- EXEC GetDefaultConnoteLabels 
CREATE PROCEDURE GetDefaultConnoteLabels 
	@BarcodeList varchar(max)=null, @Conskey int=null
as 
begin
	--declare @BarcodeList varchar(max)=null, @Conskey int=null
	
	--set @BarcodeList='010554020201001001|010554020301002002'
	--set @BarcodeList='010554020201002002|010554020301002002'
	--set @BarcodeList='010554020201002002'
	--set @Conskey=7
	declare @ConnoteLabelTable table
	(
		-- CONNOTE
		[NSW_Connote.ConsKey] [int] NULL,
		[NSW_Connote.ConsNo] [varchar](30) NULL,
		[NSW_Connote.CDID] [varchar](3) NULL,
		[NSW_Connote.ReturningOfficeName] [varchar](100) NULL,
		[NSW_Connote.LGAId] [varchar](3) NULL,
		[NSW_Connote.LGACode] [varchar](40) NULL,
		[NSW_Connote.LGAName] [varchar](100) NULL,
		[NSW_Connote.WardCode] [varchar](2) NULL,
		[NSW_Connote.WardName] [varchar](100) NULL,
		[NSW_Connote.VenueCode] [varchar](5) NULL,
		[NSW_Connote.VenueName] [varchar](100) NULL,
		[NSW_Connote.VenueLongName] [varchar](200) NULL,
		[NSW_Connote.VenueAddress] [varchar](200) NULL,
		[NSW_Connote.VenueTypeCode] [varchar](2) NULL,
		[NSW_Connote.VenueTypeName] [varchar](100) NULL,
		[NSW_Connote.ContestCode] [varchar](2) NULL,
		[NSW_Connote.ContestName] [varchar](100) NULL,
		[NSW_Connote.ContainerCode] [varchar](2) NULL,
		[NSW_Connote.ContainerName] [varchar](100) NULL,
		[NSW_Connote.ContainerQuantity] [int] NULL,
		[NSW_Connote.AdditionalContainerQuantity] [int] NULL,
		[NSW_Connote.PrinterJobId] [varchar](4) NULL,
		[NSW_Connote.Created] [datetime] NULL,
		[NSW_Connote.CreatedBy] [varchar](100) NULL,
		[NSW_Connote.Modified] [datetime] NULL,
		[NSW_Connote.ModifiedBy] [varchar](100) NULL,
		[NSW_Connote.Deleted] [datetime] NULL,
		[NSW_Connote.DeletedBy] [varchar](100) NULL,
		[NSW_Connote.DeletedReason] [varchar](1000) NULL,
		-- CONNOTE_CARTONS
		[NSW_Container.ConsKey] [int] NULL,
		[NSW_Container.LineNo] [int] NULL,
		[NSW_Container.ContainerNo] [varchar](7) NULL,
		[NSW_Container.Barcode] [varchar](100) NULL,
		[NSW_Container.VenueName] [varchar](40) NULL,
		[NSW_Container.Created] [datetime] NULL,
		[NSW_Container.CreatedBy] [varchar](100) NULL,
		[NSW_Container.Modified] [datetime] NULL,
		[NSW_Container.ModifiedBy] [varchar](100) NULL,
		[NSW_Container.Deleted] [datetime] NULL,
		[NSW_Container.DeletedBy] [varchar](100) NULL,
		[NSW_Container.DeletedReason] [varchar](1000) NULL,
		[NSW_Container.Added] [datetime] NULL,
		[NSW_Container.AddedBy] [varchar](100) NULL,
		[NSW_Container.PackageRank] [varchar](50) NULL
	)
	declare @ConnoteTable table
	(
		[NSW_Connote.ConsKey] [int] NULL,
		[NSW_Connote.ConsNo] [varchar](30) NULL,
		[NSW_Connote.CDID] [varchar](3) NULL,
		[NSW_Connote.ReturningOfficeName] [varchar](100) NULL,
		[NSW_Connote.LGAId] [varchar](3) NULL,
		[NSW_Connote.LGACode] [varchar](40) NULL,
		[NSW_Connote.LGAName] [varchar](100) NULL,
		[NSW_Connote.WardCode] [varchar](2) NULL,
		[NSW_Connote.WardName] [varchar](100) NULL,
		[NSW_Connote.VenueCode] [varchar](5) NULL,
		[NSW_Connote.VenueName] [varchar](100) NULL,
		[NSW_Connote.VenueLongName] [varchar](200) NULL,
		[NSW_Connote.VenueAddress] [varchar](200) NULL,
		[NSW_Connote.VenueTypeCode] [varchar](2) NULL,
		[NSW_Connote.VenueTypeName] [varchar](100) NULL,
		[NSW_Connote.ContestCode] [varchar](2) NULL,
		[NSW_Connote.ContestName] [varchar](100) NULL,
		[NSW_Connote.ContainerCode] [varchar](2) NULL,
		[NSW_Connote.ContainerName] [varchar](100) NULL,
		[NSW_Connote.ContainerQuantity] [int] NULL,
		[NSW_Connote.AdditionalContainerQuantity] [int] NULL,
		[NSW_Connote.PrinterJobId] [varchar](4) NULL,
		[NSW_Connote.Created] [datetime] NULL,
		[NSW_Connote.CreatedBy] [varchar](100) NULL,
		[NSW_Connote.Modified] [datetime] NULL,
		[NSW_Connote.ModifiedBy] [varchar](100) NULL,
		[NSW_Connote.Deleted] [datetime] NULL,
		[NSW_Connote.DeletedBy] [varchar](100) NULL,
		[NSW_Connote.DeletedReason] [varchar](1000) NULL
	)
	
	declare @ConnoteCartonTable table
	(
		[NSW_Container.ConsKey] [int] NULL,
		[NSW_Container.LineNo] [int] NULL,
		[NSW_Container.ContainerNo] [varchar](7) NULL,
		[NSW_Container.Barcode] [varchar](40) NULL,
		[NSW_Container.VenueName] [varchar](40) NULL,
		[NSW_Container.Created] [datetime] NULL,
		[NSW_Container.CreatedBy] [varchar](100) NULL,
		[NSW_Container.Modified] [datetime] NULL,
		[NSW_Container.ModifiedBy] [varchar](100) NULL,
		[NSW_Container.Deleted] [datetime] NULL,
		[NSW_Container.DeletedBy] [varchar](100) NULL,
		[NSW_Container.DeletedReason] [varchar](1000) NULL,
		[NSW_Container.Added] [datetime] NULL,
		[NSW_Container.AddedBy] [varchar](100) NULL,
		[NSW_Container.PackageRank] [varchar](50) NULL
	)
	
	declare @ConsKeyTable table (ConsKey int)
	
	declare @BarcodeTable table (Barcode varchar(40))
	
	if (@BarcodeList is null or LEN(@BarcodeList)=0) and (@Conskey is null or (@Conskey is not null and @Conskey<=0)) 
	begin
		declare @XSDSchema xml
		set @XSDSchema = (select top 0 * from @ConnoteLabelTable as ConnoteLabelTable
		for XML auto, BINARY BASE64, elements, xmlschema('ConnoteLabelTable'))
		select @XSDSchema
		return
	end
	
	if @BarcodeList is not null and LEN(@BarcodeList)>0
	begin
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList,'|')
	end
	else
	if @Conskey is not null and @Conskey>0
	begin
		insert into @BarcodeTable
		select distinct Barcode from NSW_Container
		where ConsKey=@Conskey and Deleted is null
	end
	--select * from @BarcodeTable
	insert into @ConsKeyTable
	select distinct C.ConsKey from NSW_Container C
		inner join @BarcodeTable B ON B.Barcode=C.Barcode
	--select * from @ConsKeyTable	
	if exists(select 1 from @ConsKeyTable)
	begin		
		insert into @ConnoteCartonTable
		select C.ConsKey,C.[LineNo],C.ContainerNo,C.Barcode,C.VenueName,
			C.Created,C.CreatedBy,C.Modified,C.ModifiedBy,
			C.Deleted,C.DeletedBy,C.DeletedReason,C.Added,C.AddedBy,NULL 
		from NSW_Container C
			inner join @ConsKeyTable CK ON CK.ConsKey=C.ConsKey
			
		--select * from @ConnoteCartonTable	
		--select * from @ConsKeyTable
		
		insert into @ConnoteTable
		select C.ConsKey,C.ConsNo,C.CDID,C.ReturningOfficeName,C.LGAId,C.LGACode,C.LGAName,C.WardCode,C.WardName,
			C.VenueCode,C.VenueName,C.VenueLongName,C.VenueAddress,C.VenueTypeCode,C.VenueTypeName,
			C.ContestCode,C.ContestName,C.ContainerCode,C.ContainerName,C.ContainerQuantity,ISNULL(C.AdditionalContainerQuantity,0),C.PrinterJobId,
			C.Created,C.CreatedBy,C.Modified,C.ModifiedBy,C.Deleted,C.DeletedBy,C.DeletedReason 
		from NSW_Connote C 
			INNER JOIN @ConsKeyTable CK ON CK.ConsKey=C.ConsKey		
	end
	else
	begin
		select * from @ConnoteLabelTable
		return
	end
	
	--select * from @ConnoteCartonTable	
	--select * from @ConnoteTable
		
	DECLARE @Rank int=0, @Cons_Key int, @LineNo int, @CartonNo varchar(7),
		@ItemQty int, @ItemQtyAdded int, @ItemQtyRemoved int, @RankTotal varchar(50)
	DECLARE KQ CURSOR FOR
	SELECT DISTINCT [NSW_Connote.ConsKey], [NSW_Connote.ContainerQuantity], [NSW_Connote.AdditionalContainerQuantity]
	FROM @ConnoteTable
	OPEN KQ
	FETCH KQ INTO @Cons_Key, @ItemQty, @ItemQtyAdded	
	WHILE @@FETCH_STATUS = 0                                
	BEGIN
		SELECT @ItemQtyRemoved=COUNT([LineNo]) FROM NSW_Container
		WHERE ConsKey=@Cons_Key AND Deleted IS NOT NULL
		
		SET @RankTotal=' of '+CAST(@ItemQty AS VARCHAR)
		IF @ItemQtyAdded>0 OR @ItemQtyRemoved>0
		BEGIN
			IF @ItemQtyAdded>@ItemQtyRemoved
				SET @RankTotal=' of '+CAST(@ItemQty AS VARCHAR)+'+'+CAST((@ItemQtyAdded-@ItemQtyRemoved) AS VARCHAR)
			ELSE
				SET @RankTotal=' of '+CAST(@ItemQty AS VARCHAR)+'-'+CAST((@ItemQtyRemoved-@ItemQtyAdded) AS VARCHAR)
		END
		
		SET @Rank=0  
		DECLARE PQ CURSOR FOR
		SELECT [NSW_Container.LineNo],[NSW_Container.ContainerNo] FROM @ConnoteCartonTable
		WHERE [NSW_Container.ConsKey]=@Cons_Key 
		--ORDER BY [NSW_Container.LineNo],[NSW_Container.ContainerNo]
		ORDER BY [NSW_Container.ContainerNo]
		OPEN PQ
		FETCH PQ INTO @LineNo, @CartonNo	
		WHILE @@FETCH_STATUS = 0                                
		BEGIN
			SET @Rank=@Rank+1
			UPDATE @ConnoteCartonTable
			SET [NSW_Container.PackageRank]=CAST(@Rank AS VARCHAR)+@RankTotal
			WHERE [NSW_Container.ConsKey]=@Cons_Key 
				AND [NSW_Container.LineNo]=@LineNo
				AND [NSW_Container.ContainerNo]=@CartonNo
			
			FETCH PQ INTO @LineNo, @CartonNo       
		END                                                       
		CLOSE PQ                                                   
		DEALLOCATE PQ
		
		FETCH KQ INTO @Cons_Key, @ItemQty, @ItemQtyAdded	      
	END                                                       
	CLOSE KQ                                                   
	DEALLOCATE KQ
	
	--select * from @ConnoteCartonTable	
	
	insert into @ConnoteLabelTable
	select Connote.*, Carton.* 
	from @ConnoteCartonTable Carton
		INNER JOIN @BarcodeTable B ON B.Barcode=Carton.[NSW_Container.Barcode]	
		inner join @ConnoteTable Connote ON Connote.[NSW_Connote.ConsKey] = Carton.[NSW_Container.ConsKey]
	
	update @ConnoteLabelTable
		set [NSW_Connote.VenueName]=[NSW_Container.VenueName]
	where [NSW_Container.VenueName] is not null
		
	select * from @ConnoteLabelTable
end
go

-- drop table SecurityDevice
if not exists (select * from dbo.sysobjects where id = object_id(N'SecurityDevice') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE SecurityDevice(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[DeviceId] [varchar](50) NOT NULL,
		[Password] [varchar](200) NULL,
		[Name] [varchar](100) NULL,
		[Active] [char](1) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [SecurityDevice_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE SecurityDevice ADD CONSTRAINT [DF_SecurityDevice_Active] DEFAULT ('Y') FOR [Active]
end
GO


if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetLGAContainerBarcodeData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetLGAContainerBarcodeData
GO
-- exec NSW_GetLGAContainerBarcodeData '002',null,null,null,null,null,null,null,5.20
-- exec NSW_GetLGAContainerBarcodeData '003','018',null,'00709',null,'02',null,null,1.00
CREATE PROCEDURE NSW_GetLGAContainerBarcodeData 
	@CDID varchar(3)=null, @LGACode varchar(3)=null, @WardCode varchar(2)=null,
	@VenueCode varchar(5)=null, @VenueTypeCode varchar(2)=null,
	@ContestCode varchar(2)=null, @CCCode varchar(5)=null, @BarcodeList varchar(max)=null, 
	@StageStatusId varchar(4)=null
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [int] NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[CountCentreCode] [varchar](5) NULL
	)
	/*
	StageId		Description
	1.00        Printer Scan-out to Returning Office
	1.10        Printer Scan-out to Sydney Town Hall
	1.11        Sydney Town Hall Scan-in from Printer
	1.12        Sydney Town Hall Scan-out to Returning Office
	2.00        Returning Office Scan-in from Printer
	2.10        Returning Office Scan-in from Sydney Town Hall
	3.00        Returning Office Scan-out to Venue
	4.00        Returning Office Scan-in from Venue
	5.00        Returning Office Scan-out to Warehouse
	5.10        Returning Office Scan-out to Sydney Town Hall
	5.11        Sydney Town Hall Scan-in from Returning Office
	5.12        Sydney Town Hall Scan-out to Warehouse
	5.20        Returning Office Scan-out to Count Centre
	5.21        Count Centre Scan-in from Returning Office
	5.22        Count Centre Scan-out to Warehouse
	6.00        Warehouse Scan-in from Returning Office
	6.10        Warehouse Scan-in from Sydney Town Hall
	6.20        Warehouse Scan-in from Count Centre

	*/
			
	if @StageStatusId is not null
	begin
		--print '@StageStatusId defined'
		
		if @StageStatusId<=1.1 -- (1.00 or 1.10)
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes],[StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and [StatusStage]=0
		end
		else
		if @StageStatusId=1.11 -- Sydney Town Hall Scan-in from Printer
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=1.10 or [StatusStage]=1.11)
		end
		else
		if @StageStatusId=1.12 -- Sydney Town Hall Scan-out to Returning Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=1.11 or [StatusStage]=1.12)
		end
		else
		if @StageStatusId=2.00 -- Returning Office Scan-in from Printer
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=0 or [StatusStage]=1.00 or [StatusStage]=2.00)
		end
		else
		if @StageStatusId=2.10 -- Returning Office Scan-in from Sydney Town Hall
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=1.12 or [StatusStage]=2.10)
		end
		else
		if @StageStatusId=3.00 -- Returning Office Scan-out to Venue
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=2.00 or [StatusStage]=2.10 or [StatusStage]=3.00)
		end
		else
		if @StageStatusId=4.00 -- Returning Office Scan-in from Venue
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=3.00 or [StatusStage]=4.00)
		end
		else
		if @StageStatusId=5.00 -- Returning Office Scan-out to Warehouse
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage], NSW_ContainerStatus.CountCentreCode,
				NSW_Connote.VenueTypeCode,NSW_Connote.VenueCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=4.00 or [StatusStage]=5.00 or 
				([StatusStage]=2.00 and [VenueTypeCode] in ('03','04') and not([VenueCode] between '09980' and '09989')))
		end
		else
		if @StageStatusId=5.10 -- Returning Office Scan-out to Sydney Town Hall
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				NSW_Connote.VenueTypeCode,NSW_Connote.VenueCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=4.00 or [StatusStage]=5.10 or 
				([StatusStage]=2.00 and [VenueTypeCode] in ('03','04') and not([VenueCode] between '09980' and '09989')))
		end
		else
		if @StageStatusId=5.11 -- Sydney Town Hall Scan-in from Returning Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.10 or [StatusStage]=5.11)
		end
		else
		if @StageStatusId=5.12 -- Sydney Town Hall Scan-out to Warehouse
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.11 or [StatusStage]=5.12)
		end
		else
		if @StageStatusId=5.20 -- Returning Office Scan-out to Count Centre
		begin
			-- ready to scan out
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				NSW_Connote.VenueTypeCode,NSW_Connote.VenueCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and [CountCentreCode] is null and ([StatusStage]=4.00 or 
				([StatusStage]=2.00 and [VenueTypeCode] in ('03','04') and not([VenueCode] between '09980' and '09989')))
			-- already scanned out to cc
			if @CCCode is not null and LEN(@CCCode)>0
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					NSW_Connote.VenueTypeCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.20 or [StatusStage]=5.21 or [StatusStage]=5.22 or [StatusStage]=6.20)
					and [CountCentreCode]=@CCCode
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					NSW_Connote.VenueTypeCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					inner join NSW_Connote ON NSW_Connote.ConsKey=NSW_Container.ConsKey
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.20 or [StatusStage]=5.21 or [StatusStage]=5.22 or [StatusStage]=6.20)
					and [CountCentreCode] is not null
		end
		else
		if @StageStatusId=5.21 -- Count Centre Scan-in from Returning Office
		begin
			if @CCCode is not null and LEN(@CCCode)>0
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.20 or [StatusStage]=5.21) and [CountCentreCode]=@CCCode
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.20 or [StatusStage]=5.21) and [CountCentreCode] is not null
		end
		else
		if @StageStatusId=5.22 -- Count Centre Scan-out to Warehouse
		begin
			if @CCCode is not null and LEN(@CCCode)>0
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.21 or [StatusStage]=5.22) and [CountCentreCode]=@CCCode
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.21 or [StatusStage]=5.22) and [CountCentreCode] is not null
		end
		else
		if @StageStatusId=6.00 -- Warehouse Scan-in from Returning Office
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.00 or [StatusStage]=6.00)
		end
		else
		if @StageStatusId=6.10 -- Warehouse Scan-in from Sydney Town Hall
		begin
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1 and ([StatusStage]=5.12 or [StatusStage]=6.10)
		end
		else
		if @StageStatusId=6.20 -- Warehouse Scan-in from Sydney Town Hall
		begin
			if @CCCode is not null and LEN(@CCCode)>0
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.22 or [StatusStage]=6.20) and [CountCentreCode]=@CCCode
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and ([StatusStage]=5.22 or [StatusStage]=6.20) and [CountCentreCode] is not null
		end
	end
	else
	begin
		--print '@StageStatusId undefined'
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage],CountCentreCode FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],NSW_ContainerStatus.CountCentreCode,
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
		) AS Q
		WHERE rn=1
	end
	
	declare @BarcodeTable table (Barcode varchar(40))
	if @BarcodeList is not null and LEN(@BarcodeList)>0
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList,'|')
		
	if exists(select 1 from @BarcodeTable)
	begin
		SELECT T.Barcode, RIGHT(CDID,2) [CDID], ReturningOfficeName, 
			ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
			RIGHT(VenueCode,4) [VenueCode], 
			case
				when T.VenueName is not null then T.VenueName
				else C.VenueName
			end [VenueName], 
			VenueTypeCode, VenueTypeName, ContestCode, ContestName, ContainerCode, ContainerName, StatusStage, StatusDate,CountCentreCode
		FROM NSW_Container T
			INNER JOIN @BarcodeTable B ON B.Barcode=T.Barcode
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
		WHERE T.Deleted IS NULL AND (CDID=@CDID OR (@CDID IS NULL)) 
			AND (LGAId=@LGACode OR (@LGACode IS NULL)) 
			AND (WardCode=@WardCode OR (@WardCode IS NULL))
			AND (VenueCode=@VenueCode OR (@VenueCode IS NULL))
			AND (VenueTypeCode=@VenueTypeCode OR (@VenueTypeCode IS NULL))
			AND (ContestCode=@ContestCode OR (@ContestCode IS NULL))
			--AND (CountCentreCode=@CCCode OR (@CCCode IS NULL))
		order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
	end
	ELSE
		SELECT T.Barcode, RIGHT(CDID,2) [CDID], ReturningOfficeName, 
			ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
			RIGHT(VenueCode,4) [VenueCode], 
			case
				when T.VenueName is not null then T.VenueName
				else C.VenueName
			end [VenueName], 
			VenueTypeCode, VenueTypeName, ContestCode, ContestName, ContainerCode, ContainerName, StatusStage, StatusDate,CountCentreCode 
		FROM NSW_Container T
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
		WHERE T.Deleted IS NULL AND (CDID=@CDID OR (@CDID IS NULL)) 
			AND (LGAId=@LGACode OR (@LGACode IS NULL))
			AND (WardCode=@WardCode OR (@WardCode IS NULL))
			AND (VenueCode=@VenueCode OR (@VenueCode IS NULL)) 
			AND (VenueTypeCode=@VenueTypeCode OR (@VenueTypeCode IS NULL))
			AND (ContestCode=@ContestCode OR (@ContestCode IS NULL))
			--AND (CountCentreCode=@CCCode OR (@CCCode IS NULL))
		order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
END
GO


if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetROLGAForCountCentre') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetROLGAForCountCentre
GO
-- exec NSW_GetROLGAForCountCentre '09993'
CREATE PROCEDURE NSW_GetROLGAForCountCentre 
	@CCCode varchar(5)=null
AS 
BEGIN
	declare @BarcodeTable table 
	(
		Barcode varchar(40),
		[CDID] [varchar](3) NULL,
		[ReturningOfficeName] [varchar](100) NULL,
		[LGACode] [varchar](3) NULL,
		[LGAName] [varchar](100) NULL,
		[WardCode] [varchar](2) NULL,
		[WardName] [varchar](100) NULL,
		[VenueCode] [varchar](5) NULL,
		[VenueName] [varchar](100) NULL,
		[VenueTypeCode] [varchar](2) NULL,
		[VenueTypeName] [varchar](100) NULL,
		[ContestCode] [varchar](2) NULL,
		[ContestName] [varchar](100) NULL,
		[ContainerCode] [varchar](2) NULL,
		[ContainerName] [varchar](100) NULL,
		[StatusStage] [decimal](4, 2) NULL,
		[StatusDate] [datetime] NULL,
		[CountCentreCode] [varchar](5) NULL
	)

	insert into @BarcodeTable
	exec NSW_GetLGAContainerBarcodeData null,null,null,null,null,null,@CCCode 
	
	select distinct CDID [ROCode], ReturningOfficeName [ROName],LGACode,LGAName from @BarcodeTable B
		left join NSW_CountCentre c on c.Code=B.CountCentreCode
	where c.Code=@CCCode or @CCCode is null
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'NSW_GetContainerBarcodeData') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE NSW_GetContainerBarcodeData
GO
-- exec NSW_GetContainerBarcodeData '015',null,null,'E',4
-- exec NSW_GetContainerBarcodeData '015',null,null,'L'
-- exec NSW_GetContainerBarcodeData '015','00719','150719020101002002'
-- exec NSW_GetContainerBarcodeData '015','00719','151841020101002002'
-- exec NSW_GetContainerBarcodeData '015','01841','151841020101002002'
-- exec NSW_GetContainerBarcodeData '015','00895','010895020301002002' 
-- exec NSW_GetContainerBarcodeData NULL,NULL,'010895020201001001|010895020501001001'
-- exec NSW_GetContainerBarcodeData NULL,NULL,'010895020301002002'
CREATE PROCEDURE NSW_GetContainerBarcodeData 
	@CDID varchar(3)=null, @VenueCode varchar(5)=null, @BarcodeList varchar(max)=null, 
	@StageStatus char(1)=null, @StageStatusId int=null
AS 
BEGIN
	declare @ContainerStatusLatestTable table
	(
		[ConsKey] [int] NULL,
		[LineNo] [int] NULL,
		[Barcode] [varchar](40) NULL,
		[StatusDate] [datetime] NULL,
		[StatusTime] [varchar](10) NULL,
		[StatusNotes] [varchar](500) NULL,
		[StatusStage] [decimal](4, 2) NULL
	)
	if @StageStatus is not null
	begin	
		if @StageStatus='E'
		begin	
			if @StageStatusId is not null
			begin
				if @StageStatusId>0 and @StageStatusId<4
					INSERT INTO @ContainerStatusLatestTable
					SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
					(     
					SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo], NSW_Container.Barcode,NSW_Container.Added, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
						ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
						ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
					FROM NSW_Container
						LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
							AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					) AS Q
					WHERE rn=1 and ([StatusStage]=@StageStatusId-1 or ([Added] is not null and [StatusStage]=0))
			end
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_Container.Added, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and [StatusStage]<4
		end
		else
		if @StageStatus='L'
		begin
			if @StageStatusId is not null
			begin
				if @StageStatusId>4 and @StageStatusId<7
					INSERT INTO @ContainerStatusLatestTable
					SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
					(     
					SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
						ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
						ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
					FROM NSW_Container
						LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
							AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
					) AS Q
					WHERE rn=1 and [StatusStage]=@StageStatusId-1
			end
			else
				INSERT INTO @ContainerStatusLatestTable
				SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
				(     
				SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
					ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
					ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
				FROM NSW_Container
					LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
						AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
				) AS Q
				WHERE rn=1 and [StatusStage]>3
		end
		else
			INSERT INTO @ContainerStatusLatestTable
			SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
			(     
			SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
				ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
				ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
			FROM NSW_Container
				LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
					AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
			) AS Q
			WHERE rn=1
	end
	else
	begin
		INSERT INTO @ContainerStatusLatestTable
		SELECT ConsKey,[LineNo],Barcode,StatusDate,StatusTime, [StatusNotes], [StatusStage] FROM
		(     
		SELECT  NSW_Container.ConsKey, NSW_Container.[LineNo],NSW_Container.Barcode, NSW_ContainerStatus.StatusDate, NSW_ContainerStatus.StatusTime,
			ISNULL(NSW_ContainerStatus.StatusNotes, '') [StatusNotes], ISNULL(NSW_ContainerStatus.StatusStage, 0) [StatusStage],
			ROW_NUMBER() OVER (PARTITION BY NSW_Container.ConsKey,NSW_Container.[LineNo] ORDER BY NSW_ContainerStatus.StatusDate DESC) AS rn
		FROM NSW_Container
			LEFT JOIN NSW_ContainerStatus ON NSW_ContainerStatus.ConsKey=NSW_Container.ConsKey 
				AND NSW_ContainerStatus.[LineNo]=NSW_Container.[LineNo]
		) AS Q
		WHERE rn=1
	end
	
	declare @BarcodeTable table (Barcode varchar(40))
	if @BarcodeList is not null and LEN(@BarcodeList)>0
	begin
		insert into @BarcodeTable
		SELECT DISTINCT [String] FROM dbo.ufn_DelimiterStringToTable(@BarcodeList,'|')
		
		SELECT T.Barcode, RIGHT(CDID,2) [CDID], ReturningOfficeName, 
			ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
			RIGHT(VenueCode,4) [VenueCode], 
			case
				when T.VenueName is not null then T.VenueName
				else C.VenueName
			end [VenueName], 
            VenueTypeCode, VenueTypeName, ContestCode, ContestName, ContainerCode, ContainerName, StatusStage 
        FROM NSW_Container T
			INNER JOIN @BarcodeTable B ON B.Barcode=T.Barcode
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
        WHERE T.Deleted IS NULL AND (CDID=@CDID OR (@CDID IS NULL)) 
            AND (VenueCode=@VenueCode OR (@VenueCode IS NULL))
            order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
	end
	ELSE
		SELECT T.Barcode, RIGHT(CDID,2) [CDID], ReturningOfficeName, 
			ISNULL(LGAId,'000') [LGACode], ISNULL(LGAName,'Undefined') [LGAName],
			ISNULL(WardCode,'00') [WardCode], ISNULL(WardName,'Undivided') [WardName],
			RIGHT(VenueCode,4) [VenueCode], 
			case
				when T.VenueName is not null then T.VenueName
				else C.VenueName
			end [VenueName], 
            VenueTypeCode, VenueTypeName, ContestCode, ContestName, ContainerCode, ContainerName, StatusStage 
        FROM NSW_Container T
			inner join @ContainerStatusLatestTable S on s.ConsKey=T.ConsKey and S.[LineNo]=T.[LineNo]
			INNER JOIN NSW_Connote C ON C.ConsKey=T.ConsKey
        WHERE T.Deleted IS NULL AND (CDID=@CDID OR (@CDID IS NULL)) 
            AND (VenueCode=@VenueCode OR (@VenueCode IS NULL)) 
            order by ReturningOfficeName, VenueName, VenueTypeName, ContestName, Barcode
END
GO

-- drop table NSW_StatusStage
if not exists (select * from dbo.sysobjects where id = object_id(N'NSW_StatusStage') 
	and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin 
	CREATE TABLE NSW_StatusStage(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[StageId] decimal(4,2) NOT NULL,
		[Description] [varchar](200) NULL,
		[Created] [datetime] NULL,
		[CreatedBy] [varchar](100) NULL,
		[Modified] [datetime] NULL,
		[ModifiedBy] [varchar](100) NULL,
	CONSTRAINT [NSW_StatusStage_PK] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end
GO

if not exists(select 1 from NSW_StatusStage)
begin
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 1.00, 'Printer Scan-out to Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 1.10, 'Printer Scan-out to Sydney Town Hall', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 1.11, 'Sydney Town Hall Scan-in from Printer', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 1.12, 'Sydney Town Hall Scan-out to Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 2.00, 'Returning Office Scan-in from Printer', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 2.10, 'Returning Office Scan-in from Sydney Town Hall', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 3.00, 'Returning Office Scan-out to Venue', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 4.00, 'Returning Office Scan-in from Venue', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.00, 'Returning Office Scan-out to Warehouse', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.10, 'Returning Office Scan-out to Sydney Town Hall', GETDATE(), 'sm'
		
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.11, 'Sydney Town Hall Scan-in from Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.12, 'Sydney Town Hall Scan-out to Warehouse', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.20, 'Returning Office Scan-out to Count Centre', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.21, 'Count Centre Scan-in from Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 5.22, 'Count Centre Scan-out to Warehouse', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 6.00, 'Warehouse Scan-in from Returning Office', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 6.10, 'Warehouse Scan-in from Sydney Town Hall', GETDATE(), 'sm'
	
	insert into NSW_StatusStage (StageId, [Description], Created, CreatedBy)
	select 6.20, 'Warehouse Scan-in from Count Centre', GETDATE(), 'sm'
end
go

if not exists(select 1 from SecurityLogin)
begin
	declare @CreatedBy varchar(100)='cts',
		@LoginKey int, @RoleId int, @CategoryId int, @FuncId int
		
-- create admin login			
	insert into SecurityLogin 
		(LoginId,[Password],WebPageTheme,Active,Verified,Created,CreatedBy)
	select 
		'cts@compdata.com.au','4Z+DylOdolIkhX/3c2T8lA==','Glass','Y','Y',GETDATE(),@CreatedBy 
	set @LoginKey=CAST(scope_identity() AS int)
		
-- Security Role
	insert into SecurityRole
		(Name,Active,Created,CreatedBy)
	select 'Administrator','Y',GETDATE(),@CreatedBy
	set @RoleId=CAST(scope_identity() AS int) 
	
-- Security Login Role
	insert into SecurityLoginRole
		(SecurityLogin,SecurityRole,Created,CreatedBy)
	select @LoginKey,@RoleId,GETDATE(),@CreatedBy
		
-- security function and system setup
	insert into SecurityFunctionCategory
		(Name,Active,Created,CreatedBy)
	select 'Security Management and System Setup','Y',GETDATE(),@CreatedBy
	set @CategoryId=CAST(scope_identity() AS int)
	-- Access to Security Role Management
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Security Role Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmRoleCatalogue','Y','Y','Security Role Management','~/SecurityRoleCatalogue.aspx','loginId',GETDATE(),@CreatedBy			
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	-- Access to Security Function Management
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Security Function Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmFunctionCatalogue','Y','Y','Security Function Management','~/SecurityFunctionCategory.aspx','loginId',GETDATE(),@CreatedBy						
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	
	-- Access to security device Management
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Security Device Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
		
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmSecurityDevice','Y','Y','Security Device Catalogue','~/SecurityDeviceCatalogue.aspx','loginId',GETDATE(),@CreatedBy
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	
	-- Access to User Management
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to User Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
		
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmUserManagement','Y','Y','User Management','~/UserCatalogue.aspx','loginId',GETDATE(),@CreatedBy
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'rpAdditional','Y','Y',null,null,null,GETDATE(),@CreatedBy
	/*								
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'edtPasswordExpiry','Y','Y',null,null,null,GETDATE(),@CreatedBy
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'lblPasswordExpiry','Y','Y',null,null,null,GETDATE(),@CreatedBy	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'grdUserRole','Y','Y',null,null,null,GETDATE(),@CreatedBy
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'UserSetup.aspx',null,'cbActive','Y','Y',null,null,null,GETDATE(),@CreatedBy	
	*/							
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	
	-- system maintenance
	insert into SecurityFunctionCategory
		(Name,Active,Created,CreatedBy)
	select 'System Maintenance','Y',GETDATE(),@CreatedBy
	set @CategoryId=CAST(scope_identity() AS int)
	
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Maintenance (Returning Office, Venue, Venue Type, Contest, Container Type etc)',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemMaintenance','itmMaint','Y','Y','Maintenance','~/Maintenance.aspx','loginId',GETDATE(),@CreatedBy	
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
	
	-- system utilities
	/*
	insert into SecurityFunctionCategory
		(Name,Active,Created,CreatedBy)
	select 'System Utilities','Y',GETDATE(),@CreatedBy
	set @CategoryId=CAST(scope_identity() AS int)		
	
	insert into SecurityFunction
		(Name,Category,Active,Created,CreatedBy)
	select 'Access to Import Consignment Container',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)	
	
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemUtilities','itmImportContainer','Y','Y','Import Consignment Container','~/ImportContainer.aspx','loginId',GETDATE(),@CreatedBy	
			
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy	
	*/
end
go

if not exists(select 1 from SecurityFunction where Name='Access to Security Device Management')
begin
	declare @CreatedBy varchar(100), 
			@RoleId int, @CategoryId int, @FuncId int
	set @CreatedBy='cts'
	                                                         
	select @CategoryId=ID from SecurityFunctionCategory 
	where Name='Security Management and System Setup'
	
	select @RoleId=ID from SecurityRole
	where Name='Administrator'
	
	insert into SecurityFunction
	(Name,Category,Active,Created,CreatedBy)
	select 'Access to Security Device Management',@CategoryId,'Y',GETDATE(),@CreatedBy
	set @FuncId=CAST(scope_identity() AS int)
		
	insert into SecurityComponent
		(SecurityFunction,FormName,FrameName,ComponentName,[Enabled],Visible,Caption,Url,UrlParam,Created,CreatedBy)
	select @FuncId,'MasterPage','mItemSetup','itmSecurityDevice','Y','Y','Security Device Catalogue','~/SecurityDeviceCatalogue.aspx','loginId',GETDATE(),@CreatedBy
	
	insert into SecurityRoleFunction
		(SecurityRole,SecurityFunction,Created,CreatedBy)
	select @RoleId,@FuncId,GETDATE(),@CreatedBy
end
go

