﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Globalization;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class Consignment : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "Consignment.aspx";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EC_User"] == null)
                return;
            if (!IsPostBack)
            {
                Session.Remove("C_ConsKey");
                Session.Remove("C_ConnoteTable");
                Session.Remove("C_ContainerTable");
                Session.Remove("C_ContainerStatusTable");
                Session.Remove("C_GrdContainerStatusLoading");

                Session.Remove("C_ChildContainerTable");
                Session.Remove("C_GrdChildContainerLoading");

                Session.Remove("C_EMOTable");
                Session.Remove("C_ContestArea");
                Session.Remove("C_Ward");
                Session.Remove("C_Venue");
                Session.Remove("C_ProductType");
                Session.Remove("C_ContainerType");
                
                //Int32 _consKey = -1;
                object _consKey;
                
                ASPxButton btnAddContainer = pcContainer.FindControl("btnAddContainer") as ASPxButton;
                if (btnAddContainer != null)
                {
                    btnAddContainer.ClientSideEvents.Click = @"function(s, e) { 
                        if (!grdContainer.IsNewRowEditing() && !grdContainer.IsEditing())
                            grdContainer.AddNewRow();
                    }";
                    btnAddContainer.ClientVisible = false;// loginUser.IsAdmin;
                }
                /*
                 if (btnId == 'btnEditContainer' && !s.IsNewRowEditing() && !s.IsEditing()) {
                            s.StartEditRow(e.visibleIndex);
                        } else
                 */
                grdContainer.ClientSideEvents.CustomButtonClick =
                    @"function(s, e) { 
                        var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                        if (btnId == 'btnPrintContainer') {
                            if (cbPrintLabel.InCallback()) return; 
                            cbPrintLabel.PerformCallback('Print|' + id);
                        } else 
                        if (btnId == 'btnEditContainer') {
                            cpContainer.PerformCallback(id);
                        } else
                        if (btnId == 'btnUpdateContainer') {
                            var barCode = s.GetEditValue('Barcode'); var deletedBy = s.GetEditValue('DeletedBy');                            
                            var deleteReason = s.GetEditValue('DeletedReason'); var deleted = s.GetEditValue('Deleted');
                            var deletedTime;
                            if (deleted != null) {
                                deletedTime = deleted.toTimeString();
                                deleted = deleted.toDateString(); 
                            }
                            var activated = s.GetEditValue('Activated');
                            s.PerformCallback('Update|' + id + '|' + barCode + '|' + deletedBy + '|' + deleteReason + '|' + deleted + '|' + deletedTime + '|' + activated); 
                        } else
                        if (btnId == 'btnCancelContainer') {
                            s.CancelEdit();
                        }                        
                    }";

                cpContainer.ClientSideEvents.EndCallback =
                    @"function(s, e) {  
                        ppCtrlContainer.Show();    
                    } ";
                ASPxButton btnContainerCancel = ppCtrlContainer.FindControl("btnContainerCancel") as ASPxButton;
                if (btnContainerCancel != null)
                {
                    ECWebClass.SetButtonStyle(btnContainerCancel);
                    btnContainerCancel.ClientSideEvents.Click = @"function(s, e) {
                        ASPxClientEdit.ClearEditorsInContainerById('containerContainer');
                        ppCtrlContainer.Hide(); } ";
                }
                ASPxButton btnContainerOK = ppCtrlContainer.FindControl("btnContainerOK") as ASPxButton;
                if (btnContainerOK != null)
                {
                    ECWebClass.SetButtonStyle(btnContainerOK);
                    btnContainerOK.ClientSideEvents.Click = @"function(s, e) { 
                        var deleted = 'N'; if (cbCtnDelete.GetChecked()) deleted = 'Y'; var delReason = edtCtnDelReason.GetText(); 
                        if (deleted == 'Y' && delReason == '') { alert('Deactivate reason is required'); edtCtnDelReason.Focus(); return; } 
                        var lineNo = ppCtrlContainer.cpLineNo; var prod = cmbProduct.GetValue(); var venue = cmbVenue.GetValue(); var ctn = cmbContainerType.GetValue();
                        ASPxClientEdit.ClearEditorsInContainerById('containerContainer');
                        ppCtrlContainer.Hide(); 
                        cbContainer.PerformCallback(lineNo + '|' + prod + '|' + venue + '|' + ctn + '|' + deleted + '|' + delReason);
                    } ";
                }
               
                cbContainer.ClientSideEvents.CallbackComplete = @"function(s, e) {                     
                    grdContainer.Refresh();                   
                    if (e.result != null && e.result != '') {
                        if (e.result.indexOf('Error') > -1) alert(e.result);
                        else window.location = e.result;
                    }
                }";

                grdContainer.ClientSideEvents.FocusedRowChanged = @"function(s, e) { 
                    function OnGetRowValues(results) {
                        ppCtrlContainer.SetHeaderText('Container Barcode [' + results[1] + ']'); 
                        ppCtrlContainer.cpLineNo = results[0]; 
                        ppCtrlContainerStatus.SetHeaderText('Container Status for Barcode [' + results[1] + ']'); 
                        ppCtrlContainerStatus.cpLineNo = results[0];                       
                    }
                    var rowIdx = s.GetFocusedRowIndex();
                    s.GetRowValues(rowIdx, 'LineNo;Barcode', OnGetRowValues);
                }";

                ppCtrlContainer.JSProperties["cpLineNo"] = string.Empty;
                ppCtrlContainerStatus.JSProperties["cpLineNo"] = string.Empty;
                ppCtrlContainerStatus.JSProperties["cpStatusId"] = string.Empty;

                cbPrintLabel.ClientSideEvents.CallbackComplete =
                    @"function(s, e) {
                        if (e.result != null && e.result != '') {
                            if (e.result.indexOf('Error') > -1 || e.result.indexOf('Comfirmation') > -1) {
                                alert(e.result); 
                                return; 
                            }
                            var newWindow; var offSet = 20; var linkArray;
                            var winX = (document.all)?window.screenLeft:window.screenX;
                            var winY = (document.all)?window.screenTop:window.screenY;
                            winX += offSet; winY += offSet;
                            if (e.result.indexOf('|') > -1) {
                                var resultArray = e.result.split('|');                             
                                for (var i=0; i<resultArray.length; i++) {
                                    newWindow = window.open(resultArray[i], 'PrintLabel' + (i+1).toString(), 'width=450,height=850,resizable=yes');                                    
                                    newWindow.moveTo(winX,winY); 
                                    winX += offSet; winY += offSet;
                                }                           
                            } else {  
                                newWindow = window.open(e.result, 'PrintLabel', 'width=450,height=850,resizable=yes');
                                newWindow.moveTo(winX,winY);
                            }
                        }
                    }";
                cpContainerStatus.ClientSideEvents.EndCallback =
                    @"function(s, e) {  
                        ppCtrlContainerStatus.Show();
                        edtStatusDate.Focus();            
                    } ";
                
                ASPxButton btnContainerStatusCancel = ppCtrlContainerStatus.FindControl("btnContainerStatusCancel") as ASPxButton;
                if (btnContainerStatusCancel != null)
                {
                    ECWebClass.SetButtonStyle(btnContainerStatusCancel);
                    btnContainerStatusCancel.ClientSideEvents.Click = @"function(s, e) {
                        ASPxClientEdit.ClearEditorsInContainerById('containerStatusContainer');
                        ppCtrlContainerStatus.Hide(); } ";
                }
                ASPxButton btnContainerStatusOK = ppCtrlContainerStatus.FindControl("btnContainerStatusOK") as ASPxButton;
                if (btnContainerStatusOK != null)
                {
                    ECWebClass.SetButtonStyle(btnContainerStatusOK);
                    btnContainerStatusOK.ClientSideEvents.Click = @"function(s, e) {
                        var idx = cmbLocation.GetSelectedIndex(); if (idx < 1) { alert('Scan location is required'); cmbLocation.Focus(); return; }
                        idx = cmbStatus.GetSelectedIndex(); if (idx < 1) { alert('Status is required'); cmbStatus.Focus(); return; }
                        var deleted = 'N'; if (cbDelete.GetChecked()) deleted = 'Y'; var delReason = edtDelReason.GetText();
                        if (deleted == 'Y' && delReason == '') { alert('Deactivate reason is required'); edtDelReason.Focus(); return; }
                        var location = cmbLocation.GetValue(); var statusDate = edtStatusDate.GetText(); var status = cmbStatus.GetValue();
                        var move = edtMovement.GetText(); var lineNo = ppCtrlContainerStatus.cpLineNo; var statusId = ppCtrlContainerStatus.cpStatusId;
                        ASPxClientEdit.ClearEditorsInContainerById('containerStatusContainer');
                        ppCtrlContainerStatus.Hide(); cbContainerStatus.PerformCallback(lineNo + '|' + statusId + '|' + statusDate + '|' + location + '|' + status + '|' + move + '|' + deleted + '|' + delReason);
                    } ";
                }
                btnContainerStatusCancel.ClientSideEvents.Click = @"function(s, e) { 
                    ASPxClientEdit.ClearEditorsInContainerById('containerStatusContainer');                   
                    ppCtrlContainerStatus.Hide(); } ";
                cbContainerStatus.ClientSideEvents.CallbackComplete = @"function(s, e) { 
                    grdContainerStatus.Refresh();                   
                    if (e.result != null && e.result != '') alert(e.result); }";

                if (Request.QueryString["qs"] != null)
                {
                    string qsStr = Request.QueryString["qs"];
                    qsStr = ECSecurityClass.GetQSDecripted(qsStr);
                    string[] qs = qsStr.Split('=');
                    if (qs[0] == "conskey")
                    {
                        _consKey = qs[1]; 
                        //if (int.TryParse(qs[1], out _consKey))
                            Session["C_ConsKey"] = _consKey;
                    }
                    AssignConnoteData();
                }
                LoginUser loginUser = Session["EC_User"] as LoginUser;
                if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                {
                    lblEMO.Text = "Returning Office";
                    lblDistrict.Text = "LGA";
                    lblProductType.Text = "Contest Type";
                    lblContestArea.Text = "Ward";
                }

                pcConnote.ShowTabs = false;
                ECWebClass.SetPageControlHeaderStyle(pcContainer, false);
                ECWebClass.SetGridViewStyle(grdContainer);
                ECWebClass.SetPopupStyle(ppCtrlContainer);
                ECWebClass.SetPopupStyle(ppCtrlContainerStatus);
            }
            LoadContainerData();
        }
        private DataTable GetConnoteDataTable()
        {
            DataTable dtConnote = Session["C_ConnoteTable"] as DataTable;
            if (dtConnote == null)
            {
                dtConnote = ECWebClass.GetConnoteDataTable(Session["C_ConsKey"]);
                Session["xPC_ConnoteTable"] = dtConnote;
            }
            return dtConnote;
        }
        private DataTable GetContainerDataTable()
        {
            DataTable dtContainer = Session["C_ContainerTable"] as DataTable;
            if (dtContainer == null)
            {
                dtContainer = ECWebClass.GetConnoteContainerDataTable(Session["C_ConsKey"]);
                dtContainer.PrimaryKey = new DataColumn[] { dtContainer.Columns["ConsKey"], dtContainer.Columns["LineNo"] };
                Session["C_ContainerTable"] = dtContainer;
            }
            return dtContainer;
        }
        private void LoadContainerData()
        {
            DataTable dtContainerDataTable = GetContainerDataTable();
            grdContainer.DataSource = dtContainerDataTable;
            grdContainer.DataSourceID = string.Empty;
            grdContainer.DataBind();
        }

        private DataTable GetChildContainerDataTable()
        {
            DataTable dtULDChildContainer = Session["C_ChildContainerTable"] as DataTable;
            if (dtULDChildContainer == null)
            {
                dtULDChildContainer = ECWebClass.GetSGEULDChildContainerBarcodeDataTable(null, Session["C_ConsKey"]);
                dtULDChildContainer.PrimaryKey =
                    new DataColumn[] { dtULDChildContainer.Columns["ConsKey"], dtULDChildContainer.Columns["LineNo"] };
                Session["C_ChildContainerTable"] = dtULDChildContainer;
            }
            return dtULDChildContainer;
        }
        private void LoadChildContainerData(ASPxGridView grdView)
        {
            ASPxPageControl pcContainer = grdView.Parent.NamingContainer as ASPxPageControl;
            if (pcContainer != null)
                ECWebClass.SetPageControlHeaderStyle(pcContainer, false);

            DataTable dtChildContainer = GetChildContainerDataTable();
            DataView dvChildContainer = new DataView(dtChildContainer);
            dvChildContainer.RowFilter = string.Format("ULDLineNo = {0}", grdView.GetMasterRowKeyValue());
            grdView.DataSource = dvChildContainer;
            grdView.DataSourceID = string.Empty;
            grdView.DataBind();
            
            ECWebClass.SetGridViewStyle(grdView);

            ASPxPageControl pcContainerStatus = grdView.Parent.NamingContainer as ASPxPageControl;
            if (pcContainerStatus != null)
            {
                TabPage tabULDChild = pcContainerStatus.TabPages.FindByName("tabULDChild");
                if (tabULDChild != null)
                    tabULDChild.ClientVisible = dvChildContainer.Count > 0;
            }
        }
        protected void grdChildContainer_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (Session["C_GrdChildContainerLoading"] != null && Convert.ToBoolean(Session["C_GrdChildContainerLoading"]))
            {
                Session["C_GrdChildContainerLoading"] = false;
                return;
            }

            ASPxGridView gridView = sender as ASPxGridView;
            if (gridView.GetMasterRowKeyValue() != null && gridView.GetMasterRowKeyValue().ToString() != string.Empty)
            {
                Session["C_GrdChildContainerLoading"] = true;
                LoadChildContainerData(gridView);
            }
        }
        protected void grdChildContainer_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Venue")
            {
                e.Value = string.Format("{0}({1})", e.GetListSourceFieldValue("VenueName"), e.GetListSourceFieldValue("VenueCode"));
            }
            else
                if (e.Column.FieldName == "VenueType")
                {
                    e.Value = string.Format("{0}({1})", e.GetListSourceFieldValue("VenueTypeName"), e.GetListSourceFieldValue("VenueTypeCode"));
                }
                else
                    if (e.Column.FieldName == "ProductType")
                    {
                        if (e.GetListSourceFieldValue("ProductName") != null && e.GetListSourceFieldValue("ProductName").ToString() != string.Empty)
                            e.Value = string.Format("{0}({1})", e.GetListSourceFieldValue("ProductName"), e.GetListSourceFieldValue("ProductCode"));
                    }
        }

        private DataTable GetContainerStatusDataTable()
        {
            DataTable dtContainerStatus = Session["C_ContainerStatusTable"] as DataTable;
            if (dtContainerStatus == null)
            {
                dtContainerStatus = ECWebClass.GetConnoteContainerStatusDataTable(Session["C_ConsKey"]);
                dtContainerStatus.PrimaryKey = new DataColumn[] { dtContainerStatus.Columns["LineNo"], dtContainerStatus.Columns["StatusId"] };
                    //new DataColumn[] { dtContainerStatus.Columns["LineNo"],
                    //    dtContainerStatus.Columns["StatusDate"], dtContainerStatus.Columns["StatusTime"]};
                Session["C_ContainerStatusTable"] = dtContainerStatus;
            }
            return dtContainerStatus;
        }
        private void LoadContainerStatusData(ASPxGridView grdView)
        {
            if (Session["EC_User"] == null)
                return;
            if (grdView.ClientSideEvents.CustomButtonClick == string.Empty)
                grdView.ClientSideEvents.CustomButtonClick =
                    @"function(s, e) { 
                        var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                        var idArray = id.split('|'); ppCtrlContainerStatus.cpStatusId = idArray[1];
                        cpContainerStatus.PerformCallback(id);
                     }";

                    /*
                    @"function(s, e) {                         
                        var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                        if (btnId == 'btnEditStatus' && !s.IsNewRowEditing() && !s.IsEditing()) {
                            s.PerformCallback('Edit|' + e.visibleIndex);
                        } else
                        if (btnId == 'btnUpdateStatus') {
                            var notes = s.GetEditValue('StatusNotes'); var movement = s.GetEditValue('MovementDirection');                            
                            s.PerformCallback('Update|' + id + '|' + notes + '|' + movement); 
                        } else
                        if (btnId == 'btnCancelStatus') {
                            s.PerformCallback('Cancel|' + e.visibleIndex);
                        }                        
                    }";
                     */
           
            ASPxPageControl pcContainer = grdView.Parent.NamingContainer as ASPxPageControl;
            if (pcContainer != null)
                ECWebClass.SetPageControlHeaderStyle(pcContainer, false);

            DataTable dtContainerStatus = GetContainerStatusDataTable();
            DataView dvContainerStatus = new DataView(dtContainerStatus);
            dvContainerStatus.RowFilter = string.Format("LineNo = {0}", grdView.GetMasterRowKeyValue());
            grdView.DataSource = dvContainerStatus;
            grdView.DataSourceID = string.Empty;
            grdView.DataBind();

            LoginUser loginUser = Session["EC_User"] as LoginUser;
            grdView.Columns["colAction"].Visible = loginUser.IsAdmin;

            ECWebClass.SetGridViewStyle(grdView);
        }
        protected void grdContainerStatus_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (Session["EC_User"] == null)
                return;
            if (Session["C_GrdContainerStatusLoading"] != null && Convert.ToBoolean(Session["C_GrdContainerStatusLoading"]))
            {
                Session["C_GrdContainerStatusLoading"] = false;
                return;
            }

            ASPxGridView gridView = sender as ASPxGridView;
            if (gridView.GetMasterRowKeyValue() != null && gridView.GetMasterRowKeyValue().ToString() != string.Empty)
            {
                Session["C_GrdContainerStatusLoading"] = true;
                LoadContainerStatusData(gridView);
            }

            ASPxPageControl pcContainerStatus = gridView.Parent.NamingContainer as ASPxPageControl;
            if (pcContainerStatus != null)
            {
                ASPxButton btnAddContainerStatus = pcContainerStatus.FindControl("btnAddContainerStatus") as ASPxButton;
                if (btnAddContainerStatus != null)
                {
                    btnAddContainerStatus.ClientSideEvents.Click = @"function(s, e) { 
                        cpContainerStatus.PerformCallback();
                    }";
                    btnAddContainerStatus.ClientVisible = (Session["EC_User"] as LoginUser).IsAdmin;
                }
            }
        }
        private void AssignConnoteData()
        {
            if (Session["EC_User"] == null)
                return;
            DataTable dtConnote = GetConnoteDataTable();
            if (dtConnote != null && dtConnote.Rows.Count > 0)
            {
                DataRow drConnote = dtConnote.Rows[0];
                string cdid = string.Empty;
                if (drConnote["CDID"] != DBNull.Value && drConnote["CDID"].ToString() != string.Empty)
                    cdid = drConnote["CDID"].ToString();
                if (!string.IsNullOrEmpty(cdid))
                    edtRO.Text = string.Format("{0} ({1})", drConnote["ReturningOfficeName"], cdid);
                else
                    edtRO.Text = string.Format("{0}", drConnote["ReturningOfficeName"]);

                string lgaId = string.Empty;
                if (drConnote["LGAId"] != DBNull.Value && drConnote["LGAId"].ToString() != string.Empty)
                    lgaId = string.Format("{0}", drConnote["LGAId"]).Trim();
                if (!string.IsNullOrEmpty(lgaId))
                    edtLGA.Text = string.Format("{0} ({1})", drConnote["LGAName"], lgaId);
                else
                    edtLGA.Text = string.Format("{0}", drConnote["LGAName"]);

                string contestAreaId = string.Empty;
                LoginUser loginUser = Session["EC_User"] as LoginUser;
                if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                {
                    if (drConnote["WardCode"] != DBNull.Value && drConnote["WardCode"].ToString() != string.Empty)
                        contestAreaId = string.Format("{0}", drConnote["WardCode"]).Trim();
                    if (!string.IsNullOrEmpty(contestAreaId))
                        edtWard.Text = string.Format("{0} ({1})", drConnote["WardName"], contestAreaId);
                    else
                        edtWard.Text = string.Format("{0}", drConnote["WardName"]);
                }
                else
                {
                    if (drConnote["ContestAreaId"] != DBNull.Value && drConnote["ContestAreaId"].ToString() != string.Empty)
                        contestAreaId = string.Format("{0}", drConnote["ContestAreaId"]).Trim();
                    if (!string.IsNullOrEmpty(contestAreaId))
                        edtWard.Text = string.Format("{0} ({1})", drConnote["ContestAreaCode"], contestAreaId);
                    else
                        edtWard.Text = string.Format("{0}", drConnote["ContestAreaCode"]);
                }

                string venueCode = string.Empty;
                if (drConnote["VenueCode"] != DBNull.Value && drConnote["VenueCode"].ToString() != string.Empty)
                {
                    venueCode = drConnote["VenueCode"].ToString();
                    /*
                    if (venueCode.Length > 4)
                        venueCode = venueCode.Substring(venueCode.Length - 4);
                     */
                }
                if (!string.IsNullOrEmpty(venueCode))
                    edtVenue.Text = string.Format("{0} ({1})", drConnote["VenueName"], venueCode);
                else
                    edtVenue.Text = string.Format("{0}", drConnote["VenueName"]);

                string venueType = string.Empty;
                if (drConnote["VenueTypeCode"] != DBNull.Value && drConnote["VenueTypeCode"].ToString() != string.Empty)
                    venueType = drConnote["VenueTypeCode"].ToString();
                if (!string.IsNullOrEmpty(venueType))
                    edtVenueType.Text = string.Format("{0} ({1})", drConnote["VenueTypeName"], venueType);
                else
                    edtVenueType.Text = string.Format("{0}", drConnote["VenueTypeName"]);

                string contestType = string.Empty;
                if (drConnote["ContestCode"] != DBNull.Value && drConnote["ContestCode"].ToString() != string.Empty)
                    contestType = drConnote["ContestCode"].ToString();
                if (!string.IsNullOrEmpty(contestType))
                    edtContest.Text = string.Format("{0} ({1})", drConnote["ContestName"], contestType);
                else
                    edtContest.Text = string.Format("{0}", drConnote["ContestName"]);

                string containerType = string.Empty;
                if (drConnote["ContainerCode"] != DBNull.Value && drConnote["ContainerCode"].ToString() != string.Empty)
                    containerType = drConnote["ContainerCode"].ToString();
                if (!string.IsNullOrEmpty(containerType))
                    edtContainerType.Text = string.Format("{0} ({1})", drConnote["ContainerName"], containerType);
                else
                    edtContainerType.Text = string.Format("{0}", drConnote["ContainerName"]);
                                
                edtContainerQty.Value = drConnote["ContainerQuantity"];

                edtDelivNum.Value = drConnote["DeliveryNumber"];
                string countCentre = string.Empty;
                if (drConnote["CountCentreCode"] != DBNull.Value && drConnote["CountCentreCode"].ToString() != string.Empty)
                    countCentre = drConnote["CountCentreCode"].ToString();
                if (!string.IsNullOrEmpty(countCentre))
                    edtCountCentre.Text = string.Format("{0} ({1})", drConnote["CountCentreName"], countCentre);
                else
                    edtCountCentre.Text = string.Format("{0}", drConnote["CountCentreName"]);

                lblCountCentre.ClientVisible = loginUser.EventType == EventType.LOCAL_GOVERNMENT;
                edtCountCentre.ClientVisible = lblCountCentre.ClientVisible;
                lblDelivNum.ClientVisible = lblCountCentre.ClientVisible;
                edtDelivNum.ClientVisible = lblCountCentre.ClientVisible;

                string labelSource = string.Empty;
                if (drConnote["SourceId"] != DBNull.Value && drConnote["SourceId"].ToString() != string.Empty)
                    labelSource = drConnote["SourceId"].ToString();
                if (!string.IsNullOrEmpty(labelSource))
                    edtSource.Text = string.Format("{0} ({1})", drConnote["Source"], labelSource);
                else
                    edtSource.Text = string.Format("{0}", drConnote["Source"]);
                /*
                if (!(drConnote["AdditionalContainerQuantity"] is DBNull))
                    edtAdditionalQty.Value = drConnote["AdditionalContainerQuantity"];
                else
                    edtAdditionalQty.Value = 0;
                 */
                LoadContainerData();
            }
        }

        protected void grdContainer_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            if (Session["EC_User"] == null)
                return;
            if (e.CellType != GridViewTableCommandCellType.Data)
                return;
            ASPxGridView gridView = sender as ASPxGridView;
            if (e.ButtonID == "btnPrintContainer")
            {                
                e.Visible = DevExpress.Utils.DefaultBoolean.False;
                object objDeleted = gridView.GetRowValues(e.VisibleIndex, "Deleted");
                if (objDeleted != null && objDeleted.ToString() != string.Empty)
                {
                    DateTime dtDeleted;
                    if (DateTime.TryParse(objDeleted.ToString(), out dtDeleted))
                        e.Visible = DevExpress.Utils.DefaultBoolean.False;
                    else
                        e.Visible = DevExpress.Utils.DefaultBoolean.True;
                }
                else
                    e.Visible = DevExpress.Utils.DefaultBoolean.True;
                if (e.Visible == DevExpress.Utils.DefaultBoolean.True)
                {
                    if (e.IsEditingRow)
                        e.Visible = DevExpress.Utils.DefaultBoolean.False;
                }
            }
            else
                if (e.ButtonID == "btnEditContainer")
                {
                    LoginUser loginUser = Session["EC_User"] as LoginUser;
                    if (loginUser.IsAdmin)
                        e.Visible = DevExpress.Utils.DefaultBoolean.True;
                    else
                        e.Visible = DevExpress.Utils.DefaultBoolean.False;
                    if (e.Visible == DevExpress.Utils.DefaultBoolean.True)
                    {
                        if (e.IsEditingRow)
                            e.Visible = DevExpress.Utils.DefaultBoolean.False;
                    }
                }
        }
        
        protected void grdView_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Update ||
                e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Cancel)
                e.Visible = false;
        }
        
        protected void cbPrintLabel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string[] cbInfo = e.Parameter.Split('|');
                if (cbInfo[0] == "Print")
                {
                    DataTable dtContainerDataTable = GetContainerDataTable();
                    object[] cartonKey = { Session["C_ConsKey"], cbInfo[1] };
                    DataRow drContainer = dtContainerDataTable.Rows.Find(cartonKey);
                    if (drContainer != null && !(drContainer["Barcode"] is DBNull) && drContainer["Barcode"].ToString() != string.Empty)
                    {
                        e.Result = string.Format("ConnoteLabelViewer.aspx?qs={0}", ECSecurityClass.GetQSEncripted(string.Format("barcode={0}", drContainer["Barcode"])));
                    }
                    else
                        e.Result = "Error: barcode not found in the system";
                }
            }
        }
        protected void grdContainer_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            DataTable dtConnote = GetConnoteDataTable();
            if (dtConnote != null && dtConnote.Rows.Count > 0)
            {
                DataRow drConnote = dtConnote.Rows[0];
                
                int conQty = ECWebClass.StrToIntDef(drConnote["ContainerQuantity"].ToString(), 0);
                int additionalQuantity = ECWebClass.StrToIntDef(drConnote["AdditionalContainerQuantity"].ToString(), 0);
                int lineNo = GetMaxContainerLineNo(gridView.DataSource as DataTable);
                string containerNo = string.Format("{0}{1}", lineNo.ToString().PadLeft(3, '0'), (conQty + additionalQuantity + 1).ToString().PadLeft(3, '0'));
                string containerBarcode = string.Format("{0}{1}", drConnote["ConsNo"], containerNo).Trim();

                e.NewValues["ConsKey"] = drConnote["ConsKey"];
                e.NewValues["LineNo"] = lineNo;
                e.NewValues["ContainerNo"] = containerNo;
                e.NewValues["Barcode"] = containerBarcode;
                e.NewValues["Activated"] = "Y";
            }
        }

        protected void grdContainerStatus_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            if (Session["EC_User"] == null)
                return;
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                string[] cbInfo = e.Parameters.Split('|');
                LoginUser loginUser = Session["EC_User"] as LoginUser;
                DataTable dtStatus = GetContainerStatusDataTable();
                if (cbInfo[0] == "Edit")
                {
                    gridView.StartEdit(Convert.ToInt32(cbInfo[1]));
                }
                else
                    if (cbInfo[0] == "Cancel")
                    {
                        gridView.CancelEdit();
                    }
                    else
                if (cbInfo[0] == "Update")
                {
                    string procMsg = string.Empty;
                    if (cbInfo[2] != null && cbInfo[2] != string.Empty)
                    {
                        try
                        {
                            DateTime dtStatusTime = DateTime.ParseExact(cbInfo[2], "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                           
                            DataRow[] drsStatus = dtStatus.Select(string.Format("[LineNo]={0} AND StatusTime='{1}'", cbInfo[1], cbInfo[3]));
                            if (drsStatus != null && drsStatus.Length > 0)
                            {
                                foreach (DataRow drStatus in drsStatus)
                                {
                                    DateTime dtStatusTime1 = Convert.ToDateTime(drStatus["StatusDate"]);
                                    dtStatusTime1 = dtStatusTime1.Subtract(new TimeSpan(0, 0, 0, 0, dtStatusTime1.Millisecond));

                                    int secondsDiff = ((TimeSpan)(dtStatusTime - dtStatusTime1)).Seconds;

                                    if (secondsDiff == 0)
                                    {
                                        if (cbInfo[4] != "null")
                                            drStatus["StatusNotes"] = cbInfo[4];
                                        else
                                            drStatus["StatusNotes"] = DBNull.Value;
                                        if (cbInfo[5] != "null")
                                            drStatus["MovementDirection"] = cbInfo[5];
                                        else
                                            drStatus["MovementDirection"] = DBNull.Value;
                                        drStatus["Modified"] = DateTime.Now;
                                        drStatus["ModifiedBy"] = loginUser.UserName;

                                        if (ECWebClass.UpdateContainerStatus(drStatus, ref procMsg))
                                            Session["C_ContainerStatusTable"] = null;
                                        break;
                                    }
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            //
                        }
                    }
                    gridView.CancelEdit();
                }
                
                LoadContainerStatusData(gridView);
            }
            
        }
        protected void grdContainer_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            if (Session["EC_User"] == null)
                return;
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                string[] cbInfo = e.Parameters.Split('|');

                LoginUser loginUser = Session["EC_User"] as LoginUser;
                DataTable dtConnote = GetConnoteDataTable();
                if (dtConnote != null && dtConnote.Rows.Count > 0)
                {
                    string procMsg = string.Empty;

                    DataRow drConnote = dtConnote.Rows[0];
                    DataTable dtContainer = GetContainerDataTable();
                    dtContainer.PrimaryKey = new DataColumn[] { dtContainer.Columns["LineNo"] };
                    DataRow drContainer = null;

                    if (cbInfo[0] == "Update")
                    {
                        if (cbInfo[1] == "null")
                        {
                            drContainer = dtContainer.NewRow();
                            drContainer["ConsKey"] = drConnote["ConsKey"];
                            drContainer["LineNo"] = GetMaxContainerLineNo(dtContainer);
                            drContainer["Barcode"] = cbInfo[2];
                            drContainer["ContainerNo"] = drContainer["Barcode"];
                            if (!(drContainer["ContainerNo"] is DBNull) && drContainer["ContainerNo"].ToString() != string.Empty &&
                                drContainer["ContainerNo"].ToString().Length >= 6)
                                drContainer["ContainerNo"] = drContainer["ContainerNo"].ToString().Substring(drContainer["ContainerNo"].ToString().Length - 6);
                            if (cbInfo[3] != "null")
                                drContainer["DeletedBy"] = cbInfo[3];
                            else
                                drContainer["DeletedBy"] = DBNull.Value;
                            if (cbInfo[4] != "null")
                                drContainer["DeletedReason"] = cbInfo[4];
                            else
                                drContainer["DeletedReason"] = DBNull.Value;
                            if (cbInfo[5] != "null")
                            {
                                try
                                {
                                    DateTime dtDeleted = Convert.ToDateTime(cbInfo[5]);
                                    if (cbInfo[6] != "null")
                                    {
                                        string[] cbTime = cbInfo[6].Split(':');
                                        if (cbTime.Length > 1)
                                        {
                                            dtDeleted = dtDeleted.AddHours(Convert.ToDouble(cbTime[0]));
                                            dtDeleted = dtDeleted.AddMinutes(Convert.ToDouble(cbTime[1]));
                                        }
                                    }
                                    drContainer["Deleted"] = dtDeleted;
                                }
                                catch(Exception ex)
                                {
                                    drContainer["Deleted"] = DateTime.Now;
                                }
                            }
                            else
                                drContainer["Deleted"] = DBNull.Value;
                            /*
                            if (cbInfo[7] != "null")
                                drContainer["Activated"] = cbInfo[7];
                            else
                             */
                                drContainer["Activated"] = "Y";                            
                            drContainer["Created"] = DateTime.Now;
                            drContainer["CreatedBy"] = loginUser.UserName;
                            drContainer["Added"] = drContainer["Created"];
                            drContainer["AddedBy"] = drContainer["CreatedBy"];
                            dtContainer.Rows.Add(drContainer);
                        }
                        else
                        {
                            drContainer = dtContainer.Rows.Find(cbInfo[1]);
                            if (drContainer != null)
                            {
                                if (cbInfo[3] != "null")
                                    drContainer["DeletedBy"] = cbInfo[3];
                                else
                                    drContainer["DeletedBy"] = DBNull.Value;
                                if (cbInfo[4] != "null")
                                    drContainer["DeletedReason"] = cbInfo[4];
                                else
                                    drContainer["DeletedReason"] = DBNull.Value;
                                if (cbInfo[5] != "null")
                                {
                                    try
                                    {
                                        DateTime dtDeleted = Convert.ToDateTime(cbInfo[5]);
                                        if (cbInfo[6] != "null")
                                        {
                                            string[] cbTime = cbInfo[6].Split(':');
                                            if (cbTime.Length > 1)
                                            {
                                                dtDeleted = dtDeleted.AddHours(Convert.ToDouble(cbTime[0]));
                                                dtDeleted = dtDeleted.AddMinutes(Convert.ToDouble(cbTime[1]));
                                            }
                                        }
                                        drContainer["Deleted"] = dtDeleted;
                                    }
                                    catch (Exception ex)
                                    {
                                        drContainer["Deleted"] = DateTime.Now;
                                    }
                                }
                                else
                                    drContainer["Deleted"] = DBNull.Value;
                                /*
                                if (cbInfo[7] != "null")
                                    drContainer["Activated"] = cbInfo[7];
                                else
                                 */
                                    drContainer["Activated"] = "Y";
                                drContainer["Modified"] = DateTime.Now;
                                drContainer["ModifiedBy"] = loginUser.UserName;
                            }
                        }
                        if (ECWebClass.UpdateContainerConnote(drContainer, ref procMsg))
                            Session["C_ContainerTable"] = null;
                    }
                }
                gridView.CancelEdit();
                LoadContainerData();
            }
        }
        private static int GetMaxContainerLineNo(DataTable dtContainer)
        {
            int result = 0;
            if (dtContainer != null && dtContainer.Rows.Count > 0)
            {
                DataView dvContainer = new DataView(dtContainer);
                dvContainer.Sort = "LineNo asc";
                DataRowView drv = dvContainer[dvContainer.Count - 1];
                result = Convert.ToInt32(drv.Row["LineNo"]);
            }
            result++;
            return result;
        }

        protected void grdContainer_DataBound(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ASPxGridView gridView = sender as ASPxGridView;
                DataTable dtConStatus = GetContainerStatusDataTable();
                DataRow[] drsConStatus = null;
                for (int i = 0; i < gridView.VisibleRowCount; i++)
                {
                    object lineKey = gridView.GetRowValues(i, new string[] { gridView.KeyFieldName });
                    drsConStatus = dtConStatus.Select(string.Format("[LineNo]={0}", lineKey));
                    if (drsConStatus != null && drsConStatus.Length > 0)
                        gridView.DetailRows.ExpandRow(i);
                }
            }
        }

        private DataTable GetEMODataTable()
        {
            DataTable dt = Session["C_EMOTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetEMODataExt(true);
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["C_EMOTable"] = dt;
            }
            return dt;
        }
        private void LoadEMOData()
        {
            cmbEMO.DataSourceID = string.Empty;
            cmbEMO.DataSource = GetEMODataTable();
            cmbEMO.DataBind();
            cmbEMO.SelectedIndex = 0;
            cmbEMO.ClientEnabled = true;
        }
        private DataTable GetContestAreaDataTable()
        {
            DataTable dt = Session["C_ContestArea"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetContestAreaDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["C_ContestArea"] = dt;
            }
            return dt;
        }
        private void LoadDistrictData()
        {
            DataTable dtArea = GetContestAreaDataTable();
            DataView dvArea = new DataView(dtArea) { /*RowFilter = String.Format("ROCode = '{0}'", cmbEMO.Value)*/ };
            cmbDistrict.DataSourceID = string.Empty;
            cmbDistrict.DataSource = dvArea;
            cmbDistrict.DataBind();
            cmbDistrict.SelectedIndex = 0;
            cmbDistrict.ClientEnabled = true;
        }
        private void LoadContestAreaData()
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            cmbContestArea.DataSourceID = string.Empty;
            if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
            {
                DataTable dtWard = GetWardDataTable();
                DataView dvWard = new DataView(dtWard) { RowFilter = String.Format("(ROCode='{0}' AND LGAId='{1}') OR (Code='-1')", cmbEMO.Value, cmbDistrict.Value) };
                cmbContestArea.DataSource = dvWard;
            }
            else
                cmbContestArea.DataSource = GetContestAreaDataTable();
            cmbContestArea.DataBind();
            cmbContestArea.SelectedIndex = 0;
            cmbContestArea.ClientEnabled = false;
        }
        private DataTable GetWardDataTable()
        {
            DataTable dt = Session["C_Ward"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetWardDataExt();
                //dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["C_Ward"] = dt;
            }
            return dt;
        }
        private DataTable GetProductTypeDataTable()
        {
            DataTable dt = Session["C_ProductType"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetProductTypeDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["C_ProductType"] = dt;
            }
            return dt;
        }
        private void LoadProductTypeData()
        {
            cmbProduct.DataSourceID = string.Empty;
            cmbProduct.DataSource = GetProductTypeDataTable();
            cmbProduct.DataBind();
            cmbProduct.SelectedIndex = 0;
        }
        private DataTable GetVenueDataTable()
        {
            DataTable dt = Session["C_Venue"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetVenueDataExt();
                //dt.PrimaryKey = new DataColumn[] { dt.Columns["ROCode"], dt.Columns["LGACode"], dt.Columns["Code"] };
                Session["C_Venue"] = dt;
            }
            return dt;
        }
        private void LoadVenueData()
        {
            DataTable dtVenue = GetVenueDataTable();
            DataView dvVenue = new DataView(dtVenue);
            if (cmbEMO.Value != null && cmbEMO.Value.ToString() != "-1" && cmbDistrict.Value != null && cmbDistrict.Value.ToString() != "-1")
                dvVenue.RowFilter = String.Format("(ROCode='{0}' AND LGACode='{1}') OR Code='-1'", cmbEMO.Value, cmbDistrict.Value);
            else
                if (cmbEMO.Value != null && cmbEMO.Value.ToString() != "-1")
                    dvVenue.RowFilter = String.Format("ROCode='{0}' OR Code='-1'", cmbEMO.Value);
                else
                    dvVenue.RowFilter = "ROCode IS NULL";

            cmbVenue.DataSourceID = string.Empty;
            cmbVenue.DataSource = dvVenue;
            cmbVenue.DataBind();
            cmbVenue.SelectedIndex = 0;
        }
        private DataTable GetContainerTypeDataTable()
        {
            DataTable dt = Session["C_ContainerType"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetContainerTypeDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["C_ContainerType"] = dt;
            }
            return dt;
        }
        private void LoadContainerTypeData()
        {
            cmbContainerType.DataSourceID = string.Empty;
            cmbContainerType.DataSource = GetContainerTypeDataTable();
            cmbContainerType.DataBind();
            cmbContainerType.SelectedIndex = 0;
        }
        protected void cpContainer_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            if (!loginUser.IsAdmin)
            {
                //DevExpress.Web.ASPxWebControl.RedirectOnCallback("Login.aspx");
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }

            LoadEMOData();
            LoadDistrictData();
            LoadProductTypeData();
            LoadContainerTypeData();

            if (!string.IsNullOrEmpty(e.Parameter))
            {
                DataTable dtConnote = GetConnoteDataTable();                
                if (dtConnote != null && dtConnote.Rows.Count > 0)
                {
                    DataTable dtContainerStatus = GetContainerStatusDataTable();
                    DataRow[] drsContainerStatus = dtContainerStatus.Select(string.Format("[LineNo]={0} AND [Deleted] IS NULL AND ([Duplicated] IS NULL OR [Duplicated]='N')", e.Parameter));
                    bool scanned = drsContainerStatus != null && drsContainerStatus.Length > 0;
                    DataRow drConnote = dtConnote.Rows[0];
                    if (drConnote["CDID"] != DBNull.Value && drConnote["CDID"].ToString() != string.Empty)
                    {
                        cmbEMO.Value = drConnote["CDID"].ToString();
                        cmbEMO.ClientEnabled = false;
                    }
                    if (drConnote["LGAId"] != DBNull.Value && drConnote["LGAId"].ToString() != string.Empty)
                    {
                        cmbDistrict.Value = drConnote["LGAId"].ToString();
                        cmbDistrict.ClientEnabled = false;
                    }
                    LoadContestAreaData();                    
                    if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                    {
                        if (drConnote["WardCode"] != DBNull.Value && drConnote["WardCode"].ToString() != string.Empty)
                        {
                            cmbContestArea.Value = drConnote["WardCode"].ToString();
                            cmbContestArea.ClientEnabled = false;
                        }
                    }
                    else
                    {
                        if (drConnote["ContestAreaId"] != DBNull.Value && drConnote["ContestAreaId"].ToString() != string.Empty)
                        {
                            cmbContestArea.Value = drConnote["ContestAreaId"].ToString();
                            cmbContestArea.ClientEnabled = false;
                        }
                    }
                    if (drConnote["ContestCode"] != DBNull.Value && drConnote["ContestCode"].ToString() != string.Empty)
                    {
                        cmbProduct.Value = drConnote["ContestCode"].ToString();
                        cmbProduct.ClientEnabled = !scanned;
                    }
                    LoadVenueData();
                    if (drConnote["VenueCode"] != DBNull.Value && drConnote["VenueCode"].ToString() != string.Empty)
                    {
                        cmbVenue.Value = drConnote["VenueCode"].ToString();
                        cmbVenue.ClientEnabled = !scanned;
                    }
                    if (drConnote["ContainerCode"] != DBNull.Value && drConnote["ContainerCode"].ToString() != string.Empty)
                    {
                        cmbContainerType.Value = drConnote["ContainerCode"].ToString();
                        cmbContainerType.ClientEnabled = !scanned;
                    }

                    DataTable dtContainerDataTable = GetContainerDataTable();
                    if (dtContainerDataTable != null && dtContainerDataTable.Rows.Count > 0)
                    {
                        object[] cartonKey = { Session["C_ConsKey"], e.Parameter };
                        DataRow drContainer = dtContainerDataTable.Rows.Find(cartonKey);
                        if (drContainer != null)
                        {
                            cbCtnDelete.Checked = !(drContainer["Deleted"] is DBNull);
                            edtCtnDelReason.Text = string.Format("{0}", drContainer["DeletedReason"]);
                        }
                    }
                }
            }
        }
        protected void cbContainer_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            if (!loginUser.IsAdmin)
            {
                //DevExpress.Web.ASPxWebControl.RedirectOnCallback("Login.aspx");
                e.Result = "Error: illegal operation detected!";
                return;
            }
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string[] cbInfo = e.Parameter.Split('|');
                string procMsg = string.Empty;                

                DataTable dtProd = GetProductTypeDataTable();
                DataTable dtCtn = GetContainerTypeDataTable();
                DataTable dtVenue = GetVenueDataTable();
                DataTable dtConnote = GetConnoteDataTable();

                if (dtConnote != null && dtConnote.Rows.Count > 0)
                {
                    DataRow drConnote = dtConnote.Rows[0];
                    DataTable dtContainer = GetContainerDataTable();
                    object[] cartonKey = { Session["C_ConsKey"], cbInfo[0] };
                    DataRow drContainer = dtContainer.Rows.Find(cartonKey);
                    if (drContainer != null)
                    {
                        bool conModified = string.Compare(cbInfo[1], string.Format("{0}", drConnote["ContestCode"]), false) != 0 ||
                            (cbInfo[2] != "-1" && string.Compare(cbInfo[2], string.Format("{0}", drConnote["VenueCode"]), false) != 0) ||
                            (cbInfo[3] != "-1" && string.Compare(cbInfo[3], string.Format("{0}", drConnote["ContainerCode"]), false) != 0);
                        if (cbInfo[4] == "Y") // deactivate
                        {
                            drContainer["DeletedReason"] = cbInfo[5];
                            drContainer["Deleted"] = DateTime.Now;
                            drContainer["DeletedBy"] = loginUser.UserName;
                            drContainer["Modified"] = DateTime.Now;
                            drContainer["ModifiedBy"] = loginUser.UserName;
                            if (ECWebClass.UpdateContainerConnote(drContainer, ref procMsg))
                                Session["C_ContainerTable"] = null;
                        }
                        else
                        {
                            if (conModified)
                            {                                
                                if (dtContainer.Rows.Count > 1)
                                {
                                    DataRow drNewConnote = dtConnote.NewRow();
                                    drNewConnote.ItemArray = drConnote.ItemArray;

                                    drNewConnote["ContestCode"] = cbInfo[1];
                                    DataRow drProd = dtProd.Rows.Find(cbInfo[1]);
                                    if (drProd != null)
                                        drNewConnote["ContestName"] = drProd["ShortName"];
                                    drNewConnote["VenueCode"] = cbInfo[2];
                                    DataRow[] drsVenue = dtVenue.Select(string.Format("ROCode='{0}' AND LGACode='{1}' AND Code='{2}'", drNewConnote["CDID"], drNewConnote["LGAId"], cbInfo[2]));
                                    if (drsVenue != null && drsVenue.Length > 0)
                                    {
                                        drNewConnote["VenueName"] = drsVenue[0]["ShortName"];
                                        drNewConnote["VenueLongName"] = drsVenue[0]["LongName"];
                                        drNewConnote["VenueTypeCode"] = drsVenue[0]["VenueTypeCode"];
                                        drNewConnote["VenueTypeName"] = drsVenue[0]["VenueTypeName"];
                                    }
                                    drNewConnote["ContainerCode"] = cbInfo[3];
                                    DataRow drCtn = dtCtn.Rows.Find(cbInfo[3]);
                                    if (drCtn != null)
                                        drNewConnote["ContainerName"] = drCtn["ShortName"];

                                    object newConsKeyObj = Guid.NewGuid().ToString().ToUpper();
                                    drNewConnote["ConsKey"] = newConsKeyObj;
                                    drNewConnote["ContainerQuantity"] = 1;
                                    drNewConnote["AdditionalContainerQuantity"] = 0;
                                    drNewConnote["Created"] = DateTime.Now;
                                    drNewConnote["CreatedBy"] = loginUser.UserName;
                                    drNewConnote["Modified"] = DBNull.Value;
                                    drNewConnote["ModifiedBy"] = DBNull.Value;
                                    if (ECWebClass.UpdateTableRecordById("NSW_Connote", drNewConnote, false, ref procMsg, false, "ConsKey"))
                                    {
                                        DataRow drNewContainer = dtContainer.NewRow();
                                        drNewContainer.ItemArray = drContainer.ItemArray;
                                        drNewContainer["ConsKey"] = newConsKeyObj;
                                        drNewContainer["LineNo"] = 1;
                                        drNewContainer["Created"] = DateTime.Now;
                                        drNewContainer["CreatedBy"] = loginUser.UserName;
                                        drNewContainer["Modified"] = DBNull.Value;
                                        drNewContainer["ModifiedBy"] = DBNull.Value;
                                        if (ECWebClass.UpdateTableRecordById("NSW_Container", drNewContainer, false, ref procMsg, false, "ConsKey;LineNo"))
                                        {
                                            drContainer["DeletedReason"] = string.Format("Moved [{0}]", newConsKeyObj);
                                            drContainer["Deleted"] = DateTime.Now;
                                            drContainer["DeletedBy"] = loginUser.UserName;
                                            drContainer["Modified"] = DateTime.Now;
                                            drContainer["ModifiedBy"] = loginUser.UserName;
                                            if (ECWebClass.UpdateTableRecordById("NSW_Container", drContainer, false, ref procMsg, false, "ConsKey;LineNo"))
                                            {
                                                drConnote["ContainerQuantity"] = Convert.ToInt16(drConnote["ContainerQuantity"]) - 1;
                                                drConnote["Modified"] = DateTime.Now;
                                                drConnote["ModifiedBy"] = loginUser.UserName;
                                                if (ECWebClass.UpdateTableRecordById("NSW_Connote", drConnote, false, ref procMsg, false, "ConsKey"))
                                                    e.Result = string.Format("{0}?qs={1}", "Consignment.aspx", ECSecurityClass.GetQSEncripted(String.Format("conskey={0}", newConsKeyObj)));                                                
                                            }
                                            else
                                            if (!string.IsNullOrEmpty(procMsg))
                                                e.Result = string.Format("Error: {0}", procMsg);
                                        }
                                        else
                                        if (!string.IsNullOrEmpty(procMsg))
                                            e.Result = string.Format("Error: {0}", procMsg);
                                    }
                                    else
                                        if (!string.IsNullOrEmpty(procMsg))
                                            e.Result = string.Format("Error: {0}", procMsg);
                                    return;
                                }
                                else
                                {
                                    drConnote["ContestCode"] = cbInfo[1];
                                    DataRow drProd = dtProd.Rows.Find(cbInfo[1]);
                                    if (drProd != null)
                                        drConnote["ContestName"] = drProd["ShortName"];
                                    drConnote["VenueCode"] = cbInfo[2];
                                    DataRow[] drsVenue = dtVenue.Select(string.Format("ROCode='{0}' AND LGACode='{1}' AND Code='{2}'", drConnote["CDID"], drConnote["LGAId"], cbInfo[2]));
                                    if (drsVenue != null && drsVenue.Length > 0)
                                    {
                                        drConnote["VenueName"] = drsVenue[0]["ShortName"];
                                        drConnote["VenueLongName"] = drsVenue[0]["LongName"];
                                        drConnote["VenueTypeCode"] = drsVenue[0]["VenueTypeCode"];
                                        drConnote["VenueTypeName"] = drsVenue[0]["VenueTypeName"];
                                    }
                                    drConnote["ContainerCode"] = cbInfo[3];
                                    DataRow drCtn = dtCtn.Rows.Find(cbInfo[3]);
                                    if (drCtn != null)
                                        drConnote["ContainerName"] = drCtn["ShortName"];
                                    drConnote["Modified"] = DateTime.Now;
                                    drConnote["ModifiedBy"] = loginUser.UserName;  
                                    if (ECWebClass.UpdateTableRecordById("NSW_Connote", drConnote, false, ref procMsg, false, "ConsKey"))
                                        e.Result = string.Format("{0}?qs={1}", "Consignment.aspx", ECSecurityClass.GetQSEncripted(String.Format("conskey={0}", drConnote["ConsKey"])));
                                    return;
                                }
                            }
                            else
                            {
                                drContainer["DeletedReason"] = DBNull.Value;
                                drContainer["Deleted"] = DBNull.Value;
                                drContainer["DeletedBy"] = DBNull.Value;
                                drContainer["Modified"] = DateTime.Now;
                                drContainer["ModifiedBy"] = loginUser.UserName;
                                if (ECWebClass.UpdateContainerConnote(drContainer, ref procMsg))
                                    Session["C_ContainerTable"] = null;
                            }
                        }
                    }
                    LoadContainerData();
                }
                if (!string.IsNullOrEmpty(procMsg))
                    e.Result = string.Format("Error: {0}", procMsg);
            }
        }
        protected void cpContainerStatus_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            if (!loginUser.IsAdmin)
            {
                //DevExpress.Web.ASPxWebControl.RedirectOnCallback("Login.aspx");
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            edtStatusDate.Date = DateTime.Now;
            cmbStatus.DataSourceID = string.Empty;
            cmbStatus.DataSource = ECWebClass.GetStatusStageDataExt();
            cmbStatus.DataBind();
            cmbStatus.SelectedIndex = 0;
            edtStatusDate.ClientEnabled = true;
            
            cbDelete.ClientVisible = false;
            edtDelReason.ClientVisible = false;

            DataTable dtConnote = GetConnoteDataTable();
            DataRow drConnote = dtConnote.Rows[0];

            cmbLocation.DataSourceID = string.Empty;
            //cmbLocation.DataSource = ECWebClass.GetScannedLocationDataExt(drConnote["CDID"]);
            cmbLocation.DataSource = ECWebClass.GetScannedLocationDataExt(null);
            cmbLocation.DataBind();
            cmbLocation.SelectedIndex = 0;

            DataTable dtContainerDataTable = GetContainerDataTable();
            DataTable dtStatus = GetContainerStatusDataTable();

            if (!string.IsNullOrEmpty(e.Parameter)) // eg "1|02/17/2020 13:44:13|13:44:13"
            {
                string[] cbInfo = e.Parameter.Split('|');
                if (cbInfo[0] != null && cbInfo[0] != string.Empty)
                {
                    DataRow[] drsContainer = dtContainerDataTable.Select(string.Format("[LineNo]={0}", cbInfo[0]));
                    if (drsContainer != null && drsContainer.Length > 0)
                    {
                        if (cbInfo[1] != null && cbInfo[1] != string.Empty)
                        {
                            object[] refKey = { cbInfo[0], cbInfo[1] };
                            DataRow drStatus = dtStatus.Rows.Find(refKey);
                            if (drStatus != null)
                            {
                                edtStatusDate.Date = Convert.ToDateTime(drStatus["StatusDate"]);
                                edtStatusDate.ClientEnabled = false;
                                cmbStatus.Value = drStatus["StatusStage"].ToString();
                                cmbLocation.Value = drStatus["LocationCode"].ToString();
                                edtMovement.Value = string.Format("{0}", drStatus["MovementDirection"]);
                                cbDelete.Checked = !(drStatus["Deleted"] is DBNull);
                                edtDelReason.Value = string.Format("{0}", drStatus["DeletedReason"]);
                            }
                        }
                    }
                }
                cbDelete.ClientVisible = true;
                edtDelReason.ClientVisible = true;
            }
        }
        protected void cbContainerStatus_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            if (!loginUser.IsAdmin)
            {
                //DevExpress.Web.ASPxWebControl.RedirectOnCallback("Login.aspx");
                e.Result = "Error: illegal operation detected!";
                return;
            }
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string[] cbInfo = e.Parameter.Split('|');
                // new - "1||19/02/2020 3:16 PM|102|2.00|test|N|"
                // edit - "1|EE769F0B-9163-48C0-84C6-E8CE93398C5F|19/02/2020 2:13 PM|102|2.00|From 'Printer(Printer)' to 'Auburn EM Office(102)'|Y|123"
               
                //DateTime dtStatusTime = DateTime.ParseExact(cbInfo[2], "dd/MM/yyyy h:mm tt", CultureInfo.InvariantCulture); 
                
                DataTable dtStatus = GetContainerStatusDataTable();
                DataTable dtStatusStage = ECWebClass.GetStatusStageDataExt();
                dtStatusStage.PrimaryKey = new DataColumn[] { dtStatusStage.Columns["StageId"] };
                DataRow drStatusStage = dtStatusStage.Rows.Find(cbInfo[4]);
                DataTable dtConnote = GetConnoteDataTable();
                DataRow drConnote = dtConnote.Rows[0];
                DataTable dtScanLocation = ECWebClass.GetScannedLocationDataExt(drConnote["CDID"]);
                dtScanLocation.PrimaryKey = new DataColumn[] { dtScanLocation.Columns["Code"] };
                DataRow drScanLocation = dtScanLocation.Rows.Find(cbInfo[3]);
                DataRow drStatus = null; string procMsg = string.Empty;
                DateTime dtNow = DateTime.Now;
                if (string.IsNullOrEmpty(cbInfo[1])) // new
                {
                    drStatus = dtStatus.NewRow();
                    drStatus["ConsKey"] = drConnote["ConsKey"];
                    drStatus["LineNo"] = cbInfo[0];
                    drStatus["StatusId"] = Guid.NewGuid().ToString().ToUpper();
                    drStatus["StatusDate"] = dtNow;
                    drStatus["StatusTime"] = dtNow.ToString("HH:mm:ss");
                    if (drStatusStage != null)
                    {
                        drStatus["StatusStage"] = drStatusStage["StageId"].ToString();
                        drStatus["StatusNotes"] = drStatusStage["Stage"];
                    }
                    if (drScanLocation != null)
                    {
                        drStatus["LocationCode"] = drScanLocation["Code"];
                        drStatus["LocationName"] = drScanLocation["Name1"];
                    }
                    drStatus["MovementDirection"] = cbInfo[5];
                    drStatus["Created"] = dtNow;
                    drStatus["CreatedBy"] = loginUser.UserName;
                    dtStatus.Rows.Add(drStatus);
                }
                else
                {
                    object[] refKey = { cbInfo[0], cbInfo[1] };
                    drStatus = dtStatus.Rows.Find(refKey);
                    if (drStatus != null)
                    {
                        if (!string.IsNullOrEmpty(cbInfo[6]) && Convert.ToChar(cbInfo[6]) == 'Y') // deactivate
                        {
                            drStatus["DeletedReason"] = cbInfo[7];
                            drStatus["Deleted"] = dtNow;
                            drStatus["DeletedBy"] = loginUser.UserName;                            
                        }
                        else // edit
                        {
                            //drStatus["StatusDate"] = dtStatusTime;
                            //drStatus["StatusTime"] = dtStatusTime.ToString("HH:mm:ss");
                            if (drStatusStage != null)
                            {
                                drStatus["StatusStage"] = drStatusStage["StageId"].ToString();
                                drStatus["StatusNotes"] = drStatusStage["Stage"];
                            }
                            if (drScanLocation != null)
                            {
                                drStatus["LocationCode"] = drScanLocation["Code"];
                                drStatus["LocationName"] = drScanLocation["Name1"];
                            }
                            drStatus["MovementDirection"] = cbInfo[5];
                            drStatus["Modified"] = dtNow;
                            drStatus["ModifiedBy"] = loginUser.UserName;
                            drStatus["DeletedReason"] = DBNull.Value;
                            drStatus["Deleted"] = DBNull.Value;
                            drStatus["DeletedBy"] = DBNull.Value;
                        }
                    }
                }
                if (ECWebClass.UpdateContainerStatus(drStatus, ref procMsg))
                {
                    Session["C_ContainerStatusTable"] = null;
                }
                e.Result = !string.IsNullOrEmpty(procMsg) ? procMsg : string.Empty;
            }
        }
    }
}