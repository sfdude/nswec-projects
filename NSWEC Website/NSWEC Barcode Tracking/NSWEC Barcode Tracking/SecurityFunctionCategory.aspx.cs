﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using DevExpress.Data;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class SecurityFunctionCategory : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "SecurityFunctionCategory.aspx";
            string tgtLogin = string.Empty;
            if (!ECSecurityClass.CheckQueryString(this.Page, "loginId", ref tgtLogin))
                //Response.Redirect("Default.aspx?Logout=1");
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            //if (!loginUser.IsAdmin || string.Compare(loginUser.UserName, tgtLogin, true) != 0)
            if (loginUser == null || !(loginUser.IsAdmin && string.Compare(loginUser.UserName, tgtLogin, true) == 0))
                //Response.Redirect("Default.aspx?Logout=1");
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("FC_SecFuncCatTable");
                Session.Remove("FC_SecFuncTable");
                Session.Remove("FC_CompanyTable");
                Session.Remove("FC_GrdFunctionLoading");
                Session.Remove("FC_FunctionID");
                Session.Remove("FC_SecCompTable");
                Session.Remove("FC_SecCompId");
                ppCtrlFunction.JSProperties["cpFunctionID"] = -1;

                AssignClientSideScripts();

                LoginUser loginUser = Session["EC_User"] as LoginUser;
                DevExpress.Web.MenuItem itmNewCategory = menAction.Items.FindByName("itmNewCategory");
                if (itmNewCategory != null)
                    itmNewCategory.ClientVisible = loginUser != null && loginUser.SpecialUser;

                ECWebClass.SetMenuStyle(menAction);
                ECWebClass.SetGridViewStyle(grdFunctionCategory);
                ECWebClass.SetGridViewStyle(grdFuncComponent);
                ECWebClass.SetPageControlHeaderStyle(pcFuncDetails, false);
            }
            LoadSecFuncCatDataTable();
            if (Session["FC_FunctionID"] != null)
                LoadGrdSecCompData(Session["FC_FunctionID"]);
        }
        protected void menAction_ItemClick(object source, DevExpress.Web.MenuItemEventArgs e)
        {
            string exportName = e.Item.Name.Replace("itmExp", string.Empty);
            ECWebClass.GridViewExport(grdFunctionCategory, exportName);
        }
        private DataTable GetSecFuncCatDataTable()
        {
            DataTable dtSecFuncCat = Session["FC_SecFuncCatTable"] as DataTable;

            if (dtSecFuncCat == null)
            {
                dtSecFuncCat = ECSecurityClass.GetSecurityFunctionCategoryDataTable();
                dtSecFuncCat.PrimaryKey = new DataColumn[] { dtSecFuncCat.Columns["ID"] };

                Session["FC_SecFuncCatTable"] = dtSecFuncCat;
            }
            return dtSecFuncCat;
        }
        private void LoadSecFuncCatDataTable()
        {
            DataTable dtSecFuncCat = GetSecFuncCatDataTable();

            grdFunctionCategory.DataSource = dtSecFuncCat;
            grdFunctionCategory.DataSourceID = String.Empty;
            grdFunctionCategory.DataBind();

            LoginUser loginUser = Session["EC_User"] as LoginUser;
            grdFunctionCategory.Columns["colCategoryAction"].Visible = loginUser != null && loginUser.SpecialUser;

            cmbCategory.DataSource = dtSecFuncCat;
            cmbCategory.DataSourceID = String.Empty;
            cmbCategory.DataBind();
        }
        private void AssignClientSideScripts()
        {
            menAction.ClientSideEvents.ItemClick = "function(s, e) { " +
                "e.processOnServer = e.item.parent.name == 'itmExport'; " +
                "if (e.item.name == 'itmNewCategory') { " +
                "if (typeof(grdFunctionCategory) != 'undefined' && !grdFunctionCategory.IsNewRowEditing() && !grdFunctionCategory.IsEditing()) { " +
                "grdFunctionCategory.AddNewRow(); }}} ";
            grdFunctionCategory.ClientSideEvents.CustomButtonClick = "function(s, e) { " +
                "var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); " +
                "if (btnId == 'btnEditCategory') { " +
                "if (!s.IsNewRowEditing() && !s.IsEditing()) s.StartEditRow(e.visibleIndex); } else " +
                "if (btnId == 'btnDeleteCategory') {if (confirm('Comfirm delete?')) { s.PerformCallback('Delete|' + id); } } else " +
                "if (btnId == 'btnUpdateCategory') { " +
                "if(!ASPxClientEdit.ValidateEditorsInContainerById('grdFunctionCategory')) return; " +
                "var catName = s.GetEditValue('Name'); var catActive = s.GetEditValue('Active'); " +
                "s.PerformCallback('Update|' + id + '|' + catName + '|' + catActive); } else  " +
                "if (btnId == 'btnCancelCategory') {s.CancelEdit();}} ";
            ASPxButton btnFuncSave = ppCtrlFunction.FindControl("btnFuncSave") as ASPxButton;
            if (btnFuncSave != null)
                btnFuncSave.ClientSideEvents.Click = "function(s, e) { " +
                    "if(!ASPxClientEdit.ValidateEditorsInContainerById('ppCtrlFunction')) return; " +
                    "ppCtrlFunction.Hide(); grdSecurityFunction.PerformCallback('Update|' + ppCtrlFunction.cpFunctionID); } ";
            ASPxButton btnFuncCancel = ppCtrlFunction.FindControl("btnFuncCancel") as ASPxButton;
            if (btnFuncCancel != null)
                btnFuncCancel.ClientSideEvents.Click = "function(s, e) { " +
                    "ASPxClientEdit.ClearEditorsInContainerById('functionContainer'); " + // clear validation error icon image
                    "ppCtrlFunction.cpFunctionID = -1; ppCtrlFunction.Hide(); } ";
            cpFunction.ClientSideEvents.EndCallback = "function(s, e) { " +
                "ppCtrlFunction.SetHeaderText(grdSecurityFunction.cpFuncActionInfo); " +
                "grdSecurityFunction.cpFuncActionInfo = ''; grdFuncComponent.Refresh(); ppCtrlFunction.Show(); } ";
            grdFuncComponent.ClientSideEvents.CustomButtonClick = "function(s, e) { " +
                "var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); " +
                "if (btnId == 'btnEditComp') { " +
                "if (!s.IsNewRowEditing() && !s.IsEditing()) s.StartEditRow(e.visibleIndex); } else " +
                "if (btnId == 'btnDeleteComp') {if (confirm('Comfirm delete?')) { s.PerformCallback('Delete|' + id); } } else " +
                "if (btnId == 'btnUpdateComp') { " +
                "if(!ASPxClientEdit.ValidateEditorsInContainerById('grdFuncComponent')) return; " +
                "var frmName = s.GetEditValue('FormName'); var frameComp = s.GetEditValue('FrameName'); " +
                "var compName = s.GetEditValue('ComponentName'); var cap = s.GetEditValue('Caption'); " +
                "var url = s.GetEditValue('Url'); var param = s.GetEditValue('UrlParam'); " +
                "var compEnable = s.GetEditValue('Enabled'); var compVisible = s.GetEditValue('Visible'); " +
                "s.PerformCallback('Update|' + id + '|' + frmName + '|' + frameComp + '|' + compName + '|' + " +
                "cap + '|' + url + '|' + param + '|' + compEnable + '|' + compVisible); } else  " +
                "if (btnId == 'btnCancelComp') {s.CancelEdit();}} ";
            btnAddFuncComp.ClientSideEvents.Click = "function(s, e) { " +
                "if (!grdFuncComponent.IsNewRowEditing() && !grdFuncComponent.IsEditing()) grdFuncComponent.AddNewRow(); }";
        }
        protected void grdView_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Update ||
                e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Cancel)
                e.Visible = false;
        }
        protected void grdFunctionCategory_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (Session["EC_User"] == null)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (e.Parameters != null && e.Parameters != "")
            {
                DataTable dtSecFuncCat = GetSecFuncCatDataTable();
                DataRow drSecFuncCat = null;
                bool success = false;
                string message = string.Empty;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Delete")
                {
                    drSecFuncCat = dtSecFuncCat.Rows.Find(cbInfo[1]);
                    success = drSecFuncCat != null && ECSecurityClass.UpdateFunctionCategory(ref message, dtSecFuncCat, drSecFuncCat, true);
                }
                else
                    if (cbInfo[0] == "Update")
                    {
                        bool isNew = cbInfo[1] == "null";
                        if (!isNew)
                            drSecFuncCat = dtSecFuncCat.Rows.Find(cbInfo[1]);
                        else
                            drSecFuncCat = dtSecFuncCat.NewRow();
                        if (drSecFuncCat != null)
                        {
                            if (cbInfo[2] != "null")
                                drSecFuncCat["Name"] = cbInfo[2];
                            
                            if (cbInfo[3] != "null")
                                drSecFuncCat["Active"] = cbInfo[3];

                            if (!isNew)
                            {
                                drSecFuncCat["Modified"] = DateTime.Now;
                                drSecFuncCat["ModifiedBy"] = (Session["EC_User"] as LoginUser).UserName;
                            }
                            else
                            {
                                drSecFuncCat["ID"] = -1;
                                drSecFuncCat["Created"] = DateTime.Now;
                                drSecFuncCat["CreatedBy"] = (Session["EC_User"] as LoginUser).UserName;
                                dtSecFuncCat.Rows.Add(drSecFuncCat);
                            }
                            success = ECSecurityClass.UpdateFunctionCategory(ref message, dtSecFuncCat, drSecFuncCat, false);
                        }
                        gridView.CancelEdit();
                    }
                Session["FC_SecFuncCatTable"] = null;
                LoadSecFuncCatDataTable();
            }
        }
        protected void grdFunction_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (Session["FC_GrdFunctionLoading"] != null && Convert.ToBoolean(Session["FC_GrdFunctionLoading"]))
            {
                Session["FC_GrdFunctionLoading"] = false;
                return;
            }

            ASPxGridView gridView = sender as ASPxGridView;
            gridView.JSProperties["cpFuncActionInfo"] = string.Empty;
            if (gridView.GetMasterRowKeyValue() != null && gridView.GetMasterRowKeyValue().ToString() != string.Empty)
            {
                Session["FC_GrdFunctionLoading"] = true;
                LoadFunctionData(gridView, gridView.GetMasterRowKeyValue());
            }
        }
        private void LoadFunctionData(ASPxGridView gridView, object catId)
        {
            ECWebClass.SetGridViewStyle(gridView);
            LoginUser loginUser = Session["EC_User"] as LoginUser;

            if (gridView.ClientSideEvents.CustomButtonClick == string.Empty)
            {
                gridView.ClientSideEvents.CustomButtonClick = "function(s, e) { " +
                    "if (typeof(grdFunctionCategory) != 'undefined' && (grdFunctionCategory.IsNewRowEditing() || grdFunctionCategory.IsEditing())) { " +
                    "alert('Please save your category changes'); return; } " +
                    "var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); " +
                    "if (btnId == 'btnDeleteFunc') { if (!confirm('Comfirm delete?')) return; s.PerformCallback('Delete|' + id); } else " +
                    "if (btnId == 'btnEditFunc') { s.cpFuncActionInfo = 'Security Function - Edit'; ppCtrlFunction.cpFunctionID = id; " +
                    "cpFunction.PerformCallback('Edit|' + id);} } ";

                ASPxPageControl pcFuncCatDetails = gridView.Parent.NamingContainer as ASPxPageControl;
                if (pcFuncCatDetails != null)
                {
                    ASPxButton btnAddFunction = pcFuncCatDetails.FindControl("btnAddFunction") as ASPxButton;
                    if (btnAddFunction != null)
                    {
                        if (btnAddFunction.ClientSideEvents.Click == string.Empty)
                            btnAddFunction.ClientSideEvents.Click = "function(s, e) { " +
                                "if (typeof(grdFunctionCategory) != 'undefined' && (grdFunctionCategory.IsNewRowEditing() || grdFunctionCategory.IsEditing())) { " +
                                "alert('Please save your category changes'); return; } " +
                                "grdSecurityFunction.cpFuncActionInfo = 'Security Function - New'; ppCtrlFunction.cpFunctionID = -1; " +
                                "cpFunction.PerformCallback('New|" + catId + "'); } ";
                        btnAddFunction.ClientVisible = loginUser != null && loginUser.SpecialUser;
                    }

                    ECWebClass.SetPageControlHeaderStyle(pcFuncCatDetails, false);
                }
            }

            DataTable dtFunction = GetSecFunctionDataTable();
            DataView dvFunction = new DataView(dtFunction);
            if (catId != null && catId.ToString() != string.Empty)
                dvFunction.RowFilter = String.Format("Category = {0}", catId);
            gridView.DataSource = dvFunction;
            gridView.DataSourceID = String.Empty;
            gridView.DataBind();

            gridView.Columns["colFuncAction"].Visible = loginUser != null && loginUser.SpecialUser;
        }
        private DataTable GetSecFunctionDataTable()
        {
            DataTable dtSecFunc = Session["FC_SecFuncTable"] as DataTable;

            if (dtSecFunc == null)
            {
                dtSecFunc = ECSecurityClass.GetSecurityFunctionDataTable(null);
                dtSecFunc.PrimaryKey = new DataColumn[] { dtSecFunc.Columns["ID"] };
                Session["FC_SecFuncTable"] = dtSecFunc;
            }
            return dtSecFunc;
        }
        protected void grdFunction_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            if (Session["EC_User"] == null)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (e.Parameters != null && e.Parameters != "")
            {
                string[] cbInfo = e.Parameters.Split('|');
                DataTable dtFunc = GetSecFunctionDataTable();
                DataRow drFunc = null;
                string errMsg = string.Empty;
                bool success = false;
                object catId = gridView.GetMasterRowKeyValue();

                if (dtFunc != null && Session["FC_FunctionID"] != null)
                {
                    DataTable dtSecCompDataTable = GetGrdSecCompDataTable(Session["FC_FunctionID"]);
                    if (cbInfo[0] == "Delete")
                    {
                        drFunc = dtFunc.Rows.Find(cbInfo[1]);
                        success = drFunc != null && ECSecurityClass.UpdateSecurityFunction(ref errMsg, dtFunc, drFunc, dtSecCompDataTable, true);
                    }
                    else
                        if (cbInfo[0] == "Update")
                        {
                            bool isNew = cbInfo[1] == "-1";
                            if (!isNew)
                                drFunc = dtFunc.Rows.Find(cbInfo[1]);
                            else
                                drFunc = dtFunc.NewRow();
                            drFunc["Name"] = edtName.Value;
                            drFunc["Category"] = catId;
                            if (cbFuncActive.Checked)
                                drFunc["Active"] = 'Y';
                            else
                                drFunc["Active"] = 'N';

                            if (isNew)
                            {
                                drFunc["ID"] = -1;
                                drFunc["Created"] = DateTime.Now;
                                drFunc["CreatedBy"] = (Session["EC_User"] as LoginUser).UserName;
                                dtFunc.Rows.Add(drFunc);
                            }
                            else
                            {
                                drFunc["Modified"] = DateTime.Now;
                                drFunc["ModifiedBy"] = (Session["EC_User"] as LoginUser).UserName;
                            }
                            success = ECSecurityClass.UpdateSecurityFunction(ref errMsg, dtFunc, drFunc, dtSecCompDataTable, false);
                            gridView.CancelEdit();
                        }
                    Session["FC_SecFuncTable"] = null;
                    Session["FC_SecCompTable"] = null;
                    Session["FC_FunctionID"] = -1;
                    LoadFunctionData(gridView, catId);
                }
            }
        }
        protected void grdFuncComponent_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (Session["EC_User"] == null)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (e.Parameters != null && e.Parameters != "")
            {
                string[] cbInfo = e.Parameters.Split('|');
                DataTable dtSecComp = GetGrdSecCompDataTable(Session["FC_FunctionID"]);
                DataRow drSecComp = null;
                if (cbInfo[0] == "Delete")
                {
                    drSecComp = dtSecComp.Rows.Find(cbInfo[1]);
                    if (drSecComp != null)
                        drSecComp["Flag"] = -1;
                }
                else
                    if (cbInfo[0] == "Update")
                    {
                        bool isNew = cbInfo[1] == "null";
                        if (!isNew)
                            drSecComp = dtSecComp.Rows.Find(cbInfo[1]);
                        else
                            drSecComp = dtSecComp.NewRow();
                        if (drSecComp != null)
                        {
                            if (cbInfo[2] != "null")
                                drSecComp["FormName"] = cbInfo[2];
                            else
                                drSecComp["FormName"] = DBNull.Value;
                            if (cbInfo[3] != "null")
                                drSecComp["FrameName"] = cbInfo[3];
                            else
                                drSecComp["FrameName"] = DBNull.Value;
                            if (cbInfo[4] != "null")
                                drSecComp["ComponentName"] = cbInfo[4];
                            else
                                drSecComp["ComponentName"] = DBNull.Value;
                            if (cbInfo[5] != "null")
                                drSecComp["Caption"] = cbInfo[5];
                            else
                                drSecComp["Caption"] = DBNull.Value;
                            if (cbInfo[6] != "null")
                                drSecComp["Url"] = cbInfo[6];
                            else
                                drSecComp["Url"] = DBNull.Value;
                            if (cbInfo[7] != "null")
                                drSecComp["UrlParam"] = cbInfo[7];
                            else
                                drSecComp["UrlParam"] = DBNull.Value;
                            if (cbInfo[8] != "null")
                                drSecComp["Enabled"] = cbInfo[8];
                            if (cbInfo[9] != "null")
                                drSecComp["Visible"] = cbInfo[9];

                            if (!isNew)
                            {
                                drSecComp["Modified"] = DateTime.Now;
                                drSecComp["ModifiedBy"] = (Session["EC_User"] as LoginUser).UserName;
                                if (drSecComp["FromDB"].ToString() == "1" && drSecComp["Flag"].ToString() != "1")
                                    drSecComp["Flag"] = 1;
                            }
                            else
                            {
                                drSecComp["ID"] = Session["FC_SecCompId"];
                                Session["FC_SecCompId"] = Convert.ToInt32(Session["FC_SecCompId"]) - 1;
                                drSecComp["SecurityFunction"] = Session["FC_FunctionID"];
                                drSecComp["Created"] = DateTime.Now;
                                drSecComp["CreatedBy"] = (Session["EC_User"] as LoginUser).UserName;
                                drSecComp["Flag"] = 2;
                                drSecComp["FromDB"] = 0;
                                dtSecComp.Rows.Add(drSecComp);
                            }
                        }
                        gridView.CancelEdit();
                    }
                LoadGrdSecCompData(Session["FC_FunctionID"]);
            }
        }
        protected void cpFunction_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            if (e.Parameter != null && e.Parameter != "")
            {
                ECWebClass.ClearValueFor(ppCtrlFunction);
                string[] cbInfo = e.Parameter.Split('|');
                Session["FC_FunctionID"] = -1;
                Session["FC_SecCompTable"] = null;
                Session["FC_SecCompId"] = -1;

                if (cbInfo[0] == "New")
                {
                    cmbCategory.Value = Convert.ToInt32(cbInfo[1]);
                    cbFuncActive.Checked = true;
                }
                else
                    if (cbInfo[0] == "Edit")
                    {
                        DataTable dtSecFunc = ECSecurityClass.GetSecurityFunctionDataTable(cbInfo[1]);
                        if (dtSecFunc != null && dtSecFunc.Rows.Count > 0)
                        {
                            cmbCategory.Value = dtSecFunc.Rows[0]["Category"];
                            cbFuncActive.Checked = false;
                            if (!(dtSecFunc.Rows[0]["Active"] is DBNull) && dtSecFunc.Rows[0]["Active"].ToString() != string.Empty)
                                cbFuncActive.Checked = Convert.ToChar(dtSecFunc.Rows[0]["Active"]) == 'Y';
                            if (!(dtSecFunc.Rows[0]["Name"] is DBNull) && dtSecFunc.Rows[0]["Name"].ToString() != string.Empty)
                                edtName.Value = dtSecFunc.Rows[0]["Name"];
                            Session["FC_FunctionID"] = dtSecFunc.Rows[0]["ID"];
                        }
                    }
                ECSecurityClass.setReadOnlyFor(cmbCategory, true);
                LoadGrdSecCompData(Session["FC_FunctionID"]);
            }
        }
        protected void grdFuncComponent_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["SecurityFunction"] = Session["FC_FunctionID"];
            e.NewValues["Enabled"] = "Y";
            e.NewValues["Visible"] = "Y";
            e.NewValues["ID"] = -1;
        }
        private DataTable GetGrdSecCompDataTable(object funcId)
        {
            DataTable grdSecCompDataTable = Session["FC_SecCompTable"] as DataTable;
            if (grdSecCompDataTable == null && funcId != null)
            {
                grdSecCompDataTable = ECSecurityClass.GetSecurityComponentDataTable(funcId);
                grdSecCompDataTable.PrimaryKey = new DataColumn[] { grdSecCompDataTable.Columns["ID"] };
                Session["FC_SecCompTable"] = grdSecCompDataTable;
            }
            return grdSecCompDataTable;
        }
        private void LoadGrdSecCompData(object funcId)
        {
            DataTable dtSecCompDataTable = GetGrdSecCompDataTable(funcId);
            DataView dvSecComp = new DataView(dtSecCompDataTable);
            dvSecComp.RowFilter = "Flag <> -1";
            grdFuncComponent.DataSource = dvSecComp;
            grdFuncComponent.DataSourceID = string.Empty;
            grdFuncComponent.DataBind();
        }
        /*
        protected void grdView_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            if (e.CellType != GridViewTableCommandCellType.Data)
                return;

            if (e.ButtonID == "btnEditCategory" || e.ButtonID == "btnDeleteCategory")
            {
                LoginUser loginUser = Session["EC_User"] as LoginUser;
                if (loginUser.SpecialUser)
                    e.Visible = DevExpress.Utils.DefaultBoolean.True;
                else
                    e.Visible = DevExpress.Utils.DefaultBoolean.False;
                if (e.Visible == DevExpress.Utils.DefaultBoolean.True)
                {
                    if (e.IsEditingRow)
                        e.Visible = DevExpress.Utils.DefaultBoolean.False;
                }
            }
        }
         */
    }
}