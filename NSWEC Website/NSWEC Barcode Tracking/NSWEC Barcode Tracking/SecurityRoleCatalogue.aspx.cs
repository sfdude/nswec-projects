﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class SecurityRoleCatalogue : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "SecurityRoleCatalogue.aspx";
            string tgtLogin = string.Empty;
            if (!ECSecurityClass.CheckQueryString(this.Page, "loginId", ref tgtLogin))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            //if (string.Compare(loginUser.UserName, tgtLogin, true) != 0)
            if (loginUser == null || !(loginUser.IsAdmin && string.Compare(loginUser.UserName, tgtLogin, true) == 0))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("RC_RoleCatalogueTable");
                LoginUser loginUser = Session["EC_User"] as LoginUser;
                ASPxButton btnAdd = pcRole.FindControl("btnAdd") as ASPxButton;
                if (btnAdd != null)
                {
                    btnAdd.ClientSideEvents.Click = "function(s, e) { " +
                        "window.location = 'SecurityRoleSetup.aspx?qs=" + ECSecurityClass.GetQSEncripted("roleId=-1") + "'; }";
                    btnAdd.ClientVisible = loginUser != null && loginUser.SpecialUser;
                }

                grdRole.ClientSideEvents.CustomButtonClick = @"function(s, e) { 
                    if (cbRole.InCallback()) return;
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnEdit')
                        cbRole.PerformCallback('Edit|' + id); 
                }";

                cbRole.ClientSideEvents.CallbackComplete = @"function(s, e) {
                    if (e.result != null && e.result != '')
                        window.location = e.result; }";

                pcRole.ShowTabs = false;
                ECWebClass.SetGridViewStyle(grdRole);
            }
            LoadRoleCatalogueData();
        }
        private DataTable GetRoleCatalogueDataTable()
        {
            DataTable dtRole = Session["RC_RoleCatalogueTable"] as DataTable;

            if (dtRole == null)
            {
                dtRole = ECSecurityClass.GetSecurityRoleDataTable(null);
                Session["RC_RoleCatalogueTable"] = dtRole;
            }
            return dtRole;
        }
        private void LoadRoleCatalogueData()
        {
            grdRole.DataSource = GetRoleCatalogueDataTable();
            grdRole.DataSourceID = String.Empty;
            grdRole.DataBind();
        }
        protected void cbRole_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            if (e.Parameter != null && e.Parameter != string.Empty)
            {
                string[] cbInfo = e.Parameter.Split('|');
                if (cbInfo[0] == "Edit")
                    e.Result = "SecurityRoleSetup.aspx?qs=" + ECSecurityClass.GetQSEncripted("roleId=" + cbInfo[1]);
            }
        }
    }
}