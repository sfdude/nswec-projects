﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI.WebControls;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Logout"] != null)  // Set by the Logout hyperlink on the Masterpage Top RHC    
                {
                    // User is logging out
                    if (Request.Cookies["EC_Theme"] != null)
                    {
                        HttpCookie c = new HttpCookie("EC_Theme");
                        c.Expires = DateTime.Now.AddDays(-1d);
                        Response.Cookies.Add(c);
                    }

                    LoginUser loginUser = Session["EC_User"] as LoginUser;
                    string userName = string.Empty;
                    if (loginUser != null)
                    {
                        userName = loginUser.UserName.ToLower();

                        ECSecurityClass.UpdateSessionTokenInvalid(loginUser);
                    }

                    //remove the user object from the session.
                    Hashtable sessions = (Hashtable)Application["WEB_SESSIONS_OBJECT"];
                    if (sessions == null)
                        sessions = new Hashtable();

                    Session.Abandon();
                    //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    if (!string.IsNullOrEmpty(userName))
                        sessions.Remove(userName);
                    
                    Application.Lock();
                    Application["WEB_SESSIONS_OBJECT"] = sessions;
                    Application.UnLock();

                    FormsAuthentication.SignOut();

                    // Session Fixation Internal
                    /*
                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                     */
                    foreach (var cookey in Request.Cookies.AllKeys)
                    {
                        if (cookey == FormsAuthentication.FormsCookieName || cookey.ToLower() == "asp.net_sessionid")

                        {
                            var reqCookie = Request.Cookies[cookey];

                            if (reqCookie != null)
                            {/*
                                Response.Cookies.Add(new HttpCookie(reqCookie.Name, string.Empty));
                                Response.Cookies.Add(new HttpCookie(reqCookie.Name, string.Empty));
                                
                                HttpCookie respCookie = new HttpCookie(reqCookie.Name, reqCookie.Value);
                              reqCookie.Value = string.Empty;
                                reqCookie.Expires = DateTime.Now.AddMonths(-20);
*/
                                HttpCookie respCookie = new HttpCookie(reqCookie.Name);
                                respCookie.Expires = DateTime.Now.AddMinutes(-20);
                                Response.Cookies.Add(respCookie);
                            }
                        }
                    }

                    //create new sessionID
                    /*
                    SessionIDManager manager = new SessionIDManager();
                    manager.RemoveSessionID(System.Web.HttpContext.Current);
                    var newId = manager.CreateSessionID(System.Web.HttpContext.Current);
                    var isRedirected = true;
                    var isAdded = true;
                    manager.SaveSessionID(System.Web.HttpContext.Current, newId, out isRedirected, out isAdded);
                    */
                    //redirect so that the response.cookies call above actually enacts
                    Response.Redirect("~/Login.aspx");

                }
                else   // User is Logging in
                {
                    if ((Request.IsAuthenticated) && (Session["EC_User"] != null))
                        Response.Redirect("~/ConsignmentCatalogue.aspx");  // Standard application access
                    else
                        Response.Redirect("~/Login.aspx");
                }
            }
            else
            {
                Cache["ErrorMessage"] = "Postback should never happen on the Default page - please report to your Administrator";
                Response.Redirect("~/ErrorPage.aspx");
            }
        }
    }
}