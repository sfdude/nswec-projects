﻿<%@ Page Title="User Setup" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="UserSetup.aspx.cs" Inherits="NSWEC_Barcode_Tracking.UserSetup" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="loginContainer">
        <table width="100%" id="tblLogin" runat="server">
        <tr>
            <td align="left" valign="top">
                <dx:ASPxMenu id="menAction" runat="server" ClientInstanceName="menAction" ShowPopOutImages="True" >
                    <border borderstyle="None"></border>
                    <items>
                        <dx:MenuItem Name="btnSave" Text="Save">
                            <Items>
                                <dx:MenuItem Name="btnSaveExit" Text="Save &amp; Exit">
                                </dx:MenuItem>
                            </Items>
                        </dx:MenuItem>
                        <dx:MenuItem Name="btnReset" Text="Reset" Visible="false" >
                        </dx:MenuItem>
                        <dx:MenuItem Name="btnDeleteUser" Text="Delete" >
                        </dx:MenuItem>                   
                    </items>
                </dx:ASPxMenu>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <dx:ASPxPageControl ID="pcUser" ClientInstanceName="pcUser" runat="server" 
                    ActiveTabIndex="0" Width="100%" EnableHierarchyRecreation="False">
                    <TabPages>
                        <dx:TabPage Name="tabUserSetup" Text="User Setup">
                            <ActiveTabStyle Font-Bold="True" HorizontalAlign="Left">
                            </ActiveTabStyle>
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td align="right" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Login Name:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxTextBox ID="edtUserName" runat="server" ClientInstanceName="edtUserName" MaxLength="100" Width="50%">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" ValidateOnLeave="False" >
                                                        <RequiredField IsRequired="True" ErrorText="Login is required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                                <span id="spanAvailability" style="text-align:left"></span>
                                            </td>
                                            <td align="right" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="First Name:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxTextBox ID="edtFName" runat="server" Width="50%" ClientInstanceName="edtFName" MaxLength="30">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Old Password:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxTextBox ID="edtOldPassword" runat="server" ClientInstanceName="edtOldPassword" MaxLength="20" OnPreRender="edtPassword_PreRender" Password="True" Width="50%">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                        <RequiredField ErrorText="Password is required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Last Name:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxTextBox ID="edtLName" runat="server" ClientInstanceName="edtLName" MaxLength="30" Width="50%">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="New Password:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxTextBox ID="edtPassword" runat="server" ClientInstanceName="edtPassword" Width="50%" 
                                                    Password="True" MaxLength="20" OnPreRender="edtPassword_PreRender">
                                                    <ValidationSettings ValidateOnLeave="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                        <RequiredField IsRequired="false" ErrorText="Password is required" />
                                                        <RegularExpression ErrorText="8 or more characters" ValidationExpression="(?=.*[A-Za-z]).{8,}" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Phone:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxTextBox ID="edtPhone" runat="server" ClientInstanceName="edtPhone" MaxLength="20" Width="50%">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Repeat Password:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxTextBox ID="edtPasswordRepeat" runat="server" ClientInstanceName="edtPasswordRepeat" Width="50%" 
                                                    Password="True" MaxLength="20" OnPreRender="edtPassword_PreRender">
                                                    <ValidationSettings ValidateOnLeave="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                        <RequiredField IsRequired="false" ErrorText="Password is required" />
                                                        <RegularExpression ErrorText="8 or more characters" ValidationExpression="(?=.*[A-Za-z]).{8,}" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Email:">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxTextBox ID="edtEmail" runat="server" ClientInstanceName="edtEmail" MaxLength="255" Width="50%">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                                                        <RegularExpression ErrorText="Invalid email address" ValidationExpression="\s*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\s*" />
                                                        <RequiredField ErrorText="Please provide an email address" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right" style="height: 18px" width="15%">
                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Web Page Theme:" ClientVisible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="height: 18px" width="35%">
                                                <dx:ASPxComboBox ID="cmbThemes" runat="server" ClientInstanceName="cmbThemes" Width="50%" ClientVisible="false">
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td align="right" style="height: 18px" width="15%">
                                                &nbsp;</td>
                                            <td style="height: 18px" width="35%">
                                                &nbsp;</td>
                                        </tr>                                       
                                        
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="tabAdditional" Text="Additional Settings">
                            <ActiveTabStyle Font-Bold="True" HorizontalAlign="Left">
                            </ActiveTabStyle>
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td width="15%" align="right">
                                                <dx:ASPxLabel ID="lblPasswordExpiry" runat="server" Text="Password Expiry:" ClientInstanceName="lblPasswordExpiry">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxDateEdit ID="edtPasswordExpiry" runat="server" ClientInstanceName="edtPasswordExpiry" Width="100%">
                                                </dx:ASPxDateEdit>
                                            </td>
                                            <td width="50%" rowspan="5">
                                                <dx:ASPxGridView ID="grdUserRole" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdUserRole" KeyboardSupport="True" KeyFieldName="ID" 
                                                    OnCustomJSProperties="grdUserRole_CustomJSProperties" Width="100%">
                                                    <Columns>
                                                        <dx:GridViewCommandColumn FixedStyle="Left" ShowInCustomizationForm="True" ShowSelectCheckbox="True" VisibleIndex="0" Width="30px">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <HeaderTemplate>
                                                                <dx:ASPxCheckBox ID="cbAll" runat="server" ClientInstanceName="cbAll" OnInit="cbAll_Init">
                                                                </dx:ASPxCheckBox>
                                                            </HeaderTemplate>
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn Caption="Role" FieldName="ID" ShowInCustomizationForm="True" Visible="False" VisibleIndex="2" Width="100px">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Role" FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="3">
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                    <SettingsBehavior EnableRowHotTrack="True" />
                                                    <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="100" />
                                                    <Styles>
                                                        <AlternatingRow Enabled="True">
                                                        </AlternatingRow>
                                                    </Styles>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                                                
                                        <tr>
                                            <td width="15%" align="right">
                                                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Election Manager Office: ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="35%">
                                                <dx:ASPxComboBox ID="cmbRO" runat="server" ClientInstanceName="cmbRO" Width="100%"
                                                    TextFormatString="{0}" DisplayFormatString="{0} - {1}" >
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="CDID" FieldName="Code" />
                                                        <dx:ListBoxColumn FieldName="Name" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                                                
                                        <tr>
                                            <td align="right" width="15%">
                                            </td>
                                            <td width="35%" align="left">
                                                <dx:ASPxCheckBox ID="cbActive" runat="server" CheckState="Unchecked" ClientInstanceName="cbActive" Text="Active">
                                                </dx:ASPxCheckBox>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right" width="15%">&nbsp;</td>
                                            <td align="right" width="35%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="15%">&nbsp;</td>
                                            <td align="right" width="35%">&nbsp;</td>
                                        </tr>
                                        
                                    </table>

                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </td>
        </tr>
        </table>
    </div>
    <dx:ASPxCallback ID="cbLoginCheck" runat="server" ClientInstanceName="cbLoginCheck" OnCallback="cbLoginCheck_Callback" >
    </dx:ASPxCallback>
    <dx:ASPxCallback ID="cbLogin" runat="server" ClientInstanceName="cbLogin" OnCallback="cbLogin_Callback" >
    </dx:ASPxCallback>
</asp:Content>
