﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class SecurityRoleSetup : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "SecurityRoleSetup.aspx";
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
            if (!ECSecurityClass.CheckQueryString(this.Page, "roleId"))
            //    Response.Redirect("ConsignmentCatalogue.aspx");
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("ConsignmentCatalogue.aspx");
                else
                    Response.Redirect("ConsignmentCatalogue.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("RS_SecFuncTable");
                Session.Remove("RS_RoleTable");

                AssignClientSideScripts();
                AssignRoleData();
                LoadSecCatFuncDataTable();
                SelectSecurityFunction();

                pcRoleSetup.ShowTabs = false;
                ECWebClass.SetMenuStyle(menAction);
                ECWebClass.SetGridViewStyle(grdRoleFunc);
                grdRoleFunc.Styles.SelectedRow.BackColor = Color.White;
                grdRoleFunc.Styles.SelectedRow.ForeColor = Color.Black;
                grdRoleFunc.Styles.AlternatingRow.Enabled = DevExpress.Utils.DefaultBoolean.False;
            }
            if (IsCallback)
                LoadSecCatFuncDataTable();
        }
        private void AssignClientSideScripts()
        {
            menAction.ClientSideEvents.ItemClick = "function(s, e) { if (cbRole.InCallback()) return; " +
                "if (e.item.name == 'btnDelete' && !confirm('Comfirm delete?')) return; " +
                "if (e.item.name != 'btnDelete' && !ASPxClientEdit.ValidateEditorsInContainerById('roleContainer')) return; " +
                "cbRole.PerformCallback(e.item.name); } ";
            // redirect using javascripts
            cbRole.ClientSideEvents.CallbackComplete = "function(s, e) { " +
                "if (e.result != null && e.result != '') { if (e.result.indexOf('Error') > -1) {alert(e.result); return; } " +
                "window.location = e.result;}} ";
        }
        private DataTable GetSecCatFuncDataTable()
        {
            DataTable dtFunc = Session["RS_SecFuncTable"] as DataTable;
            if (dtFunc == null)
            {
                dtFunc = ECSecurityClass.GetSecurityCategoryFunctionDataTable();
                Session["RS_SecFuncTable"] = dtFunc;
            }
            return dtFunc;
        }
        private void LoadSecCatFuncDataTable()
        {
            DataTable dtFunc = GetSecCatFuncDataTable();
            grdRoleFunc.DataSource = dtFunc;
            grdRoleFunc.DataSourceID = String.Empty;
            grdRoleFunc.DataBind();
        }
        private object RetrieveRoleIdFromQS()
        {
            object roleId = -1;

            string[] qs = ECSecurityClass.GetQSDecripted(Request.QueryString["qs"]).Split('=');

            if (qs.Length == 2)
                roleId = qs[1];

            return roleId;
        }
        private DataTable GetRoleDataTable()
        {
            DataTable dtRole = Session["RS_RoleTable"] as DataTable;
            if (dtRole == null)
            {
                dtRole = ECSecurityClass.GetSecurityRoleDataTable(RetrieveRoleIdFromQS());
                Session["RS_RoleTable"] = dtRole;
            }
            return dtRole;
        }
        private void AssignRoleData()
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            DataTable dtRole = GetRoleDataTable();
            DataRow drRole;
            bool foundRole = dtRole != null && dtRole.Rows.Count > 0;
            if (foundRole)
            {
                drRole = dtRole.Rows[0];
                if (drRole["Name"] != DBNull.Value && drRole["Name"].ToString() != string.Empty)
                    edtName.Value = drRole["Name"];

                if (drRole["Active"] != DBNull.Value && drRole["Active"].ToString() != string.Empty)
                    cbActive.Checked = Convert.ToChar(drRole["Active"]) == 'Y';
                else
                    cbActive.Checked = false;
            }
            else
            {
                drRole = dtRole.NewRow();
                drRole["ID"] = -1;


                drRole["Active"] = 'Y';
                dtRole.Rows.Add(drRole);


                if (drRole["Active"] != DBNull.Value && drRole["Active"].ToString() != string.Empty)
                    cbActive.Checked = Convert.ToChar(drRole["Active"]) == 'Y';
                else
                    cbActive.Checked = false;
            }
            DevExpress.Web.MenuItem mItem = menAction.Items.FindByName("btnDelete");
            if (mItem != null)
                mItem.ClientVisible = mItem.ClientVisible && foundRole && loginUser.SpecialUser;
            ECSecurityClass.setReadOnlyFor(edtName, !loginUser.SpecialUser);
            

        }
        protected void cbAll_Init(object sender, EventArgs e)
        {
            /*
            ASPxCheckBox chk = sender as ASPxCheckBox;
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            chk.Checked = grid.VisibleRowCount > 0 && grid.VisibleRowCount == grid.Selection.Count;
            if (chk.ClientSideEvents.CheckedChanged == string.Empty)
                chk.ClientSideEvents.CheckedChanged = "function(s, e) { " +
                    "if (s.GetChecked()) " + grid.ClientInstanceName + ".SelectRows(); else " +
                    grid.ClientInstanceName + ".UnselectRows(); } ";
             */
        }
        private void SelectSecurityFunction()
        {
            DataTable dtRole = GetRoleDataTable();
            DataTable dtSecRoleFunc = ECSecurityClass.GetSecurityRoleFunctionDataTable(dtRole.Rows[0]["ID"]);
            if (dtSecRoleFunc != null && dtSecRoleFunc.Rows.Count > 0)
            {
                DataTable dtRoleFunc = grdRoleFunc.DataSource as DataTable;
                DataRow[] drsRoleFunc;
                foreach (DataRow dr in dtRoleFunc.Rows)
                {
                    drsRoleFunc = dtSecRoleFunc.Select("SecurityFunction = " + dr["ID"]);
                    if (drsRoleFunc != null && drsRoleFunc.Length > 0)
                        grdRoleFunc.Selection.SelectRowByKey(dr["ID"]);
                }
            }
        }
        protected void grdRoleFunc_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            LoadSecCatFuncDataTable();
        }
        private void UpdateRoleData()
        {
            DataTable dtRole = GetRoleDataTable();
            if (dtRole != null && dtRole.Rows.Count > 0)
            {
                DataRow drRole = dtRole.Rows[0];
                if (edtName.Value != null && edtName.Value.ToString() != string.Empty)
                    drRole["Name"] = edtName.Value;
                if (cbActive.Checked)
                    drRole["Active"] = 'Y';
                else
                    drRole["Active"] = 'N';
                if (Convert.ToInt32(drRole["ID"]) < 0)
                {
                    drRole["Created"] = DateTime.Now;
                    drRole["CreatedBy"] = (Session["EC_User"] as LoginUser).UserName;
                }
                else
                {
                    drRole["Modified"] = DateTime.Now;
                    drRole["ModifiedBy"] = (Session["EC_User"] as LoginUser).UserName;
                }
            }
        }
        private bool UpdateRoleData(ref string procMsg)
        {
            bool result = false;

            LoginUser loginUser = Session["EC_User"] as LoginUser;

            DataTable dtRole = GetRoleDataTable();
            result = loginUser != null && dtRole != null && dtRole.Rows.Count > 0;
            if (result)
            {
                DataRow drRole = dtRole.Rows[0];

                if (!loginUser.SpecialUser && edtName.Value != null && !(drRole["Name"] is DBNull)
                    && string.Compare(edtName.Value.ToString(), drRole["Name"].ToString(), false) != 0)
                {
                    procMsg = "Invalid record modification";
                    return false;
                }
                if (edtName.Value != null && edtName.Value.ToString() != string.Empty)
                    drRole["Name"] = edtName.Value;
                if (cbActive.Checked)
                    drRole["Active"] = 'Y';
                else
                    drRole["Active"] = 'N';
                if (Convert.ToInt32(drRole["ID"]) < 0)
                {
                    drRole["Created"] = DateTime.Now;
                    drRole["CreatedBy"] = (Session["EC_User"] as LoginUser).UserName;
                }
                else
                {
                    drRole["Modified"] = DateTime.Now;
                    drRole["ModifiedBy"] = (Session["EC_User"] as LoginUser).UserName;
                }
            }
            return result;
        }
        protected void cbRole_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                e.Result = "Error: You're not authorized";
                return;
            }
            if (e.Parameter != null && e.Parameter != "")
            {
                bool success = false;
                string errorMsg = string.Empty;
                string funcList = string.Empty;
                e.Result = "ConsignmentCatalogue.aspx";

                int roleId = -1;

                if (e.Parameter == "btnDelete")
                    success = ECSecurityClass.UpdateSecurityRole(Page, ref errorMsg, ref roleId, GetRoleDataTable(), null, true);
                else
                    if (e.Parameter.Contains("Save"))
                    {
                        success = UpdateRoleData(ref errorMsg);
                        if (!success)
                        {
                            e.Result = "Error: " + errorMsg;
                            return;
                        }
                        DataTable dtRole = GetRoleDataTable();
                        roleId = Convert.ToInt32(dtRole.Rows[0]["ID"]);

                        DataTable dtRoleFunc = grdRoleFunc.DataSource as DataTable;
                        foreach (DataRow dr in dtRoleFunc.Rows)
                        {
                            if (grdRoleFunc.Selection.IsRowSelectedByKey(dr["ID"]))
                                funcList += dr["ID"] + ",";
                        }
                        if (funcList.Length > 0)
                            funcList = funcList.Remove(funcList.Length - 1);
                                                
                        success = ECSecurityClass.UpdateSecurityRole(Page, ref errorMsg, ref roleId, dtRole, funcList, false);
                        if (e.Parameter == "btnSave")
                            e.Result = "SecurityRoleSetup.aspx?qs=" + ECSecurityClass.GetQSEncripted("roleId=" + roleId);

                    }
                if (!success)
                    e.Result = "Error: " + errorMsg;

            }
        }
    }
}