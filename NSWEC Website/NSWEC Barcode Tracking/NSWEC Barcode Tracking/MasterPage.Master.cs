﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        string webTitle = string.Empty;
        protected void Page_PreInit(object sender, EventArgs e)
        {
            /*
            mMain.EnableTheming = false;
            mMain.Theme = null;
             */
            
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                LoginUser loginUser = Session["EC_User"] as LoginUser;
                if (loginUser == null && Page.Title != "Login" &&
                    Page.Title != "Retrieve Password" && Page.Title != "Renewal Password" &&
                    Page.Title != "Email Verification" && Page.Title != "Session Expired")
                {
                    // callback can only use javascript to re direct page
                    if (Page.IsCallback)
                    {
                        DevExpress.Web.ASPxWebControl.RedirectOnCallback("Login.aspx");
                        return;
                    }
                    else
                        Response.Redirect("Login.aspx");
                }
                else
                {
                    DevExpress.Web.MenuItem mItemView = mMain.Items.FindByName("mItemView") as DevExpress.Web.MenuItem;
                    if (mItemView != null)
                    {
                        DevExpress.Web.MenuItem itmTheme = new DevExpress.Web.MenuItem("Theme", "itmTheme");
                        itmTheme.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                        mItemView.Items.Add(itmTheme);
                        DevExpress.Web.MenuItem itmThemeItem;
                        foreach (string theme in ECWebClass.GetThemeList())
                        {
                            itmThemeItem = new DevExpress.Web.MenuItem(theme.Replace("theme", string.Empty), theme);
                            itmThemeItem.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                            itmTheme.Items.Add(itmThemeItem);
                        }
                    }
                    if (Page.Title != "Login" && Page.Title != "Retrieve Password" && Page.Title != "Renewal Password" &&
                        Page.Title != "Email Verification" && Page.Title != "Session Expired")
                        CheckSessionTimeout();
                }
            }
            catch(Exception ex)
            {
                string err = ex.ToString();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                LoginUser loginUser = Session["EC_User"] as LoginUser;
                if (Page.ID != "SessionExpired.aspx" && Page.ID != "Login.aspx" &&
                    Page.ID != "RetrievePassword.aspx" && Page.ID != "EmailVerification.aspx" &&
                    Page.ID != "RenewalPassword.aspx")
                {
                    if (!IsPostBack)
                    {
                        mMain.ClientVisible = Page.Title != "Login" && Page.Title != "Retrieve Password" &&
                            Page.Title != "Renewal Password" && Page.Title != "Email Verification";
                        lblUserStatus.ClientVisible = mMain.ClientVisible;
                        if (loginUser != null)
                        {
                            lblUserStatus.Text = String.Format("You're logged in as {0}", loginUser.UserName);
                            ECSecurityClass.ConfigureMenuForLogin(loginUser, mMain);
                        }
                        DataTable dtApp = ECSecurityClass.GetApplicationDataTable();
                        if (dtApp != null && dtApp.Rows.Count > 0 && !(dtApp.Rows[0]["WebApplicationTitle"] is DBNull) && dtApp.Rows[0]["WebApplicationTitle"].ToString() != string.Empty)
                        {
                            lblWebTitle.Text = dtApp.Rows[0]["WebApplicationTitle"].ToString();
                            lblWebTitle.ForeColor = Color.FromArgb(0, 125, 134);
                        }
                        ECWebClass.SetMainMenuFor(this.Page);
                        ECWebClass.SetRoundPanelStyle(rpBase);
                    }
                }
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                cbCheckLogin.ClientSideEvents.CallbackComplete = @"function(s, e) {  
                    if (e.result != null && e.result != '') {
                        window.location = e.result;
                    }
                }";
                LoginUser loginUser = Session["EC_User"] as LoginUser;

                if (Request.Cookies["AuthToken"] == null)
                {
                    if (Page.IsCallback)
                    {
                        DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                        return;
                    }
                    else
                        Response.Redirect("Default.aspx?Logout=1");
                }

                // Session Fixation Internal
                if (loginUser != null)
                {
                    if (!ECSecurityClass.CheckSessionValid(loginUser) ||
                        !loginUser.SessionId.Equals(Session.SessionID) ||
                        !loginUser.FrmAuthCookie.Equals(Request.Cookies["AuthToken"].Value))
                    {
                        if (Page.IsCallback)
                        {
                            DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                            return;
                        }
                        else
                            Response.Redirect("Default.aspx?Logout=1");
                    }
                    if (!ECSecurityClass.CanAccessPage(loginUser, Page.ID))
                    {
                        if (Page.IsCallback)
                        {
                            DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                            return;
                        }
                        else
                            Response.Redirect("Default.aspx?Logout=1");
                    }
                }
                /*            
                if (!ECSecurityClass.CanAccessPage(loginUser, Page.ID))
                {
                    if (Page.IsCallback)
                        DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                    else
                        Response.Redirect("Default.aspx?Logout=1");
                }
                 */
                Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoStore();
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
            }
        }

        protected void mMain_ItemClick(object source, DevExpress.Web.MenuItemEventArgs e)
        {
            foreach (DevExpress.Web.MenuItem mItem in e.Item.Parent.Items)
            {
                mItem.Checked = false;
                mItem.Selected = false;
            }
            e.Item.Checked = true;
            e.Item.Selected = true;
        }

        private void CheckSessionTimeout()
        {
            string msgSession = @"Warning: Your session will expire in 1 minute. Please save data.";

            //time to remind, 1 minutes before session ends
            int int_MilliSecondsTimeReminder = (Session.Timeout * 60000) - 1 * 60000;
            //time to redirect, 5 milliseconds before session ends
            int int_MilliSecondsTimeOut = (Session.Timeout * 60000) - 5;

            //LoginUser loginUser = Session["EC_User"] as LoginUser;
            //if (loginUser == null)
            //    int_MilliSecondsTimeOut = 0;// (loginUser.SessionTimeOut * 60000) - 5;

            string sessionExpiredPage = "SessionExpired.aspx";
            /*
            string str_Script =
                "var myTimeReminder, myTimeOut; clearTimeout(myTimeReminder); clearTimeout(myTimeOut);" +
                "var sessionTimeReminder = " + int_MilliSecondsTimeReminder + "; " +
                "var sessionTimeout = " + int_MilliSecondsTimeOut + "; " +
                "function doReminder(){ alert('" + msgSession + "'); } " +
                "function doRedirect(){ window.location.href='" + sessionExpiredPage + "'; } " +
                "myTimeReminder=setTimeout('doReminder()', sessionTimeReminder); " +
                "myTimeOut=setTimeout('doRedirect()', sessionTimeout); ";
            */
            string str_Script =
                "var myTimeOut; " +
                "var sessionTimeout = " + int_MilliSecondsTimeOut + "; " +
                "function doRedirect(){ window.location.href='" + sessionExpiredPage + "'; } " +
                @"function resetTimer() { 
                    clearTimeout(myTimeOut); 
                    myTimeOut=setTimeout('doRedirect()', sessionTimeout); 
                }             
                document.onmousemove = resetTimer;
                document.onkeypress = resetTimer; 
                resetTimer(); ";

            String csName = "CheckSessionOut";
            Type csType = Page.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsStartupScriptRegistered(csType, csName))
                cs.RegisterStartupScript(csType, csName, str_Script, true);
        }
        
        protected void cbCheckLogin_Callback(object source, CallbackEventArgs e)
        {
            e.Result = string.Empty;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
            {
                e.Result = "Default.aspx?Logout=1";
            }
        }
    }
}