﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class BarcodeScanSummaryRptParam : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "BarcodeScanSummaryRptParam.aspx";
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime dtTo = DateTime.Today;
                while (dtTo.DayOfWeek != DayOfWeek.Friday)
                {
                    dtTo = dtTo.AddDays(-1);
                }
                edtDateTo.Date = dtTo;
                do
                {
                    dtTo = dtTo.AddDays(-1);

                } while (dtTo.DayOfWeek != DayOfWeek.Saturday);
                edtDateFrom.Date = dtTo;
                
                menAction.ClientSideEvents.ItemClick =
                    @"function(s, e) { 
                        e.processOnServer = false;
                        if (e.item.name == 'btnPrint') { 
                            if (cbEnquiry.InCallback()) return;
                            if (edtDateFrom.GetDate() == null || edtDateTo.GetDate() == null || 
                                edtDateFrom.GetDate() > edtDateTo.GetDate()) { 
                                alert('Invalid date range!'); 
                                edtDateFrom.SetValue(null); edtDateTo.SetValue(null); return; 
                            }
                            e.item.SetText('Wait...'); e.item.SetEnabled(false); 
                            cbEnquiry.PerformCallback(); 
                        } 
                    } ";
                // redirect using javascripts
                cbEnquiry.ClientSideEvents.CallbackComplete =
                    @"function(s, e) {
                    if (e.result != null && e.result != '') { 
                        if (e.result.indexOf('Warning') > -1) {
                            var btnPrint = menAction.GetItemByName('btnPrint'); 
                            if (btnPrint != null) { 
                                btnPrint.SetText('Print'); btnPrint.SetEnabled(true); 
                            } 
                            alert(e.result); return; 
                        }
                        window.location = e.result; 
                    }
                } ";

                ECWebClass.SetMenuStyle(menAction);
                ECWebClass.SetRoundPanelStyle(rpDateRange);
            }
        }

        protected void cbEnquiry_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {           
            e.Result = string.Empty;

            BarcodeLabelReportParam enquiryParam = new BarcodeLabelReportParam();
            enquiryParam.DateFrom = edtDateFrom.Date;
            enquiryParam.DateTo = edtDateTo.Date;
            DataTable dtEnquiry = ECWebClass.GetBarcodeScanSummaryRptData(enquiryParam);
            
            Session["BarcodeScanRptParam"] = enquiryParam;

            if (dtEnquiry != null && dtEnquiry.Rows.Count > 0)
                e.Result = "BarcodeScanSummaryRptViewer.aspx";
            else
                e.Result = "Warning: No data found for this report!";
            /*
            BarcodeScanSummaryReport rpt = new BarcodeScanSummaryReport();
            rpt.DataSource = dtEnquiry;
            rpt.DataMember = dtEnquiry.TableName;

            BarcodeScanReportViewer.Report = rpt;
             */
             
        }
                
    }
}