﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class BarcodeScanSummaryRptViewer : System.Web.UI.Page
    {
        BarcodeLabelReportParam enquiryParam;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "BarcodeScanSummaryRptViewer.aspx";
            enquiryParam = Session["BarcodeScanRptParam"] as BarcodeLabelReportParam;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("BarcodeScanRptDataTable");
                ReportToolbar1.ClientSideEvents.ItemClick =
                    @"function(s, e) { 
                    if(e.item.name =='btnBack') window.location = 'BarcodeScanSummaryRptParam.aspx';
                }";
            }
            DataTable dtBarcodeScanRpt = GetBarcodeScanRptDataTable();
            BarcodeScanSummaryReport rpt = new BarcodeScanSummaryReport();
            rpt.DataSource = dtBarcodeScanRpt;
            rpt.DataMember = dtBarcodeScanRpt.TableName;

            BarcodeScanReportViewer.Report = rpt;

        }
        private DataTable GetBarcodeScanRptDataTable()
        {
            DataTable dt = Session["BarcodeScanRptDataTable"] as DataTable;
            if (dt == null && enquiryParam != null)
            {
                dt = ECWebClass.GetBarcodeScanSummaryRptData(enquiryParam);
                Session["BarcodeScanRptDataTable"] = dt;
            }
            return dt;
        }
    }
}