﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSWEC_Barcode_Tracking
{
    public partial class SessionExpired : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ID = "SessionExpired.aspx";
            Page.Title = "Session Expired";
            Session.RemoveAll();
        }
    }
}