﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class RetrievePassword : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ID = "RetrievePassword.aspx";
            edtLogin.Focus();
        }
        protected void btnSendPass_Click(object sender, EventArgs e)
        {
            string message = "Your login details have been sent to your email address.";
            ASPxButton btnOK = pcMessage.FindControl("btnOK") as ASPxButton;
            if (ECSecurityClass.RetrieveLoginPassword(this.Page, edtLogin.Text.Trim(), ref message))
            {
                lblMessage.Text = message;
                if (btnOK != null)
                    btnOK.Text = "OK";
                pcMessage.HeaderText = "Information";
                pcMessage.ShowOnPageLoad = true;
            }
            else
            {
                lblMessage.Text = message;
                if (btnOK != null)
                    btnOK.Text = "Cancel";
                pcMessage.HeaderText = "Warning";
                pcMessage.ShowOnPageLoad = true;
            }
        }
    }
}