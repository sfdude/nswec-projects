﻿<%@ Page Title="Error Page" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="NSWEC_Barcode_Tracking.ErrorPage" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:aspxroundpanel id="ASPxRoundPanel1" runat="server" headertext="An Error Has Occurred"
        width="100%" Font-Names="Verdana">
        <HeaderStyle Font-Names="Verdana" Font-Size="Large" HorizontalAlign="Left" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server">
                <table width="100%">
                    <tbody>
                        <tr>
                            <td align="left">
                                <dx:ASPxLabel runat="server" 
                                    Text="An unexpected error occurred on our website. The website administrator has been notified" 
                                    ID="lblError"  Font-Names="Verdana" Font-Size="Medium">
                                </dx:ASPxLabel>
                            </td>
                         </tr>
                         <tr><td></td></tr>
                        <tr>
                            <td align="left">
                                <dx:ASPxHyperLink ID="hlHome" runat="server" Font-Names="Verdana" 
                                    Font-Size="Medium" NavigateUrl="~/ConsignmentCatalogue.aspx" 
                                    Text="Return to the homepage">
                                </dx:ASPxHyperLink>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:aspxroundpanel>
</asp:Content>
