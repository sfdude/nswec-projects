﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class ImportContainer : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "ImportContainer.aspx";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("IC_RO_WS");
                Session.Remove("IC_CC_WS");
                Session.Remove("IC_CO_WS");
                AssignClientSideScripts();

                pcImport.ShowTabs = false;
                ECWebClass.SetMenuStyle(menuAction);
                
            }
        }

        private void AssignClientSideScripts()
        {
            menuAction.ClientSideEvents.ItemClick = @"function(s, e) { 
                e.processOnServer = false; 
                if (e.item.name == 'itmImport') { 
                    if (ucSource.InCallback()) return; 
                    if (ucSource.GetText() == '') { 
                        alert('Please select source file!'); 
                        ucSource.Focus(); return;
                    }
                    if (edtROWorksheet.GetText() == '' && edtCCWorksheet.GetText() == '' && edtConnoteWorksheet.GetText() == '') { 
                        alert('Please define one of the worksheet name!');
                        return;
                    }
                    e.item.SetText('Wait...'); 
                    e.item.SetEnabled(false); 
                    cbImport.PerformCallback(edtROWorksheet.GetText() + '|' + edtCCWorksheet.GetText() + '|' + edtConnoteWorksheet.GetText());  
                }
            } ";
            
            cbImport.ClientSideEvents.CallbackComplete = @"function(s, e) { 
                if (e.result != null && e.result == 'WorkSheetAssigned') 
                    ucSource.Upload();
                else { 
                    s.SetText('Import'); 
                    s.SetEnabled(true); 
                } } ";
            
            ucSource.ClientSideEvents.FileUploadComplete = @"function(s, e) { 
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null) { 
                    itmImport.SetText('Import'); 
                    itmImport.SetEnabled(true); 
                }
                if (e.callbackData != '') 
                    alert(e.callbackData); 
            } ";

        }

        protected void cbImport_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            Session.Remove("IC_RO_WS");
            Session.Remove("IC_CC_WS");
            Session.Remove("IC_CO_WS");
            e.Result = string.Empty;
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string[] cbInfo = e.Parameter.Split('|');
                Session["IC_RO_WS"] = cbInfo[0];
                Session["IC_CC_WS"] = cbInfo[1];
                Session["IC_CO_WS"] = cbInfo[2];
                e.Result = "WorkSheetAssigned";
            }
        }
        protected void ucSource_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            e.CallbackData = "Uploaded election data file is invalid";
            if (e.IsValid)
            {
                try
                {
                    LoginUser loginUser = Session["EC_User"] as LoginUser;

                    string confFolder = MapPath(ConfigurationManager.AppSettings["ImportFolder"]);
                    string fileExtension = System.IO.Path.GetExtension(e.UploadedFile.FileName).ToLower();
                    string GUIDId = Guid.NewGuid().ToString().ToUpper();
                    string fileGUIDName = String.Format("{0}\\{1}{2}", confFolder, GUIDId, fileExtension);
                    e.UploadedFile.SaveAs(fileGUIDName);
                    
                    string procMsg = string.Empty;
                    string procROMsg = string.Empty;
                    string procCCMsg = string.Empty;
                    string procCOMsg = string.Empty;
                    string connStr = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=Excel 12.0;", fileGUIDName);

                    OleDbDataAdapter adapter = null;
                    if (Session["IC_RO_WS"] != null && Session["IC_RO_WS"].ToString() != string.Empty)
                    {
                        adapter = new OleDbDataAdapter(string.Format("SELECT * FROM [{0}$]", Session["IC_RO_WS"]), connStr);
                        DataTable dtROSrc = new DataTable();
                        adapter.Fill(dtROSrc);
                        if (ECWebClass.ImportReturningOfficeData(loginUser, dtROSrc, ref procMsg))
                            procROMsg = "Import Returning Office Data is successful";
                        else
                            if (!string.IsNullOrEmpty(procMsg))
                                procROMsg = string.Format("Import Returning Office Data is failed with error: {0}", procMsg);
                            else
                                procROMsg = "Import Returning Office Data is failed";
                    } /*
                    else
                        ECWebClass.DeleteTableData("NSW_ReturningOffice", ref procROMsg);
                       */

                    if (Session["IC_CC_WS"] != null && Session["IC_CC_WS"].ToString() != string.Empty)
                    {
                        adapter = new OleDbDataAdapter(string.Format("SELECT * FROM [{0}$]", Session["IC_CC_WS"]), connStr);
                        DataTable dtCCSrc = new DataTable();
                        adapter.Fill(dtCCSrc);
                        if (ECWebClass.ImportCountCentreData(loginUser, dtCCSrc, ref procMsg))
                            procCCMsg = "Import Count Centre Data is successful";
                        else
                            if (!string.IsNullOrEmpty(procMsg))
                                procCCMsg = string.Format("Import Count Centre Data is failed with error: {0}", procMsg);
                            else
                                procCCMsg = "Import Count Centre Data is failed";
                    }
                    if (Session["IC_CO_WS"] != null && Session["IC_CO_WS"].ToString() != string.Empty)
                    {                        
                        adapter = new OleDbDataAdapter(string.Format("SELECT * FROM [{0}$]", Session["IC_CO_WS"]), connStr);
                        DataTable dtConnoteSrc = new DataTable();
                        adapter.Fill(dtConnoteSrc);

                        if (ECWebClass.ImportConsignmentData(loginUser, dtConnoteSrc, ref procMsg))
                            procCOMsg = "Import Consignment Data is successful";
                        else
                            if (!string.IsNullOrEmpty(procMsg))
                                procCOMsg = string.Format("Import Consignment Data is failed with error: {0}", procMsg);
                            else
                                procCOMsg = "Import Consignment Data is failed";                        
                    }
                    procMsg = string.Empty;
                    if (!string.IsNullOrEmpty(procROMsg))
                        procMsg += string.Format("{0}{1}", procROMsg, Environment.NewLine);
                    if (!string.IsNullOrEmpty(procCCMsg))
                        procMsg += string.Format("{0}{1}", procCCMsg, Environment.NewLine);
                    if (!string.IsNullOrEmpty(procCOMsg))
                        procMsg += string.Format("{0}{1}", procCOMsg, Environment.NewLine);
                    e.CallbackData = procMsg.Trim();
                }
                catch (Exception ex)
                {
                    e.CallbackData = string.Format("Error in File Upload: {0}", ex.Message);
                }
            }
        }
    }
}