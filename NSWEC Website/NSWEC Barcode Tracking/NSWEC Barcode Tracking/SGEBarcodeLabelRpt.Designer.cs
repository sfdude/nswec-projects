﻿namespace NSWEC_Barcode_Tracking
{
    partial class SGEBarcodeLabelRpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.statusDetailSubRpt = new DevExpress.XtraReports.UI.DetailBand();
            this.uldSubRpt = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLblULD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.statusSubRpt = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLblDeactivated = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblScanned = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblLocation = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblContainer = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelProduct = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblProduct = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblVenueType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblVenue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelContestArea = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblContestArea = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDistrict = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblDistrict = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblEMO = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelEMO = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDB_BarCode = new DevExpress.XtraReports.UI.XRBarCode();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLblTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCompTotalConnote = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // statusDetailSubRpt
            // 
            this.statusDetailSubRpt.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.uldSubRpt,
            this.xrLblULD,
            this.xrLine1,
            this.statusSubRpt,
            this.xrLblDeactivated,
            this.xrLabel9,
            this.xrLblStatus,
            this.xrLabel10,
            this.xrLblScanned,
            this.xrLblLocation,
            this.xrLabel7,
            this.xrLabel8,
            this.xrLblContainer,
            this.xrLabelProduct,
            this.xrLblProduct,
            this.xrLblVenueType,
            this.xrLabel6,
            this.xrLblVenue,
            this.xrLabel4,
            this.xrLabelContestArea,
            this.xrLblContestArea,
            this.xrLabelDistrict,
            this.xrLblDistrict,
            this.xrLblEMO,
            this.xrLabelEMO,
            this.xrDB_BarCode});
            this.statusDetailSubRpt.Dpi = 100F;
            this.statusDetailSubRpt.HeightF = 160.375F;
            this.statusDetailSubRpt.KeepTogether = true;
            this.statusDetailSubRpt.Name = "statusDetailSubRpt";
            this.statusDetailSubRpt.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.statusDetailSubRpt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.statusDetailSubRpt.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // uldSubRpt
            // 
            this.uldSubRpt.CanShrink = true;
            this.uldSubRpt.Dpi = 100F;
            this.uldSubRpt.LocationFloat = new DevExpress.Utils.PointFloat(63.27111F, 135.625F);
            this.uldSubRpt.Name = "uldSubRpt";
            this.uldSubRpt.ReportSource = new NSWEC_Barcode_Tracking.ULDDetailRpt();
            this.uldSubRpt.SizeF = new System.Drawing.SizeF(935.7289F, 23F);
            this.uldSubRpt.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.uldSubRpt_BeforePrint);
            // 
            // xrLblULD
            // 
            this.xrLblULD.Dpi = 100F;
            this.xrLblULD.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblULD.ForeColor = System.Drawing.Color.Navy;
            this.xrLblULD.LocationFloat = new DevExpress.Utils.PointFloat(89.70842F, 90.00006F);
            this.xrLblULD.Name = "xrLblULD";
            this.xrLblULD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblULD.SizeF = new System.Drawing.SizeF(171.3751F, 20F);
            this.xrLblULD.StylePriority.UseFont = false;
            this.xrLblULD.StylePriority.UseForeColor = false;
            this.xrLblULD.StylePriority.UseTextAlignment = false;
            this.xrLblULD.Text = "xrLblULD";
            this.xrLblULD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(988.9996F, 6.999969F);
            // 
            // statusSubRpt
            // 
            this.statusSubRpt.CanShrink = true;
            this.statusSubRpt.Dpi = 100F;
            this.statusSubRpt.LocationFloat = new DevExpress.Utils.PointFloat(63.27083F, 112.625F);
            this.statusSubRpt.Name = "statusSubRpt";
            this.statusSubRpt.ReportSource = new NSWEC_Barcode_Tracking.StatusDetailRpt();
            this.statusSubRpt.SizeF = new System.Drawing.SizeF(935.7289F, 23F);
            this.statusSubRpt.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.statusSubRpt_BeforePrint);
            // 
            // xrLblDeactivated
            // 
            this.xrLblDeactivated.Dpi = 100F;
            this.xrLblDeactivated.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblDeactivated.ForeColor = System.Drawing.Color.Red;
            this.xrLblDeactivated.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 90.00006F);
            this.xrLblDeactivated.Name = "xrLblDeactivated";
            this.xrLblDeactivated.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblDeactivated.SizeF = new System.Drawing.SizeF(79.70843F, 20F);
            this.xrLblDeactivated.StylePriority.UseFont = false;
            this.xrLblDeactivated.StylePriority.UseForeColor = false;
            this.xrLblDeactivated.StylePriority.UseTextAlignment = false;
            this.xrLblDeactivated.Text = "Deactivated";
            this.xrLblDeactivated.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(276.0417F, 90.00006F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(168.5416F, 20F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Last Scanned Status:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblStatus
            // 
            this.xrLblStatus.Dpi = 100F;
            this.xrLblStatus.LocationFloat = new DevExpress.Utils.PointFloat(444.5833F, 90.00006F);
            this.xrLblStatus.Name = "xrLblStatus";
            this.xrLblStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblStatus.SizeF = new System.Drawing.SizeF(554.4164F, 20F);
            this.xrLblStatus.StylePriority.UseTextAlignment = false;
            this.xrLblStatus.Text = "xrLblStatus";
            this.xrLblStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(703.1299F, 70.00005F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(112.7448F, 20F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Last Scanned:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblScanned
            // 
            this.xrLblScanned.Dpi = 100F;
            this.xrLblScanned.LocationFloat = new DevExpress.Utils.PointFloat(815.8749F, 70.00004F);
            this.xrLblScanned.Name = "xrLblScanned";
            this.xrLblScanned.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblScanned.SizeF = new System.Drawing.SizeF(183.1249F, 20F);
            this.xrLblScanned.StylePriority.UseTextAlignment = false;
            this.xrLblScanned.Text = "xrLblScanned";
            this.xrLblScanned.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblLocation
            // 
            this.xrLblLocation.Dpi = 100F;
            this.xrLblLocation.LocationFloat = new DevExpress.Utils.PointFloat(444.5833F, 70.00005F);
            this.xrLblLocation.Name = "xrLblLocation";
            this.xrLblLocation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblLocation.SizeF = new System.Drawing.SizeF(258.5466F, 20F);
            this.xrLblLocation.StylePriority.UseTextAlignment = false;
            this.xrLblLocation.Text = "xrLblLocation";
            this.xrLblLocation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(276.0417F, 70.00005F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(168.5416F, 20F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Last Scanned Location:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(703.1299F, 50.00003F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(112.7448F, 20F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Container:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblContainer
            // 
            this.xrLblContainer.Dpi = 100F;
            this.xrLblContainer.LocationFloat = new DevExpress.Utils.PointFloat(815.8747F, 50.00003F);
            this.xrLblContainer.Name = "xrLblContainer";
            this.xrLblContainer.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblContainer.SizeF = new System.Drawing.SizeF(183.1251F, 20F);
            this.xrLblContainer.StylePriority.UseTextAlignment = false;
            this.xrLblContainer.Text = "xrLblContainer";
            this.xrLblContainer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelProduct
            // 
            this.xrLabelProduct.Dpi = 100F;
            this.xrLabelProduct.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelProduct.LocationFloat = new DevExpress.Utils.PointFloat(276.0417F, 50.00003F);
            this.xrLabelProduct.Name = "xrLabelProduct";
            this.xrLabelProduct.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelProduct.SizeF = new System.Drawing.SizeF(68.24997F, 20F);
            this.xrLabelProduct.StylePriority.UseFont = false;
            this.xrLabelProduct.StylePriority.UseTextAlignment = false;
            this.xrLabelProduct.Text = "Product:";
            this.xrLabelProduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblProduct
            // 
            this.xrLblProduct.Dpi = 100F;
            this.xrLblProduct.LocationFloat = new DevExpress.Utils.PointFloat(344.2917F, 50.00003F);
            this.xrLblProduct.Name = "xrLblProduct";
            this.xrLblProduct.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblProduct.SizeF = new System.Drawing.SizeF(358.8381F, 20F);
            this.xrLblProduct.StylePriority.UseTextAlignment = false;
            this.xrLblProduct.Text = "xrLblProduct";
            this.xrLblProduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblVenueType
            // 
            this.xrLblVenueType.Dpi = 100F;
            this.xrLblVenueType.LocationFloat = new DevExpress.Utils.PointFloat(815.8747F, 30.00001F);
            this.xrLblVenueType.Name = "xrLblVenueType";
            this.xrLblVenueType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblVenueType.SizeF = new System.Drawing.SizeF(183.1251F, 20F);
            this.xrLblVenueType.StylePriority.UseTextAlignment = false;
            this.xrLblVenueType.Text = "xrLblVenueType";
            this.xrLblVenueType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(703.1299F, 29.99999F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(112.7448F, 20F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Venue Type:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblVenue
            // 
            this.xrLblVenue.Dpi = 100F;
            this.xrLblVenue.LocationFloat = new DevExpress.Utils.PointFloat(344.2917F, 29.99999F);
            this.xrLblVenue.Name = "xrLblVenue";
            this.xrLblVenue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblVenue.SizeF = new System.Drawing.SizeF(358.8382F, 20F);
            this.xrLblVenue.StylePriority.UseTextAlignment = false;
            this.xrLblVenue.Text = "xrLblVenue";
            this.xrLblVenue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(276.0417F, 29.99999F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(68.24994F, 20F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Venue:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelContestArea
            // 
            this.xrLabelContestArea.Dpi = 100F;
            this.xrLabelContestArea.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelContestArea.LocationFloat = new DevExpress.Utils.PointFloat(703.1298F, 10.00001F);
            this.xrLabelContestArea.Name = "xrLabelContestArea";
            this.xrLabelContestArea.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelContestArea.SizeF = new System.Drawing.SizeF(112.7451F, 20F);
            this.xrLabelContestArea.StylePriority.UseFont = false;
            this.xrLabelContestArea.StylePriority.UseTextAlignment = false;
            this.xrLabelContestArea.Text = "Contest Area:";
            this.xrLabelContestArea.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblContestArea
            // 
            this.xrLblContestArea.Dpi = 100F;
            this.xrLblContestArea.LocationFloat = new DevExpress.Utils.PointFloat(815.8749F, 10F);
            this.xrLblContestArea.Name = "xrLblContestArea";
            this.xrLblContestArea.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblContestArea.SizeF = new System.Drawing.SizeF(183.125F, 20F);
            this.xrLblContestArea.StylePriority.UseTextAlignment = false;
            this.xrLblContestArea.Text = "xrLblContestArea";
            this.xrLblContestArea.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelDistrict
            // 
            this.xrLabelDistrict.Dpi = 100F;
            this.xrLabelDistrict.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelDistrict.LocationFloat = new DevExpress.Utils.PointFloat(526.0417F, 10F);
            this.xrLabelDistrict.Name = "xrLabelDistrict";
            this.xrLabelDistrict.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelDistrict.SizeF = new System.Drawing.SizeF(54.16669F, 20F);
            this.xrLabelDistrict.StylePriority.UseFont = false;
            this.xrLabelDistrict.StylePriority.UseTextAlignment = false;
            this.xrLabelDistrict.Text = "District:";
            this.xrLabelDistrict.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblDistrict
            // 
            this.xrLblDistrict.Dpi = 100F;
            this.xrLblDistrict.LocationFloat = new DevExpress.Utils.PointFloat(580.2083F, 10.00001F);
            this.xrLblDistrict.Name = "xrLblDistrict";
            this.xrLblDistrict.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblDistrict.SizeF = new System.Drawing.SizeF(122.9216F, 20F);
            this.xrLblDistrict.StylePriority.UseTextAlignment = false;
            this.xrLblDistrict.Text = "xrLblDistrict";
            this.xrLblDistrict.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblEMO
            // 
            this.xrLblEMO.Dpi = 100F;
            this.xrLblEMO.LocationFloat = new DevExpress.Utils.PointFloat(344.2917F, 10.00001F);
            this.xrLblEMO.Name = "xrLblEMO";
            this.xrLblEMO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblEMO.SizeF = new System.Drawing.SizeF(181.75F, 20F);
            this.xrLblEMO.StylePriority.UseTextAlignment = false;
            this.xrLblEMO.Text = "xrLblEMO";
            this.xrLblEMO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelEMO
            // 
            this.xrLabelEMO.Dpi = 100F;
            this.xrLabelEMO.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelEMO.LocationFloat = new DevExpress.Utils.PointFloat(276.0417F, 10.00001F);
            this.xrLabelEMO.Name = "xrLabelEMO";
            this.xrLabelEMO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelEMO.SizeF = new System.Drawing.SizeF(68.25F, 20F);
            this.xrLabelEMO.StylePriority.UseFont = false;
            this.xrLabelEMO.StylePriority.UseTextAlignment = false;
            this.xrLabelEMO.Text = "EMO:";
            this.xrLabelEMO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrDB_BarCode
            // 
            this.xrDB_BarCode.AutoModule = true;
            this.xrDB_BarCode.Dpi = 100F;
            this.xrDB_BarCode.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10F);
            this.xrDB_BarCode.Name = "xrDB_BarCode";
            this.xrDB_BarCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
            this.xrDB_BarCode.SizeF = new System.Drawing.SizeF(276.0417F, 80.00005F);
            this.xrDB_BarCode.StylePriority.UseTextAlignment = false;
            code128Generator1.CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetAuto;
            this.xrDB_BarCode.Symbology = code128Generator1;
            this.xrDB_BarCode.Text = "110000000121";
            this.xrDB_BarCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 50F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 49F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox2,
            this.xrPictureBox1,
            this.xrLblTitle});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 61.06339F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseBorders = false;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox2.Dpi = 100F;
            this.xrPictureBox2.ImageUrl = "~/images/Smart-Freight_Horizontal-logo_RGB.png";
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(835.0409F, 0F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(163.9587F, 51.06339F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrPictureBox2.StylePriority.UseBorders = false;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.ImageUrl = "~/images/NSWEC_Logo_Stacked_rgb_Colour_Positive_L.png";
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(173.9583F, 61.06339F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrPictureBox1.StylePriority.UseBorders = false;
            // 
            // xrLblTitle
            // 
            this.xrLblTitle.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Double;
            this.xrLblTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblTitle.BorderWidth = 2F;
            this.xrLblTitle.Dpi = 100F;
            this.xrLblTitle.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblTitle.LocationFloat = new DevExpress.Utils.PointFloat(293.75F, 8.355062F);
            this.xrLblTitle.Name = "xrLblTitle";
            this.xrLblTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblTitle.SizeF = new System.Drawing.SizeF(409.3799F, 42.70833F);
            this.xrLblTitle.StylePriority.UseBorderDashStyle = false;
            this.xrLblTitle.StylePriority.UseBorders = false;
            this.xrLblTitle.StylePriority.UseBorderWidth = false;
            this.xrLblTitle.StylePriority.UseFont = false;
            this.xrLblTitle.StylePriority.UseTextAlignment = false;
            this.xrLblTitle.Text = "BallotTrack Label Report";
            this.xrLblTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1,
            this.xrLine2});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 31F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 100F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo2.Format = "Printed by BallotTrack on {0:dd/MM/yyyy h:mm tt}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 6.999969F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(398.5833F, 23.00002F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Page {0} of {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(821.0414F, 6.999969F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(177.9584F, 23.00002F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 100F;
            this.xrLine2.LineWidth = 2;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(998.9996F, 6.999969F);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel12,
            this.lblCompTotalConnote,
            this.xrLabel11,
            this.xrLine3});
            this.ReportFooter.Dpi = 100F;
            this.ReportFooter.HeightF = 36.45833F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 100F;
            this.xrLabel12.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 6.999969F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(89.70803F, 26.45832F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Summary:";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblCompTotalConnote
            // 
            this.lblCompTotalConnote.Dpi = 100F;
            this.lblCompTotalConnote.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompTotalConnote.LocationFloat = new DevExpress.Utils.PointFloat(916.4631F, 6.999969F);
            this.lblCompTotalConnote.Name = "lblCompTotalConnote";
            this.lblCompTotalConnote.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCompTotalConnote.SizeF = new System.Drawing.SizeF(82.53638F, 26.45833F);
            this.lblCompTotalConnote.StylePriority.UseFont = false;
            this.lblCompTotalConnote.StylePriority.UseTextAlignment = false;
            this.lblCompTotalConnote.Text = "0";
            this.lblCompTotalConnote.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 100F;
            this.xrLabel11.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(703.1298F, 6.999969F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(213.3332F, 26.45832F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Total Number of Barcodes:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 100F;
            this.xrLine3.LineWidth = 2;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0.0004132589F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(998.9991F, 6.999969F);
            // 
            // SGEBarcodeLabelRpt
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.statusDetailSubRpt,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.ReportFooter});
            this.DefaultPrinterSettingsUsing.UseLandscape = true;
            this.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margins = new System.Drawing.Printing.Margins(50, 51, 50, 49);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Version = "15.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SGEBarcodeLabelRpt_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand statusDetailSubRpt;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLblTitle;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRBarCode xrDB_BarCode;
        private DevExpress.XtraReports.UI.XRLabel xrLblEMO;
        private DevExpress.XtraReports.UI.XRLabel xrLabelEMO;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDistrict;
        private DevExpress.XtraReports.UI.XRLabel xrLblDistrict;
        private DevExpress.XtraReports.UI.XRLabel xrLabelContestArea;
        private DevExpress.XtraReports.UI.XRLabel xrLblContestArea;
        private DevExpress.XtraReports.UI.XRLabel xrLblVenueType;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLblVenue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLblContainer;
        private DevExpress.XtraReports.UI.XRLabel xrLabelProduct;
        private DevExpress.XtraReports.UI.XRLabel xrLblProduct;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLblScanned;
        private DevExpress.XtraReports.UI.XRLabel xrLblLocation;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLblStatus;
        private DevExpress.XtraReports.UI.XRLabel xrLblDeactivated;
        private DevExpress.XtraReports.UI.XRSubreport statusSubRpt;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLblULD;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lblCompTotalConnote;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRSubreport uldSubRpt;
    }
}
