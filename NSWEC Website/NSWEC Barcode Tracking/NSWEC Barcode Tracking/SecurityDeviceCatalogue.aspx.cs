﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class SecurityDeviceCatalogue : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "SecurityDeviceCatalogue.aspx";
            string tgtLogin = string.Empty;
            if (!ECSecurityClass.CheckQueryString(this.Page, "loginId", ref tgtLogin))
                //Response.Redirect("Default.aspx?Logout=1");
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            //if (!loginUser.IsAdmin || string.Compare(loginUser.UserName, tgtLogin, true) != 0)
            if (loginUser == null || !(loginUser.IsAdmin && string.Compare(loginUser.UserName, tgtLogin, true) == 0))
                //Response.Redirect("Default.aspx?Logout=1");
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("SDC_DeviceTable");
                grdSecDev.ClientSideEvents.CustomButtonClick =
                    @"function(s, e) {
                        var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                        if (btnId == 'btnEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                            s.StartEditRow(e.visibleIndex); 
                        } else 
                        if (btnId == 'btnDelete') {
                            if (confirm('Comfirm delete?')) 
                                s.PerformCallback('Delete|' + id); 
                        } else
                        if (btnId == 'btnUpdate') {
                            if(!ASPxClientEdit.ValidateEditorsInContainerById('grdSecDev')) return; 
                            var code = s.GetEditValue('DeviceId'); var name = s.GetEditValue('Name');
                            var active = s.GetEditValue('Active'); 
                            s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + active); 
                        } else
                        if (btnId == 'btnCancel') {
                            s.CancelEdit();
                        }
                    }";
                btnAdd.ClientSideEvents.Click =
                    @"function(s, e) { 
                        if (!grdSecDev.IsNewRowEditing() && !grdSecDev.IsEditing()) 
                            grdSecDev.AddNewRow();
                    }";

                pcSecDev.ShowTabs = false;
                ECWebClass.SetGridViewStyle(grdSecDev);
            }
            LoadSecurityDeviceData();
        }
        private void LoadSecurityDeviceData()
        {
            grdSecDev.DataSource = GetSecurityDeviceDataTable();
            grdSecDev.DataSourceID = String.Empty;
            grdSecDev.DataBind();
        }
        private DataTable GetSecurityDeviceDataTable()
        {
            DataTable dt = Session["SDC_DeviceTable"] as DataTable;
            if (dt == null)
            {
                dt = ECSecurityClass.GetSecurityDeviceData(null);
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["SDC_DeviceTable"] = dt;
            }
            return dt;
        }
        protected void grdView_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Update ||
                e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Cancel)
                e.Visible = false;
        }

        protected void grdView_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;

            if (loginUser == null || !loginUser.IsAdmin)
            {
                //DevExpress.Web.ASPxWebControl.RedirectOnCallback("Login.aspx");
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            if (e.CellType != GridViewTableCommandCellType.Data)
                return;
            ASPxGridView gridView = sender as ASPxGridView;

            if (e.ButtonID == "btnDelete")
            {
                //LoginUser loginUser = Session["EC_User"] as LoginUser;
                if (loginUser.SpecialUser)
                    e.Visible = DevExpress.Utils.DefaultBoolean.True;
                else
                    e.Visible = DevExpress.Utils.DefaultBoolean.False;
                if (e.Visible == DevExpress.Utils.DefaultBoolean.True)
                {
                    if (e.IsEditingRow)
                        e.Visible = DevExpress.Utils.DefaultBoolean.False;
                }
            }
        }

        protected void grdView_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            // explicitely bind for the required column, if there is no data in gridview -- ie, for inserting the first record
            // and in detail row template
            ASPxGridView gridView = sender as ASPxGridView;
            if (!gridView.IsNewRowEditing && gridView.IsEditing)
            {
                if (e.Column.FieldName == "DeviceId")
                    e.Editor.ReadOnly = true;
            }
        }
        protected void grdView_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetSecurityDeviceDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Delete")
                {
                    drData = dtData.Rows.Find(cbInfo[1]);
                    success = ECWebClass.UpdateTableRecordById("SecurityDevice", drData, false, ref errMsg, true); 
                }
                else
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["DeviceId"] = cbInfo[2];
                    else
                        drData["DeviceId"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["Active"] = cbInfo[4];
                    else
                        drData["Active"] = "N";                    

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = (Session["EC_User"] as LoginUser).UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = (Session["EC_User"] as LoginUser).UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("SecurityDevice", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("SDC_DeviceTable");
                LoadSecurityDeviceData();
            }
        }
        protected void grdView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["ID"] = Guid.NewGuid().ToString().ToUpper();
            e.NewValues["Active"] = "Y";
        }
    }
}