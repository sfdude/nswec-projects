﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Security;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ID = "Login.aspx";
            ecLogin.Focus();
        }
        protected void Login_Authenticate(object sender, AuthenticateEventArgs e)
        {
            lblloginMessage.Text = string.Empty;
            if (ValidateUser(ecLogin.UserName.ToLower(), ecLogin.Password))
            {
                string strUsername = ecLogin.UserName.ToLower();
                FormsAuthentication.Initialize();
                String strRole = AssignRoles(strUsername);

                //The AddMinutes determines how long the user will be logged in after leaving
                //the site if he doesn't log off.
                FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1,
                    strUsername, DateTime.Now, DateTime.Now.AddMinutes(30), false, strRole,
                    FormsAuthentication.FormsCookiePath);
                Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName,
                    FormsAuthentication.Encrypt(fat)));
                Response.Redirect("ConsignmentCatalogue.aspx");
            }
        }
        private static String AssignRoles(String strUsername)
        {
            //Return a | separated list of roles this user is a member of
            if (strUsername == "ec")
                return "bigboss|wimpyuser";
            else
                return String.Empty;
        }
        private bool ValidateUser(string userName, string passWord)
        {
            bool result = false;
            string processMsg = string.Empty;

            LoginUser loginUser = ECSecurityClass.GetLoginUser(userName, passWord, ref processMsg);
            result = loginUser != null;
            if (result)
            {
                //getting the sessions objects from the Application
                Hashtable sessions = (Hashtable)Application["WEB_SESSIONS_OBJECT"];
                if (sessions == null)
                    sessions = new Hashtable();

                //getting the pointer to the Session of the current logged in user
                HttpSessionState existingUserSession = (HttpSessionState)sessions[userName.ToLower()];
                //logout current logged in user
                if (existingUserSession != null)
                    existingUserSession["EC_User"] = null;

                //putting the user in the session
                Session["EC_User"] = loginUser;
                if (loginUser.SessionTimeOut > 0)
                    Session.Timeout = loginUser.SessionTimeOut;
                sessions[loginUser.UserName.ToLower()] = Session;
                Application.Lock(); //lock to prevent duplicate objects
                Application["WEB_SESSIONS_OBJECT"] = sessions;
                Application.UnLock();

                if (!string.IsNullOrEmpty(processMsg))
                    Session["LoginInfoMessage"] = processMsg;
            }
            else
            {
                ecLogin.FailureText = string.Empty;
                if (!string.IsNullOrEmpty(processMsg))
                {
                    if (processMsg.Contains("Your login password has expired and must be changed"))
                        Response.Redirect("RenewalPassword.aspx");
                    else
                        lblloginMessage.Text = processMsg;
                }
                else
                    lblloginMessage.Text = "Your login attempt was not successful. Please try again.";
            }
            return result;
        }
    }
}