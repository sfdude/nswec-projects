﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace NSWEC_Barcode_Tracking
{
    public partial class SGEBarcodeLabelRpt : DevExpress.XtraReports.UI.XtraReport
    {
        private bool _isLGA = false;
        public SGEBarcodeLabelRpt(bool isLGA=false)
        {
            InitializeComponent();
            _isLGA = isLGA;
            if (_isLGA)
            {
                xrLabelEMO.Text = "RO";
                xrLabelDistrict.Text = "LGA";
                xrLabelContestArea.Text = "Ward";
                xrLabelProduct.Text = "Contest";
            }
        }

        private void SGEBarcodeLabelRpt_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrDB_BarCode.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".Barcode", ""));
            lblCompTotalConnote.Summary.Func = SummaryFunc.Count;
            lblCompTotalConnote.Summary.Running = SummaryRunning.Report;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            object emoCode = GetCurrentColumnValue("EMOCode");
            if (emoCode != null && !string.IsNullOrEmpty(emoCode.ToString()))
                xrLblEMO.Text = string.Format("{0}({1})", GetCurrentColumnValue("EMOName"), emoCode);
            else
                xrLblEMO.Text = string.Empty;

            object districtCode = GetCurrentColumnValue("DistrictCode");
            if (districtCode != null && !string.IsNullOrEmpty(districtCode.ToString()))
                xrLblDistrict.Text = string.Format("{0}({1})", GetCurrentColumnValue("DistrictName"), districtCode);
            else
                xrLblDistrict.Text = string.Empty;

            if (_isLGA)
            {
                object contestAreaCode = GetCurrentColumnValue("WardCode");
                if (contestAreaCode != null && !string.IsNullOrEmpty(contestAreaCode.ToString()))
                    xrLblContestArea.Text = string.Format("{0}({1})", GetCurrentColumnValue("WardName"), contestAreaCode);
                else
                    xrLblContestArea.Text = string.Empty;                
            }
            else
            {
                object contestAreaCode = GetCurrentColumnValue("ContestAreaId");
                if (contestAreaCode != null && !string.IsNullOrEmpty(contestAreaCode.ToString()))
                    xrLblContestArea.Text = string.Format("{0}({1})", GetCurrentColumnValue("ContestAreaCode"), contestAreaCode);
                else
                    xrLblContestArea.Text = string.Empty;
            }

            object venueCode = GetCurrentColumnValue("VenueCode");
            if (venueCode != null && !string.IsNullOrEmpty(venueCode.ToString()))
                xrLblVenue.Text = string.Format("{0}({1})", GetCurrentColumnValue("VenueName"), venueCode);
            else
                xrLblVenue.Text = string.Empty;

            object venueTypeCode = GetCurrentColumnValue("VenueTypeCode");
            if (venueTypeCode != null && !string.IsNullOrEmpty(venueTypeCode.ToString()))
                xrLblVenueType.Text = string.Format("{0}({1})", GetCurrentColumnValue("VenueTypeName"), venueTypeCode);
            else
                xrLblVenueType.Text = string.Empty;

            object productCode = GetCurrentColumnValue("ProductCode");
            if (productCode != null && !string.IsNullOrEmpty(productCode.ToString()))
                xrLblProduct.Text = string.Format("{0}({1})", GetCurrentColumnValue("ProductName"), productCode);
            else
                xrLblProduct.Text = string.Empty;

            object containerCode = GetCurrentColumnValue("ContainerCode");
            if (containerCode != null && !string.IsNullOrEmpty(containerCode.ToString()))
                xrLblContainer.Text = string.Format("{0}({1})", GetCurrentColumnValue("ContainerName"), containerCode);
            else
                xrLblContainer.Text = string.Empty;

            object locationCode = GetCurrentColumnValue("LocationCode");
            if (locationCode != null && !string.IsNullOrEmpty(locationCode.ToString()))
                xrLblLocation.Text = string.Format("{0}({1})", GetCurrentColumnValue("LocationName"), locationCode);
            else
                xrLblLocation.Text = string.Empty;

            object scannedTime = GetCurrentColumnValue("StatusDate");
            if (scannedTime != null && !string.IsNullOrEmpty(scannedTime.ToString()))
                xrLblScanned.Text = Convert.ToDateTime(scannedTime).ToString("dd/MM/yyyy HH:mm:ss");
            else
                xrLblScanned.Text = string.Empty;

            object statusNotes = GetCurrentColumnValue("StatusNotes");
            if (statusNotes != null && !string.IsNullOrEmpty(statusNotes.ToString()))
                xrLblStatus.Text = string.Format("{0}", statusNotes);
            else
                xrLblStatus.Text = string.Empty;

            object deactivated = GetCurrentColumnValue("Deactivated");
            xrLblDeactivated.Visible = deactivated != null && deactivated.ToString() != string.Empty && Convert.ToChar(deactivated) == 'Y';

            object uldParent = GetCurrentColumnValue("ParentBarcode");
            if (uldParent != null && !string.IsNullOrEmpty(uldParent.ToString()))
                xrLblULD.Text = string.Format("In ULD({0})", uldParent);
            else
                xrLblULD.Text = string.Empty;
        }

        private void statusSubRpt_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport subRpt = sender as XRSubreport;

            DataTable dtSrc = ((StatusDetailRpt)((XRSubreport)sender).ReportSource).DataSource as DataTable;
            subRpt.Visible = dtSrc != null && dtSrc.Rows.Count > 0;

            if (subRpt.Visible)
            {
                object consKey = GetCurrentColumnValue("ConsKey");
                object lineNo = GetCurrentColumnValue("LineNo");

                DataRow[] drsSrc;
                if (consKey != null && !string.IsNullOrEmpty(consKey.ToString()) && lineNo != null && !string.IsNullOrEmpty(lineNo.ToString()))
                    drsSrc = dtSrc.Select(string.Format("ConsKey='{0}' AND [LineNo]={1}", consKey, lineNo));
                else
                    drsSrc = null;
                subRpt.Visible = drsSrc != null && drsSrc.Length > 0;
                if (subRpt.Visible)
                {
                    //((StatusDetailRpt)((XRSubreport)sender).ReportSource).ConsKey.Value = Convert.ToInt32(GetCurrentColumnValue("ConsKey"));
                    ((StatusDetailRpt)((XRSubreport)sender).ReportSource).ConsKey.Value = consKey;
                    ((StatusDetailRpt)((XRSubreport)sender).ReportSource).LineNo.Value = lineNo;
                }
            }
        }

        private void uldSubRpt_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport subRpt = sender as XRSubreport;

            DataTable dtSrc = ((ULDDetailRpt)((XRSubreport)sender).ReportSource).DataSource as DataTable;
            subRpt.Visible = dtSrc != null && dtSrc.Rows.Count > 0;

            if (subRpt.Visible)
            {
                object consKey = GetCurrentColumnValue("ConsKey");
                object lineNo = GetCurrentColumnValue("LineNo");

                DataRow[] drsSrc;
                if (consKey != null && !string.IsNullOrEmpty(consKey.ToString()) && lineNo != null && !string.IsNullOrEmpty(lineNo.ToString()))
                    drsSrc = dtSrc.Select(string.Format("ConsKey='{0}' AND [LineNo]={1}", consKey, lineNo));
                else
                    drsSrc = null;

                subRpt.Visible = drsSrc != null && drsSrc.Length > 0;
                if (subRpt.Visible)
                {
                    ((ULDDetailRpt)((XRSubreport)sender).ReportSource).ConsKey.Value = consKey;
                    ((ULDDetailRpt)((XRSubreport)sender).ReportSource).LineNo.Value = lineNo;
                    ((ULDDetailRpt)((XRSubreport)sender).ReportSource).IsLGA.Value = _isLGA;                    
                }
            }
        }
    }
}
