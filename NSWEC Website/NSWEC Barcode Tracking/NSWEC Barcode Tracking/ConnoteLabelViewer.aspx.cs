﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web;
using DevExpress.XtraPrinting;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class ConnoteLabelViewer : System.Web.UI.Page
    {
        string barcodeStr = string.Empty;
        string labelIdSpec = string.Empty;

        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "ConnoteLabelViewer.aspx";

            if (Session["EC_User"] == null)
            {
                if (Page.IsCallback)
                {
                    /*
                    Response.Write("<script>  window.location.href='Login.aspx'; </script>");
                    Response.End();
                     */
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Login.aspx");
                    return;
                }
                else
                    Response.Redirect("Login.aspx");
            }
            if (Request.QueryString["qs"] != null)
            {
                string qsStr = Request.QueryString["qs"];
                qsStr = ECSecurityClass.GetQSDecripted(qsStr);
                if (!string.IsNullOrEmpty(qsStr))
                {
                    string[] qs = qsStr.Split('=');
                    barcodeStr = qs[1];
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (!IsPostBack)
            {
                string pageId = Guid.NewGuid().ToString().ToUpper();// +".";
                hfHiddenInfo.Add("PageId", pageId);
                loginUser.AddLabelPageId(this.Page, pageId);

                btnPrint.ClientSideEvents.Click = @"function(s, e) { LabelViewer.Print(); }";
                try
                {
                    object labelIdDefault = -1;
                    string spDetaultName = "GetDefaultConnoteLabels";
                    DataTable dtLabelLayOut = ECWebClass.GetLabelLayoutDef(null);
                    if (dtLabelLayOut != null && dtLabelLayOut.Rows.Count > 0)
                    {
                        foreach (DataRow drLabelLayOut in dtLabelLayOut.Rows)
                        {
                            if (!(drLabelLayOut["LABEL_NAME"] is DBNull) && drLabelLayOut["LABEL_NAME"].ToString() == "Generic Label")
                            {
                                labelIdDefault = drLabelLayOut["ID"];
                                if (!(drLabelLayOut["STORED_PROCEDURE"] is DBNull) && drLabelLayOut["STORED_PROCEDURE"].ToString() != string.Empty)
                                    spDetaultName = drLabelLayOut["STORED_PROCEDURE"].ToString();
                                break;
                            }
                        }
                    }
                    int labelId = -1;
                    if (!string.IsNullOrEmpty(labelIdSpec))
                    {
                        int labelIdNum;
                        if (int.TryParse(labelIdSpec, out labelIdNum))
                            labelId = labelIdNum;
                    }
                    else
                    {
                        DataTable dtApp = ECSecurityClass.GetApplicationDataTable();
                        if (dtApp != null && dtApp.Rows.Count > 0)
                        {
                            if (!(dtApp.Rows[0]["PreferredLabel"] is DBNull) && dtApp.Rows[0]["PreferredLabel"].ToString() != null)
                            {
                                labelIdSpec = dtApp.Rows[0]["PreferredLabel"].ToString();
                                int labelIdNum;
                                if (int.TryParse(labelIdSpec, out labelIdNum))
                                    labelId = labelIdNum;
                            }
                        }
                    }
                    if (labelId > 0)
                    {
                        string spName = spDetaultName;
                        DataTable dtLayout = ECWebClass.GetLabelLayoutDef(labelId);
                        if (dtLayout != null && dtLayout.Rows.Count > 0 && !(dtLayout.Rows[0]["LAYOUT"] is DBNull))
                        {
                            Session[string.Format("{0}.LabelLayoutDataTable", hfHiddenInfo.Get("PageId"))] = dtLayout;
                            if (!(dtLayout.Rows[0]["STORED_PROCEDURE"] is DBNull) && dtLayout.Rows[0]["STORED_PROCEDURE"].ToString() != string.Empty)
                                spName = dtLayout.Rows[0]["STORED_PROCEDURE"].ToString();
                        }
                        DataSet dsLabel = GetConnoteLabelDataSet(spName, barcodeStr);
                        Session[string.Format("{0}.ConnoteLabelSPName", hfHiddenInfo.Get("PageId"))] = spName;

                        bool labelFound = dsLabel != null && dsLabel.Tables.Count > 0 && dsLabel.Tables[0].Rows.Count > 0;
                        lblError.ClientVisible = !labelFound;
                        btnPrint.ClientVisible = labelFound;
                        if (!labelFound)
                        {
                            Session[string.Format("{0}.ConnoteLabelDataSet", hfHiddenInfo.Get("PageId"))] = null;
                            Session[string.Format("{0}.LabelLayoutDataTable", hfHiddenInfo.Get("PageId"))] = null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.ToString();
                    lblError.ClientVisible = true;
                }
            }
            LoadLabelReport();
        }
        private DataSet GetConnoteLabelDataSet(string spName, object barcode)
        {
            DataSet ds = Session[string.Format("{0}.ConnoteLabelDataSet", hfHiddenInfo.Get("PageId"))] as DataSet;
            if (ds == null)
            {
                ds = new DataSet("DataSet");
                DataTable dt = ECWebClass.GetConnoteLabelData(spName, barcode);
                ds.Tables.Add(dt);
                Session[string.Format("{0}.ConnoteLabelDataSet", hfHiddenInfo.Get("PageId"))] = ds;
            }
            return ds;
        }
        private void LoadLabelReport()
        {
            if (Session[string.Format("{0}.LabelLayoutDataTable", hfHiddenInfo.Get("PageId"))] != null &&
            Session[string.Format("{0}.ConnoteLabelDataSet", hfHiddenInfo.Get("PageId"))] != null)
            {
                try
                {
                    DataTable dtLayout = Session[string.Format("{0}.LabelLayoutDataTable", hfHiddenInfo.Get("PageId"))] as DataTable;
                    DataSet dsLabelData = Session[string.Format("{0}.ConnoteLabelDataSet", hfHiddenInfo.Get("PageId"))] as DataSet;
                    
                    //using (XtraReport report = new XtraReport())
                    XtraReport report = new XtraReport();
                    using (MemoryStream ms = new MemoryStream(dtLayout.Rows[0]["LAYOUT"] as byte[]))
                    {
                        report.LoadLayout(ms);

                        report.DataSource = dsLabelData;
                        report.DataMember = dsLabelData.Tables[0].TableName;

                        LabelViewer.Report = report;
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.ToString();
                    lblError.ClientVisible = true;
                }
            }
        }
    }
}