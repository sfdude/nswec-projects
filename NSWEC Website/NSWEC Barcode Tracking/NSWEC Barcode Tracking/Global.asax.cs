﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Net;
using System.Web.SessionState;
using System.Security.Principal;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string usrIp = HttpContext.Current.Request.UserHostAddress;
            if (!ECSecurityClass.IsIPAllowed(usrIp))
            {
                //Response.Write(string.Format("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html lang=\"en\" xml:lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"><head><title>Request Denied</title><style type=\"text/css\">body {{font-family: Arial, Helvetica, Verdana, Sans-Serif;font-size: small;font-weight: normal;color: #000000;}}div {{margin-left: auto;margin-right: auto;text-align: center;}}.box {{width: 600px;background-color: #F2F2F2;border-left: solid 1px #C2C2C2;border-right: solid 1px #C2C2C2;vertical-align: middle;padding: 20px 10px 20px 10px;}}p {{text-align: left;}}.red {{font-weight: bold;color: Red;text-align: center;}}.band {{height: 20px;color: White;background: rgb(30, 181, 146);width: 600px;border-left: solid 1px #333333;border-right: solid 1px #333333;padding: 3px 10px 0px 10px;}}div#wrap {{margin-top: 50px;}}</style></head><body><div id=\"wrap\"><div class=\"band\"></div><div class=\"box\"><p class=\"red\">Request denied by SmartFreight.</p><p><b> Reason: </b>{0} - Invalid IP Address Range</p></div><div class=\"band\"></div></div></body></html>", usrIp));
                Response.Write(ECSecurityClass.REDIRECT_BLOCKED_HTML1);
                Response.End();
            }
            int clCount = HttpContext.Current.Request.Headers.AllKeys.Count(s => s.Equals("content-length", StringComparison.CurrentCultureIgnoreCase));
            int teCount = HttpContext.Current.Request.Headers.AllKeys.Count(s => s.Equals("transfer-encoding", StringComparison.CurrentCultureIgnoreCase));
            if ((clCount > 0 && teCount > 0) || clCount > 1)
            {
                Response.StatusCode = 400;
                Response.End();
            }
            /*
            string sPagePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oFileInfo = new System.IO.FileInfo(sPagePath);
            string sPageName = oFileInfo.Name;

            if (!string.IsNullOrEmpty(sPageName) && string.Compare(sPageName, "Login.aspx", true) == 0)
            {                
                if (Request.Form.Keys.Count > 0)
                {
                    string loginName = string.Empty;
                    foreach (string k in Request.Form.Keys)
                    {
                        if (k.Contains("rpLogin$ASPxCallbackPanel1$smLogin$UserName"))
                        {
                            loginName = Request.Form[k];
                            //loginName = Request.Form[k];// System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(Request.Unvalidated.Form[k], false);
                            //Text='<%#System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode((string)Eval("YourText"), false)%>' 
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(loginName) && ECSecurityClass.LoginUserLocked(loginName))
                    {
                        Response.Write(ECSecurityClass.REDIRECT_BLOCKED_HTML);
                        Response.End();
                    }
                }
            } 
             * */
        }
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            //Fires upon attempting to authenticate the use

            if (!(HttpContext.Current.User == null))
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (HttpContext.Current.User.Identity.GetType() == typeof(FormsIdentity))
                    {
                        FormsIdentity fi = (FormsIdentity)HttpContext.Current.User.Identity;
                        FormsAuthenticationTicket fat = fi.Ticket;

                        String[] astrRoles = fat.UserData.Split('|');
                        HttpContext.Current.User = new GenericPrincipal(fi, astrRoles);
                    }
                }
            }

        }
    }
}