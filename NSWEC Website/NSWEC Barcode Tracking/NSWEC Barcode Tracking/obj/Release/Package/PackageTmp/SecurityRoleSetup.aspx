﻿<%@ Page Title="Security Role Setup" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SecurityRoleSetup.aspx.cs" Inherits="NSWEC_Barcode_Tracking.SecurityRoleSetup" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:ASPxPageControl ID="pcRoleSetup" runat="server" ActiveTabIndex="0" Height="450px"
        Width="100%" EnableCallBacks="True">
        <TabPages>
            <dx:TabPage Name="tabRoleSetup" Text="Role Setup">
                <ActiveTabStyle Font-Bold="True"></ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div id="roleContainer">
                            <table width="100%" id="tblRole" runat="server">
                                <tr>
                                    <td align="left" valign="top" colspan="3">
                                        <dx:ASPxMenu id="menAction" runat="server" ClientInstanceName="menAction" ShowPopOutImages="True" >
                                            <border borderstyle="None"></border>
                                            <items>
                                                <dx:MenuItem Name="btnSave" Text="Save">
                                                    <Items>
                                                        <dx:MenuItem Name="btnSaveExit" Text="Save &amp; Exit">
                                                        </dx:MenuItem>
                                                    </Items>
                                                </dx:MenuItem>
                                                <dx:MenuItem Name="btnDelete" Text="Delete" >
                                                </dx:MenuItem>                      
                                            </items>
                                        </dx:ASPxMenu>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 5%">
                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Name">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td align="left" style="width: 20%">
                                        <dx:ASPxTextBox ID="edtName" runat="server" ClientInstanceName="edtName" Width="200px">
                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" ValidateOnLeave="False">
                                                <RequiredField IsRequired="True" ErrorText="Name is required" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>                
                                    </td>                                    
                                    
                                    <td align="left" style="width: 55%">            
                                        &nbsp;
                                    </td>
                                    <td align="right">
                                        <dx:ASPxCheckBox ID="cbActive" runat="server" ClientInstanceName="cbActive" Text="Active">
                                        </dx:ASPxCheckBox>
                                    </td>
                                </tr>
                
                                <tr>
                                    <td align="left" colspan="4">
                
                                        <dx:ASPxGridView ID="grdRoleFunc" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdRoleFunc"
                                            KeyboardSupport="True" KeyFieldName="ID" Width="100%" 
                                            OnCustomCallback="grdRoleFunc_CustomCallback">
                                            <Columns>
                                                <dx:GridViewCommandColumn FixedStyle="Left" ShowSelectCheckbox="True" VisibleIndex="0"
                                                    Width="30px">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <HeaderTemplate>
                                                        <dx:ASPxCheckBox ID="cbAll" runat="server" ClientInstanceName="cbAll" Visible="false" OnInit="cbAll_Init">
                                                        </dx:ASPxCheckBox>
                                                    </HeaderTemplate>
                                                </dx:GridViewCommandColumn>
                                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="1" Width="100px">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Category" FieldName="CategoryName" GroupIndex="0"
                                                    SortIndex="0" SortOrder="Ascending" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Function" FieldName="Name" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                       
                                            </Columns>
                                            <SettingsBehavior ColumnResizeMode="Control" EnableRowHotTrack="True" AutoExpandAllGroups="True" />
                                            <Settings ShowHeaderFilterButton="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="300" />
                                            <Styles>
                                                <AlternatingRow Enabled="True">
                                                </AlternatingRow>
                                                <SelectedRow BackColor="White" ForeColor="Black">
                                                </SelectedRow>
                                            </Styles>
                                            <SettingsLoadingPanel Mode="Disabled" />
                                            <SettingsPager PageSize="15">
                                            </SettingsPager>
                                        </dx:ASPxGridView>
            
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        <ContentStyle HorizontalAlign="Left">
        </ContentStyle>
    </dx:ASPxPageControl>
    
    <dx:ASPxCallback ID="cbRole" runat="server" ClientInstanceName="cbRole" OnCallback="cbRole_Callback" >
    </dx:ASPxCallback>
</asp:Content>
