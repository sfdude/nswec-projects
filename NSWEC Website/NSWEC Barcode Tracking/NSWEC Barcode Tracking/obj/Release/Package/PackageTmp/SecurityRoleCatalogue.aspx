﻿<%@ Page Title="Security Role Catalogue" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SecurityRoleCatalogue.aspx.cs" Inherits="NSWEC_Barcode_Tracking.SecurityRoleCatalogue" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:ASPxCallback ID="cbRole" runat="server" ClientInstanceName="cbRole" OnCallback="cbRole_Callback">
    </dx:ASPxCallback>
    <dx:ASPxPageControl ID="pcRole" runat="server" ActiveTabIndex="0" Height="450px"
        Width="100%" EnableCallBacks="True">
        <TabPages>
            <dx:TabPage Name="tabRoleCatalogue" Text="Role Catalogue">
                <ActiveTabStyle Font-Bold="True"></ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxButton ID="btnAdd" runat="server" ClientInstanceName="btnAdd" EnableDefaultAppearance="False" Font-Underline="True"
                            ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False">
                            <Border BorderStyle="None" />
                        </dx:ASPxButton>
                        <dx:ASPxGridView ID="grdRole" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdRole"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%" >
                            <Columns>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction"
                                    VisibleIndex="0" Width="70px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnEdit" Text="Edit">
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Name" FieldName="Name" FixedStyle="Left" VisibleIndex="1" >
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="Active" VisibleIndex="2" Width="70px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" AutoExpandAllGroups="True" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" ShowFilterRowMenu="True" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        <ContentStyle HorizontalAlign="Left">
        </ContentStyle>
    </dx:ASPxPageControl>
</asp:Content>
