﻿<%@ Page Title="Consignment" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Consignment.aspx.cs" Inherits="NSWEC_Barcode_Tracking.Consignment" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:ASPxPageControl ID="pcConnote" runat="server" ActiveTabIndex="0" ClientInstanceName="pcConnote" Width="100%">
        <TabPages>
            <dx:TabPage Name="tabConnote" Text="Consignment">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table style="width:100%;">                            
                            <tr>
                                <td width="33%">
                                    <dx:ASPxLabel ID="lblEMO" runat="server" Text="Election Manager Office">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="33%">
                                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Venue">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="34%">
                                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Venue Type">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%">
                                    <dx:ASPxTextBox ID="edtRO" runat="server" ClientInstanceName="edtRO" Width="80%" ReadOnly="True">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                                <td width="33%">
                                    <dx:ASPxTextBox ID="edtVenue" runat="server" Width="80%" ReadOnly="True">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                                <td width="34%">
                                    <dx:ASPxTextBox ID="edtVenueType" runat="server" Width="80%" ReadOnly="True">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                           
                            <tr>
                                <td width="33%">
                                    <dx:ASPxLabel ID="lblDistrict" runat="server" Text="District">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="33%">
                                    <dx:ASPxLabel ID="lblContestArea" runat="server" Text="Contest Area">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="34%">
                                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Container Quantity">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%">
                                    <dx:ASPxTextBox ID="edtLGA" runat="server" ClientInstanceName="edtLGA" ReadOnly="True" Width="80%">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                                <td width="33%">
                                    <dx:ASPxTextBox ID="edtWard" runat="server" ClientInstanceName="edtWard" ReadOnly="True" Width="80%">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                                <td width="34%">
                                    <dx:ASPxSpinEdit ID="edtContainerQty" runat="server" HorizontalAlign="Right" Number="0" NumberType="Integer" Width="80%" ReadOnly="True">
                                        <SpinButtons ShowIncrementButtons="False">
                                        </SpinButtons>
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxSpinEdit>
                                </td>
                            </tr>
                            
                            <tr>
                                <td width="33%">
                                    <dx:ASPxLabel ID="lblProductType" runat="server" Text="Product Type">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="33%">
                                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Container Type">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="34%">
                                    <%-- 
                                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Additional Quantity">
                                    </dx:ASPxLabel>    
                                    --%>
                                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Source">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%">
                                    <dx:ASPxTextBox ID="edtContest" runat="server" ClientInstanceName="edtContest" ReadOnly="True" Width="80%">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                                <td width="33%">
                                    <dx:ASPxTextBox ID="edtContainerType" runat="server" ReadOnly="True" Width="80%">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                                <td width="34%">
                                    <%--
                                    <dx:ASPxSpinEdit ID="edtAdditionalQty" runat="server" HorizontalAlign="Right" Number="0" NumberType="Integer" ReadOnly="True" Width="80%">
                                        <SpinButtons ShowIncrementButtons="False">
                                        </SpinButtons>
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxSpinEdit>
                                    --%>
                                    <dx:ASPxTextBox ID="edtSource" runat="server" ClientInstanceName="edtSource" ReadOnly="True" Width="80%">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%">   
                                    <dx:ASPxLabel ID="lblCountCentre" runat="server" Text="Count Centre">
                                    </dx:ASPxLabel>                                 
                                </td>
                                <td width="33%">                                    
                                </td>
                                <td width="34%">
                                    <dx:ASPxLabel ID="lblDelivNum" runat="server" Text="Delivery Number">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%">                                    
                                    <dx:ASPxTextBox ID="edtCountCentre" runat="server" ClientInstanceName="edtCountCentre" ReadOnly="True" Width="80%">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxTextBox>
                                </td>
                                <td width="33%">                                    
                                </td>
                                <td width="34%">
                                    <dx:ASPxSpinEdit ID="edtDelivNum" runat="server" HorizontalAlign="Right" Number="0" NumberType="Integer" Width="80%" ReadOnly="True">
                                        <SpinButtons ShowIncrementButtons="False">
                                        </SpinButtons>
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                    </dx:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="3">
                                    <dx:ASPxPageControl ID="pcContainer" runat="server" ActiveTabIndex="0" ClientInstanceName="pcContainer" Width="100%" Height="450px">
                                        <TabPages>
                                            <dx:TabPage Name="tabContainer" Text="Container Details">
                                                <ActiveTabStyle Font-Bold="True">
                                                </ActiveTabStyle>
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxButton ID="btnAddContainer" runat="server" 
                                                            ClientInstanceName="btnAddContainer" EnableDefaultAppearance="False" Font-Underline="True" ClientVisible="false"
                                                            ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False" ToolTip="New Container">
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxButton>
                                                        <dx:ASPxGridView ID="grdContainer" runat="server" ClientInstanceName="grdContainer" Width="100%"
                                                            KeyboardSupport="True" KeyFieldName="LineNo"
                                                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                                                            OnCustomButtonInitialize="grdContainer_CustomButtonInitialize"
                                                            OnInitNewRow="grdContainer_InitNewRow"
                                                            OnCustomCallback="grdContainer_CustomCallback" OnDataBound="grdContainer_DataBound" >
                                                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                                                            <Templates>
                                                                <DetailRow>
                                                                    <dx:ASPxPageControl ID="pcContainerStatus" runat="server" ActiveTabIndex="0" Width="100%">
                                                                        <TabPages>
                                                                            <dx:TabPage Name="tabContainerStatus" Text="Container Status">
                                                                                <ActiveTabStyle Font-Bold="True">
                                                                                </ActiveTabStyle>
                                                                                <ContentCollection>
                                                                                    <dx:ContentControl runat="server">
                                                                                        <dx:ASPxButton ID="btnAddContainerStatus" runat="server" 
	                                                                                        ClientInstanceName="btnAddContainerStatus" EnableDefaultAppearance="False" Font-Underline="True" ClientVisible="false"
	                                                                                        ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False" ToolTip="New Container Status">
	                                                                                        <Border BorderStyle="None" />
                                                                                        </dx:ASPxButton>
                                                                                        <dx:ASPxGridView ID="grdContainerStatus" runat="server" ClientInstanceName="grdContainerStatus" Width="100%" AutoGenerateColumns="False"
                                                                                            KeyboardSupport="True" KeyFieldName="LineNo;StatusId"
                                                                                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                                                                                            OnCustomCallback="grdContainerStatus_CustomCallback"
                                                                                            OnBeforePerformDataSelect="grdContainerStatus_BeforePerformDataSelect" Settings-HorizontalScrollBarMode="Auto">
                                                                                            <Columns>
                                                                                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="0" Width="100px">
                                                                                                    <CustomButtons>
                                                                                                        <dx:GridViewCommandColumnCustomButton ID="btnEditStatus" Text="Edit">
                                                                                                            <Styles>
                                                                                                                <Style ForeColor="White">
                                                                                                                </Style>
                                                                                                            </Styles>
                                                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                                                        <dx:GridViewCommandColumnCustomButton ID="btnUpdateStatus" Text="Update" Visibility="EditableRow">
                                                                                                            <Styles>
                                                                                                                <Style ForeColor="White">
                                                                                                                </Style>
                                                                                                            </Styles>
                                                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                                                        <dx:GridViewCommandColumnCustomButton ID="btnCancelStatus" Text="Cancel" Visibility="EditableRow">
                                                                                                            <Styles>
                                                                                                                <Style ForeColor="White">
                                                                                                                </Style>
                                                                                                            </Styles>
                                                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                                                    </CustomButtons>
                                                                                                    <CellStyle BackColor="#007D86">
                                                                                                    </CellStyle>
                                                                                                </dx:GridViewCommandColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="ConsKey" Visible="False" VisibleIndex="1">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="LineNo" Visible="False" VisibleIndex="2">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="StatusId" Visible="False" VisibleIndex="3">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataDateColumn FieldName="StatusDate" Caption="Date" ReadOnly="true"
                                                                                                    Width="80px" VisibleIndex="4" SortIndex="0" SortOrder="Descending">
                                                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy" DropDownButton-ClientVisible="false">
                                                                                                        <DropDownButton ClientVisible="False">
                                                                                                        </DropDownButton>
                                                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                                                        <Style HorizontalAlign="Right" ></Style>
                                                                                                    </PropertiesDateEdit>
                                                                                                </dx:GridViewDataDateColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="StatusTime" Caption="Time" Width="80px" ReadOnly="true" VisibleIndex="5">
                                                                                                    <PropertiesTextEdit>
                                                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                                                    </PropertiesTextEdit>
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="StatusNotes" Caption="Notes" VisibleIndex="6">
                                                                                                    <PropertiesTextEdit MaxLength="500"> </PropertiesTextEdit>
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="MovementDirection" Caption="Movement" VisibleIndex="7">
                                                                                                    <PropertiesTextEdit MaxLength="500"> </PropertiesTextEdit>
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="LocationName" Caption="Location" VisibleIndex="8">
                                                                                                    <PropertiesTextEdit MaxLength="100"> </PropertiesTextEdit>
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataDateColumn FieldName="Deleted" ShowInCustomizationForm="True" VisibleIndex="9" Width="120px" Caption="Deactivated">
                                                                                                    <PropertiesDateEdit DisplayFormatInEditMode="True" DisplayFormatString="dd/MM/yyyy HH:mm" EditFormat="DateTime" EditFormatString="dd/MM/yyyy HH:mm">
                                                                                                        <Style HorizontalAlign="Right">
                                                                                                        </Style>
                                                                                                    </PropertiesDateEdit>
                                                                                                    <Settings ShowFilterRowMenu="False" ShowInFilterControl="False" />
                                                                                                </dx:GridViewDataDateColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="DeletedBy" Width="100px" VisibleIndex="10" Caption="Deactivated By">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="DeletedReason" VisibleIndex="11" Width="120px" Caption="Deactivated Reason">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataDateColumn FieldName="Modified" Width="120px" ReadOnly="True" 
                                                                                                    Visible="true" VisibleIndex="12">
                                                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm" EditFormat="Custom" EditFormatString="dd/MM/yyyy" >
                                                                                                        <Style HorizontalAlign="Right"></Style>
                                                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                                                    </PropertiesDateEdit>
                                                                                                    <Settings ShowFilterRowMenu="False" ShowInFilterControl="False" />
                                                                                                </dx:GridViewDataDateColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="ModifiedBy" Width="100px" ReadOnly="True" VisibleIndex="13">
                                                                                                    <PropertiesTextEdit MaxLength="100">
                                                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                                                    </PropertiesTextEdit>
                                                                                                </dx:GridViewDataTextColumn>
                                                                                            </Columns>
                                                                                            <Settings VerticalScrollableHeight="300" VerticalScrollBarMode="Auto" />
                                                                                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                                                                                            <SettingsPager PageSize="15">
                                                                                            </SettingsPager>
                                                                                            <SettingsEditing Mode="Inline" />
                                                                                            <Styles>
                                                                                                <AlternatingRow Enabled="True">
                                                                                                </AlternatingRow>
                                                                                            </Styles>
                                                                                        </dx:ASPxGridView>
                                                                                    </dx:ContentControl>
                                                                                </ContentCollection>
                                                                            </dx:TabPage>
                                                                            <dx:TabPage Name="tabULDChild" Text="Child Container">
                                                                                <ActiveTabStyle Font-Bold="True">
                                                                                </ActiveTabStyle>
                                                                                <ContentCollection>
                                                                                    <dx:ContentControl runat="server">
																					    <dx:ASPxGridView ID="grdChildContainer" runat="server" ClientInstanceName="grdChildContainer" Width="100%" AutoGenerateColumns="False"
                                                                                            KeyboardSupport="True" KeyFieldName="ConsKey;LineNo"
                                                                                            OnCustomUnboundColumnData="grdChildContainer_CustomUnboundColumnData"
                                                                                            OnBeforePerformDataSelect="grdChildContainer_BeforePerformDataSelect" >
                                                                                            <Columns>                                                                                                
                                                                                                <dx:GridViewDataTextColumn FieldName="ConsKey" Visible="False" VisibleIndex="1">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="LineNo" Visible="False" VisibleIndex="2">
                                                                                                </dx:GridViewDataTextColumn>                                                                                                
                                                                                                <dx:GridViewDataTextColumn FieldName="Barcode" Width="110px" VisibleIndex="3" SortIndex="0" SortOrder="Ascending">                                                                                                    
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="Venue" UnboundType="String" VisibleIndex="4">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="VenueType" UnboundType="String" VisibleIndex="5">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="ProductType" UnboundType="String" VisibleIndex="6">
                                                                                                </dx:GridViewDataTextColumn>
                                                                                            </Columns>
                                                                                            <Settings VerticalScrollableHeight="300" VerticalScrollBarMode="Auto" />
                                                                                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                                                                                            <SettingsPager PageSize="15">
                                                                                            </SettingsPager>
                                                                                            <SettingsEditing Mode="Inline" />
                                                                                            <Styles>
                                                                                                <AlternatingRow Enabled="True">
                                                                                                </AlternatingRow>
                                                                                            </Styles>
                                                                                        </dx:ASPxGridView>
                                                                                    </dx:ContentControl>
																					</ContentCollection>
                                                                            </dx:TabPage>

                                                                        </TabPages>
                                                                    </dx:ASPxPageControl>
                                                                </DetailRow>
                                                                <StatusBar>
                                                                    <dx:ASPxLabel ID="lblStatus" runat="server" 
                                                                        Text="Click the '+' button to show container status grid view" 
                                                                        ForeColor="Maroon">
                                                                    </dx:ASPxLabel>
                                                                </StatusBar>
                                                            </Templates>
                                                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                                                            <SettingsEditing Mode="Inline" NewItemRowPosition="Bottom" />
                                                            <SettingsDetail ShowDetailRow="True" AllowOnlyOneMasterRowExpanded="false" />
                                                            <Settings ShowFooter="True" ShowStatusBar="Visible" VerticalScrollableHeight="300" VerticalScrollBarMode="Auto" />
                                                            <Columns>
                                                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction"
                                                                    VisibleIndex="0" Width="100px">
                                                                    <CustomButtons>
                                                                        <dx:GridViewCommandColumnCustomButton ID="btnPrintContainer" Text="Print">
                                                                            <Styles>
                                                                                <Style ForeColor="White">
                                                                                </Style>
                                                                            </Styles>
                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                        <dx:GridViewCommandColumnCustomButton ID="btnEditContainer" Text="Edit">
                                                                            <Styles>
                                                                                <Style ForeColor="White">
                                                                                </Style>
                                                                            </Styles>
                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                        <dx:GridViewCommandColumnCustomButton ID="btnUpdateContainer" Text="Update" Visibility="EditableRow">
                                                                            <Styles>
                                                                                <Style ForeColor="White">
                                                                                </Style>
                                                                            </Styles>
                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                        <dx:GridViewCommandColumnCustomButton ID="btnCancelContainer" Text="Cancel" Visibility="EditableRow">
                                                                            <Styles>
                                                                                <Style ForeColor="White">
                                                                                </Style>
                                                                            </Styles>
                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                    </CustomButtons>
                                                                    <CellStyle BackColor="#007D86">
                                                                    </CellStyle>
                                                                </dx:GridViewCommandColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ConsKey" Visible="False" VisibleIndex="1">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LineNo" Visible="False" VisibleIndex="2">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Barcode" Width="100px" VisibleIndex="3" ReadOnly="True" SortIndex="0" SortOrder="Ascending">
                                                                    <PropertiesTextEdit>
                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataTextColumn Caption="ULD Parent" FieldName="ParentBarcode" Width="100px" ReadOnly="True" VisibleIndex="4">
                                                                    <PropertiesTextEdit>
                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <%-- 
                                                                <dx:GridViewDataTextColumn FieldName="VenueName" Width="200px" ReadOnly="True" VisibleIndex="4">
                                                                    <PropertiesTextEdit>
                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                --%>
                                                                <dx:GridViewDataCheckColumn FieldName="Activated" VisibleIndex="5" Width="60px" Visible="False">
                                                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                                                    </PropertiesCheckEdit>
                                                                </dx:GridViewDataCheckColumn>

                                                                <dx:GridViewDataDateColumn FieldName="Deleted" ShowInCustomizationForm="True" VisibleIndex="6" Width="150px" Caption="Deactivated">
                                                                    <PropertiesDateEdit DisplayFormatInEditMode="True" DisplayFormatString="dd/MM/yyyy HH:mm" EditFormat="DateTime" EditFormatString="dd/MM/yyyy HH:mm">
                                                                        <Style HorizontalAlign="Right">
                                                                        </Style>
                                                                    </PropertiesDateEdit>
                                                                    <Settings ShowFilterRowMenu="False" ShowInFilterControl="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DeletedBy" Width="100px" VisibleIndex="7" Caption="Deactivated By">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DeletedReason" VisibleIndex="8" Caption="Deactivated Reason">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="Modified" Visible="true" VisibleIndex="9" ReadOnly="True" Width="120px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm" EditFormat="Custom" EditFormatString="dd/MM/yyyy">
                                                                        <Style HorizontalAlign="Right"></Style>
                                                                        <DropDownButton ClientVisible="False" Visible="False">
                                                                        </DropDownButton>
                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                    </PropertiesDateEdit>
                                                                    <Settings ShowFilterRowMenu="False" ShowInFilterControl="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ModifiedBy" Width="100px" VisibleIndex="10" ReadOnly="True">
                                                                    <PropertiesTextEdit MaxLength="100">
                                                                        <ReadOnlyStyle BackColor="Gainsboro"></ReadOnlyStyle>
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                        </TabPages>
                                        <ContentStyle VerticalAlign="Top">
                                        </ContentStyle>
                                    </dx:ASPxPageControl>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
    <dx:ASPxCallback ID="cbPrintLabel" runat="server" ClientInstanceName="cbPrintLabel" OnCallback="cbPrintLabel_Callback" >
    </dx:ASPxCallback>
    <dx:ASPxCallback ID="cbContainer" runat="server" ClientInstanceName="cbContainer" 
        OnCallback="cbContainer_Callback">
    </dx:ASPxCallback>
    <dx:ASPxCallback ID="cbContainerStatus" runat="server" ClientInstanceName="cbContainerStatus" 
        OnCallback="cbContainerStatus_Callback">
    </dx:ASPxCallback>
    <dx:ASPxPopupControl ID="ppCtrlContainerStatus" runat="server" 
        AllowDragging="True" ClientInstanceName="ppCtrlContainerStatus" 
        HeaderText="Container Status for Barcode" Modal="True" 
        FooterText="" PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" ShowFooter="True" 
        Width="480px" ShowCloseButton="False">
        <ContentStyle HorizontalAlign="Left"></ContentStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
				<dx:ASPxCallbackPanel ID="cpContainerStatus" runat="server" Width="100%" ClientInstanceName="cpContainerStatus"
                    OnCallback="cpContainerStatus_Callback" >
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent21" runat="server">
						    <div id="containerStatusContainer">                                
                                <table style="width:100%; height: 100%;">
                                    <tr>
                                        <td align="right" style="width: 10%">
                                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Date"></dx:ASPxLabel>
                                        </td>
                                        <td style="width: 40%">
                                            <dx:ASPxDateEdit ID="edtStatusDate" runat="server" ClientInstanceName="edtStatusDate" EditFormat="DateTime" Width="100%" TimeSectionProperties-Visible="True">
                                            </dx:ASPxDateEdit>
                                        </td>
                                        <td align="right" style="width: 10%">
                                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Location"></dx:ASPxLabel>
                                        </td>
                                        <td style="width: 40%">
                                            <dx:ASPxComboBox ID="cmbLocation" runat="server" ClientInstanceName="cmbLocation" Width="100%" ValueType="System.String" TextField="Name" ValueField="Code"></dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 10%">
                                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Status"></dx:ASPxLabel>
                                        </td>
                                        <td colspan="3" style="width: 90%">
                                            <dx:ASPxComboBox ID="cmbStatus" runat="server" ClientInstanceName="cmbStatus" Width="100%" ValueType="System.String" TextField="Stage" ValueField="StageId"></dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 10%">
                                            <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Movement"></dx:ASPxLabel>
                                        </td>
                                        <td colspan="3" style="width: 90%">
                                            <dx:ASPxTextBox ID="edtMovement" runat="server" ClientInstanceName="edtMovement" Width="100%" NullText="Movement Direction"></dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <dx:ASPxCheckBox ID="cbDelete" runat="server" ClientInstanceName="cbDelete" Text="Deactivate" ValueChecked="Y" ValueUnchecked="N" ValueType="System.Char"></dx:ASPxCheckBox>
                                        </td>
                                        <td colspan="3" style="width: 90%">
                                            <dx:ASPxTextBox ID="edtDelReason" runat="server" ClientInstanceName="edtDelReason" Width="100%" NullText="Deactivate Reason"></dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
						</dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
			</dx:PopupControlContentControl>
        </ContentCollection>
        <FooterTemplate>
            <table style="width:100%;">
                <tr>
                    <td align="right" style="width: 90%">
                        <dx:ASPxButton ID="btnContainerStatusOK" runat="server" AutoPostBack="False" Text="OK" ClientInstanceName="btnContainerStatusOK" CausesValidation="False" Native="true"></dx:ASPxButton>
                    </td>
                    <td align="right" style="width: 8%">
                        <dx:ASPxButton ID="btnContainerStatusCancel" runat="server" AutoPostBack="False" Text="Cancel" ClientInstanceName="btnContainerStatusCancel" CausesValidation="False" Native="true"></dx:ASPxButton>
                    </td>
                    <td style="width: 2%"></td>
                </tr>
            </table>
        </FooterTemplate>
        <HeaderStyle HorizontalAlign="Left" />
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ppCtrlContainer" runat="server" 
        AllowDragging="True" ClientInstanceName="ppCtrlContainer" 
        HeaderText="Container Barcode" Modal="True" 
        FooterText="" PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" ShowFooter="True" 
        Width="550px" ShowCloseButton="False">
        <ContentStyle HorizontalAlign="Left"></ContentStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
				<dx:ASPxCallbackPanel ID="cpContainer" runat="server" Width="100%" ClientInstanceName="cpContainer"
                    OnCallback="cpContainer_Callback" >
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent1" runat="server">
						    <div id="containerContainer">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 34%"><dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="RO"></dx:ASPxLabel></td>
                                        <td style="width: 33%"><dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="LGA"></dx:ASPxLabel></td>
                                        <td style="width: 33%"><dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Ward"></dx:ASPxLabel></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 34%">
                                            <dx:ASPxComboBox ID="cmbEMO" runat="server" ClientInstanceName="cmbEMO" Width="99%"
                                                IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td style="width: 33%">
                                            <dx:ASPxComboBox ID="cmbDistrict" runat="server" ClientInstanceName="cmbDistrict" Width="99%"
                                                IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td style="width: 33%">
                                            <dx:ASPxComboBox ID="cmbContestArea" runat="server" ClientInstanceName="cmbContestArea" Width="100%"
                                                IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 34%"><dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Contest"></dx:ASPxLabel></td>
                                        <td style="width: 33%"><dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Venue"></dx:ASPxLabel></td>
                                        <td style="width: 33%"><dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="Container"></dx:ASPxLabel></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 34%">
                                            <dx:ASPxComboBox ID="cmbProduct" runat="server" ClientInstanceName="cmbProduct" Width="99%"
                                                IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td style="width: 33%">                                            
                                            <dx:ASPxComboBox ID="cmbVenue" runat="server" ClientInstanceName="cmbVenue" Width="99%"
                                                IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code">
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td style="width: 33%">
                                            <dx:ASPxComboBox ID="cmbContainerType" runat="server" ClientInstanceName="cmbContainerType" Width="100%"
                                                IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <dx:ASPxCheckBox ID="cbCtnDelete" runat="server" ClientInstanceName="cbCtnDelete" Text="Deactivate" ValueChecked="Y" ValueUnchecked="N" ValueType="System.Char"></dx:ASPxCheckBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <dx:ASPxTextBox ID="edtCtnDelReason" runat="server" ClientInstanceName="edtCtnDelReason" Width="100%" NullText="Deactivate Reason"></dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                </table>
							</div>
						</dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
			</dx:PopupControlContentControl>
        </ContentCollection>
        <FooterTemplate>
            <table style="width:100%;">
                <tr>
                    <td align="right" style="width: 90%">
                        <dx:ASPxButton ID="btnContainerOK" runat="server" AutoPostBack="False" Text="OK" ClientInstanceName="btnContainerOK" CausesValidation="False" Native="true"></dx:ASPxButton>
                    </td>
                    <td align="right" style="width: 8%">
                        <dx:ASPxButton ID="btnContainerCancel" runat="server" AutoPostBack="False" Text="Cancel" ClientInstanceName="btnContainerCancel" CausesValidation="False" Native="true"></dx:ASPxButton>
                    </td>
                    <td style="width: 2%"></td>
                </tr>
            </table>
        </FooterTemplate>
        <HeaderStyle HorizontalAlign="Left" />
    </dx:ASPxPopupControl>
</asp:Content>
