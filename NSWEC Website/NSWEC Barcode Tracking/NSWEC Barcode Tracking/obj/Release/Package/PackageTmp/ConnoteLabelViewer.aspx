﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConnoteLabelViewer.aspx.cs" Inherits="NSWEC_Barcode_Tracking.ConnoteLabelViewer" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- comment
<!DOCTYPE html>
    -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Consignment Label</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <tr>            
            <td align="left" style="width: 320px">
                <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" ReportViewerID="LabelViewer" Width="100%" >
                    <Items>
                        <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                        <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                        <dx:ReportToolbarLabel ItemKind="PageLabel" />
                        <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                        </dx:ReportToolbarComboBox>
                        <dx:ReportToolbarLabel ItemKind="OfLabel" />
                        <dx:ReportToolbarTextBox ItemKind="PageCount" />
                        <dx:ReportToolbarButton ItemKind="NextPage" />
                        <dx:ReportToolbarButton ItemKind="LastPage" />
                        <dx:ReportToolbarSeparator />
                        <dx:ReportToolbarButton ItemKind='SaveToDisk' />
                        <dx:ReportToolbarComboBox ItemKind='SaveFormat' Width='70px'>
                            <Elements>
                                <dx:ListElement Value='pdf' />
                            </Elements>
                        </dx:ReportToolbarComboBox>
                    </Items>
                    <Styles>
                        <LabelStyle>
                            <Margins MarginLeft='3px' MarginRight='3px' />
                        </LabelStyle>
                    </Styles>
                </dx:ReportToolbar>
            </td>            
            <td align="left" valign="top">
                <dx:ASPxButton ID="btnPrint" ClientInstanceName="btnPrint" runat="server" Text="Print" 
                     ClientVisible="false" AutoPostBack="False">
                </dx:ASPxButton>            
            </td>
        </tr>
            <tr>
            <td align="left" colspan="2">
                <dx:ASPxLabel id="lblError" runat="server" ClientVisible="False" ForeColor="Red"
                    Text="No label for container found in this consignment">
                </dx:ASPxLabel>
            </td>
        </tr>
        </table>
        <br />
        <dx:ReportViewer ID="LabelViewer" runat="server" ClientInstanceName="LabelViewer">
            <border borderstyle="Groove"></border>
        </dx:ReportViewer>               
        <dx:ASPxHiddenField ID="hfHiddenInfo" runat="server" ClientInstanceName="hfHiddenInfo"></dx:ASPxHiddenField>    
    </div>
    </form>
</body>
</html>
