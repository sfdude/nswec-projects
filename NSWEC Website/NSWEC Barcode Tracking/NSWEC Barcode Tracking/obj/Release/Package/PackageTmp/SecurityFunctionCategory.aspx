﻿<%@ Page Title="Security Function Category" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SecurityFunctionCategory.aspx.cs" Inherits="NSWEC_Barcode_Tracking.SecurityFunctionCategory" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table width="100%">
        <tr>
            <td align="left" valign="top">
                <dx:ASPxMenu id="menAction" runat="server" ClientInstanceName="menAction" ShowPopOutImages="True" OnItemClick="menAction_ItemClick">
                    <border borderstyle="None"></border>
                    <items>
                        <dx:MenuItem Name="itmNewCategory" Text="New" ToolTip="New Security Function Category">
                        </dx:MenuItem>
                        <dx:MenuItem Name="itmExport" Text="Export">
                            <Items>
                            <dx:MenuItem Name="itmExpCSV" Text="Export to CSV"></dx:MenuItem>
                            <dx:MenuItem Name="itmExpPDF" Text="Export to PDF"></dx:MenuItem>
                            <dx:MenuItem Name="itmExpRTF" Text="Export to RTF"></dx:MenuItem>
                            <dx:MenuItem Name="itmExpXLS" Text="Export to XLS"></dx:MenuItem>
                            <dx:MenuItem Name="itmExpXLSX" Text="Export to XLSX"></dx:MenuItem>
                            </Items>
                        </dx:MenuItem>                       
                    </items>
                </dx:ASPxMenu>                
            </td>
        </tr>
        <tr>
            <td align="left">
                <dx:ASPxGridView ID="grdFunctionCategory" runat="server" AutoGenerateColumns="False"
                    ClientInstanceName="grdFunctionCategory" KeyboardSupport="True" Width="100%" KeyFieldName="ID"
                    OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                    OnCustomCallback="grdFunctionCategory_CustomCallback" >
                    <Columns>
                        <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colCategoryAction"
                            VisibleIndex="0" Width="100px">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="btnEditCategory" Text="Edit">
                                    <Styles>
                                        <Style ForeColor="White">
                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                                <dx:GridViewCommandColumnCustomButton ID="btnDeleteCategory" Text="Delete">
                                    <Styles>
                                        <Style ForeColor="White">
                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                                <dx:GridViewCommandColumnCustomButton ID="btnUpdateCategory" Text="Update" Visibility="EditableRow">
                                    <Styles>
                                        <Style ForeColor="White">
                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                                <dx:GridViewCommandColumnCustomButton ID="btnCancelCategory" Text="Cancel" Visibility="EditableRow">
                                    <Styles>
                                        <Style ForeColor="White">
                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <CellStyle BackColor="#007D86">
                            </CellStyle>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>                        
                        <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="2" Caption="Category Name">                            
                            <PropertiesTextEdit>
                                <ValidationSettings Display="Dynamic" ValidateOnLeave="False">
                                    <RequiredField ErrorText="Name is required" IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="Active" VisibleIndex="3" Width="80px">
                            <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        
                        
                    </Columns>
                    <SettingsPager PageSize="15">
                    </SettingsPager>
                    <Styles>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                    <Templates>
                        <DetailRow>
                            <div style="padding: 3px 3px 2px 3px">
                                <dx:ASPxPageControl runat="server" ID="pcFuncCatDetails" Width="100%" EnableCallBacks="True" ActiveTabIndex="0">
                                    <TabPages>
                                        <dx:TabPage Name="tabSecFunct" Text="Security Functions">
                                            <ActiveTabStyle Font-Bold="True"></ActiveTabStyle>
                                            <ContentCollection>
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxButton ID="btnAddFunction" runat="server" ClientInstanceName="btnAddFunction" EnableDefaultAppearance="False" Font-Underline="True"
                                                            ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False">
                                                        <Border BorderStyle="None" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxGridView ID="grdSecurityFunction" runat="server" AutoGenerateColumns="False"
                                                        ClientInstanceName="grdSecurityFunction" KeyboardSupport="True" Width="100%" KeyFieldName="ID"
                                                        OnBeforePerformDataSelect="grdFunction_BeforePerformDataSelect"
                                                        OnCustomCallback="grdFunction_CustomCallback" >                                                       
                                                        <Columns>
                                                            <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colFuncAction"
                                                                VisibleIndex="0" Width="100px">
                                                                <CustomButtons>
                                                                    <dx:GridViewCommandColumnCustomButton ID="btnEditFunc" Text="Edit">
                                                                        <Styles>
                                                                            <Style ForeColor="White">
                                                                            </Style>
                                                                        </Styles>
                                                                    </dx:GridViewCommandColumnCustomButton>
                                                                    <dx:GridViewCommandColumnCustomButton ID="btnDeleteFunc" Text="Delete">
                                                                        <Styles>
                                                                            <Style ForeColor="White">
                                                                            </Style>
                                                                        </Styles>
                                                                    </dx:GridViewCommandColumnCustomButton>
                                                                    <dx:GridViewCommandColumnCustomButton ID="btnUpdateFunc" Text="Update" Visibility="EditableRow">
                                                                        <Styles>
                                                                            <Style ForeColor="White">
                                                                            </Style>
                                                                        </Styles>
                                                                    </dx:GridViewCommandColumnCustomButton>
                                                                    <dx:GridViewCommandColumnCustomButton ID="btnCancelFunc" Text="Cancel" Visibility="EditableRow">
                                                                        <Styles>
                                                                            <Style ForeColor="White">
                                                                            </Style>
                                                                        </Styles>
                                                                    </dx:GridViewCommandColumnCustomButton>
                                                                </CustomButtons>
                                                                <CellStyle BackColor="#007D86">
                                                                </CellStyle>
                                                            </dx:GridViewCommandColumn>
                                                            <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="1" Caption="Function Name">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataCheckColumn FieldName="Active" VisibleIndex="2" Width="80px">
                                                                <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                                                </PropertiesCheckEdit>
                                                            </dx:GridViewDataCheckColumn>
                                                            <dx:GridViewDataTextColumn FieldName="Category" Visible="False" VisibleIndex="3">
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True"></SettingsBehavior>
                                                        <Settings VerticalScrollableHeight="300" VerticalScrollBarMode="Auto" />
                                                        <Styles>
                                                            <AlternatingRow Enabled="True">
                                                            </AlternatingRow>
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                    </TabPages>
                                </dx:ASPxPageControl>
                            </div>
                        </DetailRow>
                    </Templates>
                    <SettingsDetail ShowDetailRow="True" AllowOnlyOneMasterRowExpanded="True" />
                    <Settings ShowHeaderFilterButton="True" VerticalScrollableHeight="400" VerticalScrollBarMode="Auto" />
                    <SettingsBehavior AllowFocusedRow="True" AutoExpandAllGroups="True" ColumnResizeMode="Control"
                        EnableRowHotTrack="True" />
                    <SettingsEditing Mode="Inline" />
                    <SettingsText Title="Security Category" />
                </dx:ASPxGridView>
                </td>
        </tr>
    </table>
    <dx:ASPxHiddenField id="hfInfo" runat="server" ClientInstanceName="hfInfo">
    </dx:ASPxHiddenField>
    <dxpc:ASPxPopupControl ID="ppCtrlFunction" runat="server" 
        AllowDragging="True" ClientInstanceName="ppCtrlFunction" 
        HeaderText="Security Function" Modal="True" 
        FooterText="" PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" ShowFooter="True" 
        Width="800px" ShowCloseButton="False" ShowPageScrollbarWhenModal="True" >
        <ContentStyle HorizontalAlign="Left"></ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <dx:ASPxCallbackPanel ID="cpFunction" runat="server" Width="100%" ClientInstanceName="cpFunction" OnCallback="cpFunction_Callback">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                        <div id="functionContainer">
                            <table style="width:100%; height: 100%;">
                    <tr>
                        <td align="right" style="width: 10%">                                            
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Category">
                            </dx:ASPxLabel>
            	                
                        </td> 
                        <td align="left" style="width: 65%">                                            
                            <dx:ASPxComboBox ID="cmbCategory" runat="server" ClientInstanceName="cmbCategory"
                                IncrementalFilteringMode="StartsWith" TextField="Name"
                                ValueField="ID" Width="100%" ValueType="System.Int32" >
                            </dx:ASPxComboBox>
                        </td>
                        <td align="right" style="width: 25%" >                                            
                            <dx:ASPxCheckBox ID="cbFuncActive" runat="server" CheckState="Unchecked" ClientInstanceName="cbFuncActive"
                                Text="Active" ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                            </dx:ASPxCheckBox>        
                        </td>        
                    </tr> 
                    <tr>
                        <td align="right" style="width: 10%">                                            
                           <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Name">
                            </dx:ASPxLabel>  
                        </td> 
                        <td align="left" style="width: 90%" colspan="2" >                                            
                           <dx:ASPxTextBox ID="edtName" runat="server" ClientInstanceName="edtName" Width="100%" MaxLength="100" >
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField ErrorText="Name is mandatory" IsRequired="True"/>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>             
                    </tr>                        
                </table>
                        </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel> 
                <div style="padding: 3px 3px 2px 3px">
                    <dx:ASPxPageControl runat="server" ID="pcFuncDetails" ClientInstanceName="pcFuncDetails" Width="100%"
                        EnableCallBacks="True" ActiveTabIndex="0">
                        <TabPages>
                            <dx:TabPage Name="tabSecFunctComp" Text="Security Function Component">
                                <ActiveTabStyle Font-Bold="True"></ActiveTabStyle>
                                <ContentCollection>
                                    <dx:ContentControl runat="server">
                                        <dx:ASPxButton ID="btnAddFuncComp" runat="server" ClientInstanceName="btnAddFuncComp" EnableDefaultAppearance="False" Font-Underline="True"
                                                ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False">
                                            <Border BorderStyle="None" />
                                        </dx:ASPxButton>                  
                <dx:ASPxGridView ID="grdFuncComponent" runat="server" AutoGenerateColumns="False"
                    ClientInstanceName="grdFuncComponent" KeyboardSupport="True"
                    KeyFieldName="ID" Width="100%" OnInitNewRow="grdFuncComponent_InitNewRow"
                    OnCommandButtonInitialize="grdView_CommandButtonInitialize" OnCustomCallback="grdFuncComponent_CustomCallback" >
                    <Columns>
                        <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colCompAction"
                            VisibleIndex="0" Width="100px">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="btnEditComp" Text="Edit">
                                    <Styles>
                                        <Style ForeColor="White">
                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                                <dx:GridViewCommandColumnCustomButton ID="btnDeleteComp" Text="Delete">
                                    <Styles>
                                        <Style ForeColor="White">
                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                                <dx:GridViewCommandColumnCustomButton ID="btnUpdateComp" Text="Update" Visibility="EditableRow">
                                    <Styles>
                                        <Style ForeColor="White">
                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                                <dx:GridViewCommandColumnCustomButton ID="btnCancelComp" Text="Cancel" Visibility="EditableRow">
                                    <Styles>
                                        <Style ForeColor="White">
                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <CellStyle BackColor="#007D86">
                            </CellStyle>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SecurityFunction" Visible="False" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Page Name" FieldName="FormName" VisibleIndex="2">
                            <PropertiesTextEdit>
                                <ValidationSettings Display="Dynamic" ValidateOnLeave="False">
                                    <RequiredField ErrorText="Page name is required" IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="FrameName" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ComponentName" VisibleIndex="4" Width="150px">
                            <PropertiesTextEdit>
                                <ValidationSettings Display="Dynamic" ValidateOnLeave="False">
                                    <RequiredField ErrorText="Name is required" IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Url" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Url Parameter"  FieldName="UrlParam" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="Enabled" VisibleIndex="6" Width="60px">
                            <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="Visible" VisibleIndex="8" Width="60px">
                            <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="Caption" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowHorizontalScrollBar="True" />
                    <Styles>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                </dx:ASPxGridView>
                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                    </TabPages>
                                </dx:ASPxPageControl>
                            </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <FooterTemplate>
            <table style="width:100%;">
                <tr>
                    <td align="right" style="width: 90%">
                        <dx:ASPxButton ID="btnFuncSave" runat="server" ForeColor="#3399FF" AutoPostBack="False" Text="Save" ClientInstanceName="btnFuncSave" CausesValidation="False"></dx:ASPxButton>
                    </td>
                    <td align="right" style="width: 8%">
                        <dx:ASPxButton ID="btnFuncCancel" runat="server" ForeColor="#3399FF" AutoPostBack="False" Text="Cancel" ClientInstanceName="btnFuncCancel" CausesValidation="False"></dx:ASPxButton>
                    </td>
                    <td style="width: 2%"></td>
                </tr>
            </table>
        </FooterTemplate>
        <HeaderStyle HorizontalAlign="Left" BackColor="#007D86" ForeColor="White" />
        <FooterStyle BackColor="#007D86" />
    </dxpc:ASPxPopupControl>

</asp:Content>
