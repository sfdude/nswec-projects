﻿<%@ Page Title="Import Container" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ImportContainer.aspx.cs" Inherits="NSWEC_Barcode_Tracking.ImportContainer" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:ASPxPageControl ID="pcImport" runat="server" ActiveTabIndex="0" Width="100%">
    <TabPages>
        <dx:TabPage Name="tabImport" Text="Import Consignment/Container">
            <ActiveTabStyle Font-Bold="True">
            </ActiveTabStyle>
            <ContentCollection>
                <dx:ContentControl runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td align="left" width="20%">
                                <dx:ASPxMenu ID="menuAction" runat="server" ClientInstanceName="menuAction">
                                    <Items>
                                        <dx:MenuItem Name="itmImport" Text="Import" ToolTip="Import PPDM File">
                                        </dx:MenuItem>
                                    </Items>
                                </dx:ASPxMenu>
                            </td>
                            <td align="left" width="30%">&nbsp;</td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" width="20%">
                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Source File:">
                                </dx:ASPxLabel>
                            </td>
                            <td align="left" width="30%">
                                <dx:ASPxUploadControl ID="ucSource" runat="server" ClientInstanceName="ucSource" OnFileUploadComplete="ucSource_FileUploadComplete" Width="99%">
                                    <ValidationSettings AllowedFileExtensions=".xlsx" NotAllowedFileExtensionErrorText="This file extension isn't allowed; Accepted files extension &quot;.txt, .csv, .txt, .xlsx, .xls, .xml&quot;">
                                    </ValidationSettings>
                                </dx:ASPxUploadControl>
                            </td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                        
                        <tr>
                            <td align="right" width="20%">
                                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Election Manager Office Worksheet:">
                                </dx:ASPxLabel>
                            </td>
                            <td align="left" width="30%">
                                <dx:ASPxTextBox ID="edtROWorksheet" runat="server" ClientInstanceName="edtROWorksheet" Width="100%">
                                </dx:ASPxTextBox>
                            </td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                        
                        <tr>
                            <td align="right" width="20%">
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Processing Location Worksheet:">
                                </dx:ASPxLabel>
                            </td>
                            <td align="left" width="30%">
                                <dx:ASPxTextBox ID="edtCCWorksheet" runat="server" ClientInstanceName="edtCCWorksheet" Width="100%">
                                </dx:ASPxTextBox>
                            </td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                        
                        <tr>
                            <td align="right" width="20%">
                                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Consignment/Container Worksheet:">
                                </dx:ASPxLabel>
                            </td>
                            <td align="left" width="30%">
                                <dx:ASPxTextBox ID="edtConnoteWorksheet" runat="server" ClientInstanceName="edtConnoteWorksheet" Width="100%">
                                </dx:ASPxTextBox>
                            </td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                    </table>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
</dx:ASPxPageControl>
    <dx:ASPxCallback ID="cbImport" runat="server" ClientInstanceName="cbImport" OnCallback="cbImport_Callback" >
    </dx:ASPxCallback>

</asp:Content>
