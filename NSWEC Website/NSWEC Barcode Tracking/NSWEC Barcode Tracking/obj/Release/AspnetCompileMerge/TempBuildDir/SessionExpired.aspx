﻿<%@ Page Title="Session Expired" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SessionExpired.aspx.cs" Inherits="NSWEC_Barcode_Tracking.SessionExpired" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%; position: relative">
        <tr>
            <td style="width: 40%">
                <dx:ASPxLabel ID="lblSessionEnded" runat="server" Font-Size="Medium" Style="position: relative"
                    Text="Your session has expired. You will need to login again." Width="100%">
                </dx:ASPxLabel>
            </td>
            <td style="width: 10%">
                <dx:ASPxButton ID="ASPxButton1" runat="server" PostBackUrl="~/Login.aspx" Style="position: relative"
                    Text="Continue..." Width="100%">
                </dx:ASPxButton>
            </td>
            <td style="width: 50%">
            </td>
        </tr>
    </table>
</asp:Content>
