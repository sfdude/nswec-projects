﻿<%@ Page Title="Security Device Catalogue" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SecurityDeviceCatalogue.aspx.cs" Inherits="NSWEC_Barcode_Tracking.SecurityDeviceCatalogue" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:ASPxPageControl ID="pcSecDev" runat="server" ClientInstanceName="pcSecDev" EnableCallBacks="true" ActiveTabIndex="0">
        <TabPages>
            <dx:TabPage Name="tabSecDev" Text="Security Device">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxButton ID="btnAdd" runat="server" ClientInstanceName="btnAdd" EnableDefaultAppearance="False" Font-Underline="True"
                            ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False">
                            <Border BorderStyle="None" />
                        </dx:ASPxButton>
                        <dx:ASPxGridView ID="grdSecDev" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdSecDev"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCustomButtonInitialize="grdView_CustomButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnInitNewRow="grdView_InitNewRow"
                            OnCustomCallback="grdView_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnDelete" Text="Delete">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                
                                <dx:GridViewDataTextColumn FieldName="DeviceId" SortIndex="1"
                                    SortOrder="Ascending" VisibleIndex="2" Width="150px">
                                    <PropertiesTextEdit MaxLength="50">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Device ID is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3" SortIndex="0" SortOrder="Ascending">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                                
                                <dx:GridViewDataCheckColumn FieldName="Active" VisibleIndex="4" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsEditing Mode="Inline" />
                            <SettingsPager Mode="ShowAllRecords">
                            </SettingsPager>
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="500" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
</asp:Content>
