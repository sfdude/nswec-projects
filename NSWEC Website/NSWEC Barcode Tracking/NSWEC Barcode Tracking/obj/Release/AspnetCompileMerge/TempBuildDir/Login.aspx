﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NSWEC_Barcode_Tracking.Login1" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <!-- CSS -->	
    <style type="text/css">
    html, 
        
    body {
        height: 100%;
    }
    
    body {
        /*
        background-image: url(./images/SF-Landing.jpg);
        background-repeat: no-repeat;
        background-size: 100% 100%;
        background-position:inherit;  
        */  
    }
    
    .header-pic {
    margin: 0 auto;
    position: relative;    
    height: 80px;
    }
    .header-pic1 {
    margin: 0 auto;
    position: relative;    
    height: 70px;
    }
    .auto-style1 {
        height: 40px;
    }
    </style>

<head runat="server">
    <title>NSWEC Barcode Tracking Login</title>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        var onloadCallback = function () {
            alert("grecaptcha is ready!");
        };
        function onSubmit(token) {
            var userName = document.getElementById('UserName').value; 
            var password = document.getElementById('Password').value;
            //debugger;
            if (userName != "" && password != "") {
                cbLogin.PerformCallback(token);
            }
        }        
    </script>
</head>
<body>
    <form id="loginform" runat="server">
    <div>        
        <table style="width: 100%;">
            <tr>
                <td width="30%"> 
                    <%-- comments  
                    <img class="header-pic" alt="" src="images/Smart-Freight_Horizontal-logo_RGB.png" />
                       
                    <img class="header-pic" alt="" src="images/NSWEC_Logo_Stacked_rgb_Colour_Positive_L.png" />
                    --%>
                    <dx:ASPxImage ID="imgNSWEC" runat="server" CssClass="header-pic" ImageUrl="~/images/NSWEC_Logo_Stacked_rgb_Colour_Positive_L.png" 
                        IsPng="True" ShowLoadingImage="True">
                    </dx:ASPxImage>
                </td>
                <td align="center" valign="bottom">
                    <%-- comments
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="XX-Large" Font-Strikeout="False" 
                        ForeColor="White" Text="Welcome to" Visible="false">
                    </dx:ASPxLabel>
                    --%>
                    <dx:ASPxLabel ID="lblWebTitle" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="XX-Large" Font-Strikeout="False"
                        ForeColor="#007D86"
                        Text="Welcome to BallotTrack">
                    </dx:ASPxLabel>
                </td>
                <td width="30%" align="right">
                    <%-- comments 
                    <img class="header-pic1" alt="" src="images/Electoral-Commission.png" />
                    --%>
                    <dx:ASPxImage ID="imgSF" runat="server" CssClass="header-pic1" ImageUrl="~/images/Smart-Freight_Horizontal-logo_RGB.png" 
                        IsPng="True" ShowLoadingImage="True">
                    </dx:ASPxImage>
                </td>
            </tr>
            <tr>
                <td width="30%">&nbsp;</td>
                <td align="center">
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="XX-Large" Font-Strikeout="False" 
                        ForeColor="White" Text="NSW Electoral Commission" Visible="false">
                    </dx:ASPxLabel>
                </td>
                <td width="30%">&nbsp;</td>
            </tr>
            <tr>
                <td width="30%">&nbsp;</td>
                <td align="center">
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="XX-Large" Font-Strikeout="False" 
                        ForeColor="White" Text="Barcode Tracking Portal" Visible="false">
                    </dx:ASPxLabel>
                </td>
                <td width="30%">&nbsp;</td>
            </tr>
            <tr>
                <td width="30%">&nbsp;</td>
                <td align="center" height="200px" valign="top">
                    <%--
                    <dx:ASPxLabel ID="lblWebTitle" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="XX-Large" Font-Strikeout="False"
                        ForeColor="White"
                        Text="Welcome to BallotTrack">
                    </dx:ASPxLabel>   
                     --%> 
                </td>
                <td width="30%">&nbsp;</td>
            </tr>
            <tr>
                <td width="30%">&nbsp;</td>                
                <td align="center">
                    
                    <dx:ASPxRoundPanel ID="rpLogin" runat="server" HeaderText="Sign in" HorizontalAlign="Left" ShowCollapseButton="false" Width="350px" 
                        BackColor="#00CEA6">
                        <HeaderStyle HorizontalAlign="center" BackColor="#007D86" ForeColor="White" />
                        <PanelCollection>
                            <dx:PanelContent runat="server">
                                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="100%">
                                    <PanelCollection>
                                        <dx:PanelContent runat="server">
                                            <asp:Login ID="smLogin" runat="server" DisplayRememberMe="False" Width="100%"
                                    OnAuthenticate="Login1_Authenticate" OnLoggingIn="Login1_LoggingIn" TitleText="">
                    <LayoutTemplate>
                        <table style="width:100%;">
                            <tr>
                                <td width="30px">&nbsp;</td>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Username:" ForeColor="White">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="30px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px">&nbsp;</td>
                                <td>
                                    <asp:TextBox ID="UserName" ClientIDMode="Static" runat="server" BackColor="#FFFFD6" BorderStyle="Inset" Width="100%" >                                        
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                                        ErrorMessage="Username is required." ToolTip="Username is required." ValidationGroup="smLogin" ForeColor="Red">Username is required</asp:RequiredFieldValidator>
                                </td>
                                <td width="30px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px">&nbsp;</td>
                                <td><dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Password:" ForeColor="White">
                                    </dx:ASPxLabel></td>
                                <td width="30px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px" class="auto-style1"></td>
                                <td class="auto-style1">
                                    <asp:TextBox ID="Password" ClientIDMode="Static" runat="server" BackColor="#FFFFD6" BorderStyle="Inset" TextMode="Password" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="smLogin" ForeColor="Red">Password is required</asp:RequiredFieldValidator>
                                </td>
                                <td width="30px" class="auto-style1"></td>
                            </tr>                            
                            <tr>
                                <td width="30px">&nbsp;</td>
                                <td>
                                    <%-- comments 
                                    <dx:ASPxCaptcha ID="ASPxCaptcha1" runat="server" ClientVisible="true">
                                        <TextBoxStyle BackColor="#FFFFD6" />
                                        <ValidationSettings SetFocusOnError="True">
                                        </ValidationSettings>
                                        <ChallengeImage ForegroundColor="White" BackgroundColor="#007D86" Height="60" Width="120"></ChallengeImage>
                                        <Border BorderWidth="0px" />
                                    </dx:ASPxCaptcha>
                                        --%>
                                </td>
                                <td width="30px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px">&nbsp;</td>
                                <td align="right">  
                                    <asp:Button ID="LoginButton" class="g-recaptcha" data-sitekey="6LfwvLEZAAAAAAtjm83x1KG6gRYMdbDyQokIzmq6" data-callback='onSubmit' data-action='login'
                                        runat="server" CommandName="Login" Text="Continue" ValidationGroup="smLogin" BackColor="#007D86" ForeColor="White" />
                                    <dx:ASPxCallback ID="cbLogin" OnCallback="cbLogin_Callback" runat="server" ClientInstanceName="cbLogin">
                                         <ClientSideEvents CallbackComplete="function(s, e) { if (e.result != null && e.result != '') lblMessage.SetText(e.result); lblMessage.SetVisible(true); }" />
                                    </dx:ASPxCallback>
                                </td>
                                <td width="30px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px">&nbsp;</td>
                                <td>
                                    <%-- comments 
                                    <asp:Label ID="lblloginMessage" runat="server" ForeColor="Red"></asp:Label>
                                        --%>
                                    <span style="color:red">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="false"></asp:Literal>
                                    </span>
                                </td>
                                <td width="30px">&nbsp;</td>
                            </tr>
                        </table>
                        
                    </LayoutTemplate>
                    <TextBoxStyle BorderStyle="Inset" Width="180px" />
                </asp:Login>
                                            <dx:ASPxLabel ID="lblMessage" runat="server" ClientInstanceName="lblMessage" Text="" ForeColor="Red" ClientVisible="false"></dx:ASPxLabel>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxCallbackPanel>

                                
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </td>
                <td width="30%">&nbsp;</td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
