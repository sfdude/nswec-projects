﻿<%@ Page Title="Maintenance" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Maintenance.aspx.cs" Inherits="NSWEC_Barcode_Tracking.Maintenance" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:ASPxPageControl ID="pcMaint" runat="server" ClientInstanceName="pcMaint" EnableCallBacks="true" ActiveTabIndex="2">
        <TabPages>
            <dx:TabPage Name="tabEvent" Text="Event">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="grdEvent" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdEvent"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%" 
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnCustomCallback="grdEvent_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="60px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnEventEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnEventUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnEventCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1"
                                    SortOrder="Ascending" VisibleIndex="2" Width="100px">
                                    <PropertiesTextEdit MaxLength="2">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                                
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="4" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="450" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabSource" Text="Source">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="grdSource" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdSource"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%" 
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnCustomCallback="grdSource_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnSourceEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnSourceUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnSourceCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1"
                                    SortOrder="Ascending" VisibleIndex="2" Width="100px">
                                    <PropertiesTextEdit MaxLength="2">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>  
								<dx:GridViewDataTextColumn FieldName="BarcodeStart" VisibleIndex="4">
                                    <PropertiesTextEdit MaxLength="10">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
								<dx:GridViewDataTextColumn FieldName="BarcodeStop" VisibleIndex="5">
                                    <PropertiesTextEdit MaxLength="10">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
								
								<dx:GridViewDataSpinEditColumn FieldName="BarcodeLength" VisibleIndex="6">
                                    <PropertiesSpinEdit DisplayFormatString="g" NumberType="Integer">
                                        <SpinButtons ShowIncrementButtons="False">
                                        </SpinButtons>
                                    </PropertiesSpinEdit>
                                </dx:GridViewDataSpinEditColumn>
								
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="7" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="450" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabCountCentre" Text="Processing Location">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="grdCountCentre" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdCountCentre"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%" 
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnCustomColumnSort="grdView_CustomColumnSort"
                            OnCustomCallback="grdCountCentre_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnCCEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnCCUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnCCCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1" Settings-SortMode="Custom"
                                    SortOrder="Ascending" VisibleIndex="2" Width="100px" >
                                    <PropertiesTextEdit MaxLength="2">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                                
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="4" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="450" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabReturningOffice" Text="Election Manager Office">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="grdRO" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdRO"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnCustomColumnSort="grdView_CustomColumnSort"
                            OnCustomCallback="grdRO_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnROEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnROUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnROCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                <%-- 
                                <dx:GridViewDataTextColumn FieldName="EventId" Caption="Event ID" VisibleIndex="2" GroupIndex="0" SortIndex="0" SortOrder="Ascending">
                                </dx:GridViewDataTextColumn>
                                --%>
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1" Settings-SortMode="Custom"
                                    SortOrder="Ascending" VisibleIndex="3" Width="100px">
                                    <PropertiesTextEdit MaxLength="3">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" Width="180px" VisibleIndex="4">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>      
                                <dx:GridViewDataTextColumn FieldName="AddressLine1" VisibleIndex="5" Visible="false">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>  
                                <dx:GridViewDataTextColumn FieldName="AddressLine2" VisibleIndex="6" Visible="false">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>   
                                <dx:GridViewDataTextColumn FieldName="Suburb" VisibleIndex="7">
                                    <PropertiesTextEdit MaxLength="40">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>   
                                <dx:GridViewDataTextColumn FieldName="State" VisibleIndex="8">
                                    <PropertiesTextEdit MaxLength="10">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>  
                                <dx:GridViewDataTextColumn FieldName="PostCode" VisibleIndex="9">
                                    <PropertiesTextEdit MaxLength="10">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>   
                                <dx:GridViewDataTextColumn FieldName="Contact" VisibleIndex="10" Visible="false">
                                    <PropertiesTextEdit MaxLength="40">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn> 
                                <dx:GridViewDataTextColumn FieldName="CountCentreCode" VisibleIndex="11">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>  
                                <dx:GridViewDataTextColumn FieldName="CountCentreName" VisibleIndex="11">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>    
                                <dx:GridViewDataTextColumn FieldName="DirectPhone" VisibleIndex="11" Visible="false">
                                    <PropertiesTextEdit MaxLength="20">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>      
                                <dx:GridViewDataTextColumn FieldName="OfficePhone" VisibleIndex="12" Visible="false">
                                    <PropertiesTextEdit MaxLength="20">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>   
                                <dx:GridViewDataTextColumn FieldName="Email" VisibleIndex="13" Visible="false">
                                    <PropertiesTextEdit MaxLength="300">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>   
                                <dx:GridViewDataTextColumn FieldName="SpecialInstructions" VisibleIndex="14" Visible="false">
                                    <PropertiesTextEdit MaxLength="1000">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>     
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="15" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsPager NumericButtonCount="20" PageSize="20">
                            </SettingsPager>
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="false" VerticalScrollBarMode="Auto" 
                               ShowFilterRow="True" ShowFilterRowMenu="True"
                               ShowHeaderFilterButton="True" VerticalScrollableHeight="450" 
                               ShowGroupPanel="false" ShowGroupedColumns="false" ShowGroupFooter="VisibleIfExpanded" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabLGA" Text="District">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="grdLGA" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdLGA"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnCustomUnboundColumnData="grdLGA_CustomUnboundColumnData"
                            OnCustomColumnSort="grdView_CustomColumnSort"
                            OnCustomCallback="grdLGA_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnLGAEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnLGAUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnLGACancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                <%-- 
                                <dx:GridViewDataTextColumn FieldName="EventId" Caption="Event ID" VisibleIndex="2" GroupIndex="0" SortIndex="0" SortOrder="Ascending">
                                </dx:GridViewDataTextColumn>
                                --%>
                                <dx:GridViewDataTextColumn FieldName="ROCode" Visible="False" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="3">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="ROName" Visible="False" VisibleIndex="4">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Election Manager Office" FieldName="ROCodeName" Name="colROCodeName"
                                    UnboundType="String" ShowInCustomizationForm="True" VisibleIndex="5" GroupIndex="1" SortIndex="1" SortOrder="Ascending">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1" Settings-SortMode="Custom"
                                    SortOrder="Ascending" VisibleIndex="6" Width="100px">
                                    <PropertiesTextEdit MaxLength="3">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="AreaCode" Width="100px" VisibleIndex="7">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>  
                                <dx:GridViewDataTextColumn FieldName="AreaAuthorityName" VisibleIndex="8">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                                
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="9" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsPager NumericButtonCount="20" PageSize="20">
                            </SettingsPager>
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="false" VerticalScrollBarMode="Auto" 
                               ShowFilterRow="True" ShowFilterRowMenu="True"
                               ShowHeaderFilterButton="True" VerticalScrollableHeight="450" 
                               ShowGroupPanel="true" ShowGroupedColumns="false" ShowGroupFooter="VisibleIfExpanded" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabWard" Text="Ward">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="grdWard" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdWard"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnCustomUnboundColumnData="grdWard_CustomUnboundColumnData"
                            OnCustomCallback="grdWard_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnWardEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnWardUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnWardCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                <%-- 
                                <dx:GridViewDataTextColumn FieldName="EventId" Caption="Event ID" VisibleIndex="2" GroupIndex="0" SortIndex="0" SortOrder="Ascending">
                                </dx:GridViewDataTextColumn>
--%>
                                <dx:GridViewDataTextColumn FieldName="ROCode" Visible="False" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="3">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="ROName" Visible="False" VisibleIndex="4">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Election Manager Office" FieldName="ROCodeName" Name="colROCodeName"
                                    UnboundType="String" ShowInCustomizationForm="True" VisibleIndex="5" GroupIndex="1" SortIndex="1" SortOrder="Ascending">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="District" FieldName="LGA" GroupIndex="2" ShowInCustomizationForm="True" SortIndex="2" SortOrder="Ascending" UnboundType="String" VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="3"
                                    SortOrder="Ascending" VisibleIndex="7" Width="100px">
                                    <PropertiesTextEdit MaxLength="2">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="8" >
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                                
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="9" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsPager NumericButtonCount="20" PageSize="20">
                            </SettingsPager>
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="false" VerticalScrollBarMode="Auto" 
                               ShowFilterRow="True" ShowFilterRowMenu="True"
                               ShowHeaderFilterButton="True" VerticalScrollableHeight="450" 
                               ShowGroupPanel="true" ShowGroupedColumns="false" ShowGroupFooter="VisibleIfExpanded" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabVenue" Text="Venue">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="grdVenue" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdVenue"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%" 
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCustomUnboundColumnData="grdVenue_CustomUnboundColumnData"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnCustomColumnSort="grdView_CustomColumnSort"
                            OnCustomCallback="grdVenue_CustomCallback"
                            OnBeforeGetCallbackResult="grdVenue_BeforeGetCallbackResult" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnVenueEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnVenueUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnVenueCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                <%-- 
                                <dx:GridViewDataTextColumn FieldName="EventId" Caption="Event ID" VisibleIndex="2" GroupIndex="0" SortIndex="0" SortOrder="Ascending">
                                </dx:GridViewDataTextColumn>
--%>
                                <dx:GridViewDataTextColumn FieldName="ROCode" Visible="False" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="3">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="ROName" Visible="False" VisibleIndex="4">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Election Manager Office" FieldName="EMO" Name="colEMO"
                                    UnboundType="String" ShowInCustomizationForm="True" VisibleIndex="5" GroupIndex="1" SortIndex="1" SortOrder="Ascending">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="District" GroupIndex="2" ShowInCustomizationForm="True" SortIndex="2" SortOrder="Ascending" UnboundType="String" VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                                <%-- LGA only --%>
                                <dx:GridViewDataTextColumn FieldName="Ward" GroupIndex="3" Name="colWard" ShowInCustomizationForm="True" SortIndex="3" SortOrder="Ascending" UnboundType="String" VisibleIndex="7">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="4" Settings-SortMode="Custom"
                                    SortOrder="Ascending" VisibleIndex="8" Width="100px">
                                    <PropertiesTextEdit MaxLength="5">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="9">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>       
                                <dx:GridViewDataTextColumn Caption="Address ID" FieldName="LongName" VisibleIndex="10">
                                    <PropertiesTextEdit MaxLength="200">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>  
                                <dx:GridViewDataTextColumn FieldName="VenueTypeCode" VisibleIndex="11">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>       
                                <dx:GridViewDataTextColumn FieldName="VenueTypeName" VisibleIndex="12">
                                    <PropertiesTextEdit MaxLength="200">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                          
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="13" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsPager NumericButtonCount="20" PageSize="20">
                            </SettingsPager>
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="false" VerticalScrollBarMode="Auto" 
                               ShowFilterRow="True" ShowFilterRowMenu="True" ShowStatusBar="Visible"
                               ShowHeaderFilterButton="True" VerticalScrollableHeight="450"
                               ShowGroupPanel="true" ShowGroupedColumns="false" ShowGroupFooter="VisibleIfExpanded" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                            <Templates>
                                <StatusBar>
                                    <dx:ASPxLabel ID="lblGrdFilter" runat="server" ClientInstanceName="lblGrdFilter" 
                                        Text="" ForeColor="Maroon">
                                    </dx:ASPxLabel>
                                </StatusBar>
                            </Templates>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabVenueType" Text="Venue Type">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxButton ID="btnAddVenueType" runat="server" ClientInstanceName="btnAddVenueType" EnableDefaultAppearance="False" Font-Underline="True"
                            ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False">
                            <Border BorderStyle="None" />
                        </dx:ASPxButton>
                        <dx:ASPxGridView ID="grdVenueType" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdVenueType"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnInitNewRow="grdView_InitNewRow"
                            OnCustomColumnSort="grdView_CustomColumnSort"
                            OnCustomCallback="grdVenueType_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnVenueTypeEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnVenueTypeUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnVenueTypeCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1" Settings-SortMode="Custom"
                                    SortOrder="Ascending" VisibleIndex="2" Width="100px">
                                    <PropertiesTextEdit MaxLength="2">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                                
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="4" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            
                            <SettingsPager NumericButtonCount="20" PageSize="20">
                            </SettingsPager>
                            
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="450" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabContest" Text="Product Type">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxButton ID="btnAddContest" runat="server" ClientInstanceName="btnAddContest" EnableDefaultAppearance="False" Font-Underline="True"
                            ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False">
                            <Border BorderStyle="None" />
                        </dx:ASPxButton>
                        <dx:ASPxGridView ID="grdContest" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdContest"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnInitNewRow="grdView_InitNewRow"
                            OnCustomColumnSort="grdView_CustomColumnSort"
                            OnCustomCallback="grdContest_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnContestEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnContestUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnContestCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1"
                                    SortOrder="Ascending" VisibleIndex="2" Width="100px" Settings-SortMode="Custom">
                                    <PropertiesTextEdit MaxLength="2">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>  
                                <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="4">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                              
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="5" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsPager NumericButtonCount="20" PageSize="20">
                            </SettingsPager>
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="450" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabProgressiveCount" Text="Progressive Count">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="grdProgressiveCount" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdProgressiveCount"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnCustomCallback="grdProgressiveCount_CustomCallback" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnProgressiveCountEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnProgressiveCountUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnProgressiveCountCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1"
                                    SortOrder="Ascending" VisibleIndex="2" Width="100px">
                                    <PropertiesTextEdit MaxLength="2">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                                
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="4" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="300" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabContainerType" Text="Container Type">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxButton ID="btnAddContainerType" runat="server" ClientInstanceName="btnAddContainerType" EnableDefaultAppearance="False" Font-Underline="True"
                            ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False">
                            <Border BorderStyle="None" />
                        </dx:ASPxButton>
                        <dx:ASPxGridView ID="grdContainerType" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdContainerType"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize"
                            OnCellEditorInitialize="grdView_CellEditorInitialize"
                            OnInitNewRow="grdView_InitNewRow"
                            OnCustomCallback="grdContainerType_CustomCallback"
                            OnCustomColumnSort="grdView_CustomColumnSort" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnContainerTypeEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnContainerTypeUpdate" Text="Update" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="btnContainerTypeCancel" Text="Cancel" Visibility="EditableRow">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Name="colCode" SortIndex="1" Settings-SortMode="Custom"
                                    SortOrder="Ascending" VisibleIndex="2" Width="100px">
                                    <PropertiesTextEdit MaxLength="2">
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="100">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>                                
                                <dx:GridViewDataCheckColumn Caption="Active" FieldName="Active" VisibleIndex="4" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="450" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabStatusStage" Text="Status Stage">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">                        
                        <dx:ASPxGridView ID="grdStatusStage" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdStatusStage"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>                                
                                
                                <dx:GridViewDataTextColumn Caption="Stage" FieldName="StageId" SortIndex="1" 
                                    SortOrder="Ascending" VisibleIndex="2" Width="100px">
                                    <PropertiesTextEdit>
                                        <ReadOnlyStyle BackColor="Gainsboro">
                                        </ReadOnlyStyle>
                                        <ValidationSettings Display="Dynamic">
                                            <RequiredField ErrorText="Code is required" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
                                    <PropertiesTextEdit MaxLength="200">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn> 
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                            <SettingsPager NumericButtonCount="20" PageSize="20">
                            </SettingsPager>
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="False" ShowHeaderFilterButton="True" VerticalScrollableHeight="450" VerticalScrollBarMode="Auto" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
</asp:Content>
