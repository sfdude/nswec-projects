﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="LoginCD.aspx.cs" Inherits="NSWEC_Barcode_Tracking.Login" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table width="100%">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="center">
                <asp:Login ID="ecLogin" runat="server" DisplayRememberMe="False" TitleText=""
                    OnAuthenticate="Login_Authenticate" >
                    <TextBoxStyle BorderStyle="Inset" Width="180px" />
                </asp:Login>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="center">
                <asp:Label ID="lblloginMessage" runat="server" ForeColor="Red"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="center">
                <dx:ASPxHyperLink id="hlRetrievePassword" runat="server" navigateurl="~/RetrievePassword.aspx" 
                    text="Forgot your password?">
                </dx:ASPxHyperLink>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="center">
                <dx:ASPxLabel ID="lblAdobeNotice" ClientInstanceName="lblAdobeNotice" runat="server" 
                    Text="Adobe Reader is required, please download from " 
                    ForeColor="Red" ClientVisible="False">
                </dx:ASPxLabel>
                <dx:ASPxHyperLink ID="adobeLink" ClientInstanceName="adobeLink" runat="server" 
                    NavigateUrl="http://get.adobe.com/reader/" Target="_blank" Text="here" 
                    ClientVisible="False">
                </dx:ASPxHyperLink>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
