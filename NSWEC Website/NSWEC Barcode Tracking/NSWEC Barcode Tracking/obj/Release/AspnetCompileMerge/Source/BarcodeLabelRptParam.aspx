﻿<%@ Page Title="Barcode Label Report" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="BarcodeLabelRptParam.aspx.cs" Inherits="NSWEC_Barcode_Tracking.BarcodeLabelRptParam" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:ASPxPageControl ID="pcRptParam" runat="server" ClientInstanceName="pcRptParam" ActiveTabIndex="0" Width="100%">
        <TabPages>
            <dx:TabPage Name="tabRptParam" Text="Barcode Label Report">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxCallbackPanel ID="cpReport" runat="server" Width="100%" ClientInstanceName="cpReport" OnCallback="cpReport_Callback">
                            <PanelCollection>
                                <dx:PanelContent runat="server">
                                    <table style="width:100%;">
                                        <tr>
                                            <td width="33%">
                                                <dx:ASPxMenu id="menAction" runat="server" ClientInstanceName="menAction" ShowPopOutImages="True" >
                                                    <border borderstyle="None"></border>
                                                    <items>                        
                                                        <dx:MenuItem Name="btnPrint" Text="Print">
                                                        </dx:MenuItem>                      
                                                    </items>
                                                </dx:ASPxMenu>
                                            </td>
                                            <td width="33%">&nbsp;
                                                
                                            </td>
                                            <td width="34%">
                                                
                                                <dx:ASPxCheckBox ID="cbExportCSV" runat="server" CheckState="Unchecked" ClientInstanceName="cbExportCSV" Text="Export to CSV">
                                                </dx:ASPxCheckBox>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33%" valign="bottom">
                                                <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Barcode Source">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="33%" valign="bottom">&nbsp;</td>
                                            <td width="34%" valign="bottom">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td width="50%">
                                                            <dx:ASPxCheckBox ID="cbIncludeStatus" runat="server" CheckState="Unchecked" ClientInstanceName="cbIncludeStatus" Text="Include Status History">
                                                            </dx:ASPxCheckBox>
                                                        </td>
                                                        <td width="50%">
                                                            <dx:ASPxCheckBox ID="cbIncludeULD" runat="server" CheckState="Unchecked" ClientInstanceName="cbIncludeULD" Text="Include ULD Child">
                                                            </dx:ASPxCheckBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33%">
                                                <dx:ASPxComboBox ID="cmbSource" runat="server" ClientInstanceName="cmbSource" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" Width="80%">
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td width="33%">&nbsp;</td>
                                            <td width="34%">
                                                
                                            </td>
                                        </tr>                            
                                        <tr>
                                            <td width="33%">
                                                <dx:ASPxLabel ID="lblEMO" runat="server" Text="Election Manager Office">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="33%">
                                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Venue">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="34%">
                                                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Container Type">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33%">
                                                <dx:ASPxComboBox ID="cmbEMO" runat="server" ClientInstanceName="cmbEMO" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td width="33%">
                                                <dx:ASPxComboBox ID="cmbVenue" runat="server" ClientInstanceName="cmbVenue" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code">
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td width="34%">
                                                <dx:ASPxComboBox ID="cmbContainerType" runat="server" ClientInstanceName="cmbContainerType" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33%">
                                                <dx:ASPxLabel ID="lblDistrict" runat="server" Text="District">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="33%">
                                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Venue Type">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="34%">
                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Last Scanned Location">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33%">
                                                <dx:ASPxComboBox ID="cmbDistrict" runat="server" ClientInstanceName="cmbDistrict" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td width="33%">
                                                <dx:ASPxComboBox ID="cmbVenueType" runat="server" ClientInstanceName="cmbVenueType" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td width="34%">
                                                <dx:ASPxComboBox ID="cmbLocation" runat="server" ClientInstanceName="cmbLocation" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33%">
                                                <dx:ASPxLabel ID="lblContestArea" runat="server" Text="Contest Area">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="33%">
                                                <dx:ASPxLabel ID="lblProductType" runat="server" Text="Product Type">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="34%">
                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Last Status Stage">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33%">
                                                <dx:ASPxComboBox ID="cmbContestArea" runat="server" ClientInstanceName="cmbContestArea" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td width="33%">
                                                <dx:ASPxComboBox ID="cmbProduct" runat="server" ClientInstanceName="cmbProduct" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="Code" >
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td width="34%">
                                                <dx:ASPxComboBox ID="cmbStatusStage" runat="server" ClientInstanceName="cmbStatusStage" Width="80%"
                                                    IncrementalFilteringMode="StartsWith" TextField="Stage" ValueField="StageId" >
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                        
                                        
                                    </table>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
					</dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>        
    </dx:ASPxPageControl>
    <dx:ASPxCallback ID="cbEnquiry" runat="server" ClientInstanceName="cbEnquiry" OnCallback="cbEnquiry_Callback">
    </dx:ASPxCallback>
</asp:Content>
