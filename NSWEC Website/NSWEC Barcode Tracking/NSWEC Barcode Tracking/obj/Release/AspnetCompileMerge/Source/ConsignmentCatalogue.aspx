﻿<%@ Page Title="Consignment Catalogue" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConsignmentCatalogue.aspx.cs" Inherits="NSWEC_Barcode_Tracking.ConsignmentCatalogue" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <%-- 
    <style>
    [class*=dxGridView_gvHeaderFilter] {
        background: #00CEA6;
    }
    </style>
--%>
    <dx:ASPxPageControl ID="pcConnote" runat="server" ClientInstanceName="pcConnote" ActiveTabIndex="0" Width="100%">
        <TabPages>
            <dx:TabPage Name="tabBarcode" Text="Consignment Barcodes">
                <ActiveTabStyle Font-Bold="True">
                </ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <dx:ASPxMenu id="mGridViewExport" runat="server" ClientInstanceName="mGridViewExport"
                                        ShowPopOutImages="True" OnItemClick="mGridViewExport_ItemClick" HorizontalAlign="Right" ShowAsToolbar="True">
                                        <items>
                                            <dx:MenuItem Name="itmPrintLabel" Text="Print Label" ToolTip="Print selected barcode label">
                                            </dx:MenuItem>
                                            <dx:MenuItem Name="itmCustomisation" Text="Customisation">
                                                <Items>
                                                    <dx:MenuItem Name="itmHeadFilterMode" Text="Header Filter Mode" >                                            
                                                        <Items>
                                                            <dx:MenuItem Name="itmCheckedList" Text="Checked List">
                                                            </dx:MenuItem>
                                                            <dx:MenuItem Name="itmList" Text="List">
                                                            </dx:MenuItem>
                                                        </Items>
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Name="itmClearFilter" Text="Clear Filter" ToolTip="Clear Barcode Data Filter in Gridview">
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Name="itmShowFieldChooser" Text="Show Field Chooser">
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Name="itmHideFieldChooser" Text="Hide Field Chooser">
                                                    </dx:MenuItem>
                                                    <dx:MenuItem Name="itmGridLayout" Text="Grid Layout">                                            
                                                    </dx:MenuItem>
                                                </Items>  
                                            </dx:MenuItem>                                                            
                                            <dx:MenuItem Name="itmExport" Text="Export">                                    
                                                <Items>
                                                <dx:MenuItem Name="itmExpCSV" Text="Export to CSV"></dx:MenuItem>
                                                <dx:MenuItem Name="itmExpPDF" Text="Export to PDF"></dx:MenuItem>
                                                <dx:MenuItem Name="itmExpRTF" Text="Export to RTF"></dx:MenuItem>
                                                <dx:MenuItem Name="itmExpXLS" Text="Export to XLS"></dx:MenuItem>
                                                <dx:MenuItem Name="itmExpXLSX" Text="Export to XLSX"></dx:MenuItem>
                                                </Items>
                                            </dx:MenuItem>
                                            
                                            <dx:MenuItem Name="itmRefresh" Text="Refresh" ToolTip="Refresh Barcode Data in Gridview">
                                            </dx:MenuItem>                                            
                                        </items>
                                        <Border BorderStyle="None" />
                                    </dx:ASPxMenu>
                                </td>
                            </tr>
                            <tr>
                                <td>                                    
                                    <dx:ASPxGridView ID="grdConnoteBarcode" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdConnoteBarcode"
                                        KeyboardSupport="True" KeyFieldName="ConsKey;LineNo" Width="100%" 
                                        OnCustomJSProperties="grdView_CustomJSProperties"
                                        OnCustomButtonInitialize="grdView_CustomButtonInitialize"
                                        OnCommandButtonInitialize="grdView_CommandButtonInitialize" 
                                        OnCustomCallback="grdConnoteBarcode_CustomCallback"
                                        OnAfterPerformCallback="grdConnoteBarcode_AfterPerformCallback" 
                                        OnCustomUnboundColumnData="grdConnote_CustomUnboundColumnData"
                                        OnBeforeGetCallbackResult="grdConnoteBarcode_BeforeGetCallbackResult" >
                                        <Columns>
                                            <dx:GridViewCommandColumn FixedStyle="Left" ShowSelectCheckbox="True" VisibleIndex="0"
                                                Width="30px">
                                                <%-- 
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <dx:ASPxCheckBox ID="cbAll" runat="server" ClientInstanceName="cbAll" 
                                                        ToolTip="Select for Printing Label" 
                                                        OnInit="cbAll_Init">
                                                    </dx:ASPxCheckBox>
                                                </HeaderTemplate>
                                                --%>
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="ConsKey" Visible="False" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="LineNo" Visible="False" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction"
                                                VisibleIndex="3" Width="70px">
                                                <CustomButtons>
                                                    <dx:GridViewCommandColumnCustomButton ID="btnPrintConnote" Text="Print">
                                                    </dx:GridViewCommandColumnCustomButton>
                                                    <dx:GridViewCommandColumnCustomButton ID="btnViewConnote" Text="View">
                                                    </dx:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                            </dx:GridViewCommandColumn>
                                            <%-- 
                                            <dx:GridViewDataTextColumn FieldName="EventId" Caption="Event ID" VisibleIndex="3" GroupIndex="0" SortIndex="0" SortOrder="Ascending">
                                            </dx:GridViewDataTextColumn>
                                            --%>
                                            <dx:GridViewDataTextColumn FieldName="Source" VisibleIndex="4" GroupIndex="0" SortIndex="0" SortOrder="Ascending">
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewDataTextColumn FieldName="CDID" Visible="False" VisibleIndex="5">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="ReturningOfficeName" Visible="False" VisibleIndex="6">
                                            </dx:GridViewDataTextColumn>
                                            <%-- 
                                            <dx:GridViewDataTextColumn FieldName="ReturningOffice" Caption="EMO" VisibleIndex="6" GroupIndex="1" SortIndex="1" SortOrder="Ascending">
                                            </dx:GridViewDataTextColumn>
                                            --%>
                                            <dx:GridViewDataTextColumn FieldName="ElectionManagerOffice" VisibleIndex="7" GroupIndex="1" SortIndex="1" SortOrder="Ascending">
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewDataTextColumn FieldName="District" VisibleIndex="8" GroupIndex="2" SortIndex="2" SortOrder="Ascending">
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewDataTextColumn FieldName="ContestAreaId" Visible="False" VisibleIndex="9">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="ContestArea" Visible="False" VisibleIndex="10" GroupIndex="3" SortIndex="3" SortOrder="Ascending">
                                            </dx:GridViewDataTextColumn>
                                            <%-- --%>
                                            <dx:GridViewDataTextColumn FieldName="WardCode" Visible="False" VisibleIndex="10">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Ward" Visible="False" VisibleIndex="10" GroupIndex="3" SortIndex="3" SortOrder="Ascending" >
                                            </dx:GridViewDataTextColumn>                                            
                                            <dx:GridViewDataTextColumn FieldName="VenueCode" Visible="False" VisibleIndex="11">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="VenueName" Visible="False" VisibleIndex="12">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Venue" VisibleIndex="13" GroupIndex="4" SortIndex="4" SortOrder="Ascending">
                                            </dx:GridViewDataTextColumn>
                                                                                        
                                            <dx:GridViewDataTextColumn FieldName="Barcode" VisibleIndex="14" Width="100px"
                                                Settings-AllowAutoFilterTextInputTimer="False">
                                                <Settings AllowAutoFilterTextInputTimer="False"></Settings>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ULD Parent" FieldName="ParentBarcode" VisibleIndex="15" Width="100px"
                                                 Settings-AllowAutoFilterTextInputTimer="False">
                                                <Settings AllowAutoFilterTextInputTimer="False"></Settings>
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewDataTextColumn FieldName="VenueTypeCode" Visible="False" VisibleIndex="16">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="VenueTypeName" Visible="False" VisibleIndex="17">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="VenueType" VisibleIndex="18" Width="150px">
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewDataTextColumn FieldName="ContestCode" Visible="False" VisibleIndex="19">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="ContestName" Visible="False" VisibleIndex="20">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="ProductType" VisibleIndex="21" Width="100px">
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewDataTextColumn FieldName="ContainerCode" Visible="False" VisibleIndex="22">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="ContainerName" Visible="False" VisibleIndex="23">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="ContainerType" VisibleIndex="24" Width="110px">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataSpinEditColumn Caption="Total Labels Printed" Visible="false" FieldName="ContainerQuantity" VisibleIndex="25">
                                                <PropertiesSpinEdit DisplayFormatString="g" NumberType="Integer">
                                                    <SpinButtons ShowIncrementButtons="False">
                                                    </SpinButtons>
                                                </PropertiesSpinEdit>
                                            </dx:GridViewDataSpinEditColumn>
                                            <%--
                                            <dx:GridViewDataCheckColumn FieldName="Activated" VisibleIndex="21" Width="60px">
                                                <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                                </PropertiesCheckEdit>
                                            </dx:GridViewDataCheckColumn>
                                            --%>
                                            <dx:GridViewDataDateColumn FieldName="Deleted" Visible="false" VisibleIndex="27">
                                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm" EditFormat="Custom" EditFormatString="dd/MM/yyyy HH:mm">
                                                    <Style HorizontalAlign="Right"></Style>
                                                </PropertiesDateEdit>
                                            </dx:GridViewDataDateColumn>                                            
                                            <dx:GridViewDataTextColumn Caption="Last Location" FieldName="LocationScanned" VisibleIndex="28" Width="150px">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Last Status" FieldName="StatusNotes" VisibleIndex="29" Width="200px">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataDateColumn Caption="Last Scanned" FieldName="StatusDate" VisibleIndex="30" Width="150px">
                                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss" EditFormat="Custom" EditFormatString="dd/MM/yyyy HH:mm:ss">
                                                    <Style HorizontalAlign="Right"></Style>
                                                </PropertiesDateEdit>
                                                <Settings ShowFilterRowMenu="False" ShowInFilterControl="False" />
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn FieldName="CountCentre" VisibleIndex="31" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataCheckColumn Caption="Deactivated" FieldName="Deactivated" Name="colDeactivated" ShowInCustomizationForm="True" UnboundType="String" VisibleIndex="26">
                                                <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                                </PropertiesCheckEdit>
                                            </dx:GridViewDataCheckColumn>
                                        </Columns>
                                        <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                                        <%-- comments 
                                        <SettingsPager NumericButtonCount="100">                                            
                                        </SettingsPager>
--%>
                                        <Settings ShowHorizontalScrollBar="True" ShowHeaderFilterButton="True" ShowFilterRow="True" ShowFilterRowMenu="True" ShowStatusBar="Visible" 
                                            VerticalScrollBarMode="Auto" VerticalScrollableHeight="450" 
                                            ShowGroupPanel="true" ShowGroupedColumns="false" ShowGroupFooter="VisibleIfExpanded" ShowTitlePanel="True"/>
                                        <Styles>
                                            <AlternatingRow Enabled="True">
                                            </AlternatingRow>
                                            <TitlePanel BackColor="#007D86" ForeColor="White" HorizontalAlign="Center">
                                            </TitlePanel>
                                        </Styles>                                        
                                        <Templates>
                                            <TitlePanel>
                                                <dx:ASPxLabel ID="lblGrdConnoteFilter" runat="server" ClientInstanceName="lblGrdConnoteFilter" 
                                                    ForeColor="White" Text="">
                                                </dx:ASPxLabel>
                                            </TitlePanel>
                                            <StatusBar>
                                                <dx:ASPxLabel ID="lblGrdConnoteFilter" runat="server" ClientInstanceName="lblGrdConnoteFilter" 
                                                    Text="" ForeColor="Maroon">
                                                </dx:ASPxLabel>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup>
                                            <HeaderFilter Height="200px" MinHeight="300px" Width="250px" />
                                        </SettingsPopup>
                                        <SettingsCustomizationWindow Enabled="True" Height="300px" PopupHorizontalAlign="WindowCenter" 
                                            PopupVerticalAlign="WindowCenter" Width="300px" />
<%--
                                        <Images>
                                            <HeaderFilter>
                                                 
                                                <SpriteProperties CssClass="dxGridView_gvHeaderFilter" />
                                                    
                                            </HeaderFilter>
                                        </Images>
--%>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        
    </dx:ASPxPageControl>
    <dx:ASPxCallback ID="cbPrintLabel" runat="server" ClientInstanceName="cbPrintLabel" OnCallback="cbPrintLabel_Callback" >
    </dx:ASPxCallback> 
    <dx:ASPxCallback ID="cbConnote" runat="server" ClientInstanceName="cbConnote" OnCallback="cbConnote_Callback" >
    </dx:ASPxCallback>
    <dx:ASPxHiddenField ID="hfHiddenInfo" runat="server" ClientInstanceName="hfHiddenInfo"></dx:ASPxHiddenField> 
</asp:Content>
