﻿<%@ Page Title="Import PPDM" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ImportPPDM.aspx.cs" Inherits="NSWEC_Barcode_Tracking.ImportPPDM" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   
    <dx:ASPxPageControl ID="pcImport" runat="server" ActiveTabIndex="0" Width="100%">
    <TabPages>
        <dx:TabPage Name="tabImport" Text="Import PPDMs">
            <ActiveTabStyle Font-Bold="True">
            </ActiveTabStyle>
            <ContentCollection>
                <dx:ContentControl runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td align="left" width="30%">
                                <dx:ASPxMenu ID="menuAction" runat="server" ClientInstanceName="menuAction">
                                    <Items>
                                        <dx:MenuItem Name="itmImport" Text="Import" ToolTip="Import PPDM File">
                                        </dx:MenuItem>
                                        <dx:MenuItem Name="itmUpdateSlave" Text="Update Slave" ToolTip="Update Slave DB">
                                        </dx:MenuItem>
                                    </Items>
                                </dx:ASPxMenu>
                            </td>
                            <td align="right" width="50%">
                                
                            </td>
                            <td width="20%">
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="30%" valign="top">
                                <div class="uploadContainer">
                                    <dx:ASPxUploadControl ID="ucSource" runat="server" ClientInstanceName="ucSource" 
                                        NullText="Select files..." UploadMode="Advanced" ShowUploadButton="True" ShowProgressPanel="True"
                                        OnFileUploadComplete="ucSource_FileUploadComplete" Width="99%">
                                        <AdvancedModeSettings EnableMultiSelect="True" EnableFileList="True" TemporaryFolder="~/Upload/" />
                                        <ValidationSettings AllowedFileExtensions=".csv,.txt" 
                                            NotAllowedFileExtensionErrorText="This file extension isn't allowed; Accepted files extension &quot;.csv, .txt&quot;">
                                        </ValidationSettings>
                                    </dx:ASPxUploadControl>
                                </div>    
                            </td>
                            <td align="left" width="50%" valign="top">
                                <dx:ASPxRoundPanel ID="rpFileContainer" runat="server" ShowCollapseButton="true" Width="100%" HeaderText="Uploaded files">
                                    <PanelCollection>
                                        <dx:PanelContent runat="server">
                                            <dx:ASPxListBox ID="fileList" runat="server" ClientInstanceName="fileList" Width="100%" Rows="10">
                                            </dx:ASPxListBox>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxRoundPanel>
                            </td>
                            <td width="20%" valign="top">                                
                                <dx:ASPxCheckBox ID="cbAppendMode" runat="server" CheckState="Unchecked" ClientInstanceName="cbAppendMode" Text="Append Mode">
                                </dx:ASPxCheckBox>
                                <dx:ASPxCheckBox ID="cbVersionUpdate" runat="server" Checked="True" CheckState="Unchecked" ClientInstanceName="cbVersionUpdate" Text="Version Update">
                                </dx:ASPxCheckBox>
                                <dx:ASPxCheckBox ID="cbSlaveDBUpdate" runat="server" CheckState="Unchecked" ClientInstanceName="cbSlaveDBUpdate" Text="Slave DB Update">
                                </dx:ASPxCheckBox>
                            </td>
                        </tr>                        
                        
                    </table>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
</dx:ASPxPageControl>
    <dx:ASPxCallback ID="cbImport" runat="server" ClientInstanceName="cbImport" OnCallback="cbImport_Callback" >
    </dx:ASPxCallback>
    <dx:ASPxCallback ID="cbUpdateSlave" runat="server" ClientInstanceName="cbUpdateSlave" OnCallback="cbUpdateSlave_Callback" >
    </dx:ASPxCallback>

</asp:Content>
