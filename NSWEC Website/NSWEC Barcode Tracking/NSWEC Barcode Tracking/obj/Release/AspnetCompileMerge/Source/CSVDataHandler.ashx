<%@ WebHandler Language="C#" Class="NSWEC_Barcode_Tracking.CSVDataHandler" %>

using System;
using System.Text;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Collections.Generic;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public class CSVDataHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {           
            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
           
            if (HttpContext.Current.Session["dsCSV"] != null)
            {
                DataSet dsDataSource = (DataSet)HttpContext.Current.Session["dsCSV"];
                if (dsDataSource != null && dsDataSource.Tables != null && dsDataSource.Tables.Count > 0)
                {
                    if (dsDataSource.Tables.Count == 1)
                    {
                        string fileName = String.Format("DownloadCSV_{0}.csv", DateTime.Now.ToString("yyyyMMddHHmmss"));
                        if (!string.IsNullOrEmpty(dsDataSource.Tables[0].TableName))
                            fileName = String.Format("{0}_{1}.csv", dsDataSource.Tables[0].TableName, DateTime.Now.ToString("yyyyMMddHHmmss"));
                       
                        context.Response.ContentType = "application/csv";
                        context.Response.AddHeader("Content-Disposition", String.Format(null, "attachment; filename={0}", fileName));
                        //Dump the CSV content to context.Response
                        context.Response.Write(ECWebClass.DataTableToCSV(dsDataSource.Tables[0], ",", true));

                        context.Response.Flush();
                        context.Response.End();
                    }
                    else // zip
                    {
                        string fileName = String.Format("DownloadCSV_{0}.zip", DateTime.Now.ToString("yyyyMMddHHmmss"));
                        if (!string.IsNullOrEmpty(dsDataSource.Tables[0].TableName))
                            fileName = String.Format("{0}_{1}.zip", dsDataSource.Tables[0].TableName, DateTime.Now.ToString("yyyyMMddHHmmss"));
                       
                        byte[] zipFileData = ECWebClass.DataSetToCSVZip(dsDataSource);// File.ReadAllBytes(fileName);
                        context.Response.ContentType = "application/zip";
                        context.Response.AddHeader("Content-Disposition", String.Format(null, "attachment; filename={0}", fileName));
                        context.Response.BinaryWrite(zipFileData);
                        context.Response.Flush();
                        context.Response.End();
                    }
                    HttpContext.Current.Session.Remove("dsCSV");
                }
            }
        }

        public bool IsReusable
        {
            get { return false; }
        }
        
    }
}
