﻿<%@ Page Title="Barcode Scan Summary Report" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="BarcodeScanSummaryRptParam.aspx.cs" Inherits="NSWEC_Barcode_Tracking.BarcodeScanSummaryRptParam" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table width="100%"> 
        <tr>
            <td align="left" valign="top" colspan="2">
                <dx:ASPxMenu id="menAction" runat="server" ClientInstanceName="menAction" ShowPopOutImages="True" >
                    <border borderstyle="None"></border>
                    <items>                        
                        <dx:MenuItem Name="btnPrint" Text="Print">
                        </dx:MenuItem>                      
                    </items>
                </dx:ASPxMenu>                               
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 50%" valign="top">
                <dx:ASPxRoundPanel runat="server" HeaderText="Date Range" Width="100%" ID="rpDateRange" >
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <table width="100%">
                                <tr>
                                    <td style="WIDTH: 5%" align="left">                                    
                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="From:">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td style="WIDTH: 45%"; align="left">                                    
                                        <dx:ASPxDateEdit ID="edtDateFrom" ClientInstanceName="edtDateFrom" runat="server" 
                                            DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy" 
                                            HorizontalAlign="Right" Width="100%">
                                        </dx:ASPxDateEdit>
                                        
                                    </td>
                                    <td style="WIDTH: 5%" align="left">
                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="To:">
                                        </dx:ASPxLabel>  
                                    </td>
                                    <td style="WIDTH: 45%; " align="left">
                                        <dx:ASPxDateEdit ID="edtDateTo" ClientInstanceName="edtDateTo" runat="server" 
                                            DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy" 
                                            HorizontalAlign="Right" Width="100%">
                                        </dx:ASPxDateEdit>
                                    </td>
                                </tr>
                            </table>                            
                        </dx:PanelContent>
                    </PanelCollection>
                    <ContentPaddings Padding="0px" />
                </dx:ASPxRoundPanel>
            </td>
            <td style="width: 50%" valign="top">
            </td>
        </tr>
        
    </table>
    <dx:ASPxCallback ID="cbEnquiry" runat="server" ClientInstanceName="cbEnquiry" OnCallback="cbEnquiry_Callback">
    </dx:ASPxCallback> 
</asp:Content>
