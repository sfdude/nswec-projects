﻿<%@ Page Title="User Catalogue" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="UserCatalogue.aspx.cs" Inherits="NSWEC_Barcode_Tracking.UserCatalogue" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <dx:ASPxPageControl ID="pcUser" runat="server" ClientInstanceName="pcUser" ActiveTabIndex="0" 
        Width="100%" EnableCallBacks="True">
        <TabPages>
            <dx:TabPage Name="tabUserCatalogue" Text="User Catalogue">
                <ActiveTabStyle Font-Bold="True"></ActiveTabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                    <dx:ASPxButton ID="btnAdd" runat="server" ClientInstanceName="btnAdd" EnableDefaultAppearance="False" Font-Underline="True"
                        ForeColor="#3399FF" Text="New" Cursor="pointer" AutoPostBack="False">
                        <Border BorderStyle="None" />
                    </dx:ASPxButton>
                        <%-- comments --%>
                        <dx:ASPxGridView ID="grdUser" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdUser"
                            KeyboardSupport="True" KeyFieldName="ID" Width="100%"
                            OnCommandButtonInitialize="grdView_CommandButtonInitialize" OnCustomUnboundColumnData="grdUser_CustomUnboundColumnData" >
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" FixedStyle="Left" Name="colAction" VisibleIndex="1"
                                    Width="60px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnEdit" Text="Edit">
                                            <Styles>
                                                <Style ForeColor="White">
                                                </Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                    <CellStyle BackColor="#007D86">
                                    </CellStyle>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="ReturningOffice" Caption="Election Manager Office" FixedStyle="Left" GroupIndex="0" SortIndex="0"
                                    SortOrder="Ascending" VisibleIndex="2" Width="200px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Login Name" FieldName="LoginId" VisibleIndex="3"
                                    Width="250px" Visible="False">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Contact" UnboundType="String" VisibleIndex="5" Visible="False" >
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="Active" VisibleIndex="7" Width="100px">
                                    <PropertiesCheckEdit ValueChecked="Y" ValueType="System.Char" ValueUnchecked="N">
                                    </PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Login Name" FieldName="Login_Id" ShowInCustomizationForm="True" UnboundType="String" VisibleIndex="4" Width="250px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Contact" FieldName="Contact1" ShowInCustomizationForm="True" UnboundType="String" VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                                
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" EnableRowHotTrack="True" AutoExpandAllGroups="True" />
                            <Settings ShowHeaderFilterButton="True" ShowFilterRowMenu="True" VerticalScrollableHeight="300" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                            <SettingsPager PageSize="15">
                            </SettingsPager>
                        </dx:ASPxGridView>
                        
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        <ContentStyle HorizontalAlign="Left">
        </ContentStyle> 
    </dx:ASPxPageControl>
    <dx:ASPxCallback ID="cbUser" runat="server" ClientInstanceName="cbUser" OnCallback="cbUser_Callback">
    </dx:ASPxCallback>
</asp:Content>
