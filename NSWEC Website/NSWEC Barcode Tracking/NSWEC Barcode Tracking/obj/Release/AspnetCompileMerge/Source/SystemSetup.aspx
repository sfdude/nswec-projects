﻿<%@ Page Title="System Setup" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SystemSetup.aspx.cs" Inherits="NSWEC_Barcode_Tracking.SystemSetup" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table width="100%">
        <tr>
            <td align="left" valign="top">
                <dx:ASPxMenu id="menAction" runat="server" ClientInstanceName="menAction" ShowPopOutImages="True" >
                    <border borderstyle="None"></border>
                    <items>
                        <dx:MenuItem Name="btnSave" Text="Save">
                            <Items>
                                <dx:MenuItem Name="btnSaveExit" Text="Save &amp; Exit">
                                </dx:MenuItem>
                            </Items>
                        </dx:MenuItem>                  
                    </items>
                </dx:ASPxMenu>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <dx:ASPxPageControl ID="pcSystem" runat="server" ActiveTabIndex="0" ClientInstanceName="pcSystem"
                    Width="100%">
                    <TabPages>
                        <dx:TabPage Name="tabSystem" Text="System Information">
                            <ActiveTabStyle Font-Bold="True">
                            </ActiveTabStyle>
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="App Name">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtAppName" runat="server" ClientInstanceName="edtAppName" MaxLength="100" Width="100%">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 30%">
                                                <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="Session Time out">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%">
                                                <dx:ASPxSpinEdit ID="edtTimeout" runat="server" ClientInstanceName="edtTimeout" Height="21px"
                                                    HorizontalAlign="Right" MaxValue="100" MinValue="1" Number="0" NumberType="Integer"
                                                    Width="50%">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Web Site Title">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtWebSiteTitle" runat="server" ClientInstanceName="edtWebSiteTitle" MaxLength="100" Width="100%">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 30%">
                                                <dx:ASPxLabel ID="ASPxLabel25" runat="server" Text="Password Expiry (days)">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%">
                                                <dx:ASPxSpinEdit ID="edtPwdExpiryDays" runat="server" ClientInstanceName="edtPwdExpiryDays" Height="21px"
                                                    HorizontalAlign="Right" MaxValue="100" MinValue="1" Number="0" NumberType="Integer"
                                                    Width="50%">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" valign="top" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Web Info">
                                                </dx:ASPxLabel>                                                
                                            </td>
                                            <td align="left" valign="top" style="width: 30%" rowspan="4">
                                                <dx:ASPxMemo ID="memoWebInfo" runat="server" ClientInstanceName="memoWebInfo" Height="100%"
                                                    Width="100%">
                                                </dx:ASPxMemo>                                                
                                            </td>
                                            <td align="right" style="width: 30%">
                                                <dx:ASPxLabel ID="ASPxLabel21" runat="server" Text="Minimum Password Character">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%">
                                                <dx:ASPxSpinEdit ID="edtMinPwdChar" runat="server" ClientInstanceName="edtMinPwdChar" Height="21px"
                                                    HorizontalAlign="Right" MaxValue="100" MinValue="1" Number="0" NumberType="Integer"
                                                    Width="50%">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                
                                            </td>
                                            <td align="right" style="width: 30%">
                                                <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="Minimum Password Uppercase Character">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%">
                                                <dx:ASPxSpinEdit ID="edtMinPwdUpperChar" runat="server" ClientInstanceName="edtMinPwdUpperChar" Height="21px"
                                                    HorizontalAlign="Right" MaxValue="100" MinValue="1" Number="0" NumberType="Integer"
                                                    Width="50%">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                
                                            </td>
                                            <td align="right" style="width: 30%">
                                                <dx:ASPxLabel ID="ASPxLabel23" runat="server" Text="Minimum Password Special Character">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%">
                                                <dx:ASPxSpinEdit ID="edtMinPwdSpecialChar" runat="server" ClientInstanceName="edtMinPwdSpecialChar" Height="21px"
                                                    HorizontalAlign="Right" MaxValue="100" MinValue="1" Number="0" NumberType="Integer"
                                                    Width="50%">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%" >
                                            </td>
                                            <td align="right" style="width: 30%" >
                                                <dx:ASPxLabel ID="ASPxLabel29" runat="server" Text="Minimum Password Numeric Character">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%" >
                                                <dx:ASPxSpinEdit ID="edtMinPwdNumericChar" runat="server" ClientInstanceName="edtMinPwdNumericChar" Height="21px" HorizontalAlign="Right" MaxValue="100" MinValue="1" Number="0" NumberType="Integer" Width="50%">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%" >
                                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Support Login">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%" >
                                                <dx:ASPxTextBox ID="edtSupportLogin" runat="server" ClientInstanceName="edtSupportLogin" Height="21px" MaxLength="100" Width="100%">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 30%" >
                                                <dx:ASPxLabel ID="ASPxLabel31" runat="server" Text="Minimum Password Length">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%" >
                                                <dx:ASPxSpinEdit ID="edtMinPwdLength" runat="server" ClientInstanceName="edtMinPwdLength" Height="21px" HorizontalAlign="Right" 
                                                    MaxValue="100" MinValue="1" Number="0" NumberType="Integer" Width="50%">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%" >
                                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Support Password">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%" >
                                                <dx:ASPxTextBox ID="edtSupportPassword" runat="server" ClientInstanceName="edtSupportPassword" Width="100%" OnPreRender="edtSupportPassword_PreRender" Password="True">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 30%" >
                                                <dx:ASPxLabel ID="ASPxLabel30" runat="server" Text="Maximum Password Length">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%" >
                                                <dx:ASPxSpinEdit ID="edtMaxPwdLength" runat="server" ClientInstanceName="edtMaxPwdLength" Height="21px" HorizontalAlign="Right" MaxValue="100" MinValue="1" Number="0" NumberType="Integer" Width="50%">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Repeat Password">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtPasswordRepeat" runat="server" ClientInstanceName="edtPasswordRepeat"
                                                    OnPreRender="edtSupportPassword_PreRender" Password="True" Width="100%">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" ValidateOnLeave="False">
                                                        <RequiredField ErrorText="Password is required" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 30%">
                                                <dx:ASPxLabel ID="ASPxLabel24" runat="server" Text="Password Special Character List">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%">
                                                <dx:ASPxTextBox ID="edtPwdSpecCharList" runat="server" ClientInstanceName="edtPwdSpecCharList" 
                                                    MaxLength="100" Width="100%">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">&nbsp;</td>
                                            <td align="left" style="width: 30%">&nbsp;</td>
                                            <td align="right" style="width: 30%">&nbsp;</td>
                                            <td align="left" style="width: 25%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%; ">
                                                <dx:ASPxLabel ID="ASPxLabel32" runat="server" Text="Event Type">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%; height: 16px" valign="top">
                                                <dx:ASPxComboBox ID="cmbEventType" runat="server" ClientInstanceName="cmbEventType" Width="100%">
                                                    <Items>
                                                        <dx:ListEditItem Text="Not Defined" Value="0" />
                                                        <dx:ListEditItem Text="Local Government" Value="1" />
                                                        <dx:ListEditItem Text="State By-Election" Value="2" />
                                                        <dx:ListEditItem Text="State Election" Value="3" />
                                                    </Items>
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td align="right" style="width: 30%; ">
                                                <dx:ASPxLabel ID="ASPxLabel33" runat="server" Text="Preferred Label">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%; ">
                                                <dx:ASPxComboBox ID="cmbLabel" runat="server" ClientInstanceName="cmbLabel" 
                                                    IncrementalFilteringMode="StartsWith" TextField="LABEL_NAME" ValueField="ID"
                                                    ValueType="System.Int32" Width="100%" EnableIncrementalFiltering="True" >
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%; ">&nbsp;</td>
                                            <td align="left" style="width: 30%; height: 16px" valign="top">&nbsp;</td>
                                            <td align="right" style="width: 30%; ">&nbsp;</td>
                                            <td align="left" style="width: 25%; ">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%; ">&nbsp;</td>
                                            <td align="left" style="width: 30%; height: 16px" valign="top">&nbsp;</td>
                                            <td align="right" style="width: 30%; ">
                                                <dx:ASPxLabel ID="ASPxLabel34" runat="server" Text="PPDM Version: ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%; ">
                                                <dx:ASPxLabel ID="lblPPDMVer" runat="server" Text="lblPPDMVer">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%; ">&nbsp;</td>
                                            <td align="left" style="width: 30%; height: 16px" valign="top">&nbsp;</td>
                                            <td align="right" style="width: 30%; ">
                                                <dx:ASPxLabel ID="ASPxLabel35" runat="server" Text="PPDM Loaded: ">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 25%; ">
                                                <dx:ASPxLabel ID="lblPPDMLoaded" runat="server" Text="lblPPDMLoaded">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="tabCommSetting" Text="Communication Settings">
                            <ActiveTabStyle Font-Bold="True">
                            </ActiveTabStyle>
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Mail Host">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtMailHost" runat="server" ClientInstanceName="edtMailHost" 
                                                    MaxLength="50" Width="100%">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 20%">
                                                <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="Port">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtPort" runat="server" ClientInstanceName="edtPort" Width="50px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="left" style="width: 5%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="User Name">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtUserName" runat="server" ClientInstanceName="edtUserName" 
                                                    MaxLength="50" Width="100%">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 20%">
                                                
                                            </td>
                                            <td align="left" style="width: 30%">
                                                
                                            </td>
                                            <td align="left" style="width: 5%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="Password">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtPassword" runat="server" ClientInstanceName="edtPassword" Width="100%" Password="True" OnPreRender="edtSupportPassword_PreRender">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 20%">
                                                
                                            </td>
                                            <td align="left" style="width: 30%">
                                                                                                
                                            </td>
                                            <td align="left" style="width: 5%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="From Address">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtFromAddress" runat="server" ClientInstanceName="edtFromAddress" Width="100%" MaxLength="50">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 20%">
                                                
                                            </td>
                                            <td align="left" style="width: 30%">
                                                
                                            </td>
                                            <td align="left" style="width: 5%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 15%">
                                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="From Name">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <dx:ASPxTextBox ID="edtFromName" runat="server" ClientInstanceName="edtFromName" Width="100%" MaxLength="50">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="right" style="width: 20%">
                                                
                                            </td>
                                            <td align="left" style="width: 30%">
                                                
                                            </td>
                                            <td align="left" style="width: 5%">
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="tabSysNotice" Text="System Notice">
                            <ActiveTabStyle Font-Bold="True">
                            </ActiveTabStyle>
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <dx:ASPxMemo ID="memSysNotice" runat="server" ClientInstanceName="memSysNotice" Height="220px" Width="100%">
                                    </dx:ASPxMemo>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        
                    </TabPages>
                    <ActiveTabStyle Font-Bold="True">
                    </ActiveTabStyle>
                </dx:ASPxPageControl>
            </td>
        </tr>
    </table>
    <dx:ASPxCallback ID="cbSystem" runat="server" ClientInstanceName="cbSystem" OnCallback="cbSystem_Callback" >
    </dx:ASPxCallback>
</asp:Content>
