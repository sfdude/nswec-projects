﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class ImportPPDM : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "ImportPPDM.aspx";
            Page.Server.ScriptTimeout = 300; // 5 minutes            

            LoginUser loginUser = Session["EC_User"] as LoginUser;
            //if (!(loginUser != null && loginUser.SpecialUser))
            if (!(loginUser != null && loginUser.IsAdmin))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("IC_RO_WS");
                Session.Remove("IC_CC_WS");
                Session.Remove("IC_CO_WS");

                Session.Remove("IC_IMPORT_FILE_LIST");

                AssignClientSideScripts();

                pcImport.ShowTabs = false;
                ECWebClass.SetMenuStyle(menuAction);
                ECWebClass.SetRoundPanelStyle(rpFileContainer);

                menuAction.Items[0].ClientEnabled = false;
                cbAppendMode.Checked = false;
                cbVersionUpdate.Checked = false;
                cbSlaveDBUpdate.Checked = false;
            }
        }

        private void AssignClientSideScripts()
        {            
            menuAction.ClientSideEvents.ItemClick = @"function(s, e) { 
                e.processOnServer = false; 
                if (e.item.name == 'itmImport') { 
                    e.item.SetText('Wait...'); 
                    e.item.SetEnabled(false); 
                    cbImport.PerformCallback(fileList.GetItemCount());  
                } else
                if (e.item.name == 'itmUpdateSlave') { 
                    e.item.SetText('Wait...'); 
                    e.item.SetEnabled(false); 
                    cbUpdateSlave.PerformCallback('Update');  
                }
            } ";

            cbUpdateSlave.ClientSideEvents.CallbackComplete = @"function(s, e) {
                if (e.result != null && e.result != '')
                    alert(e.result);
                var itmUpdateSlave = menuAction.GetItemByName('itmUpdateSlave');
                if (itmUpdateSlave != null) { 
                    itmUpdateSlave.SetText('Update Slave'); 
                    itmUpdateSlave.SetEnabled(true); 
                } 
            } ";

            cbImport.ClientSideEvents.CallbackComplete = @"function(s, e) {
                if (e.result != null && e.result != '')
                    alert(e.result);
                fileList.ClearItems();
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null) { 
                    itmImport.SetText('Import'); 
                    itmImport.SetEnabled(false); 
                } 
            } ";

            ucSource.ClientSideEvents.FilesUploadStart = @"function(s, e) {
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null)
                    itmImport.SetEnabled(false);
                fileList.ClearItems();
                }";
            ucSource.ClientSideEvents.FilesUploadComplete = @"function(s, e) {
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null)
                    itmImport.SetEnabled(fileList.GetItemCount() > 0);
                }";
            ucSource.ClientSideEvents.FileUploadComplete = @"function(s, e) { 
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null) { 
                    itmImport.SetText('Import'); 
                    itmImport.SetEnabled(true); 
                }
                if (e.callbackData != '') {
                    if (e.callbackData.indexOf('Error') > -1) {
                        alert(e.callbackData); 
                        return;
                    }
                    var idx = 0;
                    if (fileList.GetItemCount() > 0)
                        idx = fileList.GetItemCount();
                    fileList.InsertItem(idx, e.callbackData);
                }
            } ";

        }

        protected void cbUpdateSlave_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            if (!string.IsNullOrEmpty(e.Parameter))
            {                
                string procMsg = "Slave DB has been updated successfully";
                procMsg = string.Format("{0}|{1}", loginUser.UserName, cbVersionUpdate.Checked);
                if (ECWebClass.SGEImportTableData(cbAppendMode.Checked, ref procMsg))
                    procMsg = "Slave DB has been updated";
                e.Result = procMsg;
            }
        }
        protected void cbImport_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {            
            e.Result = string.Empty;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            //if (!loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                int fileCount = ECWebClass.StrToIntDef(e.Parameter, 0);
                string fileListStr;
                if (Session["IC_IMPORT_FILE_LIST"] != null && Session["IC_IMPORT_FILE_LIST"].ToString() != string.Empty)
                    fileListStr = Session["IC_IMPORT_FILE_LIST"].ToString();
                else
                    fileListStr = string.Empty;

                Session.Remove("IC_IMPORT_FILE_LIST");

                string[] fileLists = fileListStr.Split('|');
                if (fileCount > 0 && fileCount == fileLists.Length)
                {
                    string procMsg = string.Empty;
                    ECWebClass.SGEImportSourceFiles(loginUser, fileLists, cbAppendMode.Checked, cbVersionUpdate.Checked, cbSlaveDBUpdate.Checked, ref procMsg);
                    e.Result = procMsg;                    
                }
                else
                    e.Result = "No file to import";
            }
        }

        protected void ucSource_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            e.CallbackData = "Error: Uploaded election data file is invalid";
            if (e.IsValid)
            {
                try
                {
                    string confFolder = MapPath(ConfigurationManager.AppSettings["ImportFolder"]);
                    if (!System.IO.Directory.Exists(confFolder))
                        System.IO.Directory.CreateDirectory(confFolder);
                    /*
                    string fileExtension = System.IO.Path.GetExtension(e.UploadedFile.FileName).ToLower();
                    string GUIDId = Guid.NewGuid().ToString().ToUpper();
                    string fileGUIDName = String.Format("{0}\\{1}{2}", confFolder, GUIDId, fileExtension);
                    e.UploadedFile.SaveAs(fileGUIDName);
                     */
                    string importFileName = string.Format("{0}\\{1}", confFolder, e.UploadedFile.FileName);
                    e.UploadedFile.SaveAs(importFileName, true);

                    if (Session["IC_IMPORT_FILE_LIST"] != null && Session["IC_IMPORT_FILE_LIST"].ToString() != string.Empty)
                        Session["IC_IMPORT_FILE_LIST"] += string.Format("|{0}", importFileName);
                    else
                        Session["IC_IMPORT_FILE_LIST"] = importFileName;

                    e.CallbackData = e.UploadedFile.FileName;
                }
                catch (Exception ex)
                {
                    e.CallbackData = string.Format("Error in File Upload: {0}", ex.Message);
                }
            }
        }
    }
}