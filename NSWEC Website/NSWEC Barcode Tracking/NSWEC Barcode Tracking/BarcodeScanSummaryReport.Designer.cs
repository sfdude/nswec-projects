﻿namespace NSWEC_Barcode_Tracking
{
    partial class BarcodeScanSummaryReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrDB_DateRange = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDB_NO_SCAN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDB_TotalCharge = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDateTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDateFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTotalScan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTotalCharge = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTotalChargeExclGst = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGST = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDB_DateRange,
            this.xrDB_NO_SCAN,
            this.xrDB_TotalCharge});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 22.91667F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDB_DateRange
            // 
            this.xrDB_DateRange.Dpi = 100F;
            this.xrDB_DateRange.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDB_DateRange.LocationFloat = new DevExpress.Utils.PointFloat(16.83334F, 0F);
            this.xrDB_DateRange.Name = "xrDB_DateRange";
            this.xrDB_DateRange.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDB_DateRange.SizeF = new System.Drawing.SizeF(341.785F, 18F);
            this.xrDB_DateRange.StylePriority.UseFont = false;
            this.xrDB_DateRange.StylePriority.UseTextAlignment = false;
            this.xrDB_DateRange.Text = "xrDB_DateRange";
            this.xrDB_DateRange.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDB_NO_SCAN
            // 
            this.xrDB_NO_SCAN.Dpi = 100F;
            this.xrDB_NO_SCAN.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDB_NO_SCAN.LocationFloat = new DevExpress.Utils.PointFloat(407.7502F, 0F);
            this.xrDB_NO_SCAN.Name = "xrDB_NO_SCAN";
            this.xrDB_NO_SCAN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDB_NO_SCAN.SizeF = new System.Drawing.SizeF(71.83003F, 18F);
            this.xrDB_NO_SCAN.StylePriority.UseFont = false;
            this.xrDB_NO_SCAN.StylePriority.UseTextAlignment = false;
            this.xrDB_NO_SCAN.Text = "xrDB_NO_SCAN";
            this.xrDB_NO_SCAN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrDB_TotalCharge
            // 
            this.xrDB_TotalCharge.Dpi = 100F;
            this.xrDB_TotalCharge.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDB_TotalCharge.LocationFloat = new DevExpress.Utils.PointFloat(609.7918F, 0F);
            this.xrDB_TotalCharge.Name = "xrDB_TotalCharge";
            this.xrDB_TotalCharge.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDB_TotalCharge.SizeF = new System.Drawing.SizeF(120.8749F, 18F);
            this.xrDB_TotalCharge.StylePriority.UseFont = false;
            this.xrDB_TotalCharge.StylePriority.UseTextAlignment = false;
            this.xrDB_TotalCharge.Text = "xrDB_TotalCharge";
            this.xrDB_TotalCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrDB_TotalCharge.Visible = false;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 51.04167F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 50.33334F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.xrLabel1,
            this.xrLabel17,
            this.xrLabel14,
            this.xrLabel3,
            this.lblDateTo,
            this.xrLabel4,
            this.lblDateFrom,
            this.xrLblTitle});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 139.5833F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 134F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(739F, 4F);
            // 
            // xrLabel1
            // 
            this.xrLabel1.AutoWidth = true;
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(16.83334F, 116F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(188.3333F, 18F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Date Range (week)";
            this.xrLabel1.WordWrap = false;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 100F;
            this.xrLabel17.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(365.0419F, 116F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(116.6217F, 18F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Number of Scan";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 100F;
            this.xrLabel14.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(629.5834F, 116F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(101.0833F, 18F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Charge";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel14.Visible = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(3.000069F, 44.00002F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(89.99993F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Date From:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblDateTo
            // 
            this.lblDateTo.AutoWidth = true;
            this.lblDateTo.Dpi = 100F;
            this.lblDateTo.LocationFloat = new DevExpress.Utils.PointFloat(96.00004F, 67.00001F);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDateTo.SizeF = new System.Drawing.SizeF(247.9166F, 23F);
            this.lblDateTo.Text = "lblDateTo";
            this.lblDateTo.WordWrap = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(8.624967F, 67.00001F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(84.375F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Date To:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblDateFrom
            // 
            this.lblDateFrom.AutoWidth = true;
            this.lblDateFrom.Dpi = 100F;
            this.lblDateFrom.LocationFloat = new DevExpress.Utils.PointFloat(96.00004F, 44.00002F);
            this.lblDateFrom.Name = "lblDateFrom";
            this.lblDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDateFrom.SizeF = new System.Drawing.SizeF(247.9166F, 23F);
            this.lblDateFrom.Text = "lblDateFrom";
            this.lblDateFrom.WordWrap = false;
            // 
            // xrLblTitle
            // 
            this.xrLblTitle.Dpi = 100F;
            this.xrLblTitle.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.xrLblTitle.LocationFloat = new DevExpress.Utils.PointFloat(155.2083F, 10.00001F);
            this.xrLblTitle.Name = "xrLblTitle";
            this.xrLblTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblTitle.SizeF = new System.Drawing.SizeF(386.4633F, 23F);
            this.xrLblTitle.StylePriority.UseFont = false;
            this.xrLblTitle.StylePriority.UseTextAlignment = false;
            this.xrLblTitle.Text = "Barcode Scan Weekly Report";
            this.xrLblTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel44,
            this.xrLabel35,
            this.lblTotalScan,
            this.lblTotalCharge,
            this.xrLabel45,
            this.xrLabel46,
            this.lblTotalChargeExclGst,
            this.lblGST,
            this.xrLine3});
            this.ReportFooter.Dpi = 100F;
            this.ReportFooter.HeightF = 100F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel44
            // 
            this.xrLabel44.Dpi = 100F;
            this.xrLabel44.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(529.9583F, 5.250009F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(76.45831F, 23.00002F);
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "Charge";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel44.Visible = false;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Dpi = 100F;
            this.xrLabel35.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(16.58334F, 5.250009F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(158.7499F, 18F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.Text = "Total Number of Scan:";
            // 
            // lblTotalScan
            // 
            this.lblTotalScan.Dpi = 100F;
            this.lblTotalScan.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalScan.LocationFloat = new DevExpress.Utils.PointFloat(175.3333F, 5.250009F);
            this.lblTotalScan.Name = "lblTotalScan";
            this.lblTotalScan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTotalScan.SizeF = new System.Drawing.SizeF(206.205F, 23.00002F);
            this.lblTotalScan.StylePriority.UseFont = false;
            this.lblTotalScan.StylePriority.UseTextAlignment = false;
            this.lblTotalScan.Text = "lblTotalScan";
            this.lblTotalScan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTotalCharge
            // 
            this.lblTotalCharge.Dpi = 100F;
            this.lblTotalCharge.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCharge.LocationFloat = new DevExpress.Utils.PointFloat(609.5418F, 5.250009F);
            this.lblTotalCharge.Name = "lblTotalCharge";
            this.lblTotalCharge.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTotalCharge.SizeF = new System.Drawing.SizeF(120.8749F, 23.00002F);
            this.lblTotalCharge.StylePriority.UseFont = false;
            this.lblTotalCharge.StylePriority.UseTextAlignment = false;
            this.lblTotalCharge.Text = "lblTotalCharge";
            this.lblTotalCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTotalCharge.Visible = false;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Dpi = 100F;
            this.xrLabel45.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(541.4168F, 29.33327F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(65F, 23F);
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "GST";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel45.Visible = false;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Dpi = 100F;
            this.xrLabel46.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(541.4168F, 54.33327F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(65F, 23F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "GST Incl.";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel46.Visible = false;
            // 
            // lblTotalChargeExclGst
            // 
            this.lblTotalChargeExclGst.Dpi = 100F;
            this.lblTotalChargeExclGst.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalChargeExclGst.LocationFloat = new DevExpress.Utils.PointFloat(609.5418F, 54.33334F);
            this.lblTotalChargeExclGst.Name = "lblTotalChargeExclGst";
            this.lblTotalChargeExclGst.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTotalChargeExclGst.SizeF = new System.Drawing.SizeF(120.8749F, 23.00002F);
            this.lblTotalChargeExclGst.StylePriority.UseFont = false;
            this.lblTotalChargeExclGst.StylePriority.UseTextAlignment = false;
            this.lblTotalChargeExclGst.Text = "lblTotalChargeExclGst";
            this.lblTotalChargeExclGst.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTotalChargeExclGst.Visible = false;
            // 
            // lblGST
            // 
            this.lblGST.Dpi = 100F;
            this.lblGST.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGST.LocationFloat = new DevExpress.Utils.PointFloat(609.5418F, 29.33334F);
            this.lblGST.Name = "lblGST";
            this.lblGST.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGST.SizeF = new System.Drawing.SizeF(120.8749F, 23.00002F);
            this.lblGST.StylePriority.UseFont = false;
            this.lblGST.StylePriority.UseTextAlignment = false;
            this.lblGST.Text = "lblGST";
            this.lblGST.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblGST.Visible = false;
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 100F;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(739F, 4F);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 33F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 100F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo2.Format = "Printed On {0:dd/MM/yyyy h:mm tt}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(5F, 4.99999F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(351.785F, 23.00002F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Page {0} of {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(566.0416F, 4.99999F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(177.9584F, 23.00002F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // BarcodeScanSummaryReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter,
            this.PageFooter});
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margins = new System.Drawing.Printing.Margins(49, 52, 51, 50);
            this.Version = "15.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.BarcodeScanSummaryReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrDB_DateRange;
        private DevExpress.XtraReports.UI.XRLabel xrDB_NO_SCAN;
        private DevExpress.XtraReports.UI.XRLabel xrDB_TotalCharge;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblDateTo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblDateFrom;
        private DevExpress.XtraReports.UI.XRLabel xrLblTitle;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel lblTotalScan;
        private DevExpress.XtraReports.UI.XRLabel lblTotalCharge;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel lblTotalChargeExclGst;
        private DevExpress.XtraReports.UI.XRLabel lblGST;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
    }
}
