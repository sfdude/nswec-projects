﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Drawing;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class Maintenance : System.Web.UI.Page
    {
        private readonly int defaultPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);

        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "Maintenance.aspx";
            string tgtLogin = string.Empty;
            if (!ECSecurityClass.CheckQueryString(this.Page, "loginId", ref tgtLogin))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
            
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            //if (!loginUser.IsAdmin || string.Compare(loginUser.UserName, tgtLogin, true) != 0)
            if (loginUser == null || !(loginUser.IsAdmin && string.Compare(loginUser.UserName, tgtLogin, true) == 0))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("M_EventTable");
                Session.Remove("M_SourceTable"); 
                Session.Remove("M_CountCentreTable");
                Session.Remove("M_ROTable");
                Session.Remove("M_LGATable");
                Session.Remove("M_WardTable");
                Session.Remove("M_VenueTable");
                Session.Remove("M_VenueTypeTable");
                Session.Remove("M_ContestTable");
                Session.Remove("M_ProgressCountTable");
                Session.Remove("M_ContainerTypeTable");
                Session.Remove("M_StatusStage");

                Session["M_EventExpanded"] = false;
                Session["M_SourceExpanded"] = false;
                Session["M_CCExpanded"] = false;
                Session["M_ROExpanded"] = false;
                Session["M_LGAExpanded"] = false;
                Session["M_WardExpanded"] = false;
                Session["M_VenueExpanded"] = false;
                Session["M_VenueTypeExpanded"] = false;
                Session["M_ContestExpanded"] = false;
                Session["M_ProgressCountExpanded"] = false;
                Session["M_ContainerTypeExpanded"] = false;
                Session["M_StatusStageExpanded"] = false;

                AssignClientSideScripts();

                // styling
                ECWebClass.SetPageControlHeaderStyle(pcMaint);

                LoginUser loginUser = Session["EC_User"] as LoginUser;

                int pgIdx = -1;
                TabPage tabEvent = pcMaint.TabPages.FindByName("tabEvent");
                if (tabEvent != null)
                {
                    DataTable dtEvent = GetEventDataTable();
                    tabEvent.ClientVisible = dtEvent != null && dtEvent.Rows.Count > 0;
                    if (tabEvent.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabEvent.Name);
                    ECWebClass.SetGridViewStyle(grdEvent);
                }

                TabPage tabSource = pcMaint.TabPages.FindByName("tabSource");
                if (tabSource != null)
                {
                    DataTable dtSource = GetSourceDataTable();
                    tabSource.ClientVisible = dtSource != null && dtSource.Rows.Count > 0;
                    if (tabSource.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabSource.Name);
                    ECWebClass.SetGridViewStyle(grdSource);
                }

                TabPage tabCountCentre = pcMaint.TabPages.FindByName("tabCountCentre");
                if (tabCountCentre != null)
                {
                    DataTable dtCountCentre = GetCountCentreDataTable();
                    tabCountCentre.ClientVisible = dtCountCentre != null && dtCountCentre.Rows.Count > 0;
                    if (tabCountCentre.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabCountCentre.Name);
                    tabCountCentre.Text = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "Count Centre" : "Processing Location";
                    ECWebClass.SetGridViewStyle(grdCountCentre);
                }

                TabPage tabReturningOffice = pcMaint.TabPages.FindByName("tabReturningOffice");
                if (tabReturningOffice != null)
                {
                    DataTable dtRO = GetRODataTable();
                    tabReturningOffice.ClientVisible = dtRO != null && dtRO.Rows.Count > 0;
                    if (tabReturningOffice.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabReturningOffice.Name);
                    tabReturningOffice.Text = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "Returning Office" : "Election Manager Office";
                    ECWebClass.SetGridViewStyle(grdRO);
                }
                TabPage tabLGA = pcMaint.TabPages.FindByName("tabLGA");
                if (tabLGA != null)
                {
                    DataTable dtLGA = GetLGADataTable();
                    tabLGA.ClientVisible = dtLGA != null && dtLGA.Rows.Count > 0;
                    if (tabLGA.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabLGA.Name);
                    tabLGA.Text = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "LGA" : "District";

                    GridViewDataTextColumn colEMO = grdLGA.Columns["ROCodeName"] as GridViewDataTextColumn;
                    if (colEMO != null)
                        colEMO.Caption = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "Returning Office" : "Election Manager Office";

                    ECWebClass.SetGridViewStyle(grdLGA);
                }
                TabPage tabWard = pcMaint.TabPages.FindByName("tabWard");
                if (tabWard != null)
                {
                    DataTable dtWard = GetWardDataTable();
                    tabWard.ClientVisible = dtWard != null && dtWard.Rows.Count > 0;
                    if (tabWard.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabWard.Name);
                    ECWebClass.SetGridViewStyle(grdWard);
                }
                TabPage tabVenue = pcMaint.TabPages.FindByName("tabVenue");
                if (tabVenue != null)
                {
                    DataTable dtVenue = GetVenueDataTable();
                    tabVenue.ClientVisible = dtVenue != null && dtVenue.Rows.Count > 0;
                    if (tabVenue.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabVenue.Name);

                    GridViewDataTextColumn colEMO = grdVenue.Columns["EMO"] as GridViewDataTextColumn;
                    if (colEMO != null)
                        colEMO.Caption = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "Returning Office" : "Election Manager Office";
                    GridViewDataTextColumn colDistrict = grdVenue.Columns["District"] as GridViewDataTextColumn;
                    if (colDistrict != null)
                        colDistrict.Caption = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "LGA" : "District";
                    GridViewDataTextColumn colWard = grdVenue.Columns["Ward"] as GridViewDataTextColumn;
                    if (colWard != null)
                        colWard.Visible = loginUser.EventType == EventType.LOCAL_GOVERNMENT;
                    ECWebClass.SetGridViewStyle(grdVenue);
                }
                TabPage tabVenueType = pcMaint.TabPages.FindByName("tabVenueType");
                if (tabVenueType != null)
                {
                    DataTable dtVenueType = GetVenueTypeDataTable();
                    tabVenueType.ClientVisible = dtVenueType != null && dtVenueType.Rows.Count > 0;
                    if (tabVenueType.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabVenueType.Name);
                    ECWebClass.SetGridViewStyle(grdVenueType);
                }
                TabPage tabContest = pcMaint.TabPages.FindByName("tabContest");
                if (tabContest != null)
                {
                    DataTable dtContest = GetContestDataTable();
                    tabContest.ClientVisible = dtContest != null && dtContest.Rows.Count > 0;
                    if (tabContest.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabContest.Name);
                    tabContest.Text = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "Contest Type" : "Product Type";
                    ECWebClass.SetGridViewStyle(grdContest);
                }
                TabPage tabProgressiveCount = pcMaint.TabPages.FindByName("tabProgressiveCount");
                if (tabProgressiveCount != null)
                {
                    DataTable dtProgressCount = GetProgressCountDataTable();
                    tabProgressiveCount.ClientVisible = dtProgressCount != null && dtProgressCount.Rows.Count > 0;
                    if (tabProgressiveCount.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabProgressiveCount.Name);
                    ECWebClass.SetGridViewStyle(grdProgressiveCount);
                }
                TabPage tabContainerType = pcMaint.TabPages.FindByName("tabContainerType");
                if (tabContainerType != null)
                {
                    DataTable dtContainerType = GetContainerTypeDataTable();
                    tabContainerType.ClientVisible = dtContainerType != null && dtContainerType.Rows.Count > 0;
                    if (tabContainerType.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabContainerType.Name);
                    ECWebClass.SetGridViewStyle(grdContainerType);
                }
                TabPage tabStatusStage = pcMaint.TabPages.FindByName("tabStatusStage");
                if (tabStatusStage != null)
                {
                    DataTable dtStatusStage = GetStatusStageDataTable();
                    tabStatusStage.ClientVisible = dtStatusStage != null && dtStatusStage.Rows.Count > 0;
                    if (tabStatusStage.ClientVisible && pgIdx < 0)
                        pgIdx = pcMaint.TabPages.IndexOfName(tabStatusStage.Name);
                    ECWebClass.SetGridViewStyle(grdStatusStage);
                }
                
                if (pgIdx > -1)
                    pcMaint.ActiveTabIndex = pgIdx;

            }
            switch (pcMaint.ActiveTabIndex)
            {
                case 0:
                    LoadEventData();
                    break;
                case 1:
                    LoadSourceData();
                    break;
                case 2:
                    LoadCountCentreData();
                    break;
                case 3:
                    LoadROData();
                    break;
                case 4:
                    LoadLGAData();
                    break;
                case 5:
                    LoadWardData();
                    break;
                case 6:
                    LoadVenueData();
                    break;
                case 7:
                    LoadVenueTypeData();
                    break;
                case 8:
                    LoadContestData();
                    break;
                case 9:
                    LoadProgressCountData();
                    break;
                case 10:
                    LoadContainerTypeData();
                    break;
                case 11:
                    LoadStatusStageData();
                    break;
            }
        }

        private void AssignClientSideScripts()
        {
            grdEvent.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnEventEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else
                    if (btnId == 'btnEventUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdEvent')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + active); 
                    } else  
                    if (btnId == 'btnEventCancel') {
                        s.CancelEdit();
                    }
                }";
            grdSource.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnSourceEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else
                    if (btnId == 'btnSourceUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdSource')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name'); 
                        var start = s.GetEditValue('BarcodeStart'); var stop = s.GetEditValue('BarcodeStop'); 
                        var len = s.GetEditValue('BarcodeLength'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + start + '|' + stop + '|' + len + '|' + active); 
                    } else  
                    if (btnId == 'btnSourceCancel') {
                        s.CancelEdit();
                    }
                }";
            grdCountCentre.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnCCEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else
                    if (btnId == 'btnCCUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdCountCentre')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + active); 
                    } else  
                    if (btnId == 'btnCCCancel') {
                        s.CancelEdit();
                    }
                }";
            grdRO.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnROEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else 
                    if (btnId == 'btnROUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdRO')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name');
                        var add1 = s.GetEditValue('CountCentreCode'); var add2 = s.GetEditValue('CountCentreName');
                        var suburb = s.GetEditValue('Suburb'); var state = s.GetEditValue('State');
                        var pCode = s.GetEditValue('PostCode'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + add1 + '|' + add2 + '|' + 
                            suburb + '|' + state + '|' + pCode + '|' + active); 
                    } else
                    if (btnId == 'btnROCancel') {
                        s.CancelEdit();
                    }
                }";
            grdLGA.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnLGAEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else 
                    if (btnId == 'btnLGAUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdLGA')) return; 
                        var code = s.GetEditValue('Code'); var areaCode = s.GetEditValue('AreaCode'); 
                        var name = s.GetEditValue('AreaAuthorityName'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + areaCode + '|' + name + '|' + active); 
                    } else  
                    if (btnId == 'btnLGACancel') {
                        s.CancelEdit();
                    }
                }";
            grdWard.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnWardEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else
                    if (btnId == 'btnWardUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdWard')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + active); 
                    } else  
                    if (btnId == 'btnWardCancel') {
                        s.CancelEdit();
                    }
                }";
            grdVenue.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnVenueEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else 
                    if (btnId == 'btnVenueUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdVenue')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name');
                        var lName = s.GetEditValue('LongName'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + lName + '|' + active); 
                    } else
                    if (btnId == 'btnVenueCancel') {
                        s.CancelEdit();
                    }
                }";
            grdVenueType.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnVenueTypeEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else
                    if (btnId == 'btnVenueTypeUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdVenueType')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + active); 
                    } else 
                    if (btnId == 'btnVenueTypeCancel') {
                        s.CancelEdit();
                    }
                }";
            btnAddVenueType.ClientSideEvents.Click =
                @"function(s, e) { 
                    if (!grdVenueType.IsNewRowEditing() && !grdVenueType.IsEditing()) 
                        grdVenueType.AddNewRow();
                }";
            grdContest.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnContestEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else
                    if (btnId == 'btnContestUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdContest')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name'); 
                        var desc = s.GetEditValue('Description'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + desc + '|' + active); 
                    } else 
                    if (btnId == 'btnContestCancel') {
                        s.CancelEdit();
                    }
                }";
            
            btnAddContest.ClientSideEvents.Click =
                @"function(s, e) { 
                    if (!grdContest.IsNewRowEditing() && !grdContest.IsEditing()) 
                        grdContest.AddNewRow();
                }";
            grdContainerType.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnContainerTypeEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else 
                    if (btnId == 'btnContainerTypeUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdContainerType')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + active); 
                    } else 
                    if (btnId == 'btnContainerTypeCancel') {
                        s.CancelEdit();
                    }
                }";
            btnAddContainerType.ClientSideEvents.Click =
                @"function(s, e) { 
                    if (!grdContainerType.IsNewRowEditing() && !grdContainerType.IsEditing()) 
                        grdContainerType.AddNewRow();
                }";
            grdProgressiveCount.ClientSideEvents.CustomButtonClick =
                @"function(s, e) {
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnProgressiveCountEdit' && !s.IsNewRowEditing() && !s.IsEditing()) {
                        s.StartEditRow(e.visibleIndex); 
                    } else 
                    if (btnId == 'btnProgressiveCountUpdate') {
                        if(!ASPxClientEdit.ValidateEditorsInContainerById('grdProgressiveCount')) return; 
                        var code = s.GetEditValue('Code'); var name = s.GetEditValue('Name'); var active = s.GetEditValue('Active');
                        s.PerformCallback('Update|' + id + '|' + code + '|' + name + '|' + active); 
                    } else 
                    if (btnId == 'btnProgressiveCountCancel') {
                        s.CancelEdit();
                    }
                }";
        }
        private DataTable GetContainerTypeDataTable()
        {
            DataTable dt = Session["M_ContainerTypeTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetTableData("NSW_ContainerType");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_ContainerTypeTable"] = dt;
            }
            return dt;
        }
        private DataTable GetProgressCountDataTable()
        {
            DataTable dt = Session["M_ProgressCountTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetTableData("NSW_ProgressiveCount");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_ProgressCountTable"] = dt;
            }
            return dt;
        }
        private DataTable GetContestDataTable()
        {
            DataTable dt = Session["M_ContestTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetTableData("NSW_Contest");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_ContestTable"] = dt;
            }
            return dt;
        }
        private DataTable GetVenueTypeDataTable()
        {
            DataTable dt = Session["M_VenueTypeTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetTableData("NSW_VenueType");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_VenueTypeTable"] = dt;
            }
            return dt;
        }
        private DataTable GetVenueDataTable()
        {
            DataTable dt = Session["M_VenueTable"] as DataTable;
            if (dt == null)
            {
                //dt = ECWebClass.GetVenueData();
                dt = ECWebClass.GetTableData("NSW_Venue");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_VenueTable"] = dt;
            }
            return dt;
        }
        private DataTable GetWardDataTable()
        {
            DataTable dt = Session["M_WardTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetTableData("NSW_Ward");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_WardTable"] = dt;
            }
            return dt;
        }
        private DataTable GetLGADataTable()
        {
            DataTable dt = Session["M_LGATable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetTableData("NSW_LGA");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_LGATable"] = dt;
            }
            return dt;
        }
        private DataTable GetRODataTable()
        {
            DataTable dt = Session["M_ROTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetEMOData();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_ROTable"] = dt;
            }
            return dt;
        }
        private DataTable GetEventDataTable()
        {
            DataTable dt = Session["M_EventTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetTableData("NSW_Event");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_EventTable"] = dt;
            }
            return dt;
        }

        private DataTable GetStatusStageDataTable()
        {
            DataTable dt = Session["M_StatusStage"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetStatusStageData();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_StatusStage"] = dt;
            }
            return dt;
        }

        private DataTable GetSourceDataTable()
        {
            DataTable dt = Session["M_SourceTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetTableData("NSW_BarcodeSource");
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_SourceTable"] = dt;
            }
            return dt;
        }
        
        private DataTable GetCountCentreDataTable()
        {
            DataTable dt = Session["M_CountCentreTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetProcessingLocationData();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };
                Session["M_CountCentreTable"] = dt;
            }
            return dt;
        }
        private void LoadContainerTypeData()
        {
            grdContainerType.DataSource = GetContainerTypeDataTable();
            grdContainerType.DataSourceID = String.Empty;
            grdContainerType.DataBind();
        }
        private void LoadProgressCountData()
        {
            grdProgressiveCount.DataSource = GetProgressCountDataTable();
            grdProgressiveCount.DataSourceID = String.Empty;
            grdProgressiveCount.DataBind();
        }
        private void LoadContestData()
        {
            grdContest.DataSource = GetContestDataTable();
            grdContest.DataSourceID = String.Empty;
            grdContest.DataBind();
        }
        private void LoadVenueTypeData()
        {
            grdVenueType.DataSource = GetVenueTypeDataTable();
            grdVenueType.DataSourceID = String.Empty;
            grdVenueType.DataBind();
        }
        private void LoadVenueData()
        {
            if (Session["EC_User"] == null)
                return;
            grdVenue.DataSource = GetVenueDataTable();
            grdVenue.DataSourceID = String.Empty;
            grdVenue.DataBind();

            LoginUser loginUser = Session["EC_User"] as LoginUser;
            GridViewDataTextColumn colWard = grdVenue.Columns["Ward"] as GridViewDataTextColumn;
            if (colWard != null)
                colWard.Visible = loginUser.EventType == EventType.LOCAL_GOVERNMENT;

            grdVenue.SettingsPager.PageSize = Convert.ToInt32(defaultPageSize);

            if (Session["M_VenueExpanded"] == null || (Session["M_VenueExpanded"] != null && !Convert.ToBoolean(Session["M_VenueExpanded"])))
            {
                Session["M_VenueExpanded"] = true;
                //grdVenue.ExpandRow(grdVenue.VisibleRowCount - 1);
                //grdVenue.ExpandRow(0);
                grdVenue.ExpandAll();
            }
        }
        private void LoadWardData()
        {
            grdWard.DataSource = GetWardDataTable();
            grdWard.DataSourceID = String.Empty;
            grdWard.DataBind();

            grdWard.SettingsPager.PageSize = Convert.ToInt32(defaultPageSize);

            if (Session["M_WardExpanded"] == null || (Session["M_WardExpanded"] != null && !Convert.ToBoolean(Session["M_WardExpanded"])))
            {
                Session["M_WardExpanded"] = true;
                grdWard.ExpandAll();
            }
        }
        private void LoadLGAData()
        {
            grdLGA.DataSource = GetLGADataTable();
            grdLGA.DataSourceID = String.Empty;
            grdLGA.DataBind();

            grdLGA.SettingsPager.PageSize = Convert.ToInt32(defaultPageSize);

            if (Session["M_LGAExpanded"] == null || (Session["M_LGAExpanded"] != null && !Convert.ToBoolean(Session["M_LGAExpanded"])))
            {
                Session["M_LGAExpanded"] = true;
                grdLGA.ExpandAll();
            }
        }
        private void LoadROData()
        {
            grdRO.DataSource = GetRODataTable();
            grdRO.DataSourceID = String.Empty;
            grdRO.DataBind();
            grdRO.SettingsPager.PageSize = Convert.ToInt32(defaultPageSize);

            if (Session["M_ROExpanded"] == null || (Session["M_ROExpanded"] != null && !Convert.ToBoolean(Session["M_ROExpanded"])))
            {
                Session["M_ROExpanded"] = true;
                grdRO.ExpandAll();                
            }
        }
        private void LoadEventData()
        {
            grdEvent.DataSource = GetEventDataTable();
            grdEvent.DataSourceID = String.Empty;
            grdEvent.DataBind();

            if (Session["M_EventExpanded"] == null || (Session["M_EventExpanded"] != null && !Convert.ToBoolean(Session["M_EventExpanded"])))
            {
                Session["M_EventExpanded"] = true;
                grdEvent.ExpandAll();
            }
        }

        private void LoadStatusStageData()
        {
            grdStatusStage.DataSource = GetStatusStageDataTable();
            grdStatusStage.DataSourceID = String.Empty;
            grdStatusStage.DataBind();

            if (Session["M_StatusStageExpanded"] == null || (Session["M_StatusStageExpanded"] != null && !Convert.ToBoolean(Session["M_StatusStageExpanded"])))
            {
                Session["M_StatusStageExpanded"] = true;
                grdStatusStage.ExpandAll();
            }
        }

        private void LoadSourceData()
        {
            grdSource.DataSource = GetSourceDataTable();
            grdSource.DataSourceID = String.Empty;
            grdSource.DataBind();
            
            if (Session["M_SourceExpanded"] == null || (Session["M_SourceExpanded"] != null && !Convert.ToBoolean(Session["M_SourceExpanded"])))
            {
                Session["M_SourceExpanded"] = true;
                grdSource.ExpandAll();
            }
        }

        private void LoadCountCentreData()
        {
            grdCountCentre.DataSource = GetCountCentreDataTable();
            grdCountCentre.DataSourceID = String.Empty;
            grdCountCentre.DataBind();

            if (Session["M_CCExpanded"] == null || (Session["M_CCExpanded"] != null && !Convert.ToBoolean(Session["M_CCExpanded"])))
            {
                Session["M_CCExpanded"] = true;
                grdCountCentre.ExpandAll();
            }
        }

        protected void grdLGA_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "ROCodeName")
            {
                e.Value = string.Format("{0} ({1})", string.Format("{0}", e.GetListSourceFieldValue("ROName")).Trim(),
                    string.Format("{0}", e.GetListSourceFieldValue("ROCode")).Trim());
            }
        }

        protected void grdVenue_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "EMO")
            {
                e.Value = string.Format("{0} ({1})", string.Format("{0}", e.GetListSourceFieldValue("ROName")).Trim(),
                    string.Format("{0}", e.GetListSourceFieldValue("ROCode")).Trim());
            }
            else
                if (e.Column.FieldName == "District")
                {
                    object objLGACode = e.GetListSourceFieldValue("LGACode");
                    object objLGAName = e.GetListSourceFieldValue("LGAName");
                    if (objLGACode != null && !string.IsNullOrEmpty(objLGACode.ToString()) &&
                        objLGAName != null && !string.IsNullOrEmpty(objLGAName.ToString()))
                        e.Value = string.Format("{0} ({1})", objLGAName, objLGACode);
                    else
                    if (objLGACode != null && !string.IsNullOrEmpty(objLGACode.ToString()))
                        e.Value = string.Format("({0})", objLGACode);
                    else
                        e.Value = "Not Defined";
                }
                else
                    if (e.Column.FieldName == "Ward")
                    {
                        object objWardCode = e.GetListSourceFieldValue("WardCode");
                        object objWardName = e.GetListSourceFieldValue("WardName");
                        if (objWardCode != null && !string.IsNullOrEmpty(objWardCode.ToString()) &&
                            objWardName != null && !string.IsNullOrEmpty(objWardName.ToString()))
                            e.Value = string.Format("{0} ({1})", objWardName, objWardCode);
                        else
                        if (objWardCode != null && !string.IsNullOrEmpty(objWardCode.ToString()))
                            e.Value = string.Format("({0})", objWardCode);
                        else
                            e.Value = "Not Defined";
                    }
        }
        
        protected void grdView_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Update ||
                e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Cancel)
                e.Visible = false;
        }
        protected void grdView_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            // explicitely bind for the required column, if there is no data in gridview -- ie, for inserting the first record
            // and in detail row template
            ASPxGridView gridView = sender as ASPxGridView;
            if (!gridView.IsNewRowEditing && gridView.IsEditing)
            {
                if (e.Column.FieldName == "Code")
                    e.Editor.ReadOnly = true;
            }
        }
        protected void grdRO_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            
            if (loginUser == null || !loginUser.IsAdmin)
            {
                //DevExpress.Web.ASPxWebControl.RedirectOnCallback("Login.aspx");
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtRO = GetRODataTable();
                DataRow drRO = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drRO = dtRO.NewRow();
                    else
                        drRO = dtRO.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drRO["Code"] = cbInfo[2];
                    else
                        drRO["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drRO["Name"] = cbInfo[3];
                    else
                        drRO["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drRO["CountCentreCode"] = cbInfo[4];
                    else
                        drRO["CountCentreCode"] = DBNull.Value;
                    if (cbInfo[5] != "null")
                        drRO["CountCentreName"] = cbInfo[5];
                    else
                        drRO["CountCentreName"] = DBNull.Value;
                    if (cbInfo[6] != "null")
                        drRO["Suburb"] = cbInfo[6];
                    else
                        drRO["Suburb"] = DBNull.Value;
                    if (cbInfo[7] != "null")
                        drRO["State"] = cbInfo[7];
                    else
                        drRO["State"] = DBNull.Value;
                    if (cbInfo[8] != "null")
                        drRO["PostCode"] = cbInfo[8];
                    else
                        drRO["PostCode"] = DBNull.Value;
                    /*
                    if (cbInfo[9] != "null")
                        drRO["Contact"] = cbInfo[9];
                    else
                        drRO["Contact"] = DBNull.Value;

                    if (cbInfo[10] != "null")
                        drRO["DirectPhone"] = cbInfo[10];
                    else
                        drRO["DirectPhone"] = DBNull.Value;
                    if (cbInfo[11] != "null")
                        drRO["OfficePhone"] = cbInfo[11];
                    else
                        drRO["OfficePhone"] = DBNull.Value;
                    if (cbInfo[12] != "null")
                        drRO["Email"] = cbInfo[12];
                    else
                        drRO["Email"] = DBNull.Value;
                    if (cbInfo[13] != "null")
                        drRO["SpecialInstructions"] = cbInfo[13];
                    else
                        drRO["SpecialInstructions"] = DBNull.Value;
                     * */
                    if (cbInfo[9] != "null")
                        drRO["Active"] = cbInfo[9];
                    else
                        drRO["Active"] = "N";

                    if (isNew)
                    {
                        drRO["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drRO["Active"] = "Y";
                        drRO["Created"] = DateTime.Now;
                        drRO["CreatedBy"] = loginUser.UserName;
                        dtRO.Rows.Add(drRO);
                    }
                    else
                    {
                        drRO["Modified"] = DateTime.Now;
                        drRO["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_ReturningOffice", drRO, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_ROTable");
                LoadROData();
            }

        }
        protected void grdVenue_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetVenueDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["LongName"] = cbInfo[4];
                    else
                        drData["LongName"] = DBNull.Value;
                    
                    if (cbInfo[5] != "null")
                        drData["Active"] = cbInfo[5];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_Venue", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_VenueTable");
                LoadVenueData();
            }

        }
        protected void grdVenueType_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetVenueTypeDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;                    
                    if (cbInfo[4] != "null")
                        drData["Active"] = cbInfo[4];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_VenueType", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_VenueTypeTable");
                LoadVenueTypeData();
            }
        }

        protected void grdContest_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetContestDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["Description"] = cbInfo[4];
                    else
                        drData["Description"] = DBNull.Value;
                    if (cbInfo[5] != "null")
                        drData["Active"] = cbInfo[5];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_Contest", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_ContestTable");
                LoadContestData();
            }

        }

        protected void grdContainerType_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetContainerTypeDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["Active"] = cbInfo[4];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_ContainerType", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_ContainerTypeTable");
                LoadContainerTypeData();
            }

        }

        protected void grdEvent_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetEventDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["Active"] = cbInfo[4];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_Event", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_EventTable");
                LoadEventData();
            }

        }

        protected void grdSource_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetSourceDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;

                    if (cbInfo[4] != "null")
                        drData["BarcodeStart"] = cbInfo[4];
                    else
                        drData["BarcodeStart"] = DBNull.Value;
                    if (cbInfo[5] != "null")
                        drData["BarcodeStop"] = cbInfo[5];
                    else
                        drData["BarcodeStop"] = DBNull.Value;
                    if (cbInfo[6] != "null")
                        drData["BarcodeLength"] = cbInfo[6];
                    else
                        drData["BarcodeLength"] = 0;


                    if (cbInfo[7] != "null")
                        drData["Active"] = cbInfo[7];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_BarcodeSource", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_SourceTable");
                LoadSourceData();
            }

        }

        protected void grdCountCentre_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetCountCentreDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["Active"] = cbInfo[4];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    //success = ECWebClass.UpdateTableRecordById("NSW_CountCentre", drData, false, ref errMsg, false);
                    success = ECWebClass.UpdateTableRecordById("NSW_ReturningOffice", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_CountCentreTable");
                LoadEventData();
            }

        }

        protected void grdLGA_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetLGADataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["AreaCode"] = cbInfo[3];
                    else
                        drData["AreaCode"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["AreaAuthorityName"] = cbInfo[4];
                    else
                        drData["AreaAuthorityName"] = DBNull.Value;
                    if (cbInfo[5] != "null")
                        drData["Active"] = cbInfo[5];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_LGA", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_LGATable");
                LoadLGAData();
            }

        }

        protected void grdWard_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetWardDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["Active"] = cbInfo[4];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_Ward", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_WardTable");
                LoadWardData();
            }

        }
        protected void grdProgressiveCount_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                DataTable dtData = GetProgressCountDataTable();
                DataRow drData = null;
                string errMsg = string.Empty;
                bool success = false;

                string[] cbInfo = e.Parameters.Split('|');
                if (cbInfo[0] == "Update")
                {
                    bool isNew = cbInfo[1] == "null";
                    if (isNew)
                        drData = dtData.NewRow();
                    else
                        drData = dtData.Rows.Find(cbInfo[1]);
                    if (cbInfo[2] != "null")
                        drData["Code"] = cbInfo[2];
                    else
                        drData["Code"] = DBNull.Value;
                    if (cbInfo[3] != "null")
                        drData["Name"] = cbInfo[3];
                    else
                        drData["Name"] = DBNull.Value;
                    if (cbInfo[4] != "null")
                        drData["Active"] = cbInfo[4];
                    else
                        drData["Active"] = "N";

                    if (isNew)
                    {
                        drData["ID"] = Guid.NewGuid().ToString().ToUpper();
                        drData["Active"] = "Y";
                        drData["Created"] = DateTime.Now;
                        drData["CreatedBy"] = loginUser.UserName;
                        dtData.Rows.Add(drData);
                    }
                    else
                    {
                        drData["Modified"] = DateTime.Now;
                        drData["ModifiedBy"] = loginUser.UserName;
                    }
                    success = ECWebClass.UpdateTableRecordById("NSW_ProgressiveCount", drData, false, ref errMsg, false);

                    gridView.CancelEdit();
                }
                Session.Remove("M_ProgressCountTable");
                LoadProgressCountData();
            }
        }
        protected void grdView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["ID"] = Guid.NewGuid().ToString().ToUpper();
            e.NewValues["Active"] = "Y";
        }

        protected void grdWard_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "ROCodeName")
            {
                e.Value = string.Format("{0} ({1})", string.Format("{0}", e.GetListSourceFieldValue("ROName")).Trim(),
                    string.Format("{0}", e.GetListSourceFieldValue("ROCode")).Trim());
            }
            else
            if (e.Column.FieldName == "LGA")
            {
                e.Value = string.Format("{0} ({1})", e.GetListSourceFieldValue("LGACode"), e.GetListSourceFieldValue("LGAId"));
            }
        }
        public static Boolean isNumeric(String val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle,
                System.Globalization.CultureInfo.CurrentCulture, out result);
        }
        protected void grdView_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {            
            if (e.Column.FieldName == "Code")
            {
                Boolean isNum1 = isNumeric(e.Value1.ToString(), System.Globalization.NumberStyles.Integer);
                Boolean isNum2 = isNumeric(e.Value2.ToString(), System.Globalization.NumberStyles.Integer);

                if (isNum1 && !isNum2)
                {
                    e.Result = -1;
                    e.Handled = true;
                }
                else if (!isNum1 && isNum2)
                {
                    e.Result = 1;
                    e.Handled = true;
                }
                else if (isNum1 && isNum2)
                {
                    Int32 val1 = Convert.ToInt32(e.Value1);
                    Int32 val2 = Convert.ToInt32(e.Value2);

                    e.Result = (val1 - val2 < 0 ? -1 : (val1 - val2 > 0 ? 1 : 0));
                    e.Handled = true;
                }
            }
        }
        protected void grdVenue_BeforeGetCallbackResult(object sender, EventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            ASPxLabel lblGrdFilter = gridView.FindStatusBarTemplateControl("lblGrdFilter") as ASPxLabel;
            if (lblGrdFilter != null)
            {
                if (!string.IsNullOrEmpty(gridView.FilterExpression))
                    lblGrdFilter.Text = string.Format("Current Filter: {0}", gridView.FilterExpression);
                else
                    lblGrdFilter.Text = string.Empty;
            }
        }
    }
}