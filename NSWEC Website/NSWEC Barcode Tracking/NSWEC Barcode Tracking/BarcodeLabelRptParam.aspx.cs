﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class BarcodeLabelRptParam : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "BarcodeLabelRptParam.aspx";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            if (!IsPostBack)
            {
                Session.Remove("LRP_EMOTable");
                Session.Remove("LRP_District");
                Session.Remove("LRP_ContestArea");
                Session.Remove("LRP_Ward");
                Session.Remove("LRP_Venue");
                Session.Remove("LRP_VenueType");
                Session.Remove("LRP_ProductType");
                Session.Remove("LRP_ContainerType");
                Session.Remove("LRP_ScannedLocation");
                Session.Remove("LRP_StatusStage");
                Session.Remove("LRP_SourceTable");

                cmbEMO.ClientSideEvents.SelectedIndexChanged = @"function(s, e) { 
                    cpReport.PerformCallback('EMO|' + s.GetValue()); 
                }";
                cmbDistrict.ClientSideEvents.SelectedIndexChanged = @"function(s, e) { 
                    cpReport.PerformCallback('LGA|' + s.GetValue()); 
                }";
                cmbContestArea.ClientSideEvents.SelectedIndexChanged = @"function(s, e) { 
                    cpReport.PerformCallback('WARD|' + s.GetValue()); 
                }";
                cpReport.SettingsLoadingPanel.Enabled = false;

                menAction.ClientSideEvents.ItemClick =
                @"function(s, e) { 
                    e.processOnServer = false; 
                    if (e.item.name == 'btnPrint') { 
                        if (cbEnquiry.InCallback()) return; 
                        e.item.SetText('Wait...'); e.item.SetEnabled(false); cbEnquiry.PerformCallback(); 
                    } 
                } ";
                // redirect using javascripts
                cbEnquiry.ClientSideEvents.CallbackComplete = @"function(s, e) { 
                    var btnPrint = menAction.GetItemByName('btnPrint'); 
                    if (btnPrint != null) { 
                        btnPrint.SetText('Print'); 
                        btnPrint.SetEnabled(true); 
                    } 
                    if (e.result != null && e.result != '') { 
                        if (e.result.indexOf('Warning') > -1) {
                            alert(e.result); 
                            return; 
                        } 
                        if (e.result == 'ExportCSV')
                            window.open('CSVDataHandler.ashx', 'Download');
                        else
                            window.location = e.result; 
                    }
                } ";
                                
                if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                {
                    lblEMO.Text = "Returning Office";
                    lblDistrict.Text = "LGA";
                    lblProductType.Text = "Contest Type";
                    lblContestArea.Text = "Ward";
                }

                // styling
                pcRptParam.ShowTabs = false;
                ECWebClass.SetMenuStyle(menAction);
            }

            LoadEMOData();
            LoadDistrictData();
            LoadContestAreaData();
            LoadVenueData();
            LoadVenueTypeData();
            LoadProductTypeData();
            LoadContainerTypeData();
            LoadStatusStageData();
            LoadScannedLocationData();
            LoadSourceData();
            

            if (!IsPostBack)
            {
                if (Session["BarcodeLabelRptParam"] != null)
                {
                    BarcodeLabelReportParam enquiryParam = Session["BarcodeLabelRptParam"] as BarcodeLabelReportParam;
                    if (enquiryParam.EMOCode != null && !string.IsNullOrEmpty(enquiryParam.EMOCode))
                        cmbEMO.Value = enquiryParam.EMOCode;
                    LoadDistrictData();
                    if (enquiryParam.DistrictCode != null && !string.IsNullOrEmpty(enquiryParam.DistrictCode))
                        cmbDistrict.Value = enquiryParam.DistrictCode;
                    LoadContestAreaData();
                    if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                    {
                        if (enquiryParam.WardCode != null && !string.IsNullOrEmpty(enquiryParam.WardCode))
                            cmbContestArea.Value = enquiryParam.WardCode;
                    }
                    else
                    if (enquiryParam.ContestAreaCode != null && !string.IsNullOrEmpty(enquiryParam.ContestAreaCode))
                        cmbContestArea.Value = enquiryParam.DistrictCode;
                    LoadVenueData();
                    if (enquiryParam.VenueCode != null && !string.IsNullOrEmpty(enquiryParam.VenueCode))
                        cmbVenue.Value = enquiryParam.VenueCode;
                    if (enquiryParam.VenueTypeCode != null && !string.IsNullOrEmpty(enquiryParam.VenueTypeCode))
                        cmbVenueType.Value = enquiryParam.VenueTypeCode;
                    if (enquiryParam.ProductTypeCode != null && !string.IsNullOrEmpty(enquiryParam.ProductTypeCode))
                        cmbProduct.Value = enquiryParam.ProductTypeCode;
                    if (enquiryParam.ContainerTypeCode != null && !string.IsNullOrEmpty(enquiryParam.ContainerTypeCode))
                        cmbContainerType.Value = enquiryParam.ContainerTypeCode;
                    if (enquiryParam.LastScannedLocationCode != null && !string.IsNullOrEmpty(enquiryParam.LastScannedLocationCode))
                        cmbLocation.Value = enquiryParam.LastScannedLocationCode;                    
                    if (enquiryParam.LastStatusStageCode != null && !string.IsNullOrEmpty(enquiryParam.LastStatusStageCode))
                        cmbStatusStage.Value = ECWebClass.StrToDecimalDef(enquiryParam.LastStatusStageCode, 0);                    
                    if (enquiryParam.SourceId != null && !string.IsNullOrEmpty(enquiryParam.SourceId))
                        cmbSource.Value = enquiryParam.SourceId;  

                    cbIncludeStatus.Checked = enquiryParam.IncludeStatusHistory;
                    cbIncludeULD.Checked = enquiryParam.IncludeULDChild;
                }
            }
        }

        private DataTable GetSourceDataTable()
        {
            DataTable dt = Session["LRP_SourceTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetBarcodeSourceDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["LRP_SourceTable"] = dt;
            }
            return dt;
        }
        private void LoadSourceData()
        {
            cmbSource.DataSourceID = string.Empty;
            cmbSource.DataSource = GetSourceDataTable();
            cmbSource.DataBind();
            if (!IsPostBack)
                cmbSource.Value = "-1";
        }

        private DataTable GetEMODataTable()
        {
            DataTable dt = Session["LRP_EMOTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetEMODataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["LRP_EMOTable"] = dt;
            }
            return dt;
        }
        private void LoadEMOData()
        {
            cmbEMO.DataSourceID = string.Empty;
            cmbEMO.DataSource = GetEMODataTable();
            cmbEMO.DataBind();
            if (!IsPostBack)
                cmbEMO.Value = "-1";
        }
        private DataTable GetContestAreaDataTable()
        {
            DataTable dt = Session["LRP_ContestArea"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetContestAreaDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["LRP_ContestArea"] = dt;
            }
            return dt;
        }
        private DataTable GetWardDataTable()
        {
            DataTable dt = Session["LRP_Ward"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetWardDataExt();
                Session["LRP_Ward"] = dt;
            }
            return dt;
        }
        private void LoadDistrictData()
        {
            DataTable dtArea = GetContestAreaDataTable();
            DataView dvArea = new DataView(dtArea) { RowFilter = String.Format("ROCode = '{0}' OR Code='-1'", cmbEMO.Value) };
            cmbDistrict.DataSourceID = string.Empty;
            cmbDistrict.DataSource = dvArea;
            cmbDistrict.DataBind();
            if (!IsPostBack)
            {
                if (dvArea.Count == 2)
                    cmbDistrict.SelectedIndex = 1;
                else
                    cmbDistrict.SelectedIndex = 0;
            }
        }
        private void LoadContestAreaData()
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            cmbContestArea.DataSourceID = string.Empty;
            if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
            {
                DataTable dtWard = GetWardDataTable();

                string filter = string.Empty;
                if (cmbEMO.Value != null && cmbEMO.Value.ToString() != "-1" && cmbDistrict.Value != null)
                    filter = String.Format("(ROCode='{0}' AND LGAId='{1}') OR Code='-1'", cmbEMO.Value, cmbDistrict.Value);
                else
                    if (cmbEMO.Value != null && cmbEMO.Value.ToString() != "-1")
                        filter = String.Format("ROCode='{0}' OR Code='-1'", cmbEMO.Value);
                    else
                        filter = "Code='-1'";

                DataView dvWard = new DataView(dtWard) { RowFilter = filter };
                cmbContestArea.DataSource = dvWard;
                cmbContestArea.DataBind();
                if (!IsPostBack)
                {
                    if (dvWard.Count == 2)
                        cmbContestArea.SelectedIndex = 1;
                    else
                        cmbContestArea.SelectedIndex = 0;
                }
            }
            else
            {
                cmbContestArea.DataSource = GetContestAreaDataTable().DefaultView;
                cmbContestArea.DataBind();
                if (!IsPostBack)
                    cmbContestArea.Value = "-1";
            }
        }

        private DataTable GetVenueDataTable()
        {
            DataTable dt = Session["LRP_Venue"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetVenueDataExt();
                Session["LRP_Venue"] = dt;
            }
            return dt;
        }
        private void LoadVenueData()
        {
            DataTable dtVenue = GetVenueDataTable();
            DataView dvVenue = new DataView(dtVenue);
           
            string filter = string.Empty;
            if (cmbEMO.Value != null && cmbEMO.Value.ToString() != "-1" && cmbDistrict.Value != null && cmbContestArea.Value != null)
                filter = String.Format("(ROCode='{0}' AND LGACode='{1}' AND WardCode='{2}') OR Code='-1'", cmbEMO.Value, cmbDistrict.Value, cmbContestArea.Value);
            else
            if (cmbEMO.Value != null && cmbEMO.Value.ToString() != "-1" && cmbDistrict.Value != null)
                filter = String.Format("(ROCode='{0}' AND LGACode='{1}') OR Code='-1'", cmbEMO.Value, cmbDistrict.Value);
            else
            if (cmbEMO.Value != null && cmbEMO.Value.ToString() != "-1")
                filter = String.Format("ROCode='{0}' OR Code='-1'", cmbEMO.Value);
            else
                filter = "Code='-1'";

            dvVenue.RowFilter = filter;
            cmbVenue.DataSourceID = string.Empty;
            cmbVenue.DataSource = dvVenue;
            cmbVenue.DataBind();
            if (!IsPostBack)
                cmbVenue.SelectedIndex = 0;
        }

        private DataTable GetVenueTypeDataTable()
        {
            DataTable dt = Session["LRP_VenueType"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetVenueTypeDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["LRP_VenueType"] = dt;
            }
            return dt;
        }

        private void LoadVenueTypeData()
        {
            cmbVenueType.DataSourceID = string.Empty;
            cmbVenueType.DataSource = GetVenueTypeDataTable();
            cmbVenueType.DataBind();
            if (!IsPostBack)
                cmbVenueType.Value = "-1";
        }

        private DataTable GetProductTypeDataTable()
        {
            DataTable dt = Session["LRP_ProductType"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetProductTypeDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["LRP_ProductType"] = dt;
            }
            return dt;
        }

        private void LoadProductTypeData()
        {
            cmbProduct.DataSourceID = string.Empty;
            cmbProduct.DataSource = GetProductTypeDataTable();
            cmbProduct.DataBind();
            if (!IsPostBack)
                cmbProduct.Value = "-1";
        }

        private DataTable GetContainerTypeDataTable()
        {
            DataTable dt = Session["LRP_ContainerType"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetContainerTypeDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["LRP_ContainerType"] = dt;
            }
            return dt;
        }

        private void LoadContainerTypeData()
        {
            cmbContainerType.DataSourceID = string.Empty;
            cmbContainerType.DataSource = GetContainerTypeDataTable();
            cmbContainerType.DataBind();
            if (!IsPostBack)
                cmbContainerType.Value = "-1";
        }

        private DataTable GetStatusStageDataTable()
        {
            DataTable dt = Session["LRP_StatusStage"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetStatusStageDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["StageId"] };
                Session["LRP_StatusStage"] = dt;
            }
            return dt;
        }

        private void LoadStatusStageData()
        {
            cmbStatusStage.DataSourceID = string.Empty;
            cmbStatusStage.DataSource = GetStatusStageDataTable();
            cmbStatusStage.DataBind();
            if (!IsPostBack)
                cmbStatusStage.Value = "-1";
        }

        private DataTable GetScannedLocationDataTable()
        {
            DataTable dt = Session["LRP_ScannedLocation"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetScannedLocationDataExt();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["StageId"] };
                Session["LRP_ScannedLocation"] = dt;
            }
            return dt;
        }

        private void LoadScannedLocationData()
        {
            cmbLocation.DataSourceID = string.Empty;
            cmbLocation.DataSource = GetScannedLocationDataTable();
            cmbLocation.DataBind();
            if (!IsPostBack)
                cmbLocation.Value = "-1";
        }

        protected void cpReport_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string[] cbInfo = e.Parameter.Split('|');
                string code = string.Empty;
                
                if (cbInfo[0] == "EMO")
                {
                    code = cbInfo[1];
                    cmbEMO.Value = code;
                    cmbDistrict.Value = null;
                    cmbDistrict.Text = string.Empty;
                    LoadDistrictData();
                    if ((cmbDistrict.DataSource as DataView).Count == 2)
                        cmbDistrict.SelectedIndex = 1;
                    else
                        cmbDistrict.SelectedIndex = 0;

                    cmbContestArea.Value = null;
                    cmbContestArea.Text = string.Empty;
                    LoadContestAreaData();
                    if ((cmbContestArea.DataSource as DataView).Count == 2)
                        cmbContestArea.SelectedIndex = 1;
                    else
                        cmbContestArea.SelectedIndex = 0;

                    cmbVenue.Value = null;
                    cmbVenue.Text = string.Empty;
                    LoadVenueData();
                    cmbVenue.SelectedIndex = 0;
                }
                else
                if (cbInfo[0] == "LGA")
                {
                    code = cbInfo[1];
                    cmbDistrict.Value = code;
                    cmbContestArea.Value = null;
                    cmbContestArea.Text = string.Empty;
                    LoadContestAreaData();
                    if ((cmbContestArea.DataSource as DataView).Count == 2)
                        cmbContestArea.SelectedIndex = 1;
                    else
                        cmbContestArea.SelectedIndex = 0;

                    cmbVenue.Value = null;
                    cmbVenue.Text = string.Empty;
                    LoadVenueData();
                    cmbVenue.SelectedIndex = 0;
                }
                else
                if (cbInfo[0] == "WARD")
                {
                    code = cbInfo[1];
                    cmbContestArea.Value = code;

                    cmbVenue.Value = null;
                    cmbVenue.Text = string.Empty;
                    LoadVenueData();
                    cmbVenue.SelectedIndex = 0;
                }
            }
        }

        protected void cbEnquiry_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;

            BarcodeLabelReportParam enquiryParam = new BarcodeLabelReportParam();
            if (cmbEMO.Value != null && cmbEMO.Value.ToString() != "-1")
                enquiryParam.EMOCode = cmbEMO.Value.ToString();
            if (cmbDistrict.Value != null && cmbDistrict.Value.ToString() != "-1")
                enquiryParam.DistrictCode = cmbDistrict.Value.ToString();
            if (cmbContestArea.Value != null && cmbContestArea.Value.ToString() != "-1")
            {   
                if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                    enquiryParam.WardCode = cmbContestArea.Value.ToString();
                else
                    enquiryParam.ContestAreaCode = cmbContestArea.Value.ToString();
            }
            if (cmbVenue.Value != null && cmbVenue.Value.ToString() != "-1")
                enquiryParam.VenueCode = cmbVenue.Value.ToString();
            if (cmbVenueType.Value != null && cmbVenueType.Value.ToString() != "-1")
                enquiryParam.VenueTypeCode = cmbVenueType.Value.ToString();
            if (cmbProduct.Value != null && cmbProduct.Value.ToString() != "-1")
                enquiryParam.ProductTypeCode = cmbProduct.Value.ToString();
            if (cmbContainerType.Value != null && cmbContainerType.Value.ToString() != "-1")
                enquiryParam.ContainerTypeCode = cmbContainerType.Value.ToString();
            if (cmbLocation.Value != null && cmbLocation.Value.ToString() != "-1")
                enquiryParam.LastScannedLocationCode = cmbLocation.Value.ToString();
            if (cmbStatusStage.Value != null && cmbStatusStage.Value.ToString() != "-1")
                enquiryParam.LastStatusStageCode = cmbStatusStage.Value.ToString();
            if (cmbSource.Value != null && cmbSource.Value.ToString() != "-1")
                enquiryParam.SourceId = cmbSource.Value.ToString();

            enquiryParam.IncludeStatusHistory = cbIncludeStatus.Checked;
            enquiryParam.IncludeULDChild = cbIncludeULD.Checked;
            enquiryParam.ExportToCSV = cbExportCSV.Checked;

            DataTable dtEnquiry = ECWebClass.GetSGEBarcodeLabelRptData(enquiryParam);

            Session["BarcodeLabelRptParam"] = enquiryParam;

            if (dtEnquiry != null && dtEnquiry.Rows.Count > 0)
            {
                if (!enquiryParam.ExportToCSV)
                    e.Result = "BarcodeLabelRptViewer.aspx";
                else
                {
                    DataSet dsCSV = new DataSet("dsCSV");

                    DataTable dtConDet;
                    if (enquiryParam != null && enquiryParam.IncludeStatusHistory)
                        dtConDet = ECWebClass.GetSGEBarcodeLabelRptDetailData(enquiryParam);
                    else
                        dtConDet = null;

                    DataTable dtReportCSV = new DataTable("BarcodeLabelReport");

                    DataColumn col = new DataColumn("Barcode", typeof(string));
                    dtReportCSV.Columns.Add(col);
                    if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                    {
                        col = new DataColumn("RO Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("RO Name", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("LGA Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("LGA Name", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Ward Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Ward Name", typeof(string));
                        dtReportCSV.Columns.Add(col);
                    }
                    else
                    {
                        col = new DataColumn("EMO Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("EMO Name", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("District Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("District Name", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Contest Area Id", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Contest Area Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                    }
                    col = new DataColumn("Venue Code", typeof(string));
                    dtReportCSV.Columns.Add(col);
                    col = new DataColumn("Venue Name", typeof(string));
                    dtReportCSV.Columns.Add(col);
                    col = new DataColumn("Venue Type Code", typeof(string));
                    dtReportCSV.Columns.Add(col);
                    col = new DataColumn("Venue Type Name", typeof(string));
                    dtReportCSV.Columns.Add(col);
                    if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
                    {
                        col = new DataColumn("Contest Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Contest Name", typeof(string));
                        dtReportCSV.Columns.Add(col); 
                    }
                    else
                    {
                        col = new DataColumn("Product Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Product Name", typeof(string));
                        dtReportCSV.Columns.Add(col);
                    }
                    col = new DataColumn("Container Code", typeof(string));
                    dtReportCSV.Columns.Add(col);
                    col = new DataColumn("Container Name", typeof(string));
                    dtReportCSV.Columns.Add(col);
                    col = new DataColumn("Parent Barcode", typeof(string));
                    dtReportCSV.Columns.Add(col);
                    col = new DataColumn("Deactivated", typeof(string));
                    dtReportCSV.Columns.Add(col);

                    if (enquiryParam != null && enquiryParam.IncludeStatusHistory)
                    {
                        // details
                        col = new DataColumn("Status Stage", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Status Notes", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Status Date", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Movement Direction", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Location Code", typeof(string));
                        dtReportCSV.Columns.Add(col);
                        col = new DataColumn("Location Name", typeof(string));
                        dtReportCSV.Columns.Add(col);
                    }

                    DataRow drTarget = null;
                    if (dtEnquiry != null && dtEnquiry.Rows.Count > 0)
                    {
                        foreach (DataRow drConnote in dtEnquiry.Rows)
                        {
                            if (enquiryParam != null && enquiryParam.IncludeStatusHistory)
                            {
                                DataRow[] drsConDet = dtConDet.Select(string.Format("ConsKey='{0}' AND [LineNo]={1}", drConnote["ConsKey"], drConnote["LineNo"]));
                                if (drsConDet != null && drsConDet.Length > 0)
                                {
                                    foreach (DataRow drConDet in drsConDet)
                                    {
                                        drTarget = dtReportCSV.NewRow();
                                        CopyData(loginUser, drTarget, drConnote, drConDet);
                                        dtReportCSV.Rows.Add(drTarget);
                                    }
                                }
                                else
                                {
                                    drTarget = dtReportCSV.NewRow();
                                    CopyData(loginUser, drTarget, drConnote, null);
                                    dtReportCSV.Rows.Add(drTarget);
                                }
                            }
                            else
                            {
                                drTarget = dtReportCSV.NewRow();
                                CopyData(loginUser, drTarget, drConnote, null);
                                dtReportCSV.Rows.Add(drTarget);
                            }                            
                        }
                    }
                    dsCSV.Tables.Add(dtReportCSV);
                    /*
                    if (enquiryParam != null && enquiryParam.IncludeULDChild)
                    {
                        DataTable dtULDCSV = GetBarcodeLabelRptULDCSVDataTable();
                        if (dtULDCSV != null && dtULDCSV.Rows.Count > 0)
                            dsCSV.Tables.Add(dtULDCSV);
                    }
                    */
                    Session["dsCSV"] = dsCSV;
                    e.Result = "ExportCSV";
                }
            }
            else
                e.Result = "Warning: No data found for this report!";
        }

        private static void CopyData(LoginUser loginUser, DataRow drTarget, DataRow drConnote, DataRow drConDet)
        {
            if (loginUser == null)
                return;
            drTarget["Barcode"] = drConnote["Barcode"];
            if (loginUser.EventType == EventType.LOCAL_GOVERNMENT)
            {
                drTarget["RO Code"] = drConnote["EMOCode"];
                drTarget["RO Name"] = drConnote["EMOName"];
                drTarget["LGA Code"] = drConnote["DistrictCode"];
                drTarget["LGA Name"] = drConnote["DistrictName"];
                drTarget["Ward Code"] = drConnote["WardCode"];
                drTarget["Ward Name"] = drConnote["WardName"];
                drTarget["Contest Code"] = drConnote["ProductCode"];
                drTarget["Contest Name"] = drConnote["ProductName"];
            }
            else
            {
                drTarget["EMO Code"] = drConnote["EMOCode"];
                drTarget["EMO Name"] = drConnote["EMOName"];
                drTarget["District Code"] = drConnote["DistrictCode"];
                drTarget["District Name"] = drConnote["DistrictName"];
                drTarget["Contest Area Id"] = drConnote["ContestAreaId"];
                drTarget["Contest Area Code"] = drConnote["ContestAreaCode"];
                drTarget["Product Code"] = drConnote["ProductCode"];
                drTarget["Product Name"] = drConnote["ProductName"];
            }
            
            drTarget["Venue Code"] = drConnote["VenueCode"];
            drTarget["Venue Name"] = drConnote["VenueName"];
            drTarget["Venue Type Code"] = drConnote["VenueTypeCode"];
            drTarget["Venue Type Name"] = drConnote["VenueTypeName"];
            
            drTarget["Container Code"] = drConnote["ContainerCode"];
            drTarget["Container Name"] = drConnote["ContainerName"];
            drTarget["Parent Barcode"] = drConnote["ParentBarcode"];
            drTarget["Deactivated"] = drConnote["Deactivated"];
            if (drConDet != null)
            {
                drTarget["Status Stage"] = drConDet["StatusStage"];
                drTarget["Status Notes"] = drConDet["StatusNotes"];
                if (!(drConDet["StatusDate"] is DBNull) && drConDet["StatusDate"].ToString() != string.Empty)
                    drTarget["Status Date"] = Convert.ToDateTime(drConDet["StatusDate"]).ToString("dd/MM/yyyy HH:mm:ss");
                else
                    drTarget["Status Date"] = string.Empty;
                drTarget["Movement Direction"] = drConDet["MovementDirection"];
                drTarget["Location Code"] = drConDet["LocationCode"];
                drTarget["Location Name"] = drConDet["LocationName"];
            }
        }
    }
}