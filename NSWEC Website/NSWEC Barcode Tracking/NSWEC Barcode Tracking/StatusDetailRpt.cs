﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace NSWEC_Barcode_Tracking
{
    public partial class StatusDetailRpt : DevExpress.XtraReports.UI.XtraReport
    {
        public StatusDetailRpt()
        {
            InitializeComponent();
        }

        private void StatusDetailRpt_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrDB_StatusDate.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".StatusDate", "{0:dd/MM/yyyy HH:mm:ss}"));
            xrDB_StatusNotes.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".StatusNotes", ""));
            xrDB_MovementDirection.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".MovementDirection", ""));
        }

        private void detailBand_StatusDet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            object locCode = GetCurrentColumnValue("LocationCode");
            object locName = GetCurrentColumnValue("LocationName");
            string loc = string.Empty;
            if (locName != null && locName.ToString() != string.Empty)
                loc = locName.ToString();
            if (locCode != null && locCode.ToString() != string.Empty)
            {
                if (!string.IsNullOrEmpty(loc))
                {
                    loc += string.Format("({0})", locCode);
                }
                else
                    loc = locCode.ToString();
            }
            xrLblLocation.Text = loc;
        }

    }
}
