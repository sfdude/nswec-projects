﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class SystemSetup : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "SystemSetup.aspx";
            /*
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (!(ECSecurityClass.CheckQueryString(this.Page, "loginId") && loginUser.SpecialUser))
                Response.Redirect("ConsignmentCatalogue.aspx");
             */
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (!(loginUser != null && loginUser.SpecialUser))
                Response.Redirect("Default.aspx?Logout=1");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("SS_ApplicationTable");
                Session.Remove("SS_LabelTable");
                AssignClientSideScripts();
                AssignAppData();
                LoadLabelData();

                ECWebClass.SetPageControlHeaderStyle(pcSystem);
                ECWebClass.SetMenuStyle(menAction);

                pcSystem.ActiveTabIndex = 0;
            }
        }
        private DataTable GetSMLabelDataTable()
        {
            DataTable dtLabel = Session["SS_LabelTable"] as DataTable;
            if (dtLabel == null)
            {
                dtLabel = ECWebClass.GetLabelLayoutDef(null);
                dtLabel.PrimaryKey = new DataColumn[] { dtLabel.Columns["ID"] };
                Session["SS_LabelTable"] = dtLabel;
            }
            return dtLabel;
        }
        private void LoadLabelData()
        {
            cmbLabel.Items.Clear();
            DataTable dtLabel = GetSMLabelDataTable();
            foreach (DataRow dr in dtLabel.Rows)
                cmbLabel.Items.Add(dr["LABEL_NAME"].ToString(), dr["ID"]);
            cmbLabel.Items.Insert(0, new ListEditItem("[None]", null));
        }
        private void AssignClientSideScripts()
        {
            menAction.ClientSideEvents.ItemClick = @"function(s, e) { 
                if (cbSystem.InCallback()) return;             
                var tab =  pcSystem.GetActiveTab(); 
                pcSystem.SetActiveTab(pcSystem.GetTabByName('tabSystem')); 
                if (!ASPxClientEdit.ValidateEditorsInContainerById(pcSystem.GetTab(0).name)) return; 
                if (edtSupportPassword.GetValue() != edtPasswordRepeat.GetValue()) {
                    alert('Password does not match'); return; }
                if (tab != pcSystem.GetActiveTab()) pcSystem.SetActiveTab(tab); 
                cbSystem.PerformCallback(e.item.name); 
            } ";
            // redirect using javascripts
            cbSystem.ClientSideEvents.CallbackComplete = @"function(s, e) { 
                if (e.result != null && e.result != '') { 
                    if (e.result.indexOf('Error') > -1) {
                        alert(e.result); return; 
                    } 
                    window.location = e.result;
                }} ";
        }
        private DataTable GetAppDataTable()
        {
            DataTable dtApp = Session["SS_ApplicationTable"] as DataTable;
            if (dtApp == null)
            {
                dtApp = ECSecurityClass.GetApplicationDataTable();
                Session["SS_ApplicationTable"] = dtApp;
            }
            return dtApp;
        }
        private void AssignAppData()
        {
            DataTable dtApp = GetAppDataTable();
            DataRow drApp;
            bool foundApp = dtApp != null && dtApp.Rows.Count > 0;
            if (foundApp)
            {
                drApp = dtApp.Rows[0];
                if (drApp["Name"] != DBNull.Value && drApp["Name"].ToString() != string.Empty)
                    edtAppName.Value = drApp["Name"];
                if (drApp["WebApplicationTitle"] != DBNull.Value && drApp["WebApplicationTitle"].ToString() != string.Empty)
                    edtWebSiteTitle.Value = drApp["WebApplicationTitle"];
                if (drApp["WebInfo"] != DBNull.Value && drApp["WebInfo"].ToString() != string.Empty)
                    memoWebInfo.Value = drApp["WebInfo"];
                if (drApp["SessionTimeOut"] != DBNull.Value && drApp["SessionTimeOut"].ToString() != string.Empty)
                    edtTimeout.Value = drApp["SessionTimeOut"];
                if (drApp["MinPasswordChars"] != DBNull.Value && drApp["MinPasswordChars"].ToString() != string.Empty)
                    edtMinPwdChar.Value = drApp["MinPasswordChars"];
                if (drApp["MinPasswordUppercaseChars"] != DBNull.Value && drApp["MinPasswordUppercaseChars"].ToString() != string.Empty)
                    edtMinPwdUpperChar.Value = drApp["MinPasswordUppercaseChars"];
                if (drApp["MinPasswordSpecialChars"] != DBNull.Value && drApp["MinPasswordSpecialChars"].ToString() != string.Empty)
                    edtMinPwdSpecialChar.Value = drApp["MinPasswordSpecialChars"];
                if (drApp["PasswordSpecialCharList"] != DBNull.Value && drApp["PasswordSpecialCharList"].ToString() != string.Empty)
                    edtPwdSpecCharList.Value = drApp["PasswordSpecialCharList"];
                if (drApp["DaysBeforePasswordExpires"] != DBNull.Value && drApp["DaysBeforePasswordExpires"].ToString() != string.Empty)
                    edtPwdExpiryDays.Value = drApp["DaysBeforePasswordExpires"];

                if (drApp["MinPasswordNumericChars"] != DBNull.Value && drApp["MinPasswordNumericChars"].ToString() != string.Empty)
                    edtMinPwdNumericChar.Value = drApp["MinPasswordNumericChars"];
                if (drApp["MinPasswordLength"] != DBNull.Value && drApp["MinPasswordLength"].ToString() != string.Empty)
                    edtMinPwdLength.Value = drApp["MinPasswordLength"];
                if (drApp["MaxPasswordLength"] != DBNull.Value && drApp["MaxPasswordLength"].ToString() != string.Empty)
                    edtMaxPwdLength.Value = drApp["MaxPasswordLength"];

                if (drApp["SupportLogin"] != DBNull.Value && drApp["SupportLogin"].ToString() != string.Empty)
                    edtSupportLogin.Value = drApp["SupportLogin"];
                if (drApp["SupportPassword"] != DBNull.Value && drApp["SupportPassword"].ToString() != string.Empty)
                {
                    edtSupportPassword.Value = ECSecurityClass.GetPValueDecripted(drApp["SupportPassword"].ToString());
                    edtPasswordRepeat.Value = edtSupportPassword.Value;
                }

                if (!(drApp["PreferredEvent"] is DBNull) && drApp["PreferredEvent"].ToString() != string.Empty)
                    cmbEventType.Value = drApp["PreferredEvent"].ToString();
                if (!(drApp["PreferredLabel"] is DBNull) && drApp["PreferredLabel"].ToString() != string.Empty)
                    cmbLabel.Value = drApp["PreferredLabel"];
                else
                    cmbLabel.Value = null;

                if (!(drApp["PPDMVersion"] is DBNull) && drApp["PPDMVersion"].ToString() != string.Empty)
                    lblPPDMVer.Value = drApp["PPDMVersion"];
                else
                    lblPPDMVer.Value = null;
                if (!(drApp["PPDMLoaded"] is DBNull) && drApp["PPDMLoaded"].ToString() != string.Empty)
                    lblPPDMLoaded.Value = Convert.ToDateTime(drApp["PPDMLoaded"]).ToString("dd/MM/yyyy HH:mm:ss");
                else
                    lblPPDMLoaded.Value = null;

                // Communication setting
                if (drApp["OutgoingMailHost"] != DBNull.Value && drApp["OutgoingMailHost"].ToString() != string.Empty)
                    edtMailHost.Value = drApp["OutgoingMailHost"];
                if (drApp["OutgoingMailPort"] != DBNull.Value && drApp["OutgoingMailPort"].ToString() != string.Empty)
                    edtPort.Value = drApp["OutgoingMailPort"];
                if (drApp["OutgoingMailUserName"] != DBNull.Value && drApp["OutgoingMailUserName"].ToString() != string.Empty)
                    edtUserName.Value = drApp["OutgoingMailUserName"];
                if (drApp["OutgoingMailPassword"] != DBNull.Value && drApp["OutgoingMailPassword"].ToString() != string.Empty)
                    edtPassword.Value = ECSecurityClass.GetPValueDecripted(drApp["OutgoingMailPassword"].ToString());
                if (drApp["OutgoingMailFromAddress"] != DBNull.Value && drApp["OutgoingMailFromAddress"].ToString() != string.Empty)
                    edtFromAddress.Value = drApp["OutgoingMailFromAddress"];
                if (drApp["OutgoingMailFromName"] != DBNull.Value && drApp["OutgoingMailFromName"].ToString() != string.Empty)
                    edtFromName.Value = drApp["OutgoingMailFromName"];

                // system Notice
                if (drApp["Notice"] != DBNull.Value && drApp["Notice"].ToString() != string.Empty)
                    memSysNotice.Value = drApp["Notice"];
            }
            else
            {
                drApp = dtApp.NewRow();
                drApp["ID"] = -1;
                drApp["SessionTimeOut"] = 20;
                drApp["MinPasswordChars"] = 0;
                drApp["MinPasswordUppercaseChars"] = 0;
                drApp["MinPasswordSpecialChars"] = 0;
                drApp["DaysBeforePasswordExpires"] = 0;

                drApp["MinPasswordNumericChars"] = 0;
                drApp["MinPasswordLength"] = 0;
                drApp["MaxPasswordLength"] = 0;

                dtApp.Rows.Add(drApp);

                edtTimeout.Value = drApp["SessionTimeOut"];
                edtMinPwdChar.Value = drApp["MinPasswordChars"];
                edtMinPwdUpperChar.Value = drApp["MinPasswordUppercaseChars"];
                edtMinPwdSpecialChar.Value = drApp["MinPasswordSpecialChars"];
                edtPwdExpiryDays.Value = drApp["DaysBeforePasswordExpires"];

                lblPPDMLoaded.Text = string.Empty;
                lblPPDMVer.Text = string.Empty;
            }
        }

        private void UpdateSystemData()
        {
            DataTable dtApp = GetAppDataTable();
            if (dtApp != null && dtApp.Rows.Count > 0)
            {
                DataRow drApp = dtApp.Rows[0];
                if (edtAppName.Value != null && edtAppName.Value.ToString() != string.Empty)
                    drApp["Name"] = edtAppName.Value;
                if (edtWebSiteTitle.Value != null && edtWebSiteTitle.Value.ToString() != string.Empty)
                    drApp["WebApplicationTitle"] = edtWebSiteTitle.Value;
                else
                    drApp["WebApplicationTitle"] = DBNull.Value;
                if (memoWebInfo.Value != null && memoWebInfo.Value.ToString() != string.Empty)
                    drApp["WebInfo"] = memoWebInfo.Value;
                else
                    drApp["WebInfo"] = DBNull.Value;

                if (edtTimeout.Value != null && edtTimeout.Value.ToString() != string.Empty)
                    drApp["SessionTimeOut"] = edtTimeout.Value;
                else
                    drApp["SessionTimeOut"] = DBNull.Value;

                if (edtMinPwdChar.Value != null && edtMinPwdChar.Value.ToString() != string.Empty)
                    drApp["MinPasswordChars"] = edtMinPwdChar.Value;
                else
                    drApp["MinPasswordChars"] = DBNull.Value;
                if (edtMinPwdUpperChar.Value != null && edtMinPwdUpperChar.Value.ToString() != string.Empty)
                    drApp["MinPasswordUppercaseChars"] = edtMinPwdUpperChar.Value;
                else
                    drApp["MinPasswordUppercaseChars"] = DBNull.Value;

                if (edtMinPwdSpecialChar.Value != null && edtMinPwdSpecialChar.Value.ToString() != string.Empty)
                    drApp["MinPasswordSpecialChars"] = edtMinPwdSpecialChar.Value;
                else
                    drApp["MinPasswordSpecialChars"] = DBNull.Value;
                if (edtPwdSpecCharList.Value != null && edtPwdSpecCharList.Value.ToString() != string.Empty)
                    drApp["PasswordSpecialCharList"] = edtPwdSpecCharList.Value;
                else
                    drApp["PasswordSpecialCharList"] = DBNull.Value;
                if (edtPwdExpiryDays.Value != null && edtPwdExpiryDays.Value.ToString() != string.Empty)
                    drApp["DaysBeforePasswordExpires"] = edtPwdExpiryDays.Value;
                else
                    drApp["DaysBeforePasswordExpires"] = DBNull.Value;

                if (edtMinPwdNumericChar.Value != null && edtMinPwdNumericChar.Value.ToString() != string.Empty)
                    drApp["MinPasswordNumericChars"] = edtMinPwdNumericChar.Value;
                else
                    drApp["MinPasswordNumericChars"] = DBNull.Value;
                if (edtMinPwdLength.Value != null && edtMinPwdLength.Value.ToString() != string.Empty)
                    drApp["MinPasswordLength"] = edtMinPwdLength.Value;
                else
                    drApp["MinPasswordLength"] = DBNull.Value;
                if (edtMaxPwdLength.Value != null && edtMaxPwdLength.Value.ToString() != string.Empty)
                    drApp["MaxPasswordLength"] = edtMaxPwdLength.Value;
                else
                    drApp["MaxPasswordLength"] = DBNull.Value;

                if (edtSupportLogin.Value != null && edtSupportLogin.Value.ToString() != string.Empty)
                    drApp["SupportLogin"] = edtSupportLogin.Value;
                if (edtSupportPassword.Value != null && edtSupportPassword.Value.ToString() != string.Empty)
                    drApp["SupportPassword"] = ECSecurityClass.GetPValueEncripted(edtSupportPassword.Value.ToString());

                if (cmbEventType.SelectedIndex >= 0)
                    drApp["PreferredEvent"] = cmbEventType.Value;
                else
                    drApp["PreferredEvent"] = 0;
                if (cmbLabel.SelectedIndex > 0)
                    drApp["PreferredLabel"] = Convert.ToInt32(cmbLabel.Value);
                else
                    drApp["PreferredLabel"] = DBNull.Value;

                // Communication setting
                if (edtMailHost.Value != null && edtMailHost.Value.ToString() != string.Empty)
                    drApp["OutgoingMailHost"] = edtMailHost.Value;
                else
                    drApp["OutgoingMailHost"] = DBNull.Value;
                if (edtPort.Value != null && edtPort.Value.ToString() != string.Empty)
                    drApp["OutgoingMailPort"] = edtPort.Value;
                else
                    drApp["OutgoingMailPort"] = DBNull.Value;
                if (edtUserName.Value != null && edtUserName.Value.ToString() != string.Empty)
                    drApp["OutgoingMailUserName"] = edtUserName.Value;
                else
                    drApp["OutgoingMailUserName"] = DBNull.Value;
                if (edtPassword.Value != null && edtPassword.Value.ToString() != string.Empty)
                    drApp["OutgoingMailPassword"] = ECSecurityClass.GetPValueEncripted(edtPassword.Value.ToString());
                else
                    drApp["OutgoingMailPassword"] = DBNull.Value;
                if (edtFromAddress.Value != null && edtFromAddress.Value.ToString() != string.Empty)
                    drApp["OutgoingMailFromAddress"] = edtFromAddress.Value;
                else
                    drApp["OutgoingMailFromAddress"] = DBNull.Value;
                if (edtFromName.Value != null && edtFromName.Value.ToString() != string.Empty)
                    drApp["OutgoingMailFromName"] = edtFromName.Value;
                else
                    drApp["OutgoingMailFromName"] = DBNull.Value;

                if (memSysNotice.Value != null && memSysNotice.Value.ToString() != string.Empty)
                    drApp["Notice"] = memSysNotice.Value;
                else
                    drApp["Notice"] = DBNull.Value;

                if (Convert.ToInt32(drApp["ID"]) < 0)
                {
                    drApp["Created"] = DateTime.Now;
                    drApp["CreatedBy"] = (Session["EC_User"] as LoginUser).UserName;
                }
                else
                {
                    drApp["Modified"] = DateTime.Now;
                    drApp["ModifiedBy"] = (Session["EC_User"] as LoginUser).UserName;
                }
            }
        }

        protected void edtSupportPassword_PreRender(object sender, EventArgs e)
        {
            ASPxTextBox edit = sender as ASPxTextBox;
            edit.ClientSideEvents.Init = String.Format("function(s, e) {{s.SetText('{0}');}}", edit.Text);
        }
        protected void cbSystem_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                bool success = false;
                string errorMsg = string.Empty;
                e.Result = "ConsignmentCatalogue.aspx";
                if (e.Parameter.Contains("Save"))
                {
                    UpdateSystemData();

                    success = ECSecurityClass.UpdateApplication(ref errorMsg, GetAppDataTable());
                    if (e.Parameter == "btnSave")
                        e.Result = "SystemSetup.aspx?qs=" + ECSecurityClass.GetQSEncripted("loginId=" + (Session["EC_User"] as LoginUser).UserName);
                }
                if (!success)
                    e.Result = "Error: " + errorMsg;
                else
                {
                    LoginUser loginUser = Session["EC_User"] as LoginUser;
                    Session["SS_ApplicationTable"] = null;
                    DataTable dtApp = GetAppDataTable();
                    if (dtApp != null && dtApp.Rows.Count > 0 && !(dtApp.Rows[0]["PreferredEvent"] is DBNull))
                        loginUser.EventType = (NSWEC.Net.EventType)(Convert.ToInt32(dtApp.Rows[0]["PreferredEvent"]));                    
                }
            }
        }
    }
}