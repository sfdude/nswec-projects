﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.XtraReports.UI;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class BarcodeLabelRptViewer : System.Web.UI.Page
    {
        BarcodeLabelReportParam enquiryParam;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "BarcodeLabelRptViewer.aspx";
            enquiryParam = Session["BarcodeLabelRptParam"] as BarcodeLabelReportParam;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("BarcodeLabelRptDataTable");
                Session.Remove("BarcodeLabelRptDetailDataTable");
                Session.Remove("BarcodeLabelRptULDDetailDataTable");

                ReportToolbar1.ClientSideEvents.ItemClick =
                    @"function(s, e) { 
                    if(e.item.name =='btnExportCSV') {
                        if (cbExportCSV.InCallback()) return;
                        cbExportCSV.PerformCallback('ExportCSV'); 
                    } else
                    if(e.item.name =='btnBack') {
                        window.location = 'BarcodeLabelRptParam.aspx';
                    }
                }";
                cbExportCSV.ClientSideEvents.CallbackComplete = "function(s, e) { window.open('CSVDataHandler.ashx', 'Download'); }";
            }

            DataTable dtBarcodeLabelRpt = GetBarcodeLabelRptDataTable();
            /*
            if (enquiryParam.ExportToCSV)
            {
                DataTable dtBarcodeLabelDetailRpt = GetBarcodeLabelRptDetailDataTable();
                DataSet dsCSV = new DataSet("dsCSV");
                DataTable dtCSV = GetBarcodeLabelRptCSVDataTable();
                dsCSV.Tables.Add(dtCSV);
                Session["dsCSV"] = dsCSV;
            }
             */
            LoginUser loginUser = Session["EC_User"] as LoginUser;

            SGEBarcodeLabelRpt rpt = new SGEBarcodeLabelRpt(loginUser.EventType == EventType.LOCAL_GOVERNMENT);
            rpt.DataSource = dtBarcodeLabelRpt;
            rpt.DataMember = dtBarcodeLabelRpt.TableName;

            XRSubreport statusSubRpt = rpt.FindControl("statusSubRpt", true) as XRSubreport;
            if (statusSubRpt != null)
            {
                statusSubRpt.Visible = enquiryParam != null && enquiryParam.IncludeStatusHistory;
                if (statusSubRpt.Visible)
                {
                    DataTable dtBarcodeLabelDetailRpt = GetBarcodeLabelRptDetailDataTable();
                    statusSubRpt.Visible = dtBarcodeLabelDetailRpt != null && dtBarcodeLabelDetailRpt.Rows.Count > 0;
                    if (statusSubRpt.Visible)
                    {
                        ((XtraReport)statusSubRpt.ReportSource).DataSource = dtBarcodeLabelDetailRpt;
                        ((XtraReport)statusSubRpt.ReportSource).DataMember = dtBarcodeLabelDetailRpt.TableName;
                        ((XtraReport)statusSubRpt.ReportSource).FilterString = "[ConsKey] = ?ConsKey AND [LineNo] = ?LineNo";
                    }
                }
            }

            XRSubreport uldSubRpt = rpt.FindControl("uldSubRpt", true) as XRSubreport;
            if (uldSubRpt != null)
            {
                uldSubRpt.Visible = enquiryParam != null && enquiryParam.IncludeULDChild;
                if (uldSubRpt.Visible)
                {
                    DataTable dtBarcodeLabelULDDetailRpt = GetBarcodeLabelRptULDDetailDataTable();
                    uldSubRpt.Visible = dtBarcodeLabelULDDetailRpt != null && dtBarcodeLabelULDDetailRpt.Rows.Count > 0;
                    if (uldSubRpt.Visible)
                    {
                        ((XtraReport)uldSubRpt.ReportSource).DataSource = dtBarcodeLabelULDDetailRpt;
                        ((XtraReport)uldSubRpt.ReportSource).DataMember = dtBarcodeLabelULDDetailRpt.TableName;
                        ((XtraReport)uldSubRpt.ReportSource).FilterString = "[ConsKey] = ?ConsKey AND [LineNo] = ?LineNo";
                    }
                }
            }
            
            BarcodeLabelReportViewer.Report = rpt;
        }
        private DataTable GetBarcodeLabelRptDataTable()
        {
            DataTable dt = Session["BarcodeLabelRptDataTable"] as DataTable;
            if (dt == null && enquiryParam != null)
            {
                dt = ECWebClass.GetSGEBarcodeLabelRptData(enquiryParam);
                Session["BarcodeLabelRptDataTable"] = dt;
            }
            return dt;
        }
        private DataTable GetBarcodeLabelRptDetailDataTable()
        {
            DataTable dt = Session["BarcodeLabelRptDetailDataTable"] as DataTable;
            if (dt == null && enquiryParam != null)
            {
                dt = ECWebClass.GetSGEBarcodeLabelRptDetailData(enquiryParam);
                Session["BarcodeLabelRptDetailDataTable"] = dt;
            }
            return dt;
        }

        private DataTable GetBarcodeLabelRptULDDetailDataTable()
        {
            DataTable dt = Session["BarcodeLabelRptULDDetailDataTable"] as DataTable;
            if (dt == null && enquiryParam != null)
            {
                dt = ECWebClass.GetSGEBarcodeLabelRptULDDetailData(enquiryParam);
                Session["BarcodeLabelRptULDDetailDataTable"] = dt;
            }
            return dt;
        }

        private static void CopyData(DataRow drTarget, DataRow drConnote, DataRow drConDet)
        {
            drTarget["Barcode"] = drConnote["Barcode"];
            drTarget["EMO Code"] = drConnote["EMOCode"];
            drTarget["EMO Name"] = drConnote["EMOName"];
            drTarget["District Code"] = drConnote["DistrictCode"];
            drTarget["District Name"] = drConnote["DistrictName"];
            drTarget["Contest Area Id"] = drConnote["ContestAreaId"];
            drTarget["Contest Area Code"] = drConnote["ContestAreaCode"];
            drTarget["Venue Code"] = drConnote["VenueCode"];
            drTarget["Venue Name"] = drConnote["VenueName"];
            drTarget["Venue Type Code"] = drConnote["VenueTypeCode"];
            drTarget["Venue Type Name"] = drConnote["VenueTypeName"];
            drTarget["Product Code"] = drConnote["ProductCode"];
            drTarget["Product Name"] = drConnote["ProductName"];
            drTarget["Container Code"] = drConnote["ContainerCode"];
            drTarget["Container Name"] = drConnote["ContainerName"];
            drTarget["Parent Barcode"] = drConnote["ParentBarcode"];
            drTarget["Deactivated"] = drConnote["Deactivated"];
            if (drConDet != null)
            {
                drTarget["Status Stage"] = drConDet["StatusStage"];
                drTarget["Status Notes"] = drConDet["StatusNotes"];
                if (!(drConDet["StatusDate"] is DBNull) && drConDet["StatusDate"].ToString() != string.Empty)
                    drTarget["Status Date"] = Convert.ToDateTime(drConDet["StatusDate"]).ToString("dd/MM/yyyy HH:mm:ss");
                else
                    drTarget["Status Date"] = string.Empty;
                drTarget["Movement Direction"] = drConDet["MovementDirection"];
                drTarget["Location Code"] = drConDet["LocationCode"];
                drTarget["Location Name"] = drConDet["LocationName"];
            }
            /*
            else
            {
                drTarget["Status Stage"] = string.Empty;
                drTarget["Status Notes"] = string.Empty;
                drTarget["Status Date"] = string.Empty;
                drTarget["Movement Direction"] = string.Empty;
                drTarget["Location Code"] = string.Empty;
                drTarget["Location Name"] = string.Empty;
            }
             */
        }

        private static void CopyULDData(DataRow drTarget, DataRow drConnote, DataRow drConDet)
        {
            drTarget["Barcode"] = drConnote["Barcode"];
            drTarget["EMO Code"] = drConnote["EMOCode"];
            drTarget["EMO Name"] = drConnote["EMOName"];
            drTarget["District Code"] = drConnote["DistrictCode"];
            drTarget["District Name"] = drConnote["DistrictName"];
            drTarget["Contest Area Id"] = drConnote["ContestAreaId"];
            drTarget["Contest Area Code"] = drConnote["ContestAreaCode"];
            drTarget["Venue Code"] = drConnote["VenueCode"];
            drTarget["Venue Name"] = drConnote["VenueName"];
            drTarget["Venue Type Code"] = drConnote["VenueTypeCode"];
            drTarget["Venue Type Name"] = drConnote["VenueTypeName"];
            drTarget["Product Code"] = drConnote["ProductCode"];
            drTarget["Product Name"] = drConnote["ProductName"];
            drTarget["Container Code"] = drConnote["ContainerCode"];
            drTarget["Container Name"] = drConnote["ContainerName"];            
            drTarget["Deactivated"] = drConnote["Deactivated"];
            if (drConDet != null)
            {
                drTarget["Child Barcode"] = drConDet["Barcode"];
                drTarget["Child EMO Code"] = drConDet["EMOCode"];
                drTarget["Child EMO Name"] = drConDet["EMOName"];
                drTarget["Child District Code"] = drConDet["DistrictCode"];
                drTarget["Child District Name"] = drConDet["DistrictName"];

                drTarget["Child Contest Area Id"] = drConDet["ContestAreaId"];
                drTarget["Child Contest Area Code"] = drConDet["ContestAreaCode"];
                drTarget["Child Venue Code"] = drConDet["VenueCode"];
                drTarget["Child Venue Name"] = drConDet["VenueName"];
                drTarget["Child Venue Type Code"] = drConDet["VenueTypeCode"];
                drTarget["Child Venue Type Name"] = drConDet["VenueTypeName"];
                drTarget["Child Product Code"] = drConDet["ProductCode"];
                drTarget["Child Product Name"] = drConDet["ProductName"];
                drTarget["Child Container Code"] = drConDet["ContainerCode"];
                drTarget["Child Container Name"] = drConDet["ContainerName"];
            }
        }

        private DataTable GetBarcodeLabelRptULDCSVDataTable()
        {
            DataTable dtConnote = GetBarcodeLabelRptDataTable();
            DataTable dtConDet = GetBarcodeLabelRptULDDetailDataTable();

            DataTable dtReportCSV = new DataTable("BarcodeLabelULDReport");

            DataColumn col = new DataColumn("Barcode", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("EMO Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("EMO Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("District Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("District Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Contest Area Id", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Contest Area Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Venue Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Venue Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Venue Type Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Venue Type Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Product Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Product Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Container Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Container Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Deactivated", typeof(string));
            dtReportCSV.Columns.Add(col);
            
            // details
            col = new DataColumn("Child Barcode", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child EMO Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child EMO Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child District Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child District Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Contest Area Id", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Contest Area Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Venue Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Venue Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Venue Type Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Venue Type Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Product Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Product Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Container Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Child Container Name", typeof(string));
            dtReportCSV.Columns.Add(col);            

            DataRow drTarget = null;
            if (dtConnote != null && dtConnote.Rows.Count > 0)
            {
                DataRow[] drsConnote = dtConnote.Select("ContainerName='ULD'");
                if (drsConnote != null && drsConnote.Length > 0)
                {
                    foreach (DataRow drConnote in drsConnote)
                    {
                        DataRow[] drsConDet = dtConDet.Select(string.Format("ConsKey='{0}' AND [LineNo]={1}", drConnote["ConsKey"], drConnote["LineNo"]));
                        if (drsConDet != null && drsConDet.Length > 0)
                        {
                            foreach (DataRow drConDet in drsConDet)
                            {
                                drTarget = dtReportCSV.NewRow();
                                CopyULDData(drTarget, drConnote, drConDet);
                                dtReportCSV.Rows.Add(drTarget);
                            }
                        }
                        else
                        {
                            drTarget = dtReportCSV.NewRow();
                            CopyULDData(drTarget, drConnote, null);
                            dtReportCSV.Rows.Add(drTarget);
                        }                    
                    }
                }
            }

            return dtReportCSV;
        }

        private DataTable GetBarcodeLabelRptCSVDataTable()
        {
            DataTable dtConnote = GetBarcodeLabelRptDataTable();
            DataTable dtConDet;
            if (enquiryParam != null && enquiryParam.IncludeStatusHistory)
                dtConDet = GetBarcodeLabelRptDetailDataTable();
            else
                dtConDet = null;

            DataTable dtReportCSV = new DataTable("BarcodeLabelReport");

            DataColumn col = new DataColumn("Barcode", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("EMO Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("EMO Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("District Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("District Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Contest Area Id", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Contest Area Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Venue Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Venue Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Venue Type Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Venue Type Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Product Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Product Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Container Code", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Container Name", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Parent Barcode", typeof(string));
            dtReportCSV.Columns.Add(col);
            col = new DataColumn("Deactivated", typeof(string));
            dtReportCSV.Columns.Add(col);

            if (enquiryParam != null && enquiryParam.IncludeStatusHistory)
            {
                // details
                col = new DataColumn("Status Stage", typeof(string));
                dtReportCSV.Columns.Add(col);
                col = new DataColumn("Status Notes", typeof(string));
                dtReportCSV.Columns.Add(col);
                col = new DataColumn("Status Date", typeof(string));
                dtReportCSV.Columns.Add(col);
                col = new DataColumn("Movement Direction", typeof(string));
                dtReportCSV.Columns.Add(col);
                col = new DataColumn("Location Code", typeof(string));
                dtReportCSV.Columns.Add(col);
                col = new DataColumn("Location Name", typeof(string));
                dtReportCSV.Columns.Add(col);
            }
            
            DataRow drTarget = null;
            if (dtConnote != null && dtConnote.Rows.Count > 0)
            {
                foreach (DataRow drConnote in dtConnote.Rows)
                {
                    if (enquiryParam != null && enquiryParam.IncludeStatusHistory)
                    {
                        DataRow[] drsConDet = dtConDet.Select(string.Format("ConsKey='{0}' AND [LineNo]={1}", drConnote["ConsKey"], drConnote["LineNo"]));
                        if (drsConDet != null && drsConDet.Length > 0)
                        {
                            foreach (DataRow drConDet in drsConDet)
                            {
                                drTarget = dtReportCSV.NewRow();
                                CopyData(drTarget, drConnote, drConDet);
                                dtReportCSV.Rows.Add(drTarget);
                            }
                        }
                        else
                        {
                            drTarget = dtReportCSV.NewRow();
                            CopyData(drTarget, drConnote, null);
                            dtReportCSV.Rows.Add(drTarget);
                        }
                    }
                    else
                    {
                        drTarget = dtReportCSV.NewRow();
                        CopyData(drTarget, drConnote, null);
                        dtReportCSV.Rows.Add(drTarget);
                    }
                    /*
                    DataRow[] drsConDet = dtConDet.Select(string.Format("ConsKey={0} AND [LineNo]={1}", drConnote["ConsKey"], drConnote["LineNo"]));
                    if (drsConDet != null && drsConDet.Length > 0)
                    {
                        foreach (DataRow drConDet in drsConDet)
                        {
                            drTarget = dtReportCSV.NewRow();
                            CopyData(drTarget, drConnote, drConDet);
                            dtReportCSV.Rows.Add(drTarget);
                        }
                    }
                    else
                    {
                        drTarget = dtReportCSV.NewRow();
                        CopyData(drTarget, drConnote, null);
                        dtReportCSV.Rows.Add(drTarget);
                    }
                     */
                }                
            }

            return dtReportCSV;
        }

        protected void cbExportCSV_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                DataSet dsCSV = new DataSet("dsCSV");

                DataTable dtCSV = GetBarcodeLabelRptCSVDataTable();
                dsCSV.Tables.Add(dtCSV);

                if (enquiryParam != null && enquiryParam.IncludeULDChild)
                {
                    DataTable dtULDCSV = GetBarcodeLabelRptULDCSVDataTable();
                    if (dtULDCSV != null && dtULDCSV.Rows.Count > 0)
                        dsCSV.Tables.Add(dtULDCSV);
                }

                Session["dsCSV"] = dsCSV;
            }
        }
    }
}