﻿<%@ Page Title="Retrieve Password" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="RetrievePassword.aspx.cs" Inherits="NSWEC_Barcode_Tracking.RetrievePassword" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="position: static" id="mainTable">
        <tr>
            <td colspan="4" align="left">
                <span style="font-size: 10pt">You may contact your Administrator or provide us the email
                    address you used when you first registered to this service.<br />
                    We will then send your login details through this email.
                </span>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
            </td>
            <td colspan="2" style="width: 40%">
            </td>
            <td style="width: 45%">
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 15%">
                <dxe:ASPxLabel ID="lblUser" runat="server" Text="Login Name:">
                </dxe:ASPxLabel>
            </td>
            <td colspan="2" style="width: 40%">
                <dxe:ASPxTextBox ID="edtLogin" runat="server" BackColor="Khaki" Width="100%">
                    <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                        ErrorText="" SetFocusOnError="True" ValidationGroup="RetrievePass">
                        <RegularExpression ErrorText="" />
                        <RequiredField ErrorText="Please enter your login name" IsRequired="True" />
                    </ValidationSettings>
                </dxe:ASPxTextBox>                
            </td>
            <td style="width: 45%">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 15%">
            </td>
            <td style="width: 20%" align="left">
                <dxe:ASPxButton ID="btnSendPass" runat="server" EnableClientSideAPI="True" EnableTheming="True"
                    OnClick="btnSendPass_Click" Text="Retrieve Password" ValidationGroup="RetrievePass" Width="90%" >
                </dxe:ASPxButton>
            </td>
            <td style="width: 20%" align="right">
                <dxe:ASPxButton ID="btnCancel" runat="server" EnableClientSideAPI="True" EnableTheming="True" Text="Cancel"
                    PostBackUrl="~/Default.aspx" CausesValidation="False" Width="90%">
                </dxe:ASPxButton>
            </td>
            <td style="width: 45%">
            </td>
        </tr>
    </table>
    
    <dxpc:aspxpopupcontrol id="pcMessage" runat="server" allowdragging="True" clientinstancename="pcMessage"
        closeaction="CloseButton" enableanimation="False" enableviewstate="False" headertext="Request submitted"
        height="80px" modal="True" popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter" 
        width="350px" ShowCloseButton="False" ShowFooter="True">
        <ContentStyle HorizontalAlign="Left">
            <Paddings PaddingBottom="5px"></Paddings>
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <dxe:ASPxLabel ID="lblMessage" runat="server" Text="msg"> </dxe:ASPxLabel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <FooterTemplate>
            <table style="width:100%;">
                <tr>
                    <td align="right" style="width: 89%">
                    </td>
                    <td align="right" style="width: 8%">
                        <dxe:ASPxButton ID="btnOK" runat="server" Text="OK" 
                            ClientInstanceName="btnOK" CausesValidation="False" PostBackUrl="~/Default.aspx">
                        </dxe:ASPxButton>
                    </td>
                    <td style="width: 3%"></td>
                </tr>
            </table>
        </FooterTemplate>
        <HeaderStyle HorizontalAlign="Left" />
    </dxpc:aspxpopupcontrol>
</asp:Content>
