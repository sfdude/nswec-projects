﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class ErrorPage : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cache["ErrorMessage"] != null && Cache["ErrorMessage"].ToString() != string.Empty)
                lblError.Value = Cache["ErrorMessage"].ToString();
        }
    }
}