﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
//using System.Web.Security.AntiXss;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class UserSetup : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "UserSetup.aspx";
            /*
            if (!ECSecurityClass.CheckQueryString(this.Page, "loginId"))
                Response.Redirect("ConsignmentCatalogue.aspx");
             */
            string tgtLogin = string.Empty;
            if (!ECSecurityClass.CheckQueryString(this.Page, "loginId", ref tgtLogin))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || (!loginUser.IsAdmin && string.Compare(loginUser.UserName, tgtLogin, true) != 0))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("US_ROTable");
                Session.Remove("US_LoginTable");
                Session.Remove("US_RoleTable");
                Session.Remove("US_PWDChanged");

                Session["US_PWDChanged"] = false;

                ECWebClass.LoadThemeList(cmbThemes);
                AssignClientSideScripts();
                SetPasswordPolicy();
                //AssignUserData();
                AssignUserDataAdv();

                LoadSecurityRoleDataTable();
                SelectSecurityRole();
                //UserAccessControl();
                
                ECWebClass.SetMenuStyle(menAction);
                ECWebClass.SetGridViewStyle(grdUserRole);
                ECWebClass.SetPageControlHeaderStyle(pcUser);

                LoginUser loginUser = Session["EC_User"] as LoginUser;
                pcUser.ShowTabs = loginUser != null && loginUser.IsAdmin;

                pcUser.ActiveTabIndex = 0;
            }
            if (IsCallback)
                LoadSecurityRoleDataTable();
            LoadROData();
        }
        private void AssignClientSideScripts()
        {
            edtUserName.ClientSideEvents.LostFocus = "function(s, e) { if (cbLoginCheck.InCallback()) return; " +
            @"if (s.GetText() == '') spanAvailability.innerHTML = ''; else { " +
            @"spanAvailability.innerHTML = ""<span style='color: #ccc;'>checking...</span>""; " +
            "cbLoginCheck.PerformCallback(s.GetText()); } }";
            edtPassword.ClientSideEvents.LostFocus = "function(s, e) { edtPasswordRepeat.Focus(); edtPasswordRepeat.SelectAll(); } ";
            cbLoginCheck.ClientSideEvents.CallbackComplete = @"function(s, e) { " +
                "if (e.result != null && e.result != '') { " +
                @"var innerHtml = ""<span style='color: Red;'>"" + e.result + ""</span>""; " +
                @"spanAvailability.innerHTML = innerHtml; } else spanAvailability.innerHTML = ''; } ";

            grdUserRole.ClientSideEvents.SelectionChanged = "function(s, e) { " +
                "cbAll.SetChecked(s.GetSelectedRowCount() == s.cpUserRoleVisibleRowCount); }";
            
            menAction.ClientSideEvents.ItemClick = @"function(s, e) { 
                if (cbLogin.InCallback()) return;
                if (e.item.name == 'btnDeleteUser') { 
                    if (!confirm('Confirm delete?')) return; 
                    cbLogin.PerformCallback(e.item.name); 
                } else {
                    if (spanAvailability.textContent != '') return; 
                    if(!ASPxClientEdit.ValidateEditorsInContainerById('loginContainer')) return; 
                    if (edtPassword.GetValue() != edtPasswordRepeat.GetValue()) {
                        alert('Password does not match'); return; 
                    } 
                    cbLogin.PerformCallback(e.item.name); 
                }
            } ";
            //menAction.ClientSideEvents.ItemClick = string.Empty;
            // redirect using javascripts
            cbLogin.ClientSideEvents.CallbackComplete = @"function(s, e) { 
                if (e.result != null && e.result != '') { 
                    if (e.result.indexOf('Error') > -1 || e.result.indexOf('Confirmation') > -1) {
                        if (e.result.indexOf('Confirmation') > -1)
                            alert(e.result.substring(14));
                        else
                            alert(e.result); 
                        return; 
                    } 
                    window.location = e.result;
                }
            } ";

        }
        private DataTable GetRODataTable()
        {
            DataTable dt = Session["US_ROTable"] as DataTable;
            if (dt == null)
            {
                dt = ECWebClass.GetROTableData();
                dt.PrimaryKey = new DataColumn[] { dt.Columns["Code"] };
                Session["US_ROTable"] = dt;
            }
            return dt;
        }
        private void LoadROData()
        {
            cmbRO.DataSourceID = string.Empty;
            cmbRO.TextField = "Name";
            cmbRO.ValueField = "Code";
            cmbRO.DataSource = GetRODataTable();
            cmbRO.DataBind();
        }
        private void AssignUserData()
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            DevExpress.Web.MenuItem btnDeleteUser = menAction.Items.FindByName("btnDeleteUser");
            DataTable dtLogin = GetLoginDataTable();
            DataRow drLogin;
            bool foundLogin = dtLogin != null && dtLogin.Rows.Count > 0;
            if (foundLogin)
            {
                drLogin = dtLogin.Rows[0];
                if (drLogin["LoginId"] != DBNull.Value && drLogin["LoginId"].ToString() != string.Empty)
                    edtUserName.Value = drLogin["LoginId"];
                /*
                if (drLogin["Password"] != DBNull.Value && drLogin["Password"].ToString() != string.Empty)
                {
                    //edtPassword.Value = ECSecurityClass.GetPValueDecripted(drLogin["Password"].ToString());
                    edtPassword.Value = drLogin["Password"].ToString();
                    edtPasswordRepeat.Value = edtPassword.Value;
                }
                 */
                edtPassword.Value = null;
                edtPasswordRepeat.Value = null;
                if (drLogin["PasswordExpires"] != DBNull.Value && drLogin["PasswordExpires"].ToString() != string.Empty)
                    edtPasswordExpiry.Value = drLogin["PasswordExpires"];
                else
                    edtPasswordExpiry.Value = null;
                if (!(drLogin["WebPageTheme"] is DBNull) && drLogin["WebPageTheme"].ToString() != string.Empty)
                    cmbThemes.Value = drLogin["WebPageTheme"];
                else
                    cmbThemes.Value = "Glass";
                if (drLogin["FirstName"] != DBNull.Value && drLogin["FirstName"].ToString() != string.Empty)
                    edtFName.Value = drLogin["FirstName"];
                if (drLogin["FamilyName"] != DBNull.Value && drLogin["FamilyName"].ToString() != string.Empty)
                    edtLName.Value = drLogin["FamilyName"];
                if (drLogin["Phone"] != DBNull.Value && drLogin["Phone"].ToString() != string.Empty)
                    edtPhone.Value = drLogin["Phone"];
                if (drLogin["Email"] != DBNull.Value && drLogin["Email"].ToString() != string.Empty)
                    edtEmail.Value = drLogin["Email"];
                if (drLogin["CDID"] != DBNull.Value && drLogin["CDID"].ToString() != string.Empty)
                    cmbRO.Value = drLogin["CDID"];
                else
                    cmbRO.Value = "[None]";                

                if (drLogin["Active"] != DBNull.Value && drLogin["Active"].ToString() != string.Empty)
                    cbActive.Checked = Convert.ToChar(drLogin["Active"]) == 'Y';
                else
                    cbActive.Checked = false;

                if (btnDeleteUser != null)
                    btnDeleteUser.ClientVisible = loginUser != null && loginUser.SpecialUser;
                /* */
                if (loginUser != null && !loginUser.SpecialUser)
                {
                    if (string.Compare(loginUser.UserName, string.Format("{0}", drLogin["LoginId"]), true) != 0)
                    {
                        ECSecurityClass.setReadOnlyFor(edtPassword, true);
                        ECSecurityClass.setReadOnlyFor(edtPasswordRepeat, true);
                        ECSecurityClass.setReadOnlyFor(edtOldPassword, true);
                    }
                }   
                
            }
            else
            {
                drLogin = dtLogin.NewRow();
                drLogin["ID"] = -1;
                drLogin["Active"] = 'N';
                string appUITheme = ConfigurationManager.AppSettings["UITheme"];
                if (!string.IsNullOrEmpty(appUITheme))
                    drLogin["WebPageTheme"] = appUITheme;
                else
                    drLogin["WebPageTheme"] = "Glass";
                dtLogin.Rows.Add(drLogin);

                cmbThemes.Value = drLogin["WebPageTheme"];
                cbActive.Checked = true;

                if (btnDeleteUser != null)
                    btnDeleteUser.ClientVisible = false;
                ECSecurityClass.setReadOnlyFor(edtOldPassword, true);
            }
            ECSecurityClass.setReadOnlyFor(edtUserName, foundLogin);
            edtUserName.ClientEnabled = !foundLogin;
        }

        private void AssignUserDataAdv()
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
                return;
            DevExpress.Web.MenuItem btnDeleteUser = menAction.Items.FindByName("btnDeleteUser");
            DataTable dtLogin = GetLoginDataTable();
            DataRow drLogin;
            bool foundLogin = dtLogin != null && dtLogin.Rows.Count > 0;
            if (foundLogin)
            {
                drLogin = dtLogin.Rows[0];
                if (drLogin["LoginId"] != DBNull.Value && drLogin["LoginId"].ToString() != string.Empty)
                    //edtUserName.Value = AntiXssEncoder.HtmlEncode(drLogin["LoginId"].ToString(), true);
                    edtUserName.Value = drLogin["LoginId"];
                /*
                if (drLogin["Password"] != DBNull.Value && drLogin["Password"].ToString() != string.Empty)
                {
                    //edtPassword.Value = ECSecurityClass.GetPValueDecripted(drLogin["Password"].ToString());
                    edtPassword.Value = drLogin["Password"].ToString();
                    edtPasswordRepeat.Value = edtPassword.Value;
                }
                 */
                edtPassword.Value = null;
                edtPasswordRepeat.Value = null;
                if (drLogin["PasswordExpires"] != DBNull.Value && drLogin["PasswordExpires"].ToString() != string.Empty)
                    edtPasswordExpiry.Value = drLogin["PasswordExpires"];
                else
                    edtPasswordExpiry.Value = null;
                if (!(drLogin["WebPageTheme"] is DBNull) && drLogin["WebPageTheme"].ToString() != string.Empty)
                    cmbThemes.Value = drLogin["WebPageTheme"];
                else
                    cmbThemes.Value = "Glass";
                if (drLogin["FirstName"] != DBNull.Value && drLogin["FirstName"].ToString() != string.Empty)
                    edtFName.Value = drLogin["FirstName"];
                if (drLogin["FamilyName"] != DBNull.Value && drLogin["FamilyName"].ToString() != string.Empty)
                    edtLName.Value = drLogin["FamilyName"];
                if (drLogin["Phone"] != DBNull.Value && drLogin["Phone"].ToString() != string.Empty)
                    edtPhone.Value = drLogin["Phone"];
                if (drLogin["Email"] != DBNull.Value && drLogin["Email"].ToString() != string.Empty)
                    edtEmail.Value = drLogin["Email"];
                if (drLogin["CDID"] != DBNull.Value && drLogin["CDID"].ToString() != string.Empty)
                    cmbRO.Value = drLogin["CDID"];
                else
                    cmbRO.Value = "[None]";

                if (drLogin["Active"] != DBNull.Value && drLogin["Active"].ToString() != string.Empty)
                    cbActive.Checked = Convert.ToChar(drLogin["Active"]) == 'Y';
                else
                    cbActive.Checked = false;

                if (btnDeleteUser != null)
                    btnDeleteUser.ClientVisible = loginUser != null && loginUser.SpecialUser;
                /* */
                if (loginUser != null && !loginUser.SpecialUser)
                {
                    if (string.Compare(loginUser.UserName, string.Format("{0}", drLogin["LoginId"]), true) != 0)
                    {
                        ECSecurityClass.setReadOnlyFor(edtPassword, true);
                        ECSecurityClass.setReadOnlyFor(edtPasswordRepeat, true);
                        ECSecurityClass.setReadOnlyFor(edtOldPassword, true);
                    }
                }

            }
            else
            {
                drLogin = dtLogin.NewRow();
                drLogin["ID"] = -1;
                drLogin["Active"] = 'N';
                string appUITheme = ConfigurationManager.AppSettings["UITheme"];
                if (!string.IsNullOrEmpty(appUITheme))
                    drLogin["WebPageTheme"] = appUITheme;
                else
                    drLogin["WebPageTheme"] = "Glass";
                dtLogin.Rows.Add(drLogin);

                cmbThemes.Value = drLogin["WebPageTheme"];
                cbActive.Checked = true;

                if (btnDeleteUser != null)
                    btnDeleteUser.ClientVisible = false;
                ECSecurityClass.setReadOnlyFor(edtOldPassword, true);
            }
            ECSecurityClass.setReadOnlyFor(edtUserName, foundLogin);
            edtUserName.ClientEnabled = !foundLogin;
        }

        private void SelectSecurityRole()
        {
            DataTable dtLogin = GetLoginDataTable();
            DataTable dtLoginRole = ECSecurityClass.GetSecurityLoginRoleDataTable(dtLogin.Rows[0]["ID"]);
            if (dtLoginRole != null && dtLoginRole.Rows.Count > 0)
            {
                DataTable dtRole = grdUserRole.DataSource as DataTable;
                DataRow[] drsLoginRole;
                if (dtRole != null && dtRole.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtRole.Rows)
                    {
                        drsLoginRole = dtLoginRole.Select("SecurityRole = " + dr["ID"]);
                        if (drsLoginRole != null && drsLoginRole.Length > 0)
                            grdUserRole.Selection.SelectRowByKey(dr["ID"]);
                    }
                }
            }
        }
        private DataTable GetSecurityRoleDataTable()
        {
            DataTable dtRole = Session["US_RoleTable"] as DataTable;
            if (dtRole == null)
            {
                dtRole = ECSecurityClass.GetSecurityRoleDataTable(null);
                Session["US_RoleTable"] = dtRole;
            }
            return dtRole;
        }
        private void LoadSecurityRoleDataTable()
        {
            DataTable dtRole = GetSecurityRoleDataTable();
            grdUserRole.DataSource = dtRole;
            grdUserRole.DataSourceID = String.Empty;
            grdUserRole.DataBind();
        }
        private void SetPasswordPolicy()
        {
            DataTable dtApp = ECSecurityClass.GetApplicationDataTable();
            if (dtApp != null && dtApp.Rows.Count > 0)
            {
                DataRow drApp = dtApp.Rows[0];
                int minPwdLen = 8;
                if (!(drApp["MinPasswordLength"] is DBNull) && drApp["MinPasswordLength"].ToString() != string.Empty)
                    minPwdLen = Convert.ToInt32(drApp["MinPasswordLength"]);
                int maxPwdLen = 14;
                if (!(drApp["MaxPasswordLength"] is DBNull) && drApp["MaxPasswordLength"].ToString() != string.Empty)
                    maxPwdLen = Convert.ToInt32(drApp["MaxPasswordLength"]);

                //if (minPwdLen > 6)
                {
                    string validExpr = "(?=.*[a-z])";
                    string errTxt = string.Format("Minimum {0}, maximum {1} characters must contains one lowercase character", minPwdLen, maxPwdLen);
                    if (!(drApp["MinPasswordUppercaseChars"] is DBNull) && drApp["MinPasswordUppercaseChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordUppercaseChars"]) > 0)
                    {
                        validExpr += "(?=.*[A-Z])";
                        errTxt += ", one uppercase character";
                    }
                    if (!(drApp["MinPasswordNumericChars"] is DBNull) && drApp["MinPasswordNumericChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordNumericChars"]) > 0)
                    {
                        validExpr += "(?=.*\\d)";
                        errTxt += ", one digit from 0-9";
                    }

                    if (!(drApp["MinPasswordSpecialChars"] is DBNull) && drApp["MinPasswordSpecialChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordSpecialChars"]) > 0)
                    {
                        validExpr += "(?=.*\\W)";
                        errTxt += ", one special character";
                    }
                    validExpr += String.Format(".{{{0},{1}}}", minPwdLen, maxPwdLen);

                    edtPassword.ValidationSettings.RegularExpression.ErrorText = errTxt;
                    edtPassword.ValidationSettings.RegularExpression.ValidationExpression = validExpr;
                    //edtPassword.ValidationSettings.ValidateOnLeave = false;

                    edtPasswordRepeat.ValidationSettings.RegularExpression.ErrorText = errTxt;
                    edtPasswordRepeat.ValidationSettings.RegularExpression.ValidationExpression = validExpr;
                    //edtPasswordRepeat.ValidationSettings.ValidateOnLeave = false;
                }
            }
        }
        
        private object RetrieveloginIdFromQS()
        {
            object loginId = "dummy";

            string[] qs = ECSecurityClass.GetQSDecripted(Request.QueryString["qs"]).Split('=');

            if (qs.Length == 2)
                loginId = qs[1];

            return loginId;
        }
        private DataTable GetLoginDataTable()
        {
            DataTable dtLogin = Session["US_LoginTable"] as DataTable;
            if (dtLogin == null)
            {
                dtLogin = ECSecurityClass.GetLoginDataTableById(RetrieveloginIdFromQS());
                Session["US_LoginTable"] = dtLogin;
            }
            return dtLogin;
        }
        protected void grdUserRole_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            e.Properties["cpUserRoleVisibleRowCount"] = grid.VisibleRowCount;
        }
        protected void cbAll_Init(object sender, EventArgs e)
        {
            ASPxCheckBox chk = sender as ASPxCheckBox;
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            chk.Checked = grid.VisibleRowCount > 0 && grid.VisibleRowCount == grid.Selection.Count;
            if (chk.ClientSideEvents.CheckedChanged == string.Empty)
                chk.ClientSideEvents.CheckedChanged = "function(s, e) { " +
                    "if (s.GetChecked()) " + grid.ClientInstanceName + ".SelectRows(); else " +
                    grid.ClientInstanceName + ".UnselectRows(); } ";
        }
        protected void edtPassword_PreRender(object sender, EventArgs e)
        {
            ASPxTextBox edit = sender as ASPxTextBox;
            edit.ClientSideEvents.Init = String.Format("function(s, e) {{s.SetText('{0}');}}", edit.Text);
        }

        protected void cbLoginCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            if (e.Parameter != null && e.Parameter != "")
            {
                if (e.Parameter == "smsupport")
                {
                    e.Result = "Unavailable";
                    return;
                }
                if (!ECSecurityClass.IsValidEmailAddress(e.Parameter.Trim().ToLower()))
                {
                    e.Result = "Not a valid Email address";
                    return;
                }
                DataTable dtLogin = ECSecurityClass.GetLoginDataTableById(e.Parameter);
                if (dtLogin != null && dtLogin.Rows.Count > 0)
                    e.Result = "Unavailable";
            }
        }

        private void UpdateLoginUserData()
        {
            DataTable dtLogin = GetLoginDataTable();
            if (dtLogin != null && dtLogin.Rows.Count > 0)
            {
                DataRow drLogin = dtLogin.Rows[0];

                if (edtUserName.Value != null && edtUserName.Value.ToString().Trim() != string.Empty)
                    drLogin["LoginId"] = edtUserName.Value;

                Session["US_PWDChanged"] = false;
                if (edtPassword.Value != null && edtPassword.Value.ToString().TrimEnd() != string.Empty)
                {
                    /*
                    if (drLogin["Password"] is DBNull || drLogin["Password"].ToString() == string.Empty)
                    {
                        drLogin["Password"] = ECSecurityClass.GetPValueEncripted(edtPassword.Value.ToString());
                        Session["US_PWDChanged"] = true;
                    }
                    else // only updated
                    {
                        string currPwd = ECSecurityClass.GetPValueDecripted(drLogin["Password"].ToString());
                        try
                        {
                            string nowPwd = ECSecurityClass.GetPValueDecripted(edtPassword.Value.ToString());
                            if (string.Compare(currPwd, nowPwd, false) != 0)
                            {
                                drLogin["Password"] = ECSecurityClass.GetPValueEncripted(edtPassword.Value.ToString());
                                Session["US_PWDChanged"] = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            drLogin["Password"] = ECSecurityClass.GetPValueEncripted(edtPassword.Value.ToString());
                            Session["US_PWDChanged"] = true;
                        }
                    }
                     */
                    drLogin["Password"] = ECSecurityClass.GetPValueEncripted(edtPassword.Value.ToString().TrimEnd());
                    Session["US_PWDChanged"] = true;
                }

                if (edtPasswordExpiry.Value != null && edtPasswordExpiry.Value.ToString() != string.Empty)
                {
                    if (!edtPasswordExpiry.ClientVisible)
                    {
                        DateTime dtPwdExpiryNew = Convert.ToDateTime(edtPasswordExpiry.Value);
                        TimeSpan ts = dtPwdExpiryNew - DateTime.Now;
                        if (ts.Days < 4)
                            dtPwdExpiryNew = DateTime.Today.AddMonths(2);
                        drLogin["PasswordExpires"] = dtPwdExpiryNew;
                    }
                    else
                        drLogin["PasswordExpires"] = edtPasswordExpiry.Value;
                }
                else
                    drLogin["PasswordExpires"] = DBNull.Value;

                if (cmbThemes.Text != string.Empty)
                    drLogin["WebPageTheme"] = cmbThemes.Text;
                else
                    drLogin["WebPageTheme"] = DBNull.Value;

                if (edtFName.Value != null && edtFName.Value.ToString() != string.Empty)
                    drLogin["FirstName"] = edtFName.Value;
                else
                    drLogin["FirstName"] = DBNull.Value;
                if (edtLName.Value != null && edtLName.Value.ToString() != string.Empty)
                    drLogin["FamilyName"] = edtLName.Value;
                else
                    drLogin["FamilyName"] = DBNull.Value;
                if (edtPhone.Value != null && edtPhone.Value.ToString() != string.Empty)
                    drLogin["Phone"] = edtPhone.Value;
                else
                    drLogin["Phone"] = DBNull.Value;
                if (edtEmail.Value != null && edtEmail.Value.ToString() != string.Empty)
                    drLogin["Email"] = edtEmail.Value;
                else
                    drLogin["Email"] = DBNull.Value;

                if (cmbRO.Text != string.Empty && !cmbRO.Text.StartsWith("[None]"))
                    drLogin["CDID"] = cmbRO.Value;
                else
                    drLogin["CDID"] = DBNull.Value;

                if (cbActive.Checked)
                    drLogin["Active"] = 'Y';
                else
                    drLogin["Active"] = 'N';
               
                if (Convert.ToInt32(drLogin["ID"]) < 0)
                {
                    drLogin["Created"] = DateTime.Now;
                    drLogin["CreatedBy"] = (Session["EC_User"] as LoginUser).UserName;
                }
                else
                {
                    drLogin["Modified"] = DateTime.Now;
                    drLogin["ModifiedBy"] = (Session["EC_User"] as LoginUser).UserName;
                }
            }
        }

        private bool UpdateLoginUserData(ref string procMsg)
        {
            bool result = false;

            DataTable dtLogin = GetLoginDataTable();
            result = dtLogin != null && dtLogin.Rows.Count > 0;
            if (!result)
            {
                procMsg = "Record not found";
                return false;
            }
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            
            DataRow drLogin = dtLogin.Rows[0];

            int id = Convert.ToInt32(drLogin["ID"]);
            bool newLogin = id == -1;
            // only admin can create new user
            if (newLogin && !loginUser.IsAdmin)
            {
                procMsg = "You're not authorized";
                return false;
            }
            // the username on the page
            string tgtLogin = string.Empty;
            if (edtUserName.Value != null && edtUserName.Value.ToString().Trim() != string.Empty)
                tgtLogin = edtUserName.Value.ToString().Trim();

            result = !string.IsNullOrEmpty(tgtLogin);
            if (!result)
            {
                procMsg = "Login name is missing";
                return result;
            }
            if (!ECSecurityClass.IsValidEmailAddress(tgtLogin))
            {
                procMsg = "Invalid email address";
                return false;
            }
            // non-admin user can only see its own record page 
            if (!loginUser.IsAdmin && string.Compare(loginUser.UserName, tgtLogin, true) != 0)
            {
                procMsg = "You're not authorized";
                return false;
            }
            // for existing user, its name is read-only
            if (!newLogin && !(drLogin["LoginId"] is DBNull) && string.Compare(drLogin["LoginId"].ToString(), tgtLogin, true) != 0)
            {
                procMsg = "Invalid record modification";
                return false;
            }
            // duplicated user record check
            DataTable dtCheckLogin = ECSecurityClass.GetLoginDataTableById(drLogin["ID"], tgtLogin);
            if (dtCheckLogin != null && dtCheckLogin.Rows.Count > 0)
            {
                procMsg = "Duplicate Record Found";
                return false;
            }
             
            drLogin["LoginId"] = tgtLogin;

            Session["US_PWDChanged"] = false;
            // password validation
            if (edtPassword.Value != null && edtPassword.Value.ToString().TrimEnd() != string.Empty)
            {
                // for existing user record, only user self can change password
                if (!newLogin && !loginUser.SpecialUser && string.Compare(loginUser.UserName, tgtLogin, true) != 0)
                {
                    procMsg = "Invalid record modification";
                    return false;
                }

                string pwd = edtPassword.Value.ToString().TrimEnd();
                string pwdRep;
                if (edtPasswordRepeat.Value != null && edtPasswordRepeat.Value.ToString().TrimEnd() != string.Empty)
                    pwdRep = edtPasswordRepeat.Value.ToString().TrimEnd();
                else
                    pwdRep = string.Empty;

                if (string.Compare(pwd, pwdRep, false) != 0)
                {
                    procMsg = "Password does not match";
                    return false;
                }
                // validate password policy
                DataTable dtApp = ECSecurityClass.GetApplicationDataTable();
                if (dtApp != null && dtApp.Rows.Count > 0)
                {
                    DataRow drApp = dtApp.Rows[0];
                    int minPwdLen = 8;
                    if (!(drApp["MinPasswordLength"] is DBNull) && drApp["MinPasswordLength"].ToString() != string.Empty)
                        minPwdLen = Convert.ToInt32(drApp["MinPasswordLength"]);
                    int maxPwdLen = 14;
                    if (!(drApp["MaxPasswordLength"] is DBNull) && drApp["MaxPasswordLength"].ToString() != string.Empty)
                        maxPwdLen = Convert.ToInt32(drApp["MaxPasswordLength"]);

                    string validExpr = "(?=.*[a-z])";
                    string errTxt = string.Format("Minimum {0}, maximum {1} characters must contains one lowercase character", minPwdLen, maxPwdLen);
                    if (!(drApp["MinPasswordUppercaseChars"] is DBNull) && drApp["MinPasswordUppercaseChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordUppercaseChars"]) > 0)
                    {
                        validExpr += "(?=.*[A-Z])";
                        errTxt += ", one uppercase character";
                    }
                    if (!(drApp["MinPasswordNumericChars"] is DBNull) && drApp["MinPasswordNumericChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordNumericChars"]) > 0)
                    {
                        validExpr += "(?=.*\\d)";
                        errTxt += ", one digit from 0-9";
                    }

                    if (!(drApp["MinPasswordSpecialChars"] is DBNull) && drApp["MinPasswordSpecialChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordSpecialChars"]) > 0)
                    {
                        validExpr += "(?=.*\\W)";
                        errTxt += ", one special character";
                    }
                    //validExpr += String.Format(".{{{0},{1}}}", minPwdLen, maxPwdLen);

                    if (pwd.Length < minPwdLen || pwd.Length > maxPwdLen)
                    {
                        procMsg = string.Format("Password validation failed, length must be between {0} and {1} characters", minPwdLen, maxPwdLen);
                        return false;
                    }

                    Regex  regEx = new Regex(validExpr);
                    Match match = regEx.Match(pwd);
                    if (!match.Success)
                    {
                        procMsg = string.Format("Password validation failed, {0}", errTxt);
                        return false;
                    }
                    if (!newLogin)
                    {
                        if (edtOldPassword.Text == string.Empty)
                        {
                            procMsg = "Please provide old password";
                            return false;
                        }
                        string oldPwd = ECSecurityClass.GetPValueDecripted(string.Format("{0}", drLogin["Password"]));
                        if (string.Compare(edtOldPassword.Text.Trim(), oldPwd, false) != 0)
                        {
                            procMsg = "Old password provided does not match";
                            return false;
                        }
                    }
                }
                // end of 
                drLogin["Password"] = ECSecurityClass.GetPValueEncripted(pwd);
                Session["US_PWDChanged"] = true;
            }

            if (edtPasswordExpiry.Value != null && edtPasswordExpiry.Value.ToString() != string.Empty)
            {
                if (!edtPasswordExpiry.ClientVisible)
                {
                    DateTime dtPwdExpiryNew = Convert.ToDateTime(edtPasswordExpiry.Value);
                    TimeSpan ts = dtPwdExpiryNew - DateTime.Now;
                    if (ts.Days < 4)
                        dtPwdExpiryNew = DateTime.Today.AddMonths(2);
                    drLogin["PasswordExpires"] = dtPwdExpiryNew;
                }
                else
                    drLogin["PasswordExpires"] = edtPasswordExpiry.Value;
            }
            else
                drLogin["PasswordExpires"] = DBNull.Value;

            if (cmbThemes.Text != string.Empty)
                drLogin["WebPageTheme"] = cmbThemes.Text;
            else
                drLogin["WebPageTheme"] = DBNull.Value;
            
            if (edtFName.Value != null && edtFName.Value.ToString() != string.Empty)
            {
                /*  
                string aa = "<script >alert(1)</script>";
                string bb = System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(aa, false);
                string cc = HttpUtility.HtmlDecode(bb);
                */
                
                drLogin["FirstName"] = edtFName.Value;
            }
            else
                drLogin["FirstName"] = DBNull.Value;
            if (edtLName.Value != null && edtLName.Value.ToString() != string.Empty)
                drLogin["FamilyName"] = edtLName.Value;
            else
                drLogin["FamilyName"] = DBNull.Value;
            if (edtPhone.Value != null && edtPhone.Value.ToString() != string.Empty)
                drLogin["Phone"] = edtPhone.Value;
            else
                drLogin["Phone"] = DBNull.Value;
            if (edtEmail.Value != null && edtEmail.Value.ToString() != string.Empty)
                drLogin["Email"] = edtEmail.Value;
            else
                drLogin["Email"] = DBNull.Value;

            if (cmbRO.Text != string.Empty && !cmbRO.Text.StartsWith("[None]"))
                drLogin["CDID"] = cmbRO.Value;
            else
                drLogin["CDID"] = DBNull.Value;

            if (cbActive.Checked)
                drLogin["Active"] = 'Y';
            else
                drLogin["Active"] = 'N';

            if (Convert.ToInt32(drLogin["ID"]) < 0)
            {
                drLogin["Created"] = DateTime.Now;
                drLogin["CreatedBy"] = loginUser.UserName;
            }
            else
            {
                drLogin["Modified"] = DateTime.Now;
                drLogin["ModifiedBy"] = loginUser.UserName;
            }
            result = true;
            return result;
        }
        private bool UpdateLoginUserDataAdv(ref string procMsg)
        {
            bool result = false;

            DataTable dtLogin = GetLoginDataTable();
            result = dtLogin != null && dtLogin.Rows.Count > 0;
            if (!result)
            {
                procMsg = "Record not found";
                return false;
            }
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null)
            {
                procMsg = "You're not authorized";
                return false;
            }

            DataRow drLogin = dtLogin.Rows[0];

            int id = Convert.ToInt32(drLogin["ID"]);
            bool newLogin = id == -1;
            // only admin can create new user
            if (newLogin && !loginUser.IsAdmin)
            {
                procMsg = "You're not authorized";
                return false;
            }
            // the username on the page
            string tgtLogin = string.Empty;
            if (edtUserName.Value != null && edtUserName.Value.ToString().Trim() != string.Empty)
                tgtLogin = edtUserName.Value.ToString().Trim();

            result = !string.IsNullOrEmpty(tgtLogin);
            if (!result)
            {
                procMsg = "Login name is missing";
                return result;
            }
            if (!ECSecurityClass.IsValidEmailAddress(tgtLogin))
            {
                procMsg = "Invalid email address";
                return false;
            }
            // non-admin user can only see its own record page 
            if (!loginUser.IsAdmin && string.Compare(loginUser.UserName, tgtLogin, true) != 0)
            {
                procMsg = "You're not authorized";
                return false;
            }
            // for existing user, its name is read-only
            if (!newLogin && !(drLogin["LoginId"] is DBNull) && string.Compare(drLogin["LoginId"].ToString(), tgtLogin, true) != 0)
            {
                procMsg = "Invalid record modification";
                return false;
            }
            // duplicated user record check
            DataTable dtCheckLogin = ECSecurityClass.GetLoginDataTableById(drLogin["ID"], tgtLogin);
            if (dtCheckLogin != null && dtCheckLogin.Rows.Count > 0)
            {
                procMsg = "Duplicate Record Found";
                return false;
            }

            drLogin["LoginId"] = tgtLogin;

            Session["US_PWDChanged"] = false;
            // password validation
            if (edtPassword.Value != null && edtPassword.Value.ToString().TrimEnd() != string.Empty)
            {
                // for existing user record, only user self can change password
                if (!newLogin && !loginUser.SpecialUser && string.Compare(loginUser.UserName, tgtLogin, true) != 0)
                {
                    procMsg = "Invalid record modification";
                    return false;
                }

                string pwd = edtPassword.Value.ToString().TrimEnd();
                string pwdRep;
                if (edtPasswordRepeat.Value != null && edtPasswordRepeat.Value.ToString().TrimEnd() != string.Empty)
                    pwdRep = edtPasswordRepeat.Value.ToString().TrimEnd();
                else
                    pwdRep = string.Empty;

                if (string.Compare(pwd, pwdRep, false) != 0)
                {
                    procMsg = "Password does not match";
                    return false;
                }
                // validate password policy
                DataTable dtApp = ECSecurityClass.GetApplicationDataTable();
                if (dtApp != null && dtApp.Rows.Count > 0)
                {
                    DataRow drApp = dtApp.Rows[0];
                    int minPwdLen = 8;
                    if (!(drApp["MinPasswordLength"] is DBNull) && drApp["MinPasswordLength"].ToString() != string.Empty)
                        minPwdLen = Convert.ToInt32(drApp["MinPasswordLength"]);
                    int maxPwdLen = 14;
                    if (!(drApp["MaxPasswordLength"] is DBNull) && drApp["MaxPasswordLength"].ToString() != string.Empty)
                        maxPwdLen = Convert.ToInt32(drApp["MaxPasswordLength"]);

                    string validExpr = "(?=.*[a-z])";
                    string errTxt = string.Format("Minimum {0}, maximum {1} characters must contains one lowercase character", minPwdLen, maxPwdLen);
                    if (!(drApp["MinPasswordUppercaseChars"] is DBNull) && drApp["MinPasswordUppercaseChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordUppercaseChars"]) > 0)
                    {
                        validExpr += "(?=.*[A-Z])";
                        errTxt += ", one uppercase character";
                    }
                    if (!(drApp["MinPasswordNumericChars"] is DBNull) && drApp["MinPasswordNumericChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordNumericChars"]) > 0)
                    {
                        validExpr += "(?=.*\\d)";
                        errTxt += ", one digit from 0-9";
                    }

                    if (!(drApp["MinPasswordSpecialChars"] is DBNull) && drApp["MinPasswordSpecialChars"].ToString() != string.Empty &&
                        Convert.ToInt32(drApp["MinPasswordSpecialChars"]) > 0)
                    {
                        validExpr += "(?=.*\\W)";
                        errTxt += ", one special character";
                    }
                    //validExpr += String.Format(".{{{0},{1}}}", minPwdLen, maxPwdLen);

                    if (pwd.Length < minPwdLen || pwd.Length > maxPwdLen)
                    {
                        procMsg = string.Format("Password validation failed, length must be between {0} and {1} characters", minPwdLen, maxPwdLen);
                        return false;
                    }

                    Regex regEx = new Regex(validExpr);
                    Match match = regEx.Match(pwd);
                    if (!match.Success)
                    {
                        procMsg = string.Format("Password validation failed, {0}", errTxt);
                        return false;
                    }
                    if (!newLogin)
                    {
                        if (edtOldPassword.Text == string.Empty)
                        {
                            procMsg = "Please provide old password";
                            return false;
                        }
                        string oldPwd = ECSecurityClass.GetPValueDecripted(string.Format("{0}", drLogin["Password"]));
                        if (string.Compare(edtOldPassword.Text.Trim(), oldPwd, false) != 0)
                        {
                            procMsg = "Old password provided does not match";
                            return false;
                        }
                    }
                }
                // end of 
                drLogin["Password"] = ECSecurityClass.GetPValueEncripted(pwd);
                Session["US_PWDChanged"] = true;
            }

            if (edtPasswordExpiry.Value != null && edtPasswordExpiry.Value.ToString() != string.Empty)
            {
                if (!edtPasswordExpiry.ClientVisible)
                {
                    DateTime dtPwdExpiryNew = Convert.ToDateTime(edtPasswordExpiry.Value);
                    TimeSpan ts = dtPwdExpiryNew - DateTime.Now;
                    if (ts.Days < 4)
                        dtPwdExpiryNew = DateTime.Today.AddMonths(2);
                    drLogin["PasswordExpires"] = dtPwdExpiryNew;
                }
                else
                    drLogin["PasswordExpires"] = edtPasswordExpiry.Value;
            }
            else
                drLogin["PasswordExpires"] = DBNull.Value;

            if (cmbThemes.Text != string.Empty)
                drLogin["WebPageTheme"] = cmbThemes.Text;
            else
                drLogin["WebPageTheme"] = DBNull.Value;

            if (edtFName.Value != null && edtFName.Value.ToString() != string.Empty)
            {
                /*  
                string aa = "<script >alert(1)</script>";
                string bb = System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(aa, false);
                string cc = HttpUtility.HtmlDecode(bb);
                */

                drLogin["FirstName"] = edtFName.Value;// HttpUtility.HtmlDecode(edtFName.Value.ToString());
            }
            else
                drLogin["FirstName"] = DBNull.Value;
            if (edtLName.Value != null && edtLName.Value.ToString() != string.Empty)
                drLogin["FamilyName"] = edtLName.Value;//HttpUtility.HtmlDecode(edtLName.Value.ToString());
            else
                drLogin["FamilyName"] = DBNull.Value;
            if (edtPhone.Value != null && edtPhone.Value.ToString() != string.Empty)
                drLogin["Phone"] = edtPhone.Value;//HttpUtility.HtmlDecode(edtPhone.Value.ToString());//
            else
                drLogin["Phone"] = DBNull.Value;
            if (edtEmail.Value != null && edtEmail.Value.ToString() != string.Empty)
                drLogin["Email"] = edtEmail.Value;//HttpUtility.HtmlDecode(edtEmail.Value.ToString());//
            else
                drLogin["Email"] = DBNull.Value;

            if (cmbRO.Text != string.Empty && !cmbRO.Text.StartsWith("[None]"))
                drLogin["CDID"] = cmbRO.Value;
            else
                drLogin["CDID"] = DBNull.Value;

            if (cbActive.Checked)
                drLogin["Active"] = 'Y';
            else
                drLogin["Active"] = 'N';

            if (Convert.ToInt32(drLogin["ID"]) < 0)
            {
                drLogin["Created"] = DateTime.Now;
                drLogin["CreatedBy"] = loginUser.UserName;
            }
            else
            {
                drLogin["Modified"] = DateTime.Now;
                drLogin["ModifiedBy"] = loginUser.UserName;
            }
            result = true;
            return result;
        }
        protected void cbLogin_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                bool success = false;
                string errorMsg = string.Empty;
                string roleList = string.Empty;
                e.Result = "ConsignmentCatalogue.aspx";

                LoginUser loginUser = Session["EC_User"] as LoginUser;
                if (loginUser == null)
                {
                    e.Result = "Default.aspx?Logout=1";
                    return;
                }

                if (e.Parameter == "btnDeleteUser")
                    success = ECSecurityClass.UpdateLoginUser(this.Page, ref errorMsg, GetLoginDataTable(), string.Empty, true);
                else
                    if (e.Parameter.Contains("Save"))
                    {
                        //UpdateLoginUserData();
                        success = UpdateLoginUserDataAdv(ref errorMsg);
                        if (!success)
                        {
                            e.Result = "Error: " + errorMsg;
                            return;
                        }
                        DataTable dtlogin = GetLoginDataTable();
                        // make sure user record sent has encrypted password 
                        success = dtlogin != null && !(dtlogin.Rows[0]["Password"] is DBNull) && dtlogin.Rows[0]["Password"].ToString() != string.Empty;
                        if (!success)
                            errorMsg = "Password is required";
                        else
                        {
                            // make sure only admin can modify user role
                            if (loginUser.IsAdmin)
                            {
                                DataTable dtRole = grdUserRole.DataSource as DataTable;
                                if (dtRole != null && dtRole.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtRole.Rows)
                                    {
                                        if (grdUserRole.Selection.IsRowSelectedByKey(dr["ID"]))
                                            roleList += dr["ID"] + ",";
                                    }
                                    if (roleList.Length > 0)
                                        roleList = roleList.Remove(roleList.Length - 1);
                                }
                            }
                            success = ECSecurityClass.UpdateLoginUser(this.Page, ref errorMsg, dtlogin, roleList, false);
                            if (e.Parameter == "btnSave")
                            {
                                e.Result = "UserSetup.aspx?qs=" + ECSecurityClass.GetQSEncripted("loginId=" + edtUserName.Value);
                            }
                        }
                    }
                if (!success)
                    e.Result = "Error: " + errorMsg;
                else
                {
                    if (Request.Cookies["EC_Theme"] != null)
                    {
                        HttpCookie c = new HttpCookie("EC_Theme");
                        c.Expires = DateTime.Now.AddDays(-1d);
                        Response.Cookies.Add(c);
                    }
                    // if user modifies its record then
                    if (loginUser.UserName == edtUserName.Value.ToString())
                    {
                        if (cmbThemes.Value != null && loginUser.PageTheme != cmbThemes.Value.ToString())
                            loginUser.PageTheme = cmbThemes.Value.ToString();
                        if (cmbRO.Value != null && loginUser.CDID != cmbRO.Value.ToString())
                        {
                            DataTable dtRO = GetRODataTable();
                            DataRow drRO = dtRO.Rows.Find(cmbRO.Value);
                            if (drRO != null)
                            {
                                loginUser.CDID = string.Format("{0}",drRO["Code"]);
                                loginUser.ReturningOffice = string.Format("{0}", drRO["Name"]);
                            }
                        }
                        if (Session["US_PWDChanged"] != null && Convert.ToBoolean(Session["US_PWDChanged"]))
                        {
                            // clear session token and form authentication cookie
                            foreach (var cookey in Request.Cookies.AllKeys)
                            {
                                if (cookey == FormsAuthentication.FormsCookieName || cookey.ToLower() == "asp.net_sessionid")
                                {
                                    var reqCookie = Request.Cookies[cookey];

                                    if (reqCookie != null)
                                    {
                                        HttpCookie respCookie = new HttpCookie(reqCookie.Name);
                                        respCookie.Expires = DateTime.Now.AddMinutes(-20);
                                        Response.Cookies.Add(respCookie);
                                    }
                                }
                            }
                            // mark session id saved in database invalid
                            ECSecurityClass.UpdateSessionTokenInvalid(loginUser);
                            // force logout and re-authenticate user
                            e.Result = "Default.aspx?Logout=1";
                            return;
                            //DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                        }
                    }

                    Session.Remove("US_LoginTable");
                    DataTable dtlogin = GetLoginDataTable();
                    if (dtlogin == null || dtlogin.Rows.Count == 0 || Convert.ToChar(dtlogin.Rows[0]["Active"]) != 'Y')
                    {
                        // get rid of this user
                        Hashtable sessions = (Hashtable)Application["WEB_SESSIONS_OBJECT"];
                        if (sessions == null)
                            sessions = new Hashtable();

                        //getting the pointer to the Session of the current logged in user
                        HttpSessionState existingUserSession = (HttpSessionState)sessions[edtUserName.Value.ToString().ToLower()];
                        //logout current logged in user
                        if (existingUserSession != null)
                            existingUserSession["EC_User"] = null;
                    }
                }
            }
        }
                
    }
}