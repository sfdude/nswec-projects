﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using DevExpress.Web;
using System.Configuration;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class ConsignmentCatalogue : System.Web.UI.Page
    {
        private readonly int defaultPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "ConsignmentCatalogue.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EC_User"] == null)
                return;

            if (!IsPostBack)
            {
                Session.Remove("CC_ConnoteTable");
                Session.Remove("CC_GrdConnoteLayoutTable");

                ppLoginInfo.ShowOnPageLoad = false;
                if (Session["LoginInfoMessage"] != null && Session["LoginInfoMessage"].ToString() != string.Empty)
                {
                    ASPxLabel lblLoginInfoMsg = ppLoginInfo.FindControl("lblLoginInfoMsg") as ASPxLabel;
                    if (lblLoginInfoMsg != null)
                        lblLoginInfoMsg.Value = Session["LoginInfoMessage"].ToString();
                    Session.Remove("LoginInfoMessage");
                    ppLoginInfo.ShowOnPageLoad = true;
                }

                hfHiddenInfo.Set("LayoutID", -1);
                hfHiddenInfo.Set("LayoutName", string.Empty);
                hfHiddenInfo.Set("PageSize", defaultPageSize);
                hfHiddenInfo.Set("DefaultLayout", 'N');

                AssignClientSideScripts();

                DevExpress.Web.MenuItem itmCheckedList = mGridViewExport.Items.FindByName("itmCheckedList");
                DevExpress.Web.MenuItem itmList = mGridViewExport.Items.FindByName("itmList");

                if (Session["CC_GrdConnoteFilterMode"] != null && string.Compare(Session["CC_GrdConnoteFilterMode"].ToString(), "CheckedList", true) == 0)
                {                    
                    foreach (GridViewColumn column in grdConnoteBarcode.Columns)
                    {
                        if (column is GridViewDataColumn)
                            (column as GridViewDataColumn).SettingsHeaderFilter.Mode = GridHeaderFilterMode.CheckedList;
                    }
                    if (itmCheckedList != null)
                        itmCheckedList.Checked = true;
                    if (itmList != null)
                        itmList.Checked = false;
                }
                else
                {
                    foreach (GridViewColumn column in grdConnoteBarcode.Columns)
                    {
                        if (column is GridViewDataColumn)
                            (column as GridViewDataColumn).SettingsHeaderFilter.Mode = GridHeaderFilterMode.List;
                    }
                    if (itmCheckedList != null)
                        itmCheckedList.Checked = false;
                    if (itmList != null)
                        itmList.Checked = true;
                }
                
                LoadConnoteData(true);
                hfHiddenInfo.Set("OriginalLayout", grdConnoteBarcode.SaveClientLayout());

                if (Session["CC_GrdConnoteFilterLayout"] != null && !string.IsNullOrEmpty(Session["CC_GrdConnoteFilterLayout"].ToString()))
                {
                    string filteredLayout = Session["CC_GrdConnoteFilterLayout"].ToString();
                    grdConnoteBarcode.LoadClientLayout(filteredLayout);
                }
                else
                    LoadGrdConnoteLayout(null, null);
                
                ASPxLabel lblGrdConnoteFilter = grdConnoteBarcode.FindTitleTemplateControl("lblGrdConnoteFilter") as ASPxLabel;
                if (lblGrdConnoteFilter != null)
                {
                    if (!string.IsNullOrEmpty(grdConnoteBarcode.FilterExpression))
                        lblGrdConnoteFilter.Text = string.Format("Current Filter: {0}", grdConnoteBarcode.FilterExpression);
                    else
                        lblGrdConnoteFilter.Text = string.Empty;
                }
                lblGrdConnoteFilter = grdConnoteBarcode.FindStatusBarTemplateControl("lblGrdConnoteFilter") as ASPxLabel;
                if (lblGrdConnoteFilter != null)
                {
                    if (!string.IsNullOrEmpty(grdConnoteBarcode.FilterExpression))
                        lblGrdConnoteFilter.Text = string.Format("Current Filter: {0}", grdConnoteBarcode.FilterExpression);
                    else
                        lblGrdConnoteFilter.Text = string.Empty;
                }

                pcConnote.ShowTabs = false;
                
                // styling                
                ECWebClass.SetMenuStyle(mGridViewExport);
                ECWebClass.SetGridViewStyle(grdConnoteBarcode);
            }
            else
                LoadConnoteData(false);
        }
        private void AssignClientSideScripts()
        {
            ASPxMenu mGridViewExport = pcConnote.FindControl("mGridViewExport") as ASPxMenu;
            if (mGridViewExport != null)
            {
                mGridViewExport.ClientSideEvents.ItemClick =
                    @"function(s, e) { 
                    var itmCustmisation = s.GetItemByName('itmCustomisation'); 
                    if (e.item.name=='itmShowFieldChooser') { 
                        var itmHideFieldChooser = itmCustmisation.GetItemByName('itmHideFieldChooser'); 
                        grdConnoteBarcode.ShowCustomizationWindow(); e.item.SetVisible(false); itmHideFieldChooser.SetVisible(true); 
                    } else 
                    if (e.item.name=='itmHideFieldChooser') { 
                        var itmShowFieldChooser = itmCustmisation.GetItemByName('itmShowFieldChooser'); 
                        grdConnoteBarcode.HideCustomizationWindow(); e.item.SetVisible(false); itmShowFieldChooser.SetVisible(true); 
                    }
                    e.processOnServer = false;
                    if (e.item.name == 'itmPrintLabel') {
                        if (cbPrintLabel.InCallback()) return; 
                        var keys = grdConnoteBarcode.GetSelectedKeysOnPage();
                        if (keys.length == 0) {
                            alert('No consignment barcode selected on this page'); return; 
                        }
                        e.item.SetText('Wait...'); e.item.SetEnabled(false);
                        cbPrintLabel.PerformCallback('Print,'+keys);
                    } else 
                    if (e.item.name == 'itmRefresh') {
                        grdConnoteBarcode.PerformCallback('Refresh');
                    } else 
                    if (e.item.name == 'itmClearFilter') {
                        grdConnoteBarcode.PerformCallback('ClearFilter');
                    } else 
                    if (e.item.name == 'itmCheckedList') {                       
                        cbConnote.PerformCallback('HeadFilterMode|CheckedList');
                    } else 
                    if (e.item.name == 'itmList') {                         
                        cbConnote.PerformCallback('HeadFilterMode|List');
                    } else
                    if (e.item.parent.name == 'itmExport' || e.item.parent.name == 'itmGridLayout') { 
                        if (e.item.name == 'itmGridLayoutPageSize') { 
                            var promptValue = prompt('Enter the number of rows to display per page', ''); 
                            if (promptValue != null && promptValue != '') { 
                                promptValue = parseInt(promptValue); 
                                if (isNaN(promptValue)) {
                                    alert('Please enter a valid number'); return; 
                                }
                                hfHiddenInfo.Set('PageSize', promptValue); 
                                e.processOnServer = true; 
                            } else return 
                        } else 
                        if (e.item.name == 'itmGridLayoutSaveAsNew') { 
                            var promptValue = prompt('Layout Name', ''); 
                            if (promptValue != null && promptValue != '') { 
                                hfHiddenInfo.Set('LayoutName', promptValue); 
                                e.processOnServer = true; 
                            } else 
                            return; 
                        } 
                        e.processOnServer = true; 
                    }
                } ";

                cbPrintLabel.ClientSideEvents.CallbackComplete =
                    @"function(s, e) {
                        var itmPrintLabel = mGridViewExport.GetItemByName('itmPrintLabel'); 
                        if (itmPrintLabel != null) { 
                            itmPrintLabel.SetText('Print Label'); itmPrintLabel.SetEnabled(true); 
                        }
                        grdConnoteBarcode.UnselectRows();
                        if (e.result != null && e.result != '') {
                            if (e.result.indexOf('Error') > -1 || e.result.indexOf('Confirmation') > -1) {
                                alert(e.result); 
                                return; 
                            }
                            var newWindow; var offSet = 20; var linkArray;
                            var winX = (document.all)?window.screenLeft:window.screenX;
                            var winY = (document.all)?window.screenTop:window.screenY;
                            winX += offSet; winY += offSet;
                            if (e.result.indexOf('|') > -1) {
                                var resultArray = e.result.split('|');                             
                                for (var i=0; i<resultArray.length; i++) {
                                    newWindow = window.open(resultArray[i], 'PrintLabel' + (i+1).toString(), 'width=450,height=850,resizable=yes');                                    
                                    newWindow.moveTo(winX,winY); 
                                    winX += offSet; winY += offSet;
                                }                           
                            } else {  
                                newWindow = window.open(e.result, 'PrintLabel', 'width=450,height=850,resizable=yes');
                                newWindow.moveTo(winX,winY);
                            }
                        }
                    }";
                
                DevExpress.Web.MenuItem itmHideFieldChooser = mGridViewExport.Items.FindByName("itmHideFieldChooser");
                if (itmHideFieldChooser != null)
                    itmHideFieldChooser.ClientVisible = false;
            }

            grdConnoteBarcode.ClientSideEvents.CustomButtonClick = @"function(s, e) { 
                var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                if (btnId == 'btnViewConnote') {
                    if (cbConnote.InCallback()) return;
                    cbConnote.PerformCallback('ConnoteView|' + id);
                } else
                if (btnId == 'btnPrintConnote') {
                    if (cbPrintLabel.InCallback()) return;
                    cbPrintLabel.PerformCallback('Print,' + id);
                }
            }";

            cbConnote.ClientSideEvents.CallbackComplete = @"function(s, e) { 
                if (e.result != null && e.result != '') {
                    window.location = e.result;
                }
            }";
        }
        protected void grdView_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            e.Properties["cpVisibleRowCount"] = grid.VisibleRowCount;
        }
        private void LoadConnoteData(bool expandAll)
        {
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            //if (loginUser == null)
            //    return;

            grdConnoteBarcode.DataSource = GetConnoteDataTable();
            grdConnoteBarcode.DataSourceID = String.Empty;
            grdConnoteBarcode.DataBind();

            GridViewDataTextColumn colEMO = grdConnoteBarcode.Columns["ElectionManagerOffice"] as GridViewDataTextColumn;
            if (colEMO != null)
                colEMO.Caption = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "Returning Office" : "Election Manager Office";
            GridViewDataTextColumn colDistrict = grdConnoteBarcode.Columns["District"] as GridViewDataTextColumn;
            if (colDistrict != null)
                colDistrict.Caption = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "LGA" : "District";
            GridViewDataTextColumn colContestArea = grdConnoteBarcode.Columns["ContestArea"] as GridViewDataTextColumn;
            if (colContestArea != null)
            {
                colContestArea.Visible = loginUser.EventType == EventType.STATE_ELECTION;
                colContestArea.GroupIndex = colContestArea.Visible ? 3 : -1;
                colContestArea.SortIndex = colContestArea.GroupIndex;
            }
            GridViewDataTextColumn colWard = grdConnoteBarcode.Columns["Ward"] as GridViewDataTextColumn;
            if (colWard != null)
            {
                colWard.Visible = loginUser.EventType == EventType.LOCAL_GOVERNMENT;
                colWard.GroupIndex = colWard.Visible ? 3 : -1;
                colWard.SortIndex = colWard.GroupIndex;
            }
            GridViewDataTextColumn colProductType = grdConnoteBarcode.Columns["ProductType"] as GridViewDataTextColumn;
            if (colProductType != null)
                colProductType.Caption = loginUser.EventType == EventType.LOCAL_GOVERNMENT ? "Contest Type" : "Product Type";

            if (expandAll)
                grdConnoteBarcode.ExpandAll();
        }
        private DataTable GetConnoteDataTable()
        {
            DataTable dtConnote = Session["CC_ConnoteTable"] as DataTable;
            if (dtConnote == null)
            {
                string procMsg = string.Empty;
                LoginUser loginUser = Session["EC_User"] as LoginUser;
                dtConnote = ECWebClass.GetConnoteCatalogueDataTable(loginUser, ref procMsg);
                dtConnote.PrimaryKey = new DataColumn[] { dtConnote.Columns["ConsKey"], dtConnote.Columns["LineNo"] };
                Session["CC_ConnoteTable"] = dtConnote;
            }
            return dtConnote;
        }
        protected void grdView_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            if (e.CellType != GridViewTableCommandCellType.Data)
                return;
            ASPxGridView gridView = sender as ASPxGridView;
            if (e.ButtonID == "btnPrintConnote")
            {
                e.Visible = DevExpress.Utils.DefaultBoolean.False;
                object objDeleted = gridView.GetRowValues(e.VisibleIndex, "Deleted");
                if (objDeleted != null && objDeleted.ToString() != string.Empty)
                {
                    DateTime dtDeleted;
                    if (DateTime.TryParse(objDeleted.ToString(), out dtDeleted))
                        e.Visible = DevExpress.Utils.DefaultBoolean.False;
                    else
                        e.Visible = DevExpress.Utils.DefaultBoolean.True;
                }
                else
                    e.Visible = DevExpress.Utils.DefaultBoolean.True;
            }
        }
        protected void grdView_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.SelectCheckbox)
            {
                object objDeleted = gridView.GetRowValues(e.VisibleIndex, "Deleted");
                if (objDeleted != null && objDeleted.ToString() != string.Empty)
                {
                    DateTime dtDeleted;
                    if (DateTime.TryParse(objDeleted.ToString(), out dtDeleted))
                        e.Enabled = false;
                    else
                        e.Enabled = true;
                }
                else
                    e.Enabled = true;
                e.Visible = e.Enabled;
            }
        }
        protected void cbAll_Init(object sender, EventArgs e)
        {
            ASPxCheckBox chk = sender as ASPxCheckBox;
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            chk.Checked = grid.VisibleRowCount > 0 && grid.VisibleRowCount == grid.Selection.Count;
            if (chk.ClientSideEvents.CheckedChanged == string.Empty)
                chk.ClientSideEvents.CheckedChanged = "function(s, e) { " +
                    "if (s.GetChecked()) " + grid.ClientInstanceName + ".SelectRows(); else " +
                    grid.ClientInstanceName + ".UnselectRows(); } ";
        }
        protected void mGridViewExport_ItemClick(object source, DevExpress.Web.MenuItemEventArgs e)
        {
            ASPxGridView gridView = grdConnoteBarcode;
            if (e.Item.Name.StartsWith("itmExp"))
            {
                string exportName = e.Item.Name.Replace("itmExp", string.Empty);
                ECWebClass.GridViewExport(gridView, exportName);
            }
            else
                if (e.Item.Name.StartsWith("itmGridLayout"))
                {
                    bool success = false;
                    int layoutId = Convert.ToInt32(hfHiddenInfo.Get("LayoutID"));
                    string itemName = e.Item.Name.Replace("itmGridLayout", string.Empty);
                    switch (itemName)
                    {
                        case "PageSize":
                            LoadGrdConnoteLayout(layoutId, Convert.ToInt32(hfHiddenInfo["PageSize"]));
                            break;
                        case "SaveAsNew":
                            layoutId = -1;
                            success = ECWebClass.UpdateGridViewLayout(gridView, ref layoutId, hfHiddenInfo["LayoutName"], 'N', false);
                            if (success)
                                LoadGrdConnoteLayout(layoutId, null);
                            break;
                        case "SetAsDefault":
                            success = ECWebClass.UpdateGridViewLayout(gridView, ref layoutId, hfHiddenInfo["LayoutName"], 'Y', false);
                            if (success)
                                LoadGrdConnoteLayout(layoutId, null);
                            break;
                        case "SaveChanges":
                            success = ECWebClass.UpdateGridViewLayout(gridView, ref layoutId, hfHiddenInfo["LayoutName"], hfHiddenInfo["DefaultLayout"], false);
                            if (success)
                                LoadGrdConnoteLayout(layoutId, null);
                            break;
                        case "Remove":
                            success = ECWebClass.UpdateGridViewLayout(gridView, ref layoutId, null, null, true);
                            if (success)
                                LoadGrdConnoteLayout(null, null);
                            break;
                        case "Original":
                            LoadGrdConnoteLayout(0, null);
                            break;
                        default:
                            LoadGrdConnoteLayout(itemName, null);
                            break;
                    }
                }
        }
        protected void cbPrintLabel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string[] cbInfo = e.Parameter.Split(',');
                if (cbInfo[0] == "Print")
                {   
                    DataTable dtBarcode = grdConnoteBarcode.DataSource as DataTable;
                    if (dtBarcode != null && dtBarcode.Rows.Count > 0)
                    {
                        List<string> sList = new List<string>();
                        for (int i = 1; i < cbInfo.Length; i++)
                        {
                            string[] cbKeys = cbInfo[i].Split('|');
                            if (cbKeys.Length > 1)
                            {
                                object[] barcodeKey = { cbKeys[0], cbKeys[1] };
                                object objBarcode = grdConnoteBarcode.GetRowValuesByKeyValue(barcodeKey, "Barcode");
                                if (objBarcode != null && objBarcode.ToString() != string.Empty)
                                    sList.Add(string.Format("{0}", objBarcode));
                            }
                        }
                        if (sList.Count > 0)
                        {
                            string selectedBarcodeList = string.Join("|", sList.ToArray());
                            e.Result = string.Format("ConnoteLabelViewer.aspx?qs={0}", ECSecurityClass.GetQSEncripted(string.Format("barcode={0}", selectedBarcodeList)));
                        }
                    }                    
                }
            }
        }
        protected void cbConnote_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string[] cbInfo = e.Parameter.Split('|');
                if (cbInfo[0] == "ConnoteView")
                {
                    e.Result = string.Format("{0}?qs={1}", "Consignment.aspx",
                        ECSecurityClass.GetQSEncripted(String.Format("conskey={0}", cbInfo[1])));
                }
                else
                    if (cbInfo[0] == "HeadFilterMode")
                    {
                        Session["CC_GrdConnoteFilterMode"] = cbInfo[1];
                        e.Result = Page.ID;
                    }
            }
        }
        private DataTable GetGridViewLayoutDataTable()
        {
            DataTable dtGridLayout = Session["CC_GrdConnoteLayoutTable"] as DataTable;
            if (dtGridLayout == null)
            {
                dtGridLayout = ECWebClass.GetUserGridLayoutTable(grdConnoteBarcode);
                dtGridLayout.PrimaryKey = new DataColumn[] { dtGridLayout.Columns["ID"] };
                Session["CC_GrdConnoteLayoutTable"] = dtGridLayout;
            }
            return dtGridLayout;
        }

        private void LoadGrdConnoteLayout(object layoutId, object pageSize)
        {
            ASPxMenu mGridViewExport = pcConnote.FindControl("mGridViewExport") as ASPxMenu;
            if (mGridViewExport != null)
            {
                DevExpress.Web.MenuItem itmGridLayout = mGridViewExport.Items.FindByName("itmGridLayout");
                if (itmGridLayout != null)
                {
                    Session["CC_GrdConnoteLayoutTable"] = null;
                    itmGridLayout.Items.Clear();

                    DevExpress.Web.MenuItem mItem;

                    DataTable dtLayout = GetGridViewLayoutDataTable();
                    bool hasDefault = false;
                    bool hasItems = dtLayout != null && dtLayout.Rows.Count > 0;
                    if (hasItems)
                    {
                        foreach (DataRow dr in dtLayout.Rows)
                        {
                            mItem = new DevExpress.Web.MenuItem(dr["LayoutName"].ToString(), "itmGridLayout" + dr["ID"]);

                            itmGridLayout.Items.Add(mItem);
                            if (layoutId != null)
                            {
                                if (layoutId.ToString() == dr["ID"].ToString())
                                {
                                    if (!hasDefault)
                                        hasDefault = Convert.ToChar(dr["DefaultLayout"]) == 'Y';
                                    mItem.Checked = true;

                                    hfHiddenInfo.Set("LayoutID", dr["ID"]);
                                    hfHiddenInfo.Set("LayoutName", dr["LayoutName"]);
                                    hfHiddenInfo.Set("DefaultLayout", dr["DefaultLayout"]);
                                    hfHiddenInfo.Set("PageSize", dr["RowsPerPage"]);

                                    grdConnoteBarcode.LoadClientLayout(dr["Layout"].ToString());
                                }
                                if (Convert.ToChar(dr["DefaultLayout"]) == 'Y')
                                    mItem.Text += " (Default)";
                            }
                            else
                                if (Convert.ToChar(dr["DefaultLayout"]) == 'Y')
                                {
                                    if (!hasDefault)
                                        hasDefault = true;
                                    mItem.Text += " (Default)";
                                    mItem.Checked = true;

                                    hfHiddenInfo.Set("LayoutID", dr["ID"]);
                                    hfHiddenInfo.Set("LayoutName", dr["LayoutName"]);
                                    hfHiddenInfo.Set("DefaultLayout", dr["DefaultLayout"]);
                                    hfHiddenInfo.Set("PageSize", dr["RowsPerPage"]);

                                    grdConnoteBarcode.LoadClientLayout(dr["Layout"].ToString());
                                }
                        }
                    }

                    itmGridLayout.Items.Add("Save As", "itmGridLayoutSaveAsNew").BeginGroup = true;

                    mItem = new DevExpress.Web.MenuItem("Original", "itmGridLayoutOriginal");
                    itmGridLayout.Items.Insert(0, mItem);
                    if ((layoutId == null && !hasDefault) || (layoutId != null && layoutId.ToString() == "0"))
                    {
                        mItem.Checked = true;

                        hfHiddenInfo.Set("LayoutID", 0);
                        hfHiddenInfo.Set("LayoutName", "Original");
                        hfHiddenInfo.Set("DefaultLayout", 'N');
                        hfHiddenInfo.Set("PageSize", defaultPageSize);

                        grdConnoteBarcode.LoadClientLayout(hfHiddenInfo.Get("OriginalLayout").ToString());
                    }
                    else
                    {
                        if (!hasDefault)
                            itmGridLayout.Items.Add("Set Default", "itmGridLayoutSetAsDefault");
                        itmGridLayout.Items.Add("Save", "itmGridLayoutSaveChanges");
                        itmGridLayout.Items.Add("Remove", "itmGridLayoutRemove");
                    }

                    itmGridLayout.Items.Add("Set Page Size", "itmGridLayoutPageSize").BeginGroup = true;

                    if (pageSize != null)
                        grdConnoteBarcode.SettingsPager.PageSize = Convert.ToInt32(pageSize);
                    else
                        grdConnoteBarcode.SettingsPager.PageSize = Convert.ToInt32(hfHiddenInfo.Get("PageSize"));
                }
            }
        }

        protected void grdConnoteBarcode_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            if (!string.IsNullOrEmpty(e.Parameters))
            {
                if (e.Parameters == "Refresh")
                {
                    Session.Remove("CC_ConnoteTable");
                    LoadConnoteData(false);
                }
                else
                    if (e.Parameters == "ClearFilter")
                    {
                        gridView.FilterExpression = string.Empty;
                        Session["CC_GrdConnoteFilterLayout"] = null;
                        Session.Remove("CC_ConnoteTable");
                        LoadConnoteData(true);
                    }
            }
        }
        protected void grdConnoteBarcode_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            ASPxGridView grdView = sender as ASPxGridView;            
            if (e.CallbackName == "APPLYFILTER" || e.CallbackName == "APPLYCOLUMNFILTER" || e.CallbackName == "APPLYHEADERCOLUMNFILTER")
            {
                string filteredLayout = grdView.SaveClientLayout();
                if (!string.IsNullOrEmpty(filteredLayout))
                    Session["CC_GrdConnoteFilterLayout"] = filteredLayout;
                else
                    Session["CC_GrdConnoteFilterLayout"] = null;
                grdView.ExpandAll();
            }
        }

        protected void grdConnoteBarcode_BeforeGetCallbackResult(object sender, EventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            
            ASPxLabel lblGrdConnoteFilter = grdConnoteBarcode.FindTitleTemplateControl("lblGrdConnoteFilter") as ASPxLabel;
            if (lblGrdConnoteFilter != null)
            {
                if (!string.IsNullOrEmpty(grdConnoteBarcode.FilterExpression))
                    lblGrdConnoteFilter.Text = string.Format("Current Filter: {0}", grdConnoteBarcode.FilterExpression);
                else
                    lblGrdConnoteFilter.Text = string.Empty;
            }
            lblGrdConnoteFilter = grdConnoteBarcode.FindStatusBarTemplateControl("lblGrdConnoteFilter") as ASPxLabel;
            if (lblGrdConnoteFilter != null)
            {
                if (!string.IsNullOrEmpty(grdConnoteBarcode.FilterExpression))
                    lblGrdConnoteFilter.Text = string.Format("Current Filter: {0}", grdConnoteBarcode.FilterExpression);
                else
                    lblGrdConnoteFilter.Text = string.Empty;
            }
        }

        protected void grdConnote_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
        {
            //ASPxGridView grdView = sender as ASPxGridView;
            string fieldValue = string.Empty;
            if (e.Column.FieldName == "Deactivated")
            {
                object deleted = e.GetListSourceFieldValue("Deleted");
                if (deleted != null && deleted.ToString() != string.Empty)
                    fieldValue = "Y";
                else
                    fieldValue = "N";

                if (fieldValue != string.Empty)
                    e.Value = fieldValue;
            }            
        }
    }
}