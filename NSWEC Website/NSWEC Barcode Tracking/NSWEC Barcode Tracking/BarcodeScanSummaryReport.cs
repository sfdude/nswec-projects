﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace NSWEC_Barcode_Tracking
{
    public partial class BarcodeScanSummaryReport : DevExpress.XtraReports.UI.XtraReport
    {
        public BarcodeScanSummaryReport()
        {
            InitializeComponent();
        }

        private void BarcodeScanSummaryReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // report header
            lblDateFrom.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".DateFrom", "{0:dd/MM/yyyy}"));
            lblDateTo.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".DateTo", "{0:dd/MM/yyyy}"));

            // connote band
            xrDB_DateRange.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".DateRange", ""));
            xrDB_NO_SCAN.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".NO_SCAN", ""));
            xrDB_TotalCharge.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".SF_Charge", "{0:C}"));

            lblTotalScan.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".NO_SCAN", ""));
            lblTotalScan.Summary.Func = SummaryFunc.Sum;
            lblTotalScan.Summary.Running = SummaryRunning.Report;

            lblTotalCharge.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".SF_Charge", ""));
            lblTotalCharge.Summary.Func = SummaryFunc.Sum;
            lblTotalCharge.Summary.FormatString = "{0:C}";
            lblTotalCharge.Summary.Running = SummaryRunning.Report;
            lblGST.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".SF_GST", ""));
            lblGST.Summary.Func = SummaryFunc.Sum;
            lblGST.Summary.FormatString = "{0:C}";
            lblGST.Summary.Running = SummaryRunning.Report;
            lblTotalChargeExclGst.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".SF_ChargeInclGst", ""));
            lblTotalChargeExclGst.Summary.Func = SummaryFunc.Sum;
            lblTotalChargeExclGst.Summary.FormatString = "{0:C}";
            lblTotalChargeExclGst.Summary.Running = SummaryRunning.Report;
        }

    }
}
