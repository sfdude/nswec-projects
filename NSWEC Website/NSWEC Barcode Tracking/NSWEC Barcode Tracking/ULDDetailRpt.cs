﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace NSWEC_Barcode_Tracking
{

    /// <summary>
    /// Summary description for ULDDetailRpt
    /// </summary>
    public partial class ULDDetailRpt : DevExpress.XtraReports.UI.XtraReport
    {
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private ReportHeaderBand ReportHeader;
        private XRLine xrLine1;
        private XRLabel xrLblContainer;
        private XRLabel xrLblProduct;
        private XRLabel xrLblVenue;
        private XRLabel xrLblContestArea;
        private XRLabel xrLblDistrict;
        private XRLabel xrLblEMO;
        private XRLabel xrLabel7;
        private XRLabel xrLabelProduct;
        private XRLabel xrLabel5;
        private XRLabel xrLabel1;
        private XRLabel xrLabelEMO;
        private XRLabel xrLabelDistrict;
        private XRLabel xrLabelContestArea;
        private XRBarCode xrDB_BarCode;
        public DevExpress.XtraReports.Parameters.Parameter ConsKey;
        public DevExpress.XtraReports.Parameters.Parameter LineNo;
        public DevExpress.XtraReports.Parameters.Parameter IsLGA;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ULDDetailRpt()
        {
            InitializeComponent();
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrDB_BarCode = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrLblContainer = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblProduct = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblVenue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblContestArea = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblDistrict = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblEMO = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelProduct = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelEMO = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDistrict = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelContestArea = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.ConsKey = new DevExpress.XtraReports.Parameters.Parameter();
            this.LineNo = new DevExpress.XtraReports.Parameters.Parameter();
            this.IsLGA = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDB_BarCode,
            this.xrLblContainer,
            this.xrLblProduct,
            this.xrLblVenue,
            this.xrLblContestArea,
            this.xrLblDistrict,
            this.xrLblEMO});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 35.41667F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBackColor = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrDB_BarCode
            // 
            this.xrDB_BarCode.AutoModule = true;
            this.xrDB_BarCode.Dpi = 100F;
            this.xrDB_BarCode.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDB_BarCode.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrDB_BarCode.Name = "xrDB_BarCode";
            this.xrDB_BarCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100F);
            this.xrDB_BarCode.SizeF = new System.Drawing.SizeF(185.4167F, 34.16672F);
            this.xrDB_BarCode.StylePriority.UseFont = false;
            this.xrDB_BarCode.StylePriority.UsePadding = false;
            this.xrDB_BarCode.StylePriority.UseTextAlignment = false;
            code128Generator1.CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetAuto;
            this.xrDB_BarCode.Symbology = code128Generator1;
            this.xrDB_BarCode.Text = "110000000121";
            this.xrDB_BarCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLblContainer
            // 
            this.xrLblContainer.Dpi = 100F;
            this.xrLblContainer.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblContainer.LocationFloat = new DevExpress.Utils.PointFloat(838F, 0F);
            this.xrLblContainer.Name = "xrLblContainer";
            this.xrLblContainer.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblContainer.SizeF = new System.Drawing.SizeF(97F, 18F);
            this.xrLblContainer.StylePriority.UseFont = false;
            this.xrLblContainer.StylePriority.UseTextAlignment = false;
            this.xrLblContainer.Text = "xrLblContainer";
            this.xrLblContainer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblProduct
            // 
            this.xrLblProduct.Dpi = 100F;
            this.xrLblProduct.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblProduct.LocationFloat = new DevExpress.Utils.PointFloat(706.9585F, 0F);
            this.xrLblProduct.Name = "xrLblProduct";
            this.xrLblProduct.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblProduct.SizeF = new System.Drawing.SizeF(131.0415F, 18F);
            this.xrLblProduct.StylePriority.UseFont = false;
            this.xrLblProduct.StylePriority.UseTextAlignment = false;
            this.xrLblProduct.Text = "xrLblProduct";
            this.xrLblProduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblVenue
            // 
            this.xrLblVenue.Dpi = 100F;
            this.xrLblVenue.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblVenue.LocationFloat = new DevExpress.Utils.PointFloat(560.5002F, 0F);
            this.xrLblVenue.Name = "xrLblVenue";
            this.xrLblVenue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblVenue.SizeF = new System.Drawing.SizeF(146.4583F, 18F);
            this.xrLblVenue.StylePriority.UseFont = false;
            this.xrLblVenue.StylePriority.UseTextAlignment = false;
            this.xrLblVenue.Text = "xrLblVenue";
            this.xrLblVenue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblContestArea
            // 
            this.xrLblContestArea.Dpi = 100F;
            this.xrLblContestArea.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblContestArea.LocationFloat = new DevExpress.Utils.PointFloat(454.6668F, 0F);
            this.xrLblContestArea.Name = "xrLblContestArea";
            this.xrLblContestArea.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblContestArea.SizeF = new System.Drawing.SizeF(105.8333F, 18F);
            this.xrLblContestArea.StylePriority.UseFont = false;
            this.xrLblContestArea.StylePriority.UseTextAlignment = false;
            this.xrLblContestArea.Text = "xrLblContestArea";
            this.xrLblContestArea.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblDistrict
            // 
            this.xrLblDistrict.Dpi = 100F;
            this.xrLblDistrict.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblDistrict.LocationFloat = new DevExpress.Utils.PointFloat(346.9585F, 0F);
            this.xrLblDistrict.Name = "xrLblDistrict";
            this.xrLblDistrict.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblDistrict.SizeF = new System.Drawing.SizeF(107.7084F, 18F);
            this.xrLblDistrict.StylePriority.UseFont = false;
            this.xrLblDistrict.StylePriority.UseTextAlignment = false;
            this.xrLblDistrict.Text = "xrLblDistrict";
            this.xrLblDistrict.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblEMO
            // 
            this.xrLblEMO.Dpi = 100F;
            this.xrLblEMO.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblEMO.LocationFloat = new DevExpress.Utils.PointFloat(185.4167F, 0F);
            this.xrLblEMO.Name = "xrLblEMO";
            this.xrLblEMO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblEMO.SizeF = new System.Drawing.SizeF(161.5418F, 18F);
            this.xrLblEMO.StylePriority.UseFont = false;
            this.xrLblEMO.StylePriority.UseTextAlignment = false;
            this.xrLblEMO.Text = "xrLblEMO";
            this.xrLblEMO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.BackColor = System.Drawing.Color.LightGray;
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.xrLabelProduct,
            this.xrLabel5,
            this.xrLabel1,
            this.xrLabelEMO,
            this.xrLabelDistrict,
            this.xrLabelContestArea,
            this.xrLine1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 25.04168F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseBackColor = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(838F, 0F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(97F, 18F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Container Type";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelProduct
            // 
            this.xrLabelProduct.Dpi = 100F;
            this.xrLabelProduct.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelProduct.LocationFloat = new DevExpress.Utils.PointFloat(706.9586F, 0F);
            this.xrLabelProduct.Name = "xrLabelProduct";
            this.xrLabelProduct.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelProduct.SizeF = new System.Drawing.SizeF(131.0414F, 18F);
            this.xrLabelProduct.StylePriority.UseFont = false;
            this.xrLabelProduct.StylePriority.UseTextAlignment = false;
            this.xrLabelProduct.Text = "Product Type";
            this.xrLabelProduct.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(560.5002F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(146.4584F, 18F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Venue";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(185.4167F, 18F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Child Barcode";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelEMO
            // 
            this.xrLabelEMO.Dpi = 100F;
            this.xrLabelEMO.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelEMO.LocationFloat = new DevExpress.Utils.PointFloat(185.4167F, 0F);
            this.xrLabelEMO.Name = "xrLabelEMO";
            this.xrLabelEMO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelEMO.SizeF = new System.Drawing.SizeF(161.5418F, 18F);
            this.xrLabelEMO.StylePriority.UseFont = false;
            this.xrLabelEMO.StylePriority.UseTextAlignment = false;
            this.xrLabelEMO.Text = "EMO";
            this.xrLabelEMO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelDistrict
            // 
            this.xrLabelDistrict.Dpi = 100F;
            this.xrLabelDistrict.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelDistrict.LocationFloat = new DevExpress.Utils.PointFloat(346.9585F, 0F);
            this.xrLabelDistrict.Name = "xrLabelDistrict";
            this.xrLabelDistrict.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelDistrict.SizeF = new System.Drawing.SizeF(107.7084F, 18F);
            this.xrLabelDistrict.StylePriority.UseFont = false;
            this.xrLabelDistrict.StylePriority.UseTextAlignment = false;
            this.xrLabelDistrict.Text = "District";
            this.xrLabelDistrict.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelContestArea
            // 
            this.xrLabelContestArea.Dpi = 100F;
            this.xrLabelContestArea.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelContestArea.LocationFloat = new DevExpress.Utils.PointFloat(454.6668F, 0F);
            this.xrLabelContestArea.Name = "xrLabelContestArea";
            this.xrLabelContestArea.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelContestArea.SizeF = new System.Drawing.SizeF(105.8334F, 18F);
            this.xrLabelContestArea.StylePriority.UseFont = false;
            this.xrLabelContestArea.StylePriority.UseTextAlignment = false;
            this.xrLabelContestArea.Text = "Contest Area";
            this.xrLabelContestArea.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 18F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(935F, 4.000001F);
            // 
            // ConsKey
            // 
            this.ConsKey.Description = "ConsKey";
            this.ConsKey.Name = "ConsKey";
            this.ConsKey.Visible = false;
            // 
            // LineNo
            // 
            this.LineNo.Description = "LineNo";
            this.LineNo.Name = "LineNo";
            this.LineNo.Type = typeof(int);
            this.LineNo.ValueInfo = "0";
            this.LineNo.Visible = false;
            // 
            // IsLGA
            // 
            this.IsLGA.Description = "IsLGA";
            this.IsLGA.Name = "IsLGA";
            this.IsLGA.Type = typeof(bool);
            this.IsLGA.ValueInfo = "False";
            this.IsLGA.Visible = false;
            // 
            // ULDDetailRpt
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margins = new System.Drawing.Printing.Margins(100, 65, 0, 0);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ConsKey,
            this.LineNo,
            this.IsLGA});
            this.Version = "15.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ULDDetailRpt_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private void ULDDetailRpt_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrDB_BarCode.DataBindings.Add(new XRBinding("Text", null, this.DataMember + ".Barcode", ""));
            if ((bool)this.IsLGA.Value)
            {
                xrLabelEMO.Text = "RO";
                xrLabelDistrict.Text = "LGA";
                xrLabelContestArea.Text = "Ward";
                xrLabelProduct.Text = "Contest";
            }
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            object emoCode = GetCurrentColumnValue("EMOCode");
            if (emoCode != null && !string.IsNullOrEmpty(emoCode.ToString()))
                xrLblEMO.Text = string.Format("{0}({1})", GetCurrentColumnValue("EMOName"), emoCode);
            else
                xrLblEMO.Text = string.Empty;

            object districtCode = GetCurrentColumnValue("DistrictCode");
            if (districtCode != null && !string.IsNullOrEmpty(districtCode.ToString()))
                xrLblDistrict.Text = string.Format("{0}({1})", GetCurrentColumnValue("DistrictName"), districtCode);
            else
                xrLblDistrict.Text = string.Empty;
            /*
            object contestAreaCode = GetCurrentColumnValue("ContestAreaId");
            if (contestAreaCode != null && !string.IsNullOrEmpty(contestAreaCode.ToString()))
                xrLblContestArea.Text = string.Format("{0}({1})", GetCurrentColumnValue("ContestAreaCode"), contestAreaCode);
            else
                xrLblContestArea.Text = string.Empty;
            */
            if ((bool)this.IsLGA.Value)
            {
                object contestAreaCode = GetCurrentColumnValue("WardCode");
                if (contestAreaCode != null && !string.IsNullOrEmpty(contestAreaCode.ToString()))
                    xrLblContestArea.Text = string.Format("{0}({1})", GetCurrentColumnValue("WardName"), contestAreaCode);
                else
                    xrLblContestArea.Text = string.Empty;
            }
            else
            {
                object contestAreaCode = GetCurrentColumnValue("ContestAreaId");
                if (contestAreaCode != null && !string.IsNullOrEmpty(contestAreaCode.ToString()))
                    xrLblContestArea.Text = string.Format("{0}({1})", GetCurrentColumnValue("ContestAreaCode"), contestAreaCode);
                else
                    xrLblContestArea.Text = string.Empty;
            }

            object venueCode = GetCurrentColumnValue("VenueCode");
            if (venueCode != null && !string.IsNullOrEmpty(venueCode.ToString()))
                xrLblVenue.Text = string.Format("{0}({1})", GetCurrentColumnValue("VenueName"), venueCode);
            else
                xrLblVenue.Text = string.Empty;

            object productCode = GetCurrentColumnValue("ProductCode");
            if (productCode != null && !string.IsNullOrEmpty(productCode.ToString()))
                xrLblProduct.Text = string.Format("{0}({1})", GetCurrentColumnValue("ProductName"), productCode);
            else
                xrLblProduct.Text = string.Empty;

            object containerCode = GetCurrentColumnValue("ContainerCode");
            if (containerCode != null && !string.IsNullOrEmpty(containerCode.ToString()))
                xrLblContainer.Text = string.Format("{0}({1})", GetCurrentColumnValue("ContainerName"), containerCode);
            else
                xrLblContainer.Text = string.Empty;
        }
    }
}
