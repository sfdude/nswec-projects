﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Security;
/*
using System.Web.Services;
using System.Web.Script.Services;
using System.Drawing;
 * */
using System.Data;
using System.Net;
using System.IO;
//using Newtonsoft;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class Login1 : System.Web.UI.Page
    {
        private static bool isPersistent = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ID = "Login.aspx";
            /* 
            Color sfColor = Color.FromArgb(0, 125, 134); // "#007D86" - Pantone: 7712U
            sfColor = Color.FromArgb(0, 206, 166); // "#00CEA6" - Pantone: 3265U
            sfColor = Color.FromArgb(137, 234, 243); // "#89EAF3" - Pantone: 630U            
            */
            if (!IsPostBack)
            {
                DataTable dtApp = ECSecurityClass.GetApplicationDataTable();
                if (dtApp != null && dtApp.Rows.Count > 0 && !(dtApp.Rows[0]["WebApplicationTitle"] is DBNull) && dtApp.Rows[0]["WebApplicationTitle"].ToString() != string.Empty)
                {
                    lblWebTitle.Text = string.Format("Welcome to {0}", dtApp.Rows[0]["WebApplicationTitle"]);
                }
                
                ASPxCaptcha myCaptcha = smLogin.FindControl("ASPxCaptcha1") as ASPxCaptcha;
                if (myCaptcha != null)
                {
                    myCaptcha.ClientVisible = Session["ShowCaptcha"] != null && Session["ShowCaptcha"].ToString() != string.Empty;
                    if (myCaptcha.ClientVisible)
                    {
                        string uId = string.Empty;
                        string attLeft = string.Empty;
                        if (ECSecurityClass.CheckShowCaptchaQueryString(Session["ShowCaptcha"].ToString(), ref uId, ref attLeft))
                        {
                            smLogin.UserName = uId;
                            lblMessage.Text = string.Format("Your login attempt was not successful. Please try again. You have {0} login attempt left", attLeft);
                            lblMessage.ClientVisible = true;
                        }                        
                        
                        Session["ShowCaptcha"] = null;
                        Control ctlPassword = smLogin.FindControl("Password") as Control;
                        if (ctlPassword != null)
                            ctlPassword.Focus();
                        else
                            smLogin.Focus();
                    }
                    else
                        smLogin.Focus();
                }
                else
                    smLogin.Focus();

            }
        }
        /*
        [WebMethod(enableSession: true)] 
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string CaptchaVerify(string token)
        {
            HttpContext.Current.Session["CaptchaVerified"] = null;
            string result = string.Empty;
            if (!ValidReCaptcha("login", token))
                result = "reCaptcha validation failed";
            else
            {
                result = "success";
            }
            HttpContext.Current.Session["CaptchaVerified"] = result;
            return result;
        }
         */
        private static bool ValidReCaptcha(string action, string recaptchaToken)
        {
            bool result = !string.IsNullOrEmpty(recaptchaToken);
            if (result)
            {
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var secretKey = ECSecurityClass.SecretKeyV3();
                string reqUri = string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, recaptchaToken);

                var req = (HttpWebRequest)WebRequest.Create(reqUri);
                using (WebResponse resp = req.GetResponse())
                {
                    using (StreamReader stream = new StreamReader(resp.GetResponseStream()))
                    {
                        string respString = stream.ReadToEnd();
                        ResponseToken response = new ResponseToken();
                        response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseToken>(respString);
                        result = response.Success && response.action == action && response.score > 0.4;
                    }
                }
            }
            return result;
        }
        private static ResponseToken GetValidReCaptchaResponse(string recaptchaToken)
        {
            ResponseToken tokenResp = null;
            var secretKey = ECSecurityClass.SecretKeyV3();
            string reqUri = string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, recaptchaToken);

            var req = (HttpWebRequest)WebRequest.Create(reqUri);
            using (WebResponse resp = req.GetResponse())
            {
                using (StreamReader stream = new StreamReader(resp.GetResponseStream()))
                {
                    string respString = stream.ReadToEnd();
                    tokenResp = new ResponseToken();
                    tokenResp = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseToken>(respString);
                }
            }
            return tokenResp;
        }
        protected void Login1_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            e.Cancel = false;
            return;            
            System.Web.UI.WebControls.Login login = sender as System.Web.UI.WebControls.Login;
            if (login != null)
            {                
                /*
                ASPxCaptcha captcha = login.FindControl("ASPxCaptcha1") as ASPxCaptcha;
                if (captcha != null && captcha.ClientVisible)
                {
                    captcha.ValidationSettings.ErrorText = string.Empty;
                    e.Cancel = !captcha.IsValid;
                    if (e.Cancel)
                    {
                        captcha.ValidationSettings.ErrorText = "The submitted code is incorrect";
                        captcha.Focus();
                    }
                }
                 */
            }
        }
        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            smLogin.FailureText = string.Empty;

            if (ECSecurityClass.LoginUserLocked(smLogin.UserName))
            {
                Response.Write(ECSecurityClass.REDIRECT_BLOCKED_HTML);
                Response.End();
            }
                                    
            if (ValidateUser(smLogin.UserName.ToLower(), smLogin.Password))
            {
                string strUsername = smLogin.UserName.ToLower();
                FormsAuthentication.Initialize();
                String strRole = AssignRoles(strUsername);

                //The AddMinutes determines how long the user will be logged in after leaving
                //the site if he doesn't log off.
                FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1,
                    strUsername, DateTime.Now, DateTime.Now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes), 
                    isPersistent, strRole, FormsAuthentication.FormsCookiePath);
                HttpCookie cook = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(fat));
                //if (fat.IsPersistent)
                    cook.Expires = fat.Expiration;

                Response.Cookies.Add(cook);

                LoginUser loginUser = Session["EC_User"] as LoginUser;
                loginUser.SessionId = Session.SessionID;
                loginUser.FrmAuthCookie = cook.Value;

                ECSecurityClass.InsertSessionToken(loginUser);
                /*
                // Session Fixation Internal
                // create a new GUID and save into the session
                string guid = Guid.NewGuid().ToString();
                Session["AuthToken"] = guid;
                // now create a new cookie with this guid value
                //Response.Cookies.Add(new HttpCookie("AuthToken", guid));
                Request.Cookies.Add(new HttpCookie("AuthToken", guid));
                */
                Response.Redirect("ConsignmentCatalogue.aspx");
            }
        }
        private bool ValidateUser(string userName, string passWord)
        {
            string processMsg = string.Empty;

            bool result = userName.StartsWith("smsupport") || ECSecurityClass.IsValidEmailAddress(userName);
            if (!result)
            {
                lblMessage.Text = "Your login is not valid email address.";
                lblMessage.ClientVisible = true;
                return result;
            }

            LoginUser loginUser = ECSecurityClass.GetLoginUser(userName, passWord, ref processMsg);
            result = loginUser != null;
            if (result)
            {
                //getting the sessions objects from the Application
                Hashtable sessions = (Hashtable)Application["WEB_SESSIONS_OBJECT"];
                if (sessions == null)
                    sessions = new Hashtable();

                //getting the pointer to the Session of the current logged in user
                HttpSessionState existingUserSession = (HttpSessionState)sessions[userName.ToLower()];
                //logout current logged in user
                if (existingUserSession != null)
                    existingUserSession["EC_User"] = null;

                //putting the user in the session
                Session["EC_User"] = loginUser;
                if (loginUser.SessionTimeOut > 0)
                    Session.Timeout = loginUser.SessionTimeOut;
                sessions[loginUser.UserName.ToLower()] = Session;
                Application.Lock(); //lock to prevent duplicate objects
                Application["WEB_SESSIONS_OBJECT"] = sessions;
                Application.UnLock();

                if (!string.IsNullOrEmpty(processMsg))
                    Session["LoginInfoMessage"] = processMsg;
            }
            else
            {
                smLogin.FailureText = string.Empty;
                if (!string.IsNullOrEmpty(processMsg))
                {
                    if (processMsg.Contains("Your login password has expired and must be changed"))
                        Response.Redirect("RenewalPassword.aspx");
                    else
                        smLogin.FailureText = processMsg;
                }
                else
                    smLogin.FailureText = "Your login attempt was not successful. Please try again.";
            }
            /*
            int maxLoginAttempts = ECSecurityClass.MaxLoginAttempts();
            int showCaptchaAttempts = ECSecurityClass.ShowCaptchaAttempts();
            int lockDuration = ECSecurityClass.LockDuration();
             */
            int attCount = ECSecurityClass.SecurityLoginAttempt(userName, Request.UserHostAddress, result, ref processMsg);
            /*
            if (attCount > 0)
            {
                if (maxLoginAttempts == attCount)
                {
                    //lblMessage.Text = string.Format("Your login attempt was not successful. You're temporarily locked. Please try again after {0} minutes", lockDuration);
                    //lblMessage.ClientVisible = true;
                    //smLogin.Visible = false;
                     
                    Response.Write(ECSecurityClass.REDIRECT_BLOCKED_HTML);
                    Response.End();
                }
                else
                {
                    if (maxLoginAttempts - attCount > 1)
                        smLogin.FailureText += string.Format(" You have {0} login attempts left", maxLoginAttempts - attCount);
                    else
                        smLogin.FailureText += string.Format(" You have {0} login attempt left", maxLoginAttempts - attCount);
                    Control ctlPassword = smLogin.FindControl("Password") as Control;
                    if (ctlPassword != null)
                        ctlPassword.Focus();
                    if (showCaptchaAttempts > 0 && attCount >= showCaptchaAttempts)
                    {
                        Session["ShowCaptcha"] = ECSecurityClass.GetQSEncripted(string.Format("{0}|{1}", userName, maxLoginAttempts - attCount));
                        Response.Redirect("Login.aspx");
                    }
                }                               
            }
             */
            return result;
        }
        private bool ValidateUser(string userName, string passWord, ref string message)
        {
            string processMsg = string.Empty;

            bool result = userName.StartsWith("smsupport") || ECSecurityClass.IsValidEmailAddress(userName);
            if (!result)
            {
                message = "Your login is not valid email address.";
                return result;
            }

            LoginUser loginUser = ECSecurityClass.GetLoginUser(userName, passWord, ref processMsg);
            result = loginUser != null;
            if (result)
            {
                //getting the sessions objects from the Application
                Hashtable sessions = (Hashtable)Application["WEB_SESSIONS_OBJECT"];
                if (sessions == null)
                    sessions = new Hashtable();

                //getting the pointer to the Session of the current logged in user
                HttpSessionState existingUserSession = (HttpSessionState)sessions[userName.ToLower()];
                //logout current logged in user
                if (existingUserSession != null)
                {
                    existingUserSession["EC_User"] = null;
                    processMsg = "An existing session using the same credentials has been found and will be terminated.";// This happens because that existing session has not been logged out properly";
                }

                //putting the user in the session
                Session["EC_User"] = loginUser;
                if (loginUser.SessionTimeOut > 0)
                    Session.Timeout = loginUser.SessionTimeOut;
                sessions[loginUser.UserName.ToLower()] = Session;
                Application.Lock(); //lock to prevent duplicate objects
                Application["WEB_SESSIONS_OBJECT"] = sessions;
                Application.UnLock();

                if (!string.IsNullOrEmpty(processMsg))
                    Session["LoginInfoMessage"] = processMsg;
            }
            else
            {
                smLogin.FailureText = string.Empty;
                if (!string.IsNullOrEmpty(processMsg))
                {
                    if (processMsg.Contains("Your login password has expired and must be changed"))
                        Response.Redirect("RenewalPassword.aspx");
                    else
                        message = processMsg;
                }
                else
                    message = "Your login attempt was not successful. Please try again.";
            }
            /*
            int maxLoginAttempts = ECSecurityClass.MaxLoginAttempts();
            int showCaptchaAttempts = ECSecurityClass.ShowCaptchaAttempts();
            int lockDuration = ECSecurityClass.LockDuration();
             */
            int attCount = ECSecurityClass.SecurityLoginAttempt(userName, Request.UserHostAddress, result, ref processMsg);
            /*
            if (attCount > 0)
            {
                if (maxLoginAttempts == attCount)
                {
                    //lblMessage.Text = string.Format("Your login attempt was not successful. You're temporarily locked. Please try again after {0} minutes", lockDuration);
                    //lblMessage.ClientVisible = true;
                    //smLogin.Visible = false;
                     
                    Response.Write(ECSecurityClass.REDIRECT_BLOCKED_HTML);
                    Response.End();
                }
                else
                {
                    if (maxLoginAttempts - attCount > 1)
                        smLogin.FailureText += string.Format(" You have {0} login attempts left", maxLoginAttempts - attCount);
                    else
                        smLogin.FailureText += string.Format(" You have {0} login attempt left", maxLoginAttempts - attCount);
                    Control ctlPassword = smLogin.FindControl("Password") as Control;
                    if (ctlPassword != null)
                        ctlPassword.Focus();
                    if (showCaptchaAttempts > 0 && attCount >= showCaptchaAttempts)
                    {
                        Session["ShowCaptcha"] = ECSecurityClass.GetQSEncripted(string.Format("{0}|{1}", userName, maxLoginAttempts - attCount));
                        Response.Redirect("Login.aspx");
                    }
                }                               
            }
             */
            return result;
        }
        private static String AssignRoles(String strUsername)
        {
            //Return a | separated list of roles this user is a member of
            if (strUsername == "ec")
                return "bigboss|wimpyuser";
            else
                return String.Empty;
        }
                
        protected void cbLogin_Callback(object source, CallbackEventArgs e)
        {
            e.Result = string.Empty;
            if (!string.IsNullOrEmpty(e.Parameter))
            { 
                ResponseToken tokenResp = GetValidReCaptchaResponse(e.Parameter);
                if (tokenResp == null)
                {
                    e.Result = "Missing reCaptcha token";
                    return;
                }
                if (!(tokenResp.Success && tokenResp.action == "login" && tokenResp.score >= ECSecurityClass.ScoreV3()))
                {
                    e.Result = "reCaptcha validation failed";
                    return;
                }

                string procMsg = string.Empty;
                if (!ValidateUser(smLogin.UserName.ToLower(), smLogin.Password, ref procMsg))
                {
                    e.Result = procMsg;
                    return;
                }
                string strUsername = smLogin.UserName.ToLower();
                FormsAuthentication.Initialize();
                String strRole = AssignRoles(strUsername);

                //The AddMinutes determines how long the user will be logged in after leaving
                //the site if he doesn't log off.
                FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1,
                    strUsername, DateTime.Now, DateTime.Now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes),
                    isPersistent, strRole, FormsAuthentication.FormsCookiePath);
                HttpCookie cook = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(fat));
                //if (fat.IsPersistent)
                cook.Expires = fat.Expiration;

                Response.Cookies.Add(cook);

                LoginUser loginUser = Session["EC_User"] as LoginUser;
                loginUser.SessionId = Session.SessionID;
                loginUser.FrmAuthCookie = cook.Value;

                ECSecurityClass.InsertSessionToken(loginUser);
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("ConsignmentCatalogue.aspx");
            }
            
        }

    }
    public class ResponseToken
    {
        public DateTime challenge_ts { get; set; }
        public double score { get; set; }
        public List<string> ErrorCodes { get; set; }
        public bool Success { get; set; }
        public string hostname { get; set; }
        public string action { get; set; }
    }
}