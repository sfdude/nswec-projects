﻿namespace NSWEC_Barcode_Tracking
{
    partial class StatusDetailRpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detailBand_StatusDet = new DevExpress.XtraReports.UI.DetailBand();
            this.xrDB_MovementDirection = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDB_StatusNotes = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblLocation = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDB_StatusDate = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsKey = new DevExpress.XtraReports.Parameters.Parameter();
            this.LineNo = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detailBand_StatusDet
            // 
            this.detailBand_StatusDet.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDB_MovementDirection,
            this.xrDB_StatusNotes,
            this.xrLblLocation,
            this.xrDB_StatusDate});
            this.detailBand_StatusDet.Dpi = 100F;
            this.detailBand_StatusDet.HeightF = 18.75F;
            this.detailBand_StatusDet.Name = "detailBand_StatusDet";
            this.detailBand_StatusDet.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.detailBand_StatusDet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.detailBand_StatusDet.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.detailBand_StatusDet_BeforePrint);
            // 
            // xrDB_MovementDirection
            // 
            this.xrDB_MovementDirection.Dpi = 100F;
            this.xrDB_MovementDirection.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDB_MovementDirection.LocationFloat = new DevExpress.Utils.PointFloat(609.7917F, 0F);
            this.xrDB_MovementDirection.Name = "xrDB_MovementDirection";
            this.xrDB_MovementDirection.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDB_MovementDirection.SizeF = new System.Drawing.SizeF(326.2083F, 18F);
            this.xrDB_MovementDirection.StylePriority.UseFont = false;
            this.xrDB_MovementDirection.StylePriority.UseTextAlignment = false;
            this.xrDB_MovementDirection.Text = "xrDB_MovementDirection";
            this.xrDB_MovementDirection.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrDB_StatusNotes
            // 
            this.xrDB_StatusNotes.Dpi = 100F;
            this.xrDB_StatusNotes.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDB_StatusNotes.LocationFloat = new DevExpress.Utils.PointFloat(273.9583F, 0F);
            this.xrDB_StatusNotes.Name = "xrDB_StatusNotes";
            this.xrDB_StatusNotes.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDB_StatusNotes.SizeF = new System.Drawing.SizeF(335.8334F, 18F);
            this.xrDB_StatusNotes.StylePriority.UseFont = false;
            this.xrDB_StatusNotes.StylePriority.UseTextAlignment = false;
            this.xrDB_StatusNotes.Text = "xrDB_StatusNotes";
            this.xrDB_StatusNotes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLblLocation
            // 
            this.xrLblLocation.Dpi = 100F;
            this.xrLblLocation.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblLocation.LocationFloat = new DevExpress.Utils.PointFloat(123.9583F, 0F);
            this.xrLblLocation.Name = "xrLblLocation";
            this.xrLblLocation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblLocation.SizeF = new System.Drawing.SizeF(150F, 18F);
            this.xrLblLocation.StylePriority.UseFont = false;
            this.xrLblLocation.StylePriority.UseTextAlignment = false;
            this.xrLblLocation.Text = "xrLblLocation";
            this.xrLblLocation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrDB_StatusDate
            // 
            this.xrDB_StatusDate.Dpi = 100F;
            this.xrDB_StatusDate.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDB_StatusDate.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrDB_StatusDate.Name = "xrDB_StatusDate";
            this.xrDB_StatusDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDB_StatusDate.SizeF = new System.Drawing.SizeF(123.9583F, 18F);
            this.xrDB_StatusDate.StylePriority.UseFont = false;
            this.xrDB_StatusDate.StylePriority.UseTextAlignment = false;
            this.xrDB_StatusDate.Text = "xrDB_StatusDate";
            this.xrDB_StatusDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.BackColor = System.Drawing.Color.LightGray;
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 23.95833F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseBackColor = false;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 18F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(936F, 4.000001F);
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(609.7917F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(326.2083F, 18F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Movement";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(273.9583F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(335.8334F, 18F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Status";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(123.9583F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(150F, 18F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Scanned Location";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(123.9583F, 18F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Scanned Time";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ConsKey
            // 
            this.ConsKey.Description = "ConsKey";
            this.ConsKey.Name = "ConsKey";
            this.ConsKey.Visible = false;
            // 
            // LineNo
            // 
            this.LineNo.Description = "LineNo";
            this.LineNo.Name = "LineNo";
            this.LineNo.Type = typeof(int);
            this.LineNo.ValueInfo = "0";
            this.LineNo.Visible = false;
            // 
            // StatusDetailRpt
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.detailBand_StatusDet,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margins = new System.Drawing.Printing.Margins(100, 64, 0, 0);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ConsKey,
            this.LineNo});
            this.Version = "15.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.StatusDetailRpt_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand detailBand_StatusDet;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrDB_MovementDirection;
        private DevExpress.XtraReports.UI.XRLabel xrDB_StatusNotes;
        private DevExpress.XtraReports.UI.XRLabel xrLblLocation;
        private DevExpress.XtraReports.UI.XRLabel xrDB_StatusDate;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        public DevExpress.XtraReports.Parameters.Parameter ConsKey;
        public DevExpress.XtraReports.Parameters.Parameter LineNo;
    }
}
