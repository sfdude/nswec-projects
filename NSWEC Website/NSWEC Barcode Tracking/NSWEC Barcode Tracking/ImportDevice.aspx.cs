﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class ImportDevice : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "ImportDevice.aspx";
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            //if (!(loginUser != null && loginUser.SpecialUser))
            if (!(loginUser != null && loginUser.IsAdmin))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("ID_Device");

                Session.Remove("ID_IMPORT_FILE_LIST");

                AssignClientSideScripts();

                pcImport.ShowTabs = false;
                ECWebClass.SetMenuStyle(menuAction);
                ECWebClass.SetRoundPanelStyle(rpFileContainer);

                menuAction.Items[0].ClientEnabled = false;
                cbSlaveDBUpdate.Checked = true;
            }
        }
        private void AssignClientSideScripts()
        {
            menuAction.ClientSideEvents.ItemClick = @"function(s, e) { 
                e.processOnServer = false; 
                if (e.item.name == 'itmImport') { 
                    e.item.SetText('Wait...'); 
                    e.item.SetEnabled(false); 
                    cbImport.PerformCallback(fileList.GetItemCount());  
                } else
                if (e.item.name == 'itmUpdateSlave') { 
                    e.item.SetText('Wait...'); 
                    e.item.SetEnabled(false); 
                    cbUpdateSlave.PerformCallback('Update');  
                }
            } ";

            cbUpdateSlave.ClientSideEvents.CallbackComplete = @"function(s, e) {
                if (e.result != null && e.result != '')
                    alert(e.result);
                var itmUpdateSlave = menuAction.GetItemByName('itmUpdateSlave');
                if (itmUpdateSlave != null) { 
                    itmUpdateSlave.SetText('Update Slave'); 
                    itmUpdateSlave.SetEnabled(true); 
                } 
            } ";

            cbImport.ClientSideEvents.CallbackComplete = @"function(s, e) {
                if (e.result != null && e.result != '')
                    alert(e.result);
                fileList.ClearItems();
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null) { 
                    itmImport.SetText('Import'); 
                    itmImport.SetEnabled(false); 
                } 
            } ";

            ucSource.ClientSideEvents.FilesUploadStart = @"function(s, e) {
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null)
                    itmImport.SetEnabled(false);
                fileList.ClearItems();
                }";
            ucSource.ClientSideEvents.FilesUploadComplete = @"function(s, e) {
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null)
                    itmImport.SetEnabled(fileList.GetItemCount() > 0);
                }";
            ucSource.ClientSideEvents.FileUploadComplete = @"function(s, e) { 
                var itmImport = menuAction.GetItemByName('itmImport');
                if (itmImport != null) { 
                    itmImport.SetText('Import'); 
                    itmImport.SetEnabled(true); 
                }
                if (e.callbackData != '') {
                    if (e.callbackData.indexOf('Error') > -1) {
                        alert(e.callbackData); 
                        return;
                    }
                    var idx = 0;
                    if (fileList.GetItemCount() > 0)
                        idx = fileList.GetItemCount();
                    fileList.InsertItem(idx, e.callbackData);
                }
            } ";

        }
        protected void cbImport_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                int fileCount = ECWebClass.StrToIntDef(e.Parameter, 0);
                string fileListStr;
                if (Session["ID_IMPORT_FILE_LIST"] != null && Session["ID_IMPORT_FILE_LIST"].ToString() != string.Empty)
                    fileListStr = Session["ID_IMPORT_FILE_LIST"].ToString();
                else
                    fileListStr = string.Empty;

                Session.Remove("ID_IMPORT_FILE_LIST");

                string[] fileLists = fileListStr.Split('|');
                if (fileCount == 1 && fileCount == fileLists.Length)
                {
                    string procMsg = string.Empty;
                    ECWebClass.ImportDeviceFile(loginUser, fileLists[0], cbSlaveDBUpdate.Checked, ref procMsg);
                    e.Result = procMsg;                    
                }
                else
                    e.Result = "No file to import";
            }
        }
        protected void ucSource_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            e.CallbackData = "Error: Uploaded device data file is invalid";
            if (e.IsValid)
            {
                try
                {
                    string confFolder = MapPath(ConfigurationManager.AppSettings["ImportFolder"]);
                    if (!System.IO.Directory.Exists(confFolder))
                        System.IO.Directory.CreateDirectory(confFolder);
                    
                    string importFileName = string.Format("{0}\\{1}", confFolder, e.UploadedFile.FileName);
                    e.UploadedFile.SaveAs(importFileName, true);

                    if (Session["ID_IMPORT_FILE_LIST"] != null && Session["ID_IMPORT_FILE_LIST"].ToString() != string.Empty)
                        Session["ID_IMPORT_FILE_LIST"] += string.Format("|{0}", importFileName);
                    else
                        Session["ID_IMPORT_FILE_LIST"] = importFileName;

                    e.CallbackData = e.UploadedFile.FileName;
                }
                catch (Exception ex)
                {
                    e.CallbackData = string.Format("Error in File Upload: {0}", ex.Message);
                }
            }
        }

        protected void cbUpdateSlave_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || !loginUser.IsAdmin)
            {
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                return;
            }
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string procMsg = "Slave DB has been updated successfully";
                ECWebClass.ImportDeviceTableData(ref procMsg);
                e.Result = procMsg;
            }
        }
    }
}