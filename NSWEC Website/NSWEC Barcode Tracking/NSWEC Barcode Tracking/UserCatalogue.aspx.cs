﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using NSWEC.Net;

namespace NSWEC_Barcode_Tracking
{
    public partial class UserCatalogue : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            ECWebClass.setPageTheme(this.Page);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ID = "UserCatalogue.aspx";
            /*
            if (!ECSecurityClass.CheckQueryString(this.Page, "loginId"))
                Response.Redirect("ConsignmentCatalogue.aspx");
             */
            string tgtLogin = string.Empty;
            if (!ECSecurityClass.CheckQueryString(this.Page, "loginId", ref tgtLogin))
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser == null || string.Compare(loginUser.UserName, tgtLogin, true) != 0)
            {
                if (Page.IsCallback)
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback("Default.aspx?Logout=1");
                else
                    Response.Redirect("Default.aspx?Logout=1");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Remove("UC_LoginCatalogueTable");

                ASPxButton btnAdd = pcUser.FindControl("btnAdd") as ASPxButton;
                if (btnAdd != null)
                    btnAdd.ClientSideEvents.Click = "function(s, e) { " +
                        "window.location = 'UserSetup.aspx?qs=" + ECSecurityClass.GetQSEncripted("loginId=dummy") + "'; }";

                grdUser.ClientSideEvents.CustomButtonClick = @"function(s, e) { 
                    var btnId = e.buttonID; var id = s.GetRowKey(e.visibleIndex); 
                    if (btnId == 'btnEdit') { 
                        if (cbUser.InCallback()) return; 
                        cbUser.PerformCallback(btnId+'|'+id);
                    }} ";

                cbUser.ClientSideEvents.CallbackComplete = @"function(s, e) {
                    if (e.result != null && e.result != '')
                        window.location = e.result; }";

                pcUser.ShowTabs = false;
                ECWebClass.SetGridViewStyle(grdUser);
            }
            LoadLoginCatalogueData();
        }
        private DataTable GetLoginCatalogueDataTable()
        {
            DataTable dtLogin = Session["UC_LoginCatalogueTable"] as DataTable;
            LoginUser loginUser = Session["EC_User"] as LoginUser;
            if (loginUser != null && dtLogin == null)
            {
                dtLogin = ECSecurityClass.GetLoginCatalogueDataTable(loginUser.CDID);
                dtLogin.PrimaryKey = new DataColumn[] { dtLogin.Columns["ID"] };
                Session["UC_LoginCatalogueTable"] = dtLogin;
            }
            return dtLogin;
        }
        private void LoadLoginCatalogueData()
        {
            grdUser.DataSource = GetLoginCatalogueDataTable();
            grdUser.DataSourceID = String.Empty;
            grdUser.DataBind();
        }
        protected void grdView_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Update ||
                e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Cancel)
                e.Visible = false;
        }
        protected void cbUser_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            e.Result = string.Empty;
            if (e.Parameter != null && e.Parameter != string.Empty)
            {
                DataTable dtLogin = GetLoginCatalogueDataTable();
                string[] cbInfo = e.Parameter.Split('|');
                DataRow drLogin = dtLogin.Rows.Find(cbInfo[1]);
                if (drLogin != null)
                {
                    e.Result = "UserSetup.aspx?qs=" + ECSecurityClass.GetQSEncripted("loginId=" + drLogin["LoginId"]);
                }                
            }
        }

        protected void grdUser_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Login_Id")
            {
                //e.Value = System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(string.Format("{0}",e.GetListSourceFieldValue("LoginId")), true);
                e.Value = string.Format("{0}", e.GetListSourceFieldValue("LoginId"));
            }
            else
            if (e.Column.FieldName == "Contact1")
            {
                //e.Value = System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(string.Format("{0}", e.GetListSourceFieldValue("Contact")), true);
                e.Value = string.Format("{0}", e.GetListSourceFieldValue("Contact"));
            }
        }
    }
}