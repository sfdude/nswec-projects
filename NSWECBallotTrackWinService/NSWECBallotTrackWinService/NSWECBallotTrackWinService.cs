﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using DBTransactionUpdate;

namespace NSWECBallotTrackWinService
{
    public partial class NSWECBallotTrackWinService : ServiceBase
    {
        private System.Timers.Timer _timer;
        private EventLog srvEventLog;
        private readonly static string _serviceName = "NSWECBallotTrackWinService";
        private readonly static string _logSrc = "NSWECBallotTrackWinServiceLogSrc";
        private readonly static string _logName = "NSWECBallotTrackWinServiceLog";
        private readonly static string _srcFileDir = "Source";
        private readonly static string _bakFileDir = "Backup";
        private readonly static string _errFileDir = "Error";
        private readonly static string dateTimeFormat = "dd/MM/yyyy HH:mm:ss:fff";

        private static int _srvInterval = 30;
        private static string _dbConnStr = string.Empty;
        private static string _pdbConnStr = string.Empty;
        private static string _procErr = string.Empty;
        private static bool _backupProcessedFile = false;

        public NSWECBallotTrackWinService()
        {
            InitializeComponent();

            if (!System.Diagnostics.EventLog.SourceExists(_logSrc))
                System.Diagnostics.EventLog.CreateEventSource(_logSrc, _logName);

            srvEventLog = new EventLog();
            srvEventLog.Source = _logSrc;
            srvEventLog.Log = _logName;

            try
            {
                string processMsg = string.Empty;
                if (!CheckAppSettingsFile(ref processMsg))
                {
                    if (!string.IsNullOrEmpty(processMsg))
                        srvEventLog.WriteEntry(processMsg);
                    else
                        srvEventLog.WriteEntry("CheckAppSettingsFile failed");
                    return;
                }

                _timer = new System.Timers.Timer(_srvInterval * 1000);
                _timer.AutoReset = false;
                _timer.Elapsed += _timer_Elapsed;
            }
            catch (Exception ex)
            {
                srvEventLog.WriteEntry(string.Format("{0}: Error: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ex.Message));
            }
        }

        private static bool CheckAppSettingsFile(ref string errormsg)
        {
            bool result = false;
            try
            {
                var dbConnStr = ConfigurationManager.ConnectionStrings["DefaultDBConString"];
                result = dbConnStr != null && !string.IsNullOrEmpty(dbConnStr.ToString());
                if (!result)
                {
                    errormsg = @"Database connection is missing in app.config";
                    return result;
                }

                dbConnStr = ConfigurationManager.ConnectionStrings["SourceDBConString"];
                result = dbConnStr != null && !string.IsNullOrEmpty(dbConnStr.ToString());
                if (!result)
                {
                    errormsg = @"Primary/Source Database connection is missing in app.config";
                    return result;
                }

                var fileSourceDir = ConfigurationManager.AppSettings["FileSourceDir"];
                result = fileSourceDir != null && Directory.Exists(string.Format("{0}", fileSourceDir).Trim());
                if (!result)
                {
                    errormsg = @"Data file source directory is not defined properly in app.config";
                    return result;
                }

                _dbConnStr = ConfigurationManager.ConnectionStrings["DefaultDBConString"].ToString();
                _pdbConnStr = ConfigurationManager.ConnectionStrings["SourceDBConString"].ToString();

                var srvInterval = ConfigurationManager.AppSettings["ServiceCallInterval"];
                if (srvInterval != null && !string.IsNullOrEmpty(srvInterval))
                    _srvInterval = Convert.ToInt32(srvInterval);

                var backupProcessedFile = ConfigurationManager.AppSettings["BackupProcessedFile"];
                _backupProcessedFile = backupProcessedFile != null && !string.IsNullOrEmpty(backupProcessedFile) && Convert.ToBoolean(backupProcessedFile);
                
            }
            catch (Exception ex)
            {
                result = false;
                errormsg = string.Format("Error in CheckAppSettingsFile: {0}", ex.Message);
            }

            return result;
        }

        void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ProcessDataFiles();
        }

        protected override void OnStart(string[] args)
        {
            srvEventLog.WriteEntry(string.Format("{0}: {1} started", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), _serviceName));
            ProcessDataFiles();
        }

        protected override void OnStop()
        {
            srvEventLog.WriteEntry(string.Format("{0}: {1} stopped", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), _serviceName));
            if (_timer.Enabled)
                _timer.Stop();
        }

        protected override void OnContinue()
        {
            srvEventLog.WriteEntry(string.Format("{0}: {1} continued", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), _serviceName));
        }

        private void ProcessDataFiles()
        {
            try
            {
                if (_timer.Enabled)
                    _timer.Stop();

                WriteTransFilesToSlaveDB();
            }
            catch (Exception ex)
            {
                srvEventLog.WriteEntry(string.Format("{0}: Error: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ex.Message));
                if (!_timer.Enabled)
                    _timer.Start();
            }
            finally
            {
                if (!_timer.Enabled)
                    _timer.Start();
            }
        }

        private void WriteTransFilesToSlaveDB()
        {
            try
            {
                string srcFileDir = _srcFileDir;
                string bakFileDir = _bakFileDir;
                string errFileDir = _errFileDir;
                string fileSrcDir = ConfigurationManager.AppSettings["FileSourceDir"];
                                
                srcFileDir = string.Format("{0}{1}\\", AppDomain.CurrentDomain.BaseDirectory, srcFileDir);
                bakFileDir = string.Format("{0}{1}\\", AppDomain.CurrentDomain.BaseDirectory, bakFileDir);
                errFileDir = string.Format("{0}{1}\\", AppDomain.CurrentDomain.BaseDirectory, errFileDir);
                
                if (!Directory.Exists(srcFileDir))
                    Directory.CreateDirectory(srcFileDir);
                if (!Directory.Exists(bakFileDir))
                    Directory.CreateDirectory(bakFileDir);
                if (!Directory.Exists(errFileDir))
                    Directory.CreateDirectory(errFileDir);

                if (!Directory.Exists(fileSrcDir))
                    return;

                // SlaveDBUpdate text file: username|True/False

                var sUdtFile = Directory.EnumerateFiles(fileSrcDir, "SlaveDBUpdate_*.txt", SearchOption.TopDirectoryOnly);//.Where(s => s.ToLower().StartsWith("slavedbupdate_"));
                if (sUdtFile != null && sUdtFile.Count() > 0)
                {
                    foreach (var srcFile in sUdtFile)
                    {
                        if (File.Exists(srcFile))
                        {
                            string fileContent = File.ReadAllText(srcFile, Encoding.UTF8);
                            if (!string.IsNullOrWhiteSpace(fileContent))
                            {
                                string[] fContents = fileContent.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                bool versionUpdate = (fContents != null && fContents.Length > 1 && fContents[1].ToLower() == "true") ? true : false;
                                bool appendMode = (fContents != null && fContents.Length > 2 && fContents[2].ToLower() == "true") ? true : false;
                               
                                if (!SGEImportTableData(_dbConnStr, appendMode))
                                {
                                    if (File.Exists(errFileDir + Path.GetFileName(srcFile)))
                                        File.Delete(errFileDir + Path.GetFileName(srcFile));
                                    File.Move(srcFile, errFileDir + Path.GetFileName(srcFile));
                                }
                                else
                                {
                                    var dbConnStrObj = ConfigurationManager.ConnectionStrings["DefaultDBConString1"];
                                    if (dbConnStrObj != null && !string.IsNullOrEmpty(dbConnStrObj.ToString()))
                                    {
                                        // third db server if any
                                        SGEImportTableData(dbConnStrObj.ToString(), appendMode);
                                    }
                                    string procMsg = string.Empty;
                                    if (!DeleteTableData("NSW_Consignment", ref procMsg))
                                        srvEventLog.WriteEntry(string.Format("{0}: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), procMsg));  
                                    if (File.Exists(bakFileDir + Path.GetFileName(srcFile)))
                                        File.Delete(bakFileDir + Path.GetFileName(srcFile));
                                    File.Move(srcFile, bakFileDir + Path.GetFileName(srcFile));
                                }
                            }
                            else
                            {
                                if (File.Exists(bakFileDir + Path.GetFileName(srcFile)))
                                    File.Delete(bakFileDir + Path.GetFileName(srcFile));
                                File.Move(srcFile, bakFileDir + Path.GetFileName(srcFile));
                            }
                        }
                    }
                    return;
                }

                var srcFiles = Directory.EnumerateFiles(fileSrcDir, "*.*", SearchOption.TopDirectoryOnly).Where(s => s.ToLower().EndsWith(".json"));
                if (srcFiles != null && srcFiles.Count() > 0)
                {
                    foreach (var srcFile in srcFiles)
                    {
                        if (File.Exists(srcFile))
                        {
                            if (File.Exists(srcFileDir + Path.GetFileName(srcFile)))
                                File.Delete(srcFileDir + Path.GetFileName(srcFile));
                            File.Move(srcFile, srcFileDir + Path.GetFileName(srcFile));
                        }
                    }
                }
                DirectoryInfo dirInfo = new DirectoryInfo(srcFileDir);
                FileSystemInfo[] allFiles = dirInfo.GetFileSystemInfos();
                var orderedFiles = allFiles.OrderBy(f => f.Name);
                foreach (FileSystemInfo fInfo in orderedFiles)
                {
                    if (File.Exists(fInfo.FullName))
                    {
                        string jsonStr = File.ReadAllText(fInfo.FullName);
                        var slaveDBUpdate = JsonConvert.DeserializeObject<DBUpdate>(jsonStr);
                        if (slaveDBUpdate != null)
                        {
                            DBUpdate sDBUpdate = slaveDBUpdate as DBUpdate;
                            //sDBUpdate.DBConn = _dbConnStr;
                            if (!WriteTransFileToSlaveDB(sDBUpdate))
                            {
                                /**/
                                if (File.Exists(errFileDir + Path.GetFileName(fInfo.FullName)))
                                    File.Delete(errFileDir + Path.GetFileName(fInfo.FullName));
                                File.Move(fInfo.FullName, errFileDir + Path.GetFileName(fInfo.FullName));
                                 
                                //break;
                            }
                            else
                            {
                                if (_backupProcessedFile)
                                {
                                    if (File.Exists(bakFileDir + Path.GetFileName(fInfo.FullName)))
                                        File.Delete(bakFileDir + Path.GetFileName(fInfo.FullName));
                                    File.Move(fInfo.FullName, bakFileDir + Path.GetFileName(fInfo.FullName));
                                }
                                else
                                    File.Delete(fInfo.FullName);
                            }
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                srvEventLog.WriteEntry(string.Format("{0}: Error in WriteTransFilesToSlaveDB: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ex.Message));               
            }
        }

        private bool UpdateSlaveDB(DBUpdate dbUpdate, string dbConnStr)
        {
            bool result = false;
            using (SqlConnection sqlConn = new SqlConnection(dbConnStr))
            {
                sqlConn.Open();
                SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                try
                {
                    foreach (DBTransaction dbTrans in dbUpdate.DBTransactions)
                    {
                        if (dbTrans.SQLStatement != null && !string.IsNullOrEmpty(dbTrans.SQLStatement) &&
                            dbTrans.SQLParams != null && dbTrans.SQLParams.Length > 0)
                        {
                            using (SqlCommand cmd = new SqlCommand(dbTrans.SQLStatement, sqlTrans.Connection, sqlTrans))
                            {
                                foreach (DBTransactionParam param in dbTrans.SQLParams)
                                {
                                    if (param.ParamValue != null && !string.IsNullOrEmpty(param.ParamValue.ToString()))
                                    {
                                        if (param.ParamType == DbType.DateTime || param.ParamType == DbType.DateTime2)
                                            cmd.Parameters.AddWithValue(param.ParamName, DateTime.ParseExact(param.ParamValue.ToString(), dateTimeFormat, CultureInfo.InvariantCulture));
                                        else
                                            cmd.Parameters.AddWithValue(param.ParamName, param.ParamValue);
                                    }
                                    else
                                        cmd.Parameters.AddWithValue(param.ParamName, DBNull.Value);
                                }
                                result = cmd.ExecuteNonQuery() > 0;
                                if (!result)
                                    break;
                            }
                        }
                    }
                    if (result)
                        sqlTrans.Commit();
                    else
                        sqlTrans.Rollback();
                }
                catch (Exception ex)
                {
                    result = false;
                    sqlTrans.Rollback();
                    if (string.Compare(_procErr, ex.Message, true) != 0)
                    {
                        _procErr = ex.Message;
                        srvEventLog.WriteEntry(string.Format("{0}: Error in UpdateSlaveDB: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), _procErr));
                    }
                }
            }
            return result;
        }

        private bool WriteTransFileToSlaveDB(DBUpdate dbUpdate)
        {
            bool result = false;
            try
            {
                result = UpdateSlaveDB(dbUpdate, _dbConnStr);
                if (result)
                {
                    var dbConnStrObj = ConfigurationManager.ConnectionStrings["DefaultDBConString1"];
                    if (dbConnStrObj != null && !string.IsNullOrEmpty(dbConnStrObj.ToString()))
                    {
                        // third db server if any
                        result = UpdateSlaveDB(dbUpdate, dbConnStrObj.ToString());
                    }
                    //else
                    //    srvEventLog.WriteEntry("End of operation");
                }
            }
            catch (Exception ex)
            {
                result = false;
                if (string.Compare(_procErr, ex.Message, true) != 0)
                {
                    _procErr = ex.Message;
                    srvEventLog.WriteEntry(string.Format("{0}: Error in WriteTransFileToSlaveDB: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), _procErr));
                }
            }
            return result;
        }

        private static bool ImportSGETableData(SqlTransaction sqlTrans, DataTable dtSrc, ref string procMsg)
        {
            bool result = false;
            string sqlStr = null;
            if (dtSrc.Columns.IndexOf("ID") > -1)
                sqlStr = string.Format("SELECT * FROM {0} WHERE ID IS NULL", dtSrc.TableName);
            else
                if (dtSrc.Columns.IndexOf("ConsKey") > -1)
                    sqlStr = string.Format("SELECT * FROM {0} WHERE ConsKey IS NULL", dtSrc.TableName);
            result = !string.IsNullOrEmpty(sqlStr);
            if (!result)
            {
                procMsg = string.Format("Primary key is not found in table '{0}'", dtSrc.TableName);
            }
            else
            {
                SqlCommand cmd = null;
                SqlDataAdapter sqlDataAdapter = null;
                SqlCommandBuilder sqlCommandBuilder = null;
                foreach (DataRow drSrc in dtSrc.Rows)
                {
                    try
                    {
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            using (sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                using (sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                {
                                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                    sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                    sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                    DataTable dtData = new DataTable();
                                    sqlDataAdapter.Fill(dtData);

                                    DataRow drData = dtData.NewRow();
                                    foreach (DataColumn col in dtSrc.Columns)
                                    {
                                        drData[col.ColumnName] = drSrc[col.ColumnName];
                                    }
                                    dtData.Rows.Add(drData);
                                    result = sqlDataAdapter.Update(dtData) > 0;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in ImportSGETableData for table '{0}': {1}", dtSrc.TableName, ex.Message);
                    }
                    if (!result)
                        break;
                }
            }
            return result;
        }

        private static bool BulkCopySGETableData(SqlTransaction sqlTrans, string tableName, ref string procMsg)
        {
            bool result = false;
            using (SqlConnection sqlSrcConn = new SqlConnection(_pdbConnStr))
            {
                sqlSrcConn.Open();
                using (SqlCommand cmd = new SqlCommand(string.Format("SELECT * FROM {0}", tableName), sqlSrcConn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlTrans.Connection, SqlBulkCopyOptions.Default, sqlTrans))
                        {
                            bulkCopy.DestinationTableName = tableName;
                            bulkCopy.WriteToServer(reader);
                        }
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        procMsg = string.Format("Error in BulkCopySGETableData for table '{0}': {1}", tableName, ex.Message);
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                
            }
            return result;
        }

        private bool SGEImportTableData(string dbConnStr, bool appendMode)
        {
            bool result = false;
            string procMsg = string.Empty;
            try
            {
                srvEventLog.WriteEntry("SGEImportTableData Started");
                using (SqlConnection sqlConn = new SqlConnection(dbConnStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        DataTable tgtSGETable;
                        if (appendMode)
                        {
                            for (int i = SGETableList1.Length - 1; i >= 0; i--)
                            {
                                result = DeleteTableData(sqlTrans, SGETableList1[i], ref procMsg);
                                if (!result)
                                    break;
                            }
                            if (result)
                            {
                                for (int i = 0; i < SGETableList1.Length; i++)
                                {
                                    result = BulkCopySGETableData(sqlTrans, SGETableList1[i], ref procMsg);
                                    if (!result)
                                        break;
                                }
                                if (result)
                                {
                                    tgtSGETable = GetSQLData("select T.* from NSW_Connote T inner join NSW_Consignment C on C.ID=T.ConsKey");
                                    if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                    {
                                        tgtSGETable.TableName = "NSW_Connote";
                                        result = ImportSGETableData(sqlTrans, tgtSGETable, ref procMsg);
                                        if (result)
                                        {
                                            tgtSGETable = GetSQLData("select T.* from NSW_Container T inner join dbo.NSW_Consignment C on C.ID=T.ConsKey");
                                            if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                            {
                                                tgtSGETable.TableName = "NSW_Container";
                                                result = ImportSGETableData(sqlTrans, tgtSGETable, ref procMsg);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            for (int i = SGETableList.Length - 1; i >= 0; i--)
                            {
                                result = DeleteTableData(sqlTrans, SGETableList[i], ref procMsg);
                                if (!result)
                                    break;
                            }
                            if (result)
                            {
                                for (int i = 0; i < SGETableList.Length; i++)
                                {
                                    result = BulkCopySGETableData(sqlTrans, SGETableList[i], ref procMsg);
                                    if (!result)
                                        break;
                                }                                
                            }
                        }
                        if (result)
                        {
                            tgtSGETable = GetSGETableData("NSW_Application");
                            if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                result = UpdatePPDMVersion(sqlTrans, tgtSGETable.Rows[0], ref procMsg);
                            if (result)
                            {
                                result = DeleteTableData(sqlTrans, "NSW_Consignment", ref procMsg);
                                //result = DeleteTableData("NSW_Consignment", ref procMsg);
                            }
                        }

                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();

                        if (!string.IsNullOrEmpty(procMsg))
                            srvEventLog.WriteEntry(procMsg);
                        else
                            srvEventLog.WriteEntry("SGEImportTableData Ended");
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        sqlTrans.Rollback();
                        srvEventLog.WriteEntry(string.Format("Error in SGEImportTableData: {0}", ex.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                srvEventLog.WriteEntry(string.Format("Error in SGEImportTableData: {0}", ex.Message));
            }
            return result;
        }
        /*
        private bool SGEImportTableData(bool versionUpdate)
        {   
            bool result = false;
            string procMsg = string.Empty;
            try
            {
                srvEventLog.WriteEntry("SGEImportTableData Started");
                using (SqlConnection sqlConn = new SqlConnection(_dbConnStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        for (int i = SGETableList.Length - 1; i >= 0; i--)
                        {
                            result = DeleteTableData(sqlTrans, SGETableList[i], ref procMsg);
                            if (!result)
                                break;
                        }
                        if (result)
                        {
                            DataTable tgtSGETable;
                            for (int i = 0; i < SGETableList.Length; i++)
                            {
                                tgtSGETable = GetSGETableData(SGETableList[i]);
                                if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                {
                                    result = ImportSGETableData(sqlTrans, tgtSGETable, ref procMsg);
                                    if (!result)
                                        break;
                                }
                            }
                            if (result)
                            {
                                tgtSGETable = GetSGETableData("NSW_Application");
                                if (tgtSGETable != null && tgtSGETable.Rows.Count > 0)
                                    result = UpdatePPDMVersion(sqlTrans, tgtSGETable.Rows[0], ref procMsg);
                            }
                            if (result)
                                sqlTrans.Commit();
                            else
                                sqlTrans.Rollback();
                        }
                        if (!string.IsNullOrEmpty(procMsg))
                            srvEventLog.WriteEntry(procMsg);
                        else
                            srvEventLog.WriteEntry("SGEImportTableData Ended");
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        sqlTrans.Rollback();
                        srvEventLog.WriteEntry(string.Format("Error in SGEImportTableData: {0}", ex.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                srvEventLog.WriteEntry(string.Format("Error in SGEImportTableData: {0}", ex.Message));
            }
            return result;
        }
        */
        public static DataTable GetSQLData(string sql)
        {
            DataTable dt = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(_pdbConnStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }
        private static DataTable GetSGETableData(string tableName)
        {
            DataTable dt = new DataTable();
            dt.TableName = tableName;

            using (SqlConnection sqlConnection = new SqlConnection(_pdbConnStr))
            {
                using (SqlCommand sqlCommand = new SqlCommand(
                    string.Format("SELECT * FROM {0}", tableName), sqlConnection))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(dt);
                }
            }
            return dt;
        }
        private static bool UpdatePPDMVersion(SqlTransaction sqlTrans, DataRow drSrcApp, ref string procMsg)
        {
            bool result = false;
            try
            {
                string sqlStr = @"update NSW_Application set PPDMVersion=@PPDMVersion, PPDMLoaded=@PPDMLoaded";
                using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                {
                    cmd.Parameters.AddWithValue("@PPDMVersion", drSrcApp["PPDMVersion"]);
                    cmd.Parameters.AddWithValue("@PPDMLoaded", drSrcApp["PPDMLoaded"]);
                    result = cmd.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                procMsg = string.Format("Error in UpdatePPDMVersion: {0}", ex.Message);
            }
            return result;
        }
        private static bool DeleteTableData(SqlTransaction sqlTrans, string tableName, ref string procMsg)
        {
            bool result = false;
            try
            {
                try
                {
                    string sql = string.Format("delete from {0}", tableName);
                    using (SqlCommand cmd = new SqlCommand(sql, sqlTrans.Connection, sqlTrans))
                    {
                        result = cmd.ExecuteNonQuery() >= 0;
                    }
                }
                catch (SqlException ex)
                {
                    sqlTrans.Rollback();
                    procMsg = string.Format("Error in DeleteTableData for table '{0}': {1}", tableName, ex.Message);
                    result = false;
                }
            }
            catch (Exception ex)
            {
                procMsg = string.Format("Error in DeleteTableData for table '{0}': {1}", tableName, ex.Message);
            }
            return result;
        }
        public static bool DeleteTableData(string tableName, ref string procMsg)
        {
            bool result = false;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(_pdbConnStr))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        string sql = string.Format("delete from {0}", tableName);
                        using (SqlCommand cmd = new SqlCommand(sql, sqlTrans.Connection, sqlTrans))
                        {
                            result = cmd.ExecuteNonQuery() >= 0;
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (SqlException ex)
                    {
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in DeleteTableData for table '{0}': {1}", tableName, ex.Message);
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                procMsg = string.Format("Error in DeleteTableData for table '{0}': {1}", tableName, ex.Message);
            }
            return result;
        }
        private static readonly string[] SGETableList =
        {
            "NSW_BarcodeSource", "NSW_ReturningOffice", "NSW_LGA", "NSW_Contest", 
            "NSW_VenueType", "NSW_ContainerType", "NSW_StatusStage", "NSW_Location", "NSW_Venue",
            /*"NSW_Consignment",*/ "NSW_Connote", "NSW_Container", "NSW_ContainerStatus"
        };
        public static string[] SGETableList1 =
        {
            "NSW_BarcodeSource", "NSW_CountCentre", "NSW_ReturningOffice", "NSW_LGA", "NSW_Ward", "NSW_Contest", 
            "NSW_VenueType", "NSW_ContainerType", "NSW_StatusStage", "NSW_Location", "NSW_Venue"
            //"NSW_Consignment", "NSW_Connote", "NSW_Container", "NSW_ContainerStatus"
        };
    }
        
}