﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using DevExpress.Web;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;

namespace NSWECBarcodeTrackingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class BarcodeTrackingService : IBarcodeTrackingService
    {
        private static string dbConn = ConfigurationManager.ConnectionStrings["DefaultDBConString"].ToString();
        private string saveLabelFolder = string.Empty;
        private string serviceRootUrl = string.Empty;
        private bool returnLabelLink = false;
        private bool checkStageId = false;
        private string _saveRequestResponseFolder = string.Empty;

        ReturningOfficeEnquiryResponseType IBarcodeTrackingService.GetReturningOffice(ReturningOfficeEnquiryRequestType returningOfficeRequest)
        {
            ReturningOfficeEnquiryResponseType resp = new ReturningOfficeEnquiryResponseType();
            resp.DocumentType = returningOfficeRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(returningOfficeRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                {
                    DataTable dtRO = GetRODataTable(returningOfficeRequest.CDID);
                    if (dtRO != null && dtRO.Rows.Count > 0)
                    {
                        ReturningOffice ro = null;
                        List<ReturningOffice> roList = new List<ReturningOffice>();
                        foreach (DataRow dr in dtRO.Rows)
                        {
                            ro = new ReturningOffice();
                            ro.CDID = string.Format("{0}", dr["Code"]);
                            if (ro.CDID.Length > 2)
                                ro.CDID = ro.CDID.Substring(ro.CDID.Length - 2);
                            ro.Name = string.Format("{0}", dr["Name"]);
                            roList.Add(ro);
                        }
                        resp.ReturningOffices = roList.ToArray();
                        resp.Notes = string.Format("Total {0} Returning Office Records returned", dtRO.Rows.Count);
                    }
                    else
                        resp.Notes = "No Returning Office Record Found";
                }
            }
            catch(Exception ex)
            {
                resp.Notes = string.Format("Error in GetReturningOffice: {0}", ex.Message);
            }
            return resp; 
        }

        VenueEnquiryResponseType IBarcodeTrackingService.GetVenue(VenueEnquiryRequestType venueRequest)
        {
            VenueEnquiryResponseType resp = new VenueEnquiryResponseType();
            resp.DocumentType = venueRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(venueRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                {
                    DataTable dtVenue = GetVenueDataTable(venueRequest.CDID);
                    if (dtVenue != null && dtVenue.Rows.Count > 0)
                    {
                        Venue venue = null;
                        List<Venue> venueList = new List<Venue>();
                        foreach(DataRow dr in dtVenue.Rows)
                        {
                            venue = new Venue();
                            venue.Code = string.Format("{0}", dr["Code"]);
                            if (venue.Code.Length > 4)
                                venue.Code = venue.Code.Substring(venue.Code.Length - 4);
                            venue.Name = string.Format("{0}", dr["Name"]);
                            venue.ReturningOffice = new ReturningOffice();
                            venue.ReturningOffice.CDID = string.Format("{0}", dr["ROCode"]);
                            if (venue.ReturningOffice.CDID.Length > 2)
                                venue.ReturningOffice.CDID = venue.ReturningOffice.CDID.Substring(venue.ReturningOffice.CDID.Length - 2);
                            venue.ReturningOffice.Name = string.Format("{0}", dr["ROName"]);
                            venueList.Add(venue);
                        }
                        resp.Venues = venueList.ToArray();
                        resp.Notes = string.Format("Total {0} Venue Records returned", dtVenue.Rows.Count);
                    }
                    else
                        resp.Notes = "No Venue Record Found";
                }
            }
            catch(Exception ex)
            {
                resp.Notes = string.Format("Error in GetVenue: {0}", ex.Message);
            }
            return resp; 
        }

        VenueTypeEnquiryResponseType IBarcodeTrackingService.GetVenueType(VenueTypeEnquiryRequestType venueTypeRequest)
        {
            VenueTypeEnquiryResponseType resp = new VenueTypeEnquiryResponseType();
            resp.DocumentType = venueTypeRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(venueTypeRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                {
                    DataTable dtVenueType = GetVenueTypeDataTable(venueTypeRequest.Code);
                    if (dtVenueType != null && dtVenueType.Rows.Count > 0)
                    {
                        VenueType venueType = null;
                        List<VenueType> venueTypeList = new List<VenueType>();
                        foreach (DataRow dr in dtVenueType.Rows)
                        {
                            venueType = new VenueType();
                            venueType.Code = string.Format("{0}", dr["Code"]);
                            venueType.Name = string.Format("{0}", dr["Name"]);
                            venueTypeList.Add(venueType);
                        }
                        resp.VenueTypes = venueTypeList.ToArray();
                        resp.Notes = string.Format("Total {0} Venue Type Records returned", dtVenueType.Rows.Count);
                    }
                    else
                        resp.Notes = "No Venue Type Record Found";
                }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in GetVenueType: {0}", ex.Message);
            }
            return resp; 
        }

        ContestEnquiryResponseType IBarcodeTrackingService.GetContest(ContestEnquiryRequestType contestRequest)
        {
            ContestEnquiryResponseType resp = new ContestEnquiryResponseType();
            resp.DocumentType = contestRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(contestRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                {
                    DataTable dtContest = GetContestDataTable(contestRequest.Code);
                    if (dtContest != null && dtContest.Rows.Count > 0)
                    {
                        Contest contest = null;
                        List<Contest> contestList = new List<Contest>();
                        foreach (DataRow dr in dtContest.Rows)
                        {
                            contest = new Contest();
                            contest.Code = string.Format("{0}", dr["Code"]);
                            contest.Name = string.Format("{0}", dr["Name"]);
                            contestList.Add(contest);
                        }
                        resp.Contests = contestList.ToArray();
                        resp.Notes = string.Format("Total {0} Contest Records returned", dtContest.Rows.Count);
                    }
                    else
                        resp.Notes = "No Contest Record Found";
                }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in GetContest: {0}", ex.Message);
            }
            return resp; 
        }

        ContainerTypeEnquiryResponseType IBarcodeTrackingService.GetContainerType(ContainerTypeEnquiryRequestType containerTypeRequest)
        {
            ContainerTypeEnquiryResponseType resp = new ContainerTypeEnquiryResponseType();
            resp.DocumentType = containerTypeRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(containerTypeRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                {
                    DataTable dtContainerType = GetContainerTypeDataTable(containerTypeRequest.Code);
                    if (dtContainerType != null && dtContainerType.Rows.Count > 0)
                    {
                        ContainerType containerType = null;
                        List<ContainerType> containerTypeList = new List<ContainerType>();
                        foreach (DataRow dr in dtContainerType.Rows)
                        {
                            containerType = new ContainerType();
                            containerType.Code = string.Format("{0}", dr["Code"]);
                            containerType.Name = string.Format("{0}", dr["Name"]);
                            containerTypeList.Add(containerType);
                        }
                        resp.ContainerTypes = containerTypeList.ToArray();
                        resp.Notes = string.Format("Total {0} Container Type Records returned", dtContainerType.Rows.Count);
                    }
                    else
                        resp.Notes = "No Container Type Record Found";
                }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in GetContainerType: {0}", ex.Message);
            }
            return resp;
        }
        ContainerBarcodeEnquiryResponseType IBarcodeTrackingService.GetContainerBarcode(ContainerBarcodeEnquiryRequestType containerBarcodeRequest)
        {
            ContainerBarcodeEnquiryResponseType resp = new ContainerBarcodeEnquiryResponseType();
            resp.DocumentType = containerBarcodeRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(containerBarcodeRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                {                    
                    DataTable dtBarcode = GetContainerBarcodeDataTable(containerBarcodeRequest.CDID, containerBarcodeRequest.VenueCode, containerBarcodeRequest.Barcode,
                        containerBarcodeRequest.StageFlag);
                    if (dtBarcode != null && dtBarcode.Rows.Count > 0)
                    {
                        ContainerBarcode barcode = null;
                        List<ContainerBarcode> barcodeList = new List<ContainerBarcode>();
                        foreach(DataRow dr in dtBarcode.Rows)
                        {
                            barcode = new ContainerBarcode();
                            barcode.Barcode = string.Format("{0}", dr["Barcode"]);
                            barcode.ReturningOffice = new ReturningOffice();
                            barcode.ReturningOffice.CDID = string.Format("{0}", dr["CDID"]);
                            if (barcode.ReturningOffice.CDID.Length > 2)
                                barcode.ReturningOffice.CDID = barcode.ReturningOffice.CDID.Substring(barcode.ReturningOffice.CDID.Length - 2);
                            barcode.ReturningOffice.Name = string.Format("{0}", dr["ReturningOfficeName"]);

                            barcode.Venue = new Venue();
                            barcode.Venue.Code = string.Format("{0}", dr["VenueCode"]);
                            if (barcode.Venue.Code.Length > 4)
                                barcode.Venue.Code = barcode.Venue.Code.Substring(barcode.Venue.Code.Length - 4);
                            barcode.Venue.Name = string.Format("{0}", dr["VenueName"]);

                            barcode.VenueType = new VenueType();
                            barcode.VenueType.Code = string.Format("{0}", dr["VenueTypeCode"]);
                            barcode.VenueType.Name = string.Format("{0}", dr["VenueTypeName"]);

                            barcode.Contest = new Contest();
                            barcode.Contest.Code = string.Format("{0}", dr["ContestCode"]);
                            barcode.Contest.Name = string.Format("{0}", dr["ContestName"]);

                            barcode.ContainerType = new ContainerType();
                            barcode.ContainerType.Code = string.Format("{0}", dr["ContainerCode"]);
                            barcode.ContainerType.Name = string.Format("{0}", dr["ContainerName"]);

                            barcodeList.Add(barcode);
                        }
                        resp.ContainerBarcodes = barcodeList.ToArray();
                        resp.Notes = string.Format("Total {0} Container Barcode Records Returned", dtBarcode.Rows.Count);
                    }
                    else
                        resp.Notes = "No Container Barcode Record Found";
                }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in GetContainerBarcode: {0}", ex.Message);
            }
            return resp; 
        }

        ContainerBarcodeEnquiryResponseType IBarcodeTrackingService.GetContainerBarcodeLabel(ContainerBarcodeEnquiryRequestType containerBarcodeRequest)
        {
            returnLabelLink = ConfigurationManager.AppSettings["ReturnLabelLink"] != null &&
                !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ReturnLabelLink"]) &&
                string.Compare(ConfigurationManager.AppSettings["ReturnLabelLink"], "true", true) == 0;
            if (returnLabelLink)
            {
                saveLabelFolder = string.Format("{0}\\Labels", AppDomain.CurrentDomain.BaseDirectory);
                if (!System.IO.Directory.Exists(saveLabelFolder))
                    System.IO.Directory.CreateDirectory(saveLabelFolder);
                serviceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            }
            ContainerBarcodeEnquiryResponseType resp = new ContainerBarcodeEnquiryResponseType();
            resp.DocumentType = containerBarcodeRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(containerBarcodeRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                if (containerBarcodeRequest.Barcode == null || string.IsNullOrEmpty(containerBarcodeRequest.Barcode))
                    resp.Notes = "Barcode is not provided in the request";
                else
                {
                    DataTable dtBarcode = GetContainerBarcodeDataTable(containerBarcodeRequest.CDID, containerBarcodeRequest.VenueCode, containerBarcodeRequest.Barcode, null);
                    if (dtBarcode != null && dtBarcode.Rows.Count > 0)
                    {
                        ContainerBarcode barcode = null;
                        List<ContainerBarcode> barcodeList = new List<ContainerBarcode>();
                        foreach (DataRow dr in dtBarcode.Rows)
                        {
                            barcode = new ContainerBarcode();
                            barcode.Barcode = string.Format("{0}", dr["Barcode"]);
                            barcode.ReturningOffice = new ReturningOffice();
                            barcode.ReturningOffice.CDID = string.Format("{0}", dr["CDID"]);
                            if (barcode.ReturningOffice.CDID.Length > 2)
                                barcode.ReturningOffice.CDID = barcode.ReturningOffice.CDID.Substring(barcode.ReturningOffice.CDID.Length - 2);
                            barcode.ReturningOffice.Name = string.Format("{0}", dr["ReturningOfficeName"]);

                            barcode.Venue = new Venue();
                            barcode.Venue.Code = string.Format("{0}", dr["VenueCode"]);
                            if (barcode.Venue.Code.Length > 4)
                                barcode.Venue.Code = barcode.Venue.Code.Substring(barcode.Venue.Code.Length - 4);
                            barcode.Venue.Name = string.Format("{0}", dr["VenueName"]);

                            barcode.VenueType = new VenueType();
                            barcode.VenueType.Code = string.Format("{0}", dr["VenueTypeCode"]);
                            barcode.VenueType.Name = string.Format("{0}", dr["VenueTypeName"]);

                            barcode.Contest = new Contest();
                            barcode.Contest.Code = string.Format("{0}", dr["ContestCode"]);
                            barcode.Contest.Name = string.Format("{0}", dr["ContestName"]);

                            barcode.ContainerType = new ContainerType();
                            barcode.ContainerType.Code = string.Format("{0}", dr["ContainerCode"]);
                            barcode.ContainerType.Name = string.Format("{0}", dr["ContainerName"]);

                            string pdfLabelData = GetPdfLabelData(barcode.Barcode, saveLabelFolder);
                            if (!string.IsNullOrEmpty(saveLabelFolder) && !(string.IsNullOrEmpty(pdfLabelData)) && File.Exists(pdfLabelData))
                                pdfLabelData = string.Format("{0}{1}/{2}", serviceRootUrl, "Labels", Path.GetFileName(pdfLabelData));

                            barcode.Attachment = pdfLabelData;
                            barcodeList.Add(barcode);
                        }
                        resp.ContainerBarcodes = barcodeList.ToArray();
                        resp.Notes = string.Format("Total {0} Container Barcode Label Records Returned", dtBarcode.Rows.Count);
                    }
                    else
                        resp.Notes = "No Container Barcode Record Found";
                }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in GetContainerBarcodeLabel: {0}", ex.Message);
            }
            return resp;
        }
        ResponseMessageType IBarcodeTrackingService.TestWebService(RequestMessageType request)
        {
            ResponseMessageType resp = new ResponseMessageType();
            resp.DocumentType = request.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(request, ref procMsg))
                    resp.Notes = procMsg;
                else
                    resp.Notes = "Test Web Service is successful";
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in TestWebService: {0}", ex.Message);
            }
            return resp;
        }

        ContainerBarcodeEnquiryResponseType IBarcodeTrackingService.GenerateContainerBarcode(GenerateContainerBarcodeRequestType containerBarcodeRequest)
        {
            _saveRequestResponseFolder = string.Format("{0}//Upload", AppDomain.CurrentDomain.BaseDirectory);
            if (!System.IO.Directory.Exists(_saveRequestResponseFolder))
                System.IO.Directory.CreateDirectory(_saveRequestResponseFolder);

            XmlSerializer soapWriter = new XmlSerializer(typeof(GenerateContainerBarcodeRequestType));
            // Create a new file stream to write the serialized object to a file
            string reqFileName = string.Format("{0}\\GenerateBarcodeRequest_{1}.xml", _saveRequestResponseFolder, DateTime.Now.ToString("yyyyMMddHHmmss"));
            System.IO.FileStream reqFile = System.IO.File.Create(reqFileName);
            soapWriter.Serialize(reqFile, containerBarcodeRequest);
            // Cleanup
            reqFile.Close();

            returnLabelLink = ConfigurationManager.AppSettings["ReturnLabelLink"] != null &&
                !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ReturnLabelLink"]) &&
                string.Compare(ConfigurationManager.AppSettings["ReturnLabelLink"], "true", true) == 0;
            if (returnLabelLink)
            {
                saveLabelFolder = string.Format("{0}\\Labels", AppDomain.CurrentDomain.BaseDirectory);
                if (!System.IO.Directory.Exists(saveLabelFolder))
                    System.IO.Directory.CreateDirectory(saveLabelFolder);
                serviceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            }
            ContainerBarcodeEnquiryResponseType resp = new ContainerBarcodeEnquiryResponseType();
            resp.DocumentType = containerBarcodeRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(containerBarcodeRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                    if (containerBarcodeRequest.CDID == null || string.IsNullOrEmpty(containerBarcodeRequest.CDID))
                        resp.Notes = "Returning office's CDID is not provided in the request";
                    else
                        if (containerBarcodeRequest.VenueCode == null || string.IsNullOrEmpty(containerBarcodeRequest.VenueCode))
                            resp.Notes = "Venue Code is not provided in the request";
                        else
                            if (containerBarcodeRequest.VenueTypeCode == null || string.IsNullOrEmpty(containerBarcodeRequest.VenueTypeCode))
                                resp.Notes = "Venue Type is not provided in the request";
                            else
                                if (containerBarcodeRequest.ContestCode == null || string.IsNullOrEmpty(containerBarcodeRequest.ContestCode))
                                    resp.Notes = "Contest Code is not provided in the request";
                                else
                                    if (containerBarcodeRequest.ContainerCode == null || string.IsNullOrEmpty(containerBarcodeRequest.ContainerCode))
                                        resp.Notes = "Container Code is not provided in the request";
                                    else
                                        if (containerBarcodeRequest.ContainerQty == 0)
                                            resp.Notes = "Container Quantity is not provided in the request";
                    else
                    {
                        string barCode = string.Format("{0}{1}{2}{3}{4}", containerBarcodeRequest.CDID, containerBarcodeRequest.VenueCode,
                            containerBarcodeRequest.VenueTypeCode, containerBarcodeRequest.ContestCode, containerBarcodeRequest.ContainerCode);
                        List<string> barCodeList = GetImportContainerBarcodeData(containerBarcodeRequest, barCode, ref procMsg);
                        if (barCodeList != null && barCodeList.Count > 0)
                        {
                            string barcodeListStr = string.Join("|", barCodeList.ToArray());
                            DataTable dtBarcode = GetContainerBarcodeDataTable(containerBarcodeRequest.CDID, containerBarcodeRequest.VenueCode, barcodeListStr, null);
                            if (dtBarcode != null && dtBarcode.Rows.Count > 0)
                            {
                                ContainerBarcode barcode = null;
                                List<ContainerBarcode> barcodeList = new List<ContainerBarcode>();
                                foreach (DataRow dr in dtBarcode.Rows)
                                {
                                    barcode = new ContainerBarcode();
                                    barcode.Barcode = string.Format("{0}", dr["Barcode"]);
                                    barcode.ReturningOffice = new ReturningOffice();
                                    barcode.ReturningOffice.CDID = string.Format("{0}", dr["CDID"]);
                                    if (barcode.ReturningOffice.CDID.Length > 2)
                                        barcode.ReturningOffice.CDID = barcode.ReturningOffice.CDID.Substring(barcode.ReturningOffice.CDID.Length - 2);
                                    barcode.ReturningOffice.Name = string.Format("{0}", dr["ReturningOfficeName"]);

                                    barcode.Venue = new Venue();
                                    barcode.Venue.Code = string.Format("{0}", dr["VenueCode"]);
                                    if (barcode.Venue.Code.Length > 4)
                                        barcode.Venue.Code = barcode.Venue.Code.Substring(barcode.Venue.Code.Length - 4);
                                    barcode.Venue.Name = string.Format("{0}", dr["VenueName"]);

                                    barcode.VenueType = new VenueType();
                                    barcode.VenueType.Code = string.Format("{0}", dr["VenueTypeCode"]);
                                    barcode.VenueType.Name = string.Format("{0}", dr["VenueTypeName"]);

                                    barcode.Contest = new Contest();
                                    barcode.Contest.Code = string.Format("{0}", dr["ContestCode"]);
                                    barcode.Contest.Name = string.Format("{0}", dr["ContestName"]);

                                    barcode.ContainerType = new ContainerType();
                                    barcode.ContainerType.Code = string.Format("{0}", dr["ContainerCode"]);
                                    barcode.ContainerType.Name = string.Format("{0}", dr["ContainerName"]);

                                    string pdfLabelData = GetPdfLabelData(barcode.Barcode, saveLabelFolder);
                                    if (!string.IsNullOrEmpty(saveLabelFolder) && !(string.IsNullOrEmpty(pdfLabelData)) && File.Exists(pdfLabelData))
                                        pdfLabelData = string.Format("{0}{1}/{2}", serviceRootUrl, "Labels", Path.GetFileName(pdfLabelData));

                                    barcode.Attachment = pdfLabelData;
                                    barcodeList.Add(barcode);
                                }
                                resp.ContainerBarcodes = barcodeList.ToArray();
                                resp.Notes = string.Format("Total {0} Container Barcode Label Records Returned", dtBarcode.Rows.Count);
                            }
                            else
                                resp.Notes = "No Container Barcode Record Found";
                        }
                        else
                            if (!string.IsNullOrEmpty(procMsg))
                                resp.Notes = procMsg;
                            else
                                resp.Notes = "No container barcode created";

                    }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in GenerateContainerBarcode: {0}", ex.Message);
            }
            if (resp != null)
            {
                // record the response                            
                soapWriter = new XmlSerializer(typeof(ContainerBarcodeEnquiryResponseType));
                // Create a new file stream to write the serialized object to a file
                string respFileName = string.Format("{0}\\GenerateBarcodeResponse_{1}.xml", _saveRequestResponseFolder, DateTime.Now.ToString("yyyyMMddHHmmss"));
                System.IO.FileStream respFile = System.IO.File.Create(respFileName);
                soapWriter.Serialize(respFile, resp);
                // Cleanup
                respFile.Close();
            }
            return resp;
        }

        StatusStageEnquiryResponseType IBarcodeTrackingService.GetStatusStage(StatusStageEnquiryRequestType statusStageRequest)
        {
            StatusStageEnquiryResponseType resp = new StatusStageEnquiryResponseType();
            resp.DocumentType = statusStageRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(statusStageRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                {
                    DataTable dtStatusStage = GetStatusStageDataTable(statusStageRequest.StageId);
                    if (dtStatusStage != null && dtStatusStage.Rows.Count > 0)
                    {
                        StatusStage statusStage = null;
                        List<StatusStage> statusStageList = new List<StatusStage>();
                        foreach (DataRow dr in dtStatusStage.Rows)
                        {
                            statusStage = new StatusStage();
                            statusStage.StageId = string.Format("{0}", dr["StageId"]);
                            statusStage.Description = string.Format("{0}", dr["Description"]);
                            statusStageList.Add(statusStage);
                        }
                        resp.StatusStages = statusStageList.ToArray();
                        resp.Notes = string.Format("Total {0} Status Stage Records returned", dtStatusStage.Rows.Count);
                    }
                    else
                        resp.Notes = "No Status Stage Record Found";
                }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in GetStatusStage: {0}", ex.Message);
            }
            return resp; 
        }

        BarcodeScanStatusUpdateResponseType IBarcodeTrackingService.BarcodeScanStatusUpdate(BarcodeScanStatusUpdateRequsetType barcodeStatusUpdateRequest)
        {
            _saveRequestResponseFolder = string.Format("{0}//Upload", AppDomain.CurrentDomain.BaseDirectory);
            if (!System.IO.Directory.Exists(_saveRequestResponseFolder))
                System.IO.Directory.CreateDirectory(_saveRequestResponseFolder);

            XmlSerializer soapWriter = new XmlSerializer(typeof(BarcodeScanStatusUpdateRequsetType));
            // Create a new file stream to write the serialized object to a file
            string reqFileName = string.Format("{0}\\ScanStatusUpdateRequset_{1}.xml", _saveRequestResponseFolder, DateTime.Now.ToString("yyyyMMddHHmmss"));
            System.IO.FileStream reqFile = System.IO.File.Create(reqFileName);
            soapWriter.Serialize(reqFile, barcodeStatusUpdateRequest);
            // Cleanup
            reqFile.Close();

            checkStageId = ConfigurationManager.AppSettings["CheckStageId"] != null &&
                !string.IsNullOrEmpty(ConfigurationManager.AppSettings["CheckStageId"]) &&
                string.Compare(ConfigurationManager.AppSettings["CheckStageId"], "true", true) == 0;

            BarcodeScanStatusUpdateResponseType resp = new BarcodeScanStatusUpdateResponseType();
            resp.DocumentType = barcodeStatusUpdateRequest.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(barcodeStatusUpdateRequest, ref procMsg))
                    resp.Notes = procMsg;
                else
                if (barcodeStatusUpdateRequest.StageId < 1 || barcodeStatusUpdateRequest.StageId > 6)
                    resp.Notes = "Invalid barcode scanning stage ID in the request";
                else
                if (barcodeStatusUpdateRequest.From == null || string.IsNullOrEmpty(barcodeStatusUpdateRequest.From))
                    resp.Notes = "Barcode scanning 'From' location is not provided in the request";
                else
                if (barcodeStatusUpdateRequest.To == null || string.IsNullOrEmpty(barcodeStatusUpdateRequest.To))
                    resp.Notes = "Barcode scanning 'To' location is not provided in the request";
                else
                if (barcodeStatusUpdateRequest.BarcodeScanList == null || barcodeStatusUpdateRequest.BarcodeScanList.Length == 0)
                    resp.Notes = "Barcode scanning list is not provided in the request";
                else
                {
                    List<BarcodeScanStatusResult> resultList = null;

                    ConnoteStatusCheck connoteStatusCheck = new ConnoteStatusCheck();
                    connoteStatusCheck.DeviceId = barcodeStatusUpdateRequest.Authentication.APIUser;
                    connoteStatusCheck.From = barcodeStatusUpdateRequest.From;
                    connoteStatusCheck.To = barcodeStatusUpdateRequest.To;
                    connoteStatusCheck.StageId = barcodeStatusUpdateRequest.StageId;

                    ConnoteStatus connoteStatus = null;
                    CartonStatus cartonStatus = null;
                    DataTable dtConnote = null;
                    DataRow drConnote = null;
                    DataTable dtContainer = null;

                    DataTable dtStatusStage = GetStatusStageDataTable(null);
                    dtStatusStage.PrimaryKey = new DataColumn[] { dtStatusStage.Columns["StageId"] };
                    DataRow drStatusStage = null;

                    if (barcodeStatusUpdateRequest.StageId < 4) 
                    {
                        // get list of connotes and their container
                        foreach(BarcodeScanStatus barcodeScanStatus in barcodeStatusUpdateRequest.BarcodeScanList)
                        {
                            connoteStatus = new ConnoteStatus();
                            if (barcodeScanStatus.Barcode == null || string.IsNullOrEmpty(barcodeScanStatus.Barcode) || barcodeScanStatus.Barcode.Length < 18)
                            {
                                connoteStatus.Notes = string.Format("Invalid Scanned Barcode '{0}'", barcodeScanStatus.Barcode);

                                cartonStatus = new CartonStatus();
                                cartonStatus.Barcode = barcodeScanStatus.Barcode;                                
                                cartonStatus.Notes = connoteStatus.Notes;
                                cartonStatus.Accepted = false;
                                connoteStatus.CartonStatusList.Add(cartonStatus);

                                connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                if (!connoteStatusCheck.HasError)
                                    connoteStatusCheck.HasError = true;
                                continue;
                            }
                            connoteStatus.ConsNo = barcodeScanStatus.Barcode.Substring(0, 12);
                            dtConnote = GetConnoteDataTable(connoteStatus.ConsNo);
                            if (dtConnote.Rows.Count == 0)
                            {
                                connoteStatus.Notes = string.Format("Scanned Barcode '{0}' not found in the system", barcodeScanStatus.Barcode);

                                cartonStatus = new CartonStatus();
                                cartonStatus.Barcode = barcodeScanStatus.Barcode;
                                cartonStatus.Notes = connoteStatus.Notes;
                                cartonStatus.Accepted = false;
                                connoteStatus.CartonStatusList.Add(cartonStatus);
                                connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                if (!connoteStatusCheck.HasError)
                                    connoteStatusCheck.HasError = true;
                                continue;
                            }
                            drConnote = dtConnote.Rows[0];
                            object consKey = drConnote["ConsKey"];
                            connoteStatus.ConsNo = string.Format("{0}", drConnote["ConsNo"]);

                            dtContainer = GetContainerDataTable(consKey);
                            foreach(DataRow drContainer in dtContainer.Rows)
                            {
                                if (!(drContainer["Deleted"] is DBNull) && drContainer["Deleted"].ToString() != string.Empty)
                                    continue;

                                cartonStatus = new CartonStatus();
                                cartonStatus.Barcode = string.Format("{0}", drContainer["Barcode"]);
                                int lineNo = 0; int statusStageId = 0; 
                                if (GetConnoteContainerStatusValues(consKey, cartonStatus.Barcode, ref lineNo, ref statusStageId, ref procMsg))
                                {
                                    cartonStatus.ConsKey = Convert.ToInt32(consKey);
                                    cartonStatus.LineNo = lineNo;
                                    cartonStatus.StatusStageId = statusStageId;
                                    cartonStatus.Accepted = true;
                                }
                                else
                                {
                                    cartonStatus.ConsKey = Convert.ToInt32(consKey);
                                    cartonStatus.LineNo = 0;
                                    cartonStatus.StatusStageId = 0;
                                    cartonStatus.Notes = procMsg;
                                    cartonStatus.Accepted = false;
                                }                                
                                connoteStatus.CartonStatusList.Add(cartonStatus);
                            }
                            if (!ConnoteInList(connoteStatusCheck.ConnoteStatusList, connoteStatus.ConsNo))
                                connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                        }

                        // check status stage and missing scanned
                        if (connoteStatusCheck.ConnoteStatusList != null && connoteStatusCheck.ConnoteStatusList.Count > 0)
                        {
                            List<BarcodeScanStatus> barcodeScanList = barcodeStatusUpdateRequest.BarcodeScanList.Cast<BarcodeScanStatus>().ToList();
                            BarcodeScanStatus barcodeScanStatus = null;
                            
                            foreach(ConnoteStatus conStatus in connoteStatusCheck.ConnoteStatusList)
                            {
                                foreach(CartonStatus ctnStatus in conStatus.CartonStatusList)
                                {
                                    if (!ctnStatus.Accepted)
                                        continue;
                                    barcodeScanStatus = GetBarcodeScanStatus(barcodeScanList, ctnStatus.Barcode);
                                    if (barcodeScanStatus != null)
                                    {
                                        ctnStatus.DateTimeStamp = barcodeScanStatus.DateTimeStamp;
                                        ctnStatus.Removed = barcodeScanStatus.Removed;

                                        if (checkStageId && barcodeStatusUpdateRequest.StageId != ctnStatus.StatusStageId + 1)
                                        {
                                            drStatusStage = dtStatusStage.Rows.Find(ctnStatus.StatusStageId + 1);
                                            if (drStatusStage != null)
                                                ctnStatus.Notes = string.Format("Incorrect barcode scan status stage, excepting '{0} - {1}'",
                                                    drStatusStage["StageId"], drStatusStage["Description"]);
                                            else
                                                ctnStatus.Notes = "Incorrect barcode scan status stage";
                                            if (!connoteStatusCheck.HasError)
                                                connoteStatusCheck.HasError = true;
                                            ctnStatus.Accepted = false;
                                        }
                                        else
                                        
                                        if (ctnStatus.Removed)
                                        {
                                            ctnStatus.Notes = barcodeScanStatus.Notes;
                                        }
                                        else 
                                        {
                                            // will insert container status record
                                            drStatusStage = dtStatusStage.Rows.Find(barcodeStatusUpdateRequest.StageId);
                                            if (drStatusStage != null)
                                                ctnStatus.Notes = string.Format("{0} - {1}", drStatusStage["StageId"], drStatusStage["Description"]); 
                                        }
                                    }
                                    else
                                    {
                                        ctnStatus.Notes = "Barcode is missing in the provided scanned list";
                                        if (!connoteStatusCheck.HasError)
                                            connoteStatusCheck.HasError = true;
                                    }    
                                }
                            }
                            if (!connoteStatusCheck.HasError)
                            {
                                // will insert container status record, or flag removed
                                connoteStatusCheck.HasError = !ProcessEarlyStageBarcodeScanDataUpdate(connoteStatusCheck, ref procMsg);
                                if (connoteStatusCheck.HasError)
                                    connoteStatusCheck.Notes = procMsg;
                                else
                                    connoteStatusCheck.Notes = "BarcodeScanStatusUpdate succeed";
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(connoteStatusCheck.Notes))
                                    connoteStatusCheck.Notes = "BarcodeScanStatusUpdate failed";
                            }
                        }
                        resultList = new List<BarcodeScanStatusResult>();
                        BarcodeScanStatusResult scanResult = null;
                        if (!string.IsNullOrEmpty(connoteStatusCheck.Notes))
                            resp.Notes = connoteStatusCheck.Notes;
                        foreach(ConnoteStatus conStatus in connoteStatusCheck.ConnoteStatusList)
                        {
                            foreach(CartonStatus ctnStatus in conStatus.CartonStatusList)
                            {
                                scanResult = new BarcodeScanStatusResult();
                                if (ctnStatus.Accepted)
                                    scanResult.Accepted = ctnStatus.Accepted;
                                if (ctnStatus.Removed)
                                    scanResult.Removed = ctnStatus.Removed;
                                scanResult.Barcode = ctnStatus.Barcode;
                                scanResult.Notes = ctnStatus.Notes;
                                resultList.Add(scanResult);
                            }
                        }
                    }
                    else
                    // after 'Returning Office Scan-in from Venue'
                    if (barcodeStatusUpdateRequest.StageId == 4) 
                    {
                        DataTable dtRO = GetRODataTable(null);
                        dtRO.PrimaryKey = new DataColumn[] { dtRO.Columns["Code"] };
                        DataTable dtVenue = GetVenueDataTable(null);
                        
                        DataTable dtVenueType = GetVenueTypeDataTable(null);
                        dtVenueType.PrimaryKey = new DataColumn[] { dtVenueType.Columns["Code"] };
                        DataTable dtContest = GetContestDataTable(null);
                        dtContest.PrimaryKey = new DataColumn[] { dtContest.Columns["Code"] };
                        DataTable dtContainerType = GetContainerTypeDataTable(null);
                        dtContainerType.PrimaryKey = new DataColumn[] { dtContainerType.Columns["Code"] };

                        DataRow drRO = null; DataRow[] drsVenue = null; DataRow drVenueType = null; DataRow drContest = null; DataRow drContainerType = null;

                        foreach(BarcodeScanStatus barcodeScanStatus in barcodeStatusUpdateRequest.BarcodeScanList)
                        {
                            connoteStatus = new ConnoteStatus();

                            if (barcodeScanStatus.Barcode == null || string.IsNullOrEmpty(barcodeScanStatus.Barcode) || barcodeScanStatus.Barcode.Length < 18)
                            {
                                connoteStatus.Notes = string.Format("Invalid Scanned Barcode '{0}'", barcodeScanStatus.Barcode);
                                cartonStatus = new CartonStatus();
                                cartonStatus.Barcode = barcodeScanStatus.Barcode;
                                cartonStatus.Notes = connoteStatus.Notes;
                                cartonStatus.Accepted = false;
                                connoteStatus.CartonStatusList.Add(cartonStatus);
                                connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                if (!connoteStatusCheck.HasError)
                                    connoteStatusCheck.HasError = true;
                                continue;
                            }
                            
                            connoteStatus.ConsNo = barcodeScanStatus.Barcode.Substring(0, 12);

                            cartonStatus = new CartonStatus();
                            cartonStatus.Barcode = barcodeScanStatus.Barcode;
                            if (cartonStatus.Barcode.Length > 18)
                                cartonStatus.Barcode = cartonStatus.Barcode.Substring(0, 18);

                            dtConnote = GetConnoteDataTable(connoteStatus.ConsNo);
                            if (dtConnote.Rows.Count == 0)
                            {
                                cartonStatus.ConsKey = 0;
                                cartonStatus.LineNo = 0;
                                cartonStatus.StatusStageId = barcodeStatusUpdateRequest.StageId;
                                cartonStatus.DateTimeStamp = barcodeScanStatus.DateTimeStamp;

                                drRO = dtRO.Rows.Find(connoteStatus.ConsNo.Substring(0,2).PadLeft(3, '0'));
                                if (drRO == null)
                                {
                                    connoteStatus.Notes = string.Format("Invalid Returning Office CDID in Scanned Barcode '{0}'", barcodeScanStatus.Barcode);
                                    
                                    cartonStatus.Notes = connoteStatus.Notes;
                                    cartonStatus.Accepted = false;
                                    connoteStatus.CartonStatusList.Add(cartonStatus);

                                    connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                    if (!connoteStatusCheck.HasError)
                                        connoteStatusCheck.HasError = true;
                                    continue;
                                }
                                drsVenue = dtVenue.Select(string.Format("Code='{0}'", connoteStatus.ConsNo.Substring(2, 4).PadLeft(5, '0')));
                                if (drsVenue == null || drsVenue.Length == 0)
                                {
                                    connoteStatus.Notes = string.Format("Invalid Venue ID in Scanned Barcode '{0}'", barcodeScanStatus.Barcode);

                                    cartonStatus.Notes = connoteStatus.Notes;
                                    cartonStatus.Accepted = false;
                                    connoteStatus.CartonStatusList.Add(cartonStatus);

                                    connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                    if (!connoteStatusCheck.HasError)
                                        connoteStatusCheck.HasError = true;
                                    continue;
                                }
                                drVenueType = dtVenueType.Rows.Find(connoteStatus.ConsNo.Substring(6, 2));
                                if (drVenueType == null)
                                {
                                    connoteStatus.Notes = string.Format("Invalid Venue Type ID in Scanned Barcode '{0}'", barcodeScanStatus.Barcode);

                                    cartonStatus.Notes = connoteStatus.Notes;
                                    cartonStatus.Accepted = false;
                                    connoteStatus.CartonStatusList.Add(cartonStatus);

                                    connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                    if (!connoteStatusCheck.HasError)
                                        connoteStatusCheck.HasError = true;
                                    continue;
                                }
                                drContest = dtContest.Rows.Find(connoteStatus.ConsNo.Substring(8, 2));
                                if (drContest == null)
                                {
                                    connoteStatus.Notes = string.Format("Invalid Contest ID in Scanned Barcode '{0}'", barcodeScanStatus.Barcode);

                                    cartonStatus.Notes = connoteStatus.Notes;
                                    cartonStatus.Accepted = false;
                                    connoteStatus.CartonStatusList.Add(cartonStatus);

                                    connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                    if (!connoteStatusCheck.HasError)
                                        connoteStatusCheck.HasError = true;
                                    continue;
                                }
                                drContainerType = dtContainerType.Rows.Find(connoteStatus.ConsNo.Substring(10));
                                if (drContainerType == null)
                                {
                                    connoteStatus.Notes = string.Format("Invalid Container Type in Scanned Barcode '{0}'", barcodeScanStatus.Barcode);

                                    cartonStatus.Notes = connoteStatus.Notes;
                                    cartonStatus.Accepted = false;
                                    connoteStatus.CartonStatusList.Add(cartonStatus);

                                    connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                    if (!connoteStatusCheck.HasError)
                                        connoteStatusCheck.HasError = true;
                                    continue;
                                }

                                cartonStatus.CDID = string.Format("{0}", drRO["Code"]);
                                cartonStatus.ReturningOfficeName = string.Format("{0}", drRO["Name"]);
                                cartonStatus.VenueCode = string.Format("{0}", drsVenue[0]["Code"]);
                                //cartonStatus.VenueName = string.Format("{0}", drsVenue[0]["Name"]);
                                cartonStatus.VenueName = barcodeStatusUpdateRequest.From;
                                cartonStatus.VenueTypeCode = string.Format("{0}", drVenueType["Code"]);
                                cartonStatus.VenueTypeName = string.Format("{0}", drVenueType["Name"]);
                                cartonStatus.ContestCode = string.Format("{0}", drContest["Code"]);
                                cartonStatus.ContestName = string.Format("{0}", drContest["Name"]);
                                cartonStatus.ContainerCode = string.Format("{0}", drContainerType["Code"]);
                                cartonStatus.ContainerName = string.Format("{0}", drContainerType["Name"]);

                                cartonStatus.Accepted = !barcodeScanStatus.Removed;
                                if (cartonStatus.Accepted)
                                {
                                    drStatusStage = dtStatusStage.Rows.Find(barcodeStatusUpdateRequest.StageId);
                                    if (drStatusStage != null)
                                        cartonStatus.Notes = string.Format("{0} - {1}", drStatusStage["StageId"], drStatusStage["Description"]); 
                                }
                            }
                            else
                            {
                                drConnote = dtConnote.Rows[0];
                                object consKey = drConnote["ConsKey"];
                                dtContainer = GetContainerDataTable(consKey);

                                int lineNo = 0; int statusStageId = 0; 
                                if (GetConnoteContainerStatusValues(consKey, cartonStatus.Barcode, ref lineNo, ref statusStageId, ref procMsg))
                                {
                                    cartonStatus.ConsKey = Convert.ToInt32(consKey);
                                    cartonStatus.LineNo = lineNo;
                                    cartonStatus.StatusStageId = statusStageId;
                                    cartonStatus.Accepted = true;
                                    drStatusStage = dtStatusStage.Rows.Find(barcodeStatusUpdateRequest.StageId);
                                    if (drStatusStage != null)
                                        cartonStatus.Notes = string.Format("{0} - {1}", drStatusStage["StageId"], drStatusStage["Description"]); 
                                }
                                else
                                {
                                    cartonStatus.ConsKey = Convert.ToInt32(consKey);
                                    cartonStatus.LineNo = 0;
                                    cartonStatus.StatusStageId = barcodeStatusUpdateRequest.StageId;
                                    cartonStatus.DateTimeStamp = barcodeScanStatus.DateTimeStamp;
                                    cartonStatus.Accepted = !barcodeScanStatus.Removed;
                                    if (cartonStatus.Accepted)
                                    {
                                        drStatusStage = dtStatusStage.Rows.Find(barcodeStatusUpdateRequest.StageId);
                                        if (drStatusStage != null)
                                            cartonStatus.Notes = string.Format("{0} - {1}", drStatusStage["StageId"], drStatusStage["Description"]);
                                    }
                                }  
                            }
                            connoteStatus.CartonStatusList.Add(cartonStatus);
                            connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                        }

                        if (!connoteStatusCheck.HasError)
                        {
                            connoteStatusCheck.HasError = !ProcessLateStageBarcodeScanDataUpdate(connoteStatusCheck, ref procMsg);
                            if (connoteStatusCheck.HasError)
                                connoteStatusCheck.Notes = procMsg;
                            else
                                connoteStatusCheck.Notes = "BarcodeScanStatusUpdate succeed";
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(connoteStatusCheck.Notes))
                                connoteStatusCheck.Notes = "BarcodeScanStatusUpdate failed";
                        }

                        resultList = new List<BarcodeScanStatusResult>();
                        BarcodeScanStatusResult scanResult = null;
                        if (!string.IsNullOrEmpty(connoteStatusCheck.Notes))
                            resp.Notes = connoteStatusCheck.Notes;
                        foreach (ConnoteStatus conStatus in connoteStatusCheck.ConnoteStatusList)
                        {
                            foreach (CartonStatus ctnStatus in conStatus.CartonStatusList)
                            {
                                scanResult = new BarcodeScanStatusResult();
                                if (ctnStatus.Accepted)
                                    scanResult.Accepted = ctnStatus.Accepted;
                                scanResult.Barcode = ctnStatus.Barcode;
                                scanResult.Notes = ctnStatus.Notes;
                                resultList.Add(scanResult);
                            }
                        }
                    }
                    else // RO Scan-out, and Warehouse Scan-in
                    {
                        // get list of connotes and their container
                        foreach (BarcodeScanStatus barcodeScanStatus in barcodeStatusUpdateRequest.BarcodeScanList)
                        {
                            connoteStatus = new ConnoteStatus();

                            if (barcodeScanStatus.Barcode == null || string.IsNullOrEmpty(barcodeScanStatus.Barcode) || barcodeScanStatus.Barcode.Length < 18)
                            {
                                connoteStatus.Notes = string.Format("Invalid Scanned Barcode '{0}'", barcodeScanStatus.Barcode);
                                cartonStatus = new CartonStatus();
                                cartonStatus.Barcode = barcodeScanStatus.Barcode;
                                cartonStatus.Notes = connoteStatus.Notes;
                                cartonStatus.Accepted = false;
                                connoteStatus.CartonStatusList.Add(cartonStatus);
                                connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                if (!connoteStatusCheck.HasError)
                                    connoteStatusCheck.HasError = true;
                                continue;
                            }

                            connoteStatus.ConsNo = barcodeScanStatus.Barcode.Substring(0, 12);

                            dtConnote = GetConnoteDataTable(connoteStatus.ConsNo);
                            if (dtConnote.Rows.Count == 0)
                            {
                                connoteStatus.Notes = string.Format("Scanned barcode '{0}' not found in the system for this status stage", barcodeScanStatus.Barcode);
                                cartonStatus = new CartonStatus();
                                cartonStatus.Barcode = barcodeScanStatus.Barcode;
                                cartonStatus.Notes = connoteStatus.Notes;
                                cartonStatus.Accepted = false;
                                connoteStatus.CartonStatusList.Add(cartonStatus);
                                connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                                if (!connoteStatusCheck.HasError)
                                    connoteStatusCheck.HasError = true;
                                continue;
                            }
                            drConnote = dtConnote.Rows[0];
                            object consKey = drConnote["ConsKey"];
                            connoteStatus.ConsNo = string.Format("{0}", drConnote["ConsNo"]);

                            dtContainer = GetContainerDataTable(consKey);
                            foreach (DataRow drContainer in dtContainer.Rows)
                            {
                                cartonStatus = new CartonStatus();
                                cartonStatus.Barcode = string.Format("{0}", drContainer["Barcode"]);
                                int lineNo = 0; int statusStageId = 0; 
                                if (GetConnoteContainerStatusValues(consKey, cartonStatus.Barcode, ref lineNo, ref statusStageId, ref procMsg))
                                {
                                    cartonStatus.ConsKey = Convert.ToInt32(consKey);
                                    cartonStatus.LineNo = lineNo;
                                    cartonStatus.StatusStageId = statusStageId;
                                }
                                else
                                {
                                    cartonStatus.ConsKey = Convert.ToInt32(consKey);
                                    cartonStatus.LineNo = 0;
                                    cartonStatus.StatusStageId = 3;
                                    cartonStatus.Notes = procMsg;
                                }
                                connoteStatus.CartonStatusList.Add(cartonStatus);
                            }
                            if (!ConnoteInList(connoteStatusCheck.ConnoteStatusList, connoteStatus.ConsNo))
                                connoteStatusCheck.ConnoteStatusList.Add(connoteStatus);
                        }
                        // check status stage and missing scanned
                        if (connoteStatusCheck.ConnoteStatusList != null && connoteStatusCheck.ConnoteStatusList.Count > 0)
                        {
                            List<BarcodeScanStatus> barcodeScanList = barcodeStatusUpdateRequest.BarcodeScanList.Cast<BarcodeScanStatus>().ToList();
                            BarcodeScanStatus barcodeScanStatus = null;

                            foreach (ConnoteStatus conStatus in connoteStatusCheck.ConnoteStatusList)
                            {
                                foreach (CartonStatus ctnStatus in conStatus.CartonStatusList)
                                {
                                    if (ctnStatus.Notes!= null && !string.IsNullOrEmpty(ctnStatus.Notes))
                                        continue;
                                    barcodeScanStatus = GetBarcodeScanStatus(barcodeScanList, ctnStatus.Barcode);
                                    if (barcodeScanStatus != null)
                                    {
                                        ctnStatus.DateTimeStamp = barcodeScanStatus.DateTimeStamp;
                                        ctnStatus.Removed = barcodeScanStatus.Removed;
                                        if (ctnStatus.Removed)
                                        {
                                            ctnStatus.Notes = barcodeScanStatus.Notes;
                                        }
                                        else
                                            if (barcodeStatusUpdateRequest.StageId != ctnStatus.StatusStageId + 1)
                                            {
                                                drStatusStage = dtStatusStage.Rows.Find(ctnStatus.StatusStageId + 1);
                                                if (drStatusStage != null)
                                                    ctnStatus.Notes = string.Format("Incorrect barcode scan status stage, excepting '{0} - {1}'",
                                                        drStatusStage["StageId"], drStatusStage["Description"]);
                                                else
                                                    ctnStatus.Notes = "Incorrect barcode scan status stage";
                                                if (!connoteStatusCheck.HasError)
                                                    connoteStatusCheck.HasError = true;
                                            }
                                            else
                                            {
                                                // will insert container status record
                                                drStatusStage = dtStatusStage.Rows.Find(barcodeStatusUpdateRequest.StageId);
                                                if (drStatusStage != null)
                                                    ctnStatus.Notes = string.Format("{0} - {1}", drStatusStage["StageId"], drStatusStage["Description"]);
                                                ctnStatus.Accepted = true;
                                            }
                                    }
                                }
                            }
                            if (!connoteStatusCheck.HasError)
                            {
                                // will insert container status record, or flag removed
                                connoteStatusCheck.HasError = !ProcessLateStageBarcodeScanDataUpdate(connoteStatusCheck, ref procMsg);
                                if (connoteStatusCheck.HasError)
                                    connoteStatusCheck.Notes = procMsg;
                                else
                                    connoteStatusCheck.Notes = "BarcodeScanStatusUpdate succeed";
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(connoteStatusCheck.Notes))
                                    connoteStatusCheck.Notes = "BarcodeScanStatusUpdate failed";
                            }
                            resultList = new List<BarcodeScanStatusResult>();
                            BarcodeScanStatusResult scanResult = null;
                            if (!string.IsNullOrEmpty(connoteStatusCheck.Notes))
                                resp.Notes = connoteStatusCheck.Notes;
                            foreach (ConnoteStatus conStatus in connoteStatusCheck.ConnoteStatusList)
                            {
                                foreach (CartonStatus ctnStatus in conStatus.CartonStatusList)
                                {
                                    scanResult = new BarcodeScanStatusResult();
                                    if (ctnStatus.Accepted)
                                        scanResult.Accepted = ctnStatus.Accepted;
                                    if (ctnStatus.Removed)
                                        scanResult.Removed = ctnStatus.Removed;
                                    scanResult.Barcode = ctnStatus.Barcode;
                                    scanResult.Notes = ctnStatus.Notes;
                                    resultList.Add(scanResult);
                                }
                            }
                        }
                    }
                    resp.StageId = barcodeStatusUpdateRequest.StageId;
                    resp.From = barcodeStatusUpdateRequest.From;
                    resp.To = barcodeStatusUpdateRequest.To;
                    if (resultList != null && resultList.Count > 0)
                        resp.BarcodeScanStatusResultList = resultList.ToArray();
                }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in BarcodeScanStatusUpdate: {0}", ex.Message);
            }
            if (resp != null)
            {
                // record the response                            
                soapWriter = new XmlSerializer(typeof(BarcodeScanStatusUpdateResponseType));
                // Create a new file stream to write the serialized object to a file
                string respFileName = string.Format("{0}\\ScanStatusUpdateResponse_{1}.xml", _saveRequestResponseFolder, DateTime.Now.ToString("yyyyMMddHHmmss"));
                System.IO.FileStream respFile = System.IO.File.Create(respFileName);
                soapWriter.Serialize(respFile, resp);
                // Cleanup
                respFile.Close();
            }
            return resp;
        }
        private static bool ProcessEarlyStageBarcodeScanDataUpdate(ConnoteStatusCheck connoteStatusCheck, ref string procMsg)
        {
            bool result = false;
            procMsg = string.Empty;
            string sqlStr = string.Empty;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(dbConn))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        foreach (ConnoteStatus conStatus in connoteStatusCheck.ConnoteStatusList)
                        {
                            foreach(CartonStatus ctnStatus in conStatus.CartonStatusList)
                            {
                                if (ctnStatus.Removed)
                                {
                                    sqlStr = @"UPDATE NSW_Container SET Deleted=@Deleted, DeletedBy=@DeletedBy, DeletedReason=@DeletedReason
                                        WHERE ConsKey=@ConsKey AND [LineNo]=@LineNo AND Barcode=@Barcode";
                                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                    {
                                        string dateTimeStr = ctnStatus.DateTimeStamp.Replace('T', ' ');
                                        DateTime dtDateTime = DateTime.Now;
                                        try
                                        {
                                            dtDateTime = Convert.ToDateTime(dateTimeStr);
                                        }
                                        catch (Exception ex)
                                        {
                                            dtDateTime = DateTime.Now;
                                        }

                                        cmd.Parameters.AddWithValue("@Deleted", dtDateTime);
                                        cmd.Parameters.AddWithValue("@DeletedBy", connoteStatusCheck.DeviceId);
                                        cmd.Parameters.AddWithValue("@DeletedReason", ctnStatus.Notes);
                                        cmd.Parameters.AddWithValue("@ConsKey", ctnStatus.ConsKey);
                                        cmd.Parameters.AddWithValue("@LineNo", ctnStatus.LineNo);
                                        cmd.Parameters.AddWithValue("@Barcode", ctnStatus.Barcode);
                                        result = cmd.ExecuteNonQuery() > 0;
                                        if (!result)
                                            break;
                                    }
                                }
                                else
                                if (ctnStatus.Accepted)
                                {
                                    sqlStr = @"INSERT INTO NSW_ContainerStatus ([ConsKey],[LineNo],[StatusDate],[StatusTime],[StatusNotes],[MovementDirection],[Created],[CreatedBy],[StatusStage])
                                        VALUES(@ConsKey,@LineNo,@StatusDate,@StatusTime,@StatusNotes,@MovementDirection,@Created,@CreatedBy,@StatusStage)";
                                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                    {
                                        string dateTimeStr = ctnStatus.DateTimeStamp.Replace('T', ' ');
                                        DateTime dtDateTime = DateTime.Now;
                                        try
                                        {
                                            dtDateTime = Convert.ToDateTime(dateTimeStr);
                                        }
                                        catch (Exception ex)
                                        {
                                            dtDateTime = DateTime.Now;
                                        }

                                        cmd.Parameters.AddWithValue("@ConsKey", ctnStatus.ConsKey);
                                        cmd.Parameters.AddWithValue("@LineNo", ctnStatus.LineNo);
                                        cmd.Parameters.AddWithValue("@StatusDate", dtDateTime);
                                        cmd.Parameters.AddWithValue("@StatusTime", dtDateTime.ToString("HH:mm"));
                                        cmd.Parameters.AddWithValue("@StatusNotes", ctnStatus.Notes);
                                        cmd.Parameters.AddWithValue("@MovementDirection", string.Format("From '{0}' to '{1}'",  connoteStatusCheck.From, connoteStatusCheck.To));
                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@CreatedBy", connoteStatusCheck.DeviceId);
                                        cmd.Parameters.AddWithValue("@StatusStage", connoteStatusCheck.StageId);
                                        result = cmd.ExecuteNonQuery() > 0;
                                        if (!result)
                                            break;
                                    }
                                }
                            }
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in ProcessEarlyStageBarcodeScanDataUpdate: {0}", ex.Message);
                    }
                }
            }
            catch(Exception ex)
            {
                result = false;
                procMsg = string.Format("Error in ProcessConnoteContainerScanStatusUpdate: {0}", ex.Message);
            }
            return result;
        }

        private static bool ConnoteInList(List<ConnoteStatus> connoteStatusList, string consNo)
        {
            bool result = false;
            foreach (ConnoteStatus connoteStatus in connoteStatusList)
            {
                result = string.Compare(connoteStatus.ConsNo, consNo, true) == 0;
                if (result)
                    break;
            }
            return result;
        }
        private static BarcodeScanStatus GetBarcodeScanStatus(List<BarcodeScanStatus> barcodeScanList, string barcode)
        {
            BarcodeScanStatus result = null;
            string scannedBarcode = string.Empty;
            foreach(BarcodeScanStatus barcodeScanStatus in barcodeScanList)
            {
                scannedBarcode = barcodeScanStatus.Barcode;
                if (scannedBarcode.Length > 18)
                    scannedBarcode = scannedBarcode.Substring(0, 18);
                if (string.Compare(scannedBarcode, barcode, true) == 0)
                {
                    result = barcodeScanStatus;
                    break;
                }
            }
            return result;
        }
        private static bool loginSuccess(RequestMessageType request)
        {
            bool result = false;

            string sqlStr = "SELECT * FROM SecurityDevice WHERE DeviceId=@DeviceId";
            DataTable dt = new DataTable();
            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                sqlCmd.Parameters.AddWithValue("@DeviceId", request.Authentication.APIUser);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dt);

                result = dt.Rows.Count > 0;
            }

            return result;
        }
        private static bool CheckAuthentication(RequestMessageType request, ref string procMsg)
        {
            bool result = request.Authentication != null && !string.IsNullOrEmpty(request.Authentication.APIUser);
            if (result)
            {
                result = loginSuccess(request);
                if (!result)
                    procMsg = "Authentication Failed";
            }
            else
                procMsg = "Authentication infomation in header not provided";
            return result;
        }
        private static DataTable GetRODataTable(object cdid)
        {
            string sqlStr = "SELECT * FROM NSW_ReturningOffice WHERE Active='Y' and (Code=@Code or (@Code is null))";
            
            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (cdid != null && cdid.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@Code", cdid.ToString().PadLeft(3, '0'));
                else
                    sqlCmd.Parameters.AddWithValue("@Code", DBNull.Value);

                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }
        private static DataTable GetVenueDataTable(object cdid)
        {
            string sqlStr = "SELECT * FROM NSW_Venue WHERE Active='Y' and (ROCode=@ROCode or (@ROCode is null)) ";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (cdid != null && cdid.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@ROCode", cdid.ToString().PadLeft(3, '0'));
                else
                    sqlCmd.Parameters.AddWithValue("@ROCode", DBNull.Value);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        private static DataTable GetVenueTypeDataTable(object code)
        {
            string sqlStr = "SELECT * FROM NSW_VenueType WHERE Active='Y' and (Code=@Code or (@Code is null))";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (code != null && code.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@Code", code);
                else
                    sqlCmd.Parameters.AddWithValue("@Code", DBNull.Value);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        private static DataTable GetContestDataTable(object code)
        {
            string sqlStr = "SELECT * FROM NSW_Contest WHERE Active='Y' and (Code=@Code or (@Code is null))";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (code != null && code.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@Code", code);
                else
                    sqlCmd.Parameters.AddWithValue("@Code", DBNull.Value);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        private static DataTable GetContainerTypeDataTable(object code)
        {
            string sqlStr = "SELECT * FROM NSW_ContainerType WHERE Active='Y' and (Code=@Code or (@Code is null))";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (code != null && code.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@Code", code);
                else
                    sqlCmd.Parameters.AddWithValue("@Code", DBNull.Value);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        private static DataTable GetStatusStageDataTable(object stageId)
        {
            string sqlStr = "SELECT * FROM NSW_StatusStage WHERE StageId=@StageId or @StageId is null";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (stageId != null && stageId.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@StageId", stageId);
                else
                    sqlCmd.Parameters.AddWithValue("@StageId", DBNull.Value);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        private static DataTable GetContainerBarcodeDataTable(object cdid, object venueCode, object barCode, object statusStageFlag)
        {
            string sqlStr = "exec NSW_GetContainerBarcodeData @CDID, @VenueCode, @BarcodeList, @StageStatus";
            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (cdid != null && cdid.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@CDID", cdid.ToString().PadLeft(3, '0'));
                else
                    sqlCmd.Parameters.AddWithValue("@CDID", DBNull.Value);
                if (venueCode != null && venueCode.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@VenueCode", venueCode.ToString().PadLeft(5, '0'));
                else
                    sqlCmd.Parameters.AddWithValue("@VenueCode", DBNull.Value);
                if (barCode != null && barCode.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@BarcodeList", barCode);
                else
                    sqlCmd.Parameters.AddWithValue("@BarcodeList", DBNull.Value);
                if (statusStageFlag != null && statusStageFlag.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@StageStatus", statusStageFlag);
                else
                    sqlCmd.Parameters.AddWithValue("@StageStatus", DBNull.Value);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        private static DataTable GetConnoteContainerDataTable(object consKey, object barCode)
        {
            string sqlStr = "exec NSW_GetConnoteContainer @Conskey, @Barcode";
            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                if (consKey != null && consKey.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@Conskey", consKey);
                else
                    sqlCmd.Parameters.AddWithValue("@Conskey", DBNull.Value);
                if (barCode != null && barCode.ToString() != string.Empty)
                    sqlCmd.Parameters.AddWithValue("@Barcode", barCode);
                else
                    sqlCmd.Parameters.AddWithValue("@Barcode", DBNull.Value);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }
        private static bool GetConnoteContainerStatusValues(object consKey, object barCode, ref int lineNo, 
            ref int statusStageId, ref string procMsg)
        {
            bool result = false;
            procMsg = string.Empty;
            try
            {
                lineNo = 0;
                statusStageId = 0;
                DataTable dt = GetConnoteContainerDataTable(consKey, barCode);
                result = dt != null && dt.Rows.Count > 0;
                if (result)
                {
                    DataRow dr = dt.Rows[0];
                    if (!(dr["LineNo"] is DBNull) && dr["LineNo"].ToString() != string.Empty)
                        lineNo = StrToIntDef(dr["LineNo"].ToString(), lineNo);
                    if (!(dr["StatusStage"] is DBNull) && dr["StatusStage"].ToString() != string.Empty)
                        statusStageId = StrToIntDef(dr["StatusStage"].ToString(), statusStageId);
                }
            }
            catch(Exception ex)
            {
                result = false;
                procMsg = string.Format("Error in GetConnoteContainerStatusValues: {0}", ex.Message);
            }
            return result;
        }
        private static bool ProcessLateStageBarcodeScanDataUpdate(ConnoteStatusCheck connoteStatusCheck, ref string procMsg)
        {
            bool result = false;
            procMsg = string.Empty;
            string sqlStr = string.Empty;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(dbConn))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        ConnoteInsertedList insertedList = new ConnoteInsertedList();
                        ConnoteInserted connoteInserted = null;
                        foreach (ConnoteStatus conStatus in connoteStatusCheck.ConnoteStatusList)
                        {
                            foreach (CartonStatus ctnStatus in conStatus.CartonStatusList)
                            {
                                if (ctnStatus.Accepted)
                                {
                                    int consKey = -1;
                                    if (ctnStatus.ConsKey == 0 && ctnStatus.LineNo == 0)
                                    {
                                        connoteInserted = insertedList.GetConnoteInserted(conStatus.ConsNo);
                                        if (connoteInserted == null)
                                        {
                                            // insert connote
                                            sqlStr = @"INSERT INTO NSW_Connote ([ConsNo],[CDID],[ReturningOfficeName],[VenueCode],[VenueName],
                                                [VenueTypeCode],[VenueTypeName],[ContestCode],[ContestName],[ContainerCode],[ContainerName],
                                                [ContainerQuantity],[Created],[CreatedBy])
                                            VALUES (@ConsNo,@CDID,@ReturningOfficeName,@VenueCode,@VenueName,@VenueTypeCode,@VenueTypeName,
                                                @ContestCode,@ContestName,@ContainerCode,@ContainerName,@ContainerQuantity,@Created,@CreatedBy); 
                                            SELECT CAST(scope_identity() AS int)";
                                            using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                            {
                                                cmd.Parameters.AddWithValue("@ConsNo", conStatus.ConsNo);

                                                cmd.Parameters.AddWithValue("@CDID", ctnStatus.CDID);
                                                cmd.Parameters.AddWithValue("@ReturningOfficeName", ctnStatus.ReturningOfficeName);
                                                cmd.Parameters.AddWithValue("@VenueCode", ctnStatus.VenueCode);
                                                cmd.Parameters.AddWithValue("@VenueName", ctnStatus.VenueName);
                                                cmd.Parameters.AddWithValue("@VenueTypeCode", ctnStatus.VenueTypeCode);
                                                cmd.Parameters.AddWithValue("@VenueTypeName", ctnStatus.VenueTypeName);
                                                cmd.Parameters.AddWithValue("@ContestCode", ctnStatus.ContestCode);
                                                cmd.Parameters.AddWithValue("@ContestName", ctnStatus.ContestName);
                                                cmd.Parameters.AddWithValue("@ContainerCode", ctnStatus.ContainerCode);
                                                cmd.Parameters.AddWithValue("@ContainerName", ctnStatus.ContainerName);

                                                cmd.Parameters.AddWithValue("@ContainerQuantity", StrToIntDef(ctnStatus.Barcode.Substring(15), 1));
                                                cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                                cmd.Parameters.AddWithValue("@CreatedBy", connoteStatusCheck.DeviceId);
                                                consKey = (Int32)cmd.ExecuteScalar();
                                                result = consKey > -1; // new id returned
                                                if (!result)
                                                {
                                                    sqlTrans.Rollback();
                                                    procMsg = string.Format("Consignment not created for barcode '{0}'", ctnStatus.Barcode);
                                                    return result;
                                                }
                                            }
                                            ctnStatus.ConsKey = consKey;

                                            connoteInserted = new ConnoteInserted();
                                            connoteInserted.ConsNo = conStatus.ConsNo;
                                            connoteInserted.ConsKey = ctnStatus.ConsKey;
                                            connoteInserted.MaxLineNo = 1;
                                            insertedList.Add(connoteInserted);
                                        }
                                        else
                                        {
                                            consKey = connoteInserted.ConsKey;
                                            ctnStatus.ConsKey = consKey;
                                        }
                                        // insert container
                                        sqlStr = @"INSERT INTO NSW_Container ([ConsKey],[LineNo],[ContainerNo],[Barcode],[Created],[CreatedBy])
                                            VALUES (@ConsKey,@LineNo,@ContainerNo,@Barcode,@Created,@CreatedBy)";
                                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                        {
                                            cmd.Parameters.AddWithValue("@ConsKey", consKey);
                                            //cmd.Parameters.AddWithValue("@LineNo", 1);

                                            ctnStatus.LineNo = connoteInserted.MaxLineNo;

                                            cmd.Parameters.AddWithValue("@LineNo", ctnStatus.LineNo);
                                            cmd.Parameters.AddWithValue("@ContainerNo", ctnStatus.Barcode.Substring(12));
                                            cmd.Parameters.AddWithValue("@Barcode", ctnStatus.Barcode);
                                            cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                            cmd.Parameters.AddWithValue("@CreatedBy", connoteStatusCheck.DeviceId);
                                            result = cmd.ExecuteNonQuery() > 0;
                                            if (!result)
                                            {
                                                sqlTrans.Rollback();
                                                procMsg = string.Format("Container not created for barcode '{0}'", ctnStatus.Barcode);
                                                return result;
                                            }
                                        }
                                    }
                                    else
                                    if (ctnStatus.ConsKey > 0 && ctnStatus.LineNo == 0)
                                    {
                                        connoteInserted = insertedList.GetConnoteInserted(conStatus.ConsNo);
                                        if (connoteInserted == null)
                                        {
                                            DataTable dtContainer = GetContainerDataTable(ctnStatus.ConsKey);
                                            connoteInserted = new ConnoteInserted();
                                            connoteInserted.ConsNo = conStatus.ConsNo;
                                            connoteInserted.ConsKey = ctnStatus.ConsKey;
                                            connoteInserted.MaxLineNo = GetMaxContainerLineNo(dtContainer) ;
                                            insertedList.Add(connoteInserted);
                                        }
                                        consKey = connoteInserted.ConsKey;
                                        ctnStatus.LineNo = connoteInserted.MaxLineNo;
                                        //DataTable dtContainer = GetContainerDataTable(consKey);
                                        //int lineNo = GetMaxContainerLineNo(dtContainer);
                                        // insert container
                                        sqlStr = @"INSERT INTO NSW_Container ([ConsKey],[LineNo],[ContainerNo],[Barcode],[Created],[CreatedBy])
                                            VALUES (@ConsKey,@LineNo,@ContainerNo,@Barcode,@Created,@CreatedBy)";
                                        using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                        {
                                            cmd.Parameters.AddWithValue("@ConsKey", consKey);
                                            cmd.Parameters.AddWithValue("@LineNo", ctnStatus.LineNo);
                                            cmd.Parameters.AddWithValue("@ContainerNo", ctnStatus.Barcode.Substring(12));
                                            cmd.Parameters.AddWithValue("@Barcode", ctnStatus.Barcode);
                                            cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                            cmd.Parameters.AddWithValue("@CreatedBy", connoteStatusCheck.DeviceId);
                                            result = cmd.ExecuteNonQuery() > 0;
                                            if (!result)
                                            {
                                                sqlTrans.Rollback();
                                                procMsg = string.Format("Container not created for barcode '{0}'", ctnStatus.Barcode);
                                                return result;
                                            }
                                        }
                                    }
                                    // insert container status
                                    sqlStr = @"INSERT INTO NSW_ContainerStatus ([ConsKey],[LineNo],[StatusDate],[StatusTime],[StatusNotes],[MovementDirection],[Created],[CreatedBy],[StatusStage])
                                        VALUES(@ConsKey,@LineNo,@StatusDate,@StatusTime,@StatusNotes,@MovementDirection,@Created,@CreatedBy,@StatusStage)";
                                    using (SqlCommand cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                                    {
                                        string dateTimeStr = ctnStatus.DateTimeStamp.Replace('T', ' ');
                                        DateTime dtDateTime = DateTime.Now;
                                        try
                                        {
                                            dtDateTime = Convert.ToDateTime(dateTimeStr);
                                        }
                                        catch (Exception ex)
                                        {
                                            dtDateTime = DateTime.Now;
                                        }

                                        cmd.Parameters.AddWithValue("@ConsKey", ctnStatus.ConsKey);
                                        cmd.Parameters.AddWithValue("@LineNo", ctnStatus.LineNo);
                                        cmd.Parameters.AddWithValue("@StatusDate", dtDateTime);
                                        cmd.Parameters.AddWithValue("@StatusTime", dtDateTime.ToString("HH:mm"));
                                        cmd.Parameters.AddWithValue("@StatusNotes", ctnStatus.Notes);
                                        cmd.Parameters.AddWithValue("@MovementDirection", string.Format("From '{0}' to '{1}'", connoteStatusCheck.From, connoteStatusCheck.To));
                                        cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@CreatedBy", connoteStatusCheck.DeviceId);
                                        cmd.Parameters.AddWithValue("@StatusStage", connoteStatusCheck.StageId);
                                        result = cmd.ExecuteNonQuery() > 0;
                                        if (!result)
                                        {
                                            sqlTrans.Rollback();
                                            procMsg = string.Format("Container status not updated for barcode '{0}'", ctnStatus.Barcode);
                                            return result;
                                        }
                                    }
                                }
                            }
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in ProcessLateStageBarcodeScanDataUpdate: {0}", ex.Message);
                    }
                }
            }
            catch(Exception ex)
            {
                procMsg = string.Format("Error in ProcessLateStageBarcodeScanDataUpdate: {0}", ex.Message);
            }
            return result;
        }
        private List<string> GetImportContainerBarcodeData(GenerateContainerBarcodeRequestType request, object consNo, ref string procMsg)
        {
            List<string> barcodeList = new List<string>();
            bool result = false;
            DataTable dtConnote = GetConnoteDataTable(consNo);
            if (dtConnote != null && dtConnote.Rows.Count > 0)
            {
                using (SqlConnection sqlConn = new SqlConnection(dbConn))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        string sqlStr = "SELECT * FROM NSW_Connote WHERE ConsNo=@ConsNo";

                        int consKey = -1;
                        int additionalQuantity = 0;
                        int conQty = 0;
                        DataTable dtConnoteData = new DataTable();
                        SqlCommand cmd = null;
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            cmd.Parameters.AddWithValue("@ConsNo", consNo);
                            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = cmd;
                                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
                                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                sqlDataAdapter.Fill(dtConnoteData);

                                result = dtConnoteData.Rows.Count > 0;
                                if (result)
                                {
                                    DataRow drConnoteData = dtConnoteData.Rows[0];
                                    consKey = StrToIntDef(drConnoteData["ConsKey"].ToString(), 0);
                                    result = consKey > 0;
                                    if (result)
                                    {
                                        conQty = StrToIntDef(drConnoteData["ContainerQuantity"].ToString(), 0);
                                        additionalQuantity = StrToIntDef(drConnoteData["AdditionalContainerQuantity"].ToString(), 0);
                                        drConnoteData["AdditionalContainerQuantity"] = additionalQuantity + request.ContainerQty;
                                        DateTime modified = DateTime.Now;
                                        drConnoteData["Modified"] = modified.Subtract(new TimeSpan(0, 0, 0, 0, modified.Millisecond));
                                        drConnoteData["ModifiedBy"] = request.Authentication.APIUser;
                                        result = sqlDataAdapter.Update(dtConnoteData) > 0;
                                    }
                                }
                            }
                        }
                        if (result)
                        {
                            DataTable dtContainer = GetContainerDataTable(consKey);
                            sqlStr = "SELECT * FROM NSW_Container WHERE ConsKey=@ConsKey";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ConsKey", consKey);
                                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                {
                                    sqlDataAdapter.SelectCommand = cmd;
                                    using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                    {
                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                        DataTable dtConDetData = new DataTable();
                                        sqlDataAdapter.Fill(dtConDetData);

                                        DataRow drConDetData = null;
                                        string containerNo = string.Empty;
                                        string containerBarcode = string.Empty;
                                        int lineNo = GetMaxContainerLineNo(dtContainer);
                                        for (int i = 0; i < request.ContainerQty; i++)
                                        {
                                            drConDetData = dtConDetData.NewRow();
                                            drConDetData["ConsKey"] = consKey;
                                            drConDetData["LineNo"] = lineNo;                                            
                                            containerNo = string.Format("{0}{1}", lineNo.ToString().PadLeft(3, '0'), (conQty + additionalQuantity + request.ContainerQty).ToString().PadLeft(3, '0'));
                                            drConDetData["ContainerNo"] = containerNo;
                                            containerBarcode = string.Format("{0}{1}", consNo, containerNo).Trim();
                                            drConDetData["Barcode"] = containerBarcode;

                                            lineNo++;

                                            DateTime created = DateTime.Now;
                                            drConDetData["Created"] = created.Subtract(new TimeSpan(0, 0, 0, 0, created.Millisecond));
                                            drConDetData["CreatedBy"] = request.Authentication.APIUser;
                                            dtConDetData.Rows.Add(drConDetData);
                                            result = sqlDataAdapter.Update(dtConDetData) > 0;
                                            if (!result)
                                                break;
                                            else
                                                barcodeList.Add(containerBarcode);
                                        }

                                    }
                                }
                            }
                        }

                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        barcodeList = null;
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in ImportConsignmentData: {0}", ex.Message);
                    }
                }
            }
            else
            {
                using (SqlConnection sqlConn = new SqlConnection(dbConn))
                {
                    sqlConn.Open();
                    SqlTransaction sqlTrans = sqlConn.BeginTransaction();
                    try
                    {
                        string sqlStr = @"INSERT INTO NSW_Connote([ConsNo],[CDID],[ReturningOfficeName],[VenueCode],[VenueName],[VenueTypeCode],[VenueTypeName],
                                        [ContestCode],[ContestName],[ContainerCode],[ContainerName],[ContainerQuantity],[Created],[CreatedBy]) 
                                        VALUES (@ConsNo,@CDID,@ReturningOfficeName,@VenueCode,@VenueName,@VenueTypeCode,@VenueTypeName,
                                        @ContestCode,@ContestName,@ContainerCode,@ContainerName,@ContainerQuantity,@Created,@CreatedBy); SELECT CAST(scope_identity() AS int)";
                        DataTable dtRO = GetRODataTable(request.CDID);
                        DataTable dtVenue = GetVenueDataTable(request.CDID);
                        DataTable dtVenueType = GetVenueTypeDataTable(null);
                        DataTable dtContest = GetContestDataTable(null);
                        DataTable dtContainerType = GetContainerTypeDataTable(null);
                        DataRow[] drs = null;

                        int consKey = -1;
                        SqlCommand cmd = null;
                        using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                        {
                            cmd.Parameters.AddWithValue("@CDID", request.CDID.PadLeft(3, '0'));
                            if (dtRO != null && dtRO.Rows.Count > 0)
                            {
                                if (!(dtRO.Rows[0]["Name"] is DBNull) && dtRO.Rows[0]["Name"].ToString() != string.Empty)
                                    cmd.Parameters.AddWithValue("@ReturningOfficeName", dtRO.Rows[0]["Name"].ToString().Replace("RO Office", string.Empty).Trim());
                                else
                                    cmd.Parameters.AddWithValue("@ReturningOfficeName", DBNull.Value);
                            }   
                            else
                                cmd.Parameters.AddWithValue("@ReturningOfficeName", DBNull.Value);

                            cmd.Parameters.AddWithValue("@VenueCode", request.VenueCode.PadLeft(5, '0'));

                            drs = dtVenue.Select(string.Format("Code = '{0}'", request.VenueCode.PadLeft(5, '0')));
                            if (drs != null && drs.Length > 0)
                                cmd.Parameters.AddWithValue("@VenueName", drs[0]["Name"]);
                            else
                                cmd.Parameters.AddWithValue("@VenueName", DBNull.Value);

                            cmd.Parameters.AddWithValue("@VenueTypeCode", request.VenueTypeCode);

                            drs = dtVenueType.Select(string.Format("Code = '{0}'", request.VenueTypeCode));
                            if (drs != null && drs.Length > 0)
                                cmd.Parameters.AddWithValue("@VenueTypeName", drs[0]["Name"]);
                            else
                                cmd.Parameters.AddWithValue("@VenueTypeName", DBNull.Value);

                            cmd.Parameters.AddWithValue("@ContestCode", request.ContestCode);

                            drs = dtContest.Select(string.Format("Code = '{0}'", request.ContestCode));
                            if (drs != null && drs.Length > 0)
                                cmd.Parameters.AddWithValue("@ContestName", drs[0]["Name"]);
                            else
                                cmd.Parameters.AddWithValue("@ContestName", DBNull.Value);

                            cmd.Parameters.AddWithValue("@ContainerCode", request.ContainerCode);

                            drs = dtContainerType.Select(string.Format("Code = '{0}'", request.ContainerCode));
                            if (drs != null && drs.Length > 0)
                                cmd.Parameters.AddWithValue("@ContainerName", drs[0]["Name"]);
                            else
                                cmd.Parameters.AddWithValue("@ContainerName", DBNull.Value);

                            cmd.Parameters.AddWithValue("@ConsNo", consNo);
                            cmd.Parameters.AddWithValue("@ContainerQuantity", request.ContainerQty);
                            cmd.Parameters.AddWithValue("@Created", DateTime.Now);
                            cmd.Parameters.AddWithValue("@CreatedBy", request.Authentication.APIUser);

                            consKey = (Int32)cmd.ExecuteScalar();
                            result = consKey > 0;
                        }
                        if (result)
                        {
                            sqlStr = "SELECT * FROM NSW_Container WHERE ConsKey=@ConsKey";
                            using (cmd = new SqlCommand(sqlStr, sqlTrans.Connection, sqlTrans))
                            {
                                cmd.Parameters.AddWithValue("@ConsKey", consKey);
                                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                                {
                                    sqlDataAdapter.SelectCommand = cmd;
                                    using (SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter))
                                    {
                                        sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();
                                        sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                                        sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();

                                        sqlDataAdapter.DeleteCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.InsertCommand.Transaction = sqlTrans;
                                        sqlDataAdapter.UpdateCommand.Transaction = sqlTrans;

                                        DataTable dtConDetData = new DataTable();
                                        sqlDataAdapter.Fill(dtConDetData);

                                        DataRow drConDetData = null;
                                        string containerNo = string.Empty;
                                        string containerBarcode = string.Empty;
                                        int lineNo = 0;
                                        for (int i = 0; i < request.ContainerQty; i++)
                                        {
                                            lineNo++;

                                            drConDetData = dtConDetData.NewRow();
                                            drConDetData["ConsKey"] = consKey;
                                            drConDetData["LineNo"] = lineNo;
                                            containerNo = string.Format("{0}{1}", lineNo.ToString().PadLeft(3, '0'), (request.ContainerQty).ToString().PadLeft(3, '0'));
                                            drConDetData["ContainerNo"] = containerNo;
                                            containerBarcode = string.Format("{0}{1}", consNo, containerNo).Trim();
                                            drConDetData["Barcode"] = containerBarcode;

                                            drConDetData["Created"] = DateTime.Now;
                                            drConDetData["CreatedBy"] = request.Authentication.APIUser;
                                            dtConDetData.Rows.Add(drConDetData);
                                            result = sqlDataAdapter.Update(dtConDetData) > 0;
                                            if (!result)
                                                break;
                                            else
                                                barcodeList.Add(containerBarcode);
                                        }
                                    }
                                }
                            }
                        }
                        if (result)
                            sqlTrans.Commit();
                        else
                            sqlTrans.Rollback();
                    }
                    catch(Exception ex)
                    {
                        result = false;
                        barcodeList = null;
                        sqlTrans.Rollback();
                        procMsg = string.Format("Error in ImportConsignmentData: {0}", ex.Message);
                    }
                }
            }

            return barcodeList;
        }
        public static int GetMaxContainerLineNo(DataTable dtContainer)
        {
            int result = 0;
            if (dtContainer != null && dtContainer.Rows.Count > 0)
            {
                DataView dvContainer = new DataView(dtContainer);
                dvContainer.Sort = "LineNo asc";
                DataRowView drv = dvContainer[dvContainer.Count - 1];
                result = Convert.ToInt32(drv.Row["LineNo"]);
            }
            result++;
            return result;
        }
        public static int StrToIntDef(string s, int @default)
        {
            int number;
            if (int.TryParse(s, out number))
                return number;
            return @default;
        }
        private DataTable GetConnoteDataTable(object consNo)
        {
            string sqlStr = "SELECT * FROM NSW_Connote WHERE ConsNo=@ConsNo";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                sqlCmd.Parameters.AddWithValue("@ConsNo", consNo);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }
        private static DataTable GetContainerDataTable(object consKey)
        {
            string sqlStr = "SELECT * FROM NSW_Container WHERE ConsKey=@ConsKey";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                sqlCmd.Parameters.AddWithValue("@ConsKey", consKey);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        private DataTable GetContainerDataByBarcode(object barcode)
        {
            string sqlStr = "SELECT * FROM NSW_Container WHERE Barcode=@Barcode";

            DataTable dtResult = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                sqlCmd.Parameters.AddWithValue("@Barcode", barcode);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }

        private static string GetPdfLabelData(object barcode, string saveLabelFolder)
        {
            string result = string.Empty;
            try
            {
                object labelIdDefault = -1;
                string spDetaultName = "GetDefaultConnoteLabels";
                DataTable dtLabelLayOut = GetLabelLayoutDef(null);
                if (dtLabelLayOut != null && dtLabelLayOut.Rows.Count > 0)
                {
                    foreach (DataRow drLabelLayOut in dtLabelLayOut.Rows)
                    {
                        if (!(drLabelLayOut["LABEL_NAME"] is DBNull) && drLabelLayOut["LABEL_NAME"].ToString() == "Generic Label")
                        {
                            labelIdDefault = drLabelLayOut["ID"];
                            if (!(drLabelLayOut["STORED_PROCEDURE"] is DBNull) && drLabelLayOut["STORED_PROCEDURE"].ToString() != string.Empty)
                                spDetaultName = drLabelLayOut["STORED_PROCEDURE"].ToString();
                            break;
                        }
                    }
                }
                int labelId = -1;
                string labelIdSpec = string.Empty;
                DataTable dtApp = GetApplicationDataTable();
                if (dtApp != null && dtApp.Rows.Count > 0)
                {
                    if (!(dtApp.Rows[0]["PreferredLabel"] is DBNull) && dtApp.Rows[0]["PreferredLabel"].ToString() != null)
                    {
                        labelIdSpec = dtApp.Rows[0]["PreferredLabel"].ToString();
                        int labelIdNum;
                        if (int.TryParse(labelIdSpec, out labelIdNum))
                            labelId = labelIdNum;
                    }
                }
                if (labelId > 0)
                {    
                    using (XtraReport report = new XtraReport())
                    {
                        string spName = spDetaultName;
                        DataTable dtLayout = GetLabelLayoutDef(labelId);
                        if (dtLayout != null && dtLayout.Rows.Count > 0 && !(dtLayout.Rows[0]["LAYOUT"] is DBNull))
                        {
                            if (!(dtLayout.Rows[0]["STORED_PROCEDURE"] is DBNull) && dtLayout.Rows[0]["STORED_PROCEDURE"].ToString() != string.Empty)
                                spName = dtLayout.Rows[0]["STORED_PROCEDURE"].ToString();
                            MemoryStream ms = new MemoryStream(dtLayout.Rows[0]["LAYOUT"] as byte[]);
                            report.LoadLayout(ms);
                        }
                        //return string.Format("{0} - {1}", labelId, spName);
                        DataSet dsLabelData = GetConnoteLabelDataSet(spName, barcode);
                        if (dsLabelData != null && dsLabelData.Tables.Count > 0)
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                report.DataSource = dsLabelData;
                                report.DataMember = dsLabelData.Tables[0].TableName;

                                if (!string.IsNullOrEmpty(saveLabelFolder))
                                {
                                    string guidName = Guid.NewGuid().ToString().ToUpper();
                                    string fileDName = String.Format("{0}\\{1}.pdf", saveLabelFolder, guidName);
                                    report.ExportToPdf(fileDName);
                                    result = fileDName;
                                }
                                else
                                {
                                    report.ExportToPdf(ms);
                                    ms.Seek(0, SeekOrigin.Begin);
                                    byte[] rpt = ms.ToArray();
                                    result = Convert.ToBase64String(rpt, 0, rpt.Length);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result = string.Format("Error in GetPdfLabelData: {0}", ex.Message);
            }
            return result;
        }
        private static DataSet GetConnoteLabelDataSet(string spName, object barcode)
        {
            DataSet ds = new DataSet("DataSet");
            DataTable dt = GetConnoteLabelData(spName, barcode);
            ds.Tables.Add(dt);
            return ds;
        }
        public static DataTable GetConnoteLabelData(string spName, object barcode)
        {
            DataTable dtLabel = new DataTable();
            dtLabel.TableName = "ConnoteLabelTable";

            using (SqlConnection sqlConnection = new SqlConnection(dbConn))
            {
                String SelectCommand = String.Format("EXEC {0} '{1}'", spName, barcode);
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLabel);
                    }
                }
            }
            return dtLabel;
        }
        public static DataTable GetApplicationDataTable()
        {
            string sqlstr = "select top 1 * from NSW_Application ";
            DataTable dtApp = new DataTable();
            dtApp.TableName = "NSW_Application";

            using (SqlConnection sqlConnection = new SqlConnection(dbConn))
            {
                String SelectCommand = sqlstr;
                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtApp);
                    }
                }
            }
            return dtApp;
        }
        private static DataTable GetLabelLayoutDef(object labelId)
        {
            DataTable dtLayout = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(dbConn))
            {
                String SelectCommand = "SELECT * FROM SM_ONLINE_LABEL WHERE ID=@ID OR @ID IS NULL ORDER BY LABEL_NAME ";

                using (SqlCommand sqlCommand = new SqlCommand(SelectCommand, sqlConnection))
                {
                    if (labelId != null)
                        sqlCommand.Parameters.AddWithValue("@ID", labelId);
                    else
                        sqlCommand.Parameters.AddWithValue("@ID", DBNull.Value);
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = sqlCommand;
                        adapter.Fill(dtLayout);
                    }
                }
            }
            return dtLayout;
        }
    }
}
