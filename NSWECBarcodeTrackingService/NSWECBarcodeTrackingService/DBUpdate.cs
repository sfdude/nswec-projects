using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace DBTransactionUpdate
{
    public class DBUpdate
    {
        public string ID { get; set; }
        public string DBConn { get; set; }
        public DBTransaction[] DBTransactions { get; set; }
    }

    public class DBTransactionParam
    {
        public string ParamName { get; set; }
        public DbType ParamType { get; set; }
        public object ParamValue { get; set; }
    }

    public class DBTransaction
    {
        public int SQLSeq { get; set; }
        public string SQLStatement { get; set; }
        public DBTransactionParam[] SQLParams { get; set; }
    }
}
