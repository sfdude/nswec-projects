﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Data;

namespace NSWECBarcodeTrackingService
{
    
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public interface IBarcodeTrackingService
    {
        // SGE 2019 Maintenance
        [OperationContract]
        ApplicationEnquiryResponseType GetApplicationInfo(RequestMessageType applicationInfoRequest);
        [OperationContract]
        SourceEnquiryResponseType GetBarcodeSource(SourceEnquiryRequestType barcodeSourceRequest);
        [OperationContract]
        ProcessingLocationEnquiryResponseType GetProcessingLocation(ProcessingLocationEnquiryRequestType processingLocationRequest);
        [OperationContract]
        EMOfficeEnquiryResponseType GetEMOffice(EMOfficeEnquiryRequestType emOfficeRequest);
        [OperationContract]
        DistrictEnquiryResponseType GetDistrict(DistrictEnquiryRequestType districtRequest);
        [OperationContract]
        LocationTypeEnquiryResponseType GetLocationType(LocationTypeEnquiryRequestType locationTypeRequest);
        [OperationContract]
        LocationEnquiryResponseType GetLocation(LocationEnquiryRequestType locationRequest);
        [OperationContract]
        ProductEnquiryResponseType GetProduct(ProductEnquiryRequestType productRequest);

        // LGA 2017 Maintenance
        [OperationContract]
        CountCentreEnquiryResponseType GetCountCentre(CountCentreEnquiryRequestType countCentreRequest);
        [OperationContract]
        ReturningOfficeEnquiryResponseType GetReturningOffice(ReturningOfficeEnquiryRequestType returningOfficeRequest);
        [OperationContract]
        LGAEnquiryResponseType GetLGA(LGAEnquiryRequestType lgaRequest);
        [OperationContract]
        WardEnquiryResponseType GetWard(WardEnquiryRequestType wardRequest);
        [OperationContract]
        VenueEnquiryResponseType GetVenue(VenueEnquiryRequestType venueRequest);
        [OperationContract]
        VenueTypeEnquiryResponseType GetVenueType(VenueTypeEnquiryRequestType venueTypeRequest);
        [OperationContract]
        ContestEnquiryResponseType GetContest(ContestEnquiryRequestType contestRequest);

        // Generic Maintenance
        [OperationContract]
        ContainerTypeEnquiryResponseType GetContainerType(ContainerTypeEnquiryRequestType containerTypeRequest);
        [OperationContract]
        StatusStageEnquiryResponseType GetStatusStage(StatusStageEnquiryRequestType statusStageRequest);
        
        // LGA 2017 Operation
        [OperationContract]
        LGAEnquiryResponseType GetLGAForCountCentre(CountCentreEnquiryRequestType lgaCountCentreRequest);
        [OperationContract]
        ContainerBarcodeEnquiryResponseType GetLGAContainerBarcode(ContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        ContainerBarcodeEnquiryResponseType GetLGAContainerBarcodeLabel(ContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        ContainerBarcodeEnquiryResponseType GenerateLGAContainerBarcode(GenerateContainerBarcodeRequestType containerBarcodeRequest);
        [OperationContract]
        LGABarcodeScanStatusUpdateResponseType LGABarcodeScanStatusUpdate(LGABarcodeScanStatusUpdateRequsetType barcodeStatusUpdateRequest);

        // LGA 2020 Operation
        [OperationContract]
        DeliveryEnquiryResponseType GetDelivery(DeliveryEnquiryRequestType deliveryRequest);
        [OperationContract]
        ScanAtLocationEnquiryResponseType GetScanAtLocation(ScanAtLocationEnquiryRequestType scanAtLocationRequest);
        [OperationContract]        
        ContainerBarcodePropertyEnquiryResponseType GetLGAContainerBarcodeProperty(ContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        LGAContainerBarcodeEnquiryResponseType GetLGAContainerBarcodes(LGAContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        LGAContainerBarcodeEnquiryResponseType GetLGAContainerBarcodesLabel(LGAContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        LGA2020BarcodeScanStatusUpdateResponseType LGA2020BarcodeScanStatusUpdate(LGA2020BarcodeScanStatusUpdateRequsetType barcodeStatusUpdateRequest);

        // SGE 2019 Operation
        [OperationContract]
        SGEContainerBarcodeEnquiryResponseType GetSGEContainerBarcode(SGEContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        SGEContainerBarcodeEnquiryResponseType GetSGEContainerBarcodeLabel(SGEContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        SGEContainerBarcodeEnquiryResponseType GetSGEULDChildContainerBarcode(SGEULDChildContainerBarcodeEnquiryRequestType uldChildContainerBarcodeRequest);
        [OperationContract]
        SGEBarcodeScanStatusUpdateResponseType SGEBarcodeScanStatusUpdate(SGEBarcodeScanStatusUpdateRequsetType barcodeStatusUpdateRequest);

        // before 2017
        [OperationContract]
        ContainerBarcodeEnquiryResponseType GetContainerBarcode(ContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        ContainerBarcodeEnquiryResponseType GetContainerBarcodeLabel(ContainerBarcodeEnquiryRequestType containerBarcodeRequest);
        [OperationContract]
        ContainerBarcodeEnquiryResponseType GenerateContainerBarcode(GenerateContainerBarcodeRequestType containerBarcodeRequest);
        [OperationContract]
        BarcodeScanStatusUpdateResponseType BarcodeScanStatusUpdate(BarcodeScanStatusUpdateRequsetType barcodeStatusUpdateRequest);
        
        // TODO: Add your service operations here
        [OperationContract]
        ResponseMessageType GetMasterData(RequestMessageType request);

        [OperationContract]
        ResponseMessageType TestWebService(RequestMessageType request);
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class AuthenticationType
    {
        [DataMember(IsRequired = true)]
        public string APIUser { get; set; }
        [DataMember]
        public string APIKey { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class MessageType
    {
        [DataMember]
        public string DocumentType { get; set; }
        [DataMember]
        public string DocumentID { get; set; }
        [DataMember]
        public string DateTimeStamp { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class RequestMessageType : MessageType
    {
        [DataMember(IsRequired = true)]
        public AuthenticationType Authentication { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class SourceEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ProcessingLocationEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public bool ExcludeROAsCC { get; set; }

    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class CountCentreEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class EMOfficeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ReturningOfficeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string CDID { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class DistrictEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string EMOCode { get; set; }
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGAEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class WardEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string LGACode { get; set; }
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LocationEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string EMOCode { get; set; }
        [DataMember]
        public string DistrictCode { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string LocationTypeCode { get; set; }
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class VenueEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string LGACode { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string VenueTypeCode { get; set; }
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LocationTypeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string EMOCode { get; set; }
        [DataMember]
        public string DistrictCode { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class VenueTypeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string LGACode { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ProductEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContestEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContainerTypeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class StatusStageEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string StageId { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class SGEULDChildContainerBarcodeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string ULDBarcode { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class SGEContainerBarcodeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string EMOCode { get; set; }
        [DataMember]
        public string DistrictCode { get; set; }
        [DataMember]
        public string ContestAreaId { get; set; }
        [DataMember]
        public string VenueCode { get; set; }
        [DataMember]
        public string VenueTypeCode { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
        [DataMember]
        public string Barcode { get; set; }
        [DataMember]
        public string StageFlag { get; set; }
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public string ScannedLocationCode { get; set; }
        [DataMember]
        public string ContainerCode { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class DeliveryEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Number { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ScanAtLocationEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string Code { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGAContainerBarcodeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string DeliveryNumber { get; set; }
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string LGACode { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string VenueCode { get; set; }
        [DataMember]
        public string VenueTypeCode { get; set; }
        [DataMember]
        public string ContestCode { get; set; }
        [DataMember]
        public string ContainerCode { get; set; }
        [DataMember]
        public string BarcodeList { get; set; }
        [DataMember]
        public string StageFlag { get; set; }
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public string ScanLocationCode { get; set; }
        [DataMember]
        public string CountCentreCode { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContainerBarcodeEnquiryRequestType : RequestMessageType
    {
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string LGACode { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string VenueCode { get; set; }
        [DataMember]
        public string VenueTypeCode { get; set; }
        [DataMember]
        public string ContestCode { get; set; }
        [DataMember]
        public string Barcode { get; set; }
        [DataMember]
        public string StageFlag { get; set; }
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public string CountCentreCode { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class GenerateContainerBarcodeRequestType : RequestMessageType
    {
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string LGACode { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string VenueCode { get; set; }
        [DataMember]
        public string VenueName { get; set; }
        [DataMember]
        public string VenueTypeCode { get; set; }
        [DataMember]
        public string ContestCode { get; set; }
        [DataMember]
        public string ContainerCode { get; set; }
        [DataMember]
        public int ContainerQty { get; set; }
        [DataMember]
        public string PredefinedBarCode { get; set; }
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public string From { get; set; }
        [DataMember]
        public string To { get; set; }
        [DataMember]
        public string CountCentreCode { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class BarcodeScanStatusUpdateRequsetType : RequestMessageType
    {
        [DataMember]
        public int StageId { get; set; }
        [DataMember]
        public string From { get; set; }
        [DataMember]
        public string To { get; set; }
        [DataMember]
        public BarcodeScanStatus[] BarcodeScanList { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class SGEBarcodeScanStatusUpdateRequsetType : RequestMessageType
    {
        [DataMember]
        public bool Reprint { get; set; }
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public ScannedLocation ScannedFrom { get; set; }
        [DataMember]
        public ScannedLocation ScannedTo { get; set; }
        [DataMember]
        public BarcodeScanStatus[] BarcodeScanList { get; set; }
        [DataMember]
        public string CountCentreCode { get; set; }
        [DataMember]
        public ScannedLocation ScannedAt { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGA2020BarcodeScanStatusUpdateRequsetType : SGEBarcodeScanStatusUpdateRequsetType
    {
        
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGABarcodeScanStatusUpdateRequsetType : RequestMessageType
    {
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public string From { get; set; }
        [DataMember]
        public string To { get; set; }
        [DataMember]
        public BarcodeScanStatus[] BarcodeScanList { get; set; }
        [DataMember]
        public string CountCentreCode { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ApplicationInfo
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Information { get; set; }
        [DataMember]
        public EventType EventType { get; set; }
        [DataMember]
        public int PPDMVersion { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class BarcodeSource
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Start { get; set; }
        [DataMember]
        public string Stop { get; set; }
        [DataMember]
        public int Length { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ProcessingLocation
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LocationTypeCode { get; set; }
        [DataMember]
        public string LocationTypeName { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class EMOffice
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LocationTypeCode { get; set; }
        [DataMember]
        public string LocationTypeName { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class CountCentre
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ReturningOffice
    {
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CountCentreCode { get; set; }
        [DataMember]
        public string CountCentreName { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class District
    {
        /*
        [DataMember(EmitDefaultValue = false)]
        public EMOffice EMOffice { get; set; }
         */
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string ROName { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContestArea
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string Code { get; set; }        
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGA
    {
        //[DataMember(EmitDefaultValue = false)]
        //public ReturningOffice ReturningOffice { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string ROName { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class Ward
    {
        /*
        [DataMember(EmitDefaultValue = false)]
        public EMOffice EMOffice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public ReturningOffice ReturningOffice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public District District { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public LGA Lga { get; set; }
         */
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string ROName { get; set; }
        [DataMember]
        public string LGACode { get; set; }
        [DataMember]
        public string LGAName { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class Location
    {
        /*
        [DataMember(EmitDefaultValue = false)]
        public EMOffice EMOffice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public District District { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public ContestArea ContestArea { get; set; }
        
        [DataMember]
        public Ward Ward { get; set; }
         */
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        /*
        [DataMember(EmitDefaultValue = false)]
        public LocationType LocationType { get; set; }
         */
        [DataMember]
        public string LocationTypeCode { get; set; }
        [DataMember]
        public string LocationTypeName { get; set; }
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string ROName { get; set; }
        [DataMember]
        public string DistrictCode { get; set; } // this is lga
        [DataMember]
        public string DistrictName { get; set; }
        [DataMember]
        public string ContestAreaCode { get; set; }
        [DataMember]
        public string ContestAreaName { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string WardName { get; set; }
    }
       

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class Venue
    {
        /*
        [DataMember(EmitDefaultValue = false)]
        public ReturningOffice ReturningOffice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public LGA Lga { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public Ward Ward { get; set; }
         */
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CDID { get; set; }
        [DataMember]
        public string ROName { get; set; }
        [DataMember]
        public string LGACode { get; set; } 
        [DataMember]
        public string LGAName { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string WardName { get; set; }
        /*
        [DataMember(EmitDefaultValue = false)]
        public VenueType VenueType { get; set; }
         */
        [DataMember]
        public string VenueTypeCode { get; set; }
        [DataMember]
        public string VenueTypeName { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LocationType
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class VenueType
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class Product
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class Contest
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContainerType
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class StatusStage
    {
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public string Description { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class BarcodeScanStatus
    {
        [DataMember]
        public string Barcode { get; set; }
        [DataMember]
        public string DateTimeStamp { get; set; }
        [DataMember]
        public string Notes { get; set; }
        [DataMember]
        public bool Removed { get; set; }
        [DataMember]
        public BarcodeLinkage BarcodeLinkage { get; set; } // this could be "Allocation from Reserve Stock", "Replacement" or "ULD_Parent" 
        // for "Allocation from Reserve Stock"         
        [DataMember]
        public Location Location { get; set; }
        [DataMember]
        public Product Product { get; set; }
        [DataMember]
        public ContainerType ContainerType { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class BarcodeLinkage
    {
        [DataMember]
        public string Barcode { get; set; }
        [DataMember]
        public BarcodeLinkType BarcodeLinkType { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class BarcodeScanStatusResult
    {
        [DataMember]
        public string Barcode { get; set; }
        [DataMember]
        public bool Accepted { get; set; }
        [DataMember]
        public bool Removed { get; set; }
        [DataMember]
        public string Notes { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class SGEContainerBarcode
    {
        [DataMember]
        public string Barcode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public EMOffice EMOffice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public District District { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public ContestArea ContestArea { get; set; }
        /*
        [DataMember]
        public Ward Ward { get; set; }
         */
        [DataMember(EmitDefaultValue = false)]
        public Location Location { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public VenueType VenueType { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public Product Product { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public ContainerType ContainerType { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public StatusStage StatusStage { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string StatusDate { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Attachment { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CountCentre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public ScannedLocation ScannedLocation { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ScannedLocation
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class Delivery
    {
        [DataMember]
        public string Number { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ScanAtLocation
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LocationType { get; set; }
        [DataMember]
        public string LocationTypeName { get; set; }        
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContainerBarcodeProperty
    {
        [DataMember]
        public string Barcode { get; set; }
        [DataMember]
        public string ROCode { get; set; }
        [DataMember]
        public string ROName { get; set; }
        [DataMember]
        public string LGACode { get; set; }
        [DataMember]
        public string LGAName { get; set; }
        [DataMember]
        public string ContestAreaCode { get; set; }
        [DataMember]
        public string ContestAreaName { get; set; }
        [DataMember]
        public string WardCode { get; set; }
        [DataMember]
        public string WardName { get; set; }
        [DataMember]
        public string VenueCode { get; set; }
        [DataMember]
        public string VenueName { get; set; }
        [DataMember]
        public string VenueTypeCode { get; set; }
        [DataMember]
        public string VenueTypeName { get; set; }
        [DataMember]
        public string ContestCode { get; set; }
        [DataMember]
        public string ContestName { get; set; }
        [DataMember]
        public string ContainerCode { get; set; }
        [DataMember]
        public string ContainerName { get; set; }
        [DataMember]
        public string CountCentreCode { get; set; }
        [DataMember]
        public string CountCentreName { get; set; }
        [DataMember]
        public string LocationCode { get; set; }
        [DataMember]
        public string LocationName { get; set; }
        [DataMember]
        public string StatusDate { get; set; }
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public string StageDescription { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGAContainerBarcode
    {
        [DataMember]
        public string Barcode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string StatusDate { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string StatusStage { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string StatusNotes { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string LocationCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string LocationName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string SourceId { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CDID { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ReturningOfficeName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string LGACode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string LGAName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string WardCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string WardName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string VenueCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string VenueName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string VenueTypeCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string VenueTypeName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ContestCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ContestName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ContainerCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ContainerName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CountCentreCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CountCentreName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ROCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ROName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Attachment { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContainerBarcode
    {
        [DataMember]
        public string Barcode { get; set; }
        [DataMember]
        public ReturningOffice ReturningOffice { get; set; }
        [DataMember]
        public LGA Lga { get; set; }
        [DataMember]
        public Ward Ward { get; set; }
        [DataMember]
        public Venue Venue { get; set; }
        [DataMember]
        public VenueType VenueType { get; set; }
        [DataMember]
        public Contest Contest { get; set; }
        [DataMember]
        public ContainerType ContainerType { get; set; }
        [DataMember]
        public StatusStage StatusStage { get; set; }
        [DataMember]
        public string StatusDate { get; set; }
        [DataMember]
        public string Attachment { get; set; }
        [DataMember]
        public string CountCentre { get; set; }
    }
    
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ResponseMessageType : MessageType
    {
        [DataMember]
        public string Notes { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ApplicationEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public ApplicationInfo ApplicationInfo { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class SourceEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public BarcodeSource[] BarcodeSources { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ProcessingLocationEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public ProcessingLocation[] ProcessingLocations { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class EMOfficeEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public EMOffice[] EMOffices { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class CountCentreEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public CountCentre[] CountCentres { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ReturningOfficeEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public ReturningOffice[] ReturningOffices { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class DistrictEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public District[] Districts { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGAEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public LGA[] LGAs { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class WardEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public Ward[] Wards { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LocationEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public Location[] Locations { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class VenueEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public Venue[] Venues { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LocationTypeEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public LocationType[] LocationTypes { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class VenueTypeEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public VenueType[] VenueTypes { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContainerTypeEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public ContainerType[] ContainerTypes { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ProductEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public Product[] Products { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContestEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public Contest[] Contests { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class SGEContainerBarcodeEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public SGEContainerBarcode[] ContainerBarcodes { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class DeliveryEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public Delivery[] Deliveries { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ScanAtLocationEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public ScanAtLocation[] ScanAtLocations { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContainerBarcodePropertyEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public ContainerBarcodeProperty[] ContainerBarcodeProperties { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGAContainerBarcodeEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public LGAContainerBarcode[] ContainerBarcodes { get; set; }
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class ContainerBarcodeEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public ContainerBarcode[] ContainerBarcodes { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class StatusStageEnquiryResponseType : ResponseMessageType
    {
        [DataMember]
        public StatusStage[] StatusStages { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class BarcodeScanStatusUpdateResponseType : ResponseMessageType
    {
        [DataMember]
        public int StageId { get; set; }
        [DataMember]
        public string From { get; set; }
        [DataMember]
        public string To { get; set; }
        [DataMember]
        public BarcodeScanStatusResult[] BarcodeScanStatusResultList { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class SGEBarcodeScanStatusUpdateResponseType : ResponseMessageType
    {
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public ScannedLocation ScannedFrom { get; set; }
        [DataMember]
        public ScannedLocation ScannedTo { get; set; }
        [DataMember]
        public string CountCentre { get; set; }        
        [DataMember]
        public BarcodeScanStatusResult[] BarcodeScanStatusResultList { get; set; }
        [DataMember]
        public ScannedLocation ScannedAt { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGA2020BarcodeScanStatusUpdateResponseType : SGEBarcodeScanStatusUpdateResponseType
    {
        
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public class LGABarcodeScanStatusUpdateResponseType : ResponseMessageType
    {
        [DataMember]
        public string StageId { get; set; }
        [DataMember]
        public string From { get; set; }
        [DataMember]
        public string To { get; set; }
        [DataMember]
        public string CountCentre { get; set; }
        [DataMember]
        public BarcodeScanStatusResult[] BarcodeScanStatusResultList { get; set; }
    }

    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public enum BarcodeLinkType
    {
        [EnumMember]
        NotDefined,
        [EnumMember]
        ReserveStock,
        [EnumMember]
        Replacement,
        [EnumMember]
        ULD_Linkage
    }
    [DataContract(Namespace = "http://schemas.compdata.com.au/BarcodeTrackingService")]
    public enum EventType
    {
        [EnumMember]
        NotDefined,
        [EnumMember]
        LocalGoverment,
        [EnumMember]
        StateByElection,
        [EnumMember]
        StateElection
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    public class CartonStatus
    {
        public string Barcode { get; set; }
        public string DateTimeStamp { get; set; }
        //public int ConsKey { get; set; }
        public string ConsKey { get; set; }
        public int LineNo { get; set; }
        public int StatusStageId { get; set; }
        public string StatusStage { get; set; }
        public bool Accepted { get; set; }
        public bool Removed { get; set; }
        public bool Added { get; set; }
        public bool Activated { get; set; }
        public string Notes { get; set; }
        public string CDID { get; set; }
        public string ReturningOfficeName { get; set; }
        public string LGACode { get; set; }
        public string LGAName { get; set; }
        public string WardCode { get; set; }
        public string WardName { get; set; }
        public string VenueCode { get; set; }
        public string VenueName { get; set; }
        public string VenueTypeCode { get; set; }
        public string VenueTypeName { get; set; }
        public string ContestCode { get; set; }
        public string ContestName { get; set; }
        public string ContainerCode { get; set; }
        public string ContainerName { get; set; }
        public int ContainerQty { get; set; }
        public int ContainerSeq { get; set; }
        /*
        public int DeliveryNumber { get; set; }
        public string CountCentreCode { get; set; }
        public string CountCentreName { get; set; }   
         * */
        public BarcodeLinkage BarcodeLinkage { get; set; } // this could be "Allocation from Reserve Stock", "Replacement" or "ULD_Parent" 
        // for "Allocation from Reserve Stock"
        public Location Location { get; set; }
        public Product Product { get; set; }
        public ContainerType ContainerType { get; set; } 
    }
    public class ConnoteStatus
    {
        public string ConsNo { get; set; }
        public string Notes { get; set; }
        public List<CartonStatus> CartonStatusList { get; set; }
        //public Location ScannedLocation { get; set; }
    }

    public class ConnoteStatusCheck
    {
        public int StageId { get; set; }
        public string StatusStage { get; set; }
        public string DeviceId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public ScannedLocation ScannedFrom { get; set; }
        public ScannedLocation ScannedTo { get; set; }
        public ScannedLocation ScannedAt { get; set; }

        public string CountCentre { get; set; }
        //public ScannedLocation ScannedLocation { get; set; }
       
        public string Notes { get; set; }
        public bool HasError { get; set; }
        public bool HasOK { get; set; }
        public List<ConnoteStatus> ConnoteStatusList { get; set; }
    }

    public class ConnoteInserted
    {
        public string ConsNo { get; set; }
        public int ConsKey { get; set; }
        public int MaxLineNo { get; set; }
    }

    public class ConnoteInsertedList
    {
        private List<ConnoteInserted> _insertedList = null;
        public ConnoteInsertedList()
        {
            if (_insertedList == null)
                _insertedList = new List<ConnoteInserted>();
        }
        public List<ConnoteInserted> InsertedList
        {
            get
            {
                if (_insertedList == null)
                    _insertedList = new List<ConnoteInserted>();
                return _insertedList;
            }
        }
        public int Count
        {
            get
            {
                return _insertedList.Count;
            }
        }
        public ConnoteInserted this[int index]
        {
            get
            {
                return _insertedList[index];
            }
            set
            {
                _insertedList[index] = value;
            }
        }
        public void Add(ConnoteInserted connoteInserted)
        {
            _insertedList.Add(connoteInserted);
        }
        public void Delete(int index)
        {
            if (index > -1 && index < _insertedList.Count)
                _insertedList.RemoveAt(index);
        }

        public ConnoteInserted GetConnoteInserted(string consNo)
        {
            ConnoteInserted inserted = null;
            foreach (ConnoteInserted item in InsertedList)
            {
                if (item.ConsNo != null && !string.IsNullOrEmpty(item.ConsNo) && string.Compare(item.ConsNo, consNo, true) == 0)
                {
                    item.MaxLineNo = item.MaxLineNo + 1;
                    inserted = item;
                    break;
                }
            }
            return inserted;
        }
    }
        
}