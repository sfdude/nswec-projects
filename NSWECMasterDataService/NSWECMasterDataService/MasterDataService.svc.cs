﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace NSWECMasterDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class MasterDataService : IMasterDataService
    {
        private static readonly string dbConn = ConfigurationManager.ConnectionStrings["DefaultDBConString"].ToString();
        private static string _saveRequestResponseFolder = string.Empty;

        ResponseMessageType IMasterDataService.GetMasterData(RequestMessageType request)
        {
            _saveRequestResponseFolder = string.Format("{0}//Upload", AppDomain.CurrentDomain.BaseDirectory);
            if (!Directory.Exists(_saveRequestResponseFolder))
                Directory.CreateDirectory(_saveRequestResponseFolder);
            using (StreamWriter w = File.AppendText(string.Format("{0}\\GetMasterData.log", _saveRequestResponseFolder)))
            {
                LogRequest("GetMasterData Request Received", w);
            }
            ResponseMessageType resp = new ResponseMessageType();
            resp.DocumentType = request.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(request, ref procMsg))
                    resp.Notes = string.Format("Error in GetMasterData: {0}", procMsg);
                else
                {
                    DataSet ds = new DataSet();
                    ds.Tables.Add(GetDataTable("NSW_Connote"));
                    ds.Tables.Add(GetDataTable("NSW_Container"));
                    ds.Tables.Add(GetDataTable("NSW_ContainerStatus"));

                    using (StringWriter sw = new StringWriter())
                    using (MemoryStream zipStream = new MemoryStream())
                    using (GZipStream zip = new GZipStream(zipStream, CompressionMode.Compress))
                    {
                        ds.WriteXml(sw, XmlWriteMode.WriteSchema);
                        byte[] data = Encoding.ASCII.GetBytes(sw.ToString());
                        zip.Write(data, 0, data.Length);
                        zip.Close();
                        resp.Notes = Convert.ToBase64String(zipStream.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in GetMasterData: {0}", ex.Message);
            }
            return resp;
        }
        ResponseMessageType IMasterDataService.TestWebService(RequestMessageType request)
        {
            ResponseMessageType resp = new ResponseMessageType();
            resp.DocumentType = request.DocumentType;
            DateTime dtDoc = DateTime.Now;
            resp.DocumentID = dtDoc.ToString("yyyyMMddHHmmss");
            resp.DateTimeStamp = string.Format("{0}{1}", dtDoc.ToString("yyyy-MM-dd'T'HH:mm:ss"), dtDoc.ToString("zzz").Replace(":", string.Empty));
            string procMsg = string.Empty;
            try
            {
                if (!CheckAuthentication(request, ref procMsg))
                    resp.Notes = procMsg;
                else
                    resp.Notes = "Test Web Service is successful";
            }
            catch (Exception ex)
            {
                resp.Notes = string.Format("Error in TestWebService: {0}", ex.Message);
            }
            return resp;
        }
        private static void LogRequest(string logMessage, TextWriter w)
        {
            w.Write(string.Format("{0}{1}: {2}", Environment.NewLine, DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fff"), logMessage));
        }
        private static bool CheckAuthentication(RequestMessageType request, ref string procMsg)
        {
            bool result = request.Authentication != null && !string.IsNullOrEmpty(request.Authentication.APIUser);
            if (result)
            {
                result = loginSuccess(request);
                if (!result)
                    procMsg = "Authentication Failed";
            }
            else
                procMsg = "Authentication information in header not provided";
            return result;
        }
        private static bool loginSuccess(RequestMessageType request)
        {
            return string.Compare(request.Authentication.APIUser, ConfigurationManager.AppSettings["APIUser"], true) == 0 &&
                string.Compare(request.Authentication.APIKey, ConfigurationManager.AppSettings["APIKey"], false) == 0;            
        }
        private static DataTable GetDataTable(string tableName)
        {
            string sqlStr = string.Format("SELECT * FROM {0} WITH (NOLOCK)", tableName);
            DataTable dtResult = new DataTable(tableName);

            using (SqlConnection sqlConn = new SqlConnection(dbConn))
            using (SqlCommand sqlCmd = new SqlCommand(sqlStr, sqlConn))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtResult);
            }
            return dtResult;
        }
    }
}
