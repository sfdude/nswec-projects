﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace NSWECMasterDataService
{
    [ServiceContract(Namespace = "http://schemas.smartfreight.com/NSWECMasterDataService")]
    public interface IMasterDataService
    {
        [OperationContract]
        ResponseMessageType GetMasterData(RequestMessageType request);

        [OperationContract]
        ResponseMessageType TestWebService(RequestMessageType request);
    }

    [DataContract(Namespace = "http://schemas.smartfreight.com/NSWECMasterDataService")]
    public class AuthenticationType
    {
        [DataMember(IsRequired = true)]
        public string APIUser { get; set; }
        [DataMember]
        public string APIKey { get; set; }
    }
    [DataContract(Namespace = "http://schemas.smartfreight.com/NSWECMasterDataService")]
    public class MessageType
    {
        [DataMember]
        public string DocumentType { get; set; }
        [DataMember]
        public string DocumentID { get; set; }
        [DataMember]
        public string DateTimeStamp { get; set; }
    }
    [DataContract(Namespace = "http://schemas.smartfreight.com/NSWECMasterDataService")]
    public class RequestMessageType : MessageType
    {
        [DataMember(IsRequired = true)]
        public AuthenticationType Authentication { get; set; }
    }
    [DataContract(Namespace = "http://schemas.smartfreight.com/NSWECMasterDataService")]
    public class ResponseMessageType : MessageType
    {
        [DataMember]
        public string Notes { get; set; }
    }
}
